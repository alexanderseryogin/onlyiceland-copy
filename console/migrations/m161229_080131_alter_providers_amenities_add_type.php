<?php

use yii\db\Migration;

class m161229_080131_alter_providers_amenities_add_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_amenities` ADD COLUMN `type` TINYINT(4) NULL AFTER `amenities_id`; ");
    }

    public function down()
    {
        echo "m161229_080131_alter_providers_amenities_add_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

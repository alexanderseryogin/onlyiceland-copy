<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Providers */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Destinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// echo "<pre>";
// print_r($provider_model->active);
// exit();
?>
<div class="providers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'provider_model' => $provider_model,
        'booking_policies_model' => $booking_policies_model,
        'provider_financial_model' => $provider_financial_model,
        'booking_rules_model' => $booking_rules_model,
        'destination_open_all_year' => $destination_open_all_year,
        'open_hours' => $open_hours,
        'meal_options' => $meal_options,
        'tab_href' => $tab_href,
        'destination_travel_partners' => $travel_partners,
        'destination_add_ons' => $destination_add_ons,
        'amenities' => $amenities,

    ]) ?>

</div>

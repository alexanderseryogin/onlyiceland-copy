<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Destinations;

$this->title = "CSV Booking Import";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];

?>

<style type="text/css">
	.tabbable-custom > .nav-tabs > li.active {
    border-top: 3px solid #36C6D3;
    margin-top: 0;
    position: relative;
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md" style="width: 100%;">
                    CSV Booking Import
                    <a href="<?=Yii::getAlias('@web').'/../uploads/sample.csv'?>" class="pull-right" download>
					  <button type="button" class="btn btn-success">Download Sample CSV</button>
					</a>
                </div>
            </div>
            
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-sm-12">
                        	<div class="speakers-form">

							    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

							    <div class="row">
							    	<div class="col-sm-6">
							    		<?= $form->field($model, 'provider_id', [
                                            'inputOptions' => [
                                                    'id' => 'provider_dropdown',
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%'
                                                    ]
                                        ])->dropdownList(ArrayHelper::map(Destinations::find()->all(),'id','name'),['prompt'=>'Select a Destination']) ?>
							    	</div>
							    	<div class="col-sm-3">
							    		<?= $form->field($model, 'csv_file')->fileInput() ?>
							    	</div>
							    	<div class="col-sm-3">
							    		<br>
							    		<?= $form->field($model, 'update_existing_booking')->checkbox(); ?>
							    	</div>
							    </div>

							    <div class="form-group">
							        <?= Html::submitButton(Yii::t('app', 'Upload'), ['class' => 'btn btn-primary']) ?>
							    </div>

							    <?php ActiveForm::end(); ?>



							</div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                    	<div class="col-sm-12">
                    		<?php if(!empty($resultArr)):

                    			$successResult = isset($resultArr['success'])?$resultArr['success']:[];
                    			$updateResult = isset($resultArr['update'])?$resultArr['update']:[];
                    			$errorResult = isset($resultArr['error'])?$resultArr['error']:[];
                    		?>
                    			<div class="tabbable-custom nav-justified">
	                                <ul class="nav nav-tabs nav-justified">
	                                    <li class="active">
	                                        <a href="#tab_1_1_1" data-toggle="tab"> Created <span class="badge badge-success"> <?=count($successResult)?> </span></a>
	                                    </li>
	                                    <li>
	                                        <a href="#tab_1_1_2" data-toggle="tab"> Updated <span class="badge badge-primary"> <?=count($updateResult)?> </span> </a>
	                                    </li>
	                                    <li>
	                                        <a href="#tab_1_1_3" data-toggle="tab"> Errors <span class="badge badge-danger"> <?=count($errorResult)?> </span> </a>
	                                    </li>
	                                </ul>
	                                <div class="tab-content">
	                                    <div class="tab-pane active" id="tab_1_1_1">
	                                        <?php if(!empty($successResult)): ?>
		                    					<div class="note note-success" style="height: 400px; overflow-y: auto">
		                                        	<?php foreach ($successResult as $key => $value): ?>
			                    						<p>Booking Ref No [<?=$key?>] : <?=$value?> </p><br>
			                    					<?php endforeach; ?>
			                    				</div>
			                    			<?php endif; ?>
	                                    </div>
	                                    <div class="tab-pane" id="tab_1_1_2">
	                                        <?php if(!empty($updateResult)): ?>
	                                        	<div class="note note-info" style="height: 400px; overflow-y: auto">
		                                        	<?php foreach ($updateResult as $key => $value): ?>
			                    						<p>Booking Ref No [<?=$key?>] : <?=$value?> </p><br>
			                    					<?php endforeach; ?>
			                    				</div>
	                                        <?php endif; ?>
	                                    </div>
	                                    <div class="tab-pane" id="tab_1_1_3">
	                                        <?php if(!empty($errorResult)): ?>
	                                        	<div class="note note-danger" style="height: 400px; overflow-y: auto">
		                                        	<?php foreach ($errorResult as $key => $value): ?>
			                    						<p>Booking Ref No [<?=$key?>] : <?=$value?> </p><br>
			                    					<?php endforeach; ?>
			                    				</div>
	                                        <?php endif; ?>
	                                    </div>
	                                </div>
	                            </div>
                    		<?php endif; ?>
                    	</div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-12">
                    		<?php
                    			/*if(!empty($dataArray))
                    			{
                    				echo '<pre>';
                    				print_r($dataArray);
                    			}*/
                    		?>
                    	</div>
                    </div>
                </div>
                <br>        
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs('
		$(document).ready(function(){
			if($("#provider_dropdown").val() == "")
			{
				document.getElementById("importcsvmodel-csv_file").disabled = true;
			}
			else
			{
				document.getElementById("importcsvmodel-csv_file").disabled = false;
			}

			$("#provider_dropdown").on("change",function(){
				if($(this).val() == "")
				{
					console.log("here in if");
					document.getElementById("importcsvmodel-csv_file").disabled = true;
				}
				else
				{
					console.log("here in else");
					document.getElementById("importcsvmodel-csv_file").disabled = false;
				}
			});
		});
		
	');

?>
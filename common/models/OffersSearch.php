<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Offers;

/**
 * OffersSearch represents the model behind the search form about `common\models\Offers`.
 */
class OffersSearch extends Offers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_id', 'item_id', 'booking_confirmation_type_id', 'show','commission'], 'integer'],
            [['name', 'summary', 'additional_details', 'allow_cancellations', 'min_stay'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Offers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => isset($params['page'])?($params['page']-1):0,
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'item_id' => $this->item_id,
            'booking_confirmation_type_id' => $this->booking_confirmation_type_id,
            'show' => $this->show,
            'commission' => $this->commission,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'additional_details', $this->additional_details])
            ->andFilterWhere(['like', 'allow_cancellations', $this->allow_cancellations])
            ->andFilterWhere(['like', 'min_stay', $this->min_stay]);

        return $dataProvider;
    }
}

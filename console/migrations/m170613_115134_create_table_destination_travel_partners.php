<?php

use yii\db\Migration;

class m170613_115134_create_table_destination_travel_partners extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `destination_travel_partners`(  
                          `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                          `destination_id` INT(11),
                          `travel_partner_id` INT(11),
                          `comission` INT(11),
                          `booking_email_address` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci,
                          `invoice_email_address` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci,
                          `primary_contact` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci,
                          `billing_contact` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`destination_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`travel_partner_id`) REFERENCES `travel_partner`(`id`) ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170613_115134_create_table_destination_travel_partners cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

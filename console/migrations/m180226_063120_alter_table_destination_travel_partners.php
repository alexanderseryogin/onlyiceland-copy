<?php

use yii\db\Migration;

class m180226_063120_alter_table_destination_travel_partners extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `destination_travel_partners`   
                          CHANGE `comission` `comission` FLOAT(11) NULL;
                        ');
    }   

    public function safeDown()
    {
        echo "m180226_063120_alter_table_destination_travel_partners cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180226_063120_alter_table_destination_travel_partners cannot be reverted.\n";

        return false;
    }
    */
}

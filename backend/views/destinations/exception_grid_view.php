<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<style type="text/css">
    #exception-gridview div
    {
        display: block;
    }
</style>
<div id="exception-gridview-div">
    <a class="btn red" data-pjax='0' id="delAll" href="javascript:void(0)">
        <i class="fa fa-trash-o"></i><?= Yii::t('app','Delete Selected') ?> 
    </a>
    <br><br>
    <?php Pjax::begin(['id' => 'exception-gridview','timeout' => false, 'enablePushState' => false]) ?>
        
        <?= GridView::widget([

        'dataProvider' => $dataProviderException,
        'columns' => [
            [
                'class'     => 'yii\grid\CheckboxColumn',
                'multiple'  => true,
                'options'   => ['style' => 'width: 2%'],
            ],
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'label' => 'Date',
                'value' => function($data)
                {
                    return date('d/m/Y',strtotime($data->date));
                },
            ],
            [
                'label' => 'Opening',
                'value' => function($data)
                {
                    if(!empty($data->opening))
                        return date('H:i',strtotime($data->opening));
                    return 'N/A';
                },
            ],
            [
                'label' => 'Closing',
                'value' => function($data)
                {
                    if(!empty($data->closing))
                        return date('H:i',strtotime($data->closing));
                    return 'N/A';
                },
            ],
            [
                'label' => 'Open',
                'value' => function($data)
                {
                    if($data->state)
                        return 'Yes';
                    else
                        return 'No';
                },
            ],
            [
                'label' => 'Hours 24',
                'value' => function($data)
                {
                    if($data->hours_24)
                        return 'Yes';
                    else
                        return 'No';
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                       return '<a style="text-decoration: none;" aria-label="update" data-id='.$model->id.' title="update" name="exception_update[]">
                                <span class="glyphicon glyphicon-pencil"></span>
                                </a>';
                  },
                  'delete' => function ($url, $model, $key) {
                       return '<a style="text-decoration: none;" aria-label="delete" data-id='.$model->id.' title="delete" name="exception_delete[]">
                                <span class="glyphicon glyphicon-trash"></span>
                                </a>';
                  },
              ]
            ],
        ],
    ]); ?>

    <?php Pjax::end() ?>
</div>

<?php 
    
    $deleteUrl = Yii::$app->urlManager->createUrl('destinations/delete-all');
    $this->registerJs("
        $(document).ready(function () {
            $('#delAll').on('click',function(){
                var keys = $('.grid-view').yiiGridView('getSelectedRows');
                if(keys != '') {
                    if(confirm('Are you sure you want to delete selected rows?') )
                    {
                        $.ajax({
                            url: '".$deleteUrl."', // your controller action
                            dataType: 'json',
                            cache: false,
                            data: {
                            keylist: keys
                            },
                            method: 'POST',
                            beforeSend: function() {
                                //$('.bootbox-close-button').click();
                                //$('body').addClass('loading');
                                App.blockUI({target:'#exception-gridview-div', animate: !0});

                            },
                            complete: function() {
                                //$('body').removeClass('loading').loader('hide');
                            },
                            success: function(data) {
                                
                                //var filtersId = ['name','status','code'];
                                //var url = getUrl(filtersId, 'CountrySearch', '10');                
                                $.pjax.defaults.timeout = false;
                                $.pjax.reload({container: '#exception-gridview'});
                                App.unblockUI('#exception-gridview-div');
                            },
                         });
                    }
                         
                } else {
                    alert('Please select at least one row!');
                }
        });
    });
    ");
?>
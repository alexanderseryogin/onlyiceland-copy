<?php

use yii\db\Migration;

class m170830_114017_alter_table_booking_date_finance_add_field_added_from_group extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_date_finances`   
                          ADD COLUMN `added_from_group` INT(11) NULL AFTER `added_by`;
                        ');
    }

    public function down()
    {
        echo "m170830_114017_alter_table_booking_date_finance_add_field_added_from_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

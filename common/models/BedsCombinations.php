<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "beds_combinations".
 *
 * @property string $id
 * @property string $combination
 * @property string $icon
 */
class BedsCombinations extends \yii\db\ActiveRecord
{
    public $temp_icon='';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'beds_combinations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['icon'], 'string'],
            [['combination'], 'string', 'max' => 256],
            [['combination'],'required'],
            [['short_code'], 'string', 'max' => 5],

            // checks if "primaryImage" is a valid image with proper size
            ['temp_icon', 'image','skipOnEmpty' => true, 'extensions' => 'png, jpg',
                'maxWidth' => 50,
                'maxHeight' => 50,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'combination' => Yii::t('app', 'Combination'),
            'icon' => Yii::t('app', 'Icon'),
            'temp_icon' => Yii::t('app', 'Upload an Icon'),
            'short_code' => Yii::t('app', 'Short Code'),
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_open_hours_exceptions".
 *
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property string $opening
 * @property string $closing
 * @property integer $state
 * @property string $opening_second
 * @property string $closing_second
 * @property integer $split_service
 * @property integer $provider_id
 *
 * @property Providers $provider
 */
class DestinationsOpenHoursExceptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_open_hours_exceptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'opening', 'closing',], 'safe'],
            [['state', 'hours_24', 'provider_id'], 'integer'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'opening' => Yii::t('app', 'Opening'),
            'closing' => Yii::t('app', 'Closing'),
            'state' => Yii::t('app', 'State'),
            'hours_24' => Yii::t('app', 'Hours 24'),
            'provider_id' => Yii::t('app', 'Provider ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    public static function getClockHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-3">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursExceptions[exceptions][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursExceptions[exceptions][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field-exception" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }
}

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\InitialResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>
                <span class="login-info"> Please choose your new password: </span>
                <div class="alert alert-info">
                    Password must contain one or more uppercase, lowercase and special characters, and at least one number, and must meet minimum strength requirements.
                </div>
                
                <!-- BEGIN FORGOT PASSWORD FORM -->
                <?php $form = ActiveForm::begin([
                    'id' => 'reset-password-form',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'tag'=>'span'
                        ]
                    ]
                ]); ?>
                    <br>
                    <?= $form->field($model, 'password')->passwordInput([
                                //'required' => true,
                                'autofocus' => true,
                                'autocomplete' => "off",
                                'placeholder' => 'New Password',
                                'id'=>'password-input',
                                'class' => "form-control form-control-solid placeholder-no-fix form-group"
                            ]) ?>

                    <div class="form-actions">
                        <?= Html::submitButton('Save', ['class' => 'btn blue uppercase', 'id' => 'reset', 'disabled' => true]) ?>
                    </div>
                <?php ActiveForm::end(); ?>
                <!-- END FORGOT PASSWORD FORM -->
<?php $this->registerJs("
var initialized = false;
var input = $('#password-input');

input.keydown(function () {
    if (initialized === false) {
        // set base options
        input.pwstrength({
            raisePower: 1.4,
            minChar: 8,
            verdicts: [\"Weak\", \"Normal\", \"Medium\", \"Strong\", \"Very Strong\"],
            scores: [17, 26, 40, 50, 60],
            common: {
                onKeyUp: function (evt, data) {
                    if(data.score >= 50) {
                        $('#reset').prop('disabled', false);
                    }
                    else{
                        $('#reset').prop('disabled', true);
                    }
                }
            }
        });

        // add your own rule to calculate the password strength
        input.pwstrength(\"addRule\", \"demoRule\", function (options, word, score) {
            return word.match(/[a-z].[0-9]/) && score;
        }, 10, true);

        // set as initialized 
        initialized = true;
    }
});
", \yii\web\View::POS_END);
?>
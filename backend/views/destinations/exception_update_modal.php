
<div class="modal fade" id="update-exception-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4> Update Exception</h4>
            </div>
            <div class="modal-body">
                <!-- <label class="control-label">Exception Date</label>
                <div class="form-group" >
                    <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" id="date_modal" />
                </div> -->

                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">Opening</label>
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="fa fa-clock-o"></i>
                                <input type="text" value="" class="form-control" id="opening_modal"> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">Closing</label>
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="fa fa-clock-o"></i>
                                <input type="text" value="" class="form-control" id="closing_modal"> 
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-sm-4 pull-right">
                        <label class="control-label">Opened</label>
                        <div style="margin-top: 5px;">
                            <input type="checkbox" id="exception_switch_modal" class="make-switch"  data-on-color="success" data-off-color="warning" data-on-text="Yes" data-off-text="No" data-size="mini" >
                        </div>
                    </div> -->
                </div>
                
<!--                 <div class="row">
                    <div class="col-sm-4 pull-right">
                        <label class="control-label">Split Service</label>
                        <div style="margin-top: 5px;">
                            <input type="checkbox" id="split_service_exception_modal" class="make-switch split-service" data-on-color="success" data-off-color="warning" data-on-text="Yes" data-off-text="No" data-size="mini">
                        </div>
                    </div>
                </div> -->

            </div>
            <div class="modal-footer">
                <button type="button" id="update_exception" class="btn blue invite_user_send">Update</button>
                <button type="button" id="no_exception" class="btn default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="delete-exception-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="fa fa-exclamation-triangle"></i> Confirm!</h4>
            </div>
            <div class="modal-body">
                <h3> Are you sure you want to delete this date exception? </h3>
            </div>
            <div class="modal-footer">
                <button type="button" id="yes_exception" class="btn blue invite_user_send">Yes</button>
                <button type="button" class="btn default" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

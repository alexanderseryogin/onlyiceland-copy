<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "add_on".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property integer $discountable
 * @property bool $commissionable
 */
class AddOn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const TYPE_FIXED = 0;
    const TYPE_PERCENTAGE = 1;

    const DISCOUNTABLE = 1;
    const NOT_DISCOUNTABLE = 0;

    public static function tableName()
    {
        return 'add_on';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'discountable', 'commissionable'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 256],
            [['name', 'type', 'discountable','description'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
            'discountable' => 'Discountable',
            'commissionable' => 'Commissionable'
        ];
    }
    public function getType()
    {
        if($this->type == self::TYPE_FIXED)
        {
            return "Fixed Price";
        }
        else
        {
            return "Percentage";
        }
    }

    public function getDiscountable()
    {
        if($this->discountable == self::DISCOUNTABLE)
        {
            return "Yes";
        }
        else
        {
            return "No";
        }
    }
}

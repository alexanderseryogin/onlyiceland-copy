<?php

use yii\db\Migration;

class m170823_114646_alter_table_bookable_item_add_field_default_bed_combination extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookable_items`   
                  ADD COLUMN `default_bed_combinations_id` INT(11) UNSIGNED NULL AFTER `bathroom`,
                  ADD CONSTRAINT `FK_bookable_items_default_bed_combination_id` FOREIGN KEY (`default_bed_combinations_id`) REFERENCES `beds_combinations`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
                ');
    }

    public function down()
    {
        echo "m170823_114646_alter_table_bookable_item_add_field_default_bed_combination cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

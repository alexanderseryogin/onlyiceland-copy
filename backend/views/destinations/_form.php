<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
Use common\models\User;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationTypes;
use common\models\AccommodationTypes;

/* @var $this yii\web\View */
/* @var $provider_model common\models\Providers */
/* @var $form yii\widgets\ActiveForm */
\metronic\assets\BackendUserProfileAsset::register($this);

if(!empty($tab_href))
{
    $this->registerJs("
        $('.destination_tabs li').each(function()
        {
            var href_val = $(this).find('a').attr('href');
            var tab_activate = '$tab_href';
            $('#tab_href').val(tab_activate);

            if(href_val==tab_activate)
            {
                $(this).addClass('active');
                $(this).find('a').attr('aria-expanded','true');
                $(tab_activate).addClass('active');
            }
            else
            {
                $(this).removeClass('active');
                $(this).find('a').attr('aria-expanded','false');
                $(href_val).removeClass('active');
            }
        });
    ");
}

if(empty($provider_model->star_rating))
{
    $provider_model->star_rating = 0;
    $this->registerJs("$('#star_rating_field').val(0);");
}
else
{
    $this->registerJs("$('#star_rating_field').val($provider_model->star_rating);");
}

if(!empty($booking_rules_model->general_booking_cancellation))
{
    $this->registerJs("
        $('#booking_cancellation').val('$booking_rules_model->general_booking_cancellation').change();
    ");
}

?>
<script type="text/javascript">
    var city_id='',state_id='';
    var flagState=0,flagCity=0;
    var fin_city_id='',fin_state_id='';
    var flagFinState=0,flagFinCity=0;
    var updated_state_id ='';
    var updated_city_id = '';
    var updated_flag = 0;
    var fin_updated_state_id ='';
    var fin_updated_city_id = '';
    var fin_updated_flag = 0;
    var option = 1;
    var success='';
    var active_tab='';
</script>

<style>
    .hint-block{
        color:blue;
    }
    .dynamic_tab
    {
        display: none;
    }
    .destination_portlet
    {
        display: none;
    }
    .hide_time
    {
        display: none;
    }
    .show_time
    {
        display: block;
    }
</style>



<?php $form = ActiveForm::begin([
                'errorSummaryCssClass' => 'alert alert-danger',
                'options' => ['enctype' => 'multipart/form-data'],
                'id' => 'providers_form',
            ]); ?>

<input type="text" name="Destinations[tab_href]" id="tab_href" hidden="">

<div class="providers-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
            
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-green-sharp">
                            <i class="icon-tag font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Destination Type</span>
                            <!-- <span class="caption-helper">select a destination type to proceed</span> -->
                        </div>
                        <div class="actions">
                        </div>
                    </div>
                    <div class="portlet-body">
                       <div class="row">

                            <div class="col-xs-4">
                                <?= $form->field($provider_model, 'provider_type_id', [
                                    'inputOptions' => [
                                        'id' => 'provider-type-dropdown',
                                        'class' => 'form-control',
                                        'style' => 'max-width: 500px'
                                        ]
                                ])->dropdownList(ArrayHelper::map(DestinationTypes::find()->orderBy('name ASC')->all(),'id','name'),['prompt'=>'Select a Type']) ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $form->field($provider_model, 'accommodation_id', [
                                    'inputOptions' => [
                                        'id' => 'accommodation-dropdown',
                                        'class' => 'form-control',
                                        'style' => 'width: 325px;'
                                        ]
                                ])->dropdownList(ArrayHelper::map(AccommodationTypes::find()->orderBy('name ASC')->all(),'id','name'),['prompt'=>'Select an Accommodation']) ?>
                             </div>
                             <div class="col-xs-4">
                                <label class="control-label" >Active</label>
                                <div class="form-group" >
                                    <input type="checkbox" id="published_switch" class="make-switch" name="Destinations[active]" data-on-color="success" data-off-color="danger" data-on-text="Yes" data-off-text="No" >
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <!-- END Portlet PORTLET-->

                <?php
                    // echo "<pre>";
                    // print_r($provider_model);

                    $this->registerJs(" 
                            
                            option = $provider_model->active;
                            if(option==0)
                            {
                                $('#published_switch').bootstrapSwitch('state',false);
                            }
                            else
                            {
                                $('#published_switch').bootstrapSwitch('state',true);
                            }
                        ");

                    if(!empty($provider_model->provider_type_id))
                    {
                        $this->registerJs("
                            $('.destination_portlet').show();
                            $('.dynamic_tab').hide();
                        ");

                        if($provider_model->provider_type_id == $provider_model->getAccomodation())
                        {
                            $this->registerJs("
                                $('.dynamic_tab').show();
                                $('#accommodation-dropdown').show();
                            ");
                        }
                        else
                        {
                            $this->registerJs("
                                $('.field-accommodation-dropdown').hide();
                            ");
                        }
                    }
                    else
                    {
                        $this->registerJs("
                                $('.field-accommodation-dropdown').hide();
                            ");
                    }
                ?>

                <div class="portlet light destination_portlet">
                    <div class="portlet-title tabbable-line ">
                        <div class="caption font-green-sharp">
                            <i class="icon-layers font-green-sharp"></i>
                            <span class="caption-subject bold uppercase">Destination Info</span>
                            <!-- <span class="caption-helper">select a destination type to proceed</span> -->
                        </div>
                        <ul class="nav nav-tabs destination_tabs">
                            <li class="active" name='tabs'>
                                <a href="#details" data-toggle="tab">Details</a>
                            </li>
                            <li name='tabs' class="dynamic_tab">
                                <a href="#amenities" data-toggle="tab">Amenities</a>
                            </li>
                            <li name='tabs' class="dynamic_tab">
                                <a href="#policies" data-toggle="tab">Booking Policies</a>
                            </li>
                            <li name='tabs' class="dynamic_tab">
                                <a href="#rules" data-toggle="tab">Booking Rules</a>
                            </li>
                            <li name='tabs' class="dynamic_tab">
                                <a href="#financial" data-toggle="tab">Financial/Legal</a>
                            </li>
                            <li name='tabs' class="dynamic_tab">
                                <a href="#meal_plan" data-toggle="tab">Meal Plan</a>
                            </li>
                            <li name="tabs">
                                <a href="#open_dates" data-toggle="tab">Open Dates/Hours</a>
                            </li>
                            <!--<li name="tabs">
                                <a href="#open_hours" data-toggle="tab">Open Hours</a>
                            </li>-->
                            <li name='tabs' class="dynamic_tab">
                                <a href="#travel_partners" data-toggle="tab">Travel Partners</a>
                            </li>
                            <li name='tabs' class="dynamic_tab">
                                <a href="#add_ons" data-toggle="tab">Add Ons</a>
                            </li>
                            <li id="photo-tab">
                                <a href="#photos" data-toggle="tab">Photos</a>
                            </li>
                        </ul>
                    </div>
                            
                    <?= $form->errorSummary($provider_model); ?>

                    <div class="portlet-body destination_portlet">
                        <div class="tab-content">
                            
                            <?= $this->render('provider_details',
                                    [
                                        'form' => $form, 
                                        'provider_model' => $provider_model
                                    ])?>

                            <?= $this->render('travel_partners',
                                    [
                                        'form' => $form,
                                        'provider_model' => $provider_model,
                                        'destination_travel_partners' => $destination_travel_partners
                                    ])?>
                            <?= $this->render('add_ons',
                                    [
                                        'form' => $form,
                                        'provider_model' => $provider_model,
                                        'destination_add_ons' => $destination_add_ons
                                    ])?>

                            <?= $this->render('destination_amenities',
                                    [
                                        'form' => $form,
                                        'provider_model' => $provider_model,
                                        'amenities' => $amenities
                                    ])?>

                            <?= $this->render('booking_policies',
                                    [
                                        'form' => $form, 
                                        'booking_policies_model' => $booking_policies_model,
                                        'provider_model' => $provider_model,
                                    ])?>

                            <?= $this->render('booking_rules',
                                    [
                                        'form' => $form, 
                                        'booking_rules_model' => $booking_rules_model,
                                    ])?>

                            <?= $this->render('provider_financial',
                                    [
                                        'form' => $form, 
                                        'provider_financial_model' => $provider_financial_model,
                                        'provider_model' => $provider_model,
                                    ])?>

                            <?= $this->render('meal_plan',
                                    [
                                        'form' => $form,
                                        'meal_options' => $meal_options,
                                    ])?>

                            <?php
                                $searchModelException = isset($searchModelException) ? $searchModelException :'';
                                $dataProviderException = isset($dataProviderException) ? $dataProviderException :'';
                            ?>

                            <?php /* $this->render('open_hours',
                                    [
                                        'form' => $form,
                                        'provider_model' => $provider_model,
                                        'open_hours' => $open_hours,
                                    ])*/ ?>

                            <?= $this->render('open_dates',
                                    [
                                        'form' => $form,
                                        'provider_model' => $provider_model,
                                        'open_hours' => $open_hours,
                                        'destination_open_all_year' => $destination_open_all_year,
                                        'searchModelException' => $searchModelException,
                                        'dataProviderException' => $dataProviderException,
                                    ])?>

                            <?php ActiveForm::end(); ?>

                            <div class="tab-pane" id="photos">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-info">
                                            <strong>Info!</strong>
                                            Recommended Ratio (4:3)
                                        </div>
                                    </div>
                                </div>

                                <form action="<?= Url::to(['/destinations/upload-images']) ?>" class="dropzone dropzone-file-area" method="POST" enctype="multipart/form-data" id="my-dropzone" style="width: 900px; margin-top: 10px;">
                                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                                        <h3 class="sbold">Drop files here or click to upload</h3>
                                </form>
                                <br>
                            </div>
                            <input id="save" type="text" name="check" value="0" hidden="true">
                            <div class="form-group">
                                <a class="<?= $provider_model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?>" id="submit_button" ><?=$provider_model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update') ?></a>
                                <?php if(!$provider_model->isNewRecord){?>
                                    <a class="<?= $provider_model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?>" id="save_button" >Save</a>

                                <?php } ?>
                                <a class="btn btn-default" href="<?= Url::to(['/destinations']) ?>" >Cancel</a>
                            </div>
                            
                            <?php 
                            if(isset($dataProvider) && !empty($dataProvider))
                            {?>
                                <?= $this->render('photo_gridview',
                                    [
                                        'dataProvider' => $dataProvider,
                                    ])?>   
                            <?php 
                            }
                            ?>

                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="fa fa-exclamation-triangle"></i> Warning!</h4>
            </div>
            <div class="modal-body">
                <h3> Are you sure to delete image? </h3>
            </div>
            <div class="modal-footer">
                <button type="button" id="yes" class="btn blue invite_user_send">Yes</button>
                <button type="button" id="no" class="btn default" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="update-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4> Update Image Description</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="6" style="resize: none;" id="image-description"></textarea>
                <form method="POST" action="" enctype="multipart/form-data">
                    <input type="file" id="image" name="file">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="update" class="btn blue invite_user_send">Updauodatte</button>
                <button type="button" id="no1" class="btn default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="max-width: 570px; max-height: 600px;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
$image_url = Url::to(['/destinations/update-image']);
$this->registerJs("
jQuery(document).ready(function() {
    var image_url = '$image_url';

    Dropzone.options.myAwesomeDropzone = {
      paramName: 'file', // The name that will be used to transfer the file
      maxFilesize: 10, // MB
    };

    var title=$('#destination_title').val();

    $('#provider-type-dropdown').on('select2:unselecting ', function (e) 
    {
        $('.destination_portlet').hide();
        $('.field-accommodation-dropdown').hide();
    });

    $('#accommodation-dropdown').on('select2:select', function (evt) 
    {
        if($('#select2-accommodation-dropdown-container').attr('title')=='Hotel')
        {
            $('div').find('.hotel_hide_field').each(function()
            {
                $(this).addClass('hotel_show_field');
                $(this).removeClass('hotel_hide_field');
            });
        }
        else
        {
            $('div').find('.hotel_show_field').each(function()
            {
                $(this).addClass('hotel_hide_field');
                $(this).removeClass('hotel_show_field');
            });
        }
    });

    $('#accommodation-dropdown').on('select2:unselecting ', function (e) 
    {
        $('div').find('.hotel_show_field').each(function()
        {
            $(this).addClass('hotel_hide_field');
            $(this).removeClass('hotel_show_field');
        });
    });

    $('#provider-type-dropdown').on('select2:select', function (evt) 
    { 
          /*$('#details').addClass('active');
          $('#details').find('a').attr('aria-expanded','true');
          $('#details').addClass('active');*/

          $('.destination_portlet').show();

          if($('#select2-provider-type-dropdown-container').attr('title')=='Accommodation')
          {
            $('.resturant_dropdown_div').addClass('hide');
            $('.offers_accommodation_field').hide();
            $('#offers_accommodation').bootstrapSwitch('state',true);

            $('.field-accommodation-dropdown').show();
            $('.dynamic_tab').show();

            showFields();

            if (typeof redrawAmenitiesDataTable == 'function') {
                redrawAmenitiesDataTable(); 
            }

          }  
          else
          {
            $('.offers_accommodation_field').show();
            $('#offers_accommodation').bootstrapSwitch('state',false);

            if($('#select2-provider-type-dropdown-container').attr('title')=='Event')
            {
                $('div').find('.event_hide_field').each(function()
                {
                    $(this).addClass('event_show_field');
                    $(this).removeClass('event_hide_field');
                });
            }
            else
            {
                $('div').find('.event_show_field').each(function()
                {
                    $(this).addClass('event_hide_field');
                    $(this).removeClass('event_show_field');
                });
            }

            if($('#select2-provider-type-dropdown-container').attr('title')=='Restaurant')
            {
                $('.resturant_dropdown_div').removeClass('hide');
            }
            else
            {
                $('.resturant_dropdown_div').addClass('hide');
            }

            $('.dynamic_tab').hide();
            $('.field-accommodation-dropdown').hide();

            hideFields();        
          }

          title = $('#select2-provider-type-dropdown-container').attr('title');

    });

    $('#offers_accommodation').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            showFields();
        }
        else
        {
            hideFields();
        }
    });

    function showFields()
    {
        $('div').find('.hide_field').each(function()
        {
            $(this).addClass('show_field');
            $(this).removeClass('hide_field');
        });
    }

    function hideFields()
    {
        $('div').find('.show_field').each(function()
        {
            $(this).addClass('hide_field');
            $(this).removeClass('show_field');
        }); 
    }

    $('a[name=\"pop[]\"]').on('click', function() 
    {
       $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
       $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
    });

    $('a[name=\"up[]\"]').click(function()
    {
        row_id = $(this).attr('data-id');

        var Object = {
                        row_id : row_id,
                        action : 'up',
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'arrange-data',
            data: Object,
            success: function(result)
            {
                 $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"down[]\"]').click(function()
    {
        row_id = $(this).attr('data-id');

        var Object = {
                        row_id : row_id,
                        action : 'down',
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'arrange-data',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    var image_id;

    $('#images-gridview').on('pjax:end', function() {

        $('a[name=\"pop[]\"]').on('click', function() 
        {
           $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
           $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        });
    
        $('a[name=\"up[]\"]').click(function()
        {
            row_id = $(this).attr('data-id');

            var Object = {
                            row_id : row_id,
                            action : 'up',
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'arrange-data',
                data: Object,
                success: function(result)
                {
                     $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        $('a[name=\"down[]\"]').click(function()
        {
            row_id = $(this).attr('data-id');

            var Object = {
                            row_id : row_id,
                            action : 'down',
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'arrange-data',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        $('a[name=\"update[]\"]').click(function(e) 
        {
            $('#update-modal').modal('show');
            $('#image').val('');

            image_id = $(this).attr('data-id');

            var Object = {
                            image_id : image_id,
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'get-description',
                data: Object,
                success: function(result)
                {
                    $('#image-description').val(result); 
                },
            });
        });

        $('a[name=\"delete[]\"]').click(function(e) 
        {
            image_id = $(this).attr('data-id');

            $('#delete-modal').modal('show');
        });
    });

    $('#images-gridview').hide();
    
    $('#photo-tab').click(function()
    {
        $('#images-gridview').show();
    });
    
    $('.destination_tabs li').click(function()
    {
        var href = $(this).find('a:first').attr('href');
        $('#tab_href').val(href);

        if(href == '#amenities')
        {
            setTimeout(function(){ 
                if (typeof redrawAmenitiesDataTable == 'function') {
                    redrawAmenitiesDataTable(); 
                }

            }, 500);
        }
        if(href == '#travel_partners')
        {
            setTimeout(function(){ 
                if (typeof redrawDataTableTravelPartner == 'function') {
                    redrawDataTableTravelPartner(); 
                }

            }, 500);
        }
        if(href == '#add_ons')
        {
            setTimeout(function(){ 
                if (typeof redrawDataTableAddOn == 'function') {
                    redrawDataTableAddOn(); 
                }

            }, 500);
        }           
    });

    $('li[name=\"tabs\"]').click(function()
    {
        $('#images-gridview').hide();
    });

    $('a[name=\"update[]\"]').click(function(e) 
    {
        $('#update-modal').modal('show');
        $('#image').val('');

        image_id = $(this).attr('data-id');

        var Object = {
                        image_id : image_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'get-description',
            data: Object,
            success: function(result)
            {
                $('#image-description').val(result); 
            },
        });
    });

    $('#image').AjaxFileUpload(
    {
        action: image_url,
    });

    $('#update').click(function(e) 
    {
        $('#update-modal').modal('hide');

        var Object = {
                        image_id : image_id,
                        description: $('#image-description').val(),
                    }

        $.ajax(
        {
            type: 'POST',
            url: 'set-description',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"delete[]\"]').click(function(e) 
    {
        image_id = $(this).attr('data-id');

        $('#delete-modal').modal('show');
    });

    $('#yes').click(function()
    {
        $('#delete-modal').modal('hide');

        var Object = {
                        image_id : image_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'delete-image',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('#telephone_number').intlTelInput({
      initialCountry: 'auto',
      preferredCountries : ['is'],
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    $('#fax').intlTelInput({
      initialCountry: 'auto',
      preferredCountries : ['is'],
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    $('#owner-dropdown').change(function()
    {
        var provider_id = {
                                    id : $(this).val(),
                          }

        $.ajax(
        {
            type: 'POST',
            url: 'get-destination-record',
            data: provider_id,
            success: function(result)
            {
                result = JSON.parse(result);

                $('#telephone_number').intlTelInput('setNumber',result['phone']);
                $('#kennitala-input').val(result['kennitala']);
                $('#vat_id-input').val(result['vat_id']);
                $('#destinations-booking_email').val(result['email']);
                $('#destinations-business_name').val(result['business_name']);
                $('#destinations-address').val(result['address']);
                $('#countries-dropdown').val(result['country_id']).trigger('change');
                
                flagState=1;
                flagCity=1;
                state_id = result['state_id'];
                city_id = result['city_id'];

                $('#destinations-postal_code').val(result['postal_code']);
            },
            error: function()
            {
                $('#telephone_number').intlTelInput('setNumber','');
                $('#kennitala-input').val('');
                $('#vat_id-input').val('');
                $('#destinations-booking_email').val('');
                $('#destinations-business_name').val('');
                $('#destinations-address').val('');
                $('#countries-dropdown').val(100).trigger('change');
                $('#destinations-postal_code').val('');
            }
        });
    });

    $('#telephone_number').change(function()
    {
      var telInput = $('#telephone_number');
      if($.trim(telInput.val())) 
      {
        if (telInput.intlTelInput('isValidNumber')) 
        {
            $('#phone_hint').hide();
        }
        else 
        {
          $('#phone_hint').show();
        }
      }
    });

    /*$('#fax').change(function()
    {
      var telInput = $('#fax');
      if ($.trim(telInput.val())) 
      {
        if (telInput.intlTelInput('isValidNumber')) 
        {
            $('#fax_hint').hide();
        }
        else 
        {
          $('#fax_hint').show();
        }
      }
    });*/

    $('#submit_button').click(function()
    {
        // *************** set title of provider type

        if(title.indexOf(' ') >= 0)
        {
            str = title.split(' ');
            title = str[0];
        }

        $('#destination_title').val(title);

        // *************** set phone and fax value

        var countryData = $('#telephone_number').intlTelInput('getSelectedCountryData');

        var tel = '+'+countryData['dialCode']+' '+$('#telephone_number').val();
        $('#server_phone').val(tel);

        if($('#fax').val()!='')
        {
            countryData = $('#fax').intlTelInput('getSelectedCountryData');

            var fax = '+'+countryData['dialCode']+' '+$('#fax').val();
            $('#server_fax').val(fax);
        }

        // *************** validate phone and fax

        if($('#telephone_number').intlTelInput('isValidNumber')==false)
        {
            $('#phone_hint').show();
            $('#telephone_number').focus();
            window.location.href='#owner-dropdown';
        }

        // *************** validate phone and fax before submit

        if($('#telephone_number').intlTelInput('isValidNumber')) //&& $('#fax').intlTelInput('isValidNumber'))
        {
            $('#providers_form').submit();
        }
    });

    // submit form on save button //
    $('#save_button').click(function()
    {
        // *************** set title of provider type

        if(title.indexOf(' ') >= 0)
        {
            str = title.split(' ');
            title = str[0];
        }

        $('#destination_title').val(title);

        // *************** set phone and fax value

        var countryData = $('#telephone_number').intlTelInput('getSelectedCountryData');

        var tel = '+'+countryData['dialCode']+' '+$('#telephone_number').val();
        $('#server_phone').val(tel);

        if($('#fax').val()!='')
        {
            countryData = $('#fax').intlTelInput('getSelectedCountryData');

            var fax = '+'+countryData['dialCode']+' '+$('#fax').val();
            $('#server_fax').val(fax);
        }

        // *************** validate phone and fax

        if($('#telephone_number').intlTelInput('isValidNumber')==false)
        {
            $('#phone_hint').show();
            $('#telephone_number').focus();
            window.location.href='#owner-dropdown';
        }

        // *************** validate phone and fax before submit

        if($('#telephone_number').intlTelInput('isValidNumber')) //&& $('#fax').intlTelInput('isValidNumber'))
        {
            $('#save').val('1');
            $('#providers_form').submit();
        }
    });

    $('#kennitala-input').inputmask('999999-9999',{showMaskOnFocus: true,showMaskOnHover: false});
    $('#bank_info-input').inputmask('9999-99-999999',{showMaskOnFocus: true,showMaskOnHover: false});
    $('#vat_rate-input').inputmask('99,99',{showMaskOnFocus: true,rightAlign: true,showMaskOnHover: false});
    $('#vat_id-input');

    $('#destinations-same_address').change(function() {
        if(this.checked) {
            $('#destinations-business_address').hide();
        } else {
            $('#destinations-business_address').show();
        }
    });

    $('#youngest-age').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 99,
    });

    $('#oldest-age').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 99,
    });

    $('#near-term-bookings').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 60,
    });

    $('#general-booking-day').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 30,
    });

    $('#same-day-bookings').ionRangeSlider({
        grid:!0,
        values:['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
    });
    
    $('#region-dropdown').select2({
        placeholder: \"Select a Region\",
        allowClear: true
    });

    $('#owner-dropdown').select2({
        placeholder: \"Select an Owner\",
        allowClear: true
    });

    $('#accommodation-dropdown').select2({
        placeholder: \"Select an Accommodation\",
        allowClear: true
    });

    $('#provider_admin_dropdown').select2({
        placeholder: \"Select an Admin\",
        allowClear: true
    });

    $('#staff-dropdown').select2({
        //placeholder: \"Select Staff\",
        allowClear: true
    });

    $('#upsell_items-dropdown').select2({
        placeholder: \"Select Items\",
        allowClear: true
    });
    $('#resturant_tags-dropdown').select2({
        placeholder: \"Select Items\",
        allowClear: true
    });


    $('#tag-dropdown').select2({
        placeholder: \"Select Tag\",
        allowClear: true
    });

    $('#suitable-dropdown').select2({
        placeholder: \"Select Tag\",
        allowClear: true
    });

    $('#difficulty-dropdown').select2({
        placeholder: \"Select Tag\",
        allowClear: true
    });

    $('#amenities-dropdown').select2({
        placeholder: \"Select Amenities\",
        allowClear: true
    });

    $('#booking-type-dropdown').select2({
        placeholder: \"Select a Flag\",
        allowClear: true
    });

    $('#new-booking-type-dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#booking-status-dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#new-booking-status-dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#routine-booking-dropdown').select2({
        placeholder: \"Select a Confirmation Type\",
        allowClear: true
    });

    $('#nearterm-booking-dropdown').select2({
        placeholder: \"Select a Confirmation Type\",
        allowClear: true
    });

    $('#ebp-booking-type-dropdown').select2({
        placeholder: \"Select a Confirmation Type\",
        allowClear: true
    });

    $('#provider-type-dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#provider-category-dropdown').select2({
        placeholder: \"Select a Category\",
        allowClear: true
    });

    $('#provider-sub-category-dropdown').select2({
        placeholder: \"Select a Sub-Category\",
        allowClear: true
    });

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });

    $('#business-cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#business-states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#business-countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });

    $('#star_rating-dropdown').select2({
        placeholder: \"Select Star Rating\",
        allowClear: true
    });

    $('#financial-cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#financial-states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#financial-countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true,
    });
    
    $('#booking_cancellation').select2({
        placeholder: \"Select a Guest Policy\",
        allowClear: true,
    });

    $('#background_color').colorpicker().on('changeColor', function (e) {
                  $('#preview_box')[0].style.backgroundColor = e.color.toHex();
              });

    $('#text_color').colorpicker().on('changeColor', function (e) {
                  $('#preview_box')[0].style.color = e.color.toHex();
              });

    $('#destinations-transparent_background').click(function()
    {
        if(this.checked)
        {
            $('.background-color').hide();
            $('#preview_box')[0].style.backgroundColor = 'transparent';
        }
        else
        {
            $('.background-color').show();
            $('#preview_box')[0].style.backgroundColor = '#ff0000';
        }
    });

    $('#star_rating').rateYo({
        rating : $provider_model->star_rating,
        fullStar: true,
        ratedFill: '#2AB4C0',
        onChange: function (rating, rateYoInstance) {
            $('#star_rating_field').val(rating);
        }
    });

    $('#time_group_dropdown').select2({
        placeholder: \"Select a Time Group\",
    });

    $('#lodging_tax_per_night').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('#lodging_tax_per_night').on('change',function()
    {
        $(this).parent().siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

    $('#vat_rate-input').on('change',function()
    {
        $(this).parent().siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

});",View::POS_END);
?>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

use common\models\Destinations;
use common\models\DestinationTypes;
use common\models\States;
use common\models\DiscountCode;


/* @var $this yii\web\View */
/* @var $model common\models\DiscountCode */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .hint-block{
        color:blue;
    }
</style>

<div class="discount-code-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-xs-6">
                                <?= $form->field($model, 'provider_id', [
                                    'inputOptions' => [
                                            'id' => 'provider_dropdown',
                                            'class' => 'form-control',
                                            'style' => 'max-width: 500px'
                                            ]
                                ])->dropdownList(ArrayHelper::map(Destinations::find()->all(),'id','name'),['prompt'=>'Select a Destination']) ?>
                            </div>
                            <div class="col-xs-6">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-4">
                                <?= $form->field($model, 'pricing_type', [
                                        'inputOptions' => [
                                                        'id' => 'pricing_type_dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                ])->dropdownList(DiscountCode::$pricing_type,['prompt'=>'Select a Pricing Type']) ?> 
                            </div>
                            <div class="col-xs-4">
                                <?= $form->field($model, 'applies_to', [
                                        'inputOptions' => [
                                                        'id' => 'applies_to_dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                ])->dropdownList(DiscountCode::$applies_to,['prompt'=>'Select an option']) ?> 
                            </div>
                            <div class="col-xs-4">
                                <?= $form->field($model, 'quantity_type', [
                                        'inputOptions' => [
                                                        'id' => 'quantity_type_dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                ])->dropdownList(DiscountCode::$quantity_type,['prompt'=>'Select a Quantity Type']) ?> 
                            </div>
                        </div>

                         <?php
                                if(!$model->isNewRecord)
                                {
                                    $this->registerJs("
                                        
                                        // ********** handling enterable_by_registered_users field

                                        option = '$model->enterable_by_registered_users';
                                        if(option=='')
                                        {
                                            $('#no_enterable').attr('checked','checked');
                                        }
                                        else
                                        {
                                            $('#'+option+'_enterable').attr('checked','checked');
                                        }

                                        // ********** handling active field

                                        option = '$model->active';
                                        if(option=='')
                                        {
                                            $('#no_active').attr('checked','checked');
                                        }
                                        else
                                        {
                                            $('#'+option+'_active').attr('checked','checked');
                                        }

                                        // ********** handling linked_to_quantity field

                                        option = '$model->linked_to_quantity';
                                        $('#'+option+'_linked').attr('checked','checked');
                                    ");
                                }
                                else
                                {
                                    $this->registerJs("
                                            $('#no_enterable').attr('checked','checked');
                                            $('#no_active').attr('checked','checked');
                                            $('#yes_linked').attr('checked','checked');
                                        ");
                                }
                        ?>

                        <div class="row">

                            <div class="col-sm-3">
                                <?= $form->field($model, 'amount', ['inputOptions' => [
                                    'id' => 'amount',
                                    'class' => 'form-control'
                                    ]
                                ])->textInput(['maxlength' => true])->hint(' e.g 999.999') ?>

                                <input type="text" value="<?=$model->server_amount?>" name="DiscountCode[server_amount]" hidden> 

                            </div>
                            <div class="col-sm-3">
                                <label class=" control-label">Enterable by Registered Users</label>
                                <div class="mt-radio-inline">
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="yes_enterable" name="DiscountCode[enterable_by_registered_users]" value="yes" > Yes
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="no_enterable" name="DiscountCode[enterable_by_registered_users]" value="no" > No
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class=" control-label">Active</label>
                                <div class="mt-radio-inline">
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="yes_active" name="DiscountCode[active]" value="yes" > Yes
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="no_active" name="DiscountCode[active]" value="no" > No
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class=" control-label">Linked to Quantity</label>
                                <div class="mt-radio-inline">
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="yes_linked" name="DiscountCode[linked_to_quantity]" value="yes" > Yes
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="no_linked" name="DiscountCode[linked_to_quantity]" value="no" > No
                                        <span></span>
                                    </label>
                                </div>
                            </div>

                        </div>
                
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => 'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <a class="btn btn-default" href="<?= Url::to(['/discount-code']) ?>" >Cancel</a>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {
    
    $('#amount').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('#pricing_type_dropdown').select2({
        placeholder: \"Select a Pricing Type\",
        allowClear: true
    });

    $('#quantity_type_dropdown').select2({
        placeholder: \"Select a Quantity Type\",
        allowClear: true
    });

    $('#applies_to_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#amount').on('change',function()
    {
        $(this).parent('div').siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

});",View::POS_END);
?>


    

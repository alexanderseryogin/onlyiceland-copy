<?php
use common\models\CustomReportColumns;
use common\models\BookingDateFinances;
$this->title = 'Execute Report';
$this->params['breadcrumbs'][] = ['label' => 'Custom Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Execute';
$columns = CustomReportColumns::getColumns();
$alias = CustomReportColumns::getAlias();
?>
<form id="pdf-form" action="<?=yii::$app->getUrlManager()->createUrl(['custom-reports/print'])?>" method="POST">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <input type="text" name="query" value="<?=$org_query?>" hidden>
    <input type="text" name="title" value="<?=$model->report_title?>" hidden>
    <input type="text" name="columns" value='<?=serialize($selected_columns)?>' hidden>
</form>
<div class="custom-report" id="custom-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$model->report_title?></span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
                            <div class="btn-group pull-left" style="margin: 5px">
                                <div class="btn-group">
                                <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                                    
                                </span></button>

                                    <ul id="w3" class="dropdown-menu">
                                        <li title="Comma Separated Values"><a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                        <li title="Microsoft Excel 2007+ (xlsx)"><a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                        <li>
                                            <a href="javascript:void(0);" target="_blank" class="print-pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            PDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <br>
	                    	<div class="tab-pane active" id="arrival-report">
                                  <h5 class="hide"><b><?=$query?></b></h5>
                                  <br><br><br>

                                  <?php
                                    // echo "<pre>";
                                    // print_r($result);
                                    // exit();
                                  ?>

	                    	      <table class="table table-striped table-bordered table2excel">
                                    <thead> 
                                      <tr>
                                        <?php
                                            foreach ($model->customReportColumns as $column) 
                                            {
                                                if($column->column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column->column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                        ?>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {
                                                $notes_exist = false;
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                    $notes_exist = true;
                                                }

                                                foreach ($result as $data) 
                                                {
                                                    echo "<tr>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';
                                                    foreach ($model->customReportColumns as $column) 
                                                    {
                                                        if($column->column != 9)
                                                        {

                                                            $style = $columns[$column->column]['style'];
                                                            echo "<td style='".$style."'>";

                                                            if($column->column != 13 && $column->column != 14 && $column->column != 15 && $column->column != 17 && $column->column != 18)
                                                            {
                                                                if($columns[$column->column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column->column]['is_amount'] == 0)
                                                                    {
                                                                        if(isset($columns[$column->column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];
                                                                        }
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column->column]['virtual_column']))
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['virtual_column']]);

                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column->column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column->column,$data[$columns[$column->column]['db_col']]);
                                                                        echo $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";
                                                        }
                                                        else
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            $notes = empty($data[$columns[$column->column]['db_col']])?'':$data[$columns[$column->column]['db_col']];
                                                            // $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    echo "</tr>";
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        // checking if charge item desc column exists //
                                                        // $chrg_itm_desc_exists = array_search(14, $post['CustomReports']['columns']);
                                                        // $pymnt_item_desc_exists = array_search(18, $post['CustomReports']['columns']);
                                                        $chrg_itm_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15] ]);
                                                        $pymnt_item_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [17,18] ]);

                                                        if(!empty($chrg_itm_desc_exists ))
                                                        {
                                                            // echo "<pre>";
                                                            // print_r($booking_item_charges);
                                                            // exit;
                                                            foreach ($booking_item_charges as $booking_item_charge) 
                                                            {
                                                                echo "<tr>";
                                                                $col_count = 0;

                                                                foreach ($model->customReportColumns as $column)
                                                                {
                                                                    $col_count++;
                                                                    if(array_key_exists($columns[$column->column]['db_col'], $booking_item_charge))
                                                                    {
                                                                        $value = $booking_item_charge[$columns[$column->column]['db_col']];
                                                                        if($column->column == 1 || $column->column == 18 || $column->column == 19 || $column->column == 17) 
                                                                        {
                                                                            echo "<td>";
                                                                            
                                                                            echo "</td>";
                                                                        }
                                                                        else
                                                                        {
                                                                           if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "<td>";
                                                                            
                                                                        echo "</td>";

                                                                    }
                                                                    
                                                                    
                                                                }
   
                                                                echo "</tr>";
                                                                // echo "<tr>";
                                                                // foreach ($post['CustomReports']['columns'] as $column)
                                                                // {
                                                                //     echo "<td>";
                                                                //     if($data[$columns[$column]['db_col'])
                                                                //     {

                                                                //     }
                                                                //     else
                                                                //     {

                                                                //     }
                                                                //     echo "</td>";
                                                                // } 
                                                                // echo "</tr>";
                                                            }
                                                            // $colspan = count($post['CustomReports']['columns']);
                                                        }

                                                        if(!empty($pymnt_item_desc_exists))
                                                        {
                                                            // echo "<pre>";
                                                            // print_r($booking_item_charges);
                                                            // exit;
                                                            foreach ($booking_item_payments as $booking_item_payment) 
                                                            {
                                                                echo "<tr>";
                                                                $col_count = 0;

                                                                foreach ($model->customReportColumns as $column)
                                                                {
                                                                    $col_count++;
                                                                    if(array_key_exists($columns[$column->column]['db_col'], $booking_item_payment))
                                                                    {
                                                                        $value = $booking_item_payment[$columns[$column->column]['db_col']];
                                                                        if($column->column == 1 || $column->column == 15 || $column->column == 16 || $column->column == 14 || $column->column == 13)
                                                                        {
                                                                            echo "<td>";
                                                                            
                                                                            echo "</td>";
                                                                        }
                                                                        else
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                                
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        echo "<td>";
                                                                        echo "</td>";
                                                                    }
                                                                }
   
                                                                echo "</tr>";
                                                            }
                                                            // $colspan = count($post['CustomReports']['columns']);
                                                        }
                                                        // exit();
                                                        
                                                    }
                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
	                    		
						        <p style="font-size: 12px">Total Records : <?=count($result)?></p>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
    $this->registerJs("
            $('.print-pdf').click(function(){
                // alert('clicked');
                $('#pdf-form').attr('target', '_blank');
                $('#pdf-form').submit();
            });

             $('#excel-btn').click(function(){
              $('.table2excel').table2excel({
                    exclude: '.noExl',
                    name: '".$model->report_title."',
                    filename: '".$model->report_title."',
                    fileext: '.xls',
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });
            $('#csv-btn').click(function(){
                console.log('clicked');
                
                  $('.table2excel').tableToCSV({
                    name: '".$model->report_title."',
                    filename: '".$model->report_title."',
                  });
            }); 

        ");
?>
<?php

use yii\db\Migration;

class m161021_111228_create_table_providers_images extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_images`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `image` VARCHAR(256), PRIMARY KEY (`id`), CONSTRAINT `FK_providers_images` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161021_111228_create_table_providers_images cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

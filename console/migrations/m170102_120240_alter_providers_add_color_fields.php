<?php

use yii\db\Migration;

class m170102_120240_alter_providers_add_color_fields extends Migration
{
    public function up()
    {
        $this->execute("
                ALTER TABLE `providers` ADD COLUMN `background_color` VARCHAR(11) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `url`, ADD COLUMN `text_color` VARCHAR(11) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `background_color`; 
        ");
    }

    public function down()
    {
        echo "m170102_120240_alter_providers_add_color_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

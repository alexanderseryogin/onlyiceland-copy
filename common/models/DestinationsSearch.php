<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Destinations;
use common\models\DestinationsImages;
use common\models\DestinationsOpenHoursExceptions;
use common\models\BookingPolicies;
/**
 * ProvidersSearch represents the model behind the search form about `common\models\Providers`.
 */
class DestinationsSearch extends Destinations
{
    public $youngest_age;
    public $oldest_free_age;
    public $license_type;
    public $license_expiration;
    public $checkin_time_from;
    public $checkin_time_to;
    public $checkout_time_from;
    public $checkout_time_to;
    public $city_name;
    public $image;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_category_id', 'provider_type_id', 'provider_admin_id','provider_admin', 'country_id', 'city_id', 'state_id', 'business_country_id', 'business_city_id', 'business_state_id','accommodation_id'], 'integer'],
            [['name', 'phone', 'fax', 'booking_email', 'website', 'kennitala', 'address', 'postal_code', 'business_name', 'business_address', 'business_postal_code','youngest_age','oldest_free_age','license_type','license_expiration','checkin_time_from','checkin_time_to','checkout_time_from','checkout_time_to','city_name','state_id','accommodation_id'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Destinations::find()->joinWith(['bookingPolicies','destinationsFinancials','city','state']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            
            'pagination' => [
                'page' => isset($params['page'])?($params['page']-1):0,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],
                ],
                'provider_type_id' => [
                    'asc' => ['provider_type_id' => SORT_ASC],
                    'desc' => ['provider_type_id' => SORT_DESC],
                ],
                'accommodation_id' => [
                    'asc' => ['accommodation_id' => SORT_ASC],
                    'desc' => ['accommodation_id' => SORT_DESC],
                ],
                'booking_email' => [
                    'asc' => ['booking_email' => SORT_ASC],
                    'desc' => ['booking_email' => SORT_DESC],
                ],
                'youngest_age' => [
                    'asc' => ['booking_policies.youngest_age' => SORT_ASC],
                    'desc' => ['booking_policies.youngest_age' => SORT_DESC],
                ],
                'oldest_free_age' => [
                    'asc' => ['booking_policies.oldest_free_age' => SORT_ASC],
                    'desc' => ['booking_policies.oldest_free_age' => SORT_DESC],
                ],
                'license_type' => [
                    'asc' => ['providers_financial.license_type' => SORT_ASC],
                    'desc' => ['providers_financial.license_type' => SORT_DESC],
                ],
                'license_expiration' => [
                    'asc' => ['providers_financial.license_expiration_date' => SORT_ASC],
                    'desc' => ['providers_financial.license_expiration_date' => SORT_DESC],
                ],
                'checkin_time_from' => [
                    'asc' => ['booking_policies.checkin_time_from' => SORT_ASC],
                    'desc' => ['booking_policies.checkin_time_from' => SORT_DESC],
                ],
                'checkin_time_to' => [
                    'asc' => ['booking_policies.checkin_time_to' => SORT_ASC],
                    'desc' => ['booking_policies.checkin_time_to' => SORT_DESC],
                ],
                'checkout_time_from' => [
                    'asc' => ['booking_policies.checkout_time_from' => SORT_ASC],
                    'desc' => ['booking_policies.checkout_time_from' => SORT_DESC],
                ],
                'checkout_time_to' => [
                    'asc' => ['booking_policies.checkout_time_to' => SORT_ASC],
                    'desc' => ['booking_policies.checkout_time_to' => SORT_DESC],
                ],
                'city_name' => [
                    'asc' => ['cities.name' => SORT_ASC],
                    'desc' => ['cities.name' => SORT_DESC],
                ],
                'state_id' => [
                    'asc' => ['states.id' => SORT_ASC],
                    'desc' => ['states.id' => SORT_DESC],
                ],

            ]
        ]);

        $this->load($params);

        if (!$this->validate()) 
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_category_id' => $this->provider_category_id,
            'provider_type_id' => $this->provider_type_id,
            'provider_admin_id' => $this->provider_admin_id,
            'provider_admin' => $this->provider_admin,
            'country_id' => $this->country_id,
            'city_id' => $this->city_id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'business_country_id' => $this->business_country_id,
            'business_city_id' => $this->business_city_id,
            'business_state_id' => $this->business_state_id,
            'accommodation_id' => $this->accommodation_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'booking_email', $this->booking_email])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'kennitala', $this->kennitala])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'business_name', $this->business_name])
            ->andFilterWhere(['like', 'business_address', $this->business_address])
            ->andFilterWhere(['like', 'business_postal_code', $this->business_postal_code])
            ->andFilterWhere(['like', 'booking_policies.youngest_age', $this->youngest_age])
            ->andFilterWhere(['like', 'booking_policies.oldest_free_age', $this->oldest_free_age])
            ->andFilterWhere(['like', 'booking_policies.checkout_time_from', $this->checkout_time_from])
            ->andFilterWhere(['like', 'booking_policies.checkout_time_to', $this->checkout_time_to])
            ->andFilterWhere(['like', 'booking_policies.checkin_time_from', $this->checkin_time_from])
            ->andFilterWhere(['like', 'booking_policies.checkin_time_to', $this->checkin_time_to])
            ->andFilterWhere(['like', 'providers_financial.license_type', $this->license_type])
            ->andFilterWhere(['like', 'cities.name', $this->city_name])
            ->andFilterWhere(['like', 'states.id', $this->state_id])
            ->andFilterWhere(['like', 'providers_financial.license_expiration_date', strtotime($this->license_expiration)]);

        return $dataProvider;
    }

    public function searchImages($params)
    {
        $query = DestinationsImages::find()->where(['provider_id' => $params])->orderBy('display_order ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }

    public function searchExceptions($params)
    {
        $query = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $params])->orderBy('date ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //$query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}

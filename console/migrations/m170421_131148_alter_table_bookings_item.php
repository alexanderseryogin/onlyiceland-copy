<?php

use yii\db\Migration;

class m170421_131148_alter_table_bookings_item extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `booking_number` INT(11) NULL AFTER `id`;
                        ');
    }

    public function down()
    {
        echo "m170421_131148_alter_table_bookings_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

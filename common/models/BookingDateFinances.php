<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_date_finances".
 *
 * @property integer $id
 * @property string $booking_date_id
 * @property string $booking_item_id
 * @property string $booking_group_id
 * @property string $price_description
 * @property double $amount
 * @property double $vat
 * @property double $tax
 * @property integer $quantity
 * @property integer $show_receipt
 * @property integer $added_by
 * @property boolean $discount_disable
 *
 * @property BookingDates $bookingDate
 * @property BookingsItems $bookingItem
 * @property BookingGroup $bookingGroup
 */
class BookingDateFinances extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const SHOW_RECEIPT = 0;
    const HIDE_RECEIPT = 1;

    const TYPE_DEBIT = 1;
    const TYPE_CREDIT = 2;

    const ITEM_TYPE_BOOKING = 1;
    const ITEM_TYPE_UPSELL = 2;
    const ITEM_TYPE_ADDON = 3;
    const ITEM_TYPE_CUSTOM = 4;

    const ENTRY_TYPE_SYSTEM = 1;
    const ENTRY_TYPE_USER = 2;

    const ADDED_FROM_GROUP = 1;

    const PAYMETN_TYPE_NORMAL = 0;
    const PAYMETN_TYPE_DEPOSIT = 1;

    public static function tableName()
    {
        return 'booking_date_finances';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_item_id', 'booking_group_id', 'quantity', 'show_receipt', 'added_by','type','added_from_group','payment_type','entry_type', 'discount_disable'], 'integer'],
            [['price_description'], 'string'],
            [['amount', 'vat', 'tax','x','y','discount','discount_amount','sub_total1','sub_total2','vat_amount','final_total','deposit_percent_age'], 'number'],
            // [['booking_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingDates::className(), 'targetAttribute' => ['booking_date_id' => 'id']],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItems::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
            [['booking_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroup::className(), 'targetAttribute' => ['booking_group_id' => 'id']],
            [['date'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Booking Date',
            'booking_item_id' => 'Booking Item ID',
            'booking_group_id' => 'Booking Group ID',
            'price_description' => 'Price Description',
            'amount' => 'Amount',
            'vat' => 'Vat',
            'tax' => 'Tax',
            'quantity' => 'Quantity',
            'show_receipt' => 'Show Receipt',
            'added_by' => 'Added By',
            'discount_disable' => 'Disable discount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getBookingDate()
    // {
    //     return $this->hasOne(BookingDates::className(), ['id' => 'booking_date_id']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItems::className(), ['id' => 'booking_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroup()
    {
        return $this->hasOne(BookingGroup::className(), ['id' => 'booking_group_id']);
    }

    public function getBookingGroupFinanceDetail()
    {
        return $this->hasOne(BookingGroupFinanceDetails::className(), ['booking_date_finance_id' => 'id']);
    }
}

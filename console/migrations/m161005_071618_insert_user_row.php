<?php

use yii\db\Migration;
use common\models\User;
use yii\base\Security;

class m161005_071618_insert_user_row extends Migration
{
    
    public function up()
    {
        $password = "123456";

        $model= new User();
        
        $model->username = 'administrator';
        $model->setPassword($password);
        $model->generateAuthKey();
        $model->email='admin@gmail.com';
        $model->type=0;
        $model->status=10;

        $model->save();
    }

    public function down()
    {
        echo "m161005_071618_insert_user_row cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

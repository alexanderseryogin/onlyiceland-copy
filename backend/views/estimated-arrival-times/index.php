<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\EstimatedArrivalTimes;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\EstimatedArrivalTimesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Estimated Arrival Times');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estimated-arrival-times-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW ESTIMATED ARRIVAL TIME'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'estimated-arrival-times-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>   

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'time_group',
                'value' => function($data)
                {
                    return EstimatedArrivalTimes::$time_groups[$data->time_group];
                },
                'filter' => Html::dropDownList(
                    'EstimatedArrivalTimesSearch[time_group]', 
                    $searchModel['time_group'], 
                    EstimatedArrivalTimes::$time_groups,
                    [
                        'prompt' => 'Select a Time Group', 
                        'id'=>'time_group_dropdown', 
                        'class'=>'form-control',
                        'style' => 'width:100%',
                    ]),
            ],
            [
                'attribute' => 'start_time',
                'value' => function($data)
                {
                    if(!empty($data->start_time))
                    {
                        $data->start_time = date('H:i',strtotime($data->start_time));
                        return $data->start_time;
                    }
                    else
                    {
                        return $data->text;
                    }
                    
                },
            ],
            [
                'attribute' => 'end_time',
                'value' => function($data)
                {
                    if(!empty($data->end_time))
                    {
                        $data->end_time = date('H:i',strtotime($data->end_time));
                    }
                    return $data->end_time;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#time_group_dropdown').select2({
        placeholder: \"Select a Time Group\",
        allowClear: true
    });

    $('#text_dropdown').select2({
        placeholder: \"Select a Time Group\",
        allowClear: true
    });

    $('#estimated-arrival-times-gridview').on('pjax:end', function() {

        $('#time_group_dropdown').select2({
            placeholder: \"Select a Time Group\",
            allowClear: true
        });

        $('#text_dropdown').select2({
            placeholder: \"Select a Time Group\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>

<?php

use common\models\BookableItemsImages;
use common\models\BookableItems;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<div class="property-grid">
    <ul class="grid-holder col-3">
        <?php
        foreach ($models as $provider) {
            $image_path = "//placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER";
            $image_count = 1;

            $query = BookableItems::find()->where(['provider_id' => $provider->id]);

            $items = $query->all();
            $result = ArrayHelper::map($items, 'id', 'id');
            $min_price = $query->min('min_price');

            if (!empty($provider->featured_image)) {
                $image_path = Yii::getAlias('@web') . '/uploads/Destinations/' . $provider->id . '/images/' . $provider->featured_image;
            }

            $arr = explode('_', $min_price);
            $min_price = $arr[count($arr) - 1];
            if (empty($min_price))
                $min_price = 'Not Set';

            if (strlen($provider->name) > 65) {
                $provider->name = substr($provider->name, 0, 65);
                $provider->name = $provider->name . '......';
            }
            ?>
            <li class="grid-item type-rent">
                <div class="property-block">

                    <a href="#" class="property-featured-image">
                        <img src="<?= $image_path ;?>" alt="" style="height:170px; width:220px;">
                        <span class="images-count"><i class="fa fa-picture-o"></i>
                            <?= $image_count ;?>
                        </span>
                    </a>

                    <div class="property-info" style="height:90px;">
                        <h4>
                            <a href="<?= Url::to(['/search/bookable-items', 'destination_id' => $provider->id]) ;?>">
                               <?= $provider->name ;?>
                            </a>
                        </h4>
                        <div>
                            <?php
                            $counter = 0;
                            foreach ($banners as $banner) {
                                ?>
                                <?php if ($banner->provider_id == $provider->id && $counter < 5) { ?>
                                    <img src="<?= Yii::getAlias('@web') . '/uploads/amenities/' . $banner->amenities_id . '/icons/' . $banner->amenities->icon ?>"
                                         alt="<?= $banner->amenities->name; ?>"
                                         data-toggle="tooltip" data-placement="top" title="<?= $banner->amenities->name;?>">
                                    <?php $counter++;
                                } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="property-amenities clearfix">
                        <span class="area"><strong><?= count($result);?></strong>Quantity</span>
                        <span class="min_price"><strong><?= $min_price ;?></strong>Min Price</span>
                    </div>
                </div>
            </li>
    <?php } ?>

    </ul>
</div>
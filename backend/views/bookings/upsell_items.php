
<?php
	use common\models\RatesUpsell;

	$Checkbox = [
	    0 => '',
	    1 => 'checked'
	];

	$Options = [
        0 => '',
        1 => 'selected',
    ];
	
	$upsell_items = RatesUpsell::find()
            ->where(['rates_id' => $rates_id])
            ->andWhere(['>','price',0])
            ->all();

    echo '<td class="upsell-items-div">';
    
	if(!empty($upsell_items))
	{
		foreach ($upsell_items as $key =>  $upsell_item) 
	    {
	    	$checked = 0; 
	    	$display = 'none';

	    	if(isset($checked_upsell_items) && !empty($checked_upsell_items) && array_key_exists($upsell_item->upsell_id, $checked_upsell_items))
	    	{
	    		// $org_price = RatesUpsell::findOne(['rates_id' => $org_rate_id, 'upsell_id' => $upsell_item->upsell_id ]); 
	    		// if($org_price->price == $upsell_item->price)
	    		// {
	    			$checked = 1; 
	    		//}
	    		
	    	}

	    	if(isset($org_rate_exists) && $org_rate_exists == 1)
            {
                $checked = 0;
            }

	    	if((isset($selected_upsell_extra_bed_quantity) && !empty($selected_upsell_extra_bed_quantity) && array_key_exists($upsell_item->upsell_id, $selected_upsell_extra_bed_quantity)))
	        {
	        	$display = 'inline';
	        }
	   
	    	if(!empty($upsell_item->price))
	    	{
	    		$price = $upsell_item->price;
	    		$upsell_item->price = Yii::$app->formatter->asDecimal( $upsell_item->price, "ISK");
	    		
	    		?>
	    		<?php if($upsell_item->upsell->type == 7 && $max_extra_beds > 0){?> <!-- Optional Extra Beds -->

	    			<label>
						<input date="<?php echo $dateKey; ?>" data="<?php echo $upsell_item->upsell_id ?>" id="<?php echo $upsell_item->rates->unit_id.':'.$rates_id?>" class="upsell-items upsell-items-<?php echo $rates_id.'-'.$dateKey?>" value="<?php echo $price?>" <?php echo $Checkbox[$checked]?> type="checkbox" >
						<?php echo $upsell_item->upsell->name.': '.$upsell_item->price; ?>
					</label>

		    		<select date="<?php echo $dateKey; ?>" style="display:<?php echo $display ?>; width: 40px; margin-left:10px;" data="<?php echo $upsell_item->rates->unit->id.':'.$rates_id?>" class="upsell-extra-bed-quantity upsell-extra-bed-quantity-<?php echo $rates_id?> upsell-extra-bed-quantity-dropdown-<?php echo $rates_id ?>-<?php echo $upsell_item->upsell_id.'-'.$dateKey?>" >
	                    <?php

	                        for ($i=0; $i <=$max_extra_beds ; $i++) 
	                        {
	                            $selected = 0;

	                            if((isset($selected_upsell_extra_bed_quantity) && !empty($selected_upsell_extra_bed_quantity) && array_key_exists($upsell_item->upsell_id, $selected_upsell_extra_bed_quantity) && $selected_upsell_extra_bed_quantity[$upsell_item->upsell_id]==$i)  || $i==1)
	                            {
	                            	$selected = 1;
	                            }

	                            if(isset($org_rate_exists) && $org_rate_exists == 1)
					            {
					                $selected = 0;
					            }
	                            echo '<option value="'.$i.'" '.$Options[$selected].'>'.$i.'</option>';
	                        }
	                    ?>
	                </select>
	                <br>

            	<?php }else{?>
            		<label>
						<input date="<?php echo $dateKey; ?>" data="<?php echo $upsell_item->upsell_id ?>" id="<?php echo $upsell_item->rates->unit_id.':'.$rates_id?>" class="upsell-items upsell-items-<?php echo $rates_id.'-'.$dateKey?>" value="<?php echo $price?>" <?php echo $Checkbox[$checked]?> type="checkbox" >
						<?php echo $upsell_item->upsell->name.': '.$upsell_item->price; ?>
					</label>
					<br>
            	<?php } ?>
	    		<?php

	    	}
	    }
	}

	echo '</td>';

?>

	



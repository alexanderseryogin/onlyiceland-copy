<?php

use yii\db\Migration;

class m161028_105026_create_table_attractions_images extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `attractions_images`( `id` INT(11) NOT NULL AUTO_INCREMENT, `attraction_id` INT(11), `image` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `description` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `display_order` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_attractions_images_a_id` FOREIGN KEY (`attraction_id`) REFERENCES `attractions`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8; 
        ");
    }

    public function down()
    {
        echo "m161028_105026_create_table_attractions_images cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

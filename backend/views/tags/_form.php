<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Tags;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\Tags */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord)
	$model->type = 0;
?>
<div class="portlet light ">
	<div class="tags-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <div class="row">
	    	<div class="col-sm-6">
	    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	    	</div>

	    	<div class="col-sm-6">
	    		<?= $form->field($model, 'type', [
                        'inputOptions' => [
                                        'id' => 'type_dropdown',
                                        'class' => 'form-control',
                                        'style' => 'width: 100%'
                                        ]
                ])->dropdownList(Tags::$types,['prompt'=>'Select a Type']) ?>
	    	</div>
	    </div>

	    <div class="form-group" style="margin-top: 10px;">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	        <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/tags']) ?>" >Cancel</a>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

});",View::POS_END);
?>
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_items_images".
 *
 * @property integer $id
 * @property integer $bookable_items_id
 * @property string $image
 * @property string $description
 * @property integer $display_order
 *
 * @property BookableItems $bookableItems
 */
class BookableItemsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_items_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookable_items_id', 'display_order'], 'integer'],
            [['image'], 'string', 'max' => 256],
            [['description'], 'string', 'max' => 500],
            [['bookable_items_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_items_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'bookable_items_id' => Yii::t('app', 'Bookable Items ID'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'display_order' => Yii::t('app', 'Display Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItems()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_items_id']);
    }

    public function isFirstRecord()
    {
          $first = BookableItemsImages::find()->where(['bookable_items_id' => $this->bookable_items_id])->orderBy('display_order ASC')->one();
          return $this->id == $first->id ? true : false;
    }

    public function isLastRecord()
    {
          $last = BookableItemsImages::find()->where(['bookable_items_id' => $this->bookable_items_id])->orderBy('display_order DESC')->one();
          return $this->id == $last->id ? true : false;
    }
}

<?php

use yii\db\Migration;

class m180423_055616_create_table_bookings_log extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `bookings_log`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `user_id` INT(11),
                          `booking_id` INT(11) UNSIGNED,
                          `setting` VARCHAR(256),
                          `old_value` TEXT,
                          `new_value` TEXT,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180423_055616_create_table_bookings_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_055616_create_table_bookings_log cannot be reverted.\n";

        return false;
    }
    */
}

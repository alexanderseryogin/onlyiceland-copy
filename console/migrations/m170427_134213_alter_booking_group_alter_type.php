<?php

use yii\db\Migration;

class m170427_134213_alter_booking_group_alter_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_group` CHANGE `cancellation_id` `cancellation_id` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
        ");
    }

    public function down()
    {
        echo "m170427_134213_alter_booking_group_alter_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170926_064433_alter_table_notifications_add_field_bookable_unit_id extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `notifications`   
                      ADD COLUMN `bookable_item_name_id` INT(11) UNSIGNED NULL AFTER `bookable_item_id`,
                      ADD FOREIGN KEY (`bookable_item_name_id`) REFERENCES `bookable_items_names`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
                    ');
    }

    public function down()
    {
        echo "m170926_064433_alter_table_notifications_add_field_bookable_unit_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

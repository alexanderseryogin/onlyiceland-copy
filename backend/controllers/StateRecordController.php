<?php

namespace backend\controllers;

use Yii;
use common\models\StateRecord;
use common\models\StateRecordSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\BaseController;

use yii\web\UploadedFile;
use common\models\UploadFile;

/**
 * StateRecordController implements the CRUD actions for StateRecord model.
 */
class StateRecordController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all StateRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StateRecordSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['StateRecordSearch']) && !$this->isArrayEmpty($params['StateRecordSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StateRecord model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StateRecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StateRecord();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StateRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) 
        {
            $model->save();

            $uploadFile = new UploadFile();

            if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'image_file')) 
            {
                if($uploadFile->uploadFile('states',$model->state_id,'images'))
                {
                    if(!empty($model->image))
                    {
                        $prev_image = Yii::getAlias('@app').'/../uploads/states/'.$model->state_id.'/images/'.$model->image;
                        unlink($prev_image);
                    }

                    $model->image = $uploadFile->FileName;
                    $model->update();
                }
            }

            return $this->redirect('index');
        } 
        else 
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StateRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StateRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StateRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StateRecord::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\db\Migration;

class m170417_112746_alter_table_booking_group extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_group` ADD COLUMN `group_guest_user_id` INT(11) UNSIGNED NULL AFTER `group_user_id`, ADD CONSTRAINT `FK_bookings_group_user_id` FOREIGN KEY (`group_guest_user_id`) REFERENCES `booking_guests`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
            ");
    }

    public function down()
    {
        echo "m170417_112746_alter_table_booking_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\widgets\LinkPager;
use common\models\BookableItemsImages;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationTypes;

if($provider_type == 11) {
    $this->title = 'Destinations';
} else if($provider_type == 6) {
    $this->title = 'Restaurants';
} else {
    $this->title = 'Destinations';
}

?>
  <!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">

              	<!-- ************ Start Sidebar  ************ -->
                
                <?=

                    $this->render('sidebar',[
                        'region_id' => $region_id,
                        'provider_type' => $provider_type,
                        'accommodation_id' => $accommodation_id,
                        'date_checkin' => $date_checkin,
                        'date_checkout' => $date_checkout,
                        'all_amenities' => $all_amenities,
                        'amenities' => $amenities,
                        'regions' => $regions,
                        'accommodation_types' => $accommodation_types,
                        'accommodation_types_list' => $accommodation_types_list,
                        'food_types' => $food_types,
                        'food_types_list' => $food_types_list
                    ]);
					
				?>

                <!-- ************ Heading  ************ -->

                <div class="col-md-9">
                    <div class="block-heading">
                        <h4><span class="heading-icon"><i class="fa fa-th-list"></i></span>
                            <?php if($provider_type == 11) {
                                echo "Destinations";
                            } else if($provider_type == 6) {
                                echo "Restaurants";
                            } else {
                                echo "Destinations";
                            }?>
                        </h4>
                        <div class="toggle-view pull-right">
                         <?php
    					// check call for list view or grid view

                          if(isset($gridview) && $gridview==1)
                          {
                          	echo '<a class="active" id="grid_view"><i class="fa fa-th-large"></i></a>
                              			<a id="list_view"><i class="fa fa-th-list"></i></a>';
                          }
                             else
                             {
                              echo '<a id="grid_view"><i class="fa fa-th-large"></i></a>
                              			<a id="list_view" class="active"><i class="fa fa-th-list"></i></a>';
                             }
                         ?>
                        </div>
                    </div>

	                <?php 
	                	if(empty($models))
						{	
							echo '<li style="background-color:#00AAB5; color:white; height:30px; font-size:17px;">';
							echo 'No Record Found.';
							echo '</li>';
						}
	                ?>

	                <!-- ************ Grid view  ************ -->

	                <?php if(isset($gridview) && $gridview==1): ?>

		                <?=
		                	$this->render('gridview',[
		                		   'models' => $models, // model of provider
                                    'banners' => $banners,
		                		]);
						?>

					<?php else: ?>
	                
	                <!-- ************ List view  ************ -->    
	                    <?=
		                	$this->render('listview',[
		                		   'models' => $models,
                                   'banners' => $banners,
		                		]);
						?>

	                <?php endif ?>

                    <?= // display pagination
						LinkPager::widget([
						    'pagination' => $pages,
						]);
					?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$itemsUrl =  Url::to(['/search/items'],true);

$acc_value = DestinationTypes::findOne(['name' => 'Accommodation']);
$acc_id = $acc_value->id; 

$this->registerJs("
jQuery(document).ready(function() 
{
    var acc_id = $acc_id;
    $('#accommodation_checklist').hide();

    $('select[name=\"provider_type\"]').change(function ()
    {
      if($('#provider_type option:selected').val()==acc_id)
      {
        $('#accommodation_checklist').fadeIn(2000);
      }
      else
      {
        $('#accommodation_checklist').fadeOut(2000);
      }
    });

	var count = 0 ;    
	var regions = '';

	$('input[class=\"regions\"]').change(function()
    {
    	regions = '';
    	count=0;

       	$('input[class=\"regions\"]').each(function()
	    {
	    	if(this.checked)
	    	{
	    		if(count==0)
		    		regions = $(this).attr('id');
		    	else
		    		regions = regions + ',' + $(this).attr('id');

	    	}
	    	count++;
	    });
    });

    var count = 0 ;    
	var accommodations = '';

	$('input[class=\"accommodations\"]').change(function()
    {
    	accommodations = '';
    	count=0;

       	$('input[class=\"accommodations\"]').each(function()
	    {
	    	if(this.checked)
	    	{
	    		if(count==0)
		    		accommodations = $(this).attr('id');
		    	else
		    		accommodations = accommodations + ',' + $(this).attr('id');

	    	}
	    	count++;
	    });
    });

    $('#search').click(function()
	{
		$('input[class=\"regions\"]').trigger('change');
	    $('#region_id').val(regions);

	    $('input[class=\"accommodations\"]').trigger('change');
	    $('#accommodation_id').val(accommodations);

	    if($('#provider_type option:selected').val()!=acc_id)
	    {
	    	$('#accommodation_id').val('');
	    }

	    $('#filter_form').submit();
	});

	$('#search_up').click(function()
	{
		$('#search').trigger('click');
	});

	$('#list_view').click(function()
	{
		$('#search').trigger('click');
	});

	$('#grid_view').click(function()
	{
		$('input[class=\"regions\"]').trigger('change');
		$('input[class=\"accommodations\"]').trigger('change');

		var type_value = $('#provider_type option:selected').val();

		if(type_value!=acc_id)
	    {
	    	accommodations = '';
	    }

		location.replace('$itemsUrl?gridview=1&region_id='+regions+'&provider_type='+type_value+'&accommodation_id='+accommodations);
	});

});",View::POS_END);
?>
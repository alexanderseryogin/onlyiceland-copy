<?php

use yii\db\Migration;

class m161221_174556_alter_open_hours_add_second_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours` ADD COLUMN `mon_opening_second` TIME NULL AFTER `mon_switch`, ADD COLUMN `mon_closing_second` TIME NULL AFTER `mon_opening_second`, ADD COLUMN `mon_split_service` TINYINT NULL AFTER `mon_closing_second`, ADD COLUMN `tue_opening_second` TIME NULL AFTER `tue_switch`, ADD COLUMN `tue_closing_second` TIME NULL AFTER `tue_opening_second`, ADD COLUMN `tue_split_service` TINYINT NULL AFTER `tue_closing_second`, ADD COLUMN `wed_opening_second` TIME NULL AFTER `wed_switch`, ADD COLUMN `wed_closing_second` TIME NULL AFTER `wed_opening_second`, ADD COLUMN `wed_split_service` TINYINT NULL AFTER `wed_closing_second`, ADD COLUMN `thu_opening_second` TIME NULL AFTER `thu_switch`, ADD COLUMN `thu_closing_second` TIME NULL AFTER `thu_opening_second`, ADD COLUMN `thu_split_service` TINYINT NULL AFTER `thu_closing_second`, ADD COLUMN `fri_opening_second` TIME NULL AFTER `fri_switch`, ADD COLUMN `fri_closing_second` TIME NULL AFTER `fri_opening_second`, ADD COLUMN `fri_split_service` TINYINT NULL AFTER `fri_closing_second`, ADD COLUMN `sat_opening_second` TIME NULL AFTER `sat_switch`, ADD COLUMN `sat_closing_second` TIME NULL AFTER `sat_opening_second`, ADD COLUMN `sat_split_service` TINYINT NULL AFTER `sat_closing_second`, ADD COLUMN `sun_opening_second` TIME NULL AFTER `sun_switch`, ADD COLUMN `sun_closing_second` TIME NULL AFTER `sun_opening_second`, ADD COLUMN `sun_split_service` TINYINT NULL AFTER `sun_closing_second`; 
        ");
    }

    public function down()
    {
        echo "m161221_174556_alter_open_hours_add_second_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161201_075833_create_table_attraction_tags extends Migration
{
    public function up()
    {
        $this->execute("
                  CREATE TABLE `attraction_tags` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
            ");
    }

    public function down()
    {
        echo "m161201_075833_create_table_attraction_tags cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

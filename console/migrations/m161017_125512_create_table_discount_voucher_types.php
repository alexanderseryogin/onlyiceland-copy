<?php

use yii\db\Migration;

class m161017_125512_create_table_discount_voucher_types extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `discount_code_types`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161017_125512_create_table_discount_voucher_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AccommodationTypes */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="portlet light ">
	<div class="accommodation-types-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	        <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/accommodation-types']) ?>" >Cancel</a>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>
</div>
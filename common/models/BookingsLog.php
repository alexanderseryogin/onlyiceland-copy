<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookings_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $booking_id
 * @property string $setting
 * @property string $old_value
 * @property string $new_value
 *
 * @property User $user
 */
class BookingsLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'booking_id'], 'integer'],
            [['old_value', 'new_value'], 'string'],
            [['setting'], 'string', 'max' => 256],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'booking_id' => 'Booking ID',
            'setting' => 'Setting',
            'old_value' => 'Old Value',
            'new_value' => 'New Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

<?php

use yii\db\Migration;

class m170829_074819_create_table_booking_group_finance extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_group_finance`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `booking_group_id` INT(11) UNSIGNED,
                          `type` INT(11),
                          `amount` DOUBLE,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`booking_group_id`) REFERENCES `booking_group`(`id`) ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170829_074819_create_table_booking_group_finance cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

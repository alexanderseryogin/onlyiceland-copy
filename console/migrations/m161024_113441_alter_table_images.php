<?php

use yii\db\Migration;

class m161024_113441_alter_table_images extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookable_items_images` ADD COLUMN `description` VARCHAR(500) CHARSET utf8 COLLATE utf8_unicode_ci DEFAULT 'Not Set' NULL AFTER `image`, ADD COLUMN `display_order` INT(11) NULL AFTER `description`;

            ALTER TABLE `providers_images` ADD COLUMN `description` VARCHAR(500) CHARSET utf8 COLLATE utf8_unicode_ci DEFAULT 'Not Set' NULL AFTER `image`, ADD COLUMN `display_order` INT(11) NULL AFTER `description`; 
        ");
    }

    public function down()
    {
        echo "m161024_113441_alter_table_images cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

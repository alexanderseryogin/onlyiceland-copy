<?php

use yii\db\Migration;

class m170802_072455_create_table_bookings_dates_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_dates_cart` (
                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                      `booking_item_id` int(11) unsigned DEFAULT NULL,
                      `date` date DEFAULT NULL,
                      `item_id` int(11) DEFAULT NULL,
                      `item_name_id` int(11) unsigned DEFAULT NULL,
                      `rate_id` int(11) DEFAULT NULL,
                      `custom_rate` double DEFAULT NULL,
                      `quantity` int(11) DEFAULT NULL,
                      `no_of_nights` int(11) DEFAULT NULL,
                      `estimated_arrival_time_id` int(11) unsigned DEFAULT NULL,
                      `beds_combinations_id` int(11) unsigned DEFAULT NULL,
                      `user_id` int(11) DEFAULT NULL,
                      `guest_first_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `guest_last_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `guest_country_id` int(11) DEFAULT NULL,
                      `confirmation_id` int(11) DEFAULT NULL,
                      `booking_cancellation` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `flag_id` int(11) DEFAULT NULL,
                      `status_id` int(11) DEFAULT NULL,
                      `housekeeping_status_id` int(11) DEFAULT NULL,
                      `no_of_children` int(11) DEFAULT NULL,
                      `no_of_adults` int(11) DEFAULT NULL,
                      `person_no` int(11) DEFAULT NULL,
                      `person_price` int(11) DEFAULT NULL,
                      `total` decimal(8,2) DEFAULT NULL,
                      `price_json` text COLLATE utf8_unicode_ci,
                      `travel_partner_id` int(11) unsigned DEFAULT NULL,
                      `voucher_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `reference_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `offers_id` int(11) DEFAULT NULL,
                      PRIMARY KEY (`id`),
                      CONSTRAINT `FK_booking_dates_cart_beds_combinations_id` FOREIGN KEY (`beds_combinations_id`) REFERENCES `beds_combinations` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_country_id` FOREIGN KEY (`guest_country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_housekeeping_status_id` FOREIGN KEY (`housekeeping_status_id`) REFERENCES `housekeeping_status` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_offers_id` FOREIGN KEY (`offers_id`) REFERENCES `offers` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rates` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `destination_travel_partners` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_dates_cart_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `Fk_booking_dates_cart_booking_item_id` FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items_cart` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
                      CONSTRAINT `Fk_booking_dates_cart_item_name_id` FOREIGN KEY (`item_name_id`) REFERENCES `bookable_items_names` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
                    ) ENGINE=InnoDB AUTO_INCREMENT=3665 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
    }

    public function down()
    {
        echo "m170802_072455_create_table_bookings_dates_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

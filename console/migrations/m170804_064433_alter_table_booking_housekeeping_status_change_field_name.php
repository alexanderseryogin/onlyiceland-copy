<?php

use yii\db\Migration;

class m170804_064433_alter_table_booking_housekeeping_status_change_field_name extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_housekeeping_status`   
                      CHANGE `housekepping_status_id` `housekeeping_status_id` INT(11) NULL;
                    ');
    }

    public function down()
    {
        echo "m170804_064433_alter_table_booking_housekeeping_status_change_field_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

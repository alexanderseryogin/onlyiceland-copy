<?php

namespace frontend\controllers;

use common\helpers\DestinationDateSearch;
use common\models\AccommodationTypes;
use common\models\Amenities;
use common\models\BookingsItems;
use common\models\DestinationsAmenities;
use common\models\DestinationsOpenAllYear;
use common\models\DestinationsOpenHoursDays;
use common\models\DestinationsOpenHoursExceptions;
use common\models\Tags;
use Yii;
use yii\filters\VerbFilter;
use frontend\components\BaseController;
use common\models\Destinations;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\models\BookableItems;
use common\models\StateRecord;
use common\models\Cities;
use common\models\States;
use yii\helpers\Url;

class SearchController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    // ************* Region Destinations ************* //

    public function actionRegionDestinations($region_name)
    {
        $session = Yii::$app->session;
        $session->open();
        if(isset($session['gridview']))
            $gridview = $session['gridview'];

        $region_name = str_replace('-', ' ', $region_name);

        $regionModel = States::findOne(['name' => $region_name]);

        $query = Destinations::find()->where(['state_id' => $regionModel->id]);
        $result = ArrayHelper::map($query->all(), 'id', 'id');

        $state_record = StateRecord::findOne(['state_id' => $regionModel->id]);

        // ************* pagination on the record for view on the search page  ************* //

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        // ************* set number of items which will be display according to the layout list or grid view  *************//

        if(isset($gridview) && $gridview==1)
        {
            $pages->pageSize = 6;
        }
        else
        {
            $pages->pageSize = 4;
        }

        $model_ids = clone $query;
        $model_ids = $model_ids->select('id')->column();

        $models = $query->offset($pages->offset)
                        ->limit($pages->limit)
                        ->all();

        $banners = DestinationsAmenities::find()
            ->where([DestinationsAmenities::tableName().'.provider_id' => $model_ids])
            ->andWhere(['!=', DestinationsAmenities::tableName().'.type', 3])
            ->andWhere([DestinationsAmenities::tableName().'.can_be_banner' => 1])
            ->joinWith('amenities')
            ->all();

        // *************  render layout depends on grid view parameter *************//

        return $this->render('region-destinations', [
            'models' => $models,
            'pages' => $pages,
            'region_name' => $region_name,
            'gridview' => $gridview,
            'total_destinations' => count($result),
            'state_record' => $state_record,
            'banners' => $banners
        ]);
    }

    // ************* West Iceland Destinations ************* //

    public function actionIcelandTownDestinations($town_name)
    {
        $city = Cities::findOne(['name' => $town_name]);

        $session = Yii::$app->session;
        $session->open();
        if(isset($session['gridview']))
            $gridview = $session['gridview'];

        if(!empty($city))
        {
            $query = Destinations::find()->where(['city_id' => $city->id]);
            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);

            if(isset($gridview) && $gridview==1)
            {
                $pages->pageSize = 6;
            }
            else
            {
                $pages->pageSize = 4;
            }

            $models = $query->offset($pages->offset)
                            ->limit($pages->limit)
                            ->all();

            // *************  render layout depends on grid view parameter *************//

            return $this->render('iceland-town-destinations', [
                             'models' => $models,
                             'pages' => $pages,
                             'town_name' => $town_name,
                             'gridview' => $gridview,
                ]);
        }
        else
        {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    // ************* Advance Search ************* //

    public function actionItems($region_id = '', $gridview = '', $destination_type = '', $accommodation_id = '', $amenities = false, $date_checkin = false, $date_checkout = false, $accommodation_types = false, $food_types = false)
    {
        //echo '<pre>';
        //print_r($_GET);
        //exit;
        if(isset($region_id) && !empty($region_id))
        {
            $region_id = explode(',', $region_id);

            if(count($region_id)==1 && empty($destination_type))
            {
                $regionModel = States::findOne(['id' => $region_id]);
                return $this->redirect(Url::to(['/search/region-destinations', 'region_name' => $regionModel->alias]));
            }
        }

        if(isset($accommodation_id) && !empty($accommodation_id))
        {
            $accommodation_id = explode(',', $accommodation_id);
        }

        // ************* get all provider which meets with get parameters ************* //
        //accommodation
        if($destination_type == 11)
        {
            if($accommodation_id == '')
            {
                if($region_id == '')
                {
                    $query = Destinations::find()->where(['provider_type_id' => $destination_type]);
                }
                else
                {
                    $query = Destinations::find()->where(['state_id' => $region_id])
                                       ->andWhere(['provider_type_id' => $destination_type]);
                }
            }
            else
            {
                if($region_id == '')
                {
                    $query = Destinations::find()->Where(['provider_type_id' => $destination_type])
                                             ->andWhere(['accommodation_id' => $accommodation_id]);
                }
                else
                {
                    $query = Destinations::find()->where(['state_id' => $region_id])
                                             ->andWhere(['provider_type_id' => $destination_type])
                                             ->andWhere(['accommodation_id' => $accommodation_id]);
                }
            }

        }
        else if($destination_type == 6)
        {
            if($food_types == '')
            {
                if($region_id == '')
                {
                    $query = Destinations::find()->Where(['provider_type_id' => $destination_type]);
                }
                else
                {
                    $query = Destinations::find()->where(['state_id' => $region_id])
                                   ->andWhere(['provider_type_id' => $destination_type]);
                }
            }
            else
            {
                if($region_id == '')
                {
                   $query =  Destinations::find()->joinWith('destinationTags')->where(['providers.provider_type_id' => $destination_type ])
                                                                            ->andWhere(['providers_tags.tag_id' => $food_types ]);
                }
                else
                {
                    $query =  Destinations::find()->joinWith('destinationTags')->where(['state_id' => $region_id])
                                                                            ->andwhere(['providers.provider_type_id' => $destination_type ])
                                                                            ->andWhere(['providers_tags.tag_id' => $food_types ]);
                }
            }
        }
        else
        {
            if($destination_type=='')
            {
                if($region_id == '')
                {
                    $query = Destinations::find();
                }
                else
                {
                    $query = Destinations::find()->where(['state_id' => $region_id]);
                }
            }
            else
            {
                if($region_id == '')
                {
                    $query = Destinations::find()->Where(['provider_type_id' => $destination_type]);
                }
                else
                {
                    $query = Destinations::find()->where(['state_id' => $region_id])
                                   ->andWhere(['provider_type_id' => $destination_type]);
                }
            }
        }

        $query = $query->andWhere(['active' => 1]);

        // If amenities checked
        if ($destination_type == 11 && isset($amenities) && !empty($amenities)) {
            $amenities_id = explode(',', $amenities);
            $providers_with_amenities = DestinationsAmenities::find()
                ->select('provider_id')
                ->where(['amenities_id' => $amenities_id])
                ->indexBy('provider_id')
                ->column();
            $query->andWhere(['id' => $providers_with_amenities]);
        }

        // If check-in and check-out fields are set
        if ($date_checkin && $date_checkout) {

            $provider_ids = clone $query;
            $provider_ids = $provider_ids->column();


            $month_start = DestinationDateSearch::getMonth($date_checkin);
            $month_end = DestinationDateSearch::getMonth($date_checkout);

            //availability in chosen month
            $provider_ids = DestinationsOpenAllYear::find()
                ->select(['provider_id'])
                ->where(['provider_id' => $provider_ids])
                ->andWhere([$month_start => 1]);
            if ($month_start != $month_end) {
                $provider_ids = $provider_ids->andWhere([$month_end => 1]);
            }
            $provider_ids = $provider_ids->indexBy('provider_id')->column();

            //are there any bookings on that day
            $booking_providers = BookingsItems::find()
                ->select(['provider_id'])
                ->where(['provider_id' => $provider_ids])
                ->andWhere(['and',
                        [
                            'and',
                            ['>=', 'arrival_date', date('Y-m-d', strtotime($date_checkin))],
                            ['<=', 'arrival_date', date('Y-m-d', strtotime($date_checkout))]
                        ],
                        [
                            'and',
                            ['>=', 'departure_date', date('Y-m-d', strtotime($date_checkin))],
                            ['<=', 'departure_date', date('Y-m-d', strtotime($date_checkout))]
                        ]
                    ])
                ->indexBy('provider_id')
                ->column();

            //delete providers with bookings from $provider_ids array
            $provider_ids = array_diff($provider_ids, $booking_providers);

            $week_array = DestinationDateSearch::getWeekDays($date_checkin, $date_checkout);

            //Open in chosen days
            $provider_open = DestinationsOpenHoursDays::find()
                ->select(['provider_id'])
                ->where([
                    'provider_id' => $provider_ids,
                    'day' => $week_array,
                    'open' => 1
                ])
                ->indexBy('provider_id')
                ->column();

            $provider_closed = DestinationsOpenHoursDays::find()
                ->select(['provider_id'])
                ->where([
                    'provider_id' => $provider_ids,
                    'day' => $week_array,
                    'open' => 0
                ])
                ->indexBy('provider_id')
                ->column();

            $providers = array_diff($provider_open, $provider_closed);

            $exceptions = DestinationsOpenHoursExceptions::find()
                ->select(['provider_id'])
                ->where([
                    'provider_id' => $providers,
                    'state' => 0
                ])
                ->andWhere([
                    'and',
                    ['>=', 'date', date('Y-m-d', strtotime($date_checkin))],
                    ['<=', 'date', date('Y-m-d', strtotime($date_checkout))]
                ])
                ->indexBy('provider_id')
                ->column();

            $providers = array_diff($providers, $exceptions);

            $query->where(['id' => $providers]);
        }

        if($destination_type == 11 && isset($accommodation_types) && !empty($accommodation_types)) {
            $accommodation_ids =  explode(',', $accommodation_types);
            $query->where(['accommodation_id' => $accommodation_ids]);
        }

        // ************* pagination on the record for view on the search page  ************* //

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        // ************* set number of items which will be display according to the layout list or grid view  *************//

        if (isset($gridview) && $gridview == 1) {
            $pages->pageSize = 6;
        } else {
            $pages->pageSize = 4;
        }

        $model_ids = clone $query;
        $model_ids = $model_ids->select('providers.id')->column();

        $models = $query->offset($pages->offset)
                        ->limit($pages->limit)
                        ->all();

        $banners = DestinationsAmenities::find()
            ->where([DestinationsAmenities::tableName().'.provider_id' => $model_ids])
            ->andWhere(['!=', DestinationsAmenities::tableName().'.type', 3])
            ->andWhere([DestinationsAmenities::tableName().'.can_be_banner' => 1])
            ->joinWith('amenities')
            ->all();

        $all_amenities = Amenities::find()->all();
        $amenities = Amenities::find()->select('id')->where(['type' => 3])->indexBy('id')->column();

        $regions = States::find()->select('name')->where(['on_main' => 1])->indexBy('id')->column();

        //Acommodation type
        $accommodation_types_ids = Destinations::find()->select('accommodation_id')->asArray();
        $accommodation_types_list = AccommodationTypes::findAll($accommodation_types_ids);

        //for restaurants
        $food_types_list = Tags::find()->where(['type' => 3])->all();

        return $this->render('show-items', [
            'models' => $models,
            'pages' => $pages,
            'region_id' => $region_id,
            'provider_type' => $destination_type,
            'accommodation_id' => $accommodation_id,
            'gridview' => $gridview,
            'date_checkin' => $date_checkin ? $date_checkin : null ,
            'date_checkout' => $date_checkout ? $date_checkout : null,
            'all_amenities' => $all_amenities,
            'amenities' => isset($amenities_id) ? $amenities_id : [],
            'regions' => $regions,
            'banners' => $banners,
            'accommodation_types_list' => $accommodation_types_list,
            'accommodation_types' => explode(',', $accommodation_types),
            'food_types_list' => $food_types_list,
            'food_types' => explode(',', $food_types),
        ]);
    }

    // ************* Destination's Bookable Items ************* //

    public function actionBookableItems($destination_id='', $gridview='')
    {
        // ************* get all bookable items which meets with get parameters ************* //

        $query = BookableItems::find()->where(['provider_id' => $destination_id]);
        $result = ArrayHelper::map($query->all(), 'id', 'id');

        //$destination = Destinations::findOne(['id' => $destination_id]);
        $destination = Destinations::find()
            ->where(['providers.id' => $destination_id])
            ->joinWith([
                'destinationsAmenities' => function ($query) {
                    $query->andWhere(['!=', DestinationsAmenities::tableName().'.type', 3])
                        ->joinWith('amenities');
                }]
            )
            ->one();
        //print_r($destination); exit;
        // ************* pagination on the record for view on the search page  ************* //

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        // ************* set number of items which will be display according to the layout list or grid view  *************//

        if(isset($gridview) && $gridview==1)
        {
            $pages->pageSize = 6;
        }
        else
        {
            $pages->pageSize = 4;
        }

        $models = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // *************  render layout depends on grid view parameter *************//

        return $this->render('destination-items', [
            'models' => $models,
            'pages' => $pages,
            'destination' => $destination,
            'gridview' => $gridview,
            'total_items' => count($result),
        ]);
    }

    public function actionUpdateSession()
    {
        $session = Yii::$app->session;
        $session->open();
        $session['gridview'] = $_POST['view'];
    }
}

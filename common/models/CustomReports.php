<?php

namespace common\models;
use common\models\BookingsItems;
use Yii;
// use common\models\CustomReportReferers;
/**
 * This is the model class for table "custom_reports".
 *
 * @property integer $id
 * @property string $report_title
 * @property string $report_color
 * @property string $report_icon
 * @property string $description
 * @property string $arrival_date_to
 * @property string $arrival_date_from
 * @property string $departure_date_to
 * @property string $departure_date_from
 * @property string $booking_date_to
 * @property string $booking_date_from
 * @property string $sort_by
 * @property string $show_max
 *
 * @property CustomReportColumns[] $customReportColumns
 * @property CustomReportConditions[] $customReportConditions
 * @property CustomReportFlags[] $customReportFlags
 * @property CustomReportStatuses[] $customReportStatuses
 * @property CustomReportsReferers[] $customReportsReferers
 */

class CustomReports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $conditions;

    const COND_AND = 0;
    const COND_OR = 1;

    const SUM = 1;
    const AVERAGE = 2;

    public $destinations_array = [];
    public $statuses_array = [];
    public $flags_array = [];
    public $referers_array = [];

    const GROUP_BY = array(
        0 => 'Property',
        1 => 'Bookable Item',
        2 => 'Booking Group (All Bookings)',
        3 => 'Booking Group (Only Groups)'
        // 3 => '',
    );

    const INCLUDE_SUB_TOTALS = array(
        0 => 'Property',
        1 => 'Rooms',
        2 => 'Report',
        3 => 'Properties & Report',
        4 => 'Properties & Rooms',
        5 => 'Report & Rooms',
        6 => 'Properties, Rooms & Report',
        7 => 'None'
        // 3 => '',
    );
    

    public static function tableName()
    {
        return 'custom_reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['report_title','description'],'required'],
            [['arrival_date_to', 'arrival_date_from', 'departure_date_to', 'departure_date_from', 'booking_date_to', 'booking_date_from','statuses_operator','status_not_operator','flags_operator','flag_not_operator','referers_operator','referer_not_operator','sum_average','group_by','show_sub_totals'], 'safe'],
            [['report_title', 'report_color', 'report_icon', 'sort_by', 'show_max'], 'string', 'max' => 256],
            ['conditions','validateAttributes'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_title' => 'Report Title',
            'report_color' => 'Report Color',
            'report_icon' => 'Report Icon',
            'description' => 'Description',
            'arrival_date_to' => 'Arrival Date To',
            'arrival_date_from' => 'Arrival Date From',
            'departure_date_to' => 'Departure Date To',
            'departure_date_from' => 'Departure Date From',
            'booking_date_to' => 'Booking Date To',
            'booking_date_from' => 'Booking Date From',
            'sort_by' => 'Sort By',
            'show_max' => 'Show Max',
            'sum_average' => 'Sum/Average'
        ];
    }

    public function validateAttributes($attribute)
    {
        // echo "here";
        // exit();
        $items = $this->$attribute;
        if (!is_array($items)) 
        {
            $items = [];
        }
        foreach ($items as $index => $item)
        {
            // if(!empty($item['price']) && !is_numeric ($item['price'])) 
            // {
            //     $key = $attribute . '[' . $index . '][price]';
            //     $this->addError($key, 'Price must be number.');
            // }
            // if(!empty($item['quantity']) && !is_numeric ($item['quantity'])) 
            // {
            //     $key = $attribute . '[' . $index . '][quantity]';
            //     $this->addError($key, 'Qunatity must be number.');
            // }
            // else
            // {
            //      $this->addError($key, 'Not working.');
            // }
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReportColumns()
    {
        return $this->hasMany(CustomReportColumns::className(), ['custom_report_id' => 'id'])->orderBy(['custom_report_columns.id' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReportConditions()
    {
        return $this->hasMany(CustomReportConditions::className(), ['custom_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReportFlags()
    {
        return $this->hasMany(CustomReportFlags::className(), ['custom_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReportStatuses()
    {
        return $this->hasMany(CustomReportStatuses::className(), ['custom_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReportsReferers()
    {
        return $this->hasMany(CustomReportReferers::className(), ['custom_report_id' => 'id']);
    }

    public function getCustomReportDestinations()
    {
        return $this->hasMany(CustomReportDestinations::className(), ['custom_report_id' => 'id']);
    }

    public static function getConditions()
    {
        $conditions = array(

            '0' => 'AND',
            '1' => 'OR',
        );

        return $conditions;
    }

    public static function getOperators()
    {
        $operators = array(
            '0' => '=',
            '1' => '!=',
            '2' => '>',
            '3' => '<',
            '4' => 'LIKE'
        );

        return $operators;
    }

    public static function hasProperties($data)
    {
        if($data == 0 || $data == 3 ||$data == 4 ||$data == 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function hasRooms($data)
    {
        if($data == 1 || $data == 4 ||$data == 5 ||$data == 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static function hasReport($data)
    {
        if($data == 2 || $data == 3 ||$data == 5 ||$data == 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function hasBookingGroup($data)
    {
        $booking_item = BookingsItems::findOne(['id' => $data]);
        if(count($booking_item->bookingItemGroups->bookingGroup->bookingItemGroups) > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

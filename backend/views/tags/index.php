<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Tags;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tags-index">
    
    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW TAG'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'tag-gridview','timeout' => 10000, 'enablePushState' => false]); ?> 
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'name',
                [
                    'attribute' => 'type',
                    'label' => 'Type',
                    'value' => function($data)
                    {
                        return Tags::$types[$data->type];
                    },
                    'filter' => Html::dropDownList(
                        'TagsSearch[type]', 
                        $searchModel['type'], 
                        Tags::$types,
                        [
                            'prompt' => 'Select a Type', 
                            'id'=>'type_dropdown', 
                            'class'=>'form-control',
                            'style' => 'width:100%',
                        ]),
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#tag-gridview').on('pjax:end', function() {

            $('#type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>
<?php
use common\models\Rates;
use common\models\RatesDates;
use common\models\RatesCapacityPricing;
use common\models\Destinations;
use yii\helpers\ArrayHelper;
use common\models\BookingTypes;
use common\models\BookingStatuses;
use common\models\BookingConfirmationTypes;
use common\models\User;
use common\models\BookingsItems;
use common\models\BookingsItemsCart;
use common\models\BookableBedTypes;
use common\models\BedTypes;
use common\models\BookableBedsCombinations;
use common\models\BookingDates;
use common\models\BookingDatesCart;
use common\models\Countries;
use common\models\BookableItemsNames;
use common\models\HousekeepingStatus;
use common\models\RatesOverride;

$arrival_date = str_replace('/', '-', $arrival_date);
$arrival_date = date('Y-m-d',strtotime($arrival_date));

$departure_date = str_replace('/', '-', $departure_date);
$departure_date = date('Y-m-d',strtotime($departure_date));

$arrival_day = date('N',strtotime($arrival_date));
$departure_day = date('N',strtotime($departure_date));

$date1=date_create($departure_date);
$date2=date_create($arrival_date);
$diff=date_diff($date1,$date2);
$difference =  $diff->format("%a");

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$record_flag = 0;
$rate_conditions_flag = 0;
$item_quantity = 0;

echo '<div class="panel-group accordion" id="accordion_item_rates">';

foreach ($items as $key => $item) 
{
    $price_json = '';
    $show_item_flag = 0;
    $mergeDatesArr = [];
    $mergeOverriddenDatesArr = [];
    $combine_dates = '';
    $combine_dates_arr = '';
    $overriddencombine_dates_arr = '';
    $housekeeping_array = array();
    if(isset($item->bookableItemsHousekeepingStatus))
        {
            foreach ($item->bookableItemsHousekeepingStatus as $value) 
            {
                array_push($housekeeping_array, $value->housekeeping_status_id);    
            }
            
        }

    $ratesArr = BookingsItemsCart::checkAvailableRates($item->id,$arrival_date,$difference,$remove_rate_conditions);
    
    $availableRates = $ratesArr['availableRates'];
    $availableRatesIds = $ratesArr['availableRatesIds'];



    // Overridden rates array //
    $availableOverriddenRates = $ratesArr['availableOverriddenRates'];
    $availableOverriddenRatesIds = $ratesArr['availableOverriddenRatesIds'];
    $overridden_rates_count = $ratesArr['overridden_rates_count'];

    $rate_conditions_flag = BookingsItemsCart::checkRateConditions($item->id,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions);
    $availableItemNames = BookingsItemsCart::checkAvailableItemNamesIncludedCart($item->id,$arrival_date,$difference);
    // getting all rooms which are not closed
    $availableItemNamesClosedRooms = BookingsItemsCart::checkAvailableItemNamesforClosedDates($item->id,$arrival_date,$difference,$remove_rate_conditions);
    // echo "<pre>";
    // print_r($availableItemNamesClosedRooms);
    // exit();
    if(!empty($availableRatesIds))
    {
        foreach ($availableRatesIds as $key => $value) 
        {
            if(isset($mergeDatesArr[$value]))
            {
                $mergeDatesArr[$value][] = $key;
            }
            else
            {
                $mergeDatesArr[$value][] = $key;
            }
        }
    }

    // echo "<pre>";
    // print_r($mergeDatesArr);
    // exit();

    // for overridden rates //
    if(!empty($availableOverriddenRatesIds))
    {
        foreach ($availableOverriddenRatesIds as $key => $value) 
        {
            foreach ($value as $key2 => $value2) 
            {
                if(isset($mergeOverriddenDatesArr[$key][$value2]))
                {
                    $mergeOverriddenDatesArr[$key][$value2][] = $key2;
                }
                else
                {
                    $mergeOverriddenDatesArr[$key][$value2][] = $key2;
                }
            }
            
        }
    }
    
    // echo "<pre>";
    // print_r($mergeOverriddenDatesArr);
    // exit();

    // echo "<pre>";
    // echo "availableRates :".count($availableRates);
    // echo "availableOverriddenRates :".count($availableOverriddenRates);
    // echo "diff :".(count($availableRates)+count($availableOverriddenRates));
    // echo " org diff :".$difference;
    // exit();

    if(isset($remove_rate_conditions) && $remove_rate_conditions == 0 && !empty($availableRates) && !empty($availableItemNames) && count($availableRates) == $difference && $rate_conditions_flag==1)
    {
        if(empty($availableItemNamesClosedRooms))
        {
            $show_item_flag = 0;
        }
        else
        {
            $show_item_flag = 1;
        }
        
    }

    if(isset($remove_rate_conditions) && $remove_rate_conditions == 1 && !empty($availableRates)  )
    {
        // echo "2";
        // exit();
        $show_item_flag = 1;
    }

    // if($item->id == 30)
    // {
    //     echo "<pre>";
    //     echo 'show item falag : '.$show_item_flag;
    //     echo "<br>".$remove_rate_conditions;
    //     print_r($availableItemNames);
    //     print_r($availableItemNamesClosedRooms);
    //     exit();
    //     die();
    // }

    // echo "<pre>";
    // print_r($availableRates);
    // exit();

    $remaining_quantity = 1;

    
    if($show_item_flag)
    {

        if($remaining_quantity>0)
        {
            $record_flag = 1;
            
        ?>

            <div class="row" id="unit_panel_<?php echo $item->id?>">
                <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading" style="background: <?php echo $item->background_color?>; color: <?php echo $item->text_color?>">
                        <h4 class="panel-title">
                            <div class="accordion-toggle" data="<?php echo $item->id?>" data-toggle="collapse" data-parent="#accordion_item_rates" href="#collapse_<?php echo $item->id?>">
                                <label><?php echo $item->itemType->name; ?></label>
                            </div>
                            <button type="button" data="<?php echo $item->id?>" style="margin-top: -38px; margin-right: 10px;" class="pull-right btn btn-default add-item add-item-<?php echo $item->id?>">Add</button>
                        </h4>
                    </div>
                    <div id="collapse_<?php echo $item->id?>" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body" style="overflow-y:auto;">

                    <div class="row">
                        <div class="col-sm-4">
                            <label class="control-label"><strong>Item Names</strong></label>
                            <div class="form-group" >

                                <select style="width: 100%;" class="form-control item-name item-name-<?php echo $item->id ?>" >
                                    <option value=""> - - - - - -</option>
                                    <?php
                                        // if(!empty($availableItemNames))
                                        // {
                                            $item_names = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item->id])->all(),'id','item_name');

                                            $temp_item_name_flag = 0;
                                            $previous =null;

                                                # code...
                                            // }
                                            // foreach ($item_names as $key => $value) 
                                            // {
                                            ////// previous code which displays all bookable items ///////
                                            // foreach ($item_names as $key => $value) 
                                            // {
                                            //     if(in_array($key, $availableItemNames))
                                            //     {
                                            //         $selected = 0;
                                            //         if(!$temp_item_name_flag)
                                            //         {
                                            //             $selected = 2;
                                            //             $previous = $key;
                                            //             $temp_item_name_flag = 1;
                                            //         }
                                                    
                                            //         echo '<option data-data="1" value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                            //     }
                                            //     else
                                            //     {
                                            //         echo '<option data-data="0" value="'.$key.'" >'.$value.'</option>';
                                            //     }
                                            // }

                                            /////// new code for displaying only available item names ////////
                                            foreach ($availableItemNamesClosedRooms as $key => $value) 
                                            {
                                                if(in_array($value->id, $availableItemNames))
                                                {
                                                    $selected = 0;
                                                    if(!$temp_item_name_flag)
                                                    {
                                                        $selected = 2;
                                                        $previous = $key;
                                                        $temp_item_name_flag = 1;
                                                    }
                                                    
                                                    echo '<option data-data="1" value="'.$value->id.'" '.$Options[$selected].'>'.$value->item_name.'</option>';
                                                }
                                                else
                                                {
                                                    echo '<option data-data="0" value="'.$value->id.'" >'.$value->item_name.'</option>';
                                                }
                                            }
                                        //}
                                    ?>
                                </select>
                                <input type="hidden" class="previous" value="<?=$previous?>">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button style="margin-top: 30px;" data="<?php echo $item->id?>" type="button" class="btn btn-xs btn-default view-details">
                                <i class="fa fa-chevron-down" aria-hidden="true"></i> <span>View Details</span> 
                            </button>
                        </div>
                    </div>

                    <div class="panel panel-default rate-details-fields-<?php echo $item->id?>" style="display: none; background: #F1F1F1">
                        <div class="panel-body">
                            <?php 

                                if($item->destination->getDestinationTypeName()=='Accommodation')
                                {
                                    $itemBedsCombinationsModel = BookableBedsCombinations::find()->where(['item_id' => $item->id])->all();

                                    if(!empty($itemBedsCombinationsModel))
                                    {
                                        echo '<label class="control-label rate-details-fields-'.$item->id.'" style="display: none;"><strong>Bed Preference:</strong></label>';

                                        echo '<div class="rate-details-fields-'.$item->id.'" style="display: block;">';

                                            foreach ($itemBedsCombinationsModel as $key => $value) 
                                            {
                                                if(isset($item->default_bed_combinations_id) && !empty($item->default_bed_combinations_id))
                                                {
                                                    if($item->default_bed_combinations_id == $value->beds_combinations_id)
                                                    {
                                                         $checked = 1;
                                                    }
                                                    else
                                                    {
                                                        $checked = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    $checked = $key==0?1:0;
                                                }
                                                
                                                $path = Yii::getAlias('@web').'/../uploads/beds-combinations/'.$value->beds_combinations_id.'/icons/'.$value->bedsCombinations->icon;
                                                ?>

                                                <label style="margin-left:10px;" class="mt-radio mt-radio-outline">
                                                    <input data="<?php echo $value->beds_combinations_id?>" class="beds-combinations beds-combinations-<?php echo $item->id?>" value="<?php echo $value->beds_combinations_id?>" <?php echo $Options[$checked]?> type="radio"> <?php echo $value->bedsCombinations->combination?> <img src="<?php echo $path ?>" style="max-height:30px">
                                                    <span></span>
                                                </label>

                                                <?php
                                            }
                                        echo '</div>';
                                    }
                                }
                            ?>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>User</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control user-dropdown user-<?php echo $item->id?>" >
                                            <option value="">Select a User</option>
                                            <?php
                                                $users = ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_USER])->all(),
                                                'id',
                                                function($model, $defaultValue) 
                                                {
                                                    if(empty($model->profile->country_id))
                                                        $country = '';
                                                    else
                                                        $country = ' - '.$model->profile->country->name;

                                                    return $model->username.' - '.$model->email.$country;
                                                });

                                                if(!empty($users))
                                                {
                                                    $selected = 0;
                                                    foreach ($users as $key => $value) 
                                                    {
                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>First Name</strong></label>
                                    <div class="form-group" >
                                        <input type="text" class="form-control guest-first-name guest-first-name-<?php echo $item->id?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Last Name</strong></label>
                                    <div class="form-group" >
                                        <input type="text" class="form-control guest-last-name guest-last-name-<?php echo $item->id?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Country</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control guest-country guest-country-<?php echo $item->id?>" >
                                            <?php
                                                $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                                foreach ($countries as $key => $value) 
                                                {
                                                    $selected = ($value == 'Iceland') ? 2 : 0 ;
                                                    echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Booking Confirmation</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control rate-details-dropdowns booking-confirmation-<?php echo $item->id?>">

                                            <?php
                                                $bookingConfirmation =ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name');

                                                if(!empty($bookingConfirmation))
                                                {
                                                    foreach ($bookingConfirmation as $key => $value) 
                                                    {
                                                        $selected = ($key==$item->booking_confirmation_type_id) ? 2 : 0 ;

                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Cancellation Period for Bookings</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control rate-details-dropdowns booking-cancellation-<?php echo $item->id?>">
                                            <?php
                                                foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                                                {
                                                    $selected = ($key==$item->general_booking_cancellation) ? 2 : 0 ;

                                                    echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label"><strong>Flag</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control rate-details-dropdowns booking-flag-<?php echo $item->id?>">
                                            <?php
                                                $bookingTypes = ArrayHelper::map(BookingTypes::find()->all(),'id','label');

                                                if(!empty($bookingTypes))
                                                {
                                                    foreach ($bookingTypes as $key => $value) 
                                                    {
                                                        $selected = ($key==$item->booking_type_id) ? 2 : 0 ;

                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label"><strong>Status</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control rate-details-dropdowns booking-status-<?php echo $item->id?>">
                                            <?php
                                                $bookingStatuses = ArrayHelper::map(BookingStatuses::find()->all(),'id','label');

                                                if(!empty($bookingStatuses))
                                                {
                                                    foreach ($bookingStatuses as $key => $value) 
                                                    {
                                                        //$selected = ($key==$item->booking_status_id) ? 2 : 0 ;
                                            
                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2 <?=($item->destination->use_housekeeping == 1)?'':'hide' ?>">
                                    <label class="control-label"><strong>Housekeeping Status</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control housekeeping-status housekeeping-status-<?php echo $item->id?>" multiple="true">
                                            <!-- <option value="">Select a Status</option> -->
                                            <?php
                                                $housekeepingStatuses = ArrayHelper::map(HousekeepingStatus::find()->all(),'id','label');
                                                   
                                                if(!empty($housekeepingStatuses))
                                                {
                                                    
                                                    foreach ($housekeepingStatuses as $key => $value) 
                                                    {
                                                        //$selected = ($key==$item->booking_status_id) ? 2 : 0 ;
                                                           
                                                        echo  '<option class="remove-selected" value="'.$key.'" >'.$value.'</option>';

                                                    }

                                                }
                
                                              if(!empty($housekeeping_array))
                                              {
                                                $this->registerJs("
                                                        //alert('housekeeping-status-'+".$item->id.");
                                                        var housekeeping_statuses_array = ".json_encode($housekeeping_array).";
                                                        console.log( housekeeping_statuses_array );
                                                        console.log($('.housekeeping-status-'+".$item->id."));
                                                        var arr = [1,2,3];
                                                        $('.housekeeping-status-'+".$item->id.").select2().val(housekeeping_statuses_array).trigger('change');
                                                    ");
                                              }
                                              else
                                              {
                                                $this->registerJs("
                                                    $('.housekeeping-status-'+".$item->id.").select2({
                                                        multiple: true,
                                                        placeholder: 'Select a Status',
                                                        //width:'100%',
                                                        //data: z 
                                                    });
                                                ");
                                              }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row vou_ref_div hide">
                                <div class="col-sm-3">
                                    <label class="control-label "><strong>Voucher #</strong></label>
                                    <div class="form-group" >
                                        <input class="form-control voucher_no-<?php echo $item->id?>" name="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Reference #</strong></label>
                                    <div class="form-group" >
                                        <input class="form-control reference_no-<?php echo $item->id?>" type="" name="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label"><strong>Comment</strong></label>
                                    <div class="form-group" >
                                    <textarea class="form-control comment-<?php echo $item->id?>" type="" name="" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="control-label"><strong>Notes</strong></label>
                                    <div class="form-group" >
                                    <textarea class="form-control notes-<?php echo $item->id?>" type="" name="" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rate-details-fields-<?php echo $item->id?>" style="display: none;"><br></div>
            <?php
    //         echo "<pre>";
    // print_r($availableOverriddenRates);
    // exit();
            foreach ($availableRates as $dateKey => $rates) 
            {
                $dateFlag = 1;

                if(!empty($mergeDatesArr))
                {

                    if(!empty($combine_dates_arr) && in_array($dateKey, $combine_dates_arr))
                    {
                        $dateFlag = 0;
                    }
                    else
                    {
                        foreach ($mergeDatesArr as $rateKeys => $dateValues) 
                        {
                            // echo "<pre>";
                            // print_r($dateValues);
                            // exit();
                            if(in_array($dateKey, $dateValues) && count($dateValues)>1)
                            {
                                $combine_dates_arr = $dateValues;
                                break;
                            }
                            else
                            {
                                $combine_dates_arr = [];
                            }
                        }
                    }

                }
                $test_display_overridden_rates_arr = array();
                $flag_to_display_all_dates = true;
                if($dateFlag)
                {
                ?>
                    <table class="table table-bordered table-<?php echo $item->id?>" date="<?php echo $dateKey;?>">
                        <thead>
                            <tr>
                                <strong>
                                    <th width="13%">Date</th>
                                    <th width="87%">Available Rates</th>
                                </strong>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <?php
                                    $item_date = '';
                                    $flag_for_pricing_color = 1;
                                    // $normal_dates_arr = [];
                                    // $normal_dates_flag = true;
                                    $abnormal_dates_arr = [];
                                    $abnormal_dates_arr_flag = true;

                                    if(!empty($combine_dates_arr))
                                    {
                                        $dateKey = $combine_dates_arr[0];
                                        foreach ($combine_dates_arr as $key => $value) 
                                        {
                                            $check_rates_ids = array();
                                            foreach ($rates as $rate) 
                                            {
                                                array_push($check_rates_ids, $rate->id);
                                            }
                                            $rate_override = RatesOverride::findOne(['date' => $value, 'rate_id' => $check_rates_ids]);

                                            if(empty($rate_override))
                                            {
                                                // $normal_dates_flag = false;
                                                // $flag_for_pricing_color = 0;
                                                echo date('d/m/Y',strtotime($value)).'<br>';
                                                // $normal_dates_flag [] = date('d/m/Y',strtotime($value)).'<br>';
                                            }
                                            else
                                            {
                                                $display_capacity_prining_values = '';
                                                if($rate->destination->pricing == 1)
                                                {
                                                    $display_capacity_prining_values = ' [';
                                                    $display_capacity_prining_values .= isset($rate_override->single)?Yii::$app->formatter->asDecimal( $rate_override->single, "ISK"):'';
                                                    $display_capacity_prining_values .= isset($rate_override->double)?' | '.Yii::$app->formatter->asDecimal( $rate_override->double, "ISK"):'';
                                                    $display_capacity_prining_values .= isset($rate_override->triple)?' | '.Yii::$app->formatter->asDecimal( $rate_override->triple, "ISK"):'';
                                                    $display_capacity_prining_values .= isset($rate_override->quad)?' | '.Yii::$app->formatter->asDecimal( $rate_override->quad, "ISK"):'';
                                                    $display_capacity_prining_values .= ']';
                                                }
                                                // if($abnormal_dates_arr_flag)
                                                // {
                                                    // $abnormal_dates_arr[] = '<span style="color:red">'.date('d/m/Y',strtotime($value)).$display_capacity_prining_values.'</span><br>';
                                                    // $abnormal_dates_arr_flag = false;
                                                // }

                                                echo '<span style="color:red" class ="hide">'.date('d/m/Y',strtotime($value)).$display_capacity_prining_values.'</span><br class="hide">';
                                                // to diplay overridden_rates_below the table //
                                                $test_display_overridden_rates_arr[] = date('d/m/Y',strtotime($value)).$display_capacity_prining_values;
                                            }

                                            
                                        }
                                        if(count($combine_dates_arr) == count($test_display_overridden_rates_arr))
                                        {
                                            $flag_to_display_all_dates = false;
                                            // $displayDate = substr($test_display_overridden_rates_arr[0], start)
                                            echo  '<span style="color:red">'.substr($test_display_overridden_rates_arr[0], 0,10).'</span><br>';
                                            
                                            // display only date //
                                            // if($item->id == 34)
                                            // {
                                            //     print_r($test_display_overridden_rates_arr[0]);
                                            //     print_r( '<span style="color:red">'.date('d/m/Y',strtotime($value)).$display_capacity_prining_values.'</span><br>';);
                                            //     exit();
                                            // }
                                            
                                            array_splice($test_display_overridden_rates_arr,0,1);
                                        }
                                        $item_date = implode(',', $combine_dates_arr);
                                    }
                                    else
                                    {
                                        $check_rates_ids = array();
                                        foreach ($rates as $rate) 
                                        {
                                            array_push($check_rates_ids, $rate->id);
                                        }
                                        $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $check_rates_ids]);

                                        if(empty($rate_override))
                                        {
                                            $flag_for_pricing_color = 0;
                                            echo date('d/m/Y',strtotime($dateKey)).'<br>';
                                        }
                                        else
                                        {
                                            $display_capacity_prining_values = '';
                                            if($rate->destination->pricing == 1)
                                            {
                                                // $display_capacity_prining_values = ' [';

                                                // $display_capacity_prining_values .= isset($rate_override->single)?Yii::$app->formatter->asDecimal( $rate_override->single, "ISK"):'';
                                                // $display_capacity_prining_values .= isset($rate_override->double)?' | '.Yii::$app->formatter->asDecimal( $rate_override->double, "ISK"):'';
                                                // $display_capacity_prining_values .= isset($rate_override->triple)?' | '.Yii::$app->formatter->asDecimal( $rate_override->triple, "ISK"):'';
                                                // $display_capacity_prining_values .= isset($rate_override->quad)?' | '.Yii::$app->formatter->asDecimal( $rate_override->quad, "ISK"):'';
                                                // $display_capacity_prining_values .= ']';
                                            }
                                            echo '<span style="color:red">'.date('d/m/Y',strtotime($dateKey)).$display_capacity_prining_values.'</span><br>';
                                        }
                                        // echo date('d/m/Y',strtotime($dateKey)).'<br>';
                                        $item_date = $dateKey;
                                    }
                                ?>
                                <br>

                                <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $item->id?>" class="form-control item-quantity item-quantity-<?php echo $item->id?> item-quantities-<?php echo $item->id.'-'.$dateKey?>" value="1">

                                <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $item->id?>" class="form-control item-date-<?php echo $item->id.'-'.$dateKey?>" value="<?php echo $item_date ?>">
                            </td>
                            <td>
                            <?php
                            foreach ($rates as $key => $rate)
                            {
                                // ***************** add condition for checkin ******************* //
                                $rate_row = $key;

                                $checkin_allowed = explode(';', $rate->check_in_allowed);
                                $checkout_allowed = explode(';', $rate->check_out_allowed);

                                if(isset($remove_rate_conditions) && $remove_rate_conditions == 1)
                                {
                                    $rate_conditions_flag = 1;
                                }
                                else
                                {
                                    if(BookingsItems::RateAllowedQualifies($arrival_date,$departure_date,$rate->rate_allowed) && in_array($arrival_day-1, $checkin_allowed) && in_array($departure_day-1, $checkout_allowed))
                                    {
                                        $rate_conditions_flag = 1;
                                    }
                                    else
                                    {
                                        $rate_conditions_flag = 0;
                                    }
                                }
                                ?>

                                <div class="table-scrollable">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <strong>
                                                    <th width="20%">Rate</th>
                                                    
                                                    <?php
                                                        if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                        {  
                                                            echo '<th width="15%">Capacity Pricing </th>';
                                                        }
                                                        else if($rate->destination->pricing == 0)
                                                        {
                                                            echo '<th width="10%">Adults</th><th width="10%">Children</th>';
                                                        }
                                                    ?>
                                                    <?php if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                    { ?>
                                                    <th width="10%">Adults/Children</th>
                                                    <?php } ?>
                                                    <th width="22%">Available Discounts</th>
                                                    <th width="28%">Upsell Items</th>
                                                    <th width="10%">Total </th>
                                                </strong>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td  style="min-width:120px;">
                                                <input type="text" value="<?php echo  $rate->name?>" hidden>
                                                <?php echo  $rate->name.'<br><br>'?>

                                                <input type="text" value="<?php echo $item->id.':'.$rate->id.':'.$dateKey?>" class="helper-field helper-field-<?php echo $item->id?> helper-field-<?php echo $item->id.'-'.$dateKey?>" hidden>
                                            </td>

                                            <?php

                                            if($rate_conditions_flag)
                                            {
                                                // ************* capacity pricing ************ //

                                                if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                {
                                                ?>
                                                    <td date="<?php echo $dateKey; ?>" class="capacity-pricing-box capacity-pricing-box-<?php echo $item->id.'-'.$dateKey?>">

                                                <?php
                                                    $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $rate->id])->all();
                                                    $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $rate->id]);
                                                    // $flag_for_pricing_color = 0;
                                                    if(!empty($rates_capacity_pricing))
                                                    {
                                                        $count_overridden_rates = 0;
                                                        foreach ($rates_capacity_pricing as $key => $value)
                                                        {
                                                            $flag_for_pricing_color = 0;
                                                            if($value->person_no >=1 && $value->person_no<=4)
                                                            {
                                                                $word =  Rates::$numberWords[$value->person_no-1];
                                                                $checked = 0;

                                                                $price = $value->price;

                                                                // checking if there is only one date  and is overridden //
                                                                if(count($availableRates) == 1 && !empty($rate_override))
                                                                {
                                                                    if($key==0)
                                                                    {
                                                                        $value->price = Yii::$app->formatter->asDecimal( $rate_override->single, "ISK");
                                                                    }
                                                                    if($key==1)
                                                                    {
                                                                        $value->price = Yii::$app->formatter->asDecimal( $rate_override->double, "ISK");
                                                                    }
                                                                    if($key==2)
                                                                    {
                                                                        $value->price = Yii::$app->formatter->asDecimal( $rate_override->triple, "ISK");
                                                                    }
                                                                    if($key==3)
                                                                    {
                                                                        $value->price = Yii::$app->formatter->asDecimal( $rate_override->quad, "ISK");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    // $has_overridden_rate_flag = false;
                                                                    $count_overridden_rates = 0;
                                                                    // $has_normal_rate = true;

                                                                    $has_same_overridden_rates = true;

                                                                    $check_arr_for_overridden = [];
                                                                    $test_flag = false;
                                                                    $overridden_rates_count_for_price = [];
                                                                    foreach ($combine_dates_arr as  $value1) 
                                                                    {
                                                                        $rate_override = RatesOverride::findOne(['date' => $value1, 'rate_id' => $rate->id]);
                                                                        
                                                                        if(!empty($rate_override))
                                                                        {
                                                                            // array_push($overridden_rates_count_for_price, $rate_override);
                                                                            $has_overridden_rate_flag = true;
                                                                            $count_overridden_rates++;

                                                                            if($test_flag)
                                                                            {
                                                                                
                                                                                if($rate_override->single ==  $check_arr_for_overridden['single'] && 
                                                                                    $rate_override->double ==  $check_arr_for_overridden['double'] &&
                                                                                    $rate_override->triple ==  $check_arr_for_overridden['triple'] &&
                                                                                    $rate_override->quad ==  $check_arr_for_overridden['quad'])
                                                                                {

                                                                                }
                                                                                else
                                                                                {
                                                                                    $has_same_overridden_rates = false;
                                                                                    break;
                                                                                }
                                                
                                                                                    
                                                                            }
                                                                            else
                                                                            {
                                                                                
                                                                                $check_arr_for_overridden['single'] = $rate_override->single;
                                                                                $check_arr_for_overridden['double'] = $rate_override->double;
                                                                                $check_arr_for_overridden['triple'] = $rate_override->triple;
                                                                                $check_arr_for_overridden['quad'] = $rate_override->quad;
                                                                                
                                                                                $test_flag = true;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    

                                                                    foreach ($combine_dates_arr as  $value2) 
                                                                    {
                                                                        $rate_override2 = RatesOverride::findOne(['date' => $value2, 'rate_id' => $rate->id]);
                                                                        
                                                                        if(!empty($rate_override2))
                                                                        {
                                                                            array_push($overridden_rates_count_for_price, $rate_override2->id);
                                                                        }
                                                                    }

                                                                    // if($item->id == 34)
                                                                    // {
                                                                    //     echo "<pre>";
                                                                    //     print_r($combine_dates_arr);
                                                                    //     print_r($test_display_overridden_rates_arr);
                                                                    //     print_r($overridden_rates_count_for_price);
                                                                    //      exit();
                                                                    // }
                                                                    // print_r($has_same_overridden_rates);
                                                                    // print_r($check_arr_for_overridden);
                                                                   
                                                                    // if($count_overridden_rates == count($availableRates))
                                                                    if((count($overridden_rates_count_for_price) == count($combine_dates_arr) ) && !empty($combine_dates_arr))
                                                                    {
                                                                        // $flag_for_pricing_color = 1;
                                                                        // echo "here in if";
                                                                        // print_r($check_arr_for_overridden);
                                                                        // exit();
                                                                        // if($has_same_overridden_rates)
                                                                        // {
                                                                            if($key==0)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $check_arr_for_overridden['single'], "ISK");
                                                                            }
                                                                            if($key==1)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $check_arr_for_overridden['double'], "ISK");
                                                                            }
                                                                            if($key==2)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $check_arr_for_overridden['triple'], "ISK");
                                                                            }
                                                                            if($key==3)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $check_arr_for_overridden['quad'], "ISK");
                                                                            }
                                                                        // }
                                                                
                                                                    }
                                                                    else
                                                                    {
                                                                        // echo "here in else";
                                                                        // print_r($value);
                                                                        // exit();
                                                                        $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                                    }

                                                                //     echo "<pre>";
                                                                // print_r($count_overridden_rates);
                                                                // exit();
                                                                    
                                                                }
                                                                

                                                                $icon_user = '';

                                                                for ($i=1; $i <= $value->person_no ; $i++) 
                                                                { 
                                                                    $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                                }
                                                                ?>
                                                                <?php


                                                                    $flag_for_pricing_color = 1;
                                                                    if(empty($combine_dates_arr) && empty($rate_override))
                                                                    {
                                                                        $flag_for_pricing_color = 0;
                                                                    }
                                                                    foreach ($combine_dates_arr as  $value1) 
                                                                    {

                                                                        $rate_override = RatesOverride::findOne(['date' => $value1, 'rate_id' => $rate->id]);
                                                                        
                                                                        if(empty($rate_override))
                                                                        {
                                                                            $flag_for_pricing_color = 0;
                                                                        }
                                                                        
                                                                    }
                                                                    // echo "<pre>";
                                                                    // print_r($flag_for_pricing_color);
                                                                    // exit();
                                                                ?>
                                                                <label style="width: 100%">
                                                                    <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $value->person_no?>" id="<?php echo $item->id.':'.$rate->id.':'.$value->person_no?>" class="capacity-pricing capacity-pricing-<?php echo $item->id?> capacity-pricing-<?php echo $item->id.'-'.$dateKey?> cpricing-<?php echo $rate->id?> cpricing-<?php echo $rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $item->id.'-'.$dateKey.'-'.$rate_row?>" value="<?php echo $value->person_no.' : '.$price?>" <?php echo $Options[$checked]?>> <?php echo $icon_user?>
                                                                    <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$item->id.':'.$rate->id.':'.$value->person_no?>" value="<?=$value->price?>" date="<?php echo $dateKey?>" style="width: 50%;float: right;text-align:right;<?=(($flag_for_pricing_color==1))?'color:red;':''?>" data="<?php echo $value->person_no?>">
                                                                </label><br>
                                                                <?php
                                                            }
                                                        } 
                                                    }
                                                    echo '</td>'; ?>
                                                    <td style="min-width:20px;">
                                                        <label>AD</label>
                                                        <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                            <?php
                                                                $adult_count = isset($item->max_adults)?$item->max_adults:10;
                                                                for ($i=0; $i <= $adult_count ; $i++) 
                                                                { 
                                                                    $selected = 0;
                                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                        <br>
                                                        <label>CH</label>
                                                        <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                            <?php
                                                                $children_count = isset($item->max_children)?$item->max_children:10;
                                                                for ($i=0; $i <= $children_count ; $i++) 
                                                                { 
                                                                    $selected = 0;
                                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>

                                               <?php }

                                                else if($rate->destination->pricing == 0)  // ************* First/Additional ************ //
                                                {
                                                ?>
                                                    <td style="min-width:50px;">
                                                        <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                            <?php
                                                                for ($i=0; $i <= 20 ; $i++) 
                                                                { 
                                                                    $selected = 0;
                                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>

                                                    <td style="min-width:50px;">
                                                        <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                            <?php
                                                                for ($i=0; $i <= 20 ; $i++) 
                                                                { 
                                                                    $selected = 0;
                                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>
                                                <?php
                                                }
                                            }
                                            else
                                            {
                                            ?>
                                                <td colspan="5">
                                                    <span class="label label-danger"> Note! </span>
                                                    <span>&nbsp; Rate did not match with the conditions (Rate Allowed /Check-in Allowed / Checkout Allowed). </span>
                                                </td>
                                            <?php
                                            } 
                                            ?>

                                        <?php

                                        $max_extra_beds = $item->max_extra_beds;
                                        $bed_type_people = 0;

                                        $selected_upsell_extra_bed_quantity = '';
                                        $selected_discount_code_extra_bed_quantity = '';

                                        if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                        {
                                            $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $item->id])->all();

                                            if(!empty($itemBedTypesModel))
                                            {
                                                foreach ($itemBedTypesModel as $key => $value) 
                                                {
                                                    $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                                }
                                            }
                                        }

                                        if($rate_conditions_flag)
                                        {
                                            // *********** Discount Codes ********** //

                                            $checked_discount_codes = '';

                                            echo $this->render('discount_codes',
                                            [
                                                'dateKey' => $dateKey,
                                                'rates_id' => $rate->id,
                                                'checked_discount_codes' => $checked_discount_codes,
                                                'bed_type_people' => $bed_type_people,
                                                'max_extra_beds' => $max_extra_beds,
                                                'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                                // 'model_id' => $model->id,
                                                // 'bookedDate' =>$bookedDate,
                                            ]);

                                            // *********** Upsell Items ********** //

                                            $checked_upsell_items = '';

                                            echo $this->render('upsell_items',
                                            [
                                                'dateKey' => $dateKey,
                                                'rates_id' => $rate->id,
                                                'checked_upsell_items' => $checked_upsell_items,
                                                'max_extra_beds' => $max_extra_beds,
                                                'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                                // 'model_id' => $model->id,
                                                // 'bookedDate' =>$bookedDate,
                                            ]);
                                        }

                                        $orgTotal = 0;
                                        $icelandTotal = 0;
                                        $tooltips_title = 'Item Price';

                                        ?>

                                        <td>
                                            <input class="item-total-input item-total-input-<?php echo $rate->id.'-'.$dateKey?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                            <input type="text" value="<?php echo $price_json; ?>" class="price-json-<?php echo $rate->id.'-'.$dateKey?>" hidden>

                                            <label>
                                                <a href="javascript:;" data-placement="left" class="item-total-<?php echo $rate->id.'-'.$dateKey?> total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                                <span class="hide"><?php echo $icelandTotal?></span>
                                            </label>
                                            <lable class="item-total-nights-price-<?php echo $rate->id.'-'.$dateKey?>"> 0</label>
                                        </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            <?php
                            }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100">
                        <?php
                                // $flag_to_show_all_dates = true;
                                // if(count($combine_dates_arr) == count($test_display_overridden_rates_arr))
                                // {
                                //     $flag_to_show_all_dates = false;
                                // }
                                foreach ($test_display_overridden_rates_arr as $arrKey => $display_date) 
                                {
                                    
                                    // if($flag_to_display_all_dates && $arrKey == 0)
                                    // {
                                    //     echo $display_date;
                                    // }
                                    // else
                                    // {
                                   // date('d/m/Y',strtotime($value)).$display_capacity_prining_values
                                        echo  '<span style="color:red">'.$display_date.'</span><br>';;
                                    // }
                                    
                                }
                            
                        ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                <?php
                }
            ?>
            <?php
            }?>

            <!-- // OverRidden Rates Functionality // -->

            <?php /*
            foreach ($availableOverriddenRates as $key => $value) 
            {
                $overriddencombine_dates_arr = '';
             // foreach ($availableOverriddenRates as $dateKey => $rates) 
                foreach ($value as $dateKey => $rates) 
                {
                    $dateFlag = 1;

                    if(!empty($mergeOverriddenDatesArr))
                    {
                        // echo "<pre>";
                        // print_r($mergeOverriddenDatesArr);
                        // exit();
                        
                        if(!empty($overriddencombine_dates_arr) && in_array($dateKey, $overriddencombine_dates_arr))
                        {
                            $dateFlag = 0;
                        }
                        else
                        {
                            $new_flag = false;
                            foreach ($mergeOverriddenDatesArr as $key => $value) 
                            {
                                foreach ($value as $rateKeys => $dateValues) 
                                {
                                    // echo "<pre>";
                                    // print_r($dateValues);
                                    // exit();
                                    if(in_array($dateKey, $dateValues) && count($dateValues)>1)
                                    {
                                        
                                        $overriddencombine_dates_arr = $dateValues;
                                    //     echo "<pre>";
                                    // print_r($overriddencombine_dates_arr);
                                    // exit();
                                        $new_flag = true;
                                        break;
                                    }
                                    else
                                    {
                                        $overriddencombine_dates_arr = [];
                                    }
                                }
                                if($new_flag)
                                {
                                    break;
                                }
                                // echo "<pre>";
                                // print_r($overriddencombine_dates_arr);
                                
                            }

                            // exit();
                        }

                    }
                    // echo "<pre>";
                    // print_r($overriddencombine_dates_arr);
                    // exit();

                    if($dateFlag)
                    {
                    ?>
                        <table class="table table-bordered table-<?php echo $item->id?>" date="<?php echo $dateKey;?>">
                            <thead>
                                <tr>
                                    <strong>
                                        <th width="13%">Date</th>
                                        <th width="87%" style="color:red;">Overridden Available Rates</th>
                                    </strong>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <?php
                                        $item_date = '';
                                        if(!empty($overriddencombine_dates_arr))
                                        {
                                            $dateKey = $overriddencombine_dates_arr[0];
                                            foreach ($overriddencombine_dates_arr as $key => $value) 
                                            {
                                                echo date('d/m/Y',strtotime($value)).'<br>';
                                            }
                                            $item_date = implode(',', $overriddencombine_dates_arr);
                                        }
                                        else
                                        {
                                            echo date('d/m/Y',strtotime($dateKey)).'<br>';
                                            $item_date = $dateKey;
                                        }
                                    ?>
                                    <br>

                                    <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $item->id?>" class="form-control item-quantity item-quantity-<?php echo $item->id?> item-quantities-<?php echo $item->id.'-'.$dateKey?>" value="1">

                                    <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $item->id?>" class="form-control item-date-<?php echo $item->id.'-'.$dateKey?>" value="<?php echo $item_date ?>">
                                </td>
                                <td>
                                <?php
                                foreach ($rates as $key => $rate)
                                {
                                    // ***************** add condition for checkin ******************* //
                                    $rate_row = $key;
                                    $rate = Rates::findOne(['id' => $rate->rate_id]);
                                    $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $rate->id]);


                                    $checkin_allowed = explode(';', $rate->check_in_allowed);
                                    $checkout_allowed = explode(';', $rate->check_out_allowed);

                                    if(isset($remove_rate_conditions) && $remove_rate_conditions == 1)
                                    {
                                        $rate_conditions_flag = 1;
                                    }
                                    else
                                    {
                                        if(BookingsItems::RateAllowedQualifies($arrival_date,$departure_date,$rate->rate_allowed) && in_array($arrival_day-1, $checkin_allowed) && in_array($departure_day-1, $checkout_allowed))
                                        {
                                            $rate_conditions_flag = 1;
                                        }
                                        else
                                        {
                                            $rate_conditions_flag = 0;
                                        }
                                    }
                                    ?>

                                    <div class="table-scrollable">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <strong>
                                                        <th width="20%">Rate</th>
                                                        
                                                        <?php
                                                            if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                            {  
                                                                echo '<th width="15%">Capacity Pricing </th>';
                                                            }
                                                            else if($rate->destination->pricing == 0)
                                                            {
                                                                echo '<th width="10%">Adults</th><th width="10%">Children</th>';
                                                            }
                                                        ?>
                                                        <?php if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                        { ?>
                                                        <th width="10%">Adults/Children</th>
                                                        <?php } ?>
                                                        <th width="22%">Available Discounts</th>
                                                        <th width="28%">Upsell Items</th>
                                                        <th width="10%">Total </th>
                                                    </strong>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            <tr>
                                                <td  style="min-width:120px;">
                                                    <input type="text" value="<?php echo  $rate->name?>" hidden>
                                                    <?php echo  $rate->name.'<br><br>'?>

                                                    <input type="text" value="<?php echo $item->id.':'.$rate->id.':'.$dateKey?>" class="helper-field helper-field-<?php echo $item->id?> helper-field-<?php echo $item->id.'-'.$dateKey?>" hidden>
                                                </td>

                                                <?php

                                                if($rate_conditions_flag)
                                                {
                                                    // ************* capacity pricing ************ //

                                                    if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                    {
                                                    ?>
                                                        <td date="<?php echo $dateKey; ?>" class="capacity-pricing-box capacity-pricing-box-<?php echo $item->id.'-'.$dateKey?>">
                                                    <?php

                                                        $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $rate->id])->all();

                                                        if(!empty($rates_capacity_pricing))
                                                        {
                                                            foreach ($rates_capacity_pricing as $key => $value)
                                                            {
                                                                if($value->person_no >=1 && $value->person_no<=4)
                                                                {
                                                                    if($key==0)
                                                                    {
                                                                        $value->price = $rate_override->single;
                                                                    }
                                                                    if($key==1)
                                                                    {
                                                                        $value->price = $rate_override->double;
                                                                    }
                                                                    if($key==2)
                                                                    {
                                                                        $value->price = $rate_override->triple;
                                                                    }
                                                                    if($key==3)
                                                                    {
                                                                        $value->price = $rate_override->quad;
                                                                    }
                                                                    $word =  Rates::$numberWords[$value->person_no-1];
                                                                    $checked = 0;

                                                                    $price = $value->price;
                                                                    $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");

                                                                    $icon_user = '';

                                                                    for ($i=1; $i <= $value->person_no ; $i++) 
                                                                    { 
                                                                        $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                                    }
                                                                    ?>
                                                                    <label style="width: 100%">
                                                                        <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $value->person_no?>" id="<?php echo $item->id.':'.$rate->id.':'.$value->person_no?>" class="capacity-pricing capacity-pricing-<?php echo $item->id?> capacity-pricing-<?php echo $item->id.'-'.$dateKey?> cpricing-<?php echo $rate->id?> cpricing-<?php echo $rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $item->id.'-'.$dateKey.'-'.$rate_row?>" value="<?php echo $value->person_no.' : '.$price?>" <?php echo $Options[$checked]?>> <?php echo $icon_user?>
                                                                        <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$item->id.':'.$rate->id.':'.$value->person_no?>" value="<?=$value->price?>" date="<?php echo $dateKey?>" style="width: 50%;float: right;text-align:right;" data="<?php echo $value->person_no?>">
                                                                    </label><br>
                                                                    <?php
                                                                }
                                                            } 
                                                        }
                                                        echo '</td>'; ?>
                                                        <td style="min-width:20px;">
                                                            <label>AD</label>
                                                            <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    $adult_count = isset($item->max_adults)?$item->max_adults:10;
                                                                    for ($i=0; $i <= $adult_count ; $i++) 
                                                                    { 
                                                                        $selected = 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <br>
                                                            <label>CH</label>
                                                            <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    $children_count = isset($item->max_children)?$item->max_children:10;
                                                                    for ($i=0; $i <= $children_count ; $i++) 
                                                                    { 
                                                                        $selected = 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>

                                                   <?php }

                                                    else if($rate->destination->pricing == 0)  // ************* First/Additional ************ //
                                                    {
                                                    ?>
                                                        <td style="min-width:50px;">
                                                            <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    for ($i=0; $i <= 20 ; $i++) 
                                                                    { 
                                                                        $selected = 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>

                                                        <td style="min-width:50px;">
                                                            <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    for ($i=0; $i <= 20 ; $i++) 
                                                                    { 
                                                                        $selected = 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>
                                                    <?php
                                                    }
                                                }
                                                else
                                                {
                                                ?>
                                                    <td colspan="5">
                                                        <span class="label label-danger"> Note! </span>
                                                        <span>&nbsp; Rate did not match with the conditions (Rate Allowed /Check-in Allowed / Checkout Allowed). </span>
                                                    </td>
                                                <?php
                                                } 
                                                ?>

                                            <?php

                                            $max_extra_beds = $item->max_extra_beds;
                                            $bed_type_people = 0;

                                            $selected_upsell_extra_bed_quantity = '';
                                            $selected_discount_code_extra_bed_quantity = '';

                                            if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                            {
                                                $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $item->id])->all();

                                                if(!empty($itemBedTypesModel))
                                                {
                                                    foreach ($itemBedTypesModel as $key => $value) 
                                                    {
                                                        $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                                    }
                                                }
                                            }

                                            if($rate_conditions_flag)
                                            {
                                                // *********** Discount Codes ********** //

                                                $checked_discount_codes = '';

                                                echo $this->render('discount_codes',
                                                [
                                                    'dateKey' => $dateKey,
                                                    'rates_id' => $rate->id,
                                                    'checked_discount_codes' => $checked_discount_codes,
                                                    'bed_type_people' => $bed_type_people,
                                                    'max_extra_beds' => $max_extra_beds,
                                                    'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                                    // 'model_id' => $model->id,
                                                    // 'bookedDate' =>$bookedDate,
                                                ]);

                                                // *********** Upsell Items ********** //

                                                $checked_upsell_items = '';

                                                echo $this->render('upsell_items',
                                                [
                                                    'dateKey' => $dateKey,
                                                    'rates_id' => $rate->id,
                                                    'checked_upsell_items' => $checked_upsell_items,
                                                    'max_extra_beds' => $max_extra_beds,
                                                    'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                                    // 'model_id' => $model->id,
                                                    // 'bookedDate' =>$bookedDate,
                                                ]);
                                            }

                                            $orgTotal = 0;
                                            $icelandTotal = 0;
                                            $tooltips_title = 'Item Price';

                                            ?>

                                            <td>
                                                <input class="item-total-input item-total-input-<?php echo $rate->id.'-'.$dateKey?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                                <input type="text" value="<?php echo $price_json; ?>" class="price-json-<?php echo $rate->id.'-'.$dateKey?>" hidden>

                                                <label>
                                                    <a href="javascript:;" data-placement="left" class="item-total-<?php echo $rate->id.'-'.$dateKey?> total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                                    </a>
                                                    <span><?php echo $icelandTotal?></span>
                                                </label>
                                                <lable class="item-total-nights-price-<?php echo $rate->id.'-'.$dateKey?>"></label>
                                            </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                <?php
                                }
                                ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    <?php
                    }
            ?>
            <?php
               }
            }
            */
            ?>

            <!-- // OverRidden Rates Functionality End// -->


            <?php     
            echo '</div></div></div></div></div><br>';
        }
    }
    /*else
    {
        echo '<span class="label label-danger"> Whoops! </span> <span>&nbsp;  No Rate Found. </span>';
    }*/
}

echo '</div>';

if(!$record_flag)
{
    echo '<div class="capacity-error alert-danger alert fade in">No Item found.</div>';
}
?>

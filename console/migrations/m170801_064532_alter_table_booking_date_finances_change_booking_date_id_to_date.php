<?php

use yii\db\Migration;

class m170801_064532_alter_table_booking_date_finances_change_booking_date_id_to_date extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_date_finances`   
                          CHANGE `booking_date_id` `date` DATE NULL,
                          DROP FOREIGN KEY `booking_date_finances_ibfk_1`;
                        ');
    }

    public function down()
    {
        echo "m170801_064532_alter_table_booking_date_finances_change_booking_date_id_to_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

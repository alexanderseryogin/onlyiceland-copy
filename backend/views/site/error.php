<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $exception->getName() . " (" . $exception->statusCode . ")";
// echo "<pre>";
// print_r($exception);
// exit();
?>
<div class="site-error">

    <div class="page-content">
        
        <div class="row">
            <div class="col-md-12 page-500">
                <div class=" number font-red"> <?= $exception->statusCode ?> </div>
                <div class=" details">
                    <h3>Oops! Something went wrong.</h3>
                    <p>Unable to resolve the request. We can not find the page you're looking for. 
                        <br/> </p>
                    <p>
                        <a href="<?= Url::to(['site/index']) ?>" class="btn red btn-outline"> Return home </a>
                        <br> </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Destinations;
use common\models\BookableItems;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\BookingStatuses;
use yii\web\View;
use yii\helpers\Url;
use common\models\Rates;
use common\models\BookingDates;
use common\models\BookingTypes;
use kartik\date\DatePicker;
use common\models\BookableItemsNames;
use common\models\TravelPartner;



/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingsItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bookings');
$this->params['breadcrumbs'][] = $this->title;
$update_page_url = Url::to(['/bookings/update']);
$unset_url = Url::to(['/bookings/unset-date-range-session']);
$update_bookings_flag_late_checkin_url = Url::to(['/bookings/update-bookings-flag-late-checkin-gridview']);
$check_destination_housekeeping = Url::to(['/bookings/has-housekeeping']);

$Checkbox = [
    0 => '',
    1 => 'checked'
];

$BookingTypes = [
    0 => 'Active',
    1 => 'Deleted'
];

// ***************** show or hide filters div **************** //

if($filter_flag)
{
    $this->registerJs("
            $('#filters_div').removeClass('hide-filters');
            $('#filters_div').addClass('show-filters');");
}

// ***************** show or hide group field **************** //

if(!$group_flag)
{
    $this->registerJs("
            $('.group-col').addClass('hide');");
}
else
{
    $this->registerJs("
            $('.group-col').removeClass('hide');");
}

if(!$booking_ref_flag)
{
    $this->registerJs("
            $('.booking_ref-col').addClass('hide');");
}
else
{
    $this->registerJs("
            $('.booking_ref-col').removeClass('hide');");
}

// ***************** set selected value of date range picker **************** //

$session = Yii::$app->session;
$session->open();

$bookingDates = BookingDates::find()->orderBy('date DESC')->one();
if(!empty($bookingDates))
{
    $last_booking_date = $bookingDates->date;
}
else
{
    $last_booking_date = date('Y-m-d');
}


if(isset($session[Yii::$app->controller->id.'-grid-start-date']) && !empty($session[Yii::$app->controller->id.'-grid-start-date']))
{
    $startDate =  date('d-m-Y',strtotime($session[Yii::$app->controller->id.'-grid-start-date']));
}
else
{
    $startDate = date('d-m-Y');
}

// $this->registerJs("
//     var start = '$startDate';
// ");

?>

<style type="text/css">
    .show-filters{ display: block; }
    .hide-filters{ display: none; }
    tr:hover{
        cursor: pointer;
    }

    .grid-view .filters input, .grid-view .filters select {
        width: 65px;
    }

    .grid-view td {
        white-space: pre-wrap;
    }

</style>

<script type="text/javascript">
    var server_group_flag = '<?php echo $group_flag ?>';
    var server_booking_ref_flag = '<?php echo $booking_ref_flag ?>';
</script>

<div class="bookings-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BOOKING'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
        <div class="pull-right" style="margin: 8px;">
            Filters &nbsp;<input type="checkbox" id="filters_switch" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size="mini" <?php echo $Checkbox[$filter_flag]  ?>>
        </div>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <div id="booking-range-picker" class="tooltips btn btn-default" data-container="body" data-placement="bottom" data-original-title="Change Booking date range">
        <i class="icon-calendar"></i>&nbsp;
        <span class="thin uppercase hidden-xs"></span>&nbsp;
        <i class="fa fa-angle-down"></i>
    </div> -->
    <?php $form = ActiveForm::begin([
        'id' => 'booking_form',
    ]);?>

        <div class="row">
            <div class="col-md-2">
                <input name='start_date' id="booking-range-picker" class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=$startDate?>">
            </div>
            <div class="col-md-1" style="margin-left: -90px;margin-top:1px;">
                 <a href="#" id="refresh_btn" class="btn btn-default btn-square"><i class="fa fa-refresh" aria-hidden="true" style="float: right;"></i></a>
            </div>

            <div class="btn-group col-sm-4">
                <button type="button" class="btn btn-default btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu " role="menu">
                    <li>
                        <a href="javascript:void(0)" id="select_all">Select All</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" id="deselect_all">Deselect All</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" id="delete_booking">Delete</a>
                    </li>
                    <li class="divider"> </li>
                    <?php foreach ($booking_flags as $key => $value): ?>
                        <li style="background-color: <?=$value->background_color?>;">
                            <a style="color: <?=$value->text_color?>;" class="flags-options" data-key="<?=$value->id?>" href="javascript:void(0)"><?=$value->label?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- <div class="form-group">
                <div class="col-md-3">
                    <select class="bs-select form-control">
                        <option value="deselect_all">Deselect All</option>
                        <option value="select_all">Select All</option>
                        <option value="delete">Delete</option>
                        <option data-divider="true"></option>
                        <option>Barbecue Sauce</option>
                        <option>Salad Dressing</option>
                        <option>Tabasco</option>
                        <option>Salsa</option>
                    </select>
                </div>
            </div> -->
            
           <!--  <span class="help-block"> Select date </span> -->
        </div>

        <div class="hide-filters" id="filters_div">
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-sliders"></i>
                                <span class="caption-subject bold uppercase"> Booking Filters </span>
                            </div>
                            <div class="inputs">
                                <div class="portlet-input input-inline input-medium">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <input class="btn btn-warning pull-right" type="submit" value="Update">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php
                                        if(!empty($booking_status))
                                        {
                                            echo '<h4>Booking Status</h4>';
                                            foreach ($booking_status as $key => $value) 
                                            {
                                                $checked = 0;

                                                if(!empty($selected_status) && in_array($value->id, $selected_status))
                                                {
                                                    $checked = 1;
                                                }
                                                echo '<label><input name="Bookings[booking_status][]"  value="'.$value->id.'" style="margin-left:10px;" type="checkbox" '.$Checkbox[$checked].'>&nbsp;'.$value->label.'</label>';
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <h4>Booking Group </h4>
                                    <input style="display: inline;" type="checkbox" name="Bookings[group_switch]" id="group_switch" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size="mini" <?php echo $Checkbox[$group_flag]  ?>>
                                </div>
                                <div class="col-sm-3">
                                    <h4>Imported Booking # </h4>
                                    <input style="display: inline;" type="checkbox" name="Bookings[booking_ref_switch]" id="group_switch" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size="mini" <?php echo $Checkbox[$booking_ref_flag]  ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Portlet PORTLET-->
                </div>
            </div>
        </div>
        <br>

        <input type="text" id="rangepicker_status" name="Bookings[daterangepicker]" value="0" hidden>
    <?php ActiveForm::end(); ?>


<?php Pjax::begin(['id' => 'bookings-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    

<?= GridView::widget([
        'id' => 'bookings_gridview',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions' => function($data)
                {
                    return  ['style' => 'width: 1%'];
                }
                // you may configure additional properties here
            ],
            [
                'attribute' => 'housekeeping_status_id',
                'format' =>'raw',
                'label' => 'HK',
                'value' => function($data)
                {
                    
                    if($data->provider->use_housekeeping == NULL || $data->provider->use_housekeeping = 0)
                    {
                        return '';
                    }
                    else
                    {
                        $html ='';
                        if(!empty($data->bookingHousekeepingStatus))
                        {
                            $check = 0;
                            $not_ready_statuses = '';
                            foreach ($data->bookingHousekeepingStatus as $value) 
                            {

                                $not_ready_statuses = $not_ready_statuses.$value->housekeepingStatus->label.'<br>';
                                if($value->housekeeping_status_id == 1)
                                {
                                    $check = 1;
                                    break;
                                }
                            }

                            // echo "<pre>";
                            // print_r($not_ready_statuses);
                            // exit();
                            if($check)
                            {
                                 $html.='<i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color:green;"></i>';
                            }
                            else
                            {
                                 $html.='<a><span data-html="true" data-toggle="tooltip" title="'.$not_ready_statuses.'" class="fa fa-times-circle-o fa-2x" aria-hidden="true" style="color:red;"></span></a>';
                            }
                            
                        }
                        else
                        {
                            $not_ready_statuses = 'No status given';
                            $html.='<a><span data-html="true" data-toggle="tooltip" title="'.$not_ready_statuses.'" class="fa fa-times-circle-o fa-2x" aria-hidden="true" style="color:red;"></span></a>';
                        }
                        return $html;
                    }
                        
                },
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'text-align:center;vertical-align:middle;width:1%;margin-top:7px;color:'.$color.'; background:'.$backgroundColor.';', 'class' => 'has_housekeeping'];
                },
                'headerOptions' => ['class' => 'has_housekeeping']
                //'headerOptions' =>['style' => 'width:30px;'],
            ],
            [
                'attribute' => 'id',
                'format' =>'raw',
                'label' => 'Number',
                'value' => function($data)
                {
                    $first_booking_date = BookingDates::findOne(['booking_item_id' => $data->id ]);
                    $html='';
                    // if(!empty($data->comments) && $data->comments!=NULL)
                    // {
                    //     //$path = Yii::getAlias('@web').'/../uploads/icons/comments_red.PNG';
                    //     $html.='<i id="'.$data->id.'" class="fa fa-commenting-o fa-3x" style="color:red;margin-top:7px;" data="'.$data->comments.'"  data-toggle = "tooltip" title ="'.$data->comments.'",> </i>';

                    // }

                    return $html.$data->id.'<br>'.$first_booking_date->no_of_adults.'/'.$first_booking_date->no_of_children;
                },
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'text-align:right;width:2%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
                //'headerOptions' =>['style' => 'width:30px;'],
            ],
            [
                'attribute' => 'group_name',
                //'contentOptions' => ['class' => 'hide'],
                //'headerOptions' => ['class' => 'hide'],
                //'filterOptions' => ['class' => 'hide'],
                'label' => 'Group',
                'value' => function($data)
                {
                    if(!isset($data->bookingItemGroups->bookingGroup))
                    {
                        echo "<pre>";
                        print_r($data);
                        exit();
                    }
                    return isset($data->bookingItemGroups)?$data->bookingItemGroups->bookingGroup->group_name:'';
                },
                'headerOptions' =>[ 'class' => 'group-col'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:5%; color:'.$color.'; background:'.$backgroundColor.';', 'class' => 'group-col' ];
                },
                'filterOptions' => ['class' => 'group-col'],
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
            ],
            [
                'attribute' => 'reference_no',
                //'contentOptions' => ['class' => 'hide'],
                //'headerOptions' => ['class' => 'hide'],
                //'filterOptions' => ['class' => 'hide'],
                'label' => 'Imported #',
                'value' => function($data)
                {
                    return (empty($data->reference_no)?'':$data->reference_no);
                },
                'headerOptions' =>[ 'class' => 'booking_ref-col'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:5%; color:'.$color.'; background:'.$backgroundColor.';', 'class' => 'booking_ref-col' ];
                },
                'filterOptions' => ['class' => 'booking_ref-col'],
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Status',
                'value' => function($data)
                {
                    if(isset($data->status->label) && !empty($data->status->label))
                        return $data->status->label;
                    else
                        return '';
                },
                /*'filter' => Html::dropDownList(
                    'BookingsItemsSearch[status_id]', 
                    $searchModel['status_id'], 
                    ArrayHelper::map(BookingStatuses::find()->all(),'id','label'), 
                    [
                        'prompt' => 'Select a Status', 
                        'id'=>'status_dropdown',
                        'style' => 'width:100%',
                    ]),*/
                'contentOptions' => function($data)
                {
                    if(!empty($data->status_id))
                        return ['style' => 'width:1%; color:'.$data->status->text_color.'; background:'.$data->status->background_color.';' ];
                    else
                       return ['style' => 'width:1%;' ]; 
                },
                //'headerOptions' =>['style' => 'width:75px;'],
            ],
            [
                'attribute' => 'flag_id',
                'label' => 'Flag',
                'value' => function($data)
                {
                    if(isset($data->flag->label) && !empty($data->flag->label))
                        return $data->flag->label;
                    else
                        return '';
                },
                /*'filter' => Html::dropDownList(
                    'BookingsItemsSearch[flag_id]', 
                    $searchModel['flag_id'], 
                    ArrayHelper::map(BookingTypes::find()->all(),'id','label'), 
                    [
                        'prompt' => 'Select a Flag', 
                        'id'=>'flag_dropdown',
                        'style' => 'width:100%',
                    ]),*/
                'contentOptions' => function($data)
                {
                    if(!empty($data->flag_id))
                        return ['style' => 'width:1%; color:'.$data->flag->text_color.'; background:'.$data->flag->background_color.';' ];
                    else
                       return ['style' => 'width:1%;' ];
                },
                //'headerOptions' =>['style' => 'width:75px;'],
            ],
            [
                'attribute' => 'provider_id',
                'label' => 'Destination',
                'value' => function($data)
                {
                    return $data->provider->name;
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[provider_id]', 
                    $searchModel['provider_id'], 
                    ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Destination', 
                        'id'=>'provider_dropdown',
                        'style' => 'width:100%',
                    ]),
                //'headerOptions' =>['style' => 'width:120px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:8%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'attribute' => 'item_id',
                'label' => 'Item',
                'value' => function($data)
                {
                    return isset($data->item->itemType->name) ? $data->item->itemType->name : 'N/A';
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[item_id]', 
                    $searchModel['item_id'], 
                    ArrayHelper::map(BookableItems::find()->where(['provider_id' => $searchModel['provider_id']])->all(),'id','itemType.name'), 
                    [
                        'prompt' => 'Select an Item', 
                        'id'=>'item_dropdown',
                        'style' => 'width:100%',
                    ]),
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:7.5%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
                //'headerOptions' =>['style' => 'width:75px;'],
            ],
            [
                'attribute' => 'item_name_id',
                'label' => 'Item Name',
                'format' =>'raw',
                'value' => function($data)
                {
                    $html='';
                    if(!empty($data->comments) && $data->comments!=NULL)
                    {
                        //$path = Yii::getAlias('@web').'/../uploads/icons/comments_red.PNG';
                        $html.='<i id="'.$data->id.'" class="fa fa-commenting-o fa-2x" style="color:red;margin-top:7px;line-height: 25px; !important" data="'.$data->comments.'"  data-toggle = "tooltip" title ="'.$data->comments.'" > </i>';

                    }
                    $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                    if(!empty($data->item_name_id))
                        return $data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'').' '.$html;
                    else
                        return '- - - - -'.' '.$html;
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[item_name_id]', 
                    $searchModel['item_name_id'], 
                    ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $searchModel['item_id']])->all(),'id','item_name'), 
                    [
                        'prompt' => 'Select an Item Name', 
                        'id'=>'item_name_dropdown',
                        'style' => 'width:100%',
                    ]),
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:6%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
                //'headerOptions' =>['style' => 'width:110px;'],
            ],
            [
                'attribute' => 'arrival_date',
                'label' => 'Arr',
                'format' => 'raw',
                'value' => function($data)
                {
                    $bookingDates = $data->bookingDatesSortedAndNotNull;
                    return date('d.m.Y',strtotime($bookingDates[0]->date)).'<br>'.date('l',strtotime($bookingDates[0]->date));
                },
                //'headerOptions' =>['style' => 'width:80px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:1%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'attribute' => 'departure_date',
                'label' => 'Dep',
                'format' => 'raw',
                'value' => function($data)
                {
                    $bookingDates = $data->bookingDatesSortedAndNotNull;
                    return date('d.m.Y',strtotime("+1 day", strtotime($bookingDates[count($bookingDates)-1]->date))).'<br>'.date('l',strtotime("+1 day", strtotime($bookingDates[count($bookingDates)-1]->date)));
                },
                //'headerOptions' =>['style' => 'width:80px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:1%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'label' => '#',
                'value' => function($data)
                {
                    $date1=date_create($data->departure_date);
                    $date2=date_create($data->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a");
                    return $difference;
                },
                //'headerOptions' =>['style' => 'width:30px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:1%; text-align:right;color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'label' => 'Guest',
                'value' => function($data)
                {
                    $bookingDateModel = $data->FirstBookingDate();
                    $guest_string = '';

                    if(!empty($bookingDateModel->user_id))
                    {
                        $guest_string .= $bookingDateModel->user->profile->first_name.' ';
                        $guest_string .= $bookingDateModel->user->profile->last_name;
                    }
                    else
                    {
                        $guest_string .= $bookingDateModel->guest_first_name.' ';
                        $guest_string .= $bookingDateModel->guest_last_name;
                    }
                    return $guest_string;
                },
                //'headerOptions' =>['style' => 'width:70px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:5%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'label' => 'Balance',
                'format' => 'raw',
                'value' => function($data)
                {
                    if(isset($data->bookingItemGroups->bookingGroup))
                    {
                        // echo "<pre>";
                        // print_r(($data->bookingItemGroups->bookingGroup->balance != null)?'null':'not null');
                        // exit();
                        return '<span>B '.Yii::$app->formatter->asDecimal($data->balance).'<span><br><span>GB '.(($data->bookingItemGroups->bookingGroup->balance !== null)?Yii::$app->formatter->asDecimal($data->bookingItemGroups->bookingGroup->balance).'</span>':'(<i>not set</i>)</span>');
                    }
                    return '<span>B '.Yii::$app->formatter->asDecimal($data->balance).'<span><br><span>GB 0<span>';
                },
                //'headerOptions' =>['style' => 'width:70px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:2%; text-align:right; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'attribute' => 'travel_partner_id',
                'label' => 'Referrer',
                'value' => function($data)
                {
                    if(isset($data->travelPartner->travelPartner->company_name) && !empty($data->travelPartner->travelPartner->company_name))
                        return $data->travelPartner->travelPartner->company_name;
                    else
                        return '- - - - - - -';
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[travel_partner_id]', 
                    $searchModel['travel_partner_id'], 
                    ArrayHelper::map(TravelPartner::find()->all(),'id','company_name'), 
                    [
                        'prompt' => 'Select Referrer', 
                        'id'=>'travel_partner_dropdown',
                        'style' => 'width:100%',
                    ]),
                //'headerOptions' =>['style' => 'width:90px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:5%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'format' => 'raw',
                'attribute' => 'created_at',
                'label' => 'Booked',
                'value' => function($data)
                {
                        return '<span class="pull-right">'.date('d.m.Y',$data->created_at)."</span><br><span class='pull-right'>".date('H:i:s',$data->created_at)."</span>";
                },
                // 'filter' => Html::dropDownList(
                //     'BookingsItemsSearch[travel_partner_id]', 
                //     $searchModel['travel_partner_id'], 
                //     ArrayHelper::map(TravelPartner::find()->all(),'id','company_name'), 
                //     [
                //         'prompt' => 'Select Referrer', 
                //         'id'=>'travel_partner_dropdown',
                //         'style' => 'width:100%',
                //     ]),
                //'headerOptions' =>['style' => 'width:90px;'],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'width:3%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                //'headerOptions' =>['style' => 'width:70px;'],
                'contentOptions' =>['style' => 'width:4%;'],
                'template' => '{late-checkin}{receipt}{receipt-group}{update-group}{delete}',

                'buttons' => [
                    'update-group' => function ($url, $model) {
                            return Html::a('<span class="fa fa-object-group" style="margin-left:5px;"></span>', Url::to(['update-group', 'id'=>isset($model->bookingItemGroups->booking_group_id)?$model->bookingItemGroups->booking_group_id:0]), 
                            [
                                'class'   => 'updateGroup',
                                'title' => 'Update Group',
                                'aria-label' => 'Update-Group',
                                'data-pjax' => '0',
                                'data-toggle' => 'tooltip'
                            ]);
                        },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash" style="margin-left:5px;"></span>', Url::to(['delete', 'id'=>$model->id]), 
                        [   
                            'class'   => 'deleteBookings',
                            'title' => 'Delete',
                            'aria-label' => 'Delete',
                            'data-pjax' => '0',
                            'data-toggle' => 'tooltip'
                        ]);
                    },
                    'late-checkin' => function ($url, $model) {
                        return Html::a('<span class="fa fa-print print" style="margin-left:5px;z-index:20;"></span>', Url::to(['late-checkin', 'id'=>$model->id]), 
                        [
                            'title' => 'Print Notes',
                            'aria-label' => 'print-notes',
                            'data-pjax' => '0',
                            'target' => '_blank',
                            'data-toggle' => 'tooltip',
                            'class' => 'late-checkin-print cancel',
                            'data-key' => $model->id
                        ]);
                    },
                    'receipt' => function ($url, $model) {
                        return Html::a('<span class="fa fa-dollar print" style="margin-left:5px;z-index:20;"></span>', Url::to(['print-receipt', 'id'=>$model->id]),
                        [
                            'title' => 'Print Receipt',
                            'aria-label' => 'print-receipt',
                            'data-pjax' => '0',
                            'target' => '_blank',
                            'data-toggle' => 'tooltip',
                            'data-key' => $model->id
                        ]);
                    },
                    'receipt-group' => function ($url, $model) {
                            return Html::a('<span class="fa fa-list-alt print" style="margin-left:5px;z-index:20;"></span>', Url::to(['print-receipt-group', 'id'=>isset($model->bookingItemGroups->booking_group_id)?$model->bookingItemGroups->booking_group_id:0]),
                            [
                                'title' => 'Print Group Receipt',
                                'aria-label' => 'print-receipt-group',
                                'data-pjax' => '0',
                                'target' => '_blank',
                                'data-toggle' => 'tooltip',
                            ]);
                        },
                    ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php

$this->registerJs("
jQuery(document).ready(function() {

    $('#provider_dropdown').on('change',function(){
        
        CheckHouseKeeping();
    });
    CheckHouseKeeping();

    function CheckHouseKeeping()
    {
        id = $('#provider_dropdown').val();
        console.log('provider id : '+id);
        if(id != '')
        {
            $.ajax(
            {
                type: 'POST',
                url: '$check_destination_housekeeping',
                data: {id:id},
                success: function(result)
                {
                    // alert(result);
                    if(result == 0)
                    {
                        $('.has_housekeeping').addClass('hide');
                        $('table tr:nth-child(2) td:nth-child(2)').addClass('hide');
                    }
                    else
                    {
                        $('.has_housekeeping').removeClass('hide');
                        $('table tr:nth-child(2) td:nth-child(2)').removeClass('hide');
                    }
                },
            });
        }
        else
        {
            $('.has_housekeeping').removeClass('hide');
            $('table tr:nth-child(2) td:nth-child(2)').removeClass('hide');
        }
    }
    $('.status_icon').on('hover',function(){

    });

    $('[data-toggle=\'tooltip\']').tooltip(); 
    $('#filters_switch').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            $('#filters_div').removeClass('hide-filters');
            $('#filters_div').addClass('show-filters');
        }
        else
        {
            $('#filters_div').removeClass('show-filters');
            $('#filters_div').addClass('hide-filters');
        }
    });

    /*$('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#item_dropdown').select2({
        placeholder: \"Select an Item\",
        allowClear: true
    });

    $('#item_name_dropdown').select2({
        placeholder: \"Select an Item Name\",
        allowClear: true
    });

    $('#rates_dropdown').select2({
        placeholder: \"Select a Rate\",
        allowClear: true
    });

    $('#user_dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true
    });

    $('#status_dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#flag_dropdown').select2({
        placeholder: \"Select a Flag\",
        allowClear: true
    });*/

    $('#bookings-gridview').on('pjax:end', function() 
    {
        /*$('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
        });

        $('#item_dropdown').select2({
            placeholder: \"Select an Item\",
            allowClear: true
        });

        $('#item_name_dropdown').select2({
            placeholder: \"Select an Item Name\",
            allowClear: true
        });

        $('#rates_dropdown').select2({
            placeholder: \"Select a Rate\",
            allowClear: true
        });

        $('#user_dropdown').select2({
            placeholder: \"Select a User\",
            allowClear: true
        });

        $('#status_dropdown').select2({
            placeholder: \"Select a Status\",
            allowClear: true
        });

        $('#flag_dropdown').select2({
            placeholder: \"Select a Flag\",
            allowClear: true
        });*/

        if(server_group_flag=='0')
            $('.group-col').addClass('hide');
        else
            $('.group-col').removeClass('hide');

        if(booking_ref_flag=='0')
            $('.booking_ref-col').addClass('hide');
        else
            $('.booking_ref-col').removeClass('hide');
    });

    $(document).on('pjax:complete', function() {
        $(document).find('.select2-container').hide();

        $('.table-striped tbody tr').each(function() 
        {
            $(this).find('td').each(function(index) 
            {   
                if(index==0)
                    $(this).addClass('cancel');
            });
        });

        $(document).on('click','.late-checkin-print',function()
        {
            App.blockUI({target:'.page-wrapper', animate: !0});
            var Object = {
                            id: $(this).attr('data-key')
                        }
            $.ajax(
            {
                type: 'POST',
                url: '$update_bookings_flag_late_checkin_url',
                data: Object,
                success: function(result)
                {
                    var pageNo = $('.pagination .active a').html();
                    if(!pageNo)
                    {
                        pageNo = 1;
                    }

                    var url = window.location.href+'/index?page='+pageNo+'&per-page=10';
                    App.unblockUI('.page-wrapper');
                    $.pjax.reload({url:url,container: '#bookings-gridview',replace: false,timeout: 900000});
                },
            });
        });

        $('.late-checkin-print').parent().addClass('cancel');

    });

    $(document).on('click','.table-striped tbody tr td',function()
    {
        // console.log();
        // return false;
        //id = 
        if($(this).hasClass('cancel'))
        {

        }
        else
        {
            var pageNo = $('.pagination .active a').html();
            if(!pageNo)
            {
                pageNo = 1;
            }
            console.log('page no :'+ pageNo);
            console.log('url: '+ $(this).attr('href') +'&pageNo='+pageNo);
            $(this).attr('href',$(this).attr('href') +'&pageNo='+pageNo);

            window.location.replace('$update_page_url?id='+$(this).parent().attr('data-key')+'&pageNo='+pageNo);
        }
    });
    $('.print').parent().parent().addClass('cancel');
    $(document).on('click','#refresh_btn',function()
    {
        $.ajax(
        {
            type: 'GET',
            url: '$unset_url',
        });
    });

});",View::POS_END);

$this->registerJs('

    function CheckHouseKeeping()
    {
        id = $("#provider_dropdown").val();
        console.log("provider id : "+id);
        if(id != "")
        {
            $.ajax(
            {
                type: "POST",
                url: "'.$check_destination_housekeeping.'",
                data: {id:id},
                success: function(result)
                {
                    // alert(result);
                    if(result == 0)
                    {
                        $(".has_housekeeping").addClass("hide");
                        $("table tr:nth-child(2) td:nth-child(2)").addClass("hide");
                    }
                    else
                    {
                        $(".has_housekeeping").removeClass("hide");
                        $("table tr:nth-child(2) td:nth-child(2)").removeClass("hide");
                    }
                },
            });
        }
        else
        {
            $(".has_housekeeping").removeClass("hide");
            $("table tr:nth-child(2) td:nth-child(2)").removeClass("hide");
        }
    }
    jQuery(document).on("pjax:success", "#bookings-gridview",  function(event){
        //alert("pjax complete");
        CheckHouseKeeping();

        var gridview_id = "#bookings_gridview"; // specific gridview
        var columns = [4]; // index column that will grouping, start 1
        var group_flag = 0;

        var column_data = [];
            column_start = [];
            rowspan = [];

        for (var i = 0; i < columns.length; i++) {
            column = columns[i];
            column_data[column] = "";
            column_start[column] = null;
            rowspan[column] = 1;
        }


        var row = 1;
        var flag = 1;
        $(gridview_id+" table > tbody  > tr").each(function() {
            var row = $(this);                
            var col = 1;
            
            $(this).find("td").each(function(){
                for (var i = 0; i < columns.length; i++) {
                    if(col==columns[i]){
                        if(column_data[columns[i]] == $(this).html()){
                            $(this).remove();
                            rowspan[columns[i]]++;
                            $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                            if(flag)
                            {
                                row.prev().css("border-top","5px solid #36C6D3");
                                flag = 0;
                            }
                            row.prev().css("border-left","5px solid #36C6D3");
                            row.prev().css("border-right","5px solid #36C6D3");     
                            row.css("border-left","5px solid #36C6D3");
                            row.css("border-right","5px solid #36C6D3");
                           
                            row.css("border-bottom","5px solid #36C6D3");  
                            row.prev().css("border-bottom","");
                            
                            if(group_flag)
                            {
                                row.prev().css("border-top","5px solid #36C6D3");
                                group_flag = 0;
                            }
                        }
                        else
                        {
                            group_flag = 1;
                            column_data[columns[i]] = $(this).html();
                            rowspan[columns[i]] = 1;
                            column_start[columns[i]] = $(this);
                        }
                    }
                }
                col++;
            })
            row++;
        });
      }
    );

    var gridview_id = "#bookings_gridview"; // specific gridview
    var columns = [4]; // index column that will grouping, start 1

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    var flag = 1;
    var group_flag = 0;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var row = $(this);                
        var col = 1;
        
        $(this).find("td").each(function()
        {
            for (var i = 0; i < columns.length; i++) 
            {
                if(col==columns[i])
                {
                    if(column_data[columns[i]] == $(this).html())
                    {
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                        if(flag)
                        {
                            row.prev().css("border-top","5px solid #36C6D3");
                            flag = 0;
                        }
                        row.prev().css("border-left","5px solid #36C6D3");
                        row.prev().css("border-right","5px solid #36C6D3");     
                        row.css("border-left","5px solid #36C6D3");
                        row.css("border-right","5px solid #36C6D3");
                        
                        row.css("border-bottom","5px solid #36C6D3");  
                        row.prev().css("border-bottom","");

                        if(group_flag)
                        {
                            row.prev().css("border-top","5px solid #36C6D3");
                            group_flag = 0;
                        }
                    }
                    else
                    {
                        group_flag = 1;
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });


',View::POS_END);

$delete_bookings_url = Url::to(['/bookings/delete-bookings-gridview']);
$update_bookings_flag_url = Url::to(['/bookings/update-bookings-flags-gridview']);

$this->registerJs("
$(document).on('click','.deleteBookings',function(){

    var filters_switch = $('#filters_switch').bootstrapSwitch('state');
    console.log('url: '+ $(this).attr('href') +'&filter_flag='+filters_switch);
    var pageNo = $('.pagination .active a').html();
    if(!pageNo)
    {
        pageNo = 1;
    }
    console.log('page no :'+ pageNo);
    $(this).attr('href',$(this).attr('href') +'&filter_flag='+filters_switch+'&pageNo='+pageNo);

});

$(document).on('click','.updateBookings',function(){

    //var filters_switch = $('#filters_switch').bootstrapSwitch('state');
    //console.log('url: '+ $(this).attr('href') +'&filter_flag='+filters_switch);
    var pageNo = $('.pagination .active a').html();
    if(!pageNo)
    {
        pageNo = 1;
    }
    console.log('page no :'+ pageNo);
    console.log('url: '+ $(this).attr('href') +'&pageNo='+pageNo);
    $(this).attr('href',$(this).attr('href') +'&pageNo='+pageNo);

});

$(document).on('click','.updateGroup',function(){

    //var filters_switch = $('#filters_switch').bootstrapSwitch('state');
    //console.log('url: '+ $(this).attr('href') +'&filter_flag='+filters_switch);
    var pageNo = $('.pagination .active a').html();
    if(!pageNo)
    {
        pageNo = 1;
    }
    console.log('page no :'+ pageNo);
    console.log('url: '+ $(this).attr('href') +'&pageNo='+pageNo);
    $(this).attr('href',$(this).attr('href') +'&pageNo='+pageNo);

});

$(document).ready(function(){
     var start = '$startDate';

    $('#booking-range-picker').datepicker().val(start);
    //history.pushState(null, '', location.href.split('?')[0])
    //window.history.replaceState({}, document.title, '/' + '');

    $('#booking-range-picker').on('change', function() {
        $('#rangepicker_status').val(1);
        //alert( $('#booking-range-picker').val());
      $('#booking_form').submit();
    });
});
{
    // function cb(start, end) {
    //     $('#booking-range-picker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    // }

    // $('#booking-range-picker').daterangepicker({
    //     startDate: start,
    //     endDate: end,
    //     ranges: {
    //        'Today': [moment(), moment()],
    //        'Today to N days': [moment(), last_booking_date],
    //        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //        'This Month': [moment().startOf('month'), moment().endOf('month')],
    //        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //     }
    // }, cb);

    // cb(start, end);


    $(document).on('click','#select_all',function()
    {
        if(!$('.select-on-check-all').is(':checked'))
        {
            $('.select-on-check-all').trigger('click');
        }
    });

    $(document).on('click','#deselect_all',function()
    {
        if($('.select-on-check-all').is(':checked'))
        {
            $('.select-on-check-all').trigger('click');
        }
        else //some bookings are selected
        {
            $('.table-striped tbody tr').each(function() 
            {
                if($(this).children('td:first').find('input').is(':checked'))
                    $(this).children('td:first').find('input').prop('checked', false);
            });
        }
    });

    $(document).on('click','#delete_booking',function()
    {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');
        if(keys!='')
        {
            if(confirm('Are you sure you want to delete selected bookings?'))
            {
                App.blockUI({target:'.page-wrapper', animate: !0});
                var Object = {
                                keys : keys,
                            }
                $.ajax(
                {
                    type: 'POST',
                    url: '$delete_bookings_url',
                    data: Object,
                    success: function(result)
                    {
                        App.unblockUI('.page-wrapper');
                        $.pjax.reload({container: '#bookings-gridview',replace: false,timeout: 900000});
                    },
                });
            }
        }
        else
        {
            alert('Please select atleast one booking to delete.');
        }
    });

    $(document).on('click','.flags-options',function()
    {
        var keys = $('.grid-view').yiiGridView('getSelectedRows');
        if(keys!='')
        {
            App.blockUI({target:'.page-wrapper', animate: !0});
            var Object = {
                            keys : keys,
                            flag_id: $(this).attr('data-key')
                        }
            $.ajax(
            {
                type: 'POST',
                url: '$update_bookings_flag_url',
                data: Object,
                success: function(result)
                {
                    var pageNo = $('.pagination .active a').html();
                    if(!pageNo)
                    {
                        pageNo = 1;
                    }

                    var url = window.location.href+'/index?page='+pageNo+'&per-page=10';
                    App.unblockUI('.page-wrapper');
                    $.pjax.reload({url:url,container: '#bookings-gridview',replace: false,timeout: 900000});
                },
            });
        }
        else
        {
            alert('Please select atleast one booking to change flag.');
        }
    });

    $('.table-striped tbody tr').each(function() 
    {
        $(this).find('td').each(function(index) 
        {   
            if(index==0)
                $(this).addClass('cancel');
        });
    });

    $(document).on('click','.late-checkin-print',function()
    {
        App.blockUI({target:'.page-wrapper', animate: !0});
        var Object = {
                        id: $(this).attr('data-key')
                    }
        $.ajax(
        {
            type: 'POST',
            url: '$update_bookings_flag_late_checkin_url',
            data: Object,
            success: function(result)
            {
                var pageNo = $('.pagination .active a').html();
                if(!pageNo)
                {
                    pageNo = 1;
                }

                var url = window.location.href+'/index?page='+pageNo+'&per-page=10';
                App.unblockUI('.page-wrapper');
                $.pjax.reload({url:url,container: '#bookings-gridview',replace: false,timeout: 900000});
            },
        });
    });


}",View::POS_END);

if($sort != null)
{
    $this->registerJs("
        // alert();
        // $(document).ready(function(){
        //     var pageNo = $('.pagination .active a').html();
        //     var url = window.location.href+'/index?page='+pageNo+'&per-page=10&sort='+'".$sort."';
        //     // alert(url);
        //     $.pjax.reload({url:url,container: '#bookings-gridview',replace: false,timeout: 900000});
        // });
        


    ");
    // echo "<pre>";
    // print_r($sort);
    // exit();    
}
?>
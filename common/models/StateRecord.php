<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "state_image_text".
 *
 * @property integer $id
 * @property integer $state_id
 * @property string $image
 * @property string $description
 *
 * @property States $state
 */
class StateRecord extends \yii\db\ActiveRecord
{
    public $image_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state_image_text';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id'], 'integer'],
            [['image', 'description'], 'string'],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['image_file'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png','jpg']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'state_id' => Yii::t('app', 'State ID'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'image_file' => Yii::t('app', 'Upload an Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }
}

<?php
    use yii\web\View;
    use yii\helpers\Url;
?>

<style type="text/css">
    .align-center {
        text-align: center;
    }
    table tr th {
        text-align: center;
    }
    .table-scrollable {border: none !important; overflow-x: hidden !important;}
    .dataTables_scrollBody {overflow-x: hidden !important;}
    table.dataTable thead .sorting_desc {
        background-image: none;
    }
    table.dataTable thead .sorting_asc {
        background-image: none;
    }

    body.dragging, body.dragging * {
      cursor: move !important;
    }

    tr:hover{
        cursor: pointer;
    }

    .dragged {
      position: absolute;
      opacity: 0.5;
      z-index: 2000;
    }

</style>

<!-- BEGIN ACCORDION PORTLET-->
<div class="tab-pane" id="item_names">

    <label class=" control-label">Quantity</label>
    <div class="form-group">
        <input id="item_quantity" value="<?=$model->item_quantity ?>" type="text" name="BookableItems[item_quantity]" />
    </div>

    <div class="row">
        <div class="col-sm-6">
            <table class="table table-striped table-bordered table-hover order-column" id="bookable_item_names">
                <thead>
                    <tr>
                        <th width="10%"></th>
                        <th width="90%">Item Name</th>
                    </tr>
                </thead>
                <tbody id="item_names_body">
                    <?php if(empty($model->item_names)): ?>
                        <tr class="item-name-row">
                            <td>
                                <i class="fa fa-arrows" aria-hidden="true" style="margin: 10px;"></i>
                                <input type="text" name="BookableItems[item_names][0][item_number]" value="Item 1" hidden>
                                <input type="text" class="order" name="BookableItems[item_names][0][item_order]" value="1" hidden>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="BookableItems[item_names][0][item_name]" value="Item 1">
                            </td>
                        </tr>
                    <?php else: ?>
                        <?php
                            foreach ($model->item_names as $key => $value) 
                            {
                            ?>
                                <tr class="item-name-row">
                                    <td>
                                        <i class="fa fa-arrows" aria-hidden="true" style="margin: 10px;"></i>
                                        <input type="text" name="BookableItems[item_names][<?php echo $key?>][item_number]" value="<?php echo $value['item_number']; ?>" hidden> 
                                         <input type="text" class="order" name="BookableItems[item_names][<?php echo $key?>][item_order]" value="<?php echo $value['item_order']; ?>" hidden>   
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="BookableItems[item_names][<?php echo $key?>][item_name]" value="<?php echo $value['item_name']; ?>">
                                    </td>
                                </tr>
                            <?php
                            }
                        ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-6">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-arrows"></i>Reordering </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    
                    <p class="text-primary"> You may use arrows icon to reorder your item names 
                  </p>
                    
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="form-group">
        <?php
            if(!$model->isNewRecord)
            { ?>
                <button type="button" class="btn btn-primary common-save-btn" >Update</button>
        <?php     }
        ?>
        <a class="<?= $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?> common-submit-button" ><?=$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save') ?></a>
        <a class="btn btn-default" href="<?= Url::to(['/bookable-items']) ?>" >Cancel</a>
    </div>

</div>

<?php

$this->registerJs("
jQuery(document).ready(function() 
{
    $('#bookable_item_names').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class=\"placeholder\"/>',
        onDrop: function (item, container, _super, event) {
            item.removeClass(container.group.options.draggedClass).removeAttr(\"style\");
            $(\"body\").removeClass(container.group.options.bodyClass);

            $('.order').each(function(index,element)
            {
                console.log(index);
                $(this).val((index+1));
            });
        }
    });

});",View::POS_END);

/*$this->registerJs("

function initDataTableItemNames(tableSelector)
{
    return $(tableSelector).DataTable({
            language: {
                aria: {
                    sortAscending: \": activate to sort column ascending\",
                    sortDescending: \": activate to sort column descending\"
                },
                emptyTable: \"No data available in table\",
                info: \"Showing _TOTAL_ Item Names\",
                infoEmpty: \"No Item Names found\",
                infoFiltered: \"(filtered 1 from _MAX_ total Item Names)\",
                lengthMenu: \"_MENU_ Item Names\",
                search: \"Search:\",
                zeroRecords: \"No matching records found\"
            },
            scrollY: 300,
            scrollCollapse: true,
            //scroller: !0,
            statesave : false,
            columns: [
            ],
            order: [
                [0, \"asc\"]
            ],
            columns: [
                { 'width': '50%' },
                { 'width': '50%' }
            ],
            \"columnDefs\": [
                { \"orderable\": false, \"targets\": [0,1] }
            ],
            lengthMenu: [
                [10, 15, 20, -1],
                [10, 15, 20, \"All\"]
            ],
            paging: false,
            retrieve: true
        }); 
}

function redrawDataTableItemNames(tableSelector)
{
    var table = initDataTableItemNames(tableSelector);
    table.draw();
}

function redrawItemNamesDataTable()
{
    redrawDataTableItemNames('#bookable_item_names');
}

jQuery(document).ready(function() 
{
    initDataTableItemNames('#bookable_item_names');
    setTimeout(function()
    { 
        redrawItemNamesDataTable(); 
    }, 500);
    
});",View::POS_END);*/
?>
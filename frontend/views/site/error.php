<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $exception->getName() . " (" . $exception->statusCode . ")";
?>
  <!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
            <div class="container">
                <div class="page error-404">
                    <div class="row">
                    <div class="col-md-12">
                            <h2><i class="fa fa-exclamation-triangle"></i> <?= Html::encode($this->title) ?></h2>
                        <h4><?= nl2br(Html::encode($exception->getMessage())) ?></h4>
                        <p>
                            The above error occurred while the Web server was processing your request.
                        </p>
                        <p>
                            Please contact us if you think this is a server error. Thank you.
                        </p>
                        </div>
                  </div>
                </div>
            </div>
        </div>
  </div>

<?php

use yii\db\Migration;

class m161213_123837_alter_provider_tags_add_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_tags` ADD COLUMN `tag_type` INT(11) NULL AFTER `provider_id`; ");
    }

    public function down()
    {
        echo "m161213_123837_alter_provider_tags_add_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$this->title = $title;
?>
<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - <?=$title?> (<?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?>)</span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
	                    	<div class="tab-pane active" id="arrival-report">
	                    	<!-- <h3><b>Arrival Reports <?=date('jS M Y',strtotime($report_from))?> - <?=date('jS M Y',strtotime($report_to))?></b> <h3> -->    
	                    		<?php
							        $gridColumns = [
							            // ['class' => 'yii\grid\SerialColumn'],
							            [
							        		'attribute' => 'Arrival',
							        		'value'		=> function($data)
							        		{
							        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return date('d.m.Y',strtotime($data->arrival_date));
							        		}
							        	],
							        	[
							        		'attribute' => 'Departure',
							        		'value'		=> function($data)
							        		{
							        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return date('d.m.Y',strtotime($data->departure_date));
							        		}
							        	],
							        	[
							        		'attribute' => '#',
							        		'value'		=> function($data)
							        		{
							        			$date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
										        $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
										        $diff=date_diff($date1,$date2);
										        $difference =  $diff->format("%a");
										        return $difference;
							        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			// return $booking_date->no_of_nights;
							        		}
							        	],
							        	[
							        		'label' => '?',
							        		'value' => function($data)
							        		{
						        				if(strtotime($data->departure_date) < strtotime(date('Y-m-d')))
					                    		{
					                    			return 0;
					                    		}
					                    		else
					                    		{
					                    			$date1 = new DateTime(date('Y-m-d'));
													$date2 = new DateTime($data->departure_date);

													$diff = $date2->diff($date1)->format("%a");
													return $diff;	
					                    		}
						        				
							        		}
							        	],
							        	[
							        		'format' => 'raw',
							        		'attribute' => 'Room Type',
							        		'value'		=> function($data)
							        		{
							        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return $data->item->itemType->name;
							        		},
							        		'contentOptions' => function($data)
							                {
							        
							                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
							                },
							        	],
							        	[
							        		'format' => 'raw',
							        		'attribute' => 'Unit Name',
							        		'value'		=> function($data)
							        		{
							        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return isset($data->item_name_id)?$data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:''):"- - - - - ";
							        		},
							        		'contentOptions' => function($data)
							                {
							        
							                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
							                },
							        	],
							        	[
							        		'attribute' => 'Guest Name',
							        		'value'		=> function($data)
							        		{
							        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return ($booking_date->guest_first_name != NULL && $booking_date->guest_last_name != NULL)?$booking_date->guest_first_name.' '.$booking_date->guest_last_name :"- - - - -";
							        		}
							        	],
							        	[
							        		'label' => 'A/C',
							        		'attribute' => 'A/C',
							        		'value'		=> function($data)
							        		{
							        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return $booking_date->no_of_adults.'/'.$booking_date->no_of_children;
							        		}
							        	],
							        	[
							        		'attribute' => 'Duration',
							        		'value'		=> function($data)
							        		{
							        			$date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
								                $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
								                $diff=date_diff($date1,$date2);
								                $difference =  $diff->format("%a");
								                // return $difference;
							        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
							        			return 'Staying '.$difference.(($difference >1)?' Nights':' Night');
							        		}
							        	],
							        	[
							        		'label' => 'Balance Due',
							        		'attribute' => 'balance',
							        		'value'		=> function($data)
							        		{
							        			
							        			return Yii::$app->formatter->asDecimal($data->balance);
							        		},
								            'contentOptions' => function($data)
								            {
								    
								                return ['style' => 'text-align:right;' ];
								            },
							        		
							        	]

							        ];
							        echo ExportMenu::widget([
							        'showConfirmAlert' => false,	
							        'dataProvider' => $dataProvider,
							        'columns' => $gridColumns,
							        'target' => ExportMenu::TARGET_BLANK,
							        'exportConfig' => [
									    ExportMenu::FORMAT_TEXT => false,
    									ExportMenu::FORMAT_PDF => false,
    									ExportMenu::FORMAT_HTML => false,
    									ExportMenu::FORMAT_EXCEL => false,
									    ExportMenu::FORMAT_CSV => [
									        'label' => 'CSV',
									    ],
									    
									    ExportMenu::FORMAT_EXCEL_X => [
									        'label' => 'EXCEL',
									    ],

									],
									'fontAwesome' => true,
							        'dropdownOptions' => [
							            'label' => 'Export Report',
							            'class' => 'btn btn-success'
							        ],
							        'enableFormatter' =>false,
							        'showColumnSelector' => false,
							        //'asDropdown' => false,
							        'container'=>['class'=>'btn-group pull-left', 'style'=> 'margin: 5px'],
							        'filename' => 'current-guests-report',
							        'exportRequestParam' => 'hello;',

							    ]);
							    ?>
							    <br><br><br>
	                    		<?= GridView::widget([

						        'id' => 'bookings_gridview',
						        'dataProvider' => $dataProvider,
						        //'filterModel' => $searchModel,
						        'options' => ['class' => 'grid-view fixed-layout','Gridlines' => "None",'BorderStyle'=> "None"],
						        'layout'=>"\n{items}\n{pager}",
						        'columns' => [
						        	//'id',
						        	[
						        		'attribute' => 'Arrival',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return date('d.m.Y',strtotime($data->arrival_date));
						        		}
						        	],
						        	[
						        		'attribute' => 'Departure',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return date('d.m.Y',strtotime($data->departure_date));
						        		}
						        	],
						        	[
						        		'attribute' => '#',
						        		'value'		=> function($data)
						        		{
						        			$date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
									        $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
									        $diff=date_diff($date1,$date2);
									        $difference =  $diff->format("%a");
									        return $difference;
						        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			// return $booking_date->no_of_nights;
						        		},
						        		'contentOptions' => function($data)
							            {
							    
							                return ['style' => 'text-align:right;' ];
							            },
						        		'headerOptions' =>[ 'style' => 'width:3%;'],
						        	],
						        	[
						        		'label' => '?',
						        		'value' => function($data)
						        		{
					        				if(strtotime($data->departure_date) < strtotime(date('Y-m-d')))
				                    		{
				                    			return 0;
				                    		}
				                    		else
				                    		{
				                    			$date1 = new DateTime(date('Y-m-d'));
												$date2 = new DateTime($data->departure_date);

												$diff = $date2->diff($date1)->format("%a");
												return $diff;	
				                    		}
					        				
						        		},
						        		'contentOptions' => function($data)
							            {
							    
							                return ['style' => 'text-align:right;' ];
							            },
						        		'headerOptions' =>[ 'style' => 'width:3%;'],
						        	],
						        	[
						        		'format' => 'raw',
						        		'attribute' => 'Room Type',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return $data->item->itemType->name;
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
						                },
						                'headerOptions' =>[ 'style' => 'width:20%;'],
						        	],
						        	[
						        		'format' => 'raw',
						        		'attribute' => 'Unit Name',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return isset($data->item_name_id)?$data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:''):"- - - - - ";
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
						                },
						        	],
						        	[
						        		'attribute' => 'Guest Name',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return ($booking_date->guest_first_name != NULL && $booking_date->guest_last_name != NULL)?$booking_date->guest_first_name.' '.$booking_date->guest_last_name :"- - - - -";
						        		}
						        	],
						        	[
						        		'label' => 'A/C',
						        		'attribute' => 'A/C',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return $booking_date->no_of_adults.'/'.$booking_date->no_of_children;
						        		},
						        		'contentOptions' => function($data)
							            {
							    
							                return ['style' => 'text-align:right;' ];
							            },
						        		'headerOptions' =>[ 'style' => 'width:5%;'],
						        	],
						        	[
						        		'attribute' => 'Duration',
						        		'value'		=> function($data)
						        		{
						        			$date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
							                $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
							                $diff=date_diff($date1,$date2);
							                $difference =  $diff->format("%a");
							                // return $difference;
						        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return 'Staying '.$difference.(($difference >1)?' Nights':' Night');
						        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			// return 'Staying '.$booking_date->no_of_nights.(($booking_date->no_of_nights >1)?' Nights':' Night');
						        		}
						        	],
						        	[
						        		'format' => 'raw',
						        		'label' => 'Balance Due',
						        		'attribute' => 'balance',
						        		'value'		=> function($data)
						        		{
						        			
						        			return Yii::$app->formatter->asDecimal($data->balance);
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'text-align:right;' ];
						                },
						        	]
						     

						        ],
						        'afterRow' => function($model, $key, $index) {
						        	//$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        	if(isset($model->comments) && !empty($model->comments))
						        	{
						        		return "<tr style='border-top:0px !important;'><td colspan=10 style='white-space: normal;word-wrap:break-word !important;'>".$model->comments."</td></tr>";

						        	}
								        
							    }
						        ]);?>
						        <p style="font-size: 12px">Total Current Guests : <?=$count?></p>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
$this->registerJs("
	$(document).ready(function(){
		$('#w3').append(
			`<li>
            	<a href=".yii::$app->getUrlManager()->createUrl(['standard-reports/current-guests-pdf',
            		'destination_id' => $destination_id,
			    	'report_to' => $report_to,
			    	'report_from' => $report_from,
			    	'cancelled_bookings' => $cancelled_bookings,
			    	'report_type' => $report_type])." 
			    	target=\"_blank\"><i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>
			    PDF</a>
		    </li>`

		);
	});
");
$this->registerCss("
		//table { table-layout:fixed; word-break:break-all; word-wrap:break-word; }
		.table{
			width:100%;
			table-layout: fixed;
		}
	");
?>
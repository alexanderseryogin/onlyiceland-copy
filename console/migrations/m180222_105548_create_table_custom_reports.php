<?php

use yii\db\Migration;

class m180222_105548_create_table_custom_reports extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `custom_reports`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `report_title` VARCHAR(256),
                          `report_color` VARCHAR(256),
                          `report_icon` VARCHAR(256),
                          `description` LONGTEXT,
                          `arrival_date_to` DATE,
                          `arrival_date_from` DATE,
                          `departure_date_to` DATE,
                          `departure_date_from` DATE,
                          `booking_date_to` DATE,
                          `booking_date_from` DATE,
                          `sort_by` VARCHAR(256),
                          `show_max` VARCHAR(256),
                          PRIMARY KEY (`id`)
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180222_105548_create_table_custom_reports cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180222_105548_create_table_custom_reports cannot be reverted.\n";

        return false;
    }
    */
}

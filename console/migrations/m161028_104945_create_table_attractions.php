<?php

use yii\db\Migration;

class m161028_104945_create_table_attractions extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `attractions`( `id` INT(11) NOT NULL AUTO_INCREMENT, `at_id` INT(11) NOT NULL, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL, `state_id` INT(11) NOT NULL, `city_id` INT(11), `description` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL, `address` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL, `phone` VARCHAR(256), `price` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;

            ALTER TABLE `attractions` CHANGE `at_id` `at_id` INT(11) NULL, CHANGE `state_id` `state_id` INT(11) NULL, ADD CONSTRAINT `FK_attractions_at_id` FOREIGN KEY (`at_id`) REFERENCES `attraction_types`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_attractions_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_attractions_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL; 
 
            ");
    }

    public function down()
    {
        echo "m161028_104945_create_table_attractions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

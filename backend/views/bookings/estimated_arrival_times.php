
<?php
	use common\models\EstimatedArrivalTimes;
	
	if($time_group >= '0')
	{
		$estimated_arrival_times = EstimatedArrivalTimes::find()->where(['time_group' => $time_group])->all();
		echo "<option value=''></option>";
    
		if(!empty($estimated_arrival_times))
		{
			foreach ($estimated_arrival_times as $key =>  $obj) 
		    {
		    	$start_time = '';

		    	if(empty($obj->text))
		    	{
		    		$start_time = date('H:i',strtotime($obj->start_time));
		    	}
		    	else
		    	{
		    		$start_time = $obj->text;
		    	}
		    	$obj->end_time = date('H:i',strtotime($obj->end_time));
		    	
		        echo "<option value='".$obj->id."'>".$start_time." - ".$obj->end_time."</option>";  
		    }
		}  
	}
	else
		return '';
?>


<?php
$this->title = "HouseKeeping";
?>
<div class="bookable-items-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Housekeeping</span>
                        </div>
                        <ul class="nav nav-tabs bookable_tabs">
                            <li class="active">
                                <a href="#items" data-toggle="tab">Reports</a>
                            </li>
                            <!-- <li>
                                <a href="#details" data-toggle="tab">Details</a>
                            </li> -->
      

                        </ul>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content" active>
                           
                            <?=  
                                $this->render('reports',[
                                    
                                ]); 
                            ?>                            

                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

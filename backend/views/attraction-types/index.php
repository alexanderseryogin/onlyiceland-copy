<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\User;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AttractionTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Attraction Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attraction-types-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW ATTRACTION TYPE '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'attraction-types-gridview','timeout' => 10000, 'enablePushState' => false]); ?>      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Logo',
                'format' =>'raw',
                'value' => function($data)
                {
                    if(!empty($data->logo))
                    {
                        $path = Yii::getAlias('@web').'/../uploads/attraction-types/images/logo/'.$data->logo;

                        return '<a name="pop[]"> <img src="'.$path.'" style="max-height:100px" > </a>';
                    }
                },
            ],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Logo preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="max-width: 570px; max-height: 600px;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('a[name=\"pop[]\"]').on('click', function() 
    {
       $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
       $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
    });

});",View::POS_END);
?>

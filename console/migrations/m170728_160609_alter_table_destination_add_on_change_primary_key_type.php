<?php

use yii\db\Migration;

class m170728_160609_alter_table_destination_add_on_change_primary_key_type extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `destination_add_ons`   
                          CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
                        ');
    }

    public function down()
    {
        echo "m170728_160609_alter_table_destination_add_on_change_primary_key_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

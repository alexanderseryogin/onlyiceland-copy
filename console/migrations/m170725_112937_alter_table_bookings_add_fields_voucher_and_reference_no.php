<?php

use yii\db\Migration;

class m170725_112937_alter_table_bookings_add_fields_voucher_and_reference_no extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `voucher_no` VARCHAR(256) NULL AFTER `balance`,
                          ADD COLUMN `reference_no` VARCHAR(256) NULL AFTER `voucher_no`;
                        ');
    }

    public function down()
    {
        echo "m170725_112937_alter_table_bookings_add_fields_voucher_and_reference_no cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_dates".
 *
 * @property integer $id
 * @property integer $rate_id
 * @property string $night_rate
 * @property integer $over_ride
 *
 * @property Rates $rate
 */
class RatesDates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_dates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate_id', 'over_ride'], 'integer'],
            [['night_rate'], 'required'],
            [['night_rate'], 'safe'],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rate_id' => Yii::t('app', 'Rate ID'),
            'night_rate' => Yii::t('app', 'Night Rate'),
            'over_ride' => Yii::t('app', 'Over Ride'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }
}

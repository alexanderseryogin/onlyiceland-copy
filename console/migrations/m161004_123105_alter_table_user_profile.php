<?php

use yii\db\Migration;

class m161004_123105_alter_table_user_profile extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_profile` ADD COLUMN `business_id_number` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `profile_picture`, ADD COLUMN `business_address` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `business_id_number`, ADD COLUMN `business_city_id` INT(11) NULL AFTER `business_address`, ADD COLUMN `business_state_id` INT(11) NULL AFTER `business_city_id`, ADD COLUMN `business_country_id` INT(11) NULL AFTER `business_state_id`, ADD COLUMN `business_postal_code` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `business_country_id`, ADD CONSTRAINT `FK_user_profile_business_city_id` FOREIGN KEY (`business_city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_user_profile_business_country_id` FOREIGN KEY (`business_country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_user_profile_business_state_id` FOREIGN KEY (`business_state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m161004_123105_alter_table_user_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets_b\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
	    <meta charset="<?= Yii::$app->charset ?>">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="onlyiceland site description" name="description" />
        <meta content="M. Saqib Zafar" name="author" />

	    <?= Html::csrfMetaTags() ?>
	    <title><?= Html::encode($this->title) ?></title>

        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <link rel="shortcut icon" href="favicon.ico" /> 
        <style>
            .summary {
                float:right;
                margin-bottom:5px;
            }
            .page-header.navbar .top-menu .navbar-nav > li.dropdown-user .dropdown-toggle > img {
                width: 35px;
            }
        </style>
		<?php $this->head() ?>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
    <?php $this->beginBody() ?>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <?= $this->render('header_nav')?>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                
                <?php

                    // echo $this->render('sidebar')
                ?>
                
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="overflow-x: auto;margin-left: 0px">
                        <!-- BEGIN PAGE HEADER-->
                    
                        <?php 
                         // echo $this->render('breadcrumbs')
                        ?>
                        
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> <?= $this->params['title'] ?>
                            <small><?= $this->params['subTitle'] ?></small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

                <!-- BEGIN QUICK SIDEBAR -->
                <!-- END QUICK SIDEBAR -->

            </div>
            <!-- END CONTAINER -->
            <?php
                // echo $this->render('footer')
            ?>
        </div>
        <!--[if lt IE 9]>
		<script src="../assets/global/plugins/respond.min.js"></script>
		<script src="../assets/global/plugins/excanvas.min.js"></script> 
		<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
		<![endif]-->
    	<?php $this->endBody() ?>
    </body>
	
</html>
<?php $this->endPage() ?>
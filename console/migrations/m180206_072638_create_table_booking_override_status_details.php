<?php

use yii\db\Migration;

class m180206_072638_create_table_booking_override_status_details extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `booking_override_stattus_details`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `date` DATE,
                          `bookable_item_id` INT(11),
                          `bookable_item_name_id` INT(11) UNSIGNED,
                          `booking_override_status_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`bookable_item_id`) REFERENCES `bookable_items`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`bookable_item_name_id`) REFERENCES `bookable_items_names`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`booking_override_status_id`) REFERENCES `booking_override_statuses`(`id`) ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180206_072638_create_table_booking_override_status_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180206_072638_create_table_booking_override_status_details cannot be reverted.\n";

        return false;
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "upsell_items".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $type
 * @property string $description
 * @property string $amount
 *
 * @property Providers $provider
 */
class UpsellItems extends \yii\db\ActiveRecord
{
    public static $types = ['Not Used','Obligatory','Obligatory %','Obligatory % Tax','Optional','Optional %','Optional Qty','Optional Extra Beds'];
    public static $applies_to = ['Each Room','Each Adult','Each Booking','Each Child','Everybody'];
    public static $period = ['Daily','Once','Weekly','Monthly'];
    public $server_amount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upsell_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','applies_to','period'], 'integer'],
            [['name','type','description','applies_to','period'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'applies_to' => Yii::t('app', 'Applies To'),
            'period' => Yii::t('app', 'Period'),
        ];
    }
}

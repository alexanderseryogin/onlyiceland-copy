
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Destinations;	
$current_date = date('d-m-Y',time());
?>
<div class="main-div">
	<div class="row">
		<div class="col-md-2">
	        <?= Html::dropDownList('id', null,ArrayHelper::map(Destinations::find()->where(['active' => 1 ])->all(), 'id', 'name'),['id'=> 'dest-dropdown','class' => 'form-control select2','prompt' => 'Select Destination']) ?>
	    </div>
	    <div class="col-md-2">
	        <input name='date' id="date" class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=$current_date?>">
	    </div>
	    <!-- <div class="col-md-2" style="margin-left: -50px;margin-top:1px;">
	         <button id="add_report" class="btn btn-primary btn-square" disabled> Generate Report</button>
	    </div> -->
	    
	   <!--  <span class="help-block"> Select date </span> -->
	</div>
	<br>
	<div id="items_table">

	</div>
</div>
<?php
$get_items = Url::to(['/house-keeping/get-items']);
$this->registerJs("
	$(document).ready(function(){
		$('#dest-dropdown').select2({
	        //allowClear: true,
	    });
		$('#dest-dropdown').on('change',function(){
			//alert($(this).val());
				App.blockUI({target:'.main-div', animate: !0});
				var date = $('#date').val();
				var dest_id = $('#dest-dropdown').val();
				//alert(date);

				$.ajax(
		        {
		            type: 'POST',
		            url: '$get_items',
		            data: {date:date,dest_id:dest_id},
		            success: function(data)
		            {
		                data = jQuery.parseJSON( data );
		                $('#items_table').empty().html(data.report_items);
		                App.unblockUI('.main-div');

		                // $('#booking_item_modal').modal('show');
		                // initJsAfterBookingViewRender();
		            },
		            error: function()
		            {
		                alert('error');
		            }
		        });
				//alert('here');
		});
		$('#date').on('change',function(){
			//alert($(this).val());
			if($(this).val() == '')
			{
				//$('#add_report').attr('disabled',true);
				//alert('here');
			}
			else
			{
				App.blockUI({target:'.main-div', animate: !0});
				var date = $('#date').val();
				var dest_id = $('#dest-dropdown').val();
				//alert(date);

				$.ajax(
		        {
		            type: 'POST',
		            url: '$get_items',
		            data: {date:date,dest_id:dest_id},
		            success: function(data)
		            {
		                data = jQuery.parseJSON( data );
		                $('#items_table').empty().html(data.report_items);
		                App.unblockUI('.main-div');

		                // $('#booking_item_modal').modal('show');
		                // initJsAfterBookingViewRender();
		            },
		            error: function()
		            {
		                alert('error');
		            }
		        });
				//alert('here');
			}
		});


		// $('#add_report').click(function(){
		// 	App.blockUI({target:'.main-div', animate: !0});
		// 	var date = $('#date').val();
		// 	var dest_id = $('#dest-dropdown').val();
		// 	//alert(date);

		// 	$.ajax(
	 //        {
	 //            type: 'POST',
	 //            url: '$get_items',
	 //            data: {date:date,dest_id:dest_id},
	 //            success: function(data)
	 //            {
	 //                data = jQuery.parseJSON( data );
	 //                $('#items_table').empty().html(data.report_items);
	 //                App.unblockUI('.main-div');

	 //                // $('#booking_item_modal').modal('show');
	 //                // initJsAfterBookingViewRender();
	 //            },
	 //            error: function()
	 //            {
	 //                alert('error');
	 //            }
	 //        });

		// });

	});
		
");

?>
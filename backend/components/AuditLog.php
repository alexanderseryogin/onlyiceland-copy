<?php

namespace backend\components;
use common\models\Modelhistory;
use common\models\Destinations;
use common\models\BookableItems;
use common\models\TravelPartner;
use common\models\Rates;
use common\models\Offers;
use common\models\BookingStatuses;
use common\models\BookingTypes;
use common\models\BookingConfirmationTypes;
use common\models\User;

use Yii;
use yii\helpers\Url;

class AuditLog
{
    public $table;
    public $field_name;
    public $field_id;
    public $old_value;
    public $new_value;
    public $type=1;

    public function SaveHistory()
    {
        $history_model = new Modelhistory();
        $history_model->date = date('Y-m-d H:i:s');
        $history_model->table = $this->table;
        $history_model->field_name = $this->field_name;
        $history_model->field_id = $this->field_id;
        $history_model->old_value = $this->old_value;
        $history_model->new_value = $this->new_value;
        $history_model->type = $this->type;
        $history_model->user_id = Yii::$app->user->identity->username;

        $history_model->save();
        /*print_r($history_model->getErrors());
        exit;*/
    }

    public function getAttributeValue($attribute_name,$attribute_id) // return name instead of id's
    {
        $value = '';

        switch ($attribute_name) 
        {
            case 'user_id':
                $model = User::findOne(['id' => $attribute_id]);
                $value = $model->username;
                break;

            case 'provider_id':
                $model = Destinations::findOne(['id' => $attribute_id]);
                $value = $model->name;
                break;

            case 'item_id':
                $model = BookableItems::findOne(['id' => $attribute_id]);
                $value = $model->itemType->name;
                break;

            case 'travel_partner_id':
                $model = TravelPartner::findOne(['id' => $attribute_id]);
                $value = $model->company_name;
                break;

            case 'rates_id':
                $model = Rates::findOne(['id' => $attribute_id]);
                $value = $model->name;
                break;

            case 'offers_id':
                $model = Offers::findOne(['id' => $attribute_id]);
                $value = $model->name;
                break;
                
            case 'status_id':
                $model = BookingStatuses::findOne(['id' => $attribute_id]);
                $value = $model->label;
                break;

            case 'flag_id':
                $model = BookingTypes::findOne(['id' => $attribute_id]);
                $value = $model->label;
                break;

            case 'cancellation_id':
                $arr = Destinations::getBookingCancellationArray();
                $value = $arr[$attribute_id];
                break;
                
            case 'confirmation_id':
                $model = BookingConfirmationTypes::findOne(['id' => $attribute_id]);
                $value = $model->name;
                break;
            
        }

        return $value;
    }
}

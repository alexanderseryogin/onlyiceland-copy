<?php

use yii\db\Migration;

class m161221_124323_alter_open_hours_name_remove_dates extends Migration
{
    public function up()
    {
        $this->execute("RENAME TABLE `providers_open_hours_days` TO `providers_open_hours`; 
            DROP TABLE `providers_open_hours_dates`; ");
    }

    public function down()
    {
        echo "m161221_124323_alter_open_hours_name_remove_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

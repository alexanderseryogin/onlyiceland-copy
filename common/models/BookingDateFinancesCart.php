<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_date_finances_cart".
 *
 * @property integer $id
 * @property string $date
 * @property string $booking_item_id
 * @property string $booking_group_id
 * @property integer $type
 * @property string $price_description
 * @property double $amount
 * @property double $vat
 * @property double $tax
 * @property integer $quantity
 * @property integer $show_receipt
 * @property integer $added_by
 *
 * @property BookingsItemsCart $bookingItem
 * @property BookingGroupCart $bookingGroup
 */
class BookingDateFinancesCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const SHOW_RECEIPT = 0;
    const HIDE_RECEIPT = 1;

    const TYPE_DEBIT = 1;
    const TYPE_CREDIT = 2;

    const ITEM_TYPE_BOOKING = 1;
    const ITEM_TYPE_UPSELL = 2;
    const ITEM_TYPE_ADDON = 3;
    const ITEM_TYPE_CUSTOM = 4;

    const ENTRY_TYPE_SYSTEM = 1;
    const ENTRY_TYPE_USER = 2;

    public static function tableName()
    {
        return 'booking_date_finances_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['booking_item_id', 'booking_group_id', 'type', 'quantity', 'show_receipt', 'added_by'], 'integer'],
            [['price_description'], 'string'],
             [['amount', 'vat', 'tax','x','y','discount','discount_amount','sub_total1','sub_total2','vat_amount','final_total'], 'number'],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItemsCart::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
            [['booking_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroupCart::className(), 'targetAttribute' => ['booking_group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'booking_item_id' => 'Booking Item ID',
            'booking_group_id' => 'Booking Group ID',
            'type' => 'Type',
            'price_description' => 'Price Description',
            'amount' => 'Amount',
            'vat' => 'Vat',
            'tax' => 'Tax',
            'quantity' => 'Quantity',
            'show_receipt' => 'Show Receipt',
            'added_by' => 'Added By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItemsCart::className(), ['id' => 'booking_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroup()
    {
        return $this->hasOne(BookingGroupCart::className(), ['id' => 'booking_group_id']);
    }
}

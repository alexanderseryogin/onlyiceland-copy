<?php

use yii\db\Migration;

class m161006_113155_insert_payment_methods extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `payment_methods` (`name`) 
        VALUES ('American Express'),
        ('Diners Club'),
        ('Discover'),
        ('enRoute'),
        ('JCB'),
        ('MasterCard'),
        ('Visa'),
        ('Maestro'),
        ('Cash (ISK)'),
        ('Cash (Euros)'),
        ('Cash (USD)')
        ;");
    }

    public function down()
    {
        echo "m161006_113155_insert_payment_methods cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

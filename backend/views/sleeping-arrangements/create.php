<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SleepingArrangements */

$this->title = Yii::t('app', 'Sleeping Arrangements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sleeping Arrangements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sleeping-arrangements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

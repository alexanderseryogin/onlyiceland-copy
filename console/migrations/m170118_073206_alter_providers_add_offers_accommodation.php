<?php

use yii\db\Migration;

class m170118_073206_alter_providers_add_offers_accommodation extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `providers` ADD COLUMN `offers_accommodation` TINYINT(1) NULL AFTER `banner`;  ');
    }

    public function down()
    {
        echo "m170118_073206_alter_providers_add_offers_accommodation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

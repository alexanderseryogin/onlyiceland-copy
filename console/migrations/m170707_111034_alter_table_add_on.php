<?php

use yii\db\Migration;

class m170707_111034_alter_table_add_on extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `add_on`   
                          CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, 
                          ADD PRIMARY KEY (`id`);
                        ');
    }

    public function down()
    {
        echo "m170707_111034_alter_table_add_on cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

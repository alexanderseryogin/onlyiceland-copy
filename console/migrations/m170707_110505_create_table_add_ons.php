<?php

use yii\db\Migration;

class m170707_110505_create_table_add_ons extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `add_on`(  
                          `id` INT(11),
                          `name` VARCHAR(256),
                          `description` LONGTEXT,
                          `type` INT(6),
                          `discountable` INT(6)
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170707_110505_create_table_add_ons cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

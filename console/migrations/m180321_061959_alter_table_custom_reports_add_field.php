<?php

use yii\db\Migration;

class m180321_061959_alter_table_custom_reports_add_field extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_reports`   
                          ADD COLUMN `group_by` INT(11) DEFAULT 0  NULL AFTER `sum_average`;
                        ');
    }

    public function safeDown()
    {
        echo "m180321_061959_alter_table_custom_reports_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_061959_alter_table_custom_reports_add_field cannot be reverted.\n";

        return false;
    }
    */
}

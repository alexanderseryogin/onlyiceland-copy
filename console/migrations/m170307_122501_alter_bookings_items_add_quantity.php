<?php

use yii\db\Migration;

class m170307_122501_alter_bookings_items_add_quantity extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` ADD COLUMN `quantity` INT(11) NULL AFTER `user_id`; ");
    }

    public function down()
    {
        echo "m170307_122501_alter_bookings_items_add_quantity cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

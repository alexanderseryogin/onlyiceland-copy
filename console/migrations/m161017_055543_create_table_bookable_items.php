<?php

use yii\db\Migration;

class m161017_055543_create_table_bookable_items extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `bookable_items`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `item_type_id` INT(11), `item_quantity` INT(11), `how_booked` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `item_names` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `shortest_period` INT(11), `longest_period` INT(11), `no_of_guests` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `max_adults` INT(11), `max_children` INT(11), `max_extra_beds` INT(11), `max_baby_beds` INT(11), `min_age_participation` INT(11), `max_age_participation` INT(11), `max_free_age` INT(11), `background_color` VARCHAR(11), `text_color` VARCHAR(11), `include_in_groups` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_bookable_items_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_bookable_items_item_type` FOREIGN KEY (`item_type_id`) REFERENCES `bookable_item_types`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161017_055543_create_table_bookable_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

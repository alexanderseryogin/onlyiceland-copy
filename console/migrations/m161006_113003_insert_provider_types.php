<?php

use yii\db\Migration;

class m161006_113003_insert_provider_types extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `provider_types` (`name`) 
        VALUES ('Accommodation (on list)'),
        ('Activity'),
        ('Entertainment'),
        ('Event'),
        ('Guide'),
        ('Restaurant'),
        ('Shopping'),
        ('Sight (on list)'),
        ('Transportation'),
        ('Vehicle Rental')
        ;");
    }

    public function down()
    {
        echo "m161006_113003_insert_provider_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

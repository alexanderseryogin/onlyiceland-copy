<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

use common\models\DiscountCode;
use common\models\Destinations;
use common\models\DestinationTypes;
use common\models\States;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DiscountCodeSearch */
/* @var $dataDestination yii\data\ActiveDataDestination */

$this->title = Yii::t('app', 'Discount / Voucher Codes');
$this->params['breadcrumbs'][] = $this->title;

$update_page_url = Url::to(['/discount-code/update']);
?>

<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>
<div class="discount-code-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW DISCOUNT CODE'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'discount-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-scrollable'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'provider_id',
                'label' => 'Destination Name',
                'value' => function($data)
                {
                    if(isset($data->destination->name) && !empty($data->destination->name))
                        return $data->destination->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[provider_id]', 
                    $searchModel['provider_id'], 
                    ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Destination', 
                        'id'=>'provider_dropdown', 
                        'class'=>'form-control'
                    ]),
                'headerOptions' =>['style' => 'min-width:250px;'],
                'contentOptions' =>['style' => 'min-width:250px;'],
            ],
            [
                'attribute' => 'provider_type_id',
                'label' => 'Destination Type',
                'value' => function($data)
                {
                    if(isset($data->destination->destinationType->name) && !empty($data->destination->destinationType->name))
                        return $data->destination->destinationType->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[provider_type_id]', 
                    $searchModel['provider_type_id'], 
                    ArrayHelper::map(DestinationTypes::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'provider_type_dropdown', 
                        'class'=>'form-control'
                    ]),
                'headerOptions' =>['style' => 'min-width:200px;'],
                'contentOptions' =>['style' => 'min-width:200px;'],
            ],

            [
                'attribute' => 'state_id',
                'label' => 'State',
                'value' => function($data)
                {
                    if(isset($data->destination->state->name) && !empty($data->destination->state->name))
                        return $data->destination->state->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[state_id]', 
                    $searchModel['state_id'], 
                    ArrayHelper::map(States::find()->where(['country_id' => 100])->orderBy('name ASC')->all(),'id','name'), 
                    [
                        'prompt' => 'Select a State', 
                        'id'=>'state_dropdown', 
                        'class'=>'form-control'
                    ]),
                'headerOptions' =>['style' => 'min-width:200px;'],
                'contentOptions' =>['style' => 'min-width:200px;'],
            ],

            [
                'attribute' => 'name',
                'value' => function($data)
                {
                    return $data->name;
                },
                'headerOptions' =>['style' => 'min-width:200px;'],
                'contentOptions' =>['style' => 'min-width:200px;'],
            ],
            [
                'attribute' => 'amount',
                'format' => ['decimal'],
                'value' => function($data)
                {
                    return $data->amount;
                },
                'headerOptions' =>['style' => 'min-width:100px;'],
                'contentOptions' =>['style' => 'min-width:100px;'],
            ],
            [
                'attribute' => 'pricing_type',
                'value' => function($data)
                {
                    return DiscountCode::$pricing_type[$data->pricing_type];
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[pricing_type]', 
                    $searchModel['pricing_type'], 
                    DiscountCode::$pricing_type,
                    [
                        'prompt' => 'Select a Pricing Type', 
                        'id'=>'pricing_type_dropdown', 
                        'class'=>'form-control'
                    ]),

                'headerOptions' =>['style' => 'min-width:150px;'],
                'contentOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'applies_to',
                'value' => function($data)
                {
                    return DiscountCode::$applies_to[$data->applies_to];
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[applies_to]', 
                    $searchModel['applies_to'], 
                    DiscountCode::$applies_to,
                    [
                        'prompt' => 'Select a Pricing Type', 
                        'id'=>'applies_to_dropdown', 
                        'class'=>'form-control'
                    ]),

                'headerOptions' =>['style' => 'min-width:150px;'],
                'contentOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'quantity_type',
                'value' => function($data)
                {
                    return DiscountCode::$quantity_type[$data->quantity_type];
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[quantity_type]', 
                    $searchModel['quantity_type'], 
                    DiscountCode::$quantity_type,
                    [
                        'prompt' => 'Select a Quantity Type', 
                        'id'=>'quantity_type_dropdown', 
                        'class'=>'form-control'
                    ]),

                'headerOptions' =>['style' => 'min-width:220px;'],
                'contentOptions' =>['style' => 'min-width:220px;'],
            ],
            [
                'attribute' => 'enterable_by_registered_users',
                'label' => 'Type In',
                'value' => function($data)
                {
                    return $data->enterable_by_registered_users;
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[enterable_by_registered_users]', 
                    $searchModel['enterable_by_registered_users'], 
                    DiscountCode::$type_in,
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'type_in_dropdown', 
                        'class'=>'form-control'
                    ]),
                'headerOptions' =>['style' => 'min-width:100px;'],
                'contentOptions' =>['style' => 'min-width:100px;'],
            ],
            [
                'attribute' => 'active',
                'value' => function($data)
                {
                    return $data->active;
                },
                'filter' => Html::dropDownList(
                    'DiscountCodeSearch[active]', 
                    $searchModel['active'], 
                    DiscountCode::$type_in,
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'active_dropdown', 
                        'class'=>'form-control'
                    ]),
                'headerOptions' =>['style' => 'min-width:100px;'],
                'contentOptions' =>['style' => 'min-width:100px;'],
            ],

            ['class' => 'yii\grid\ActionColumn',
            'headerOptions' =>['style' => 'min-width:80px;'],
            'contentOptions' =>['style' => 'min-width:80px;'],],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#pricing_type_dropdown').select2({
        placeholder: \"Select a Pricing Type\",
        allowClear: true
    });

    $('#quantity_type_dropdown').select2({
        placeholder: \"Select a Quantity Type\",
        allowClear: true
    });

    $('#applies_to_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#provider_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#type_in_dropdown').select2({
        placeholder: \"Select a Type In\",
        allowClear: true
    });

    $('#active_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#state_dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#discount-gridview').on('pjax:end', function() 
    {
        $('#pricing_type_dropdown').select2({
            placeholder: \"Select a Pricing Type\",
            allowClear: true
        });

        $('#quantity_type_dropdown').select2({
            placeholder: \"Select a Quantity Type\",
            allowClear: true
        });

        $('#applies_to_dropdown').select2({
            placeholder: \"Select an Option\",
            allowClear: true
        });

        $('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
        });

        $('#provider_type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });

        $('#state_dropdown').select2({
            placeholder: \"Select a State\",
            allowClear: true
        });

        $('#type_in_dropdown').select2({
            placeholder: \"Select a Type In\",
            allowClear: true
        });

        $('#active_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });

    });
    $(document).on('click','.table-striped tbody tr',function()
    {
        window.location.replace('$update_page_url?id='+$(this).attr('data-key'));
    });
    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>

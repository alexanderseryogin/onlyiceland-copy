<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$this->title = 'Gross Credit Report';
$bookableItems = $destination->bookableItems;
// echo "<pre>";
// print_r($report);
// exit;
?>
<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
                            <div class="btn-group pull-left" style="margin: 5px">
                                <div class="btn-group">
                                <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                                    
                                </span></button>

                                    <ul id="w3" class="dropdown-menu">
                                        <li title="Comma Separated Values"><a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                        <li title="Microsoft Excel 2007+ (xlsx)"><a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                        <li>
                                            <a href="<?=yii::$app->getUrlManager()->createUrl(['standard-reports/gross-credit-pdf','destination_id' => $destination_id,
                                                'report_to' => $report_to,
                                                'report_from' => $report_from,
                                                'cancelled_bookings' => $cancelled_bookings,
                                                'report_type' => $report_type,
                                                'type' => $type ])?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            PDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <br><br><br>
	                    	<div class="tab-pane active" id="occupancy-report">
                                <table class="table table-striped table-bordered table2excel">
                                    <!-- <thead>
                                      <tr>
                                        <th><?=$destination->name?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                      </tr>
                                    </thead> -->
                                    <tbody>
                                     <?php
                                        $total_arr = array();
                                        $total_arr['total_gross'] = 0;
                                        // $total_arr['booked_count'] = 0;
                                        // $total_arr['avail_count'] = 0;
                                        // $total_arr['occupancy_count'] = 0;

                                        for ($i=0; $i < count($destination->bookableItems)+2 ; $i++) 
                                        { 
                                            if($i ==  0)
                                            {
                                                echo "<tr>";
                                                echo "<td><b>".$destination->name."</b></td>";
                                                foreach ($report as $key => $value) 
                                                {
                                                    echo "<td style='text-align:center;'>".$key."</td>";
                                                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                    // echo "<td style='border-left: 0px;'></td>";
                                                    
                                                }
                                                echo "<td style='text-align:center;'>Total</td>";
                                                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                // echo "<td style='border-left: 0px;'></td>";
                                                echo "</tr>";
                                            }   
                                            else
                                            {
                                                if($i < count($destination->bookableItems)+1)
                                                {
                                                    $b_item = $bookableItems[$i-1];
                                                    echo "<tr>";
                                                    echo "<td>".$b_item->itemType->name."</td>";
                                                    // echo "<pre>";
                                                    //     print_r($report);
                                                    //     exit();
                                                    // $booked_count = 0;
                                                    // $avail_count = 0;
                                                    $total_gross = 0;
                                                    foreach ($report as $key => $value) 
                                                    {
                                                        $item = $value[$b_item->itemType->name];
                                                        // $total_qty = $item['booked']+$item['avail'];
                                                        // $occupancy = round(($item['booked']/$total_qty)*100,2);
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($item['gross_amount'])."</td>";
                                                        // echo "<td style='text-align:right'>".$item['booked']."</td>";
                                                        // echo "<td style='text-align:right'>".$item['avail']."</td>";
                                                        // echo "<td style='text-align:right'>".round($item['occupancy'],2).' %' ."</td>";
                                                        $total_gross += $item['gross_amount'];
                                                    }
                                                    $total_arr['total_gross'] += $total_gross;
                                                    // $total_arr['booked_count'] += $booked_count;
                                                    // $total_arr['avail_count'] += $avail_count;
                                                    //$total_arr['occupancy_count'] += round($guests_occupancy/count($report),2);

                                                    // echo "<td style='text-align:right'>".$guests_count."</td>";
                                                    // echo "<td style='text-align:right'>".$booked_count."</td>";
                                                    // echo "<td style='text-align:right'>".$avail_count."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_gross)."</td>";
                                                    echo "</tr>";

                                                }
                                                else
                                                {
                                                    // echo "<pre>";
                                                    // print_r($total_arr);
                                                    echo "<tr>";
                                                    echo "<td>Total</td>";
                                                    // echo "<pre>";
                                                    // print_r($report);
                                                    // exit();
                                                    
                                                    foreach ($report as $key => $value) 
                                                    {
                                                        // $guests_count = 0;
                                                        $booked_count = 0;
                                                        $avail_count = 0;
                                                        $total_gross = 0;
                                                        foreach ($destination->bookableItems as $item) 
                                                        {
                                                            $gross_amount = $value[$item->itemType->name]['gross_amount'];

                                                    

                                                            $total_gross += $gross_amount;
                                                        }
                                                        
                                                        // $occupancy = round(($total_occupancy/count($destination->bookableItems)),2);
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_gross)."</td>";

                                                    }
                                                    // echo "<td style='text-align:right'>".$total_arr['guests_count']."</td>";
                                                    // echo "<td style='text-align:right'>".$total_arr['booked_count']."</td>";
                                                    // echo "<td style='text-align:right'>".$total_arr['avail_count']."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_gross'])."</td>";
                                                    // */
                                                    echo "</tr>";

                                                }
                                                                                                    
                                            }
                                            
                                    
                                        }
                                    ?>
                                    </tbody>
                                  </table>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>
<?php
$this->registerJs('
    $("#excel-btn").click(function(){
        console.log("clicked");
      $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Gross Credit Report",
            filename: "gross-revenue-report",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
    $("#csv-btn").click(function(){
        console.log("clicked");
        
          $(".table2excel").tableToCSV({
            name: "Gross Credit Report",
            filename: "gross-revenue-report",
          });
    }); 
');
$this->registerCss("
    table {
        // display: block;
        // overflow-x: auto;
        // white-space: nowrap;
        // table-layout: fixed;
    }
    ");
?>
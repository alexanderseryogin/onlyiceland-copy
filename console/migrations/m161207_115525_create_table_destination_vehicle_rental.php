<?php

use yii\db\Migration;

class m161207_115525_create_table_destination_vehicle_rental extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_vehicle_rental` (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `provider_id` INT(11) DEFAULT NULL,
          `owner_id` INT(11) DEFAULT NULL,
          `region_id` INT(11) DEFAULT NULL,
          `city_id` INT(11) DEFAULT NULL,
          `address` TEXT COLLATE utf8_unicode_ci NOT NULL,
          `description` TEXT COLLATE utf8_unicode_ci NOT NULL,
          `gps_coordinates` TEXT COLLATE utf8_unicode_ci,
          `phone` VARCHAR(256) COLLATE utf8_unicode_ci DEFAULT NULL,
          `website` VARCHAR(500) COLLATE utf8_unicode_ci DEFAULT NULL,
          `price_range` TEXT COLLATE utf8_unicode_ci,
          `postal_code` VARCHAR(256) COLLATE utf8_unicode_ci DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `FK_providers_vehicle_rental_provider_id` (`provider_id`),
          KEY `FK_providers_vehicle_rental_region_id` (`region_id`),
          KEY `FK_providers_vehicle_rental_owner_id` (`owner_id`),
          KEY `FK_providers_vehicle_rental_city_id` (`city_id`),
          CONSTRAINT `FK_providers_vehicle_rental_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL,
          CONSTRAINT `FK_providers_vehicle_rental_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
          CONSTRAINT `FK_providers_vehicle_rental_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
          CONSTRAINT `FK_providers_vehicle_rental_region_id` FOREIGN KEY (`region_id`) REFERENCES `states` (`id`) ON DELETE SET NULL
        ) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    public function down()
    {
        echo "m161207_115525_create_table_destination_vehicle_rental cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

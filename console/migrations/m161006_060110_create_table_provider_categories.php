<?php

use yii\db\Migration;

class m161006_060110_create_table_provider_categories extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `provider_categories`( `id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
            ");
    }

    public function down()
    {
        echo "m161006_060110_create_table_provider_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace backend\controllers;

use Yii;
use DateTime;
use common\models\Destinations;
use common\models\Notifications;
use common\models\User;
use backend\components\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AmenitiesController implements the CRUD actions for Amenities model.
 */
class CronController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionNoRateAvailableNotification()
    {
        $this->NoRateAvailableNotification(); // weekly
    }

    public function NoRateAvailableNotification()
    {
        foreach(Destinations::find()->all() as $key => $destination)  // get all destinations
        {
            if(!empty($destination->bookableItems))
            {
                foreach ($destination->bookableItems as $key => $bookableItem) // get all bookable items
                {
                    Notifications::generateNoRateAvailableNotifications($bookableItem);
                }
            }
        }
        return $this->redirect(['notifications/index']);
    }
}

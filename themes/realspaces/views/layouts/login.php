<?php

/* @var $this \yii\web\View */
/* @var $content string */

use realspaces\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <!-- Basic Page Needs
          ================================================== -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="onlyiceland site description" name="description" />
        <meta content="M. Saqib Zafar" name="author" />

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <link rel="shortcut icon" href="favicon.ico" />
        <?php $this->head() ?>
    </head>
    <!-- END HEAD -->

    <body class="home">
        <?php $this->beginBody() ?>
            <!--[if lt IE 7]>
                <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
            <![endif]-->
            <div class="body">

              <!-- Start Site Header -->
              <?= $this->render('header_nav')?>
              <!-- End Site Header -->

                <!-- BEGIN PAGE BASE CONTENT -->
                    <?= Alert::widget() ?>
                    <?= $content ?>
                <!-- END PAGE BASE CONTENT -->

              <!-- Start Site Footer -->
              
              <!-- Start sub Footer -->
              <?= $this->render('sub_footer')?>
              <!-- End sub Footer -->

              <!-- Start main Footer -->
              <?= $this->render('footer')?>
              <!-- End main Footer -->

              <!-- End Site Footer -->
              <a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>
            </div>
        <?php $this->endBody() ?>
    </body>
    
</html>
<?php $this->endPage() ?>
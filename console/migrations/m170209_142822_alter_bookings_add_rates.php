<?php

use yii\db\Migration;

class m170209_142822_alter_bookings_add_rates extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` CHANGE `price` `rates_id` INT(11) NULL COMMENT 'fk of rates', ADD CONSTRAINT `FK_bookings_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE; 
        ");
    }

    public function down()
    {
        echo "m170209_142822_alter_bookings_add_rates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

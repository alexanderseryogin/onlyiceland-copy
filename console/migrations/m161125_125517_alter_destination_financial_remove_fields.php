<?php

use yii\db\Migration;

class m161125_125517_alter_destination_financial_remove_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_financial` DROP COLUMN `email`, DROP COLUMN `owning_entity`, DROP COLUMN `address`, DROP COLUMN `country_id`, DROP COLUMN `city_id`, DROP COLUMN `state_id`, DROP COLUMN `postal_code`, CHANGE `bank_info` `bank_info` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL, DROP INDEX `FK_financial_country_id`, DROP INDEX `FK_financial_state_id`, DROP INDEX `FK_financial_city_id`, DROP FOREIGN KEY `FK_financial_city_id`, DROP FOREIGN KEY `FK_financial_country_id`, DROP FOREIGN KEY `FK_financial_state_id`; 
        ");
    }

    public function down()
    {
        echo "m161125_125517_alter_destination_financial_remove_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

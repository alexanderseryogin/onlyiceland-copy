<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_method_checkin".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $pm_id
 *
 * @property PaymentMethods $pm
 * @property Providers $provider
 */
class PaymentMethodCheckin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_method_checkin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'pm_id'], 'required'],
            [['provider_id', 'pm_id'], 'integer'],
            [['pm_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyPaymentMethods::className(), 'targetAttribute' => ['pm_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination ID'),
            'pm_id' => Yii::t('app', 'Pm ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPm()
    {
        return $this->hasOne(PropertyPaymentMethods::className(), ['id' => 'pm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }
}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BedTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bed Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bed-types-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BED TYPE'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'bed-types-gridview','timeout' => 10000, 'enablePushState' => false]); ?>   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'intended_sleeping_capacity',
            'max_sleeping_capacity',
            [
                'attribute' => 'combineable',
                'value' => function($data)
                {
                    if($data->combineable == 1)
                        return 'Yes';
                    else
                        return 'No';
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' =>['style' => 'min-width:70px;'],
                'contentOptions' =>['style' => 'min-width:70px;'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

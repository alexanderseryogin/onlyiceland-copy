<?php
use common\models\BookingDates;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<?= GridView::widget([

    'id' => 'bookings_gridview',
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'options' => ['class' => 'grid-view fixed-layout','Gridlines' => "None",'BorderStyle'=> "None"],
    'layout'=>"\n{items}",
    'columns' => [
                                    //'id',
        [
            'attribute' => 'country',
            'value'     => function($data)
            {
                // echo "<pre>";
                // print_r($data);
                // exit();
                return $data['country'];
            }
        ],
        [
            'attribute' => 'bookings',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return $data['bookings'];
            },
            'contentOptions' => function($data)
            {

                return ['style' => 'text-align:right;' ];
            },
        ],
        [
            'attribute' => 'total_invoice',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return isset($data['total_invoice'])?Yii::$app->formatter->asDecimal($data['total_invoice']):0;
            },
            'contentOptions' => function($data)
            {

                return ['style' => 'text-align:right;' ];
            },
        ],
        [
            'attribute' => 'revenue_per_booking',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return Yii::$app->formatter->asDecimal($data['revenue_per_booking']);
            },
            'contentOptions' => function($data)
            {

                return ['style' => 'text-align:right;' ];
            },
        ],
        [
            'attribute' => 'no_of_nights',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return isset($data['no_of_nights'])?$data['no_of_nights']:0;
            },
            'contentOptions' => function($data)
            {

                return ['style' => 'text-align:right;' ];
            },
        ],
        [
            'attribute' => 'average_stay',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return isset($data['average_stay'])?$data['average_stay']:0;
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
        ],
        [
            'label' => 'Guests Coming',
            'attribute' => 'no_of_guests',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return $data['no_of_guests'];
            },
            'contentOptions' => function($data)
            {

                return ['style' => 'text-align:right;' ];
            },
        ],
        [
            'attribute' => 'guest_nights',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return $data['guest_nights'];
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
        ],
        

    ],
    // 'afterRow' => function($model, $key, $index) {
    //     //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
    //     if(isset($model->comments) && !empty($model->comments))
    //     {
    //         return "<tr style='border-top:0px !important;'><td colspan=10 style='white-space: normal;word-wrap:break-word !important;'>".$model->comments."</td></tr>";

    //     }
            
    // }
]);?>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>

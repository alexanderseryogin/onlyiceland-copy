<?php

use yii\db\Migration;

class m161006_141259_create_table_provider extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers`( `id` INT(11) NOT NULL AUTO_INCREMENT, `region_id` INT(11), `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `provider_category_id` INT(11), `provider_type_id` INT(11), `provider_admin_id` INT(11), `phone` VARCHAR(256), `fax` VARCHAR(256), `booking_email` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `website` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `kennitala` VARCHAR(256), `address` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `country_id` INT(11), `city_id` INT(11), `state_id` INT(11), `postal_code` VARCHAR(256), `latitude` DECIMAL(18,15), `longitude` DECIMAL(18,15), `business_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `business_address` TEXT, `business_country_id` INT(11), `business_city_id` INT(11), `business_state_id` INT(11), `business_postal_code` VARCHAR(256), PRIMARY KEY (`id`), CONSTRAINT `FK_provider_region_id` FOREIGN KEY (`region_id`) REFERENCES `regions`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_category_id` FOREIGN KEY (`provider_category_id`) REFERENCES `provider_categories`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_type_id` FOREIGN KEY (`provider_type_id`) REFERENCES `provider_types`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_admin_id` FOREIGN KEY (`provider_admin_id`) REFERENCES `user`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_business_country_id` FOREIGN KEY (`business_country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_business_city_id` FOREIGN KEY (`business_city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_provider_business_state_id` FOREIGN KEY (`business_state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161006_141259_create_table_provider cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_group_finance".
 *
 * @property integer $id
 * @property string $booking_group_id
 * @property string $price_description
 * @property string $date
 * @property integer $type
 * @property double $amount
 * @property integer $show_receipt
 *
 * @property BookingGroup $bookingGroup
 */
class BookingGroupFinance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const PAYMETN_TYPE_NORMAL = 0;
    const PAYMETN_TYPE_DEPOSIT = 1;
    
    public static function tableName()
    {
        return 'booking_group_finance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_group_id', 'type', 'show_receipt','payment_type'], 'integer'],
            [['price_description'], 'string'],
            [['date'], 'safe'],
            [['amount','deposit_percent_age'], 'number'],
            [['booking_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroup::className(), 'targetAttribute' => ['booking_group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_group_id' => 'Booking Group ID',
            'price_description' => 'Price Description',
            'date' => 'Date',
            'type' => 'Type',
            'amount' => 'Amount',
            'show_receipt' => 'Show Receipt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroup()
    {
        return $this->hasOne(BookingGroup::className(), ['id' => 'booking_group_id']);
    }
    public function getBookingGroupFinanceChildren()
    {
        return $this->hasMany(BookingGroupFinanceDetails::className(), ['booking_group_finance_id' => 'id']);
    }
}

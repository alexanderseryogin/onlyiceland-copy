<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BedsCombinations */

$this->title = Yii::t('app', 'Beds Combinations');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Beds Combinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="beds-combinations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

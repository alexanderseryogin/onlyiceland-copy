<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\BookingOverrideStatuses;
/* @var $this yii\web\View */
/* @var $model common\models\BookingOverrideStatuses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portlet light ">
<div class="booking-override-statuses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    
    <?php 
    	if(empty($model->background_color) && $model->isNewRecord)
    	{
    		$model->background_color='ff6161';
    	}
    	if(empty($model->text_color) && $model->isNewRecord)
    	{
    		$model->text_color='ff6161';
    	}
    ?>

    <label class="control-label">Background Color</label>
    <div class="form-group">
        <input type="text" id="hue-demo1" name="BookingOverrideStatuses[background_color]" class="form-control demo" data-control="hue"  value=<?=$model->background_color?>> 
    </div>

    <label class=" control-label">Text Color</label>
    <div class="form-group">
        <input type="text" id="hue-demo3" name="BookingOverrideStatuses[text_color]" class="form-control demo" data-control="hue" value=<?=$model->text_color?>>
    </div>


    <?= $form->field($model, 'type')->dropDownList([
                                        BookingOverrideStatuses::TYPE_CUSTOM => 'Custom',
                                        BookingOverrideStatuses::TYPE_DEFAULT => 'Default',
                                        
                                    ]); ?>
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>

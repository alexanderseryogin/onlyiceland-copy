<?php

use yii\db\Migration;

class m161006_143854_create_table_booking_policies extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `booking_policies`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `booking_type_id` INT(11), `booking_status_id` INT(11), `youngest_age` INT(11), `oldest_free_age` INT(11), `payment_methods_gauarantee` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `payment_methods_checkin` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `checkin_time_from` TIME, `checkin_time_to` TIME, `checkout_time_from` TIME, `checkout_time_to` TIME, PRIMARY KEY (`id`), CONSTRAINT `FK_booking_policies_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_policies_booking_type_id` FOREIGN KEY (`booking_type_id`) REFERENCES `booking_types`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_policies_booking_status_id` FOREIGN KEY (`booking_status_id`) REFERENCES `booking_statuses`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161006_143854_create_table_booking_policies cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

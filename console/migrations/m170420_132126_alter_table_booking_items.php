<?php

use yii\db\Migration;

class m170420_132126_alter_table_booking_items extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `guest_user_first_name` VARCHAR(256) NULL AFTER `deleted_at`,
                          ADD COLUMN `guest_user_last_name` VARCHAR(256) NULL AFTER `guest_user_first_name`,
                          ADD COLUMN `guest_user_country` INT(11) NULL AFTER `guest_user_last_name`,
                          ADD CONSTRAINT `FK_booking_items_country_id` FOREIGN KEY (`guest_user_country`) REFERENCES `countries`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
                        ');
    }

    public function down()
    {
        echo "m170420_132126_alter_table_booking_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

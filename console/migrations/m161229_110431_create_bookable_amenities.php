<?php

use yii\db\Migration;

class m161229_110431_create_bookable_amenities extends Migration
{
    public function up()
    {   
        $this->execute("CREATE TABLE `bookable_amenities`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `item_id` INT(11), `amenities_id` INT(11) UNSIGNED, `type` TINYINT(4), PRIMARY KEY (`id`), CONSTRAINT `FK_bookable_amenities_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_bookable_amenities_amenities` FOREIGN KEY (`amenities_id`) REFERENCES `amenities`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161229_110431_create_bookable_amenities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace metronic\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BackendAsset extends AssetBundle
{
    public $sourcePath = '@metronic/resources';
    public $css = [
                // GLOBAL MANDATORY STYLES
        'global/plugins/font-awesome/css/font-awesome.min.css',
        'global/plugins/simple-line-icons/simple-line-icons.min.css',
        //'global/plugins/bootstrap/css/bootstrap.min.css',
        'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',

        // THEME GLOBAL STYLES
        'global/css/components-md.min.css',
        'global/css/plugins-md.min.css',
        
        // BEGIN PAGE LEVEL PLUGINS 
        'global/plugins/select2/css/select2.min.css',
        'global/plugins/select2/css/select2-bootstrap.min.css',

        'global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
        'global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        'global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
        
        'global/plugins/bootstrap-colorpicker/css/colorpicker.css',
        'global/plugins/jquery-minicolors/jquery.minicolors.css',

        'global/plugins/ion.rangeslider/css/ion.rangeSlider.css',
        'global/plugins/ion.rangeslider/css/ion.rangeSlider.skinFlat.css',
        'global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
        //'//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css',
        'global/plugins/clockface/css/clockface.css',
        'global/plugins/ladda/ladda-themeless.min.css',
        'global/plugins/bootstrap-select/css/bootstrap-select.min.css',

        'global/plugins/dropzone/dropzone.min.css',
        'global/plugins/dropzone/basic.min.css',
        'global/plugins/bootstrap-summernote/summernote.css',
        'pages/css/error.min.css',

        'external-plugins/rating/jquery.rateyo.css',
        //'external-plugins/sortable/jquery-sortable.css',
        //'//cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css',

        'global/plugins/datatables/datatables.min.css',
        'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

        // calendar files
        //'fullcalendar/cupertino/jquery-ui.min.css',
        //'fullcalendar/fullcalendar.print.min.css',
        'fullcalendar/fullcalendar.min.css',
        'fullcalendar/scheduler.min.css',

        'global/plugins/jquery-nestable/jquery.nestable.css', 

        // THEME LAYOUT STYLES
        'layouts/layout/css/layout.min.css',
        'layouts/layout/css/themes/darkblue.min.css',
        'layouts/layout/css/custom.min.css',
        
        //Login page
        //'pages/css/login.min.css',
    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [

        // BEGIN CORE PLUGINS 
        //'global/plugins/jquery.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js',
        'global/plugins/bootstrap/js/bootstrap.min.js',
        'global/plugins/js.cookie.min.js',
        'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'global/plugins/jquery.blockui.min.js',
        'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'global/plugins/jquery-validation/js/jquery.validate.min.js',
        
        // BEGIN PAGE LEVEL PLUGINS 
        'global/plugins/jquery-validation/js/jquery.validate.min.js',
        'global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
        'global/plugins/jquery-validation/js/additional-methods.min.js',
        'global/plugins/select2/js/select2.full.min.js',
        'global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',

        // BEGIN THEME GLOBAL SCRIPTS
        'global/scripts/app.min.js',

        'global/plugins/moment.min.js',
        'global/plugins/jquery.table2excel.min.js',
        'global/plugins/jquery.tabletoCSV.js',
        'global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
        'global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        'pages/scripts/components-date-time-pickers.min.js',
        'global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        //'//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js',
        'global/plugins/clockface/js/clockface.js',

        'global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
        'global/plugins/jquery-minicolors/jquery.minicolors.min.js',
        'pages/scripts/components-color-pickers.min.js',

        'global/plugins/ladda/spin.min.js',
        'global/plugins/ladda/ladda.min.js',
        'pages/scripts/ui-buttons.min.js',
        'global/plugins/bootstrap-select/js/bootstrap-select.min.js',
        'pages/scripts/components-bootstrap-select.min.js',

        'global/plugins/dropzone/dropzone.min.js',
        'pages/scripts/form-dropzone.min.js',
      //  'global/plugins/bootstrap-summernote/summernote.min.js',
        'global/plugins/bootstrap-summernote/summernote.js',
        'pages/scripts/components-editors.min.js',

        'global/plugins/ion.rangeslider/js/ion.rangeSlider.min.js',
        'global/plugins/bootstrap-markdown/lib/markdown.js',
        'global/plugins/bootstrap-markdown/js/bootstrap-markdown.js',
        'pages/scripts/components-ion-sliders.min.js',

        'external-plugins/jquery.ajaxfileupload.js',
        'external-plugins/rating/jquery.rateyo.js',
        'external-plugins/sortable/jquery-sortable-min.js',
        'external-plugins/sortable/jquery-sortable.js',
        //'//cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js',

        'global/scripts/datatable.js',
        'global/plugins/datatables/datatables.min.js',
        'global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',
        'pages/scripts/table-datatables-scroller.min.js',

        /*'global/plugins/jquery-ui/jquery-ui.min.js',
        'apps/scripts/calendar.min.js',*/
        'pages/scripts/components-bootstrap-switch.min.js',
        // my changes // 
        'fullcalendar/fullcalendar.min.js',
        'fullcalendar/scheduler.min.js',

        'global/plugins/jquery-nestable/jquery.nestable.js',
        // 'pages/scripts/ui-nestable.min.js',

        // BEGIN THEME LAYOUT SCRIPTS
        'layouts/layout/scripts/layout.min.js',
        'layouts/layout/scripts/demo.min.js',
        'layouts/global/scripts/quick-sidebar.min.js',
        'layouts/global/scripts/quick-nav.min.js',
        //'pages/scripts/login.min.js',

    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
}
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use frontend\components\NotificationFlash;

AppAsset::register($this);

$activeController = strtolower($this->context->id);
$activeAction = strtolower($this->context->action->id);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <!-- Basic Page Needs
          ================================================== -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="onlyiceland site description" name="description" />
        <meta content="M. Saqib Zafar" name="author" />

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <link rel="shortcut icon" href="favicon.ico" />

        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.2/cookieconsent.min.css" />

        <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.2/cookieconsent.min.js"></script>
        <script>
        window.addEventListener("load", function(){
        window.cookieconsent.initialise({
          "palette": {
            "popup": {
              "background": "#000"
            },
            "button": {
              "background": "#f1d600"
            }
          },
          "position": "bottom-left",
          "theme": "edgeless",
          "content": {
            "message": "This website uses cookies to ensure you get the best possible experience.",
            "dismiss": "Got it!",
            "link": "Learn More",
            "href": "#"
          }
        })});
        </script>

        <style type="text/css">
            #mapsvg {
              max-height: 350px;
              padding-top: 30px;
              opacity: 0.8;
            }

            #map_text
            {
              margin-top: -420px;
            }

            .heading
            {
              color:white;
            }

            .checkbox label:after, 
            .radio label:after {
                content: '';
                display: table;
                clear: both;
            }

            .checkbox .cr,
            .radio .cr {
                position: relative;
                display: inline-block;
                border: 1px solid #a9a9a9;
                border-radius: .25em;
                width: 1.3em;
                height: 1.3em;
                float: left;
                margin-right: .5em;
            }

            .radio .cr {
                border-radius: 50%;
            }

            .checkbox .cr .cr-icon,
            .radio .cr .cr-icon {
                position: absolute;
                font-size: .8em;
                line-height: 0;
                top: 50%;
                left: 20%;
            }

            .radio .cr .cr-icon {
                margin-left: 0.04em;
            }

            .checkbox label input[type="checkbox"],
            .radio label input[type="radio"] {
                display: none;
            }

            .checkbox label input[type="checkbox"] + .cr > .cr-icon,
            .radio label input[type="radio"] + .cr > .cr-icon {
                transform: scale(3) rotateZ(-20deg);
                opacity: 0;
                transition: all .3s ease-in;
            }

            .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
            .radio label input[type="radio"]:checked + .cr > .cr-icon {
                transform: scale(1) rotateZ(0deg);
                opacity: 1;
            }

            .checkbox label input[type="checkbox"]:disabled + .cr,
            .radio label input[type="radio"]:disabled + .cr {
                opacity: .5;
            }

            .checkbox label:hover
            {
              cursor: pointer;
            }

        </style>

        <?php $this->head() ?>
    </head>
    <!-- END HEAD -->

    <body class="home">

        <?php $this->beginBody() ?>
            <!--[if lt IE 7]>
                <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
            <![endif]-->
            <div class="body">

              <!-- Start Site Header -->
              <?= $this->render('header_nav')?>
              <!-- End Site Header -->
              <?php if ($activeController != 'site' || $activeAction != 'index')
              {
                ?>
              <!-- Site Showcase -->
              <!-- <div class="site-showcase"> -->
                    <!-- Start Page Header -->
                  <!--  <div class="parallax page-header" style="background-image:url(//placehold.it/1200x260&amp;text=IMAGE+PLACEHOLDER);"> -->
                      <!--<div class="container">
                          <div class="row">
                              <div class="col-md-12">
                                  <h1><?= Html::encode($this->title) ?></h1>
                              </div>
                         </div>-->
                  <!--   </div>
                </div> -->
                <!-- End Page Header -->
              </div>
              <?php } ?>

                <!-- BEGIN PAGE BASE CONTENT -->
                    <?= Alert::widget() ?>

                    <?= $content ?>
                <!-- END PAGE BASE CONTENT -->

              <!-- Start Site Footer -->
              
              <!-- Start sub Footer -->
              <?= $this->render('sub_footer')?>
              <!-- End sub Footer -->

              <!-- Start main Footer -->
              <?= $this->render('footer')?>
              <!-- End main Footer -->

              <!-- End Site Footer -->
              <a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>
            </div>
        <?php $this->endBody() ?>
    </body>
    
</html>
<?php $this->endPage() ?>
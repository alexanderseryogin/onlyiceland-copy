<?php

use yii\db\Migration;

class m170224_141010_alter_bookings_remove_item_rates extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` DROP COLUMN `item_id`, DROP COLUMN `rates_id`, DROP INDEX `FK_bookings_item_id`, DROP INDEX `FK_bookings_rates_id`, DROP FOREIGN KEY `FK_bookings_item_id`, DROP FOREIGN KEY `FK_bookings_rates_id`; 
        ");
    }

    public function down()
    {
        echo "m170224_141010_alter_bookings_remove_item_rates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

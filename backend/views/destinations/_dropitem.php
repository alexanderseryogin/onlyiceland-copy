<div class="row">
    <div class="col-md-2 drop_item">
        <img src="<?=Yii::getAlias('@web').'/../uploads/amenities/'.$value->id.'/icons/'.$value->icon ?>" height="25" width="25" style="margin-left:20px;"> &nbsp; <?=$value['name'];?>
    </div>
    <div class="col-md-2 text-center free drop_item">
        <input type="radio" name="Destinations[amenities_radio][<?=$value->id;?>]" value="<?=$value->id;?>,0" <?=$radioChecked[0];?>>
    </div>
    <div class="col-md-2 text-center extra drop_item">
        <input type="radio" name="Destinations[amenities_radio][<?=$value->id;?>]"  value="<?=$value->id;?>,1" <?=$radioChecked[1];?>>
    </div>
    <div class="col-md-2 text-center not_available drop_item">
        <input type="radio" name="Destinations[amenities_radio][<?=$value->id;?>]" value="<?=$value->id;?>,2" <?=$radioChecked[2];?>>
    </div>
    <div class="col-md-2 text-center not_display drop_item">
        <input type="radio" name="Destinations[amenities_radio][<?=$value->id;?>]" value="<?=$value->id;?>,3" <?=$radioChecked[3];?>>
    </div>
    <div class="col-md-2 text-center drop_item">
        <input name="Destinations[amenities_checkbox][<?=$value->id;?>]" <?=$Checkbox[$isChecked];?> value="<?=$value->id;?>" type="checkbox">
    </div>
</div>
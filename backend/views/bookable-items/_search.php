<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookableItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookable-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'provider_id') ?>

    <?= $form->field($model, 'item_type_id') ?>

    <?= $form->field($model, 'item_quantity') ?>

    <?= $form->field($model, 'how_booked') ?>

    <?php // echo $form->field($model, 'item_names') ?>

    <?php // echo $form->field($model, 'shortest_period') ?>

    <?php // echo $form->field($model, 'longest_period') ?>

    <?php // echo $form->field($model, 'no_of_guests') ?>

    <?php // echo $form->field($model, 'max_adults') ?>

    <?php // echo $form->field($model, 'max_children') ?>

    <?php // echo $form->field($model, 'max_extra_beds') ?>

    <?php // echo $form->field($model, 'max_baby_beds') ?>

    <?php // echo $form->field($model, 'min_age_participation') ?>

    <?php // echo $form->field($model, 'max_age_participation') ?>

    <?php // echo $form->field($model, 'max_free_age') ?>

    <?php // echo $form->field($model, 'background_color') ?>

    <?php // echo $form->field($model, 'text_color') ?>

    <?php // echo $form->field($model, 'include_in_groups') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

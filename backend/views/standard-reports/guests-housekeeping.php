<?php

?>

<div class="tiles">
	<!-- Arrivals  Tile-->
	<!-- <div class="col-md-1"> -->
		<!-- <a href="#" > -->
			<div class="tile bg-green" id="arrival" data-toggle="tooltip" title="Guest arrivals for selected period" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Arrivals </div>
			        <div class="number"> </div>
			    </div>
			</div>
		<!-- </a> -->		
	<!-- </div> -->
	<!-- Departure  Tile-->
	<!-- <div class="col-md-1"> -->
		<!-- <a href="#"> -->
			<div class="tile bg-red-sunglo" id="departure" data-toggle="tooltip" title="Guest departures for selected period" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Departures </div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>		 -->
	<!-- </div> -->
	
	<!-- Current  Tile-->
	<!-- <div class="col-md-2"> -->
		<!-- <a href="#"> -->
			<div class="tile bg-blue" id="current_guests" data-toggle="tooltip" title="Guests staying during selected period" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Current Guests </div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>


			<div class="tile bg-green-meadow" id="daily_unit_activity" data-toggle="tooltip" title="Details of daily guest activity" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Daily Guest Activity </div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>

			<div class="tile bg-blue-hoki" id="daily_unit_activity_org" data-toggle="tooltip" title="Details of daily unit activity" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Daily Unit Activity </div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>


			<div class="tile bg-purple-studio" id="daily_cleaning_breif" data-toggle="tooltip" title="Details of daily cleaning" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Daily Cleaning Brief </div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
			
		<!-- </a>	 -->	
	<!-- </div> -->
	<!-- Current  Tile-->
	<!-- <div class="col-md-2"> -->
		<!-- <a href="#"> -->
			<!-- <div class="tile bg-purple-studio" id="daily_occupancy">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Daily Occupancy </div>
			        <div class="number"> <i data-toggle="tooltip" title="Hooray!" class="fa fa-info-circle" aria-hidden="true"></i> </div> -->
			        <!-- <div class="number"> 12 </div> -->
			   <!--  </div>
			</div> -->
		<!-- </a>	 -->	
	<!-- </div> -->
</div>

<?php
$this->registerJs("
	$(document).ready(function(){
		$('[data-toggle=\"tooltip\"]').tooltip();
	});
	 
");

?>

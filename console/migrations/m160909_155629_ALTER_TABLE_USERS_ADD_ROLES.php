<?php

use yii\db\Migration;

class m160909_155629_ALTER_TABLE_USERS_ADD_ROLES extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `user` ADD COLUMN `type` TINYINT NOT NULL AFTER `email`; ');
    }

    public function down()
    {
        echo "m160909_155629_ALTER_TABLE_USERS_ADD_ROLES cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

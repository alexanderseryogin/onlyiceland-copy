<?php

use yii\db\Migration;

class m161224_093246_create_providers_open_all_years extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_open_all_year`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `jan` TINYINT(1), `feb` TINYINT(1), `march` TINYINT(1), `april` TINYINT(1), `may` TINYINT(1), `june` TINYINT(1), `july` TINYINT(1), `august` TINYINT(1), `sep` TINYINT(1), `oct` TINYINT(1), `nov` TINYINT(1), `dec` TINYINT(1), PRIMARY KEY (`id`), CONSTRAINT `FK_providers_open_all_year_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161224_093246_create_providers_open_all_years cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

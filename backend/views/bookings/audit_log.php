<?php

use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<div class="tab-pane" id="audit_log">

<?php Pjax::begin(['timeout' => 1000000, 'enablePushState' => false]) ?>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        //['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'User',
            'value' => function($data)
            {
                return $data->user_id;
            },
        ],
        [
            'attribute' => 'date',
            'value' => function($data)
            {
                return $data->date;
            },
            'contentOptions' =>['style' => 'min-width:150px;'],
            'headerOptions' =>['style' => 'min-width:150px;'],
        ],
        'field_name',
        [
            'label' => 'Old Value',
            'value' => function($data)
            {
                return $data->old_value;
            },
        ],
        [
            'label' => 'New Value',
            'value' => function($data)
            {
                return $data->new_value;
            },
        ],
    ],
]); ?>

<?php Pjax::end() ?>

</div>
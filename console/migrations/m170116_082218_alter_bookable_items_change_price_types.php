<?php

use yii\db\Migration;

class m170116_082218_alter_bookable_items_change_price_types extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` CHANGE `min_price` `min_price` INT(11) NULL, CHANGE `max_price` `max_price` INT(11) NULL; 
        ");
    }

    public function down()
    {
        echo "m170116_082218_alter_bookable_items_change_price_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

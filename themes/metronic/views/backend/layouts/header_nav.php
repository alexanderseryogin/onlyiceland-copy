<?php
use common\models\Notifications;
$notification_count = count(Notifications::find()->where(['status' => Notifications::NOTIFICATION_UNSEEN])->all()); 
$notifications = Notifications::find()->where(['status' => Notifications::NOTIFICATION_UNSEEN])->orderBy(['created_at' => SORT_DESC])->limit(10)->all(); 
?>
<div class="page-header-inner ">
    <!-- BEGIN LOGO -->
    <div class="page-logo">
        <a href="<?= \yii\helpers\Url::to(['/']) ?>">
            <img style="margin-top: 2px;max-height: 44px;" src="<?= Yii::getAlias('@web')?>/assets_b/img/oi_logo.png" alt="logo" class="logo-default" /> 
        </a>
        <div class="menu-toggler sidebar-toggler">
            <span></span>
        </div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        <span></span>
    </a>

    <!-- END RESPONSIVE MENU TOGGLER -->
    <!-- BEGIN TOP NAVIGATION MENU -->
    

    <div class="page-top">
        <div class="btn-group" style="margin:8px;">
            <button type="button" class="btn btn-circle btn-outline green dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-plus"></i>
                <span class="hidden-sm hidden-xs">Actions</span>
                <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="<?php echo \yii\helpers\Url::to(['/bookings/create']) ?>">
                        <i class="fa fa-book"></i>&nbsp;Add New Booking</a>
                </li>
                <li>
                    <a href="<?php echo \yii\helpers\Url::to(['/bookings']) ?>">
                        <i class="fa fa-list"></i>&nbsp;View Bookings List</a>
                </li>
                <li>
                    <a href="<?php echo \yii\helpers\Url::to(['/bookings/calendar']) ?>">
                        <i class="fa fa-calendar"></i>&nbsp;View Bookings Calendar</a>
                </li>
            </ul>
        </div>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-bell"></i>
                        <span class="badge badge-danger"> <?= $notification_count ?> </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>You have
                                <span class="bold"><?= $notification_count ?> New</span> Alerts</h3>
                            <a href="<?= \yii\helpers\Url::to(['/notifications']) ?>">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                <?php 
                                foreach ($notifications as  $notification) { ?>
                                <li>
                                    <a href="<?=$notification->getNotificationUrl($notification->date)?>">
                                        <!-- <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span> -->
                                        <span class="subject" style="margin-left: 0px;">
                                            <span class="from"> <?=isset($notification->bookable_item_id)?$notification->bookableItem->itemType->name:$notification->destination->name?> </span>
                                            <!-- <span class="time">Just Now </span> -->
                                        </span>
                                        <span class="message" style="margin-left: 0px;"> <?=$notification->message?> </span>
                                    </a>
                                </li>
                                <?php } ?>
                                <!-- <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Richard Doe </span>
                                            <span class="time">16 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar1.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Bob Nilson </span>
                                            <span class="time">2 hrs </span>
                                        </span>
                                        <span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Lisa Wong </span>
                                            <span class="time">40 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Richard Doe </span>
                                            <span class="time">46 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?php if(empty(Yii::$app->user->identity->profile->profile_picture)){echo Yii::getAlias('@web').'/assets_b/img/avatar.png';}else{echo Yii::getAlias('@web').'/../uploads/user-profile/'.Yii::$app->user->identity->profile->profile_picture;} ?>"/>
                        <span class="username username-hide-on-mobile"> <?= Yii::$app->user->identity->username ?> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['/user-profile']) ?>">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <!--
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['/site/change-password']) ?>">
                                <i class="icon-key"></i> Change Password </a>
                        </li>
                        -->
                        <!-- <li>
                            <a href="app_inbox.html">
                                <i class="icon-envelope-open"></i> My Inbox
                                <span class="badge badge-danger"> 3 </span>
                            </a>
                        </li>
                        <li>
                            <a href="app_todo.html">
                                <i class="icon-rocket"></i> My Tasks
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li> -->
                        <li class="divider"> </li>
                        <!-- <li>
                            <a href="page_user_lock_1.html">
                                <i class="icon-lock"></i> Lock Screen </a>
                        </li> -->
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-quick-sidebar-toggler" style="display: none;">
                    <a href="javascript:;" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <!-- <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="javascript:;" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                    </a>
                </li> -->
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
    </div>
    <!-- END TOP NAVIGATION MENU -->
</div>

<?php

$this->registerJs("

    $(document).ready(function()
    {
        $('.dropdown-toggle').dropdown();
    });

");?>
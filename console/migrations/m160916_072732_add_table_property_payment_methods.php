<?php

use yii\db\Migration;

class m160916_072732_add_table_property_payment_methods extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `property_payment_methods`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) NOT NULL, `type` TINYINT NOT NULL DEFAULT 0, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; ");
    }

    public function down()
    {
        echo "m160916_072732_add_table_property_payment_methods cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

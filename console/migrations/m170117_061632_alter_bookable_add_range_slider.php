<?php

use yii\db\Migration;

class m170117_061632_alter_bookable_add_range_slider extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `age_range_children` VARCHAR(10) NULL AFTER `general_booking_cancellation`, ADD COLUMN `min_age_adults` VARCHAR(10) NULL AFTER `age_range_children`; 
        ");
    }

    public function down()
    {
        echo "m170117_061632_alter_bookable_add_range_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

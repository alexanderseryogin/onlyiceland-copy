<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\BookingsItems;
use common\models\BookingDateFinances;
use common\models\DestinationAddOns;
use common\models\PaymentMethodCheckin;
use common\models\TravelPartner;
// $booking_dates = $model->bookingDatesSortedAndNotNull;
$currentDate =  date('d-m-Y');
$destination_id = $model->provider_id;

$total_debit = BookingDateFinances::find()->where(['booking_item_id' => $model->id])
										  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
										  ->sum('final_total');

$total_credit = BookingDateFinances::find()->where(['booking_item_id' => $model->id])
										  ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
										  ->sum('final_total');
$total_debit = empty($total_debit)?0:$total_debit;
$total_credit =  empty($total_credit)?0:$total_credit;		

$balance_due = $total_debit - $total_credit; 

$rates_arr = array();
foreach ($model->bookingDatesSortedAndNotNull as $value) 
{
    if(!in_array($value->rate, $rates_arr))
    {
        array_push($rates_arr, $value->rate);
    }
}
?>
<script type="text/javascript">
	var global_payment = <?=$model->balance?>;
	console.log(global_payment);
</script>
<div class="portlet box green-meadow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-minus"></i>&nbsp;&nbsp;&nbsp;Charges / Debits </div>
        <div class="tools">
            <a href="javascript:void(0);" class="collapse"> </a>
           <!--  <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="" class="fullscreen"> </a>
            <a href="javascript:;" class="reload"> </a> -->
        </div>
    </div>
    <div class="portlet-body"> 
    	<!-- <div class="row">
		    <div class="col-md-4 col-md-offset-5">
		          <h3><b>Charges</b></h3>
		    </div>  
		</div> -->
		<a href="javascript:void(0)" id="add_custom_charge" class="btn btn-default pull-right hide">Add Custom Charge</a>
		<a href="javascript:void(0)" id="add_add_on_btn" class="btn btn-primary pull-right hide">Add new Add-On/Upsell</a>
		
		<!-- <a href="javascript:void(0)" id="test_button" class="btn btn-primary pull-right">Test</a> -->
		<br>
		<!-- <input type="button" class="btn btn-success" ></input>
		<input type="button" class="btn btn-primary" ></input> -->
		<div id="table_charges_div" class="table_charges_div">
			<table id="charges_table" class="table">
			    <thead>
			        <tr>
			        <!-- <th>Id </th> -->
			        <th style="width: 1%;"><i class="fa fa-eye"  title="Hide on Receipt" data-toggle="tooltip"></i></th>
			        <th style="width: 8%">Disable Discount</th>
			        <th style="width: 8%">Date</th>
			        <th style="width: 25%;">Description</th>
			        <th style="width: 4%;">Qty</th>
			        <th style="width: 7%;">Price</th>
			        <th style="width: 6%;">VAT %</th>
			        <th style="width: 6%;">Tax</th>
			        <!-- <th style="width: 6%;">Discount %</th> -->

			        <th style="width: 6%;">
			        	<?php
			        		if($model->travel_partner_id != null)
			        		{
			        			if($model->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
			        			{
			        				echo "Comm. % ";
			        			}
			        			else
			        			{
			        				echo "Discount %";
			        			}
			        		}
			        		else
			        		{
			        			echo "Discount %";
			        		}
			        	?>
			        </th>

			        <!-- <th style="width: 10%;">Discount Amount</th> -->
			        <th style="width: 7%;">
			        	<?php
			        		if($model->travel_partner_id != null)
			        		{
			        			if($model->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
			        			{
			        				echo "Comm. Amount  ";
			        			}
			        			else
			        			{
			        				echo "Discount Amount";
			        			}
			        		}
			        		else
			        		{
			        			echo "Discount Amount";
			        		}
			        	?>
			        </th>
			        <th style="width: 8%;">VAT Amount</th>
			        <th style="width: 8%;">Charge/Debit Amount</th>
			        <th style="width: 5%;"> Actions</th>
			      </tr>
			    </thead>
			    <tbody id="charges-row">
			    	<?php 
			    		//$counter = 1;
			    		// foreach ($booking_dates as $booking_date) {
		    			if(!empty($model->bookingDateCharges)){
		    				// $count = count($booking_date->bookingDateCharges)*count($booking_dates);
		    				foreach ($model->bookingDateCharges as $bookingDateFinance) {
		    					// if($bookingDateFinance->item_type == BookingDateFinances::ITEM_TYPE_BOOKING || $bookingDateFinance->item_type == BookingDateFinances::ITEM_TYPE_UPSELL )
		    					if($bookingDateFinance->entry_type == BookingDateFinances::ENTRY_TYPE_SYSTEM){
			    	?>
			    	<tr id="<?=$bookingDateFinance->id?>">
			    		<td>
			    			<input class="checkbox" type="checkbox" name="" <?= ($bookingDateFinance->show_receipt == BookingDateFinances::HIDE_RECEIPT)?"checked":"" ?> >
			    		</td>
			    		<td>

			    		</td>
			    		<td>
			    			
					        <input   class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=date('d-m-Y',strtotime($bookingDateFinance->date))?>" disabled>
					        		
			    		</td>
			    		
			    			<?php 
			    				$all_add_ons = DestinationAddOns::find()->where(['destination_id' => $model->provider_id])->all();
			    				$selected_add_on = DestinationAddOns::findOne(['id' => $bookingDateFinance->price_description]);
			    				if(empty($selected_add_on)){
			    			?>
			    			<td>
			    				<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" readonly>
			    			</td>
			    			<?php } else { 
			    					echo '<td class="hasSelect"><select data-placeholder="Select an Option" class="form-control add_on_dropdown " style="width:83%;float:right;">';
						            echo '<option></option>';
					                foreach ($all_add_ons as $add_on) 
					                {
					                	if($bookingDateFinance->price_description == $add_on->id)
					                	{
					                    	echo '<option  value="'.$add_on->id.'" selected>'.$add_on->name.'</option>';
					                	}
					                	else
					                	{
					                		echo '<option value="'.$add_on->id.'">'.$add_on->name.'</option>';
					                	}
					                }
					               
						            echo '</select></td>';
			    			}?>
			    		
			    		<td>
			    			<input class="form-control " type="text" name="" value="<?= isset($bookingDateFinance->quantity)?$bookingDateFinance->quantity:"" ?>" style="text-align: right" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control " type="text" name="" value="<?= isset($bookingDateFinance->amount)?Yii::$app->formatter->asDecimal($bookingDateFinance->amount,0):""?>" style="text-align: right" readonly >
			    		</td>
			    		<td>
			    			<?php
			    				$temp = explode('.', $bookingDateFinance->vat);
			    				if(isset($temp[1]))
			    				{
			    					$temp[1].='00000';
			    					$temp[1] = substr($temp[1], 0, 2);
			    					$temp[0]=$temp[0].','.$temp[1];
			    					$bookingDateFinance->vat = $temp[0];
			    				}
			    				else
			    				{
			    					$bookingDateFinance->vat.=',00';
			    				}
			    			 	
			    			?>
			    			<input class="form-control " type="text" name="" value="<?= isset($bookingDateFinance->vat)?$bookingDateFinance->vat:"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control " type="text" name="" value="<?= isset($bookingDateFinance->tax)?Yii::$app->formatter->asDecimal($bookingDateFinance->tax,0):"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<?php
			    			?>
			    			<input class="form-control " type="text" name="" value="<?= isset($bookingDateFinance->discount)?$bookingDateFinance->discount:"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control " type="text" name="" value="<?= isset($bookingDateFinance->discount_amount)?Yii::$app->formatter->asDecimal($bookingDateFinance->discount_amount,2):"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control vat_amount" type="text" name="" value="<?= isset($bookingDateFinance->vat_amount)?Yii::$app->formatter->asDecimal($bookingDateFinance->vat_amount,2):"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control total" type="text" name="" value="<?= isset($bookingDateFinance->final_total)?Yii::$app->formatter->asDecimal($bookingDateFinance->final_total,0):"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			
		                	

			    		</td>
			    	</tr>
			    	<?php 			//$counter++;
			    					//echo $counter;
			    				}
			    				else
			    				{	?>
			    	<tr id="<?=$bookingDateFinance->id?>" class="custom">
			    		<td>
			    			<input class="checkbox" type="checkbox" name="" <?= ($bookingDateFinance->show_receipt == BookingDateFinances::HIDE_RECEIPT)?"checked":"" ?> >
			    		</td>
                        <td>
                            <input class="checkbox disable_discount_checkbox" type="checkbox" name="disable_discount_checkbox-<?=$bookingDateFinance->id?>"  <?= ($bookingDateFinance->discount_disable == 1)?"checked":"" ;?>>
                        </td>
			    		<td>
			    			
					        <input   class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=date('d-m-Y',strtotime($bookingDateFinance->date))?>" >
					        		
			    		</td>
			    		
			    			<?php 
			    				$all_add_ons = DestinationAddOns::find()->where(['destination_id' => $model->provider_id])->all();
			    				$selected_add_on = DestinationAddOns::findOne(['id' => $bookingDateFinance->price_description]);
			    
			    					echo '<td class="hasSelect">';?>
			    					<div class="form-group" style="margin-bottom: 0px;">
				    					<div class="input-group" style="text-align:left">
				    						<span class="input-group-btn">
								                <a href="javascript:;" class="btn btn-default btn-small show_dropdown" >
								                   <i class="fa fa-caret-down" aria-hidden="true"></i></a>
								            </span>
								            <!-- <input type="text" class="form-control" name="username1" id="username1_input"> -->
								            <input class="form-control" type="text" name="" value="<?=isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:""?>" >
								        

			    					<!-- <button class="btn btn-default btn-small show_dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></button>
			    					<input class="form-control" type="text" name="" value="<?=isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:""?>" style="float: right;width: 83%;"> -->
			    					<?php
			    					echo '<select data-placeholder="Select an Option" class="form-control add_on_dropdown" style="width:100%;">';
						            echo '<option></option><optgroup label="Add-Ons">';
					                foreach ($all_add_ons as $add_on) 
					                {
					                	if($bookingDateFinance->price_description == $add_on->id && $bookingDateFinance->item_type == BookingDateFinances::ITEM_TYPE_ADDON)
					                	{
					                    	echo '<option data-type=3 value="'.$add_on->id.'" selected>'.$add_on->name.'</option>';
					                	}
					                	else
					                	{
					                		echo '<option data-type=3 value="'.$add_on->id.'">'.$add_on->name.'</option>';
					                	}
					                }
					                echo "</optgroup>";
					                if(!empty($rates_arr))
					                {
					                    foreach ($rates_arr as $rate) 
					                    {
					                        if(!empty($rate->ratesUpsells))
					                        {
					                            echo '<optgroup label="Upsells">';
					                            foreach ($rate->ratesUpsells as $rate_upsell) 
					                            {
					                            	if($bookingDateFinance->price_description == $rate_upsell->id && $bookingDateFinance->item_type == BookingDateFinances::ITEM_TYPE_UPSELL)
								                	{
								                		// echo "here";
								                		// exit();
								                    	echo '<option data-type=2 value="'.$rate_upsell->id.'" selected>'.$rate->name.'-'.$rate_upsell->upsell->name.'</option>';
								                	}
								                	else
								                	{
					                                	echo '<option data-type=2 value="'.$rate_upsell->id.'">'.$rate_upsell->upsell->name.'</option>';
					                                }
					                            }
					                           	echo '</optgroup>';
					                        }
					                        
					                    }
					                }

						            echo '</select>';
			   				?>
			   					</div>
							</div>
			   			</td>
			    		
			    		<td>
			    			<input class="form-control qty" type="text" name="" value="<?= isset($bookingDateFinance->quantity)?$bookingDateFinance->quantity:"" ?>">
			    		</td>
			    		<td>
			    			<input class="form-control price" type="text" name="" value="<?= isset($bookingDateFinance->amount)?Yii::$app->formatter->asDecimal($bookingDateFinance->amount,0):""?>" style="text-align: right;" >
			    		</td>
			    		<td>
			    			<?php 
			    				$temp = explode('.', $bookingDateFinance->vat);
			    				if(isset($temp[1]))
			    				{
			    					$temp[1].='00000';
			    					$temp[1] = substr($temp[1], 0, 2);
			    					$temp[0].=$temp[1];
			    					$bookingDateFinance->vat = $temp[0];
			    				}
			    				else
			    				{
			    					$bookingDateFinance->vat.='.00';
			    				}
			    			 	
			    			?>
			    			<input class="form-control vat" type="text" name="" value="<?= isset($bookingDateFinance->vat)?$bookingDateFinance->vat:"0"?>" style="text-align: right;" >
			    		</td>
			    		<td>
			    			<input class="form-control tax" type="text" name="" value="<?= isset($bookingDateFinance->tax)?$bookingDateFinance->tax:"0"?>" style="text-align: right;" >
			    		</td>
			    		<td>
			    			<?php
			    			?>
			    			<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->discount)?$bookingDateFinance->discount:"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control discount_amount" type="text" name="" value="<?= isset($bookingDateFinance->discount_amount)?Yii::$app->formatter->asDecimal($bookingDateFinance->discount_amount,2):"0"?>" style="text-align: right;" <?= ($bookingDateFinance->discount_disable == 1)?"disabled":"" ;?>>
			    		</td>
			    		<td>
			    			<input class="form-control vat_amount" type="text" name="" value="<?= isset($bookingDateFinance->vat_amount)?Yii::$app->formatter->asDecimal($bookingDateFinance->vat_amount,2):"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<input class="form-control total" type="text" name="" value="<?= isset($bookingDateFinance->final_total)?Yii::$app->formatter->asDecimal($bookingDateFinance->final_total,0):"0"?>" style="text-align: right;" readonly>
			    		</td>
			    		<td>
			    			<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-charges hide" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus" aria-hidden="true" ></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-charges" title="Delete" data-toggle="tooltip"><i class="fa fa-minus"  aria-hidden="true"></i></a>
			    		</td>

			    	</tr>

			    				<?php
			    				}
			    			}
			    		}
			    	 ?>
			    </tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-8">
				<p style="text-align: right;"><b>Total Charges/Debits:</b></p>
			</div>
			<div class="col-md-2">
				<input id="total_charges" class="form-control" type="text" readonly style="width: 65%;text-align: right"; value="<?=Yii::$app->formatter->asDecimal($total_debit,0)?>">
			</div>
		</div>
    </div>
</div>

<br>


<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus" style="margin-top:-1px"></i>&nbsp;&nbsp;&nbsp;Payments/Credits</div>
        <div class="tools">
            <a href="javascript:void(0);" class="collapse"> </a>
           <!--  <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="" class="fullscreen"> </a>
            <a href="javascript:;" class="reload"> </a> -->
        </div>
    </div>
    <div class="portlet-body"> 
    	<a href="javascript:void(0)" id="add_custom_credit_btn" class="btn btn-default pull-right hide">Add Custom Credit</a>
    	<a href="javascript:void(0)" id="add_credit_btn" class="btn btn-primary pull-right hide">Add new Credit</a>   	
    	<br>
    	<div class="table_payments_div">
	    	<table id="payments_table" class="table">
			    <thead>
			      <tr>
			        <!-- <th>Id </th> -->
			        <th style="width: 1%;"><i class="fa fa-eye"  title="Hide on Receipt" data-toggle="tooltip"></th>
			        <th style="width: 8%">Date</th>
			        <th style="width: 40%;">Payment/Credit Type</th>
			        <th style="width: 10%;">Payment Type</th>
			        <th style="width: 10%;">Deposit %</th>
			        <th style="width: 9%;"></th>
			        <th style="width: 8%;"></th>
			        <th style="width: 10%;">Payment/Credit Amount</th>
			        <th style="width: 5%;"> Actions</th>
			      </tr>
			    </thead>
			    <tbody id="payments-row">
			    	<?php 
			    		//$counter = 1;
			    		// foreach ($booking_dates as $booking_date) {
		    			if(!empty($model->bookingDatePayments)){
		    				// $count = count($booking_date->bookingDateCharges)*count($booking_dates);
		    				foreach ($model->bookingDatePayments as $bookingDateFinance) {
								if($bookingDateFinance->entry_type == BookingDateFinances::ENTRY_TYPE_SYSTEM){
			    	?>
			    	<tr id="<?=$bookingDateFinance->id?>" class="">
			    		<td>
			    			<input class="checkbox" type="checkbox" name="" <?= ($bookingDateFinance->show_receipt == BookingDateFinances::HIDE_RECEIPT)?"checked":"" ?> >
			    		</td>
			    		<td>
			    		
					      <input   class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=date('d-m-Y',strtotime($bookingDateFinance->date))?>" >
					        		
			    		</td>
		    			<td>
		    				<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" >
		    			</td>
		    						    			
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			<input class="form-control total" type="text" name="" style="text-align: right;" value="<?= isset($bookingDateFinance->final_total)?Yii::$app->formatter->asDecimal($bookingDateFinance->final_total,0):"0"?>"  readonly>
			    		</td>
			    		<td>
			    			<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-payments hide" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus " aria-hidden="true" ></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments hide" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>

			    		</td>
			    	</tr>
			    	<?php } else { ?>
			    	<tr id="<?=$bookingDateFinance->id?>" class="custom">
			    		<td>
			    			<input class="checkbox" type="checkbox" name="" <?= ($bookingDateFinance->show_receipt == BookingDateFinances::HIDE_RECEIPT)?"checked":"" ?> >
			    		</td>
			    		<td>
			    		
					      <input   class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=date('d-m-Y',strtotime($bookingDateFinance->date))?>">
					        		
			    		</td>
		    			<?php 
		    				$all_payment_methods = PaymentMethodCheckin::find()->where(['provider_id' => $model->provider_id])->all();
		    				//$selected_payment_methods = PaymentMethodCheckin::findOne(['id' => $bookingDateFinance->price_description]);
		    			?>
		    			<!-- <td>
		    				<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" >
		    			</td> -->
		    			<?php 
		    					echo '<td class="hasSelect">';?>
		    					<div class="form-group" style="margin-bottom: 0px;">
			    					<div class="input-group" style="text-align:left">
			    						<span class="input-group-btn">
							                <a href="javascript:;" class="btn btn-default btn-small show_dropdown" >
							                   <i class="fa fa-caret-down" aria-hidden="true"></i></a>
							            </span>
							            <!-- <input type="text" class="form-control" name="username1" id="username1_input"> -->
							            <input class="form-control" type="text" name="" value="<?=isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:""?>" >
<!-- 
		    					<button class="btn btn-default btn-small show_dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></button>
		    					<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" style="float: right; width: 92%;"> -->
		    					<?php 
		    					echo '<select data-placeholder="Select an Option" class="form-control payment_method_dropdown " style="width:100%">';
					            echo '<option></option>';
				                foreach ($all_payment_methods as $payment_method) 
				                {
				                	
				                	echo '<option value="'.$payment_method->id.'">'.$payment_method->pm->name.'</option>';
				                	
				                }
					            echo '</select></div></div></td>';
		    			?>
			    			
			    		<td>
			    			<?php if($bookingDateFinance->payment_type){?>
			    				<span class="label label-info">Deposit</span>
			    			<?php } else{?>
			    				<span class="label label-default">Normal</span>
			    			<?php }?>
			    		</td>
			    		<td>
			    			<?php if($bookingDateFinance->payment_type){?>
			    				<?=$bookingDateFinance->deposit_percent_age?>
			    			<?php } else{?>
			    				
			    			<?php }?>
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			<input class="form-control total" type="text" name="" style="text-align: right;" value="<?= isset($bookingDateFinance->final_total)?Yii::$app->formatter->asDecimal($bookingDateFinance->final_total,0):"0"?>" >
			    		</td>
			    		<td>
			    			<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-payments hide" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus " aria-hidden="true" ></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>

			    		</td>
			    	</tr>
			    	<?php 		
			    				}	
			    			}
			    		}
			    	 ?>
			    </tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-8">
				<p style="text-align: right"><b>Total Payments/Credits:</b></p>
			</div>
			<div class="col-md-2">
				<input id="total_payment" class="form-control" type="text" readonly  style="width: 65%;text-align: right";  value="<?=Yii::$app->formatter->asDecimal($total_credit,0)?>">
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-1 col-md-offset-8" style="width: 12.5%;">
				<p><b>Balance Due</b></p>
			</div>
			<div class="col-md-2 ">
				<input id="total_due" class="form-control" type="text" readonly style="width: 90%;text-align: right";>
			</div>
		</div> -->
    </div>
</div>
<div class="row">
	<div class="col-md-2 col-md-offset-8">
		<p style="font-size:20px;text-align: right"><b>Balance Due:</b></p>
	</div>
	<div class="col-md-1" style="text-align: right">
		<span style="font-size:20px;text-align:right"><b id="total_due"><?=Yii::$app->formatter->asDecimal($model->balance,0)?></b></span>
		
		<!-- <input id="total_due" class="form-control" type="text" readonly style="width: 90%;text-align: right"; value="<?=Yii::$app->formatter->asDecimal($model->balance,0)?>"> -->
	</div>
</div>




<?php
$get_add_on_details = Url::to(['/bookings/get-add-on-details']);
$get_price_for_debit_row = Url::to(['/bookings/calculate-price']);
$add_charge_entry = Url::to(['/bookings/add-finance-entry']);
$delete_charge_entry = Url::to(['/bookings/delete-finance-entry']);
$add_payment_entry = Url::to(['/bookings/add-payment-entry']);
$delete_payment_entry = Url::to(['/bookings/delete-payment-entry']);
$calculate_deposit = Url::to(['/bookings/calculate-item-deposit']);
$calculate_percentage = Url::to(['/bookings/calculate-item-deposit-percentage']);
$this->registerJs("

			// Charges tables Js
		    $(document).on('click','#add_custom_charge',function()
		    {
		   		//alert('clicked');
		    	var htmlCode = '".BookingsItems::getChargesRow()."';
        		$('#charges-row').append(htmlCode);
        		$('.calendar').datepicker();
        		$('.price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.vat').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.tax').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.discount_amount_new').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        		//alert('htmlCode');
        		$('[data-toggle=\'tooltip\']').tooltip(); 
		    });
		  //   $(document).on('click','.delete-dynamic-row-charges',function(event)
		  //   {
		  //   	//alert('here');
		  //   	//console.log($(this).parent().parent().parent());
		  //   	if (confirm('Are you sure, You want to remove')) {
			 //    //alert('Thanks for confirming');
		  //   		$(this).parent().parent().parent().remove();
				// } 
		        
		  //   });

		    $(document).on('click','#add_add_on_btn',function()
		    {
		   		//alert('clicked');
		    	var htmlCode = '".BookingsItems::getAddOnDropdownRow($destination_id,$model->id)."';
		    	if(htmlCode == '')
		    	{
		    		alert('No add on available for this destination');
		    	}
		    	else
		    	{
		    		$('#charges-row').append(htmlCode);
        			// $('.add_on_dropdown');
        			$('.calendar').datepicker();
        			$('.price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
			    	$('.vat').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
			    	$('.tax').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
			    	$('.discount_amount_new').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
			    	$('[data-toggle=\'tooltip\']').tooltip(); 
			    	$('.add_on_dropdown').select2({
			    		placeholder: 'Select an Option',
			    		// width: '50%',
		    			containerCssClass : 'hide custom_class_add_on'
  						//allowClear: true
			    	}).on('select2:close', function (e) {
			    			// log('select2:close', e);
			    			console.log('event triggered');
			    			$(this).select2().data('select2').\$container.addClass('hide');
			    			$(this).prev().removeClass('hide');
			    			console.log($(this));
			    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
			    			// elements.eq(0).removeClass('hide');
			    	 });
		    	}
        		
        		//alert('htmlCode');
		    });

		    $(document).on('change','.add_on_dropdown',function()
		    {
		   		var id = $(this).val();
		   		var input_this = $(this);
		   		//alert($(this).find(':selected').text());
		   		var type= $(this).find(':selected').attr('data-type')
		   		//alert(type);
		   		console.log('id :'+id);
		   		var charge_cell = $(this).parent().parent().parent().nextAll();
		   		var prev_charge_cell = $(this).prevAll();
		   		//console.log(prev_charge_cell);
		   		//console.log(charge_cell);
		   		console.log(charge_cell.eq(0).children('input'));
		   		if(id != 0)
		   		{
		   			$.ajax(
			        {
			            type: 'POST',
			            url: '$get_add_on_details',
			            data: {
			            	id:id,
			            	type:type,
			            },
			            beforeSend: function(){
			     			App.blockUI({target:'.table_charges_div', animate: !0});
					    },
			            success: function(data)
			            {
			                data = jQuery.parseJSON( data );
			                console.log(data);
			                charge_cell.eq(1).children('input').first().val(data.price);
			                charge_cell.eq(2).children('input').first().val(data.vat);
			                charge_cell.eq(3).children('input').first().val(data.tax);
			                console.log('before: '+input_this);
			            	calculatePrice(input_this,1);
			            	//console.log();
			            	prev_charge_cell.eq(0).removeClass('hide');
			            	prev_charge_cell.eq(0).val(input_this.find(':selected').text());
			            	prev_charge_cell.eq(0).trigger('change');
			            	input_this.select2().data('select2').\$container.addClass('hide');
			            	//alert();
			            },
			            complete: function()
			            {
			                App.unblockUI('.table_charges_div');
			                // alert('unblocked')
			            },
			            error: function()
			            {
			                alert('error');
			            }
			        });
		   		}
		   		else
		   		{
		   			charge_cell.eq(1).children('input').first().val(0);
			        charge_cell.eq(2).children('input').first().val(0);
			        charge_cell.eq(3).children('input').first().val(0);
			        charge_cell.eq(4).children('input').first().val(0);
			        charge_cell.eq(5).children('input').first().val(0);
			        charge_cell.eq(6).children('input').first().val(0);
			        charge_cell.eq(7).children('input').first().val(0);
		   		}
			    	
		    });

		    $(document).on('change','.payment_method_dropdown',function()
		    {
		   		var id = $(this).val();
		   		var input_this = $(this);
		   		//alert($(this).find(':selected').text());
		   		var type= $(this).find(':selected').attr('data-type')
		   		//alert(type);
		   		console.log('id :'+id);
		   		var charge_cell = $(this).parent().nextAll();
		   		var prev_charge_cell = $(this).prevAll();
		   		console.log(charge_cell.eq(0).children('input'));
		   		prev_charge_cell.eq(0).removeClass('hide');
			    prev_charge_cell.eq(0).val(input_this.find(':selected').text());
			    prev_charge_cell.eq(0).trigger('change');
			    input_this.select2().data('select2').\$container.addClass('hide');
			    	
		    });

		    $(document).on('change','.payment_percentage_dropdown',function()
		    {
		   		var id = $(this).val();
		   		var input_this = $(this);
		   		//alert($(this).find(':selected').text());
		   		var type= $(this).find(':selected').attr('data-type')
		   		//alert(type);
		   		console.log('id :'+id);
		   		var charge_cell = $(this).parent().nextAll();
		   		var prev_charge_cell = $(this).prevAll();
		   		console.log(charge_cell.eq(0).children('input'));
		   		prev_charge_cell.eq(0).removeClass('hide');
			    prev_charge_cell.eq(0).val(input_this.find(':selected').text());
			    prev_charge_cell.eq(0).trigger('change');
			    input_this.select2().data('select2').\$container.addClass('hide');
			    	
		    });


		    // Payments table Js
		    $(document).on('click','#add_custom_credit_btn',function()
		    {
		   		//alert('clicked');
		    	var htmlCode = '".BookingsItems::getPaymentsRow()."';
        		$('#payments-row').append(htmlCode);
        		$('.calendar').datepicker();
        		// $('.total').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});        		
        		//alert('htmlCode');
        		$('[data-toggle=\'tooltip\']').tooltip(); 
		    });


		    $(document).on('click','#add_credit_btn',function()
		    {
		   		//alert('clicked');
		    	var htmlCode = '".BookingsItems::getPaymentmethodsDropdown($destination_id,$model->id)."';
		    	//alert(htmlCode);
		    	if(htmlCode == '')
		    	{
		    		alert('No payment methods for this destination');
		    	}
		    	else
		    	{
		    		$('#payments-row').append(htmlCode);
        			$('.calendar').datepicker(); 
        			//$('.payment_method_dropdown');
        			// $('.total').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        			//$('.calendar').datepicker();
        			$('[data-toggle=\'tooltip\']').tooltip(); 
        			$('.payment_method_dropdown').select2({

		    									placeholder: 'Select an option',
  												//allowClear: true,
		    									width: '90%',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });
					$('.payment_percentage_dropdown').select2({
												minimumResultsForSearch: -1,
		    									placeholder: 'Select an option',
  												//allowClear: true,
		    									width: '90%',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });
		    	}
		    	$('.payment-switch').bootstrapSwitch();
        		
        		//alert('htmlCode');
		    });

		    ////////////////////////////// Testing Function ////////////////////////////////////
		    $(document).ready(function(){

		    	$('.vat').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.discount').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

		    	////////////// Adding extra rows when page loads //////////////////

		    	///////////// Charges Row ////////////

		    	var htmlCode = '".BookingsItems::getAddOnDropdownRow($destination_id,$model->id)."';
        		$('#charges-row').append(htmlCode);
        		$('.calendar').datepicker();
        		$('.price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.vat').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.tax').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.discount_amount_new').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	$('.add_on_dropdown').select2({
		    									placeholder: 'Select an option',
  												//allowClear: true,
		    									//width: '50%',
		    									 containerCssClass : 'hide custom_class_add_on' }).
		    									 on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });

		    	

		    	//$('#total_charges').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	//$('#total_payment').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
		    	//$('#total_due').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

		    	////////// Payments Row //////////////

		    	var htmlCode = '".BookingsItems::getPaymentmethodsDropdown($destination_id,$model->id)."';
        		$('#payments-row').append(htmlCode);
        		$('.calendar').datepicker();
        		$('.payment-switch').bootstrapSwitch();
        		//$('.total').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        		$('.payment_method_dropdown').select2({
		    									placeholder: 'Select an Option',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });
				$('.payment_percentage_dropdown').select2({
												minimumResultsForSearch: -1,
		    									placeholder: 'Select an Option',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });
        		$('[data-toggle=\'tooltip\']').tooltip(); 
		    	$('#test_button').on('click',function(){

		    		//////////////////////// charges table /////////////////////////////

		    		var charges_table = $('#charges_table tbody');
		    	    var charges_data =[];
				    charges_table.find('tr').each(function (i) {

			    		var tds = $(this).find('td');
			        	if(tds.hasClass('hasSelect'))
			        	{
			        		alert('has Select');
			        		var object = 
	                        {
	                        	'receipt' : tds.eq(0).children('input').first().is(':checked'),
	                        	'date'	  : tds.eq(1).children('input').first().val(),
	                        	'description' : tds.eq(2).children('select').first().val(),
	                        	'quantity'	  : tds.eq(3).children('input').first().val(),
	                        	'price'		  : tds.eq(4).children('input').first().inputmask('unmaskedvalue'),
	                        	'vat'		  : tds.eq(5).children('input').first().val(),
	                        	'tax'		  : tds.eq(6).children('input').first().inputmask('unmaskedvalue'),
	                        };
			        	}
			        	else
			        	{
			        		alert('No Select');
			        		var object = 
	                        {
	                        	'receipt' : tds.eq(0).children('input').first().is(':checked'),
	                        	'date'	  : tds.eq(1).children('input').first().val(),
	                        	'description' : tds.eq(2).children('input').first().val(),
	                        	'quantity'	  : tds.eq(3).children('input').first().val(),
	                        	'price'		  : tds.eq(4).children('input').first().val(),
	                        	'vat'		  : tds.eq(5).children('input').first().val(),
	                        	'tax'		  : tds.eq(6).children('input').first().val(),
	                        };
			        	}
			            
                        charges_data.push(object);
				        	
				    });
				    console.log(charges_data);

				    ////////////////////////////// payments table //////////////////////////

				    var payments_table = $('#payments_table tbody');
		    	    var payments_data =[];
				    payments_table.find('tr').each(function (i){

			    		var tds = $(this).find('td');
			        	if(tds.hasClass('hasSelect'))
			        	{
			        		alert('has Select');
			        		var object = 
	                        {
	                        	'receipt' : tds.eq(0).children('input').first().is(':checked'),
	                        	'date'	  : tds.eq(1).children('input').first().val(),
	                        	'description' : tds.eq(2).children('select').first().val(),
	                        	'total'	  : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
	                        	// 'price'		  : tds.eq(4).children('input').first().val(),
	                        	// 'vat'		  : tds.eq(5).children('input').first().val(),
	                        	// 'tax'		  : tds.eq(6).children('input').first().val(),
	                        };
			        	}
			        	else
			        	{
			        		alert('No Select');
			        		var object = 
	                        {
	                        	'receipt' : tds.eq(0).children('input').first().is(':checked'),
	                        	'date'	  : tds.eq(1).children('input').first().val(),
	                        	'description' : tds.eq(2).children('input').first().val(),
	                        	'total'	  : tds.eq(7).children('input').first().val(),
	                        	// 'price'		  : tds.eq(4).children('input').first().val(),
	                        	// 'vat'		  : tds.eq(5).children('input').first().val(),
	                        	// 'tax'		  : tds.eq(6).children('input').first().val(),
	                        };
			        	}
			            
                        payments_data.push(object);

				        	
				    });
				    console.log(payments_data);
		    	});
	    	    
		    });

		    //////////////////// Functions to calculate vat amount and actual price /////////////////////

		    $(document).on('change','.qty',function(){
		    	calculatePrice($(this));
		    });

		    $(document).on('change','.price',function(){
		    	calculatePrice($(this));
		    });
		    $(document).on('change','.vat',function(){
		    	calculatePrice($(this));
		    });
		    $(document).on('change','.tax',function(){
		    	calculatePrice($(this));
		    });

		    $(document).on('change','.discount_amount',function(){
		    	calculatePrice($(this));
		    });

		    function calculatePrice(clicked_on,from = null)
		    {
		    	console.log('input '+clicked_on);
		    	var qty;
		    	var check;
		    	var price;
		    	var vat;
		    	var tax;
		    	var discount_amount;
		    	if(from == 1)
		    	{
		    		clicked_on = clicked_on.parent().parent().parent().nextAll().eq(1).children('input').first();
		    	}
		    	console.log('clicked_on '+clicked_on);
		    	var charge_cell_next = clicked_on.parent().nextAll();
		    	var charge_cell_prev = clicked_on.parent().prevAll();

		    	console.log('charge_cell_next : '+charge_cell_next);
		    	console.log('charge_cell_prev : '+charge_cell_prev);

		    	if(clicked_on.hasClass('qty'))
		    	{
		    		var charge_cell_next = clicked_on.parent().nextAll();
		    		qty = clicked_on.val();
		    		price = charge_cell_next.eq(0).children('input').first().val();
		    		vat = charge_cell_next.eq(1).children('input').first().val();
		    		tax = charge_cell_next.eq(2).children('input').first().val();
		    		discount_amount = charge_cell_next.eq(4).children('input').first().val();
		    		check = 0;
		    	}

		    	if(clicked_on.hasClass('price'))
		    	{
		    		var charge_cell_next = clicked_on.parent().nextAll();
		    		qty = charge_cell_prev.eq(0).children('input').first().val();
		    		price = clicked_on.val();
		    		vat = charge_cell_next.eq(0).children('input').first().val();
		    		tax = charge_cell_next.eq(1).children('input').first().val();
		    		discount_amount = charge_cell_next.eq(3).children('input').first().val();
		    		check = 1;
		    	}
		    	if(clicked_on.hasClass('vat'))
		    	{
		    		qty = charge_cell_prev.eq(1).children('input').first().val();
		    		price = charge_cell_prev.eq(0).children('input').first().val();
		    		vat = clicked_on.val();
		    		tax = charge_cell_next.eq(0).children('input').first().val();
		    		discount_amount = charge_cell_next.eq(2).children('input').first().val();
		    		check = 2;
		    	}
		    	if(clicked_on.hasClass('tax'))
		    	{
		    		qty = charge_cell_prev.eq(2).children('input').first().val();
		    		price = charge_cell_prev.eq(1).children('input').first().val();
		    		vat = charge_cell_prev.eq(0).children('input').first().val();
		    		tax = clicked_on.val();
		    		discount_amount = charge_cell_next.eq(1).children('input').first().val();
		    		check = 3;
		    	}
		    	if(clicked_on.hasClass('discount_amount'))
		    	{
		    		qty = charge_cell_prev.eq(4).children('input').first().val();
		    		price = charge_cell_prev.eq(3).children('input').first().val();
		    		vat = charge_cell_prev.eq(2).children('input').first().val();
		    		tax = charge_cell_prev.eq(1).children('input').first().val();
		    		discount_amount = clicked_on.val();
		    		check = 4;
		    	}
		    	console.log('qty :'+qty);
		    	console.log('price :'+price);
		    	console.log('vat :'+vat);
		    	console.log('tax :'+tax);
		    	console.log('discount_amount :'+discount_amount);

		    	$.ajax(
			        {
			            type: 'POST',
			            url: '$get_price_for_debit_row',
			            data: {qty:qty,price:price,vat:vat,tax:tax,discount_amount:discount_amount},
			            success: function(data)
			            {
			                data = jQuery.parseJSON( data );
			                //alert(data.final_total);
			                console.log(data);
			                if(check == 0)
			                {
			                	charge_cell_next.eq(5).children('input').first().val(data.vat_amount);
			                	charge_cell_next.eq(6).children('input').first().val(data.final_total);
			                }
			                if(check == 1)
			                {
			                	charge_cell_next.eq(4).children('input').first().val(data.vat_amount);
			                	charge_cell_next.eq(5).children('input').first().val(data.final_total);
			                }
			                if(check == 2)
			                {
			                	charge_cell_next.eq(3).children('input').first().val(data.vat_amount);
			                	charge_cell_next.eq(4).children('input').first().val(data.final_total);
			                }
			                if(check == 3)
			                {
			                	charge_cell_next.eq(2).children('input').first().val(data.vat_amount);
			                	charge_cell_next.eq(3).children('input').first().val(data.final_total);
			                }
			                if(check == 4)
			                {
			                	charge_cell_next.eq(0).children('input').first().val(data.vat_amount);
			                	charge_cell_next.eq(1).children('input').first().val(data.final_total);
			                }  
			            },
			            error: function()
			            {
			                alert('error');
			            }
			        });
		    }


		    ///////////////////////////////////// On change of Save and Delete of Payment and Charges Rows //////////////////////////////////


		    /////////////////////// Charges / Debits Button //////////////////////////////

		    $(document).on('click','.save-dynamic-row-charges',function(){
		    	
		    	var row = $(this).parent().parent();
		    	var id;
		    	console.log(row);
		    	if(row.attr('id') == undefined)
		    	{
		    		id = 0;
		    	}
		    	else
		    	{
		    		id = row.attr('id');
		    	}
		    	console.log('id: '+id);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	console.log('select :'+tds.eq(2).find('input').first().val());
		    	var object;

		    	if(tds.hasClass('hasSelect'))
                {
                	//alert(tds.eq(2).find('select').find(':selected').attr('data-type'));
                    //alert('has Select');
                    var object = 
                    {
                    	'booking_item_id' : ".$model->id.",
                    	'id'	: id,
                    	'type'	: 1,
                        'item'  : tds.eq(2).find('select').find(':selected').attr('data-type'),
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'date'    : tds.eq(1).children('input').first().val(),
                        'description' : tds.eq(2).find('input').first().val(),
                        'quantity'    : tds.eq(3).children('input').first().val(),
                        'price'       : tds.eq(4).children('input').first().val(),
                        'vat'         : tds.eq(5).children('input').first().val(),
                        'tax'         : tds.eq(6).children('input').first().val(),
                        'discount_amount'         : tds.eq(8).children('input').first().val(),
                        'vat_amount'         : tds.eq(9).children('input').first().val(),
                        'final_total'         : tds.eq(10).children('input').first().val(),
                    };
                }
                else
                {
                   // alert('No Select');
                    var object = 
                    {
                    	'booking_item_id' : ".$model->id.",
                    	'id'	: id,
                    	'type'	: 1,
                        'item'  : 4,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'date'    : tds.eq(1).children('input').first().val(),
                        'description' : tds.eq(2).find('input').first().val(),
                        'quantity'    : tds.eq(3).children('input').first().val(),
                        'price'       : tds.eq(4).children('input').first().val(),
                        'vat'         : tds.eq(5).children('input').first().val(),
                        'tax'         : tds.eq(6).children('input').first().val(),
                        'discount_amount'         : tds.eq(8).children('input').first().val(),
                        'vat_amount'         : tds.eq(9).children('input').first().val(),
                        'final_total'         : tds.eq(10).children('input').first().val(),
                    };
                }
                
                $.ajax(
		        {
		            type: 'POST',
		            data: {
		            	debit : object,
		            },
		            url: '$add_charge_entry',
		            async: true,
		     		beforeSend: function(){
		     			App.blockUI({target:'.table_charges_div', animate: !0});
				    },
		            success: function(data)
		            {
		               data = JSON.parse(data);
		               if(data.id == 0)
		               {
		               		alert('price is 0');
		               }
		               else
		               {
		               		//alert('Record Updated');
		               		row.attr('id',data.id);
		               		$('#total_charges').val(data.total_debit);
		               		$('#total_payment').val(data.total_credit);
		               		$('#total_due').empty().html(data.balance_due);
		               		$('#balance_due').empty().html(data.balance_due);
		               		if(data.balance_due == 0)
		               		{
		               			$('#total_due').css('color','green');
		               			$('#balance_due').css('color','green');
		               		}
		               		else if(data.balance_due < 0)
		               		{
		               			$('#total_due').css('color','orange');
		               			$('#balance_due').css('color','orange');
		               		}
		               		else
		               		{
		               			$('#total_due').css('color','black');
		               			$('#balance_due').css('color','black');
		               		}
		               		global_payment = data.balance_due;
		               		$('#add_add_on_btn').trigger('click');
		               		tds.eq(11).html('');
		               		tds.eq(11).append('<a href=\"javascript:void(0)\" class=\"btn btn-icon-only red delete-dynamic-row-charges\" title=\"Delete\" data-toggle=\"tooltip\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></a>');
		               		var last_row = $('#payments_table').find('tr').last();
		               		var last_td = last_row.find('td');
		               		last_td.eq(7).children('input').first().val(data.balance_due);
				               	
		               }
		            },
		            complete: function()
		            {
		                App.unblockUI('.table_charges_div');
		                // alert('unblocked')

		            },
		            error: function()
		            {
		                alert('error');
		            }
		        });

		    });
		    $(document).on('click','.delete-dynamic-row-charges',function(){

		    	// if (confirm('Are you sure, You want to remove')) {
			    	
			    	var row = $(this).parent().parent();
			    	var id;
			    	console.log(row);
			    	if(row.attr('id') == undefined)
			    	{
			    		id = 0;
			    		row.remove();
			    	}
			    	else
			    	{
			    		id = row.attr('id');
			    		$.ajax(
				        {
				            type: 'POST',
				            data: {
				            	id : id,
				            	booking_item_id : ".$model->id.",
				            },
				            url: '$delete_charge_entry',
				            async: true,
				            beforeSend: function(){
				     			App.blockUI({target:'.table_charges_div', animate: !0});
						    },
				            success: function(data)
				            {
				            	data = JSON.parse(data);
				               	if(data.id == 0)
				               	{
				               		alert('Error');
				               	}
				               	else
				               	{
				               		row.remove();
				               		//alert('Record Updated');
				               		$('#total_charges').val(data.total_debit);
				               		$('#total_payment').val(data.total_credit);
				               		$('#total_due').empty().html(data.balance_due);
				               		$('#balance_due').empty().html(data.balance_due);
				               		if(data.balance_due == 0)
				               		{
				               			$('#total_due').css('color','green');
				               			$('#balance_due').css('color','green');
				               		}
				               		else if(data.balance_due < 0)
				               		{
				               			$('#total_due').css('color','orange');
				               			$('#balance_due').css('color','orange');
				               		}
				               		else
				               		{
				               			$('#total_due').css('color','black');
				               			$('#balance_due').css('color','black');
				               		}
				               		global_payment = data.balance_due;
				               		var last_row = $('#payments_table').find('tr').last();
				               		var last_td = last_row.find('td');
				               		last_td.eq(7).children('input').first().val(data.balance_due);
				               		
				               	}
				            	
				            },
				            complete: function()
				            {
				                App.unblockUI('.table_charges_div');
				                // alert('unblocked')
				            },
				            error: function()
				            {
				                alert('error');
				            }
				        });
			    	}
		    		
				// } 
		    });

		    /////////////////////// Payments / Credits Button //////////////////////////////

		    $(document).on('click','.save-dynamic-row-payments',function(){
		    	var row = $(this).parent().parent();
		    	var id;
		    	console.log(row);
		    	if(row.attr('id') == undefined)
		    	{
		    		id = 0;
		    	}
		    	else
		    	{
		    		id = row.attr('id');
		    	}
		    	console.log('id: '+id);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
                if(tds.hasClass('hasSelect'))
                {
                   // alert('has Select');
                    var object = 
                    {
                    	'booking_item_id' : ".$model->id.",
                    	'id'	: id,
                    	'type'	: 2,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'date'    : tds.eq(1).children('input').first().val(),
                        'description' : tds.eq(2).find('input').first().val(),
                        'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                        'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                        'deposit_percentage' : tds.eq(4).find('input').val(),
                        // 'price'        : tds.eq(4).children('input').first().val(),
                        // 'vat'          : tds.eq(5).children('input').first().val(),
                        // 'tax'          : tds.eq(6).children('input').first().val(),
                    };
                }
                else
                {
                   // alert('No Select');
                    var object = 
                    {
                    	'booking_item_id' : ".$model->id.",
                    	'id'	: id,
                    	'type'	: 2,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'date'    : tds.eq(1).children('input').first().val(),
                        'description' : tds.eq(2).find('input').first().val(),
                        'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                        'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                        'deposit_percentage' : tds.eq(4).find('input').val(),
                        // 'price'        : tds.eq(4).children('input').first().val(),
                        // 'vat'          : tds.eq(5).children('input').first().val(),
                        // 'tax'          : tds.eq(6).children('input').first().val(),
                    };
                }
                

                $.ajax(
		        {
		            type: 'POST',
		            data: {
		            	credit : object,
		            },
		            url: '$add_payment_entry',
		            async: true,
		            beforeSend: function(){
		     			App.blockUI({target:'.table_payments_div', animate: !0});
				    },
		            success: function(data)
		            {
		               data = JSON.parse(data);
		               if(data.id == 0)
		               {
		               		alert('price is 0');
		               }
		               else
		               {
		               		//alert('Record Updated');
		               		row.attr('id',data.id);
		               		$('#total_charges').val(data.total_debit);
		               		$('#total_payment').val(data.total_credit);
		               		$('#total_due').empty().html(data.balance_due);
		               		$('#balance_due').empty().html(data.balance_due);
		               		if(data.balance_due == 0)
		               		{
		               			$('#total_due').css('color','green');
		               			$('#balance_due').css('color','green');
		               		}
		               		else if(data.balance_due < 0)
		               		{
		               			$('#total_due').css('color','orange');
		               			$('#balance_due').css('color','orange');
		               		}
		               		else
		               		{
		               			$('#total_due').css('color','black');
		               			$('#balance_due').css('color','black');
		               		}
		               		global_payment = data.balance_due;
		               		$('#add_credit_btn').trigger('click');
		               		tds.eq(8).html('');
		               		tds.eq(8).append('<a href=\"javascript:void(0)\" class=\"btn btn-icon-only red delete-dynamic-row-payments\" title=\"Delete\" data-toggle=\"tooltip\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></a>');
		               		var last_row = $('#payments_table').find('tr').last();
		               		var last_td = last_row.find('td');
		               		last_td.eq(7).children('input').first().val(data.balance_due);

		               		if(tds.eq(3).find('input').bootstrapSwitch('state'))
		               		{
		               			tds.eq(3).empty().html('<span class=\"label label-info\">Deposit</span>');
		               			var p_age = tds.eq(4).find('input').find(':selected').val();
		               			tds.eq(4).empty().append(data.percentage);
		               		}
		               		else
		               		{
		               			tds.eq(3).empty().html('<span class=\"label label-default\">Normal</span>');
		               		}
		               		row.removeClass('has-switch');
		               }
		            },
		            complete: function()
		            {
		                App.unblockUI('.table_payments_div');
		                // alert('unblocked')
		            },
		            error: function()
		            {
		                alert('error');
		            }
		        });

		    });


		    $(document).on('click','.delete-dynamic-row-payments',function(){
		    	// if (confirm('Are you sure, You want to remove')) {
			    var row = $(this).parent().parent();
			    	var id;
			    	console.log(row);
			    	if(row.attr('id') == undefined)
			    	{
			    		id = 0;
			    		row.remove();
			    	}
			    	else
			    	{
			    		id = row.attr('id');
			    		$.ajax(
				        {
				            type: 'POST',
				            data: {
				            	id : id,
				            	booking_item_id : ".$model->id.",
				            },
				            url: '$delete_payment_entry',
				            async: true,
				            beforeSend: function(){
				     			App.blockUI({target:'.table_payments_div', animate: !0});
						    },
				            success: function(data)
				            {
				            	data = JSON.parse(data);
				               	if(data.id == 0)
				               	{
				               		alert('Error');
				               	}
				               	else
				               	{
				               		row.remove();
				               		//alert('Record Updated');
				               		$('#total_charges').val(data.total_debit);
				               		$('#total_payment').val(data.total_credit);
				               		$('#total_due').empty().html(data.balance_due);
				               		$('#balance_due').empty().html(data.balance_due);
				               		if(data.balance_due == 0)
				               		{
				               			$('#total_due').css('color','green');
				               			$('#balance_due').css('color','green');
				               		}
				               		else if(data.balance_due < 0)
				               		{
				               			$('#total_due').css('color','orange');
				               			$('#balance_due').css('color','orange');
				               		}
				               		else
				               		{
				               			$('#total_due').css('color','black');
				               			$('#balance_due').css('color','black');
				               		}
				               		global_payment = data.balance_due;
				               		var last_row = $('#payments_table').find('tr').last();
				               		var last_td = last_row.find('td');
				               		last_td.eq(7).children('input').first().val(data.balance_due);
				               		if(last_td.eq(3).find('input').bootstrapSwitch('state'))
				               		{
				               			updatePercentage(last_row);
				               		}
				               		
				               	}
				            	
				               	
				            },
				            complete: function()
				            {
				                App.unblockUI('.table_payments_div');
				                // alert('unblocked')
				            },
				            error: function()
				            {
				                alert('error');
				            }
				        });
			    	}
		    		$(this).parent().parent().remove();
				// } 
		    });


		    //////////////////////////////////////// NEW JS TO Handel dropdown and input box ///////////////////////////////////
		    $(document).on('click','.show_dropdown',function(){
		    	//alert('clicked_on');
		    	var elements = $(this).parent().nextAll();
		    	console.log($(this).parent().nextAll());
		    	elements.eq(0).addClass('hide');
		    	console.log(elements.eq(1).data('select2'));
		    	//$(elements.eq(1).data('select2').options.options.containerCssClass).removeClass('hide');
		    	//$(elements.eq(1).select2('container')).removeClass('hide');
		    	elements.eq(1).select2().data('select2').\$dropdown.removeClass('hide');
		    	//elements.eq(1).select2().data('select2').\$container.addClass('custom_class_add_on');
		    	elements.eq(1).select2().select2('open');

		    	// elements.eq(1).select2().select2('open');
		    	// elements.eq(1).on('select2:close', function (e) {
		    	// 		// log('select2:close', e);
		    	// 		console.log('event triggered');
		    	// 		elements.eq(1).select2().data('select2').\$container.addClass('hide');
		    	// 		elements.eq(0).removeClass('hide');
		    	//  });
		    	console.log(elements.eq(1).data('select2'));
		    });

		    $(document).on('click','.show_dropdown2',function(){
		    	//alert('clicked_on');
		    	var elements = $(this).parent().nextAll();
		    	console.log($(this).parent().nextAll());
		    	elements.eq(0).addClass('hide');
		    	console.log(elements.eq(1).data('select2'));
		    	//$(elements.eq(1).data('select2').options.options.containerCssClass).removeClass('hide');
		    	//$(elements.eq(1).select2('container')).removeClass('hide');
		    	elements.eq(1).select2().data('select2').\$dropdown.removeClass('hide');
		    	//elements.eq(1).select2().data('select2').\$container.addClass('custom_class_add_on');
		    	elements.eq(1).select2({minimumResultsForSearch: -1,}).val('').select2('open');

		    	// elements.eq(1).select2().select2('open');
		    	// elements.eq(1).on('select2:close', function (e) {
		    	// 		// log('select2:close', e);
		    	// 		console.log('event triggered');
		    	// 		elements.eq(1).select2().data('select2').\$container.addClass('hide');
		    	// 		elements.eq(0).removeClass('hide');
		    	//  });
		    	console.log(elements.eq(1).data('select2'));
		    });
		    $(document).on('change','.payment_description',function(){
		    	//alert('changed');
		    	console.log('payment_description change');
		    	var row = $(this).parent().parent().parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
		    	var total = tds.eq(7).children('input').first().inputmask('unmaskedvalue');
		    	console.log(tds.eq(8).children('button'));

		    	if(total != '' && total != 0 && $(this).val() != '' )
		    	{
		    		console.log('payment if');
		    		tds.eq(8).children('button').removeAttr('disabled');
		    	}
		    	else
		    	{
		    		console.log('payment else');
		    		tds.eq(8).children('button').attr('disabled',true);
		    	}
		    });

		    $(document).on('change','.total',function(){
		    	console.log('total change');

		    	var row = $(this).parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
		    	var total = $(this).val();
		    	console.log(tds.eq(2).find('input').first().val());
		    	
		    	if(total != '' && total != 0 && tds.eq(2).find('input').first().val() != '' )
		    	{
		    		console.log('total if');
		    		tds.eq(8).children('button').removeAttr('disabled');
		    	}
		    	else
		    	{
		    		console.log('total else');
		    		tds.eq(8).children('button').attr('disabled',true);
		    	}

		    	console.log('switch :' +tds.eq(3).find('input').bootstrapSwitch('state'));
		    	if(tds.eq(3).find('input').bootstrapSwitch('state'))
		    	{
		    		updatePercentage(row);
		    	}
		    	

		    });

		    $(document).on('change','.charge_description',function(){
		    	//alert('changed');
		    	console.log('charge_description change');
		    	var row = $(this).parent().parent().parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
		    	var total = tds.eq(4).children('input').first().inputmask('unmaskedvalue');
		    	console.log(tds.eq(11).children('button'));

		    	if(total != '' && total != 0 && $(this).val() != '' )
		    	{
		    		console.log('payment if');
		    		tds.eq(11).children('button').removeAttr('disabled');
		    	}
		    	else
		    	{
		    		console.log('payment else');
		    		tds.eq(11).children('button').attr('disabled',true);
		    	}
		    });

		    $(document).on('change','.price',function(){
		    	console.log('price change');
		    	var row = $(this).parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
		    	var total = $(this).val();
		    	console.log(tds.eq(2).find('input').first().val());
		    	if(total != '' && total != 0 && tds.eq(2).find('input').first().val() != '' )
		    	{
		    		console.log('total if');
		    		tds.eq(11).children('button').removeAttr('disabled');
		    	}
		    	else
		    	{
		    		console.log('total else');
		    		tds.eq(11).children('button').attr('disabled',true);
		    	}
		    	

		    });

		    $(document).on('switchChange.bootstrapSwitch', '.payment-switch', function (event, state) {
				console.log('total change');
		    	var row = $(this).parent().parent().parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
			    if ($(this).bootstrapSwitch('state')) {     
			        console.log($(this).data('on-text'));
			        var percentage =  tds.eq(4).find('input').val();
			        var amount =  tds.eq(7).find('input').val();
					console.log(percentage);
						
					// if(amount != '' && amount > 0)
					if(amount != '')
					{
						updatePercentage(row);
					}
					else
					{
						if(percentage != '' && percentage > 0)
						{
							updateAmount(row);
						}
					}
			       // updateAmount(row);
			        tds.eq(4).children('.deposit_percentage').removeClass('hide');
			    }else {
			        console.log($(this).data('off-text'));
			        tds.eq(4).children('.deposit_percentage').addClass('hide');
			        tds.eq(7).children('input').first().val(global_payment);
			    }
			});

			$(document).on('change','.deposit_amount',function(){
				
				var row = $(this).parent().parent().parent().parent();
				console.log(row.attr('class'));
				console.log('row :'+row);
				updateAmount(row);
			})

			function updateAmount(row)
			{	
				var tds = row.find('td');
				console.log('tds :'+tds);
				var percentage =  tds.eq(4).find('input').val();
				console.log(percentage);
				if(percentage != '' && percentage > 0)
				{
					$.ajax(
			        {
			            type: 'POST',
			            data: {
			            	percentage : percentage,
			            	booking_item_id : ".$model->id."
			            },
			            url: '$calculate_deposit',
			            async: true,
			            beforeSend: function(){
			     			App.blockUI({target:'.table_payments_div', animate: !0});
					    },
			            success: function(data)
			            {
			               data = JSON.parse(data);
			              
			               tds.eq(7).children('input').first().val(data.deposit_amount);			               		
			            },
			            complete: function()
			            {
			                App.unblockUI('.table_payments_div');
			                // alert('unblocked')
			            },
			            error: function()
			            {
			                alert('error');
			            }
			        });	
				}
					

			}

			function updatePercentage(row)
			{	
				var tds = row.find('td');
				console.log('tds :'+tds);
				var amount =  tds.eq(7).find('input').val();
				console.log(amount);
				if(amount != '' && amount > -1)
				{
					$.ajax(
			        {
			            type: 'POST',
			            data: {
			            	amount : amount,
			            	booking_item_id : ".$model->id."
			            },
			            url: '$calculate_percentage',
			            async: true,
			            beforeSend: function(){
			     			// App.blockUI({target:'.table_payments_div', animate: !0});
					    },
			            success: function(data)
			            {
			               data = JSON.parse(data);
			              
			               tds.eq(4).find('input').first().val(data.deposit_percentage);			               		
			            },
			            complete: function()
			            {
			                //App.unblockUI('.table_payments_div');
			                // alert('unblocked')
			            },
			            error: function()
			            {
			                alert('error');
			            }
			        });	
				}
					

			}
	");
		
	$this->registerCss("
		// .input-small {
		//     width: 100px!important;
		// }
		.custom_class_add_on{
			float: right !important;
			width: 207px !important; 
			
		}
		.custom_class_payment{
			float: right !important;
			width: 600px !important; 
    		//float: right;
		}
		.select2-container{
			float: right !important;
		}
		"
	);
	if($balance_due == 0)
	{
		$this->registerJs("
			$('#total_due').css('color','green');
		");
	}
	if($balance_due < 0)
	{
		$this->registerJs("
			$('#total_due').css('color','orange');
		");
	}
?>
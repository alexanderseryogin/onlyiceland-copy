<?php
    use common\models\Amenities;
use kartik\sortable\Sortable;
use yii\web\View;

    $Checkbox = [
        0 => '',
        1 => 'checked'
    ];
    $isChecked = 0;
?>
<style type="text/css">
    table tr th {
        text-align: center;
    }
    .free{background: #DFF0D8;}
    .extra{background: #FCF8E3}
    .not_available{background: #FFEB89}
    .not_display{background: #F2DEDE}
    .table-scrollable {border: none !important; overflow-x: hidden !important;}
    .dataTables_scrollBody {overflow-x: hidden !important;}
    table.dataTable thead .sorting_desc {
        background-image: none;
    }
    table.dataTable thead .sorting_asc {
        background-image: none;
    }
</style>

<!-- BEGIN ACCORDION PORTLET-->

<div class="tab-pane" id="amenities">
<div class="panel-group accordion" id="accordion_destination_amenities">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_destination_amenities" href="#collapse_amenities" aria-expanded="true">Destination Amenities</a>
            </h4>
        </div>
        <div id="collapse_amenities" class="panel-collapse collapse in"> 
            <div class="panel-body" style="overflow-y:auto;">
                 <div style="height: 500px">
                        <div class="row drop_header text-center">
                            <div class="col-md-2"><strong>Amenities</strong></div>
                            <div class="col-md-2"><strong>Free</strong></div>
                            <div class="col-md-2"><strong>Available at extra cost</strong></div>
                            <div class="col-md-2"><strong>Not available</strong></div>
                            <div class="col-md-2"><strong>Do not display</strong></div>
                            <div class="col-md-2"><strong>Can be banner</strong></div>
                        </div>
                        <?php
                            $i=0;
                            $drop_items = array();
                            foreach ($amenities as $key => $value) {
                                $radioChecked = [
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                ];

                                if (!empty($provider_model->amenities) && isset($provider_model->amenities[$value->id])) {
                                    $arr = explode(',', $provider_model->amenities[$value->id]);
                                    if ($arr[0] == $value->id) {
                                        switch ($arr[1]) {
                                            case 0:
                                                $radioChecked[0] = 'checked';
                                                break;

                                            case 1:
                                                $radioChecked[1] = 'checked';
                                                break;

                                            case 2:
                                                $radioChecked[2] = 'checked';
                                                break;

                                            case 3:
                                                $radioChecked[3] = 'checked';
                                                break;
                                        }
                                    }
                                }
                                if (!empty($provider_model->amenities_banners)) {
                                    if (in_array($value->id, $provider_model->amenities_banners))
                                        $isChecked = 1;
                                    else
                                        $isChecked = 0;
                                }

                                $drop_items[] = ['content' => (string)$this->render('_dropitem', compact('value', 'radioChecked', 'Checkbox', 'isChecked'))];
                            }

                            echo Sortable::widget([
                                'itemOptions'=>['class'=>'drop_line'],
                                'items' => $drop_items
                            ]);
                        ?>
                </div>
            </div>
        </div>
    </div>
    <br>

    <?php 
        if(empty($provider_model->background_color))
        {
            $provider_model->background_color='#ff0000';
        }
        if(empty($provider_model->text_color))
        {
            $provider_model->text_color='#000000';
        }

        $this->registerJs("
                $('#preview_box')[0].style.backgroundColor = '$provider_model->background_color';
                $('#preview_box')[0].style.color = '$provider_model->text_color';
                if('$provider_model->transparent_background' == 1)
                {
                    $('.background-color').hide();
                }
        ");
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion_destination_amenities" href="#collapse_banner">Banner Style</a>
            </h4>
        </div>
        <div id="collapse_banner" class="panel-collapse collapse"> 
            <div class="panel-body" style="overflow-y:auto;">
                <div class="row">
                    <div class="col-xs-3">
                        <div id="preview_box" class="preview_box"><strong>Banner</strong></div>
                    </div>
                    <div class="col-xs-3">
                        <label class=" control-label">Text Color</label>
                        <div class="input-group colorpicker-component">
                            <input type="text" id="text_color" name="Destinations[text_color]" class="form-control"  value=<?=$provider_model->text_color?>> <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                    <div class="col-xs-3 background-color">
                        <label class="control-label">Background Color</label>
                         <div class="input-group colorpicker-component">
                            <input type="text" id="background_color" name="Destinations[background_color]" class="form-control"  value=<?=$provider_model->background_color?>> <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                    <div class="col-xs-3" style="margin-top: 3%;">
                        <?= $form->field($provider_model, 'transparent_background')->checkbox() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php
$this->registerJs("

function initDataTable(tableSelector)
{
    return $(tableSelector).DataTable({
            language: {
                aria: {
                    sortAscending: \": activate to sort column ascending\",
                    sortDescending: \": activate to sort column descending\"
                },
                emptyTable: \"No data available in table\",
                info: \"Showing _TOTAL_ amenities\",
                infoEmpty: \"No amenities found\",
                infoFiltered: \"(filtered 1 from _MAX_ total amenities)\",
                lengthMenu: \"_MENU_ amenities\",
                search: \"Search:\",
                zeroRecords: \"No matching records found\"
            },
            scrollY: 300,
            scrollCollapse: true,
            //scroller: !0,
            statesave : false,
            columns: [
                { 'width': '30%' },
                { 'width': '15%' },
                { 'width': '15%' },
                { 'width': '15%' },
                { 'width': '15%' },
            ],
            order: [
                [0, \"asc\"]
            ],
            \"columnDefs\": [
                { \"orderable\": false, \"targets\": [1,2,3,4,5] }
            ],
            lengthMenu: [
                [10, 15, 20, -1],
                [10, 15, 20, \"All\"]
            ],
            paging: false,
            retrieve: true
        }); 
}

function redrawDataTable(tableSelector)
{
    var table = initDataTable(tableSelector);
    table.draw();
}

function redrawAmenitiesDataTable()
{
    redrawDataTable('#destination_amenities');
}

jQuery(document).ready(function() 
{
    initDataTable('#destination_amenities');
    setTimeout(function()
    { 
        redrawAmenitiesDataTable(); 
    }, 500);
    
});",View::POS_END);
?>
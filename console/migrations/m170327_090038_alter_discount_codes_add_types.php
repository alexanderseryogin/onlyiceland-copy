<?php

use yii\db\Migration;

class m170327_090038_alter_discount_codes_add_types extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `discount_code` DROP COLUMN `discount_type_id`, ADD COLUMN `pricing_type` TINYINT(1) NULL AFTER `linked_to_quantity`, ADD COLUMN `applies_to` TINYINT(1) NULL AFTER `pricing_type`, ADD COLUMN `quantity_type` TINYINT(1) NULL AFTER `applies_to`, DROP INDEX `FK_discount_code_type_id`, DROP FOREIGN KEY `FK_discount_code_type_id`; 
        ");
    }

    public function down()
    {
        echo "m170327_090038_alter_discount_codes_add_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

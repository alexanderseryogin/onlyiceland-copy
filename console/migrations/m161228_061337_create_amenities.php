<?php

use yii\db\Migration;

class m161228_061337_create_amenities extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `amenities`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `icon` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; ");
    }

    public function down()
    {
        echo "m161228_061337_create_amenities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

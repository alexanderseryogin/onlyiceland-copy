<?php

namespace backend\controllers;

use Yii;
use common\models\BookableItems;
use common\models\BookableItemsSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;
use common\models\UploadFile;
use yii\imagine\Image;
use yii\helpers\ArrayHelper;

use common\models\BookableItemsImages;
use common\models\BookableAmenities;
use common\models\DestinationsAmenities;
use common\models\Amenities;
use common\models\Destinations;
use common\models\BookingRules;
use common\models\BedTypes;
use common\models\BookableBedTypes;
use yii\filters\AccessControl;
use common\models\BookingPolicies;
use common\models\BookableBedsCombinations;
use common\models\BookableItemsNames;
use common\models\BookableItemsHousekeepingStatus;
use common\models\DestinationsOpenHoursExceptions;
use common\models\UnitAvailability;
use common\models\Rates;
/**
 * BookableItemsController implements the CRUD actions for BookableItems model.
 */
class BookableItemsController extends BaseController
{
    public $counter;
    public $images_arr=[];
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [ 
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'upload-images' => ['POST'],
                ],
            ],
        ];
    }

    /*public function beforeAction($action)
    {
        if ($action->id == '' || $action->id == '') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }*/

    public function beforeAction($action)
    {
        if ($action->id == 'update-image' || $action->id == 'get-bed-types-html' || $action->id == 'arrange-data' || $action->id == 'get-description' || $action->id == 'set-description' || $action->id == 'upload-images' || $action->id == 'delete-image' || $action->id == 'delete-bed-type') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionUpdateImage()
    {
        $srcPath = Yii::getAlias('@app'). '/../uploads/bookable-items/temp/images/';
        if(is_dir($srcPath))
        {
            $this->removeDirectory($srcPath);
        }   

        $uploadFile = new UploadFile();
        $session = Yii::$app->session;
        $_SESSION['image'] = '';
        $error='';

        if(isset($_FILES['file'])) 
        {
            $uploadFile->FileToUpload = $_FILES;

            if($uploadFile->uploadImage('bookable-items','temp','images'))
            {   
                $_SESSION['image']= $uploadFile->FileName;
            }
            else
            {
                $error = 'Invalid Image';
            }
        }

        echo json_encode(array(
            'name'  => $_SESSION['image'],
            'error' => $error,
        ));
    }

    public function actionArrangeData()
    {
        if($_POST['action']=='up')
        {
            $model = BookableItemsImages::findOne(['id'=>$_POST['row_id']]);
            $previous_model = BookableItemsImages::find()
                                                    ->where(['bookable_items_id' => $model->bookable_items_id])
                                                    ->andWhere(['<','display_order',$model->display_order])
                                                    ->orderBy('display_order DESC')
                                                    ->one();

            $temp = $model->display_order;
            $model->display_order = $previous_model->display_order;
            $previous_model->display_order = $temp;

            $model->save();
            $previous_model->save();
        }
        else if($_POST['action']=='down')
        {
            $model = BookableItemsImages::findOne(['id'=>$_POST['row_id']]);
            $next_model = BookableItemsImages::find()
                                                    ->where(['bookable_items_id' => $model->bookable_items_id])
                                                    ->andWhere(['>','display_order',$model->display_order])
                                                    ->orderBy('display_order ASC')
                                                    ->one();

            $temp = $model->display_order;
            $model->display_order = $next_model->display_order;
            $next_model->display_order = $temp;

            $model->save();
            $next_model->save();
        }
    }

    public function actionGetDescription()
    {
        $model = BookableItemsImages::findOne(['id'=>$_POST['image_id']]);
        return $model->description;
    }

    public function actionSetDescription()
    {
        $model = new BookableItemsImages();
        $model = BookableItemsImages::findOne(['id'=>$_POST['image_id']]);
        $model->description = $_POST['description'];

        $session = Yii::$app->session;
        $srcPath = Yii::getAlias('@app'). '/../uploads/bookable-items/temp/images/';
        $destPath = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->bookable_items_id.'/images/';

        if(isset($_SESSION['image']) && !empty($_SESSION['image']))
        {
            if(file_exists($srcPath.$_SESSION['image']))
            {
                if(copy($srcPath.$_SESSION['image'], $destPath.$_SESSION['image']))
                {
                    if(!empty($model->image))
                    {
                        $Path = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->bookable_items_id.'/images/'.$model->image;
                        if(file_exists($Path))
                            unlink($Path);

                        $Path = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->bookable_items_id.'/images/thumb_'.$model->image;
                        if(file_exists($Path))
                            unlink($Path);
                    }
                    $model->image = $_SESSION['image'];

                    // create thumbnail of image

                    Image::thumbnail($destPath.$_SESSION['image'], 90,90)
                    ->save($destPath.'thumb_'.$_SESSION['image'], ['quality' => 80]);

                    $this->removeDirectory($srcPath);
                    unset($_SESSION['image']);
                }
            }       
        }

        $model->save();
        return ;
    }
    /**
     * Lists all BookableItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookableItemsSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['BookableItemsSearch']) && !$this->isArrayEmpty($params['BookableItemsSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BookableItems model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUploadImages()
    {
        $uploadFile = new UploadFile();

        $session = Yii::$app->session;

        $uploadFile->FileToUpload = $_FILES;

        if($uploadFile->uploadImage('bookable-items','temp','images'))
        {
            if(!empty($_SESSION['images']))
            {
                $count = count($_SESSION['images']);
            }
            else
            {
                $count=0;
            }
            
            $_SESSION['images'][$count] = $uploadFile->FileName;
        }
    }

    /**
     * Creates a new BookableItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tab_href = '';
        $model = new BookableItems();
        $model->setDefaultValues();

        $bed_type = [
            0 => '',
        ];
        $quantity = [
            0 => 1
        ];

        $allIds = Amenities::find()->select('id')->column();
        $ids = BookableAmenities::find()->select('amenities_id')->where(['item_id' => $model->id])->column();
        $ids = array_merge($ids, array_diff($allIds, $ids));
        $amenities = Amenities::find()
            ->orderBy(new \yii\db\Expression('FIELD (id, '.implode(',', $ids).')'))
            ->all();

        if($model->load(Yii::$app->request->post())) 
        {
            // echo '<pre>';
            // print_r($_POST);
            // exit;

            $tab_href = $_POST['BookableItems']['tab_href'];
            $model->item_names = $_POST['BookableItems']['item_names'];
            
            // ************* Check Selected Destination has Type Accommodatino ************* //

            if($model->destination->provider_type_id == $model->getAccomodationId())
            {
                $model->isAccommodation = 1;
            }
            else
            {
                $model->isAccommodation = 0;
            }

            if(!empty($model->age_participation))
            {
                $age_arr = explode(';', $model->age_participation);
                $model->min_age_participation = $age_arr[0];
                $model->max_age_participation = $age_arr[1];
            }
            else
            {
                $model->min_age_participation = 0;
                $model->max_age_participation = 100;
            }

            $model->amenities = isset($_POST['BookableItems']['amenities_radio']) && !empty($_POST['BookableItems']['amenities_radio'])? $_POST['BookableItems']['amenities_radio']: NULL;

            $model->amenities_banners = isset($_POST['BookableItems']['amenities_checkbox']) && !empty($_POST['BookableItems']['amenities_checkbox'])? $_POST['BookableItems']['amenities_checkbox']: NULL;

            $bed_type = isset($_POST['BookableItems']['bed_type']) ? $_POST['BookableItems']['bed_type']: NULL;
            $quantity = isset($_POST['BookableItems']['quantity']) ? $_POST['BookableItems']['quantity']: NULL;

            $model->beds_combinations = isset($_POST['BookableItems']['beds_combinations']) ? $_POST['BookableItems']['beds_combinations']: NULL;

            if(!empty($model->bookable_periods))
            {
                $periods_arr = explode(';', $model->bookable_periods);
                $model->shortest_period = $periods_arr[0];
                $model->longest_period = $periods_arr[1];
            }
            else
            {
                $model->shortest_period = 1;
                $model->longest_period = 180;
            }

            if($model->transparent_background)
                $model->background_color = 'transparent';

            /*$model->min_price = ltrim($model->server_min_price,'0');
            $model->max_price = ltrim($model->server_max_price,'0');

            if($model->min_price>$model->max_price)
            {
                Yii::$app->session->setFlash('error', 'Min Price should less than Max Price');

                return $this->render('create', [
                'model' => $model,
                'tab_href' => $tab_href,
                'bed_type' => $bed_type,
                'quantity' => $quantity,
                ]);
            }*/

            if($model->save())
            {
                // Saving Default Bed Combination Id //
                if(isset($_POST['BookableItems']['default_beds_combinations_id']) && !empty($_POST['BookableItems']['default_beds_combinations_id']))
                {
                    // echo "here";
                    // exit();
                    $model->default_bed_combinations_id = $_POST['BookableItems']['default_beds_combinations_id'];
                    if(!$model->save())
                    {
                        echo "<pre>";
                        print_r($model->errors);
                        exit();
                    }
                }
                else
                {
                    $model->default_bed_combinations_id = NULL;
                    if(!$model->save())
                    {
                        echo "<pre>";
                        print_r($model->errors);
                        exit();
                    }
                }

                // *************** Update Record depend on destination type ***************//

                if(isset($_POST['BookableItems']['bookable_item_housekeeping']) && !empty($_POST['BookableItems']['bookable_item_housekeeping']))
                {

                    foreach ($_POST['BookableItems']['bookable_item_housekeeping'] as $status) 
                    {
                       $hk_status = new BookableItemsHousekeepingStatus();
                       $hk_status->bookable_item_id = $model->id;
                       $hk_status->housekeeping_status_id = $status;

                       if(!$hk_status->save())
                       {
                            echo "<pre>";
                            print_r($hk_status->errors);
                            exit();
                       }
                    }
                }

                if($model->isAccommodation)
                {
                    // $model->max_guests = NULL;
                    // $model->max_adults = NULL;
                    // $model->max_children = NULL;
                    // $model->update();

                    // *************** Save Bed Types ************* //

                    if(!empty($bed_type[0]))
                    {
                        foreach ($bed_type as $key => $value) 
                        {
                            if(!empty($value))
                            {
                                $obj = new BookableBedTypes();
                                $obj->bookable_id = $model->id;
                                $obj->bed_type_id = $value;
                                $obj->quantity = $quantity[$key];
                                $obj->save();
                            }
                        }

                        // *************** Save Beds Combination ************* //

                        if(!empty($model->beds_combinations))
                        {
                            foreach ($model->beds_combinations as $key => $value) 
                            {
                                $obj = new BookableBedsCombinations();
                                $obj->item_id = $model->id;
                                $obj->beds_combinations_id = $value;
                                $obj->save();
                            }
                        } 
                    } 
                }
                else
                {
                    $model->sleeping_arrangement_id = NULL;
                    $model->update();
                }              

                // *************** Save Amenities ************* //

                if(is_null($model->amenities_banners))
                        $model->amenities_banners = [];

                if(!empty($model->amenities))
                {
                    foreach ($model->amenities as $key => $value) 
                    {
                        $arr = explode(',', $value);

                        $obj = new BookableAmenities();
                        $obj->item_id = $model->id;
                        $obj->amenities_id = $arr[0];
                        $obj->type = $arr[1];
                        $obj->can_be_banner = in_array($arr[0], $model->amenities_banners)?1:0;
                        $obj->save(); 
                    }
                }

                // ************** Save Item Names ************** //

                for ($i=1; $i <=$model->item_quantity; $i++) 
                {
                    $obj = new BookableItemsNames();
                    $obj->bookable_item_id = $model->id;
                    $obj->item_number = $model->item_names[$i-1]['item_number'];
                    $obj->item_name = $model->item_names[$i-1]['item_name'];
                    $obj->item_order = $model->item_names[$i-1]['item_order'];
                    $obj->save();
                }

                //////////////// Saving House Keeping Status //////////////////
                // echo "here";
                // print_r($_POST['BookableItems']['bookable_item_housekeeping']);
                // exit();
                
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'image')) 
                {
                    if($uploadFile->uploadFile('bookable-items',$model->id,'images'))
                    {
                        if(!empty($model->featured_image))
                        {
                            $prev_image = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images/'.$model->featured_image;
                            unlink($prev_image);
                        }

                        $model->featured_image = $uploadFile->FileName;
                        $model->update();
                    }
                }

                if(!file_exists(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id))
                {
                    mkdir(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id, 0777, true);
                }

                if(!file_exists(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images'))
                {
                     mkdir(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images', 0777, true);
                }

                $srcPath = Yii::getAlias('@app'). '/../uploads/bookable-items/temp/images/';
                $destPath = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images/';

                if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                {
                    foreach ($_SESSION['images'] as $key => $value) 
                    {
                        if(copy($srcPath.$value, $destPath.$value))
                        {
                            $obj = new BookableItemsImages();
                            $obj->bookable_items_id = $model->id;
                            $obj->image = $value;

                            $last_model = BookableItemsImages::find()
                                                        ->where(['bookable_items_id' => $model->id])
                                                        ->orderBy('display_order DESC')
                                                        ->one();
                            if(!empty($last_model))
                            {
                                $obj->display_order = $last_model->display_order+1;
                            }
                            else
                            {
                                $obj->display_order = 1;
                            }

                            $obj->save();

                            unlink($srcPath.$value);
                        }   
                    }

                    $this->removeDirectory($srcPath);
                }
            }
            else
            {
                echo "<pre>";
                print_r($model->errors);
                exit();
            }
            $errors['BookableItems'] = $model->getErrors();
            /*print_r($errors);
            exit;*/

            return $this->redirect(['index']);
        } 
        else 
        {
            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                unset($_SESSION['images']);

            return $this->render('create', [
                'model' => $model,
                'tab_href' => $tab_href,
                'bed_type' => $bed_type,
                'quantity' => $quantity,
                'amenities' => $amenities,
            ]);
        }
    }

    public function removeDirectory($path) 
    {
        $files = glob($path . '/*');
        foreach ($files as $file) 
        {
          is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }

    /**
     * Updates an existing BookableItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$tab_href='#items')
    {
        /*echo '<pre>';
        print_r($_POST);
        exit;*/

        $tab_href = $tab_href;
        $searchModel = new BookableItemsSearch();
        $dataProvider = $searchModel->searchImages($id);
        $dataProviderBedType = $searchModel->searchBedTypes($id);

        $bed_type = [
            0 => '',
        ];
        $quantity = [
            0 => 1
        ];

        $model = $this->findModel($id);

        $get_amenities = BookableAmenities::find()->where(['item_id' => $id,])->all();
        $get_beds_combinations = BookableBedsCombinations::find()->where(['item_id' => $id])->all();
        $get_bookable_items_names = BookableItemsNames::find()->where(['bookable_item_id' => $id])->orderBy('item_order ASC')->all();

        $allIds = Amenities::find()->select('id')->column();
        $ids = BookableAmenities::find()->select('amenities_id')->where(['item_id' => $model->id])->column();
        $ids = array_merge($ids, array_diff($allIds, $ids));
        $amenities = Amenities::find()
            ->orderBy(new \yii\db\Expression('FIELD (id, '.implode(',', $ids).')'))
            ->all();

        if(!empty($get_bookable_items_names))
        {
            foreach ($get_bookable_items_names as $key => $value) 
            {
                $item_names_arr[$key]['item_name'] = $value->item_name;
                $item_names_arr[$key]['item_number'] = $value->item_number;
                $item_names_arr[$key]['item_order'] = $value->item_order;
            }
            $model->item_names = $item_names_arr;
        }

        if(!empty($get_beds_combinations))
        {
            foreach ($get_beds_combinations as $key => $value) 
            {
                $beds_combinations_arr[] = $value->beds_combinations_id;
            }
            $model->beds_combinations = $beds_combinations_arr;
        }

        if(!empty($get_amenities))
        {
            $saved_amenities = array();
            $saved_banners = array();

            foreach ($get_amenities as $key => $value) 
            {
                $temp = array();
                $temp[] = $value['amenities_id'];
                $temp[] = $value['type'];
                $temp = implode(',', $temp);
                $saved_amenities[$value['amenities_id']] = $temp;

                if($value['can_be_banner'])
                    $saved_banners[] = $value['amenities_id'];
            }
            $model->amenities = $saved_amenities;
            $model->amenities_banners = $saved_banners;
        }

        if(isset($model->bookableItemsHousekeepingStatus))
        {
            foreach ($model->bookableItemsHousekeepingStatus as $value) 
            {
                array_push($model->housekeeping_status_array, $value->housekeeping_status_id);    
            }
            
        }

        if($model->load(Yii::$app->request->post())) 
        {

            $tab_href = $_POST['BookableItems']['tab_href'];
            // echo "<pre>";
            // print_r($tab_href);
            // exit();
            $model->item_names = $_POST['BookableItems']['item_names'];

            // ************* Check Selected Destination has Type Accommodation ************* //

            if($model->destination->provider_type_id == $model->getAccomodationId())
            {
                $model->isAccommodation = 1;
            }
            else
            {
                $model->isAccommodation = 0;
            }

            if(!empty($model->age_participation))
            {
                $age_arr = explode(';', $model->age_participation);
                $model->min_age_participation = $age_arr[0];
                $model->max_age_participation = $age_arr[1];
            }

            $model->amenities = isset($_POST['BookableItems']['amenities_radio']) && !empty($_POST['BookableItems']['amenities_radio'])? $_POST['BookableItems']['amenities_radio']: NULL;

            $model->amenities_banners = isset($_POST['BookableItems']['amenities_checkbox']) && !empty($_POST['BookableItems']['amenities_checkbox'])? $_POST['BookableItems']['amenities_checkbox']: NULL;

            $bed_type = isset($_POST['BookableItems']['bed_type']) ? $_POST['BookableItems']['bed_type']: NULL;
            $quantity = isset($_POST['BookableItems']['quantity']) ? $_POST['BookableItems']['quantity']: NULL;

            $model->beds_combinations = isset($_POST['BookableItems']['beds_combinations']) ? $_POST['BookableItems']['beds_combinations']: NULL;

            if(!empty($model->bookable_periods))
            {
                $periods_arr = explode(';', $model->bookable_periods);
                $model->shortest_period = $periods_arr[0];
                $model->longest_period = $periods_arr[1];
            }

            if($model->transparent_background)
                $model->background_color = 'transparent';

            /*$model->min_price = ltrim($model->server_min_price,'0');
            $model->max_price = ltrim($model->server_max_price,'0');

            if($model->min_price>$model->max_price)
            {
                Yii::$app->session->setFlash('error', 'Min Price should less than Max Price');

                return $this->render('update', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'dataProviderBedType' => $dataProviderBedType,
                    'bed_type' => $bed_type,
                    'quantity' => $quantity,
                    'tab_href' => $tab_href,
                ]);
            }*/

            if($model->save())
            {
        //             echo '<pre>';
        // print_r($_POST);
        // exit;
                ////////////////// Saving  default bed combonations ///////////////
                
                if(isset($_POST['BookableItems']['default_beds_combinations_id']) && !empty($_POST['BookableItems']['default_beds_combinations_id']))
                {
                    // echo "here";
                    // exit();
                    $model->default_bed_combinations_id = $_POST['BookableItems']['default_beds_combinations_id'];
                    if(!$model->save())
                    {
                        echo "<pre>";
                        print_r($model->errors);
                        exit();
                    }
                }
                else
                {
                    $model->default_bed_combinations_id = NULL;
                    if(!$model->save())
                    {
                        echo "<pre>";
                        print_r($model->errors);
                        exit();
                    }
                }
                ////////////////// Updating HouseKeeping statuses //////////////////////////
                BookableItemsHousekeepingStatus::deleteAll(['bookable_item_id' => $model->id]);
                if(isset($_POST['BookableItems']['bookable_item_housekeeping']) && !empty($_POST['BookableItems']['bookable_item_housekeeping']))
                {

                    foreach ($_POST['BookableItems']['bookable_item_housekeeping'] as $status) 
                    {
                       $hk_status = new BookableItemsHousekeepingStatus();
                       $hk_status->bookable_item_id = $model->id;
                       $hk_status->housekeeping_status_id = $status;

                       if(!$hk_status->save())
                       {
                            echo "<pre>";
                            print_r($hk_status->errors);
                            exit();
                       }
                    }
                }
                // *************** Update Record depend on destination type ***************//

                if($model->isAccommodation)
                {
                    // $model->max_guests = NULL;
                    // $model->max_adults = NULL;
                    // $model->max_children = NULL;
                    // $model->update();

                    // *************** Save Bed Types ************* //

                    if(!empty($bed_type))
                    {
                        foreach ($bed_type as $key => $value) 
                        {
                            if(!empty($value))
                            {
                                $temp = BookableBedTypes::findOne(['bookable_id'=>$model->id, 'bed_type_id' => $value,'quantity' => $quantity[$key]]);
                                if(empty($temp))
                                {
                                    $obj = new BookableBedTypes();
                                    $obj->bookable_id = $model->id;
                                    $obj->bed_type_id = $value;
                                    $obj->quantity = $quantity[$key];
                                    $obj->save();
                                }
                            }
                        } 
                    }

                    BookableBedsCombinations::deleteAll(['item_id' => $model->id]);

                    // *************** Save Beds Combination ************* //

                    if(!empty($model->beds_combinations))
                    {
                        foreach ($model->beds_combinations as $key => $value) 
                        {
                            $obj = new BookableBedsCombinations();
                            $obj->item_id = $model->id;
                            $obj->beds_combinations_id = $value;
                            $obj->save();
                        }
                    }
                }
                else
                {
                    $model->sleeping_arrangement_id = NULL;
                    $model->update();
                }

                // *************** Save Amenities ************* //

                if(is_null($model->amenities_banners))
                    $model->amenities_banners = [];

                if(!empty($model->amenities))
                {
                    BookableAmenities::deleteAll(['item_id' => $id]);
                    foreach ($model->amenities as $key => $value) 
                    {
                        $arr = explode(',', $value);

                        $obj = new BookableAmenities();
                        $obj->item_id = $model->id;
                        $obj->amenities_id = $arr[0];
                        $obj->type = $arr[1];
                        $obj->can_be_banner = in_array($arr[0], $model->amenities_banners)?1:0;
                        $obj->save(); 
                    }
                }

                // ************** Save Item Names ************** //

                $to_not_be_deleted = array();
                for ($i=1; $i <=$model->item_quantity; $i++) 
                {
                    array_push($to_not_be_deleted, $model->item_names[$i-1]['item_name']);
                }       
                        // DestinationTravelPartners::deleteAll(['destination_id' => $provider_model->id]);
                BookableItemsNames::deleteAll(['and', 
                                                            'bookable_item_id = :type_id', 
                                                            ['not in', 'item_name', $to_not_be_deleted]], 
                                                            [':type_id' =>$model->id ]
                                                        );
                // BookableItemsNames::deleteAll(['bookable_item_id' => $model->id]);

                for ($i=1; $i <=$model->item_quantity; $i++) 
                {
                    $obj = BookableItemsNames::findOne(['bookable_item_id' => $model->id,'item_name' => $model->item_names[$i-1]['item_name']]);
                    if(empty($obj))
                    {
                        $obj = new BookableItemsNames();
                    }
                    $obj->bookable_item_id = $model->id;
                    $obj->item_number = $model->item_names[$i-1]['item_number'];
                    $obj->item_name = $model->item_names[$i-1]['item_name'];
                    $obj->item_order = $model->item_names[$i-1]['item_order'];
                    $obj->save();
                }

                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'image')) 
                {
                    if($uploadFile->uploadFile('bookable-items',$model->id,'images'))
                    {
                        if(!empty($model->featured_image))
                        {
                            $prev_image = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images/'.$model->featured_image;
                            if(file_exists($prev_image))
                                unlink($prev_image);
                        }

                        $model->featured_image = $uploadFile->FileName;
                        $model->update();
                    }
                }

                Yii::$app->session->setFlash('success', 'Bookable Items Updated Successfully');

                if(!file_exists(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id))
                {
                    mkdir(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id, 0777, true);
                }

                if(!file_exists(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images'))
                {
                     mkdir(Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images', 0777, true);
                }

                $srcPath = Yii::getAlias('@app'). '/../uploads/bookable-items/temp/images/';
                $destPath = Yii::getAlias('@app').'/../uploads/bookable-items/'.$model->id.'/images/';

                if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                {
                    foreach ($_SESSION['images'] as $key => $value) 
                    {
                        if(copy($srcPath.$value, $destPath.$value))
                        {
                            $obj = new BookableItemsImages();
                            $obj->bookable_items_id = $model->id;
                            $obj->image = $value;

                            $last_model = BookableItemsImages::find()
                                                        ->where(['bookable_items_id' => $model->id])
                                                        ->orderBy('display_order DESC')
                                                        ->one();
                            if(!empty($last_model))
                            {
                                $obj->display_order = $last_model->display_order+1;
                            }
                            else
                            {
                                $obj->display_order = 1;
                            }

                            $obj->save();

                            unlink($srcPath.$value);
                        }   
                    }

                    $this->removeDirectory($srcPath);
                }
            }
            
            $errors['BookableItems'] = $model->getErrors();

            /*return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderBedType' => $dataProviderBedType,
                'bed_type' => $bed_type,
                'quantity' => $quantity,
                'tab_href' => $tab_href,
            ]);*/

            //return $this->redirect(['update','id' => $id, 'tab_href' => $tab_href]);
            Yii::$app->session->setFlash('success','Record is updated Successfully.');

            if(isset($_POST['save']))
            {
                // echo "<pre>";
                // print_r($tab_href);
                // exit();
                return $this->redirect(['update','id' => $model->id,'tab_href' => $tab_href]);
            }
            else
            {
                return $this->redirect(['index']);
            }
            //return $this->redirect(['index']);
        } 
        else 
        {
            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                unset($_SESSION['images']);

            if($model->background_color=='transparent')
                $model->transparent_background = 1;

            if($model->destination->provider_type_id == $model->getAccomodationId())
            {
                $model->isAccommodation = 1;
                // $model->max_guests = '0;1';
                // $model->max_adults = '1';
                // $model->max_children = '0';
            }
            else
            {
                $model->isAccommodation = 0;
            }

            $model->server_min_price = $model->min_price;
            $model->server_max_price = $model->max_price;

            return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderBedType' => $dataProviderBedType,
                'bed_type' => $bed_type,
                'quantity' => $quantity,
                'tab_href' => $tab_href,
                'amenities' => $amenities,
            ]);
        }
    }

    public function actionDeleteImage()
    {
        $image = BookableItemsImages::findOne(['id'=>$_POST['image_id']]);

        if(!empty($image))
        {
            $Path = Yii::getAlias('@app').'/../uploads/bookable-items/'.$image->bookable_items_id.'/images/'.$image->image;
            unlink($Path);
            $image->delete();
        }
    }

    public function actionDeleteBedType()
    {
        $obj = BookableBedTypes::findOne(['id'=>$_POST['id']]);
        $obj->delete();
    }

    /**
     * Deletes an existing BookableItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $path = Yii::getAlias('@app'). '/../uploads/bookable-items/'.$id;
        if(is_dir($path))
        {
            $this->removeDirectory($path);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the BookableItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BookableItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BookableItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListDestinationValues($provider_id)
    {
        $html = '<div><span class="label label-danger"> Whoops! </span>
                        <span>&nbsp;  No Amenities found </span></div>';

        if($provider_id=="null" || empty($provider_id))
        {
            echo json_encode([
                'amenities' => $html,
                'error_case' => 'true',
                'accommodation_type' => '', 
                'general_booking' => 'Not Found',
                'flag' => '',
                'status' => '',
                'confirmation' => '',
                'yng' => '0',
                'olf' => '0',
            ]);
        }
        else
        {
            $destination = Destinations::findOne(['id' => $provider_id]);
            if($destination->getDestinationTypeName()!='Accommodation')
            {
                echo json_encode([
                    'amenities' => $html,
                    'error_case' => 'true',
                    'accommodation_type' => 0, 
                    'general_booking' => 'Not Found',
                    'flag' => '',
                    'status' => '',
                    'confirmation' => '',
                    'yng' => '0',
                    'olf' => '0',
                ]);
            }
            else
            {
                // *********** Get General Booking Cancellation *************//
                $booking_rules_model = BookingRules::findOne(['provider_id' => $provider_id]);
                $booking_policies_model = BookingPolicies::findOne(['provider_id' => $provider_id]);

                $destination_amenities = [];
                $destination_banners = [];
                $destination_amenities_model = DestinationsAmenities::find()->where(['provider_id' => $provider_id])->orderBy('id')->all();

                foreach ($destination_amenities_model as $key => $value) 
                {
                    $destination_amenities[$value->amenities_id] = $value->amenities_id.','.$value->type;
                    if($value->can_be_banner)
                        $destination_banners[] = $value->amenities_id;
                }

                echo json_encode([
                    'amenities' => $this->renderAjax('total_amenities_dynamic', [
                                            'selected_amenities' => $destination_amenities,
                                            'selected_banners' => $destination_banners,
                                        ]),
                    'error_case' => 'false',
                    'accommodation_type' => 1, 
                    'general_booking' => $booking_rules_model->general_booking_cancellation,
                    'flag' => $booking_rules_model->new_booking_type,
                    'status' => $booking_rules_model->new_booking_status,
                    'confirmation' => $booking_rules_model->routine_booking_confirmation,
                    'yng' => $booking_policies_model->youngest_age,
                    'olf' => $booking_policies_model->oldest_free_age,
                ]);
            }
        }
    }

    public function actionGetBedTypesHtml()
    {
        $bedTypes = BedTypes::find()->all();
        echo json_encode([
            'bed_types_html' => $this->renderAjax('bed_types_dynamic_html', [
                                    'bedTypes' => $bedTypes,
                                ]),
        ]);
    }

    // functionality for closing / opening bookable item unit

    public function actionUnitScheduler($destination_id = null, $exp_date = null)
    {
        if($exp_date == NULL)
        {
            $date = date("Y-m-d");
            //$date = explode("-", $current_date);
            //$date = $date[2];
        }
        else
        {
            $date = $exp_date;
            //$date = $date[2];   
        }
        // echo "<pre>";
        // print_r($date[2]);
        // exit();
        return $this->render('unit_schedular',[
            'destination_id' => $destination_id,
           // 'bookable_item_id' => $bookable_item_id,
            'date'  => $date,
            // 'date'  => 25-1,
        ]);
    }

    public function actionGetRatesSchedular() // use for Scheduler
    {
        $session = Yii::$app->session;
        $session->open();
        
        // $bookingsItemsModel = BookingsItems::find()
        //                         ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
        //                         ->all();
    
        $bookings_items_arr = [];
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);

        if(!empty($destination->bookableItems))
        {
            $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            $closed_dates_arr = array();
            if(!empty($closed_dates))
            {
                foreach ($closed_dates  as $date) 
                {
                    array_push($closed_dates_arr, $date->date);
                }
            }

            // $schedular_resources = $this->generateSchedularResources(Destinations::findOne(['id' => $_POST['destination_id'] ]));
            $schedular_resources = [];
            // echo "<pre>";
            // print_r($schedular_resources);
            // exit();
            // foreach ($bookingsItemsModel as $key => $model) 
            // {
            //     $bookings_items_arr[] = $this->generateSchedularEvent($model);
            // }

            // $events = $this->generateSchedularEvent(Destinations::findOne(['id' => $_POST['destination_id'] ]));
            $events = [];
            // echo "<pre>";
            // print_r($events);
            // exit();
            // echo "<pre>";
            // print_r($schedular_resources);
            // exit();
            

            echo json_encode([
                'events' => json_encode($events),
                'bookable_items' => $this->renderAjax('bookable_item_unit_scheduler',['provider_id' => $_POST['destination_id']]),
                'resources'     => json_encode($schedular_resources),
                'empty' => 0,
                'date' => date('Y-m-d'),
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
            $params['provider_id'] =  $_POST['destination_id'];
            $session[Yii::$app->controller->id.'-booking-schedular-values'] = json_encode($params);
                
        }
        else
        {
            echo json_encode([
                'empty' => 1,
            ]); 
        }
    }



    // generating events and resources for ajax calls //

    public function actionGenerateSchedularEvents()
    {
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        // $month_start = date('Y-m-d',strtotime($_POST['date']));
        // $month_start = date('Y-m-01',strtotime($_POST['date']));
        // $month_end =  date('Y-m-t',strtotime($_POST['date']));

        $month_start = date('Y-m-d',strtotime($_POST['start']));
        $month_end =  date('Y-m-d',strtotime($_POST['end']));
        // echo "<pre>";
        // print_r(date('Y-m-d',strtotime($_POST['start'])));
        // print_r(date('Y-m-d',strtotime($_POST['end'])));
        // exit();
        // generating events // 


        $events = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                foreach ($bookable_item->bookableItemsNames as $item_name) 
                {

                    foreach ($bookable_item->rates as $rate) 
                    {   
                        if($rate->published == 1)
                        {

                            if( (strtotime($month_start) <  strtotime($rate->from)  &&  strtotime($month_end) > strtotime($rate->from) )
                                ||  (strtotime($month_start) <  strtotime($rate->to)  &&  strtotime($month_end) > strtotime($rate->from) )
                                ||  (strtotime($month_start) >  strtotime($rate->to)  &&  strtotime($month_end) < strtotime($rate->from) )
                                )
                            {
                                if($destination->pricing == 1)
                                {
                                    foreach ($rate->dates as $date) 
                                    {
                                        if(strtotime($date->night_rate) >=  strtotime($month_start) && strtotime($date->night_rate) <=  strtotime($month_end))
                                        {

                                            $rate_item['id'] = $rate->id;
                                            
                                            $rate_override = UnitAvailability::findOne(['date' => $date, 'unit_id' => $item_name->id,'bookable_item_id' => $rate->unit_id]);
                                            
                                            if(empty($rate_override))
                                            {
                                               $rate_item['title'] = 'OPEN';
                                            }
                                            else
                                            {
                                                // red color if unit is closed
                                                $rate_item['title'] = 'CLOSED';
                                            }
                                            
                                            $rate_item['resourceId'] = $item_name->id;
                                            $rate_item['start'] = $date->night_rate;
                                            $rate_item['end'] = $date->night_rate;
                                            // $rate_item['borderColor'] = '#000000';
                                            // $rate_item['textColor'] = 'green';
                                            if(empty($rate_override))
                                            {
                                                $rate_item['textColor'] = 'green';
                                            }
                                            else
                                            {
                                                // red color if unit is closed
                                                $rate_item['textColor'] = 'red';
                                            }
                                            // $rate_item['bookable_item_id'] = $bookable_item->id;
                                            $rate_item['bookable_item_id'] = $item_name->id;
                                            // $rate_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);
                                            $closed_dates = DestinationsOpenHoursExceptions::findOne(['provider_id' => $rate->destination->id, 'state' => 0,'date' => $date->night_rate]);
                                            if(empty($closed_dates))
                                            {
                                                array_push($events, $rate_item);    
                                            }
                                            
                                        }
                                            
                                    }
                                }
                                else
                                {
                                    // if(strtotime($date->night_rate) >=  strtotime($month_start) && strtotime($date->night_rate) <=  strtotime($month_end))
                                    // {
                                        $rate_from = date('Y-m-d',strtotime($rate->from));
                                        $rate_to = date('Y-m-d',strtotime($rate->to));

                                        $date1=date_create($rate_from);
                                        $date2=date_create($rate_to);
                                        $diff=date_diff($date1,$date2);
                                        $difference =  $diff->format("%a");

                                        $date = $rate_from;
                                        $date = date("Y-m-d", strtotime($date));

                                        for ($i=0; $i <= $difference ; $i++) 
                                        { 
                                            $rate_item['id'] = $rate->id;
                                            
                                            $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id,'bookable_item_id' => $rate->unit_id]);
                                            $name ='';
                                            
                                            if(empty($rate_override))
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate->item_price_first_adult).', '. Yii::$app->formatter->asDecimal($rate->item_price_additional_adult).', '.Yii::$app->formatter->asDecimal($rate->item_price_first_child).', '.Yii::$app->formatter->asDecimal($rate->item_price_additional_child);
                                            }
                                            else
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate_override->item_price_first_adult).', '. Yii::$app->formatter->asDecimal($rate_override->item_price_additional_adult).', '.Yii::$app->formatter->asDecimal($rate_override->item_price_first_child).', '.Yii::$app->formatter->asDecimal($rate_override->item_price_additional_child);
                                            }
                                            $name .= ''.$price;
                                            
            
                                            // if($name == '' )
                                            // {
                                            //     $rate_item['title'] = $rate->id.' - '.$name;
                                            // }
                            
                                            // // }
                                            // else
                                            // {
                                            //     $rate_item['title'] = $name;
                                            // }
                                            // $rate_item['title'] = $rate->ratesCapacityPricings[0]->price;
                                            // $rate_item['title'] = $rate->id.' - '.$name;
                                            // $rate_item['resourceId'] = $rate->id;
                                            $rate_item['title'] = 'Open';
                                            $rate_item['resourceId'] = $item_name->id;
                                            $rate_item['start'] = $date;
                                            $rate_item['end'] = $date;
                                            // $rate_item['borderColor'] = '#000000';
                                            // if(empty($rate_override))
                                            // {
                                            //     $rate_item['textColor'] = 'black';
                                            // }
                                            // else
                                            // {
                                            //     if($rate_override->bookable_item_closed == 1)
                                            //     {
                                            //         $rate_item['textColor'] = 'orange';
                                            //         // $rate_item['backgroundColor'] = 'red';
                                            //     }
                                            //     else
                                            //     {
                                            //         $rate_item['textColor'] = 'red';
                                            //     }
                                            // }
                                            $rate_item['textColor'] = 'green';
                                            // $rate_item['bookable_item_id'] = $bookable_item->id;
                                            $rate_item['bookable_item_id'] = $item_name->id;
                                            // $rate_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);
                                            
                                            array_push($events, $rate_item);

                                            $date = strtotime("+1 day", strtotime($date));
                                            $date = date("Y-m-d", $date);
                                        }
                                    // }
                                           
                                }
                            }
                                
                           
                        }
                    }
                }
            }
            
        }

        echo json_encode([
                'events' => $events,
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }
    // generating schedular resources //
    public function actionGenerateSchedularResources()
    {
        // $month_start = date('Y-m-01',strtotime(date('Y-m-d')));
        // $month_end =  date('Y-m-t',strtotime(date('Y-m-d')));
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        // $month_start = date('Y-m-d',strtotime($_POST['date']));
        // $month_start = date('Y-m-01',strtotime($_POST['date']));
        // $month_end =  date('Y-m-t',strtotime($_POST['date']));

        $month_start = date('Y-m-d',strtotime($_POST['start']));
        $month_end =  date('Y-m-d',strtotime($_POST['end']));

        // echo "<pre>";
        // print_r($search_form);
        // print_r($search_to);
        // exit();
        $resources = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                $item_names_count = count($bookable_item->bookableItemsNames);
                $counter = 1;
                foreach ($bookable_item->bookableItemsNames as $item_name) 
                {   
                    $item_array = array();
                    $item_array['id'] = $item_name->id;
                    $item_array['item'] = $bookable_item->itemType->name;
                    $item_array['title'] = $item_name->item_name;
                    $item_array['eventColor'] = $bookable_item->background_color;
                    // $item_array['bookable_item_id'] = $bookable_item->id;
                    $item_array['bookable_item_id'] = $item_name->id;
                    $item_array['background_color'] = $bookable_item->background_color;
                    $item_array['text_color'] = $bookable_item->text_color;

                    array_push($resources, $item_array);
                    // if($counter == $item_names_count)
                    // {
                    //     $item_array = array();
                    //     $item_array['id'] = $bookable_item->id.$destination->id;
                    //     $item_array['item'] = $bookable_item->itemType->name;
                    //     $item_array['title'] = "No Unit Assigned";
                    //     $item_array['eventColor'] = $bookable_item->background_color;
                    //     $item_array['bookable_item_id'] = $bookable_item->id;
                    //     $item_array['background_color'] = $bookable_item->background_color;
                    //     $item_array['text_color'] = $bookable_item->text_color;

                    //     array_push($resources, $item_array);
                    // }
                    $counter++;
                }
            }
            // return $resources;
        }

        echo json_encode([
                'resources' => $resources,
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }
    // getting dropdown for bookable item names dropdown //
    public function actionGetRatesInputs()
    {
        // $rate = Rates::findOne(['id' => $_POST['rate_id'] ]);
        // $bookable_item_id = $_POST['bookable_item_id'];
        // $is_bookabele_item_closed = RatesOverride::findOne(['rate_id'=> $rate->id,'date' => $_POST['date'],'bookable_item_id' => $bookable_item_id, 'bookable_item_closed' => 1]);

        $is_bookabele_item_closed = UnitAvailability::findOne(['date' => $_POST['date'], 'unit_id' =>  $_POST['bookable_item_id'] ]);


        echo json_encode([
                // 'events' => $events,
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date']]),
                'is_item_closed' => !empty($is_bookabele_item_closed)?1:0
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }
    
    // closing bookable items //

    public function actionCloseBookableItem()
    {

        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        $bookable_items_names =  isset($_POST['bookable_items'])?$_POST['bookable_items']:[];
        $rate_from = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_from'])));
        $rate_to = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_to'])));
        $rate_days = isset($_POST['rate_days'])?$_POST['rate_days']:[];
        // $override_type = ($_POST['override_type'])?1:0;
        // $cpricing = isset($_POST['cpricing'])?$_POST['cpricing']:[];

        $date1=date_create($rate_from);
        $date2=date_create($rate_to);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");


        // if($destination->pricing == 1)
        // {
            foreach ($bookable_items_names as $value) 
            {
                $bookable_item_name = BookableItemsNames::findOne(['id' => $value]);
                // $rates = Rates::find()->where(['unit_id' => $value])->all();
                $rates = Rates::find()->where(['unit_id' => $bookable_item_name->bookable_item_id])->all();

                if(!empty($rates))
                {
                    foreach ($rates as $rate) 
                    {
                        $date = $rate_from;
                        $date = date("Y-m-d", strtotime($date));
                        for ($i=0; $i <= $difference ; $i++)
                        {
                            
                            if(strtotime($date)>= strtotime($rate->from) && (strtotime($date)<= strtotime($rate->to)) )
                            {
                                $day = date('N', strtotime($date));
                                if($rate_days[$day-1] == 'true')
                                {
                                    $rate_override = UnitAvailability::findOne(['date' => $date, 'unit_id' => $value,'bookable_item_id' => $rate->unit_id]);
                                    if(empty($rate_override))
                                    {
                                        $rate_override = new UnitAvailability;
                                    }
                                    
                                    // $rate_override->rate_id = $rate->id;
                                    $rate_override->bookable_item_id = $rate->unit_id;
                                    $rate_override->unit_id = $value;
                                    $rate_override->date = $date;
                                    
                                    $rate_override->save();

                                }
                            }
                            $date = strtotime("+1 day", strtotime($date));
                            $date = date("Y-m-d", $date);
                        }
                    }
                }
                
            }
        // }
        // else
        // {

        // }

        $schedular_resources = [];

        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

        $closed_dates_arr = array();
        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                array_push($closed_dates_arr, $date->date);
            }
        }
        // exit();
        // $dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        // $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        echo json_encode([
                'success' => 'true',
                'events' => [],
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                'resources'     => $schedular_resources,
                'empty' => 0,
                'date' => $rate_from,
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

    //open bookable item

    public function actionOpenBookableItem()
    {

        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        $bookable_items_names =  isset($_POST['bookable_items'])?$_POST['bookable_items']:[];
        $rate_from = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_from'])));
        $rate_to = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_to'])));
        $rate_days = isset($_POST['rate_days'])?$_POST['rate_days']:[];
        // $override_type = ($_POST['override_type'])?1:0;
        // $cpricing = isset($_POST['cpricing'])?$_POST['cpricing']:[];

        $date1=date_create($rate_from);
        $date2=date_create($rate_to);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");


        // if($destination->pricing == 1)
        // {
            foreach ($bookable_items_names as $value) 
            {
                $bookable_item_name = BookableItemsNames::findOne(['id' => $value]);
                // $rates = Rates::find()->where(['unit_id' => $value])->all();
                $rates = Rates::find()->where(['unit_id' => $bookable_item_name->bookable_item_id])->all();

                if(!empty($rates))
                {
                    foreach ($rates as $rate) 
                    {
                        $date = $rate_from;
                        $date = date("Y-m-d", strtotime($date));
                        for ($i=0; $i <= $difference ; $i++)
                        {
                            
                            if(strtotime($date)>= strtotime($rate->from) && (strtotime($date)<= strtotime($rate->to)) )
                            {
                                $day = date('N', strtotime($date));
                                if($rate_days[$day-1] == 'true')
                                {
                                    $rate_override = UnitAvailability::findOne(['date' => $date, 'unit_id' => $value,'bookable_item_id' => $rate->unit_id]);
                                    if(!empty($rate_override))
                                    {
                                        $rate_override->delete();
                                    }

                                }
                            }
                            $date = strtotime("+1 day", strtotime($date));
                            $date = date("Y-m-d", $date);
                        }
                    }
                }
                
            }
        // }
        // else
        // {

        // }

        $schedular_resources = [];

        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

        $closed_dates_arr = array();
        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                array_push($closed_dates_arr, $date->date);
            }
        }
        // exit();
        // $dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        // $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        echo json_encode([
                'success' => 'true',
                'events' => [],
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                'resources'     => $schedular_resources,
                'empty' => 0,
                'date' => $rate_from,
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

}

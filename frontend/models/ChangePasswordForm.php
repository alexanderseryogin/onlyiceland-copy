<?php
namespace frontend\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use common\models\User;

/**
 * Password reset form
 */
class ChangePasswordForm extends Model
{
	public $current_password;
    public $new_password;

    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($id, $config = [])
    {
        if (!isset($id) || empty($id)) {
            throw new InvalidParamException('User Id cannot be blank.');
        }
        $this->_user = User::findIdentity($id);
        if (!$this->_user) {
            throw new InvalidParamException('Invalid user ID.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //http://stackoverflow.com/questions/8141125/regex-for-password-php
        return [
            [['new_password', 'current_password'], 'required'],
            ['new_password', 'string', 'min' => 8],
            //['password', 'match', 'pattern' => '/^\S*(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\W])(?=\S*[\d])\S*$/', 'message' => 'The password length must be greater than or equal to 8, must contain one or more uppercase characters, must contain one or more lowercase characters, must contain one or more numeric values, must contain one or more special characters.'],
            ['new_password', 'match', 'pattern' => '/^\S*(?=\S*[a-z])\S*$/', 
                'message' => 'The password must contain one or more lowercase characters'],

            ['new_password', 'match', 'pattern' => '/^\S*(?=\S*[A-Z])\S*$/', 
                'message' => 'The password length must contain one or more uppercase characters'],

            ['new_password', 'match', 'pattern' => '/^\S*(?=\S*[\W])\S*$/', 
                'message' => 'The password length must contain one or more special characters'],

            ['new_password', 'match', 'pattern' => '/^\S*(?=\S*[\d])\S*$/', 
                'message' => 'The password must contain one or more numeric values'],

        ];
    }

    //matching the old password with existing password and saving new password
    public function changePassword()
    {
    	$user = $this->_user;
        if ($user->validatePassword($this->current_password))
        {
            $user->setPassword($this->new_password);
            return $user->save(false);
        }

        return false;
    }

}
<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BookingPolicies;

/**
 * BookingPoliciesSearch represents the model behind the search form about `common\models\BookingPolicies`.
 */
class BookingPoliciesSearch extends BookingPolicies
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_id', 'booking_type_id', 'booking_status_id', 'youngest_age', 'oldest_free_age'], 'integer'],
            [['payment_methods_gauarantee', 'payment_methods_checkin', 'checkin_time_from', 'checkin_time_to', 'checkout_time_from', 'checkout_time_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BookingPolicies::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'booking_type_id' => $this->booking_type_id,
            'booking_status_id' => $this->booking_status_id,
            'youngest_age' => $this->youngest_age,
            'oldest_free_age' => $this->oldest_free_age,
            'checkin_time_from' => $this->checkin_time_from,
            'checkin_time_to' => $this->checkin_time_to,
            'checkout_time_from' => $this->checkout_time_from,
            'checkout_time_to' => $this->checkout_time_to,
        ]);

        $query->andFilterWhere(['like', 'payment_methods_gauarantee', $this->payment_methods_gauarantee])
            ->andFilterWhere(['like', 'payment_methods_checkin', $this->payment_methods_checkin]);

        return $dataProvider;
    }
}

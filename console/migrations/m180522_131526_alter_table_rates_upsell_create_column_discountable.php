<?php

use yii\db\Migration;

class m180522_131526_alter_table_rates_upsell_create_column_discountable extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('rates_upsell', 'discountable', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('rates_upsell', 'discountable');
    }
}

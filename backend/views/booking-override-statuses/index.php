<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\components\Helpers;
use common\models\BookingOverrideStatuses;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingOverrideStatusesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Booking Override Statuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-override-statuses-index">

     <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BOOKING OVERRIDE STATUS'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!--  <p>
        <?= Html::a('Create Booking Override Statuses', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'label',
                'value' => function($data)
                {
                    return $data->label;
                },
                'contentOptions' => function($data)
                {
                    return ['style' => 'color:'.$data->text_color.'; background:'.$data->background_color.';' ];
                },
            ],
            'background_color',
            'text_color',
            // 'type',
            [
                'attribute' => 'type',
                'value' => function($data) {
                    return Helpers::overridestatusesLables($data->type); 
                },
                'format' => 'raw',
                'filter' => Html::dropDownList(
                    'BookingOverrideStatusesSearch[type]', 
                    $searchModel['type'], 
                    [
                        ""=>"",
                        BookingOverrideStatuses::TYPE_CUSTOM => Helpers::overridestatusesLables(BookingOverrideStatuses::TYPE_CUSTOM, true), 
                        BookingOverrideStatuses::TYPE_DEFAULT => Helpers::overridestatusesLables(BookingOverrideStatuses::TYPE_DEFAULT, true)
                        
                    ],
                    ['class'=>'form-control',
                    'id' => 'type_dropdown']),
               
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>['style' => 'width:10%'],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'force_reset' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-off"></span>', Url::to(['force-reset', 'id'=>$model->id]), 
                        [
                            'title' => 'Reset Password',
                        ]);
                    }
                ],
                'visibleButtons' => [
                    // hide delete button for administrator who is logged in
                    'delete' => function ($model, $key, $index) {
                        return $model->type == BookingOverrideStatuses::TYPE_CUSTOM ? true : false;
                    },
                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php

use yii\db\Migration;

class m161223_121307_alter_open_hours_exceptions_change_date extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours_exceptions` DROP COLUMN `date_to`, CHANGE `date_from` `date` DATE NULL; 
        ");
    }

    public function down()
    {
        echo "m161223_121307_alter_open_hours_exceptions_change_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

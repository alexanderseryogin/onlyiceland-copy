<?php

use yii\db\Migration;

class m170519_112527_alter_booking_dates_added_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_dates`   
          CHANGE `item_id` `item_id` INT(11) NULL  AFTER `date`,
          ADD COLUMN `rate_id` INT(11) NULL AFTER `item_id`,
          ADD COLUMN `no_of_nights` INT(11) NULL AFTER `quantity`,
          ADD COLUMN `estimated_arrival_time_id` INT(11) UNSIGNED NULL AFTER `no_of_nights`,
          ADD COLUMN `beds_combinations_id` INT(11) UNSIGNED NULL AFTER `estimated_arrival_time_id`,
          ADD COLUMN `user_id` INT(11) NULL AFTER `beds_combinations_id`,
          ADD COLUMN `guest_first_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `user_id`,
          ADD COLUMN `guest_last_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `guest_first_name`,
          ADD COLUMN `guest_country_id` INT(11) NULL AFTER `guest_last_name`,
          ADD COLUMN `confirmation_id` INT(11) NULL AFTER `guest_country_id`,
          ADD COLUMN `booking_cancellation` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `confirmation_id`,
          ADD COLUMN `flag_id` INT(11) NULL AFTER `booking_cancellation`,
          ADD COLUMN `status_id` INT(11) NULL AFTER `flag_id`,
          ADD COLUMN `no_of_children` INT(11) NULL AFTER `status_id`,
          ADD COLUMN `no_of_adults` INT(11) NULL AFTER `no_of_children`,
          ADD COLUMN `person_no` INT(11) NULL AFTER `no_of_adults`,
          ADD COLUMN `person_price` INT(11) NULL AFTER `person_no`,
          ADD COLUMN `total` DECIMAL(8,2) NULL AFTER `person_price`,
          ADD COLUMN `price_json` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `total`,
          ADD COLUMN `travel_partner_id` INT(11) NULL AFTER `price_json`,
          ADD COLUMN `offers_id` INT(11) NULL AFTER `travel_partner_id`,
          ADD CONSTRAINT `FK_booking_dates_country_id` FOREIGN KEY (`guest_country_id`) REFERENCES `countries`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          ADD CONSTRAINT `FK_booking_dates_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          ADD CONSTRAINT `FK_booking_dates_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          ADD CONSTRAINT `FK_booking_dates_beds_combinations_id` FOREIGN KEY (`beds_combinations_id`) REFERENCES `beds_combinations`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          add constraint `FK_booking_dates_flag_id` foreign key (`flag_id`) references `booking_types`(`id`) on update Set null on delete Set null,
          add constraint `FK_booking_dates_status_id` foreign key (`status_id`) references `booking_statuses`(`id`) on update Set null on delete Set null,
          add constraint `FK_booking_dates_offers_id` foreign key (`offers_id`) references `offers`(`id`) on update Set null on delete Set null,
          add constraint `FK_booking_dates_travel_partner_id` foreign key (`travel_partner_id`) references `travel_partner`(`id`) on update Set null on delete Set null,
          add constraint `FK_booking_dates_user_id` foreign key (`user_id`) references `user`(`id`) on update Set null on delete Set null;
        ");
    }

    public function down()
    {
        echo "m170519_112527_alter_booking_dates_added_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

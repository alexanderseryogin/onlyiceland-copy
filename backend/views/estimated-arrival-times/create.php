<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EstimatedArrivalTimes */

$this->title = Yii::t('app', 'Create Estimated Arrival Times');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estimated Arrival Times'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estimated-arrival-times-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Gallery;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PlacesOfInterestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Places Of Interests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="places-of-interest-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW PLACE OF INTEREST'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'places-of-interest-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'description:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>['style' => 'width:10%'],
                'template' => '{view} {update} {delete} {gallery}',
                'buttons' => [
                    'gallery' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-picture"></span>', Url::to(['/gallery/index', 'id'=>$model->id, 'type'=>Gallery::GALLERY_TYPE_POI]), 
                        [
                            'title' => 'Edit Gallery',
                            'data-pjax' => '0',
                            //'class' => 'btn btn-primary',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>

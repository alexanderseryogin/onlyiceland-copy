<?php

use yii\db\Migration;

class m161020_115308_create_table_upsell_items extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `upsell_items`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `type` INT(11) NOT NULL, `description` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL, `amount` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (`id`), CONSTRAINT `FK_upsell_items_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161020_115308_create_table_upsell_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

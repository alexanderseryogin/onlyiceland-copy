<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
use common\models\BookingsItems;

$title = 'Daily Cleaning Breif Report';
$this->title = $title;
?>
<table class="table table-striped table-bordered daily_unit_report_table table2excel" >
	<thead style="display: none;">
		<tr>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
		</tr>
		
	</thead>
	<tbody>
	
    	<?php
    		$date = $report_from;
	       
	        $date1=date_create($report_to);
	        $date2=date_create($report_from);
	        $diff=date_diff($date1,$date2);
	        $difference =  $diff->format("%a");

	        for ($i=0; $i <= $difference ; $i++)
	        {
	            // $data_array = array();
	            // $average_stay = 0.0;

	            echo "<tr>";
		            echo "<td  style='font-size:25px;border-right: 0px;'>";
		            echo date('d.m.Y',strtotime($date));
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-left:0px;'>";
		            echo "</td>";
	            echo "</tr>";
	            if($cancelled_bookings)
	            {
	            	$bookings = BookingsItems::find()->where(['or',['arrival_date'=> $date],['departure_date'=>$date]])->andWhere(['deleted_at' => 0 ])->all();

	                $arrivals = BookingsItems::find()->where(['arrival_date' => $date])->andWhere(['deleted_at' => 0 ])->count();
	                $departures = BookingsItems::find()->where(['departure_date' => $date])->andWhere(['deleted_at' => 0 ])->count();

	                echo "<tr>";
		            echo "<td style='border-right: 0px;'>";
		            echo $arrivals.' Arrivals'.' /'.$departures.' Departures';
		            echo "</td>";
		            
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-left:0px;'>";
		            echo "</td>";
		            echo "</tr>";
	                if(!empty($bookings))
	                {
	                	foreach ($bookings as $booking) 
	                	{
	                		echo "<tr>";

	                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
		                    if(!empty($booking->item_name_id))
		                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
		                    else
		                         $unit_name = '- - - - -'.' ';

				           
			            	echo "<td>";
				            echo $unit_name;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->item->itemType->name;
				            echo "</td>";

				            $bookingDateModel = $booking->FirstBookingDate();

		                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations))
		                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
		                    else
		                         $bed_pref = '- - - - -'.' ';

				            echo "<td >";
				            echo $bed_pref;
				            echo "</td>";

				            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

				            echo "<td>";
				            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->id;
				            echo "</td>";


	                		$date1=date_create($booking->departure_date);
		                    $date2=date_create($booking->arrival_date);
		                    $diff=date_diff($date1,$date2);
		                    $difference1 =  $diff->format("%a");

	                		
			            	echo "<td>";
				            echo $difference1;
				            echo "</td>";
			            	
			            	echo "<td>";
				            echo date('d.m.Y', strtotime($booking->arrival_date));
				            echo "</td>";

				            echo "<td>";
				            echo date('d.m.Y', strtotime($booking->departure_date));
				            echo "</td>";
			            	
			            	echo "<td>";
				            // echo ;
				            echo "</td>";

				            echo "</tr>";

	                	}
	                }


	                $bookings = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['deleted_at' => 0 ])->all();

	                $stay_through_count = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['deleted_at' => 0 ])->count();

	                echo "<tr>";
		            echo "<td style='border-right: 0px;'>";
		            echo $stay_through_count.' Staying through';
		            echo "</td>";
		            
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-left:0px;'>";
		            echo "</td>";
		            echo "</tr>";
	                if(!empty($bookings))
	                {
	                	foreach ($bookings as $booking) 
	                	{
	                		echo "<tr>";

	                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
		                    if(!empty($booking->item_name_id))
		                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
		                    else
		                         $unit_name = '- - - - -'.' ';

				           
			            	echo "<td>";
				            echo $unit_name;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->item->itemType->name;
				            echo "</td>";

				            $bookingDateModel = $booking->FirstBookingDate();

		                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations) )
		                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
		                    else
		                         $bed_pref = '- - - - -'.' ';

				            echo "<td >";
				            echo $bed_pref;
				            echo "</td>";

				            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

				            echo "<td>";
				            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->id;
				            echo "</td>";


	                		$date1=date_create($booking->departure_date);
		                    $date2=date_create($booking->arrival_date);
		                    $diff=date_diff($date1,$date2);
		                    $difference1 =  $diff->format("%a");

	                		
			            	echo "<td>";
				            echo $difference1;
				            echo "</td>";
			            	
			            	echo "<td>";
				            echo date('d.m.Y', strtotime($booking->arrival_date));
				            echo "</td>";

				            echo "<td>";
				            echo date('d.m.Y', strtotime($booking->departure_date));
				            echo "</td>";
			            	
			            	echo "<td>";
				            // echo ;
				            echo "</td>";

				            echo "</tr>";
	                	}
	                }


	                
	            }
	            else
	            {
	                
	                $bookings = BookingsItems::find()->where(['or',['arrival_date'=> $date],['departure_date'=>$date]])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->all();

	                $arrivals = BookingsItems::find()->where(['arrival_date' => $date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->count();
	                $departures = BookingsItems::find()->where(['departure_date' => $date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->count();

	                echo "<tr>";
		            echo "<td style='border-right: 0px;'>";
		            echo $arrivals.' Arrivals'.' /'.$departures.' Departures';
		            echo "</td>";
		            
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-left:0px;'>";
		            echo "</td>";
		            echo "</tr>";
	                if(!empty($bookings))
	                {
	                	foreach ($bookings as $booking) 
	                	{
	                		echo "<tr>";

	                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
		                    if(!empty($booking->item_name_id))
		                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
		                    else
		                         $unit_name = '- - - - -'.' ';

				           
			            	echo "<td>";
				            echo $unit_name;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->item->itemType->name;
				            echo "</td>";

				            $bookingDateModel = $booking->FirstBookingDate();

		                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations))
		                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
		                    else
		                         $bed_pref = '- - - - -'.' ';

				            echo "<td >";
				            echo $bed_pref;
				            echo "</td>";

				            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

				            echo "<td>";
				            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->id;
				            echo "</td>";


	                		$date1=date_create($booking->departure_date);
		                    $date2=date_create($booking->arrival_date);
		                    $diff=date_diff($date1,$date2);
		                    $difference1 =  $diff->format("%a");

	                		
			            	echo "<td>";
				            echo $difference1;
				            echo "</td>";
			            	
			            	echo "<td>";
				            echo date('d.m.Y', strtotime($booking->arrival_date));
				            echo "</td>";

				            echo "<td>";
				            echo date('d.m.Y', strtotime($booking->departure_date));
				            echo "</td>";
			            	
			            	echo "<td>";
				            // echo ;
				            echo "</td>";

				            echo "</tr>";
	                	}
	                }


	                $bookings = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->all();

	                $stay_through_count = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->count();

	                echo "<tr>";
		            echo "<td style='border-right: 0px;'>";
		            echo $stay_through_count.' Staying through';
		            echo "</td>";
		            
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-left:0px;'>";
		            echo "</td>";
		            echo "</tr>";
	                if(!empty($bookings))
	                {
	                	foreach ($bookings as $booking) 
	                	{
	                		echo "<tr>";

	                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
		                    if(!empty($booking->item_name_id))
		                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
		                    else
		                         $unit_name = '- - - - -'.' ';

				           
			            	echo "<td>";
				            echo $unit_name;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->item->itemType->name;
				            echo "</td>";

				            $bookingDateModel = $booking->FirstBookingDate();

		                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations))
		                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
		                    else
		                         $bed_pref = '- - - - -'.' ';

				            echo "<td >";
				            echo $bed_pref;
				            echo "</td>";

				            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

				            echo "<td>";
				            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
				            echo "</td>";

				            echo "<td>";
				            echo $booking->id;
				            echo "</td>";


	                		$date1=date_create($booking->departure_date);
		                    $date2=date_create($booking->arrival_date);
		                    $diff=date_diff($date1,$date2);
		                    $difference1 =  $diff->format("%a");

	                		
			            	echo "<td>";
				            echo $difference1;
				            echo "</td>";
			            	
			            	echo "<td>";
				            echo date('d.m.Y', strtotime($booking->arrival_date));
				            echo "</td>";

				            echo "<td>";
				            echo date('d.m.Y', strtotime($booking->departure_date));
				            echo "</td>";
			            	
			            	echo "<td>";
				            // echo ;
				            echo "</td>";

				            echo "</tr>";
	                	}
	                }
	            }
	         
	            $date = strtotime("+1 day", strtotime($date));
	            $date = date("Y-m-d", $date);
	        }
    	?>
    </tbody>
</table>


<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
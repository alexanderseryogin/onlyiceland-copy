<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\StateRecord */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portlet light ">
<div class="state-record-form">

    <?php $form = ActiveForm::begin([
        'errorSummaryCssClass' => 'alert alert-danger',
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php
        if(!empty($model->image))
        {
            echo '<img src='.Yii::getAlias('@web').'/../uploads/states/'.$model->state_id.'/images/'.$model->image.' height="120" width="150">';
            echo '<br>'; 
        }
    ?>

    <?= $form->field($model, 'image_file')->fileInput() ?>
    <span class="label label-danger"> NOTE! </span>
    <span>&nbsp; Recommended Dimensions (1200x450) </span>
    <div style="margin-top: 20px;">
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/state-record']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

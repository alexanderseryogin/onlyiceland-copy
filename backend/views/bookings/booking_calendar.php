<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Destinations;
use common\models\BookableItems;

$this->title = "Booking Calendar";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];

$session = Yii::$app->session;
$session->open();
$params = '';

if(isset($session[Yii::$app->controller->id.'-booking-calendar-values']) && !empty($session[Yii::$app->controller->id.'-booking-calendar-values']))
{
    $params = json_decode($session[Yii::$app->controller->id.'-booking-calendar-values'], true);
    unset($session[Yii::$app->controller->id.'-booking-calendar-values']);
    //print_r($params);
}
// echo "<pre>";
// print_r('destination_id :'.$destination_id);
// print_r('bookable_item_id :'.$bookable_item_id);
// exit();
?>

<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	#calendar {
		
		margin: 0 auto;
	}

</style>

<div class="row calendar-div">
    <div class="col-md-12">
        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md" style="width: 100%;">
                    <div class="row">
                		<div class="col-sm-4">
	                        <label class="control-label">Destinations</label>
	                        <div class="form-group" >
	                            <select id="provider_dropdown" style="width: 100%;" class="form-control">
	                            	<option value=""></option>
	                                <?php
	                                    $destinations = ArrayHelper::map(Destinations::find()->where(['active' => 1])->all(),'id','name');

                                        if($destination_id != null )
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if($destination_id == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }
                                        }
                                        else
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if(!empty($params) && $params['provider_id'] == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }    
                                        }
	                                    
	                                ?>
	                            </select>
	                        </div>
	                    </div>
                        <div class="col-sm-4">
                            <label class="control-label">Bookable Items</label>
                            <div class="form-group" >
                                <select id="item_dropdown" style="width: 100%;" class="form-control">
                                    <option value=""></option>
                                    <?php
                                        if(!empty($bookable_item_id))
                                        {
                                            $items = ArrayHelper::map(BookableItems::find()->where(['provider_id' => $destination_id])->all(),'id','itemType.name');

                                            echo "<option value='show_all'>Show All</option>";

                                            foreach ($items as $key => $value) 
                                            {
                                                if($bookable_item_id == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }
                                        }
                                        else
                                        {
                                            if(!empty($params) && $params['item_id'] != 'show_all')
                                            {
                                                $items = ArrayHelper::map(BookableItems::find()->where(['provider_id' => $params['provider_id']])->all(),'id','itemType.name');

                                                echo "<option value='show_all'>Show All</option>";

                                                foreach ($items as $key => $value) 
                                                {
                                                    if($params['item_id'] == $key)
                                                       echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                    else
                                                       echo '<option value="'.$key.'">'.$value.'</option>'; 
                                                }
                                            }
                                        }
                                            
                                    ?>
                                </select>
                            </div>
                        </div>
                	</div>
                </div>
                <ul class="nav nav-tabs bookings_tabs">
                    <li class="active">
                        <!-- <a href="#general_info" data-toggle="tab">General info</a> -->
                    </li>
                </ul>
            </div>
            
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <center><div class="loader"></div></center><br>
                        	<div id="error_div" class="alert-danger alert fade in" style="display: none">
								No item found.
							</div>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
                <br>        
            </div>
        </div>
    </div>
</div>

<?php



if($destination_id != null)
{
    if($bookable_item_id != null)
    {
        $this->registerJs("$('#item_dropdown').trigger('change');");
    }
    else
    {
        $this->registerJs("$('#provider_dropdown').trigger('change');");
    }
}
else
{
    if(!empty($params) && isset($params['provider_id']) && $params['item_id'] == 'show_all')
    {
        $this->registerJs("$('#provider_dropdown').trigger('change');");
    }
    else if(!empty($params))
    {
        $this->registerJs("$('#item_dropdown').trigger('change');");
    }
}

$get_destination_booked_items = Url::to(['/bookings/get-destination-booked-items']);
$edit_or_drag_booked_item = Url::to(['/bookings/edit-or-drag-booked-item']);
$create_booking_page = Url::to(['/bookings/create-booking-page-redirect']);
$add_event_exception = Url::to(['/bookings/add-event-exception']);
$remove_event_exception = Url::to(['/bookings/remove-event-exception']);

$this->registerJs("
jQuery(document).ready(function() 
{
	$('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true,
    });

    $('#item_dropdown').select2({
        placeholder: \"Select a Bookable Item\",
    });

    $('#error_div').hide();
    $('#calendar').hide();

    $(document).on('change','#provider_dropdown',function()
    {
    	App.blockUI({target:\".loader\", animate: !0});

        var object = {
                    	'destination_id' : $(this).val(),
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$get_destination_booked_items',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                   	$('#error_div').css('display','block');
                   	$('#calendar').hide();
                    $('#item_dropdown').empty().html('');
                    App.unblockUI(\".loader\");
                }
                else
                {

                   	$('#error_div').css('display','none');
                   	$('#calendar').show();
                    $('#item_dropdown').empty().html(data.bookable_items);
                    App.unblockUI(\".loader\");
                   	console.log(jQuery.parseJSON(data.bookings_items));
                   	initFullCalendar(jQuery.parseJSON(data.bookings_items),data.date,data.closed_dates);
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    $(document).on('change','#item_dropdown',function()
    {
        App.blockUI({target:\".loader\", animate: !0});

        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'item_id' : $(this).val(),
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$get_destination_booked_items',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                    $('#error_div').css('display','block');
                    $('#calendar').hide();
                    App.unblockUI(\".loader\");
                }
                else
                {
                    $('#error_div').css('display','none');
                    $('#calendar').show();
                    App.unblockUI(\".loader\");
                    console.log(jQuery.parseJSON(data.bookings_items));
                    initFullCalendar(jQuery.parseJSON(data.bookings_items),data.date,data.closed_dates);
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    function initFullCalendar(bookings_items,date,closed_dates)
    {
        for (var i = 0; i < closed_dates.length; i++) 
        {
            var d = new Date(closed_dates[i]);
            //alert(new_date);   
            var curr_date = d.getDate();

            var curr_month = d.getMonth();

            var curr_year = d.getFullYear();

            //document.write(curr_date +'-' + curr_month + '-' + curr_year);
        }
        var before_start = '';
        var before_end = '';

        var after_start = '';
        var after_end = '';

    	$('#calendar').fullCalendar( 'destroy' );
    	$('#calendar').fullCalendar({
            schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listWeek'
			},
			defaultDate: date,
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow \"more\" link when too many events
			events: bookings_items,
			disableDragging: true,
            eventResizeStart: function (event, jsEvent, ui, view) 
            {
                before_start = event.start.format();
                before_end = event.end.format();
                //console.log('RESIZE Before!! ' + before_start + ' to '+ before_end);
            },
            eventResize: function(event, delta, revertFunc) 
            {
                console.log('RESIZE After!! ' + event.start.format() + ' to '+ event.end.format());
        
                editOrDragBookingItem(event,revertFunc);
            },
            eventDrop: function(event, delta, revertFunc) 
            {
                editOrDragBookingItem(event,revertFunc);
            },
            dayClick: function(date, jsEvent, view) 
            {   
                var acn = jsEvent.target;
                console.log(acn.getAttribute('class'));
                if( acn.getAttribute('class') == 'add_event_label fa fa-times' || acn.getAttribute('class') == 'remove_event_label fa fa-check') 
                {
                    console.log(this);
                    console.log(jsEvent.target);
                    return;
                }
                var check_flag = 0;
                var d = new Date(date);
                
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();

                for (var i = 0; i < closed_dates.length; i++) 
                {
                    var new_c_date = new Date(closed_dates[i]);
                    var new_date = new_c_date.getDate();
                    var new_month = new_c_date.getMonth();
                    var new_year = new_c_date.getFullYear();

                    if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                    {
                        check_flag = 1;
                    }
                }

                if(view.name == 'month' && check_flag == 1) {
                    alert('you cannot book on this day');
                }
                else if(view.name == 'month' && check_flag == 0)
                {
                    createBookingPage(date);
                }
                else
                {

                }

                // var start = new Date('2017-07-02');
                // var end = new Date('2017-07-05');

                // if(view.name == 'month' && date >= start && date <= end) {
                //     alert('you cannot book on this day');
                // }
                // else if(view.name == 'month')
                // {
                //     createBookingPage(date);
                // }
                // else
                // {

                // }
            },
            dayRender: function (date, cell) {

                var d = new Date(date);
                
                //alert(date);
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();
               // console.log(curr_date +'-' + curr_month + '-' + curr_year);
                for (var i = 0; i < closed_dates.length; i++) 
                {
                    var new_c_date = new Date(closed_dates[i]);
                    var new_date = new_c_date.getDate();
                    var new_month = new_c_date.getMonth();
                    var new_year = new_c_date.getFullYear();

                    if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                    {
                        console.log('exception_dates');
                        cell.css('background-color','#DB7070');
                        
                        
                        cell.prepend(`<span style='font-size:15px;margin-left:20px;'><b>Closed for new bookings</b></span>`);
                        // cell.prepend(`<a class='add_event_label' style='position:relative;top:0 ;left:0;display: block;font-size:12px;color:#000;cursor:pointer;'>(+)Add Event</a>`);                    
                    }
                    // else
                    // {
                    //     cell.prepend(`<a class='remove_event_label' style='position:relative;top:0;left:0;display: block;font-size:12px;color:#000;cursor:pointer;'>(-)Remove Event</a>`);
                    // }
                }
                
            }, 
            navLinkDayClick: function(date, jsEvent) 
            {
                var check_flag = 0;
                var d = new Date(date);
                
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();

                for (var i = 0; i < closed_dates.length; i++) 
                {
                    var new_c_date = new Date(closed_dates[i]);
                    var new_date = new_c_date.getDate();
                    var new_month = new_c_date.getMonth();
                    var new_year = new_c_date.getFullYear();

                    if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                    {
                        check_flag = 1;
                    }
                }

                if(check_flag == 1) {
                    alert('you cannot book on this day');
                }
                else
                {
                    createBookingPage(date);
                }
            },
            eventAfterAllRender: function(view){
                console.log(closed_dates);
                if(view.name == 'month')
                {                       
                    $('.fc-day-top').each(function(){
                        var check = 1;
                        $(this).css('position','relative');
                        var curr_date = $(this).data('date');
                        //console.log($(this).data('date'));

                        for (var i = 0; i < closed_dates.length; i++) 
                        {
                            if(curr_date == closed_dates[i]) 
                            {
                                $(this).append('<a id='+curr_date+' class=\"remove_event_label fa fa-check\" style=\"position:absolute;top:0;left:0;display: block;font-size:12px;color:#000;cursor:pointer;\"></a>');
                                check = 0;
                                break;
                            }
                        }

                        if(check != 0)
                        {
                            $(this).append('<a id='+curr_date+' class=\"add_event_label fa fa-times\" style=\"position:absolute;top:0;left:0;display: block;font-size:12px;color:#000;cursor:pointer;\"></a>');
                        }
                       
                    });      
                }                   
            }
		});
    }

    function createBookingPage(date)
    {
        var object = 
            {
                'provider_id': $('#provider_dropdown').val(),
                'arrival_date' : date.format(),
            };

        $.ajax(
        {
            type: 'POST',
            data: object,
            url: '$create_booking_page',
            success: function(data)
            {
                alert('success');
            },
            error: function()
            {
            }
        });
    }

    function editOrDragBookingItem(event,revertFunc)
    {
        // if(confirm('Are you sure about this change?')) 
        // {
            var object = 
                {
                    'id': event.id,
                    'arrival_date' : event.start.format(),
                    'departure_date' : event.end.format(),
                };

            $.ajax(
            {
                type: 'POST',
                data: object,
                url: '$edit_or_drag_booked_item',
                success: function(data)
                {
                    if(data=='false')
                    {
                        //alert('This rate is not available '+event.start.format() +' - '+event.end.format());
                        alert('The rate is not available');
                        revertFunc();
                    }
                    if(data == 'date_exception')
                    {
                        alert('you cannot book on this day');
                        revertFunc();
                    }
                },
                error: function()
                {
                    alert('error');
                }
            });
      //  }
        // else
        // {
        //     revertFunc();
        // }
    }

    $(document).on('click','.remove_event_label',function(){
        event.preventDefault();
        //alert('remove clicked');
        var date = $(this).attr('id');
        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'item_id' : $('#item_dropdown').val(),
                        'date' : $(this).attr('id')
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$remove_event_exception',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                    $('#error_div').css('display','block');
                    $('#calendar').hide();
                    $('#item_dropdown').empty().html('');
                    App.unblockUI(\".loader\");

                }
                else
                {

                    $('#error_div').css('display','none');
                    $('#calendar').show();
                    //$('#item_dropdown').empty().html(data.bookable_items);
                    App.unblockUI(\".loader\");
                    
                    initFullCalendar(jQuery.parseJSON(data.bookings_items),data.date,data.closed_dates);
                    $('#calendar').fullCalendar('gotoDate',date);
                }
            },
            error: function()
            {
                alert('error');
            }
        });


    });
    $(document).on('click','.add_event_label',function(event){
        event.preventDefault();
        //alert('add clicked');
        var date = $(this).attr('id');
        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'item_id' : $('#item_dropdown').val(),
                        'date' : $(this).attr('id')
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$add_event_exception',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                    $('#error_div').css('display','block');
                    $('#calendar').hide();
                    $('#item_dropdown').empty().html('');
                    App.unblockUI(\".loader\");
                }
                else
                {

                    $('#error_div').css('display','none');
                    $('#calendar').show();
                    //$('#item_dropdown').empty().html(data.bookable_items);
                    App.unblockUI(\".loader\");
                    if(data.check_booking == 1)
                    {
                        alert('there is a booking on this date');
                    }
                    else
                    {
                        var CurrentDate = new Date();
                        var CurrentYear = CurrentDate.getFullYear();
                        initFullCalendar(jQuery.parseJSON(data.bookings_items),data.date,data.closed_dates);
                        var str = date.split('-');
                        //console.log(parseInt(str[1])-1);
                        var month = str[1]-1;
                        console.log('month :' +month);
                        console.log('year :' +CurrentYear);
                        $('#calendar').fullCalendar('gotoDate',date);
                    }
                    
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    });

});",View::POS_END);

$this->registerCss("
        .background1 {
            background-color: red;
            width: 180px;
            height: 170px;
            padding: 0; 
            margin: 0
        }
        .line1 {
            width: 112px;
            height: 47px;
            border-bottom: 1px solid black;
            -webkit-transform:
                translateY(-20px)
                translateX(5px)
                rotate(45deg); 
            position: absolute;
            /* top: -20px; */
        }
        .line2 {
            width: 112px;
            height: 47px;
            border-bottom: 1px solid black;
            -webkit-transform:
                translateY(20px)
                translateX(5px)
                rotate(-26deg);
            position: absolute;
            // top: -33px;
            // left: -13px;
        }
        .fc-day-number{
            color: black;
        }
        .fc-day:after {
            background-color: red;
            color: white;
            // width: 20%;
        }

    ");
?>


<?php

use yii\db\Migration;

class m161230_063226_create_rates_amenities extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `rates_amenities`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `rates_id` INT(11), `amenities_id` INT(11) UNSIGNED, `type` TINYINT(4), PRIMARY KEY (`id`), CONSTRAINT `FK_rates_amenities_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_rates_amenities_amenities_id` FOREIGN KEY (`amenities_id`) REFERENCES `amenities`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
            ");
    }

    public function down()
    {
        echo "m161230_063226_create_rates_amenities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

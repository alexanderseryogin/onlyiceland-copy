<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_group".
 *
 * @property string $id
 * @property string $group_name
 * @property integer $group_user_id
 * @property string $group_guest_user_id
 * @property integer $confirmation_id
 * @property integer $cancellation_id
 * @property integer $flag_id
 * @property integer $status_id
 * @property integer $travel_partner_id
 * @property integer $offers_id
 * @property string $estimated_arrival_time_id
 *
 * @property BookingConfirmationTypes $confirmation
 * @property EstimatedArrivalTimes $estimatedArrivalTime
 * @property BookingTypes $flag
 * @property Offers $offers
 * @property BookingStatuses $status
 * @property TravelPartner $travelPartner
 * @property BookingGuests $groupGuestUser
 * @property User $groupUser
 * @property BookingItemGroup[] $bookingItemGroups
 */
class BookingGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_user_id', 'group_guest_user_id', 'confirmation_id', 'flag_id', 'status_id', 'travel_partner_id', 'offers_id', 'estimated_arrival_time_id'], 'integer'],
            [['group_name','cancellation_id'], 'string', 'max' => 256],
            [['confirmation_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['confirmation_id' => 'id']],
            [['estimated_arrival_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => EstimatedArrivalTimes::className(), 'targetAttribute' => ['estimated_arrival_time_id' => 'id']],
            [['flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['flag_id' => 'id']],
            [['offers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offers::className(), 'targetAttribute' => ['offers_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['travel_partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTravelPartners::className(), 'targetAttribute' => ['travel_partner_id' => 'id']],
            [['group_guest_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGuests::className(), 'targetAttribute' => ['group_guest_user_id' => 'id']],
            [['group_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['group_user_id' => 'id']],
            [['balance','comments','voucher_no','reference_no','old_group_no'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_name' => Yii::t('app', 'Group Name'),
            'group_user_id' => Yii::t('app', 'Group User ID'),
            'group_guest_user_id' => Yii::t('app', 'Group Guest User ID'),
            'confirmation_id' => Yii::t('app', 'Confirmation ID'),
            'cancellation_id' => Yii::t('app', 'Cancellation ID'),
            'flag_id' => Yii::t('app', 'Flag ID'),
            'status_id' => Yii::t('app', 'Status ID'),
            'travel_partner_id' => Yii::t('app', 'Travel Partner ID'),
            'offers_id' => Yii::t('app', 'Offers ID'),
            'estimated_arrival_time_id' => Yii::t('app', 'Estimated Arrival Time ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmation()
    {
        return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'confirmation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimatedArrivalTime()
    {
        return $this->hasOne(EstimatedArrivalTimes::className(), ['id' => 'estimated_arrival_time_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlag()
    {
        return $this->hasOne(BookingTypes::className(), ['id' => 'flag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasOne(Offers::className(), ['id' => 'offers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(BookingStatuses::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravelPartner()
    {
        return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupGuestUser()
    {
        return $this->hasOne(BookingGuests::className(), ['id' => 'group_guest_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupUser()
    {
        return $this->hasOne(User::className(), ['id' => 'group_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItemGroups()
    {
        return $this->hasMany(BookingItemGroup::className(), ['booking_group_id' => 'id']);
    }

    public function getBookingGroupSpecialRequests() 
    { 
        return $this->hasMany(BookingGroupSpecialRequests::className(), ['booking_group_id' => 'id']); 
    }

    public function getBookingGroupCharges()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_group_id' => 'id'])
                    ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT])
                    ->orderBy(['date' => SORT_ASC,'entry_type'=> SORT_ASC]);
    }

    public function getBookingGroupPayments()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_group_id' => 'id'])
                    ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT])
                    ->orderBy(['date'=> SORT_ASC]);
    } 

    public function getGroupPayments()
    {
        return $this->hasMany(BookingGroupFinance::className(), ['booking_group_id' => 'id']);
    }
}

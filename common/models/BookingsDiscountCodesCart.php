<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookings_discount_codes_cart".
 *
 * @property string $id
 * @property string $booking_date_id
 * @property integer $discount_id
 * @property integer $price
 * @property integer $extra_bed_quantity
 *
 * @property BookingDatesCart $bookingDate
 * @property DiscountCode $discount
 */
class BookingsDiscountCodesCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings_discount_codes_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_date_id', 'discount_id', 'price', 'extra_bed_quantity'], 'integer'],
            [['booking_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingDatesCart::className(), 'targetAttribute' => ['booking_date_id' => 'id']],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountCode::className(), 'targetAttribute' => ['discount_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_date_id' => 'Booking Date ID',
            'discount_id' => 'Discount ID',
            'price' => 'Price',
            'extra_bed_quantity' => 'Extra Bed Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingDate()
    {
        return $this->hasOne(BookingDatesCart::className(), ['id' => 'booking_date_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(DiscountCode::className(), ['id' => 'discount_id']);
    }
}

<?php

use yii\db\Migration;

class m170815_131015_alter_table_rates_upsell_add_field_show_on_frontend extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `rates_upsell`   
                          ADD COLUMN `show_on_frontend` INT(11) NULL AFTER `fee`;
                        ');
    }

    public function down()
    {
        echo "m170815_131015_alter_table_rates_upsell_add_field_show_on_frontend cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m180206_073127_alter_table_change_name_booking_override_status_details extends Migration
{
    public function safeUp()
    {
        $this->execute('RENAME TABLE `booking_override_stattus_details` TO `booking_override_status_details`;
');
    }

    public function safeDown()
    {
        echo "m180206_073127_alter_table_change_name_booking_override_status_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180206_073127_alter_table_change_name_booking_override_status_details cannot be reverted.\n";

        return false;
    }
    */
}

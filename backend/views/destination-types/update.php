<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProviderTypes */

/*$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Provider Types',
]) . $model->name;*/
$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Destination Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="provider-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

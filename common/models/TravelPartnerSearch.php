<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TravelPartner;

/**
 * TravelPartnerSearch represents the model behind the search form about `common\models\TravelPartner`.
 */
class TravelPartnerSearch extends TravelPartner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'state_id', 'city_id'], 'integer'],
            [['company_name', 'phone', 'emergency_number', 'booking_email', 'invoice_email', 'commission', 'kennitala', 'address', 'second_address', 'postal_code','primary_contact','billing_contact'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TravelPartner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['company_name'=>SORT_ASC]],
            // 'pagination' => [
            //     'page' => isset($params['page'])?($params['page']-1):0,
            // ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'state_id' => $this->state_id,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'primary_contact', $this->primary_contact])
            ->andFilterWhere(['like', 'billing_contact', $this->billing_contact])
            ->andFilterWhere(['like', 'emergency_number', $this->emergency_number])
            ->andFilterWhere(['like', 'booking_email', $this->booking_email])
            ->andFilterWhere(['like', 'invoice_email', $this->invoice_email])
            ->andFilterWhere(['like', 'commission', $this->commission])
            ->andFilterWhere(['like', 'kennitala', $this->kennitala])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'second_address', $this->second_address])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code]);

        return $dataProvider;
    }
}

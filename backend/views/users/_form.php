<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use backend\components\Helpers;
use common\models\UserProfile;
use yii\helpers\ArrayHelper;
Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
use yii\web\View;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $user_model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
\metronic\assets\BackendUserProfileAsset::register($this);
?>
<style>
    .hint-block{
        color:blue;
    }
</style>
<div class="portlet light ">
<div class="users-form">

    <?php $form = ActiveForm::begin([
        'errorSummaryCssClass' => 'alert alert-danger',
        'id' => 'user_form'
    ]); ?>

    <input type="text" name="Users[server_phone]" value="" id="server_phone" hidden="">

    <?= $form->errorSummary($user_model); ?>
    <div class="row">
	    <div class="col-xs-4">
	    <?= $form->field($user_model, 'username')->textInput(['maxlength' => true]) ?>
		</div>
	    <div class="col-xs-4">
	    <?= $form->field($profile_model, 'company_name')->textInput(['maxlength' => true]) ?>
		</div>
	    <div class="col-xs-4">
	    <?= $form->field($user_model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
    <?php 
        //echo $form->field($user_model, 'password')->passwordInput(['maxlength' => true]) 
    ?>
    <div class="row">
        <div class="col-xs-2">
            <?php 
                // if model=update and type=admin
                // dont allow an admin to deactive him/her self
                if(!$user_model->isNewRecord && $user_model->type == User::USER_TYPE_ADMIN){
                    echo $form->field($user_model, 'status')->dropdownList([
                        User::STATUS_ACTIVE => Helpers::statusLables(User::STATUS_ACTIVE, true),
                    ]); 
                } else {
                    echo $form->field($user_model, 'status')->dropdownList([
                        User::STATUS_ACTIVE => Helpers::statusLables(User::STATUS_ACTIVE, true), 
                        User::STATUS_DELETED => Helpers::statusLables(User::STATUS_DELETED, true), 
                    ]); 
                }
            
            ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($user_model, 'type')->dropdownList([
                User::USER_TYPE_USER => Helpers::userLables(User::USER_TYPE_USER, true),
                User::USER_TYPE_ADMIN => Helpers::userLables(User::USER_TYPE_ADMIN, true), 
                User::USER_TYPE_PMANAGER => Helpers::userLables(User::USER_TYPE_PMANAGER, true), 
                User::USER_TYPE_STAFF => Helpers::userLables(User::USER_TYPE_STAFF, true),
                User::USER_TYPE_OWNER => Helpers::userLables(User::USER_TYPE_OWNER, true),
            ]) ?>
        </div>

    
    <!-- ///////////////////////////// User Profile ////////////////////////////////// -->

        <div class="col-xs-1">
            <?= $form->field($profile_model, 'title')->dropDownList([
                                        UserProfile::TITLE_EMPTY => Helpers::ProfileTitleTypes(UserProfile::TITLE_EMPTY),
                                        UserProfile::TITLE_MR => Helpers::ProfileTitleTypes(UserProfile::TITLE_MR),
                                        UserProfile::TITLE_MS => Helpers::ProfileTitleTypes(UserProfile::TITLE_MS),
                                        UserProfile::TITLE_MRS => Helpers::ProfileTitleTypes(UserProfile::TITLE_MRS),
                                        UserProfile::TITLE_DR => Helpers::ProfileTitleTypes(UserProfile::TITLE_DR),
                                    ]); ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($profile_model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($profile_model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($profile_model, 'telephone_number', [
                                            'inputOptions' => [
                                                'id' => 'telephone_number',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 800px'
                                                ],
                                                'template' => "{label}<br>{input}"
                                            ])->textInput()?>
            <div id="phone_hint" style="margin-top: -20px; color:red; display: none;">Phone is invalid.</div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-2">
            <?= $form->field($profile_model, 'business_id_number', [
                                        'inputOptions' => [
                                            'id' => 'kennitala-input',
                                            'class' => 'form-control'
                                            ]
                                        ])->textInput(['maxlength' => true])->hint(' e.g. 123456-7890') ?>
        </div>
    </div>
                                   

                                    <?php /*echo $form->field($profile_model, 'city_id')->textInput() ?>

                                    <?php echo $form->field($profile_model, 'state_id')->textInput() ?>

                                    <?php echo $form->field($profile_model, 'country_id')->textInput()*/ ?>
                                    <div class="row">
                                        <div class="col-xs-6">

                                            <?= $form->field($profile_model, 'address',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            ]
                                                        ])->textarea(['rows' => 6]) ?>

                                            <?= $form->field($profile_model, 'postal_code')->textInput(['maxlength' => true]) ?>

                                            <div class="row">

                                                <div class="col-xs-4">
                                                    <?= $form->field($profile_model, 'country_id', [
                                                    'inputOptions' => [
                                                        'id' => 'countries-dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                                    ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                                                        'prompt'=>'Select a Country',
                                                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/liststates?country_id=').'"+$(this).val(), function( data ) {
                                                              $("#states-dropdown").html( data );
                                                            });'
                                                    ]) ?>      
                                                </div>
                                                <div class="col-xs-4">
                                                    <?= $form->field($profile_model, 'state_id', [
                                                    'inputOptions' => [
                                                        'id' => 'states-dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                                    ])->dropdownList(ArrayHelper::map(States::find()->where(['country_id'=>$profile_model->country_id])->all(),'id','name'),[
                                                        'prompt'=>'Select a State',
                                                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/listcities?state_id=').'"+$(this).val(), function( data ) {
                                                              $("#cities-dropdown").html( data );
                                                            });'
                                                    ]) ?>  
                                                </div>
                                                <div class="col-xs-4">
                                                    <?= $form->field($profile_model, 'city_id', [
                                                    'inputOptions' => [
                                                        'id' => 'cities-dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                                    ])->dropdownList(ArrayHelper::map(Cities::find()->where(['state_id'=>$profile_model->state_id])->all(),'id','name'),[
                                                        'prompt'=>'Select a City',
                                                        
                                                    ]) ?> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6" id="provider-business_address">
                                            <div >
                                                <?= $form->field($profile_model, 'business_address',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            ]
                                                        ])->textarea(['rows' => 6]) ?>

                                                <?= $form->field($profile_model, 'business_postal_code')->textInput(['maxlength' => true]) ?>

                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <?= $form->field($profile_model, 'business_country_id', [
                                                        'inputOptions' => [
                                                            'id' => 'business-countries-dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                                        ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                                                            'prompt'=>'Select a Country',
                                                            'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/liststates?country_id=').'"+$(this).val(), function( data ) {
                                                                  $("#business-states-dropdown").html( data );
                                                                });'
                                                        ]) ?>      
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <?= $form->field($profile_model, 'business_state_id', [
                                                        'inputOptions' => [
                                                            'id' => 'business-states-dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                                        ])->dropdownList(ArrayHelper::map(States::find()->where(['country_id'=>$profile_model->business_country_id])->all(),'id','name'),[
                                                            'prompt'=>'Select a State',
                                                            'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/listcities?state_id=').'"+$(this).val(), function( data ) {
                                                                  $("#business-cities-dropdown").html( data );
                                                                });'
                                                        ]) ?>  
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <?= $form->field($profile_model, 'business_city_id', [
                                                        'inputOptions' => [
                                                            'id' => 'business-cities-dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                                        ])->dropdownList(ArrayHelper::map(Cities::find()->where(['state_id'=>$profile_model->business_state_id])->all(),'id','name'),[
                                                            'prompt'=>'Select a City',
                                                            
                                                        ]) ?> 
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?= $form->field($profile_model, 'same_address')->checkbox() ?>
                                        </div>
                                    </div>

                                    


    <!-- ///////////////////////////////////////////////////////////////////// -->

    <div class="form-group">
       <a class="<?= $user_model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?>" id="submit_button" ><?=$user_model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update') ?></a>
        <a class="btn btn-default" href="<?= Url::to(['/users']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {
    
    //$('#telephone_number').intlTelInput();

    $('#telephone_number').intlTelInput({
      initialCountry: 'auto',
      preferredCountries : ['is'],
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }

    });

    $('#userprofile-same_address').change(function() {
        if(this.checked) {
            $('#provider-business_address').hide();
        } else {
            $('#provider-business_address').show();
        }
    });

    if($('#user-type').val()==1)
    {
        $('.field-kennitala-input').show();
    }
    else
    {
        $('.field-kennitala-input').hide();
    }

    $('#user-type').on('change', function() 
    {
        if($(this).val()==1)
        {
            $('.field-kennitala-input').show();
        }
        else{
            $('.field-kennitala-input').hide();
        }   
    });

    $('#telephone_number').change(function()
    {
      var telInput = $('#telephone_number');
      if ($.trim(telInput.val())) 
      {
        if (telInput.intlTelInput('isValidNumber')) 
        {
            $('#phone_hint').hide();
        }
        else 
        {
          $('#phone_hint').show();
        }
      }
    });

    $('#submit_button').click(function()
    {   
        if($('#telephone_number').intlTelInput('isValidNumber')==false)
        {
            $('#phone_hint').show();
        }

        if($('#telephone_number').intlTelInput('isValidNumber'))
        {
            var countryData = $('#telephone_number').intlTelInput('getSelectedCountryData');

            var tel = '+'+countryData['dialCode']+' '+$('#telephone_number').val();
            $('#server_phone').val(tel);

            $('#user_form').submit();
        }
    });

    $('#kennitala-input').inputmask('999999-9999',{showMaskOnFocus: true,showMaskOnHover: false});

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });

    $('#business-cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#business-states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#business-countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });


});",View::POS_END);
?>

<?php

use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationsOpenAllYear;
use common\models\DestinationsOpenHoursExceptions;

$statusCheckbox = [
    0 => '',
    1 => 'checked'
];

$timeDisabled = [
    0 => 'disabled',
    1 => ''
];

$provider_id = '';

if($open_hours['form']=='update')
{
    $provider_id = $open_hours['provider_id'];
}
else
{
    $provider_id = 1;
}

if($open_hours['exceptions']['state']==0)
{
    $this->registerJs("
        $('#hours_24_exception').bootstrapSwitch('disabled',true);

        $('#opening').attr('disabled',true);
        $('#closing').attr('disabled',true);

        $('#opening').val('00:00');
        $('#closing').val('00:00');

        $('.add-dynamic-field-exception').hide();
    ");
}

if($open_hours['exceptions']['hours_24']===1)
{
    $this->registerJs("
        $('#exception_switch').bootstrapSwitch('disabled',true);

        $('#opening').attr('disabled',true);
        $('#closing').attr('disabled',true);

        $('#opening').val('00:00');
        $('#closing').val('00:00');

        $('.add-dynamic-field-exception').hide();
    ");
}

?>

<script type="text/javascript">
    var id = '';
</script>

<div class="tab-pane" id="open_dates">

    <!-- BEGIN ACCORDION PORTLET-->

    <div class="panel-group accordion" id="accordion_exception">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_exception" href="#collapse_years"> Months Open</a>
                </h4>
            </div>
            <div id="collapse_years" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">

                    <?= $form->field($destination_open_all_year, 'selected_months')->checkboxList(DestinationsOpenAllYear::$months,[
                        'itemOptions' => ['style' => 'margin-left:10px;']
                    ]) ?>

                    <div style="display:inline-block;">&nbsp; <input type="button" class="btn btn-info" value="Check All" id="open_all_year_check_all"> </div>
                    <div style="display:inline-block">&nbsp; <input type="button" class="btn btn-danger" value="Clear All" id="open_all_year_clear_all">  </div>

                </div>
            </div>
        </div>
        <br>
        <?= $this->render('open_hours',
                                    [
                                        'form' => $form,
                                        'provider_model' => $provider_model,
                                        'open_hours' => $open_hours,
                                    ]) ?>
        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_exception" href="#collapse_exception">Date Exceptions 
                    </a>
                </h4>
            </div>
            <div id="collapse_exception" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info">
                                <strong>Info!</strong>
                                Leave Closing/End Time at 00:00 to list Opening/Start Time only.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Date</div><div class="col-sm-3">Opening/Start Time</div><div class="col-sm-3">Closing/End Time</div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2 pull-right">
                            <div class="col-sm-6">Open</div>
                            <div class="col-sm-6">Open All Day</div>
                        </div>
                    </div>
                    <br>

                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group" >
                                <div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" value="<?=$open_hours['exceptions']['date_from']?>" name="DestinationsOpenHoursExceptions[exceptions][date_from]" id="date_from">

                                    <span class="input-group-addon"> to </span>

                                    <input type="text" class="form-control" value="<?=$open_hours['exceptions']['date_to']?>" name="DestinationsOpenHoursExceptions[exceptions][date_to]" id="date_to">
                                </div>
                            </div>
                        </div>   
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="fa fa-clock-o"></i>
                                    <input type="text" value="<?=$open_hours['exceptions']['opening'][0]?>" name="DestinationsOpenHoursExceptions[exceptions][opening][]" <?= $timeDisabled[$open_hours['exceptions']['state']] ?> class="form-control" id="opening"> 
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="fa fa-clock-o"></i>
                                    <input type="text" value="<?=$open_hours['exceptions']['closing'][0]?>" name="DestinationsOpenHoursExceptions[exceptions][closing][]" <?= $timeDisabled[$open_hours['exceptions']['state']] ?> class="form-control" id="closing"> 
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-1 add-dynamic-field-exception" style="margin-top:7px;" >
                            <a href="javascript:void(0)" ><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                        </div>
                        
                        <div class="col-sm-2 pull-right" style="margin-top:5px;">
                            
                            <div class="col-sm-6">
                                <input type="checkbox" id="exception_switch" class="make-switch" name="DestinationsOpenHoursExceptions[exceptions][state]" <?= $statusCheckbox[$open_hours['exceptions']['state']] ?>  data-on-color="success" data-off-color="warning" data-on-text="Yes" data-off-text="No" data-size="mini" >
                            </div>
                            <div class="col-sm-6">
                                <input type="checkbox" id="hours_24_exception" class="make-switch" name="DestinationsOpenHoursExceptions[exceptions][hours_24]" <?= $statusCheckbox[$open_hours['exceptions']['hours_24']] ?> data-on-color="success" data-off-color="warning" data-on-text="Yes" data-off-text="No" data-size="mini">
                            </div>
                        </div>

                    </div>

                    <div class="row" id="exception_split_div">
                         <?php
                                $exceptions = $open_hours['exceptions'];
                                for ($i=1; $i < count($exceptions['opening']) ; $i++) 
                                {
                            ?>      
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                        <input type="text" value="<?= $exceptions['opening'][$i] ?>" name="DestinationsOpenHoursExceptions[exceptions][opening][]" class="form-control exception-server-clock">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                        <input type="text"  value="<?= $exceptions['closing'][$i] ?>" name="DestinationsOpenHoursExceptions[exceptions][closing][]" class="form-control exception-server-clock" > 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 delete-dynamic-field-exception" style="margin-top:7px;">
                                                <a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                }
                            ?>
                    </div>
                    
                    <br><hr>

                    <?php
                        if(isset($dataProviderException) && !empty($dataProviderException))
                        {?>
                            <?= $this->render('exception_grid_view',
                                [
                                    'dataProviderException' => $dataProviderException,
                                    'provider_model' => $provider_model,
                                ])?>   
                        <?php 
                        }
                    ?>
                    
                </div>
            </div>
        </div>
        
    </div>
</div>

<?= $this->render('exception_update_modal')?> 

<?php
$this->registerJs("
$(document).ready(function() 
{
    // check all or clear all fields

    $('#open_all_year_check_all').click(function()
    {
        $('input[name=\"DestinationsOpenAllYear[selected_months][]\"]').each(function()
        {
            $(this).prop('checked', true);
        }); 
    });

    $('#open_all_year_clear_all').click(function()
    {
        $('input[name=\"DestinationsOpenAllYear[selected_months][]\"]').each(function()
        {
            $(this).prop('checked', false);
        }); 
    });

    var openingKey = 'opening';
    var closingKey = 'closing';

    $('#'+openingKey).clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });   
     
    $('#'+openingKey).click(function(e){   
        $(this).clockface('toggle');
    });

    $('#'+closingKey).clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });   
 
    $('#'+closingKey).click(function(e){   
        $(this).clockface('toggle');
    });

    openingKey = 'opening_modal';
    closingKey = 'closing_modal';

    $('#'+openingKey).clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });   
     
    $('#'+openingKey).click(function(e){   
        $(this).clockface('toggle');
    });

    $('#'+closingKey).clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });   
 
    $('#'+closingKey).click(function(e){   
        $(this).clockface('toggle');
    });

    $('input[name=\"DestinationsOpenHoursExceptions[exceptions][state]\"]').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            makeFieldEnabled();
            $('#hours_24_exception').bootstrapSwitch('disabled',false);
            $('#exception_split_div').show();
            $('.add-dynamic-field-exception').show();
        }
        else
        {
            makeFieldDisabled();
            $('#opening').val('00:00');
            $('#closing').val('00:00');
            $('#hours_24_exception').bootstrapSwitch('disabled',true);
            $('#exception_split_div').hide();
            $('.add-dynamic-field-exception').hide();
        }
    });

    $('#hours_24_exception').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            makeFieldDisabled();
            $('#opening').val('00:00');
            $('#closing').val('00:00');
            $('#exception_switch').bootstrapSwitch('disabled',true);

            $('#exception_split_div').hide();
            $('.add-dynamic-field-exception').hide();
        }
        else
        {
            makeFieldEnabled();
            $('#exception_switch').bootstrapSwitch('disabled',false);

            $('#exception_split_div').show();
            $('.add-dynamic-field-exception').show();
        }
    });

    function makeFieldDisabled()
    {
        //$('#date_from').attr('readonly',true);
        //$('#date_to').attr('readonly',true);
        $('#opening').attr('disabled',true);
        $('#closing').attr('disabled',true);
    }

    function makeFieldEnabled()
    {
        //$('#date_from').attr('readonly',false);
        //$('#date_to').attr('readonly',false);
        $('#opening').attr('disabled',false);
        $('#closing').attr('disabled',false);

        $('#opening').val('00:00');
        $('#closing').val('00:00');
    }

    $('#exception_switch_modal').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            $('#date_modal').attr('disabled',false);
            $('#opening_modal').attr('disabled',false);
            $('#closing_modal').attr('disabled',false);
        }
        else
        {
            $('#date_modal').attr('disabled',true);
            $('#opening_modal').attr('disabled',true);
            $('#closing_modal').attr('disabled',true);
        }
    });

    $('#add-exception').click(function()
    {
        var object = {
                        date_from:$('#date_from').val(),
                        date_to:$('#date_to').val(),
                        opening:$('#opening').val(),
                        closing:$('#closing').val(),
                        state:$('#exception_switch').bootstrapSwitch('state'),
                        hours_24:$('#hours_24_exception').bootstrapSwitch('state'),
                        provider_id:$provider_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'add-exception-record',
            data: object,
            success: function(result)
            {
                if(result==1)
                {
                    alert('Exception Opening Time must be earlier than Closing Time ');
                }
                else if(result!='')
                {
                    var arr = JSON.parse(result);
                    alert(arr[0]);
                }

                $.pjax.reload({container:'#exception-gridview'});  //Reload GridView
            },
            error: function()
            {
                
            }
        });
    });

    var exception_id='';

    $('a[name=\"exception_delete[]\"]').click(function()
    {
        exception_id = $(this).attr('data-id');
        $('#delete-exception-modal').modal('show');
    });

    $('#yes_exception').click(function()
    {
        $('#delete-exception-modal').modal('hide');

        var Object = {
                        id : exception_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'delete-exception',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#exception-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"exception_update[]\"]').click(function() 
    {
        exception_id = $(this).attr('data-id');
        $('#update-exception-modal').modal('show');

        $(this).parents('tr').find('td').each(function(i) 
        {
            switch (i) 
            {
                case 1:
                    $('#date_modal').val($(this).text());
                    break;
                case 2:
                    if($(this).text()=='N/A')
                        $('#opening_modal').val('');
                    else
                        $('#opening_modal').val($(this).text());
                    break;
                case 3:
                    if($(this).text()=='N/A')
                        $('#closing_modal').val('');
                    else
                        $('#closing_modal').val($(this).text());
                    break;
            }
        });
    });

    $('#update_exception').click(function()
    {
        var object = {
                        opening:$('#opening_modal').val(),
                        closing:$('#closing_modal').val(),
                        //state:$('#exception_switch_modal').bootstrapSwitch('state'),
                        exception_id:exception_id
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'update-exception-record',
            data: object,
            success: function(result)
            {
                if(result=='error')
                {
                    alert('Exception Opening Time must be earlier than Closing Time ');
                }
                else
                {
                    $('#update-exception-modal').modal('hide');
                    $.pjax.reload({container:'#exception-gridview'});  //Reload GridView
                }   
            },
            error: function()
            {
                
            }
        });
    });

    $('#exception-gridview').on('pjax:end', function() 
    {
        $('a[name=\"exception_delete[]\"]').click(function()
        {
            exception_id = $(this).attr('data-id');
            $('#delete-exception-modal').modal('show');
        });

        $('a[name=\"exception_update[]\"]').click(function() 
        {
            exception_id = $(this).attr('data-id');
            $('#update-exception-modal').modal('show');

            $(this).parents('tr').find('td').each(function(i) 
            {
                switch (i) 
                {
                    case 1:
                        $('#date_modal').val($(this).text());
                        break;
                    case 2:
                        if($(this).text()=='N/A')
                            $('#opening_modal').val('');
                        else
                            $('#opening_modal').val($(this).text());
                        break;
                    case 3:
                        if($(this).text()=='N/A')
                            $('#closing_modal').val('');
                        else
                            $('#closing_modal').val($(this).text());
                        break;
                }
            });
        });
    });

    $('.add-dynamic-field-exception').on('click',function()
    {
        var htmlCode = '".DestinationsOpenHoursExceptions::getClockHtml()."';

        $('#exception_split_div').append(htmlCode);

        $('.dynamic-clock').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });   
     
        $('.dynamic-clock').click(function(e){   
            $(this).clockface('toggle');
        });

        $('.dynamic-clock').removeClass('dynamic-clock');
    });

    $('.row').on('click','.delete-dynamic-field-exception',function(event)
    {
        $('.clockface').hide(); 
        $(this).closest('.row').remove();
    });

    // ********** Apply clock on run time created fields row

    $('.exception-server-clock').clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });   
 
    $('.exception-server-clock').click(function(e){   
        $(this).clockface('toggle');
    });

    $('.exception-server-clock').removeClass('.exception-server-clock');

});
");
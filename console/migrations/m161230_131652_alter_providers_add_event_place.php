<?php

use yii\db\Migration;

class m161230_131652_alter_providers_add_event_place extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` ADD COLUMN `event_place` VARCHAR(256) NULL AFTER `name`; ");
    }

    public function down()
    {
        echo "m161230_131652_alter_providers_add_event_place cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170728_081149_alter_table_booking_date_finances_add_field_type extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_date_finances`   
                          ADD COLUMN `type` INT(11) NULL AFTER `booking_group_id`;
                        ');
    }

    public function down()
    {
        echo "m170728_081149_alter_table_booking_date_finances_add_field_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

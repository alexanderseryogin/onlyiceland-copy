<?php
use common\models\CustomReportColumns;
use common\models\BookingDateFinances;
use common\models\BookingsItems;
use common\models\CustomReports;
$this->title = 'Execute Report';
// $this->params['breadcrumbs'][] = ['label' => 'Custom Reports', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Execute';
$columns = CustomReportColumns::getColumns();
$alias = CustomReportColumns::getAlias();

$no_of_bookings_to_exclude = 0;
function bubble_Sort($my_array )
{
    do
    {
        $swapped = false;
        for( $i = 0, $c = count( $my_array ) - 1; $i < $c; $i++ )
        {
            if( $my_array[$i]['date'] > $my_array[$i + 1]['date'] )
            {
                list( $my_array[$i + 1], $my_array[$i] ) =
                        array( $my_array[$i], $my_array[$i + 1] );
                $swapped = true;
            }
        }
    }
    while( $swapped );
return $my_array;
}

// echo "<pre>";
// print_r($result);
// exit();
?>
<style type="text/css">
    table {
         display: block;
         overflow-x: auto;
 /*       white-space: nowrap;*/
       /* table-layout: fixed;*/
    }
</style>
<!-- <form id="pdf-form" action="<?=yii::$app->getUrlManager()->createUrl(['custom-reports/print'])?>" method="POST">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <input type="text" name="query" value="<?=$org_query?>" hidden>
    <input type="text" name="title" value="<?=$post['CustomReports']['report_title']?>" hidden>
    <input type="text" name="sum_average" value="<?=$post['CustomReports']['sum_average']?>" hidden>
    <input type="text" name="columns" value='<?=serialize($post["CustomReports"]["columns"])?>' hidden>
    <input type="text" name="group_by" value='<?=$post["CustomReports"]["group_by"]?>' hidden>
</form> -->

<form id="pdf-form" action="<?=yii::$app->getUrlManager()->createUrl(['custom-reports/print-report'])?>" method="POST">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <input type="text" name="query" value="<?=$org_query?>" hidden>
    <input type="text" name="title" value="<?=$post['CustomReports']['report_title']?>" hidden>
    <input type="text" name="sum_average" value="<?=$post['CustomReports']['sum_average']?>" hidden>
    <input type="text" name="columns" value='<?=serialize($post["CustomReports"]["columns"])?>' hidden>
    <input type="text" name="group_by" value='<?=$post["CustomReports"]["group_by"]?>' hidden>
    <input type="text" name="post" value='<?=serialize($post)?>' hidden>
</form>


<div class="custom-report" id="custom-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$post['CustomReports']['report_title']?></span>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group">
                        <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                            
                        </span></button>

                            <ul id="w3" class="dropdown-menu">
                                <li title="Comma Separated Values">
                                    <a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                <li title="Microsoft Excel 2007+ (xlsx)">
                                    <a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                <li>
                                    <a href="javascript:void(0);" target="_blank" class="print-pdf"><i class="fa fa-file-pdf-o " aria-hidden="true"></i>
                                    PDF</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
                            <!-- <div class="btn-group pull-left" style="margin: 5px">
                                <div class="btn-group">
                                <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                                    
                                </span></button>

                                    <ul id="w3" class="dropdown-menu">
                                        <li title="Comma Separated Values">
                                            <a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                        <li title="Microsoft Excel 2007+ (xlsx)">
                                            <a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                        <li>
                                            <a href="javascript:void(0);" target="_blank" class="print-pdf"><i class="fa fa-file-pdf-o " aria-hidden="true"></i>
                                            PDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- <br><br><br> -->
	                    	<div class="tab-pane active" id="arrival-report">
                                  <h5 class="hide"><b><?=$query?></b></h5>
                                  <br>
                                  <?php
                                    // echo "<pre>";
                                    // print_r($result);
                                    // exit();

                                  ?>
                            <?php 
                                if($post['CustomReports']['group_by'] == 0)
                                {
                            ?>
	                    	    <table class="table table-striped table-bordered table2excel">
                                    <thead>
                                      <tr>
                                        <?php
                                            /*
                                            foreach ($post['CustomReports']['columns'] as $column) 
                                            {
                                                if($column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            */
                                        ?>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {

                                                $notes_exist = false;
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                    $notes_exist = true;
                                                }
                                                    
                                                $sum_row_data = array();
                                                $prev_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;
                                                $prev_sum_debit = 0;
                                                $prev_sum_payment = 0;

                                                $sum_vat = 0;
                                                $prev_sum_vat = 0;
                                                $sum_tax = 0;
                                                $prev_sum_tax = 0;
                                                $sum_discount = 0;
                                                $prev_sum_discount = 0;

                                                $b_item_count = 0;
                                                $properties_count = 0;
                                                $no_of_bookings_per_property = 0;
                                                foreach ($result as $key => $data) 
                                                {
                                                    if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) || in_array(36, $post['CustomReports']['columns']) || in_array(37, $post['CustomReports']['columns']) || in_array(38, $post['CustomReports']['columns']) || in_array(39, $post['CustomReports']['columns']) || in_array(40, $post['CustomReports']['columns']))
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }

                                                    if($b_item_count == 0)
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan =".$colspan."><b>".CustomReportColumns::getDestinationName($data['id'])."</b></td>";
                                                        echo "<tr>";
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column) 
                                                        {
                                                            if($column != 9)
                                                            {
                                                                echo "<td>";
                                                                echo "<b>".$columns[$column]['label']."</b>";
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                        $b_item_count++;
                                                        $properties_count++;
                                                        $no_of_bookings_per_property++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['provider_id'] != $result[$key-1]['provider_id'])
                                                        {
                                                            $properties_count = 1;
                                                            if(CustomReports::hasProperties($post['CustomReports']['show_sub_totals']) )
                                                            {
                                                                echo "<tr>";
                                                                foreach ($post['CustomReports']['columns'] as $column) 
                                                                {
                                                                    if($column != 9)
                                                                    {
                                                                        $value = '';
                                                                        if($column == 16)
                                                                        {
                                                                            if($prev_sum_debit == 0)
                                                                            {
                                                                                $value = $sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_debit - $prev_sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                        }
                                                                        else if($column == 19)
                                                                        {
                                                                           if($prev_sum_payment == 0 )
                                                                            {
                                                                                $value = $sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_payment - $prev_sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                        }
                                                                        else if($column == 37)
                                                                        {
                                                                           if($prev_sum_tax == 0 )
                                                                            {
                                                                                $value = $sum_tax;
                                                                                $prev_sum_tax = $sum_tax;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_tax - $prev_sum_payment;
                                                                                $prev_sum_tax = $sum_tax;
                                                                            }
                                                                        }
                                                                        else if($column == 39)
                                                                        {
                                                                           if($prev_sum_discount == 0 )
                                                                            {
                                                                                $value = $sum_discount;
                                                                                $prev_sum_discount = $sum_discount;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_discount - $prev_sum_discount;
                                                                                $prev_sum_discount = $sum_discount;
                                                                            }
                                                                        }
                                                                        else if($column == 40)
                                                                        {
                                                                           if($prev_sum_vat == 0 )
                                                                            {
                                                                                $value = $sum_vat;
                                                                                $prev_sum_vat = $sum_vat;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_vat - $prev_sum_vat;
                                                                                $prev_sum_vat = $sum_vat;
                                                                            }
                                                                        }
                                                                        else if(array_key_exists($columns[$column]['db_col'], $sum_row_data) )
                                                                        {
                                                                            if(array_key_exists($columns[$column]['db_col'], $prev_row_data) )
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column]['db_col']] - $prev_row_data[$columns[$column]['db_col']];
                                                                                $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column]['db_col']];
                                                                                $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                            }
                                                                        } 
                                                                        else
                                                                        {

                                                                        }
                                                                        // $sum_row_data[$columns[$column]['db_col']]
                                                                        // if($post['CustomReports']['sum_average'] == 2)
                                                                        // {
                                                                        //     $value = round($sum_debit/count($result));
                                                                        // }
                                                                        // else
                                                                        // {
                                                                        //     $value = $sum_debit;
                                                                        // }
                                                                        if($post['CustomReports']['sum_average'] == 2)
                                                                        {
                                                                            $value = round($value/$no_of_bookings_per_property);
                                                                        }
                                                                        $style = $columns[$column]['style'];
                                                                        echo "<td style='".$style."'>";
                                                                        echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                        echo "</td>";
                                                                    }
                                                                }
                                                                echo "</tr>";
                                                                $no_of_bookings_per_property = 1;
                                                            }


                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getDestinationName($data['id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($post['CustomReports']['columns'] as $column) 
                                                            {
                                                                if($column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        else
                                                        {
                                                            $properties_count++;
                                                            $no_of_bookings_per_property++;
                                                        }


                                                    }

                                                    echo "<tr class='".$class."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]); 

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;

                                                    $booking_item_vat_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('vat_amount');

                                                    $booking_item_tax_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('tax');

                                                    $booking_item_discount_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('discount_amount');

                                                    $sum_vat += $booking_item_vat_total;
                                                    $sum_tax += $booking_item_tax_total;
                                                    $sum_discount += $booking_item_discount_total;

                                                    $row_data = array();
                                                    // echo $booking_item_id;
                                                    // exit();
                                                    foreach ($post['CustomReports']['columns'] as $column) 
                                                    {
                                                        if($column != 9)
                                                        {
                                                            $style = $columns[$column]['style'];
                                                            echo "<td style='".$style."'>";
                                                            // echo "<td>";
                                                            if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40)
                                                            {
                                                                if($columns[$column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column]['is_amount'] == 0)
                                                                    {
                                                                        
                                                                        if(isset($columns[$column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                                                $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                                            // echo $virtual_column_val;        
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                                            $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                            if($column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                                                $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                                                $sum_row_data[$columns[$column]['db_col']] = $val;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";

                                                                        $row_data[$columns[$column]['db_col']] = '- - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                                        echo $value;
                                                                        
                                                                        $row_data[$columns[$column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";

                                                        }
                                                        else
                                                        {
                                                            $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                            $row_count = 0;
                                                            $row_count2 = 0;
                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column]['db_col']];
                                                                    if($column == 1)
                                                                    {
                                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                                        if($column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16 ||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31 && $column != 9)
                                                                        {
                                                                            echo $row_data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }



                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            echo "<tr>";
                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1  )
                                                                {
                                                                    if($notes == '')
                                                                    {
                                                                        $border =  'border-bottom: solid 3px';
                                                                    }
                                                                        
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                if($column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                       
                                                        
                                                    }
                                                     

                                                    if($notes_exist && $notes !='')
                                                    {
                                                        $border =  'border-bottom: solid 3px';
                                                        echo "<tr>";
                                                            echo "<td colspan='".$colspan."' style='".$border."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }

                                                // if( $key + 1 == count($result) && $properties_count == 1)
                                                if( $key + 1 == count($result) )
                                                {
                                                    if(CustomReports::hasProperties($post['CustomReports']['show_sub_totals']) )
                                                    {
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column) 
                                                        {
                                                            if($column != 9)
                                                            {
                                                                $value = '';
                                                                if($column == 16)
                                                                {
                                                                    if($prev_sum_debit == 0)
                                                                    {
                                                                        $value = $sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_debit - $prev_sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                }
                                                                else if($column == 19)
                                                                {
                                                                   if($prev_sum_payment == 0 )
                                                                    {
                                                                        $value = $sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_payment - $prev_sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                }
                                                                else if($column == 37)
                                                                {
                                                                   if($prev_sum_tax == 0 )
                                                                    {
                                                                        $value = $sum_tax;
                                                                        $prev_sum_tax = $sum_tax;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_tax - $prev_sum_payment;
                                                                        $prev_sum_tax = $sum_tax;
                                                                    }
                                                                }
                                                                else if($column == 39)
                                                                {
                                                                   if($prev_sum_discount == 0 )
                                                                    {
                                                                        $value = $sum_discount;
                                                                        $prev_sum_discount = $sum_discount;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_discount - $prev_sum_discount;
                                                                        $prev_sum_discount = $sum_discount;
                                                                    }
                                                                }
                                                                else if($column == 40)
                                                                {
                                                                   if($prev_sum_vat == 0 )
                                                                    {
                                                                        $value = $sum_vat;
                                                                        $prev_sum_vat = $sum_vat;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_vat - $prev_sum_vat;
                                                                        $prev_sum_vat = $sum_vat;
                                                                    }
                                                                }
                                                                else if(array_key_exists($columns[$column]['db_col'], $sum_row_data) )
                                                                {
                                                                    if(array_key_exists($columns[$column]['db_col'], $prev_row_data) )
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column]['db_col']] - $prev_row_data[$columns[$column]['db_col']];
                                                                        $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column]['db_col']];
                                                                        $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                    }
                                                                } 
                                                                else
                                                                {

                                                                }
                                                                // $sum_row_data[$columns[$column]['db_col']]
                                                                // if($post['CustomReports']['sum_average'] == 2)
                                                                // {
                                                                //     $value = round($sum_debit/count($result));
                                                                // }
                                                                // else
                                                                // {
                                                                //     $value = $sum_debit;
                                                                // }
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $value = round($value/$no_of_bookings_per_property);
                                                                }

                                                                $style = $columns[$column]['style'];
                                                                echo "<td style='".$style."'>";
                                                                echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                    }
                                                        
                                                }

                                                if((in_array(10, $post['CustomReports']['columns']) || in_array(28, $post['CustomReports']['columns'])|| in_array(29, $post['CustomReports']['columns']) || in_array(30, $post['CustomReports']['columns']) || in_array(31, $post['CustomReports']['columns'])  || in_array(16, $post['CustomReports']['columns']) || in_array(19, $post['CustomReports']['columns'])) && (count($result) > 1 ) )
                                                {
                                                    
                                                    // if(CustomReports::hasReport($post['CustomReports']['show_sub_totals']) )
                                                    // {
                                                        $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column)
                                                        {
                                                            if($column == 16)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 19)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 37)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_tax/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_tax;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 39)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_discount/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_discount;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 40)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_vat/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_vat;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    // }
                                                }

                                                // echo "<pre>";
                                                //     print_r($sum_row_data);
                                                //     exit();
                                            }
                                        ?>
                                    </tbody>
                                </table>

                            <?php
                                }
                                else if($post['CustomReports']['group_by'] == 1) 
                                {
                            ?>
                                <table class="table table-striped table-bordered table2excel">
                                    <thead>
                                      <!-- <tr> -->
                                        <?php
                                            /*
                                            foreach ($post['CustomReports']['columns'] as $column) 
                                            {
                                                if($column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            */
                                        ?>
                                     <!--  </tr> -->
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {

                                                $notes_exist = false;
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                    $notes_exist = true;
                                                }
                                                $sum_row_data = array();
                                                $prev_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;
                                                $prev_sum_debit = 0;
                                                $prev_sum_payment = 0;

                                                $sum_vat = 0;
                                                $prev_sum_vat = 0;
                                                $sum_tax = 0;
                                                $prev_sum_tax = 0;
                                                $sum_discount = 0;
                                                $prev_sum_discount = 0;

                                                $b_item_count = 0;
                                                $no_of_bookings_per_room = 0;
                                                $rooms_count = 0;

                                                foreach ($result as $key => $data) 
                                                {
                                                    if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) || in_array(36, $post['CustomReports']['columns']) || in_array(37, $post['CustomReports']['columns']) || in_array(38, $post['CustomReports']['columns']) || in_array(39, $post['CustomReports']['columns']) || in_array(40, $post['CustomReports']['columns']))
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }

                                                    if($b_item_count == 0)
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                        echo "<tr>";
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column) 
                                                        {
                                                            if($column != 9)
                                                            {
                                                                echo "<td>";
                                                                echo "<b>".$columns[$column]['label']."</b>";
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                        $b_item_count++;
                                                        $rooms_count++;
                                                        $no_of_bookings_per_room++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['item_id'] != $result[$key-1]['item_id'])
                                                        {
                                                            $rooms_count = 1;
                                                            
                                                            if(CustomReports::hasRooms($post['CustomReports']['show_sub_totals']) )
                                                            {
                                                                echo "<tr>";
                                                                foreach ($post['CustomReports']['columns'] as $column) 
                                                                {
                                                                    if($column != 9)
                                                                    {
                                                                        $value = '';
                                                                        if($column == 16)
                                                                        {
                                                                            if($prev_sum_debit == 0)
                                                                            {
                                                                                $value = $sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_debit - $prev_sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                        }
                                                                        else if($column == 19)
                                                                        {
                                                                           if($prev_sum_payment == 0 )
                                                                            {
                                                                                $value = $sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_payment - $prev_sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                        }
                                                                        else if($column == 37)
                                                                        {
                                                                           if($prev_sum_tax == 0 )
                                                                            {
                                                                                $value = $sum_tax;
                                                                                $prev_sum_tax = $sum_tax;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_tax - $prev_sum_payment;
                                                                                $prev_sum_tax = $sum_tax;
                                                                            }
                                                                        }
                                                                        else if($column == 39)
                                                                        {
                                                                           if($prev_sum_discount == 0 )
                                                                            {
                                                                                $value = $sum_discount;
                                                                                $prev_sum_discount = $sum_discount;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_discount - $prev_sum_discount;
                                                                                $prev_sum_discount = $sum_discount;
                                                                            }
                                                                        }
                                                                        else if($column == 40)
                                                                        {
                                                                           if($prev_sum_vat == 0 )
                                                                            {
                                                                                $value = $sum_vat;
                                                                                $prev_sum_vat = $sum_vat;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_vat - $prev_sum_vat;
                                                                                $prev_sum_vat = $sum_vat;
                                                                            }
                                                                        }
                                                                        else if(array_key_exists($columns[$column]['db_col'], $sum_row_data) )
                                                                        {
                                                                            if(array_key_exists($columns[$column]['db_col'], $prev_row_data) )
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column]['db_col']] - $prev_row_data[$columns[$column]['db_col']];
                                                                                $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column]['db_col']];
                                                                                $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                            }
                                                                        } 
                                                                        else
                                                                        {

                                                                        }
                                                                        // $sum_row_data[$columns[$column]['db_col']]
                                                                        // if($post['CustomReports']['sum_average'] == 2)
                                                                        // {
                                                                        //     $value = round($sum_debit/count($result));
                                                                        // }
                                                                        // else
                                                                        // {
                                                                        //     $value = $sum_debit;
                                                                        // }

                                                                        // checking if it is sum or average //

                                                                        // echo "<pre>";
                                                                        // echo "sum/avg : ".$post['CustomReports']['sum_average'];
                                                                        // echo "<br>rooms count : ".$no_of_bookings_per_room;
                                                                        // echo "<br>value : ".$value;
                                                                        // exit();
                                                                        if($post['CustomReports']['sum_average'] == 2)
                                                                        {
                                                                            $value = round($value/$no_of_bookings_per_room);
                                                                        }
                                                                        $style = $columns[$column]['style'];
                                                                        echo "<td style='".$style."'>";
                                                                        echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                        echo "</td>";
                                                                    }
                                                                }
                                                                echo "</tr>";
                                                                $no_of_bookings_per_room = 1;
                                                            }
                                                                


                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($post['CustomReports']['columns'] as $column) 
                                                            {
                                                                if($column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        else
                                                        {
                                                            $rooms_count++;
                                                            $no_of_bookings_per_room++;
                                                        }


                                                    }
                                                    
                                                    echo "<tr class='".$class."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]); 

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;

                                                    $booking_item_vat_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('vat_amount');

                                                    $booking_item_tax_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('tax');

                                                    $booking_item_discount_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('discount_amount');

                                                    $sum_vat += $booking_item_vat_total;
                                                    $sum_tax += $booking_item_tax_total;
                                                    $sum_discount += $booking_item_discount_total;

                                                    $row_data = array();
                                                    // echo $booking_item_id;
                                                    // exit();
                                                    foreach ($post['CustomReports']['columns'] as $column) 
                                                    {
                                                        if($column != 9)
                                                        {
                                                            $style = $columns[$column]['style'];
                                                            echo "<td style='".$style."'>";
                                                            // echo "<td>";
                                                            if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40 )
                                                            {
                                                                if($columns[$column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column]['is_amount'] == 0)
                                                                    {
                                                                        
                                                                        if(isset($columns[$column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                                                $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                                            // echo $virtual_column_val;        
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                                            $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                            if($column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                                                $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                                                $sum_row_data[$columns[$column]['db_col']] = $val;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";

                                                                        $row_data[$columns[$column]['db_col']] = '- - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                                        echo $value;

                                                                        $row_data[$columns[$column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";

                                                        }
                                                        else
                                                        {
                                                            $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                            $row_count = 0;
                                                            $row_count2 = 0;
                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column]['db_col']];
                                                                    if($column == 1)
                                                                    {
                                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                                        if($column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16 ||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31 && $column !=9)
                                                                        {
                                                                            echo $row_data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }



                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            // echo "<tr style='border-bottom: solid red 3px;'>";
                                                            if($notes == '')
                                                            {
                                                                echo "<tr style='border-bottom: solid red 3px;'>";
                                                            }
                                                            else
                                                            {
                                                                echo "<tr >";
                                                            }
                                                            
                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1)
                                                                {
                                                                    if($b_item_count ==0 )
                                                                    {
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($result[$key+1]))
                                                                        {
                                                                            if($data['item_id'] != $result[$key+1]['item_id'])
                                                                            {

                                                                                $border =  'border-bottom: solid 3px';
                                                                            }
                                                                        }
                                                                    }

                                                                    
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                if($column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                       
                                                        
                                                    }
                                                     

                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr style='border-bottom: solid red 3px;'>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }
                                                // if( $key + 1 == count($result) && $rooms_count == 1)
                                                if( $key + 1 == count($result) )
                                                {
                                                    // echo "<pre>";
                                                    // print_r($rooms_count);
                                                    // exit();
                                                    if(CustomReports::hasRooms($post['CustomReports']['show_sub_totals']) )
                                                    {
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column) 
                                                        {
                                                            if($column != 9)
                                                            {
                                                                $value = '';
                                                                if($column == 16)
                                                                {
                                                                    if($prev_sum_debit == 0)
                                                                    {
                                                                        $value = $sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_debit - $prev_sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                }
                                                                else if($column == 19)
                                                                {
                                                                   if($prev_sum_payment == 0 )
                                                                    {
                                                                        $value = $sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_payment - $prev_sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                }
                                                                else if($column == 37)
                                                                {
                                                                   if($prev_sum_tax == 0 )
                                                                    {
                                                                        $value = $sum_tax;
                                                                        $prev_sum_tax = $sum_tax;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_tax - $prev_sum_payment;
                                                                        $prev_sum_tax = $sum_tax;
                                                                    }
                                                                }
                                                                else if($column == 39)
                                                                {
                                                                   if($prev_sum_discount == 0 )
                                                                    {
                                                                        $value = $sum_discount;
                                                                        $prev_sum_discount = $sum_discount;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_discount - $prev_sum_discount;
                                                                        $prev_sum_discount = $sum_discount;
                                                                    }
                                                                }
                                                                else if($column == 40)
                                                                {
                                                                   if($prev_sum_vat == 0 )
                                                                    {
                                                                        $value = $sum_vat;
                                                                        $prev_sum_vat = $sum_vat;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_vat - $prev_sum_vat;
                                                                        $prev_sum_vat = $sum_vat;
                                                                    }
                                                                }
                                                                else if(array_key_exists($columns[$column]['db_col'], $sum_row_data) )
                                                                {
                                                                    if(array_key_exists($columns[$column]['db_col'], $prev_row_data) )
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column]['db_col']] - $prev_row_data[$columns[$column]['db_col']];
                                                                        $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column]['db_col']];
                                                                        $prev_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']];
                                                                    }
                                                                } 
                                                                else
                                                                {

                                                                }
                                                                // $sum_row_data[$columns[$column]['db_col']]
                                                                // if($post['CustomReports']['sum_average'] == 2)
                                                                // {
                                                                //     $value = round($sum_debit/count($result));
                                                                // }
                                                                // else
                                                                // {
                                                                //     $value = $sum_debit;
                                                                // }
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $value = round($value/$no_of_bookings_per_room);
                                                                }
                                                                $style = $columns[$column]['style'];
                                                                echo "<td style='".$style."'>";
                                                                echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                    }
                                                        
                                                }
                                                
                                                if((in_array(10, $post['CustomReports']['columns']) || in_array(28, $post['CustomReports']['columns'])|| in_array(29, $post['CustomReports']['columns']) || in_array(30, $post['CustomReports']['columns']) || in_array(31, $post['CustomReports']['columns'])  || in_array(16, $post['CustomReports']['columns']) || in_array(19, $post['CustomReports']['columns'])) && (count($result) > 1 ) )
                                                {
                                                    
                                                    // if(CustomReports::hasReport($post['CustomReports']['show_sub_totals']) )
                                                    // {
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column)
                                                        {
                                                            $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                            if($column == 16)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 19)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 37)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_tax/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_tax;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 39)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_discount/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_discount;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 40)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_vat/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_vat;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    // }
                                                        
                                                }


                                                // echo "<pre>";
                                                //     print_r($sum_row_data);
                                                //     exit();
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <?php
                                }
                                else if($post['CustomReports']['group_by'] == 2) 
                                {
                            ?>
                                <table class="table table-striped table-bordered table2excel">
                                    <thead>
                                      <tr>
                                        <?php
                                            /*
                                            foreach ($post['CustomReports']['columns'] as $column) 
                                            {
                                                if($column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            */
                                        ?>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {

                                                $notes_exist = false;
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                    $notes_exist = true;
                                                }
                                                $sum_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;

                                                $sum_vat = 0;
                                                $prev_sum_vat = 0;
                                                $sum_tax = 0;
                                                $prev_sum_tax = 0;
                                                $sum_discount = 0;
                                                $prev_sum_discount = 0;

                                                $b_item_count = 0;
                                                $no_of_bookings_per_group = 0;
                                                foreach ($result as $key => $data) 
                                                {
                                                    if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) || in_array(36, $post['CustomReports']['columns']) || in_array(37, $post['CustomReports']['columns']) || in_array(38, $post['CustomReports']['columns']) || in_array(39, $post['CustomReports']['columns']) || in_array(40, $post['CustomReports']['columns']) )
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }

                                                    $border_groups = '';
                                                    if($b_item_count == 0)
                                                    {

                                                        echo "<tr>";
                                                            echo "<td colspan =".$colspan."><b>".CustomReportColumns::getGroupName($data['id'])."</b></td>";
                                                        echo "<tr>";
                                                        // echo "<tr>";
                                                        //     echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                        // echo "<tr>";
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column) 
                                                        {
                                                            if($column != 9)
                                                            {
                                                                echo "<td>";
                                                                echo "<b>".$columns[$column]['label']."</b>";
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                        $b_item_count++;
                                                        $no_of_bookings_per_group++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['booking_group_id'] != $result[$key-1]['booking_group_id'])
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            $border_groups = 'border-top:solid 3px';
                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getGroupName($data['id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($post['CustomReports']['columns'] as $column) 
                                                            {
                                                                if($column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                    }
                                                    
                                                    echo "<tr class='".$class."' style='".$border_groups."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]); 

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;

                                                    $booking_item_vat_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('vat_amount');

                                                    $booking_item_tax_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('tax');

                                                    $booking_item_discount_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('discount_amount');

                                                    $sum_vat += $booking_item_vat_total;
                                                    $sum_tax += $booking_item_tax_total;
                                                    $sum_discount += $booking_item_discount_total;

                                                    $row_data = array();
                                                    // echo $booking_item_id;
                                                    // exit();
                                                    foreach ($post['CustomReports']['columns'] as $column) 
                                                    {
                                                        if($column != 9)
                                                        {
                                                            $style = $columns[$column]['style'];
                                                            echo "<td style='".$style."'>";
                                                            // echo "<td>";
                                                            if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40)
                                                            {
                                                                if($columns[$column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column]['is_amount'] == 0)
                                                                    {
                                                                        
                                                                        if(isset($columns[$column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                                                $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                                            // echo $virtual_column_val;        
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                                            $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                            if($column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                                                $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                                                $sum_row_data[$columns[$column]['db_col']] = $val;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";

                                                                        $row_data[$columns[$column]['db_col']] = '- - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                                        echo $value;

                                                                        $row_data[$columns[$column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";

                                                        }
                                                        else
                                                        {
                                                            $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                            $row_count = 0;
                                                            $row_count2 = 0;
                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column]['db_col']];
                                                                    if($column == 1)
                                                                    {
                                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                                        if($column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31 && $column !=9)
                                                                        {
                                                                            echo $row_data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }



                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            // echo "<tr style='border-bottom: solid red 3px;'>";
                                                            if($notes == '')
                                                            {
                                                                echo "<tr style='border-bottom: solid red 3px;'>";
                                                            }
                                                            else
                                                            {
                                                                echo "<tr>";
                                                            }
                                                            
                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1)
                                                                {
                                                                    if($b_item_count ==0 )
                                                                    {
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($result[$key+1]))
                                                                        {
                                                                            if($data['booking_group_id'] != $result[$key+1]['booking_group_id'])
                                                                            {
                                                                                $border =  'border-bottom: solid 3px';
                                                                            }
                                                                        }
                                                                    }

                                                                    
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                if($column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                       
                                                        
                                                    }
                                                     

                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr style='border-bottom: solid red 3px;'>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }

                                                if((in_array(10, $post['CustomReports']['columns']) || in_array(28, $post['CustomReports']['columns'])|| in_array(29, $post['CustomReports']['columns']) || in_array(30, $post['CustomReports']['columns']) || in_array(31, $post['CustomReports']['columns'])  || in_array(16, $post['CustomReports']['columns']) || in_array(19, $post['CustomReports']['columns'])) && (count($result) > 1 ) )
                                                {
                                                    
                                                    // if(CustomReports::hasReport($post['CustomReports']['show_sub_totals']) )
                                                    // {
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column)
                                                        {
                                                            $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                            if($column == 16)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 19)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 37)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_tax/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_tax;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 39)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_discount/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_discount;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 40)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_vat/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_vat;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    // }
                                                }


                                                // echo "<pre>";
                                                //     print_r($sum_row_data);
                                                //     exit();
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <?php
                                }
                            
                                else if($post['CustomReports']['group_by'] == 3) 
                                {
                            ?>
                                <table class="table table-striped table-bordered table2excel">
                                    <thead>
                                      <tr>
                                        <?php
                                            /*
                                            foreach ($post['CustomReports']['columns'] as $column) 
                                            {
                                                if($column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            */
                                        ?>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php

                                            if(empty($result))
                                            {
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {

                                                $notes_exist = false;
                                                $check = array_search(9, $post['CustomReports']['columns']);
                                                if($check === false)
                                                {
                                                    $colspan = count($post['CustomReports']['columns']);
                                                }
                                                else
                                                {
                                                    $colspan = count($post['CustomReports']['columns']) - 1;
                                                    $notes_exist = true;
                                                }
                                                $sum_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;

                                                $sum_vat = 0;
                                                $prev_sum_vat = 0;
                                                $sum_tax = 0;
                                                $prev_sum_tax = 0;
                                                $sum_discount = 0;
                                                $prev_sum_discount = 0;

                                                $b_item_count = 0;
                                                
                                                foreach ($result as $key => $data) 
                                                {
                                                    if(!CustomReports::hasBookingGroup($data['id']) )
                                                    {
                                                        $no_of_bookings_to_exclude++;
                                                        continue;
                                                    }
                                                    if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) || in_array(36, $post['CustomReports']['columns']) || in_array(37, $post['CustomReports']['columns']) || in_array(38, $post['CustomReports']['columns']) || in_array(39, $post['CustomReports']['columns']) || in_array(40, $post['CustomReports']['columns']) )
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }

                                                    $border_groups = '';
                                                    if($b_item_count == 0)
                                                    {

                                                        echo "<tr>";
                                                            echo "<td colspan =".$colspan."><b>".CustomReportColumns::getGroupName($data['id'])."</b></td>";
                                                        echo "<tr>";
                                                        // echo "<tr>";
                                                        //     echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                        // echo "<tr>";
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column) 
                                                        {
                                                            if($column != 9)
                                                            {
                                                                echo "<td>";
                                                                echo "<b>".$columns[$column]['label']."</b>";
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                        $b_item_count++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['booking_group_id'] != $result[$key-1]['booking_group_id'])
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            $border_groups = 'border-top:solid 3px';
                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getGroupName($data['id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($post['CustomReports']['columns'] as $column) 
                                                            {
                                                                if($column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                    }
                                                    
                                                    echo "<tr class='".$class."' style='".$border_groups."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]); 

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;

                                                    $booking_item_vat_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('vat_amount');

                                                    $booking_item_tax_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('tax');

                                                    $booking_item_discount_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('discount_amount');

                                                    $sum_vat += $booking_item_vat_total;
                                                    $sum_tax += $booking_item_tax_total;
                                                    $sum_discount += $booking_item_discount_total;
                                                    $row_data = array();
                                                    // echo $booking_item_id;
                                                    // exit();
                                                    foreach ($post['CustomReports']['columns'] as $column) 
                                                    {
                                                        if($column != 9)
                                                        {
                                                            $style = $columns[$column]['style'];
                                                            echo "<td style='".$style."'>";
                                                            // echo "<td>";
                                                            if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40)
                                                            {
                                                                if($columns[$column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column]['is_amount'] == 0)
                                                                    {
                                                                        
                                                                        if(isset($columns[$column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                                                $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                                            // echo $virtual_column_val;        
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                                            $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                            if($column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                                            if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                                                $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                                                $sum_row_data[$columns[$column]['db_col']] = $val;
                                                                            }
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";

                                                                        $row_data[$columns[$column]['db_col']] = '- - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                                        echo $value;

                                                                        $row_data[$columns[$column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";

                                                        }
                                                        else
                                                        {
                                                            $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                            $row_count = 0;
                                                            $row_count2 = 0;
                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column]['db_col']];
                                                                    if($column == 1)
                                                                    {
                                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                                        if($column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                                        {
                                                                            if($columns[$column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31 && $column !=9)
                                                                        {
                                                                            echo $row_data[$columns[$column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }



                                                        if(in_array(13, $post['CustomReports']['columns']) || in_array(14, $post['CustomReports']['columns'])|| in_array(15, $post['CustomReports']['columns']) || in_array(17, $post['CustomReports']['columns']) || in_array(18, $post['CustomReports']['columns']) )
                                                        {
                                                            // echo "<tr style='border-bottom: solid red 3px;'>";
                                                            if($notes == '')
                                                            {
                                                                echo "<tr style='border-bottom: solid red 3px;'>";
                                                            }
                                                            else
                                                            {
                                                                echo "<tr>";
                                                            }
                                                            
                                                            foreach ($post['CustomReports']['columns'] as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1)
                                                                {
                                                                    if($b_item_count ==0 )
                                                                    {
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($result[$key+1]))
                                                                        {
                                                                            if($data['booking_group_id'] != $result[$key+1]['booking_group_id'])
                                                                            {
                                                                                $border =  'border-bottom: solid 3px';
                                                                            }
                                                                        }
                                                                    }

                                                                    
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                if($column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }
                                                       
                                                        
                                                    }
                                                     

                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr style='border-bottom: solid red 3px;'>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }

                                                if((in_array(10, $post['CustomReports']['columns']) || in_array(28, $post['CustomReports']['columns'])|| in_array(29, $post['CustomReports']['columns']) || in_array(30, $post['CustomReports']['columns']) || in_array(31, $post['CustomReports']['columns'])  || in_array(16, $post['CustomReports']['columns']) || in_array(19, $post['CustomReports']['columns'])) && (count($result) - $no_of_bookings_to_exclude > 1 ) )
                                                {
                                                    
                                                    // if(CustomReports::hasReport($post['CustomReports']['show_sub_totals']) )
                                                    // {
                                                        echo "<tr>";
                                                        foreach ($post['CustomReports']['columns'] as $column)
                                                        {
                                                            $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                            if($column == 16)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 19)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 37)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_tax/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_tax;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 39)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_discount/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_discount;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column == 40)
                                                            {
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_vat/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_vat;
                                                                }
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($post['CustomReports']['sum_average'] == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                
                                                                echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    // }
                                                }

                                                if(count($result) - $no_of_bookings_to_exclude == 0)
                                                {
                                                    echo "<tr>";
                                                        echo "<td colspan='".$colspan."'>";
                                                        echo "<b>No Record Found</b>";
                                                        echo "</td>";
                                                    echo "</tr>";
                                                }
                                                    


                                                // echo "<pre>";
                                                //     print_r($sum_row_data);
                                                //     exit();
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <?php
                                }
                            ?>
	                    		
						        <p style="font-size: 12px">Total Records : <?=count($result) - $no_of_bookings_to_exclude ?></p>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php

    $this->registerJs("
            $('.print-pdf').click(function(){
                // alert('clicked');
                $('#pdf-form').attr('target', '_blank');
                $('#pdf-form').submit();
            });
            $('#excel-btn').click(function(){
              $('.table2excel').table2excel({
                    exclude: '.noExl',
                    name: '".$post['CustomReports']['report_title']."',
                    filename: '".$post['CustomReports']['report_title']."',
                    fileext: '.xls',
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });
            $('#csv-btn').click(function(){
                console.log('clicked');
                
                  $('.table2excel').tableToCSV({
                    name: '".$post['CustomReports']['report_title']."',
                    filename: '".$post['CustomReports']['report_title']."',
                  });
            }); 

        ");
?>
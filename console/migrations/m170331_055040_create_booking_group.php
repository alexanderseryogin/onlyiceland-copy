<?php

use yii\db\Migration;

class m170331_055040_create_booking_group extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `booking_item_group`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `booking_item_id` INT(11) UNSIGNED, `booking_group_id` INT(11) UNSIGNED, PRIMARY KEY (`id`), CONSTRAINT `booking_item_group_biid` FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `booking_item_group_bgid` FOREIGN KEY (`booking_group_id`) REFERENCES `booking_group`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ); 
        ");
    }

    public function down()
    {
        echo "m170331_055040_create_booking_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

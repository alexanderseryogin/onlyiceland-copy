<?php
use common\models\CustomReportColumns;
use common\models\BookingDateFinances;
use common\models\BookingsItems;
use common\models\CustomReports;

$this->title = 'Execute Report';
$this->params['breadcrumbs'][] = ['label' => 'Custom Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Execute';
$columns = CustomReportColumns::getColumns();
$alias = CustomReportColumns::getAlias();
function bubble_Sort($my_array )
{
    do
    {
        $swapped = false;
        for( $i = 0, $c = count( $my_array ) - 1; $i < $c; $i++ )
        {
            if( $my_array[$i]['date'] > $my_array[$i + 1]['date'] )
            {
                list( $my_array[$i + 1], $my_array[$i] ) =
                        array( $my_array[$i], $my_array[$i + 1] );
                $swapped = true;
            }
        }
    }
    while( $swapped );
return $my_array;
}
?>
<form id="pdf-form" action="<?=yii::$app->getUrlManager()->createUrl(['custom-reports/print'])?>" method="POST">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
    <input type="text" name="query" value="<?=$org_query?>" hidden>
    <input type="text" name="title" value="<?=$model->report_title?>" hidden>
    <input type="text" name="sum_average" value="<?=$model->sum_average?>" hidden>
    <input type="text" name="group_by" value="<?=$model->group_by?>" hidden>
    <input type="text" name="columns" value='<?=serialize($selected_columns)?>' hidden>
</form>
<div class="custom-report" id="custom-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$model->report_title?></span>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group">
                        <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                            
                        </span></button>

                            <ul id="w3" class="dropdown-menu">
                                <li title="Comma Separated Values"><a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                <li title="Microsoft Excel 2007+ (xlsx)"><a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                <li>
                                    <a href="javascript:void(0);" target="_blank" class="print-pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                    PDF</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
                            <!-- <div class="btn-group pull-left" style="margin: 5px">
                                <div class="btn-group">
                                <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                                    
                                </span></button>

                                    <ul id="w3" class="dropdown-menu">
                                        <li title="Comma Separated Values"><a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                        <li title="Microsoft Excel 2007+ (xlsx)"><a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                        <li>
                                            <a href="javascript:void(0);" target="_blank" class="print-pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            PDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div> -->
                            <br>
	                    	<div class="tab-pane active" id="arrival-report">
                                  <h5 class="hide"><b><?=$query?></b></h5>
                                  <!-- <br><br><br> -->

                                  <?php
                                    // echo "<pre>";
                                    // print_r($result);
                                    // exit();
                                  ?>
                            <?php 
                                if($model->group_by == 0)
                                {
                            ?>
	                    	    <table class="table table-striped table-bordered table2excel">
                                    <thead> 
                                      <tr>
                                        <?php
                                            /*
                                            foreach ($model->customReportColumns as $column) 
                                            {
                                                if($column->column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column->column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            */
                                        ?>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {
                                                $notes_exist = false;
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                    $notes_exist = true;
                                                }
                                                
                                                $sum_row_data = array();
                                                $prev_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;
                                                $prev_sum_debit = 0;
                                                $prev_sum_payment = 0;
                                                $b_item_count = 0;
                                                $properties_count = 0;
                                                foreach ($result  as $key => $data) 
                                                {
                                                    $check_paymnt_chrgs_cols = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15,17,18,36,37,38,39,40] ]);
                                                   if(!empty($check_paymnt_chrgs_cols) )
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }

                                                    if($b_item_count == 0)
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan =".$colspan."><b>".CustomReportColumns::getDestinationName($data['id'])."</b></td>";
                                                        echo "<tr>";
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column) 
                                                        {
                                                            if($column->column != 9)
                                                            {
                                                                echo "<td>";
                                                                echo "<b>".$columns[$column->column]['label']."</b>";
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                        $b_item_count++;
                                                        $properties_count++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['provider_id'] != $result[$key-1]['provider_id'])
                                                        {
                                                            $properties_count = 1;
                                                            if(CustomReports::hasProperties($model->show_sub_totals) )
                                                            {
                                                                echo "<tr>";
                                                                foreach ($model->customReportColumns as $column) 
                                                                {
                                                                    if($column->column != 9)
                                                                    {
                                                                        $value = '';
                                                                        if($column->column == 16)
                                                                        {
                                                                            if($prev_sum_debit == 0)
                                                                            {
                                                                                $value = $sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_debit - $prev_sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                        }
                                                                        else if($column->column == 19)
                                                                        {
                                                                           if($prev_sum_payment == 0 )
                                                                            {
                                                                                $value = $sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_payment - $prev_sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                        }
                                                                        else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data) )
                                                                        {
                                                                            if(array_key_exists($columns[$column->column]['db_col'], $prev_row_data) )
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column->column]['db_col']] - $prev_row_data[$columns[$column->column]['db_col']];
                                                                                $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column->column]['db_col']];
                                                                                $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                            }
                                                                        } 
                                                                        else
                                                                        {

                                                                        }
                                                                        // $sum_row_data[$columns[$column]['db_col']]
                                                                        // if($post['CustomReports']['sum_average'] == 2)
                                                                        // {
                                                                        //     $value = round($sum_debit/count($result));
                                                                        // }
                                                                        // else
                                                                        // {
                                                                        //     $value = $sum_debit;
                                                                        // }
                                                                        echo "<td style='".$style."'>";
                                                                        echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                        echo "</td>";
                                                                    }
                                                                }
                                                                echo "</tr>";
                                                            }


                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getDestinationName($data['id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($model->customReportColumns as $column) 
                                                            {
                                                                if($column->column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column->column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        else
                                                        {
                                                            $properties_count++;
                                                        }

                                                    }

                                                    echo "<tr class='".$class."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');

                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]);

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;
                                                    $row_data = array();

                                                    foreach ($model->customReportColumns as $column) 
                                                    {
                                                        if($column->column != 9)
                                                        {

                                                            $style = $columns[$column->column]['style'];
                                                            echo "<td style='".$style."'>";

                                                            if($column->column != 13 && $column->column != 14 && $column->column != 15 && $column->column != 17 && $column->column != 18 && $column->column != 36 && $column->column != 37&& $column->column != 38 && $column->column != 39 && $column->column != 40)
                                                            {
                                                                if($columns[$column->column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column->column]['is_amount'] == 0)
                                                                    {
                                                                        if(isset($columns[$column->column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);

                                                                            $row_data[$columns[$column->column]['db_col']] =  empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];

                                                                            $row_data[$columns[$column->column]['db_col']] = empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];
                                                                        }
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column->column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column->column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['virtual_column']]);

                                                                            if($column->column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column->column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }

                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0 )?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);

                                                                            $row_data[$columns[$column->column]['db_col']] = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0 )?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);

                                                                            if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0)?0:$data[$columns[$column->column]['db_col']];

                                                                                $sum_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0)?0:$data[$columns[$column->column]['db_col']];
                                                                                $sum_row_data[$columns[$column->column]['db_col']] = $val;
                                                                            }

                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column->column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";
                                                                        $row_data[$columns[$column->column]['db_col']] = '- - - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column->column,$data[$columns[$column->column]['db_col']]);
                                                                        echo $value;

                                                                        $row_data[$columns[$column->column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";
                                                        }
                                                        else
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            $notes = empty($data[$columns[$column->column]['db_col']])?'':$data[$columns[$column->column]['db_col']];
                                                            // $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    // echo "<pre>";
                                                    // print_r($row_data);
                                                    // exit(); 
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        // checking if charge item desc column exists //
                                                        // $chrg_itm_desc_exists = array_search(14, $post['CustomReports']['columns']);
                                                        // $pymnt_item_desc_exists = array_search(18, $post['CustomReports']['columns']);
                                                        $chrg_itm_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15] ]);
                                                        $pymnt_item_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [17,18] ]);

                                                        // $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        // $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(!empty($chrg_itm_desc_exists))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(!empty($pymnt_item_desc_exists))
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                        $row_count = 0;
                                                        $row_count2 = 0;

                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($model->customReportColumns as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column->column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column->column]['db_col']];
                                                                    if($column->column == 1 )
                                                                    {
                                                                        echo "<td style='".$columns[$column->column]['style']."'>";
                                                                        if($column->column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column->column == 13 || $column->column == 14 || $column->column == 15 ||$column->column == 16 ||$column->column == 36 ||$column->column == 37 ||$column->column == 38 ||$column->column == 39 ||$column->column == 40) )
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column->column == 17 || $column->column == 18 || $column->column == 19) )
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column->column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column->column != 10 && $column->column != 28 && $column->column != 29 && $column->column != 30 && $column->column != 31)
                                                                        {
                                                                            echo $row_data[$columns[$column->column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }


                                                        if(!empty(CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15,17,18] ])) )
                                                        {
                                                            echo "<tr>";
                                                            foreach ($model->customReportColumns as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1 )
                                                                {
                                                                    $border =  'border-bottom: solid 3px';
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                if($column->column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column->column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column->column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column->column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column->column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column->column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column->column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }

                                                        // if(!empty($chrg_itm_desc_exists ))
                                                        // {
                                                        //     // echo "<pre>";
                                                        //     // print_r($booking_item_charges);
                                                        //     // exit;
                                                        //     foreach ($booking_item_charges as $booking_item_charge) 
                                                        //     {
                                                        //         echo "<tr>";
                                                        //         $col_count = 0;

                                                        //         foreach ($model->customReportColumns as $column)
                                                        //         {
                                                        //             $col_count++;
                                                        //             if(array_key_exists($columns[$column->column]['db_col'], $booking_item_charge))
                                                        //             {
                                                        //                 $value = $booking_item_charge[$columns[$column->column]['db_col']];
                                                        //                 if($column->column == 1 || $column->column == 18 || $column->column == 19 || $column->column == 17) 
                                                        //                 {
                                                        //                     echo "<td>";
                                                                            
                                                        //                     echo "</td>";
                                                        //                 }
                                                        //                 else
                                                        //                 {
                                                        //                    if($columns[$column->column]['is_amount'] == 0)
                                                        //                     { 
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo $value;
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                     else
                                                        //                     {
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo Yii::$app->formatter->asDecimal($value);
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                 }
                                                                        
                                                                        
                                                        //             }
                                                        //             else
                                                        //             {
                                                        //                 echo "<td>";
                                                                            
                                                        //                 echo "</td>";

                                                        //             }
                                                                    
                                                                    
                                                        //         }
   
                                                        //         echo "</tr>";
                                                        //         // echo "<tr>";
                                                        //         // foreach ($post['CustomReports']['columns'] as $column)
                                                        //         // {
                                                        //         //     echo "<td>";
                                                        //         //     if($data[$columns[$column]['db_col'])
                                                        //         //     {

                                                        //         //     }
                                                        //         //     else
                                                        //         //     {

                                                        //         //     }
                                                        //         //     echo "</td>";
                                                        //         // } 
                                                        //         // echo "</tr>";
                                                        //     }
                                                        //     // $colspan = count($post['CustomReports']['columns']);
                                                        // }

                                                        // if(!empty($pymnt_item_desc_exists))
                                                        // {
                                                        //     // echo "<pre>";
                                                        //     // print_r($booking_item_charges);
                                                        //     // exit;
                                                        //     foreach ($booking_item_payments as $booking_item_payment) 
                                                        //     {
                                                        //         echo "<tr>";
                                                        //         $col_count = 0;

                                                        //         foreach ($model->customReportColumns as $column)
                                                        //         {
                                                        //             $col_count++;
                                                        //             if(array_key_exists($columns[$column->column]['db_col'], $booking_item_payment))
                                                        //             {
                                                        //                 $value = $booking_item_payment[$columns[$column->column]['db_col']];
                                                        //                 if($column->column == 1 || $column->column == 15 || $column->column == 16 || $column->column == 14 || $column->column == 13)
                                                        //                 {
                                                        //                     echo "<td>";
                                                                            
                                                        //                     echo "</td>";
                                                        //                 }
                                                        //                 else
                                                        //                 {
                                                        //                     if($columns[$column->column]['is_amount'] == 0)
                                                        //                     { 
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo $value;
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                     else
                                                        //                     {
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo Yii::$app->formatter->asDecimal($value);
                                                        //                         echo "</td>";
                                                        //                     }
                                                                                
                                                        //                 }

                                                        //             }
                                                        //             else
                                                        //             {
                                                        //                 echo "<td>";
                                                        //                 echo "</td>";
                                                        //             }
                                                        //         }
   
                                                        //         echo "</tr>";
                                                        //     }
                                                        //     // $colspan = count($post['CustomReports']['columns']);
                                                        // }
                                                        // exit();
                                                        
                                                    }
                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }

                                                // if( $key + 1 == count($result) && $properties_count == 1)
                                                if( $key + 1 == count($result) )
                                                {
                                                    if(CustomReports::hasProperties($model->show_sub_totals) )
                                                    {
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column) 
                                                        {
                                                            if($column->column != 9)
                                                            {
                                                                $value = '';
                                                                if($column->column == 16)
                                                                {
                                                                    if($prev_sum_debit == 0)
                                                                    {
                                                                        $value = $sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_debit - $prev_sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                }
                                                                else if($column->column == 19)
                                                                {
                                                                   if($prev_sum_payment == 0 )
                                                                    {
                                                                        $value = $sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_payment - $prev_sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                }
                                                                else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data) )
                                                                {
                                                                    if(array_key_exists($columns[$column->column]['db_col'], $prev_row_data) )
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column->column]['db_col']] - $prev_row_data[$columns[$column->column]['db_col']];
                                                                        $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column->column]['db_col']];
                                                                        $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                    }
                                                                } 
                                                                else
                                                                {

                                                                }
                                                                // $sum_row_data[$columns[$column]['db_col']]
                                                                // if($post['CustomReports']['sum_average'] == 2)
                                                                // {
                                                                //     $value = round($sum_debit/count($result));
                                                                // }
                                                                // else
                                                                // {
                                                                //     $value = $sum_debit;
                                                                // }
                                                                echo "<td style='".$style."'>";
                                                                echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "<tr>";
                                                    }
                                                        
                                                }

                                                if(!empty(CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [10,28,29,30,31,16,19] ])) && (count($result) > 1 ) )
                                                {
                                                    
                                                    if(CustomReports::hasReport($model->show_sub_totals) )
                                                    {
                                                        $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column)
                                                        {
                                                            if($column->column == 16)
                                                            {
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column->column == 19)
                                                            {
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column->column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column->column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
	                    	<?php
                                }
                                else if($model->group_by == 1) 
                                {
                            ?>
                                <table class="table table-striped table-bordered table2excel">
                                    <thead> 
                                      <!-- <tr> -->
                                        <?php
                                            /*
                                            foreach ($model->customReportColumns as $column) 
                                            {
                                                if($column->column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column->column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            */
                                        ?>
                                      <!-- </tr> -->
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {
                                                $notes_exist = false;
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                    $notes_exist = true;
                                                }
                                                $sum_row_data = array();
                                                $prev_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;
                                                $prev_sum_debit = 0;
                                                $prev_sum_payment = 0;
                                                $b_item_count = 0;
                                                $no_of_bookings_per_room = 0;
                                                $rooms_count = 0;
                                                // $sum_row_data = array();
                                                // $sum_debit = 0;
                                                // $sum_payment = 0;
                                                // $b_item_count = 0;
                                                foreach ($result as $key => $data) 
                                                {
                                                    $check_paymnt_chrgs_cols = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15,17,18,36,37,38,39,40] ]);
                                                   if(!empty($check_paymnt_chrgs_cols) )
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }
                                                    if($b_item_count == 0)
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                        echo "<tr>";
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column) 
                                                        {
                                                            if($column->column != 9)
                                                            {
                                                                echo "<td>";
                                                                echo "<b>".$columns[$column->column]['label']."</b>";
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                        $b_item_count++;
                                                        $rooms_count++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['item_id'] != $result[$key-1]['item_id'])
                                                        {
                                                            // echo "<pre>";
                                                            // print_r($sum_row_data);
                                                            // exit();
                                                            $rooms_count = 1;
                                                            if(CustomReports::hasRooms($model->show_sub_totals) )
                                                            {
                                                                echo "<tr>";
                                                                foreach ($model->customReportColumns as $column) 
                                                                {
                                                                    if($column->column != 9)
                                                                    {
                                                                        $value = '';
                                                                        if($column->column == 16)
                                                                        {
                                                                            if($prev_sum_debit == 0)
                                                                            {
                                                                                $value = $sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_debit - $prev_sum_debit;
                                                                                $prev_sum_debit = $sum_debit;
                                                                            }
                                                                        }
                                                                        else if($column->column == 19)
                                                                        {
                                                                           if($prev_sum_payment == 0 )
                                                                            {
                                                                                $value = $sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_payment - $prev_sum_payment;
                                                                                $prev_sum_payment = $sum_payment;
                                                                            }
                                                                        }
                                                                        else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data) )
                                                                        {
                                                                            if(array_key_exists($columns[$column->column]['db_col'], $prev_row_data) )
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column->column]['db_col']] - $prev_row_data[$columns[$column->column]['db_col']];
                                                                                $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                            }
                                                                            else
                                                                            {
                                                                                $value = $sum_row_data[$columns[$column->column]['db_col']];
                                                                                $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                            }
                                                                        } 
                                                                        else
                                                                        {

                                                                        }
                                                                        // $sum_row_data[$columns[$column]['db_col']]
                                                                        // if($post['CustomReports']['sum_average'] == 2)
                                                                        // {
                                                                        //     $value = round($sum_debit/count($result));
                                                                        // }
                                                                        // else
                                                                        // {
                                                                        //     $value = $sum_debit;
                                                                        // }
                                                                        echo "<td style='".$style."'>";
                                                                        echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                        echo "</td>";
                                                                    }
                                                                }
                                                                echo "</tr>";
                                                            }
                                                                


                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($model->customReportColumns as $column) 
                                                            {
                                                                if($column->column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column->column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                        else
                                                        {
                                                            $rooms_count++;
                                                        }


                                                    }

                                                    echo "<tr class='".$class."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');

                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]);

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;
                                                    $row_data = array();

                                                    foreach ($model->customReportColumns as $column) 
                                                    {
                                                        if($column->column != 9)
                                                        {

                                                            $style = $columns[$column->column]['style'];
                                                            echo "<td style='".$style."'>";

                                                            if($column->column != 13 && $column->column != 14 && $column->column != 15 && $column->column != 17 && $column->column != 18 && $column->column != 36 && $column->column != 37 && $column->column != 38 && $column->column != 39 && $column->column != 40)
                                                            {
                                                                if($columns[$column->column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column->column]['is_amount'] == 0)
                                                                    {
                                                                        if(isset($columns[$column->column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);

                                                                            $row_data[$columns[$column->column]['db_col']] =  empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];

                                                                            $row_data[$columns[$column->column]['db_col']] = empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];
                                                                        }
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column->column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column->column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['virtual_column']]);

                                                                            if($column->column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column->column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }

                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0 )?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);

                                                                            $row_data[$columns[$column->column]['db_col']] = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0 )?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);

                                                                            if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0)?0:$data[$columns[$column->column]['db_col']];

                                                                                $sum_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0)?0:$data[$columns[$column->column]['db_col']];
                                                                                $sum_row_data[$columns[$column->column]['db_col']] = $val;
                                                                            }

                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column->column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";
                                                                        $row_data[$columns[$column->column]['db_col']] = '- - - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column->column,$data[$columns[$column->column]['db_col']]);
                                                                        echo $value;

                                                                        $row_data[$columns[$column->column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";
                                                        }
                                                        else
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            $notes = empty($data[$columns[$column->column]['db_col']])?'':$data[$columns[$column->column]['db_col']];
                                                            // $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    // echo "<pre>";
                                                    // print_r($row_data);
                                                    // exit(); 
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        // checking if charge item desc column exists //
                                                        // $chrg_itm_desc_exists = array_search(14, $post['CustomReports']['columns']);
                                                        // $pymnt_item_desc_exists = array_search(18, $post['CustomReports']['columns']);
                                                        $chrg_itm_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15] ]);
                                                        $pymnt_item_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [17,18] ]);

                                                        // $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        // $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(!empty($chrg_itm_desc_exists))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(!empty($pymnt_item_desc_exists))
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                        $row_count = 0;
                                                        $row_count2 = 0;

                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($model->customReportColumns as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column->column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column->column]['db_col']];
                                                                    if($column->column == 1 )
                                                                    {
                                                                        echo "<td style='".$columns[$column->column]['style']."'>";
                                                                        if($column->column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column->column == 13 || $column->column == 14 || $column->column == 15 ||$column->column == 16 ||$column->column == 36 ||$column->column == 37 ||$column->column == 38 ||$column->column == 39||$column->column == 40) )
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column->column == 17 || $column->column == 18 || $column->column == 19) )
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column->column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column->column != 10 && $column->column != 28 && $column->column != 29 && $column->column != 30 && $column->column != 31)
                                                                        {
                                                                            echo $row_data[$columns[$column->column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }


                                                        if(!empty(CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15,17,18] ])) )
                                                        {
                                                            echo "<tr style='border-bottom: solid red 3px;'>";
                                                            foreach ($model->customReportColumns as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1 )
                                                                {
                                                                    if($b_item_count ==0 )
                                                                    {
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($result[$key+1]))
                                                                        {
                                                                            if($data['item_id'] != $result[$key+1]['item_id'])
                                                                            {
                                                                                $border =  'border-bottom: solid 3px';
                                                                            }
                                                                        }
                                                                    }
                                                                    // $border =  'border-bottom: solid 3px';
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                if($column->column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column->column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column->column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column->column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column->column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column->column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column->column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }

                                                        // if(!empty($chrg_itm_desc_exists ))
                                                        // {
                                                        //     // echo "<pre>";
                                                        //     // print_r($booking_item_charges);
                                                        //     // exit;
                                                        //     foreach ($booking_item_charges as $booking_item_charge) 
                                                        //     {
                                                        //         echo "<tr>";
                                                        //         $col_count = 0;

                                                        //         foreach ($model->customReportColumns as $column)
                                                        //         {
                                                        //             $col_count++;
                                                        //             if(array_key_exists($columns[$column->column]['db_col'], $booking_item_charge))
                                                        //             {
                                                        //                 $value = $booking_item_charge[$columns[$column->column]['db_col']];
                                                        //                 if($column->column == 1 || $column->column == 18 || $column->column == 19 || $column->column == 17) 
                                                        //                 {
                                                        //                     echo "<td>";
                                                                            
                                                        //                     echo "</td>";
                                                        //                 }
                                                        //                 else
                                                        //                 {
                                                        //                    if($columns[$column->column]['is_amount'] == 0)
                                                        //                     { 
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo $value;
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                     else
                                                        //                     {
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo Yii::$app->formatter->asDecimal($value);
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                 }
                                                                        
                                                                        
                                                        //             }
                                                        //             else
                                                        //             {
                                                        //                 echo "<td>";
                                                                            
                                                        //                 echo "</td>";

                                                        //             }
                                                                    
                                                                    
                                                        //         }
   
                                                        //         echo "</tr>";
                                                        //         // echo "<tr>";
                                                        //         // foreach ($post['CustomReports']['columns'] as $column)
                                                        //         // {
                                                        //         //     echo "<td>";
                                                        //         //     if($data[$columns[$column]['db_col'])
                                                        //         //     {

                                                        //         //     }
                                                        //         //     else
                                                        //         //     {

                                                        //         //     }
                                                        //         //     echo "</td>";
                                                        //         // } 
                                                        //         // echo "</tr>";
                                                        //     }
                                                        //     // $colspan = count($post['CustomReports']['columns']);
                                                        // }

                                                        // if(!empty($pymnt_item_desc_exists))
                                                        // {
                                                        //     // echo "<pre>";
                                                        //     // print_r($booking_item_charges);
                                                        //     // exit;
                                                        //     foreach ($booking_item_payments as $booking_item_payment) 
                                                        //     {
                                                        //         echo "<tr>";
                                                        //         $col_count = 0;

                                                        //         foreach ($model->customReportColumns as $column)
                                                        //         {
                                                        //             $col_count++;
                                                        //             if(array_key_exists($columns[$column->column]['db_col'], $booking_item_payment))
                                                        //             {
                                                        //                 $value = $booking_item_payment[$columns[$column->column]['db_col']];
                                                        //                 if($column->column == 1 || $column->column == 15 || $column->column == 16 || $column->column == 14 || $column->column == 13)
                                                        //                 {
                                                        //                     echo "<td>";
                                                                            
                                                        //                     echo "</td>";
                                                        //                 }
                                                        //                 else
                                                        //                 {
                                                        //                     if($columns[$column->column]['is_amount'] == 0)
                                                        //                     { 
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo $value;
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                     else
                                                        //                     {
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo Yii::$app->formatter->asDecimal($value);
                                                        //                         echo "</td>";
                                                        //                     }
                                                                                
                                                        //                 }

                                                        //             }
                                                        //             else
                                                        //             {
                                                        //                 echo "<td>";
                                                        //                 echo "</td>";
                                                        //             }
                                                        //         }
   
                                                        //         echo "</tr>";
                                                        //     }
                                                        //     // $colspan = count($post['CustomReports']['columns']);
                                                        // }
                                                        // exit();
                                                        
                                                    }
                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }

                                                // if( $key + 1 == count($result)  && $rooms_count == 1 )
                                                if( $key + 1 == count($result)  )
                                                {
                                                    if(CustomReports::hasRooms($model->show_sub_totals) )
                                                    {
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column) 
                                                        {
                                                            if($column->column != 9)
                                                            {
                                                                $value = '';
                                                                if($column->column == 16)
                                                                {
                                                                    if($prev_sum_debit == 0)
                                                                    {
                                                                        $value = $sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_debit - $prev_sum_debit;
                                                                        $prev_sum_debit = $sum_debit;
                                                                    }
                                                                }
                                                                else if($column->column == 19)
                                                                {
                                                                   if($prev_sum_payment == 0 )
                                                                    {
                                                                        $value = $sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_payment - $prev_sum_payment;
                                                                        $prev_sum_payment = $sum_payment;
                                                                    }
                                                                }
                                                                else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data) )
                                                                {
                                                                    if(array_key_exists($columns[$column->column]['db_col'], $prev_row_data) )
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column->column]['db_col']] - $prev_row_data[$columns[$column->column]['db_col']];
                                                                        $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = $sum_row_data[$columns[$column->column]['db_col']];
                                                                        $prev_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']];
                                                                    }
                                                                } 
                                                                else
                                                                {

                                                                }
                                                                // $sum_row_data[$columns[$column]['db_col']]
                                                                // if($post['CustomReports']['sum_average'] == 2)
                                                                // {
                                                                //     $value = round($sum_debit/count($result));
                                                                // }
                                                                // else
                                                                // {
                                                                //     $value = $sum_debit;
                                                                // }
                                                                echo "<td style='".$style."'>";
                                                                echo  ($value =='')?'':Yii::$app->formatter->asDecimal($value);
                                                                echo "</td>";
                                                            }
                                                        }
                                                        echo "</tr>";
                                                    }
                                                        
                                                }


                                                if(!empty(CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [10,28,29,30,31,16,19] ])) && (count($result) > 1 ) )
                                                {
                                                    
                                                    if(CustomReports::hasReport($model->show_sub_totals) )
                                                    {
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column)
                                                        {
                                                            $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                            if($column->column == 16)
                                                            {
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column->column == 19)
                                                            {
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column->column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column->column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <?php
                                }
                                else if($model->group_by == 2) 
                                {
                            ?>
                                <table class="table table-striped table-bordered table2excel">
                                    <thead> 
                                      <tr>
                                        <?php
                                           
                                            foreach ($model->customReportColumns as $column) 
                                            {
                                                if($column->column != 9)
                                                {
                                                    echo "<th>";
                                                    echo $columns[$column->column]['label'];
                                                    echo "</th>";
                                                }
                                            }
                                            
                                        ?>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(empty($result))
                                            {
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                }
                                                echo "<tr>";
                                                    echo "<td colspan='".$colspan."'>";
                                                    echo "<b>No Record Found</b>";
                                                    echo "</td>";
                                                echo "</tr>";
                                            }
                                            else
                                            {
                                                $notes_exist = false;
                                                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => 9]);
                                                if(empty($check))
                                                {
                                                    $colspan = count($model->customReportColumns);
                                                }
                                                else
                                                {
                                                    $colspan = count($model->customReportColumns) - 1;
                                                    $notes_exist = true;
                                                }
                                                $sum_row_data = array();
                                                $sum_debit = 0;
                                                $sum_payment = 0;
                                                $b_item_count = 0;
                                                foreach ($result as $key => $data) 
                                                {
                                                    $check_paymnt_chrgs_cols = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15,17,18,36,37,38,39,40] ]);
                                                   if(!empty($check_paymnt_chrgs_cols) )
                                                    {
                                                        $class ='hide';
                                                    }
                                                    else
                                                    {
                                                        $class = '';
                                                    }
                                                    $border_groups = '';
                                                    if($b_item_count == 0)
                                                    {

                                                        // echo "<tr>";
                                                        //     echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                                        // echo "<tr>";
                                                        // echo "<tr>";
                                                        // foreach ($post['CustomReports']['columns'] as $column) 
                                                        // {
                                                        //     if($column != 9)
                                                        //     {
                                                        //         echo "<td>";
                                                        //         echo "<b>".$columns[$column]['label']."</b>";
                                                        //         echo "</td>";
                                                        //     }
                                                        // }
                                                        // echo "</tr>";
                                                        $b_item_count++;
                                                    }
                                                    else
                                                    {
                                                        if( $data['booking_group_id'] != $result[$key-1]['booking_group_id'])
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            // $border_groups = 'border-top:solid 3px';
                                                            $border_groups = 'border-top:solid 3px';
                                                            echo "<tr>";
                                                                echo "<td colspan =".$colspan."><b>".CustomReportColumns::getGroupName($data['id'])."</b></td>";
                                                            echo "<tr>";
                                                            echo "<tr>";
                                                            foreach ($model->customReportColumns as $column) 
                                                            {
                                                                if($column->column != 9)
                                                                {
                                                                    echo "<td>";
                                                                    echo "<b>".$columns[$column->column]['label']."</b>";
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            echo "</tr>";
                                                        }
                                                    }

                                                    echo "<tr class='".$class."' style='".$border_groups."'>";
                                                    $notes = '';
                                                    $booking_item_id = isset($data['id'])?$data['id']:'';

                                                    $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                                                    $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');

                                                    $booking_item = BookingsItems::findOne(['id' => $booking_item_id]);

                                                    $sum_debit += $booking_item_charges_total;
                                                    $sum_payment += $booking_item_payments_total;
                                                    $row_data = array();

                                                    foreach ($model->customReportColumns as $column) 
                                                    {
                                                        if($column->column != 9)
                                                        {

                                                            $style = $columns[$column->column]['style'];
                                                            echo "<td style='".$style."'>";

                                                            if($column->column != 13 && $column->column != 14 && $column->column != 15 && $column->column != 17 && $column->column != 18 && $column->column != 36 && $column->column != 37 && $column->column != 38 && $column->column != 39 && $column->column != 40)
                                                            {
                                                                if($columns[$column->column]['is_foreign_key'] == 0)
                                                                {
                                                                    if($columns[$column->column]['is_amount'] == 0)
                                                                    {
                                                                        if(isset($columns[$column->column]['timestamp']))
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);

                                                                            $row_data[$columns[$column->column]['db_col']] =  empty($data[$columns[$column->column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column->column]['db_col']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];

                                                                            $row_data[$columns[$column->column]['db_col']] = empty($data[$columns[$column->column]['db_col']])?'- - - -':$data[$columns[$column->column]['db_col']];
                                                                        }
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($columns[$column->column]['virtual_column']))
                                                                        {
                                                                            // echo empty($data[$columns[$column->column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['virtual_column']]);

                                                                            if($column->column == 16)
                                                                            {
                                                                                echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                            }
                                                                            if($column->column ==19)
                                                                            {
                                                                                echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                                                // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                            }

                                                                            // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                                        }
                                                                        else
                                                                        {
                                                                            echo (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0 )?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);

                                                                            $row_data[$columns[$column->column]['db_col']] = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0 )?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column->column]['db_col']]);

                                                                            if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data))
                                                                            {
                                                                                $val = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0)?0:$data[$columns[$column->column]['db_col']];

                                                                                $sum_row_data[$columns[$column->column]['db_col']] = $sum_row_data[$columns[$column->column]['db_col']] + $val;
                                                                            }
                                                                            else
                                                                            {
                                                                                $val = (empty($data[$columns[$column->column]['db_col']]) && $data[$columns[$column->column]['db_col']] !=0)?0:$data[$columns[$column->column]['db_col']];
                                                                                $sum_row_data[$columns[$column->column]['db_col']] = $val;
                                                                            }

                                                                        }
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    if(empty($data[$columns[$column->column]['db_col']]))
                                                                    {
                                                                        echo "- - - -";
                                                                        $row_data[$columns[$column->column]['db_col']] = '- - - - -';
                                                                    }
                                                                    else
                                                                    {
                                                                        $value = CustomReportColumns::getColumnValue($column->column,$data[$columns[$column->column]['db_col']]);
                                                                        echo $value;

                                                                        $row_data[$columns[$column->column]['db_col']] = $value;
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            echo "</td>";
                                                        }
                                                        else
                                                        {
                                                            // echo "here";
                                                            // exit();
                                                            $notes = empty($data[$columns[$column->column]['db_col']])?'':$data[$columns[$column->column]['db_col']];
                                                            // $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                                        }
                                                    }
                                                    // echo "</tr>";
                                                    // echo "<pre>";
                                                    // print_r($row_data);
                                                    // exit(); 
                                                    if($booking_item_id != '')
                                                    {
                                                        $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        // checking if charge item desc column exists //
                                                        // $chrg_itm_desc_exists = array_search(14, $post['CustomReports']['columns']);
                                                        // $pymnt_item_desc_exists = array_search(18, $post['CustomReports']['columns']);
                                                        $chrg_itm_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15] ]);
                                                        $pymnt_item_desc_exists = CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [17,18] ]);

                                                        // $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                                        // $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                                        $charges_arr = array();
                                                        $payments_arr = array();

                                                        if(!empty($chrg_itm_desc_exists))
                                                        {
                                                            $charges_arr = $booking_item_charges;
                                                        }

                                                        if(!empty($pymnt_item_desc_exists))
                                                        {
                                                            $payments_arr = $booking_item_payments;
                                                        }

                                                        $merged_arr = array_merge($charges_arr,$payments_arr);
                                                        // echo "<pre>";
                                                        $merged_arr =bubble_Sort($merged_arr);
                                                        $row_count = 0;
                                                        $row_count2 = 0;

                                                        foreach ($merged_arr as $arr) 
                                                        {
                                                            echo "<tr>";
                                                            $col_count = 0;

                                                            foreach ($model->customReportColumns as $column)
                                                            {
                                                                $col_count++;
                                                                if(array_key_exists($columns[$column->column]['db_col'], $arr))
                                                                {
                                                                    $value = $arr[$columns[$column->column]['db_col']];
                                                                    if($column->column == 1 )
                                                                    {
                                                                        echo "<td style='".$columns[$column->column]['style']."'>";
                                                                        if($column->column == 1 && $row_count2 < 1)
                                                                        {
                                                                            
                                                                            echo $row_data['id'];
                                                                            
                                                                            $row_count2++;
                                                                        }
                                                                        echo "</td>";
                                                                    }
                                                                    else
                                                                    {
                                                                        if($arr['type'] == 1 && ($column->column == 13 || $column->column == 14 || $column->column == 15 ||$column->column == 16||$column->column == 36 ||$column->column == 37 ||$column->column == 38 ||$column->column == 39 ||$column->column == 40) )
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else if($arr['type'] == 2 && ($column->column == 17 || $column->column == 18 || $column->column == 19) )
                                                                        {
                                                                            if($columns[$column->column]['is_amount'] == 0)
                                                                            { 
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo $value;
                                                                                echo "</td>";
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<td style='".$columns[$column->column]['style']."'>";
                                                                                    echo Yii::$app->formatter->asDecimal($value);
                                                                                echo "</td>";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<td>";
                                                                            echo "</td>";
                                                                        }

                                                                            
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    echo "<td style='".$columns[$column->column]['style']."'>";
                                                                    if($row_count < 1 )
                                                                    {
                                                                        if($column->column != 10 && $column->column != 28 && $column->column != 29 && $column->column != 30 && $column->column != 31)
                                                                        {
                                                                            echo $row_data[$columns[$column->column]['db_col']];
                                                                        }
                                                                    }
                                                                    echo "</td>";
                                                                }
                                                            }
                                                            $row_count++;
                                                            echo "</tr>";
                                                        }


                                                        if(!empty(CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [13,14,15,17,18] ])) )
                                                        {
                                                            echo "<tr style='border-bottom: solid red 3px;'>";
                                                            foreach ($model->customReportColumns as $column)
                                                            {
                                                                $border = '';
                                                                if(count($result) >1 )
                                                                {
                                                                    if($b_item_count ==0 )
                                                                    {
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        if(isset($result[$key+1]))
                                                                        {
                                                                            if($data['booking_group_id'] != $result[$key+1]['booking_group_id'])
                                                                            {
                                                                                $border =  'border-bottom: solid 3px';
                                                                            }
                                                                        }
                                                                    }
                                                                    // $border =  'border-bottom: solid 3px';
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                if($column->column == 16)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                                }
                                                                if($column->column == 19)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                                }
                                                                if($column->column == 10)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                                                }
                                                                if($column->column == 28)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                                                }
                                                                if($column->column == 29)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                                                }
                                                                if($column->column == 30)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                                                }
                                                                if($column->column == 31)
                                                                {
                                                                    echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                                                }
                                                                echo "</td>";
                                                            }
                                                            echo "</tr>";
                                                        }

                                                        // if(!empty($chrg_itm_desc_exists ))
                                                        // {
                                                        //     // echo "<pre>";
                                                        //     // print_r($booking_item_charges);
                                                        //     // exit;
                                                        //     foreach ($booking_item_charges as $booking_item_charge) 
                                                        //     {
                                                        //         echo "<tr>";
                                                        //         $col_count = 0;

                                                        //         foreach ($model->customReportColumns as $column)
                                                        //         {
                                                        //             $col_count++;
                                                        //             if(array_key_exists($columns[$column->column]['db_col'], $booking_item_charge))
                                                        //             {
                                                        //                 $value = $booking_item_charge[$columns[$column->column]['db_col']];
                                                        //                 if($column->column == 1 || $column->column == 18 || $column->column == 19 || $column->column == 17) 
                                                        //                 {
                                                        //                     echo "<td>";
                                                                            
                                                        //                     echo "</td>";
                                                        //                 }
                                                        //                 else
                                                        //                 {
                                                        //                    if($columns[$column->column]['is_amount'] == 0)
                                                        //                     { 
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo $value;
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                     else
                                                        //                     {
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo Yii::$app->formatter->asDecimal($value);
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                 }
                                                                        
                                                                        
                                                        //             }
                                                        //             else
                                                        //             {
                                                        //                 echo "<td>";
                                                                            
                                                        //                 echo "</td>";

                                                        //             }
                                                                    
                                                                    
                                                        //         }
   
                                                        //         echo "</tr>";
                                                        //         // echo "<tr>";
                                                        //         // foreach ($post['CustomReports']['columns'] as $column)
                                                        //         // {
                                                        //         //     echo "<td>";
                                                        //         //     if($data[$columns[$column]['db_col'])
                                                        //         //     {

                                                        //         //     }
                                                        //         //     else
                                                        //         //     {

                                                        //         //     }
                                                        //         //     echo "</td>";
                                                        //         // } 
                                                        //         // echo "</tr>";
                                                        //     }
                                                        //     // $colspan = count($post['CustomReports']['columns']);
                                                        // }

                                                        // if(!empty($pymnt_item_desc_exists))
                                                        // {
                                                        //     // echo "<pre>";
                                                        //     // print_r($booking_item_charges);
                                                        //     // exit;
                                                        //     foreach ($booking_item_payments as $booking_item_payment) 
                                                        //     {
                                                        //         echo "<tr>";
                                                        //         $col_count = 0;

                                                        //         foreach ($model->customReportColumns as $column)
                                                        //         {
                                                        //             $col_count++;
                                                        //             if(array_key_exists($columns[$column->column]['db_col'], $booking_item_payment))
                                                        //             {
                                                        //                 $value = $booking_item_payment[$columns[$column->column]['db_col']];
                                                        //                 if($column->column == 1 || $column->column == 15 || $column->column == 16 || $column->column == 14 || $column->column == 13)
                                                        //                 {
                                                        //                     echo "<td>";
                                                                            
                                                        //                     echo "</td>";
                                                        //                 }
                                                        //                 else
                                                        //                 {
                                                        //                     if($columns[$column->column]['is_amount'] == 0)
                                                        //                     { 
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo $value;
                                                        //                         echo "</td>";
                                                        //                     }
                                                        //                     else
                                                        //                     {
                                                        //                         echo "<td style='".$columns[$column->column]['style']."'>";
                                                        //                             echo Yii::$app->formatter->asDecimal($value);
                                                        //                         echo "</td>";
                                                        //                     }
                                                                                
                                                        //                 }

                                                        //             }
                                                        //             else
                                                        //             {
                                                        //                 echo "<td>";
                                                        //                 echo "</td>";
                                                        //             }
                                                        //         }
   
                                                        //         echo "</tr>";
                                                        //     }
                                                        //     // $colspan = count($post['CustomReports']['columns']);
                                                        // }
                                                        // exit();
                                                        
                                                    }
                                                    if($notes_exist && $notes !='')
                                                    {
                                                        echo "<tr>";
                                                            echo "<td colspan='".$colspan."'>";
                                                            echo $notes;
                                                            echo "</td>";
                                                        echo "</tr>";
                                                    }
                                                }



                                                if(!empty(CustomReportColumns::findOne(['custom_report_id' => $model->id,'column' => [10,28,29,30,31,16,19] ])) && (count($result) > 1 ) )
                                                {
                                                    
                                                    if(CustomReports::hasReport($model->show_sub_totals) )
                                                    {
                                                        echo "<tr>";
                                                        foreach ($model->customReportColumns as $column)
                                                        {
                                                            $border = 'border-bottom:solid 3px;border-top:solid 3px';
                                                            if($column->column == 16)
                                                            {
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_debit/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_debit;
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if($column->column == 19)
                                                            {
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_payment/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_payment;
                                                                }
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else if(array_key_exists($columns[$column->column]['db_col'], $sum_row_data))
                                                            {
                                                                // echo "here";
                                                                // exit();
                                                                if($model->sum_average == 2)
                                                                {
                                                                    $val = round($sum_row_data[$columns[$column->column]['db_col']]/count($result));
                                                                }
                                                                else
                                                                {
                                                                    $val = $sum_row_data[$columns[$column->column]['db_col']];
                                                                }
                                                                
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                echo Yii::$app->formatter->asDecimal($val);
                                                                echo "</td>";
                                                            }
                                                            else
                                                            {
                                                                
                                                                echo "<td style='".$columns[$column->column]['style'].' '.$border."'>";
                                                                // echo $column;
                                                                echo "</td>";
                                                            }
                                                           
                                                        }
                                                        echo "</tr>";
                                                    }
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            <?php
                                }
                            ?>

						        <p style="font-size: 12px">Total Records : <?=count($result)?></p>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
    $this->registerJs("
            $('.print-pdf').click(function(){
                // alert('clicked');
                $('#pdf-form').attr('target', '_blank');
                $('#pdf-form').submit();
            });

             $('#excel-btn').click(function(){
              $('.table2excel').table2excel({
                    exclude: '.noExl',
                    name: '".$model->report_title."',
                    filename: '".$model->report_title."',
                    fileext: '.xls',
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true
                });
            });
            $('#csv-btn').click(function(){
                console.log('clicked');
                
                  $('.table2excel').tableToCSV({
                    name: '".$model->report_title."',
                    filename: '".$model->report_title."',
                  });
            }); 

        ");
?>
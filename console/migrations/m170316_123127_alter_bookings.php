<?php

use yii\db\Migration;

class m170316_123127_alter_bookings extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` ADD COLUMN `arrival_date` DATE NULL AFTER `rate_conditions`, ADD COLUMN `departure_date` DATE NULL AFTER `arrival_date`, ADD COLUMN `no_of_nights` INT(11) NULL AFTER `departure_date`; 
        ");
    }

    public function down()
    {
        echo "m170316_123127_alter_bookings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

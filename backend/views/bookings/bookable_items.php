
<?php
	use common\models\BookableItems;
	
	$items = BookableItems::find()
            ->where(['provider_id' => $provider_id])
            ->all();
    echo "<option value='show_all'>Show All</option>";
    
	if(!empty($items))
	{
		foreach ($items as $key =>  $item) 
	    {
	        echo "<option value='".$item->id."'>".$item->itemType->name."</option>";  
	    }
	}  
?>


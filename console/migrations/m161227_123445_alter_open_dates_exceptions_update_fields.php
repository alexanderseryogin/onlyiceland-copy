<?php

use yii\db\Migration;

class m161227_123445_alter_open_dates_exceptions_update_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours_exceptions` DROP COLUMN `opening_second`, DROP COLUMN `closing_second`, CHANGE `split_service` `hours_24` TINYINT(1) NULL, CHANGE `provider_id` `provider_id` INT(11) NULL AFTER `hours_24`; ");
    }

    public function down()
    {
        echo "m161227_123445_alter_open_dates_exceptions_update_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

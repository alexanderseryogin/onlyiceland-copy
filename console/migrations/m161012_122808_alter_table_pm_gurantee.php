<?php

use yii\db\Migration;

class m161012_122808_alter_table_pm_gurantee extends Migration
{
    public function up()
    {
        $this->execute("
                ALTER TABLE `property_pm_guarantee` CHANGE `property_id` `provider_id` INT(11) NOT NULL, CHANGE `property_pm_id` `pm_id` INT(11) NOT NULL, DROP INDEX `FK_property_pm_guarantee_property_id`, DROP INDEX `FK_property_pm_guarantee_property_pm_id`, DROP FOREIGN KEY `FK_property_pm_guarantee_property_id`, DROP FOREIGN KEY `FK_property_pm_guarantee_property_pm_id`, ADD CONSTRAINT `FK_pm_guarantee_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE, ADD CONSTRAINT `FK_pm_guarantee_pm_id` FOREIGN KEY (`pm_id`) REFERENCES `payment_methods`(`id`) ON DELETE CASCADE; 
                    
                    RENAME TABLE `property_pm_guarantee` TO `payment_method_guarantee`;
            ");
    }

    public function down()
    {
        echo "m161012_122808_alter_table_pm_gurantee cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

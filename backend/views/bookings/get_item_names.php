<?php
use yii\helpers\ArrayHelper;
use common\models\BookableItemsNames;
?>

<option value=""> - - - - - -</option>
<?php
	$selected_item = $b_item->item_name_id;
	$check = 0;
    if(!empty($availableItemNames))
    {
    	if(in_array($selected_item, $availableItemNames))
    	{
    		$check = 1;
    	}
        foreach ($availableItemNames as $key => $value) 
        {
            $model = BookableItemsNames::findOne(['id' => $value]);
            if($check == 1 && $value == $selected_item)
            {
            	echo '<option value="'.$model->id.'"selected>'.$model->item_name.'</option>';
            }
            else if($check ==0 )
            {
            	echo '<option value="'.$model->id.'"selected>'.$model->item_name.'</option>';
            	$check = 1;
            }
            else
            {
                echo '<option value="'.$model->id.'">'.$model->item_name.'</option>';
            }
            
        }
    }
?>
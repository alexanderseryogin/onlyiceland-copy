
<?php
	use common\models\RatesDiscountCode;
	$Checkbox = [
	    0 => '',
	    1 => 'checked'
	];

	$Options = [
        0 => '',
        1 => 'selected',
    ];
	
	$codes = RatesDiscountCode::find()
            ->where(['rates_id' => $rates_id])
            ->all();

    echo '<td style="min-width:170px;">';
    
	if(!empty($codes))
	{
		foreach ($codes as $key =>  $code) 
	    {
	    	$checked = 0; 
            $display = 'none';

	    	if(!empty($checked_discount_codes) && array_key_exists($code->discount_code_id, $checked_discount_codes))
	    	{
	    		$checked = 1; 
	    	}
            if(isset($org_rate_exists) && $org_rate_exists == 1)
            {
                $checked = 0;
            }
            if(isset($selected_discount_code_extra_bed_quantity) && !empty($selected_discount_code_extra_bed_quantity) && array_key_exists($code->discount_code_id, $selected_discount_code_extra_bed_quantity))
            {
                $display = 'inline';
            }

	    	$orgAmount = $code->discountCode->amount;
	    	$icelandAmount = Yii::$app->formatter->asDecimal( $orgAmount, "ISK");

	    	?>

	    	<?php if($code->discountCode->quantity_type == 1 && $max_extra_beds > 0): ?> <!-- Dependant on extra persons -->

    			<label>
    				<input date="<?php echo $dateKey; ?>" data="<?php echo $code->discount_code_id ?>" id="<?php echo $code->rates->unit_id.':'.$rates_id ?>" class="discount-codes discount-codes-<?php echo $rates_id.'-'.$dateKey?>" value="<?php echo $orgAmount?>" <?php echo $Checkbox[$checked]?> type="checkbox" >
    			 	<?php echo $code->discountCode->name.': '.$icelandAmount ?>
    			</label>

	    		<select date="<?php echo $dateKey; ?>" style="display:<?php echo $display ?>; width: 40px;" data="<?php echo $code->rates->unit_id.':'.$rates_id?>" class="discount-code-extra-bed-quantity discount-code-extra-bed-quantity-<?php echo $rates_id?> discount-code-extra-bed-quantity-<?php echo $rates_id?>-<?php echo $code->discount_code_id.'-'.$dateKey?>" >
                    <?php

                        for ($i=0; $i <=$max_extra_beds ; $i++) 
                        {
                            $selected = 0;

                            if((isset($selected_discount_code_extra_bed_quantity) && !empty($selected_discount_code_extra_bed_quantity) && array_key_exists($code->discount_code_id, $selected_discount_code_extra_bed_quantity) && $selected_discount_code_extra_bed_quantity[$code->discount_code_id]==$i) || $i==1)
                            {
                            	$selected = 1;
                            }

                            if(isset($org_rate_exists) && $org_rate_exists == 1)
                            {
                                $selected = 0;
                            }
                            echo '<option value="'.$i.'" '.$Options[$selected].'>'.$i.'</option>';
                        }
                    ?>
                </select>
                <br>

            <?php elseif($code->discountCode->quantity_type == 2): ?> <!-- Dependant on all persons -->

        		<label>
    				<input date="<?php echo $dateKey; ?>" data="<?php echo $code->discount_code_id ?>" id="<?php echo $code->rates->unit_id.':'.$rates_id ?>" class="discount-codes discount-codes-<?php echo $rates_id.'-'.$dateKey?>" value="<?php echo $orgAmount?>" <?php echo $Checkbox[$checked]?> type="checkbox" >
    			 	<?php echo $code->discountCode->name.': '.$icelandAmount ?>
    			</label>

	    		<select date="<?php echo $dateKey; ?>" style="<?php echo $display ?>; width: 40px;" data="<?php echo $code->rates->unit_id.':'.$rates_id?>" class="discount-code-extra-bed-quantity discount-code-extra-bed-quantity-<?php echo $rates_id?>-<?php echo $code->discount_code_id.'-'.$dateKey?>" >
                    <?php

                        for ($i=0; $i <=$bed_type_people ; $i++) 
                        {
                            $selected = 0;

                            if((isset($selected_discount_code_extra_bed_quantity) && !empty($selected_discount_code_extra_bed_quantity) && array_key_exists($code->discount_code_id, $selected_discount_code_extra_bed_quantity) && $selected_discount_code_extra_bed_quantity[$code->discount_code_id]==$i)  || $i==1)
                            {
                            	$selected = 1;
                            }
                            if(isset($org_rate_exists) && $org_rate_exists == 1)
                            {
                                $selected = 0;
                            }

                            echo '<option value="'.$i.'" '.$Options[$selected].'>'.$i.'</option>';
                        }
                    ?>
                </select>
                <br>

            <?php endif ?>
    		<?php
	    }
	}

	echo '</td>'
?>


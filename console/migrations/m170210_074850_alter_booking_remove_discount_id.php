<?php

use yii\db\Migration;

class m170210_074850_alter_booking_remove_discount_id extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` DROP COLUMN `discount_id`, DROP INDEX `FK_bookings_discount_id`, DROP FOREIGN KEY `FK_bookings_discount_id`; 
        ");
    }

    public function down()
    {
        echo "m170210_074850_alter_booking_remove_discount_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

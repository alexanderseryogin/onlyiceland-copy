<?php

use yii\db\Migration;

class m170525_072342_alter_bookable_items_names extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items_names` ADD COLUMN `item_order` INT(11) NULL AFTER `item_name`; 
        ");
    }

    public function down()
    {
        echo "m170525_072342_alter_bookable_items_names cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

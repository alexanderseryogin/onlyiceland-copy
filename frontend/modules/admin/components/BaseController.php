<?php

namespace frontend\modules\admin\components;

use Yii;
use yii\helpers\Url;
use common\models\User;

class BaseController extends \yii\web\Controller
{
    public function beforeAction($event)
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

    	// check if user is of type Provider Admin
        if (!Yii::$app->user->isGuest)
        {
            if(Yii::$app->user->identity->first_login_reset_flag == 0)
            {
                if($action != 'first-login-reset')
                    $this->redirect(['/site/first-login-reset']);
            }
            elseif(Yii::$app->user->identity->type != User::USER_TYPE_PMANAGER)
            {
                $this->redirect(['/site/login']);
            }
        }
        else
        {
            $this->redirect(['/site/login']);
        }

    	// intialize page titles
        $this->view->params['title'] = '';
        $this->view->params['subTitle'] = '';

        return parent::beforeAction($event);
    }

}

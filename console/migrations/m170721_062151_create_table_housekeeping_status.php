<?php

use yii\db\Migration;

class m170721_062151_create_table_housekeeping_status extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `housekeeping_status`(  
                          `id` INT(11),
                          `label` VARCHAR(256),
                          `abbreviation` VARCHAR(256),
                          `background_color` VARCHAR(256),
                          `text-color` VARCHAR(256)
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170721_062151_create_table_housekeeping_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

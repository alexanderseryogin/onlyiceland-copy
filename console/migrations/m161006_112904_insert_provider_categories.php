<?php

use yii\db\Migration;

class m161006_112904_insert_provider_categories extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `provider_categories` (`name`) 
        VALUES ('No License Required/Engra leyfa krafist'),
               ('Accommodation/Gististaður: Category/Flokkur I'),
               ('Accommodation/Gististaður: Category/Flokkur II'),
               ('Accommodation/Gististaður: Category/Flokkur III'),
               ('Accommodation/Gististaður: Category/Flokkur IV'),
               ('Accommodation/Gististaður: Category/Flokkur V'),
               ('Restaurant/Veitingastaður: Category/Flokkur I'),
               ('Restaurant/Veitingastaður: Category/Flokkur II'),
               ('Restaurant/Veitingastaður: Category/Flokkur III')
        ;");
    }

    public function down()
    {
        echo "m161006_112904_insert_provider_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

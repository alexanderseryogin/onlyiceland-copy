<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BedTypes */
/* @var $form yii\widgets\ActiveForm */

$Checkbox = [
    0 => '',
    1 => 'checked',
];

?>
<style type="text/css">
	.rightAlgin
	{
		text-align: right;
	}
</style>
<div class="portlet light ">
<div class="bed-types-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
    	<div class="col-sm-6">
    		<?= $form->field($model, 'intended_sleeping_capacity')->textInput(['maxlength' => true, 'class' => 'rightAlgin form-control']) ?>
    	</div>
    	<div class="col-sm-6">
    		<?= $form->field($model, 'max_sleeping_capacity')->textInput(['maxlength' => true, 'class' => 'rightAlgin form-control']) ?>
    	</div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <label><input type="checkbox" name="BedTypes[combineable]" <?= $Checkbox[$model->combineable] ?> > Combinable</label>
        </div>
    </div>
    <br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/bed-types']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

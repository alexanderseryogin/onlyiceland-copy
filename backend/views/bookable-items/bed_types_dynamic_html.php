
<div class="row">
    <div class="col-sm-5" >
        <select name="BookableItems[bed_type][]" style="width: 100%;" class="dynamic-bed-types">
            <option value="">Select a Bed Type</option>
            <?php
                foreach ($bedTypes as $key => $value) 
                {
                    echo '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
            ?>
        </select>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <input class="dynamic-quantity" value="1" type="text" name="BookableItems[quantity][]" />
        </div>
    </div>
    <div class="col-sm-1 delete-dynamic-field" style="margin-top:17px;" >
        <a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>
    </div>
</div>
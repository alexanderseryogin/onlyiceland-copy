<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_items_names".
 *
 * @property string $id
 * @property integer $bookable_item_id
 * @property string $item_number
 * @property string $item_name
 * @property integer $item_order
 *
 * @property BookableItems $bookableItem
 */
class BookableItemsNames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_items_names';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookable_item_id', 'item_order'], 'integer'],
            [['item_number', 'item_name'], 'string', 'max' => 256],
            [['bookable_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'bookable_item_id' => Yii::t('app', 'Bookable Item ID'),
            'item_number' => Yii::t('app', 'Item Number'),
            'item_name' => Yii::t('app', 'Item Name'),
            'item_order' => Yii::t('app', 'Item Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_item_id']);
    }
}

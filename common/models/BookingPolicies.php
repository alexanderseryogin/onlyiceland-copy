<?php

namespace common\models;

use Yii;
use common\models\PropertyPaymentMethods;
use common\models\DestinationTypes;

/**
 * This is the model class for table "booking_policies".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $booking_type_id
 * @property integer $booking_status_id
 * @property integer $youngest_age
 * @property integer $oldest_free_age
 * @property string $payment_methods_gauarantee
 * @property string $payment_methods_checkin
 * @property string $checkin_time_from
 * @property string $checkin_time_to
 * @property string $checkout_time_from
 * @property string $checkout_time_to
 *
 * @property BookingStatuses $bookingStatus
 * @property BookingTypes $bookingType
 * @property Providers $provider
 */
class BookingPolicies extends \yii\db\ActiveRecord
{
    public static $methodsGuarantee='';
    public static $methodsCheckin='';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_policies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'booking_status_id', 'youngest_age', 'oldest_free_age','time_group'], 'required',
            'when' => function ($model) 
            {
                if(isset($model->destination->provider_type_id))
                {
                    $obj = DestinationTypes::findOne(['id' => $model->destination->provider_type_id]);
                    return  $obj->name == 'Accommodation';
                }
            },
            'whenClient' => "function (attribute, value) {
                return $('#select2-provider-type-dropdown-container').attr('title') == 'Accommodation';
            }"],
            [['provider_id', 'booking_type_id', 'booking_status_id', 'youngest_age', 'oldest_free_age','time_group','booking_flag_updated'], 'integer'],
            [['payment_methods_gauarantee', 'payment_methods_checkin'], 'string'],
            [['checkin_time_from', 'checkin_time_to', 'checkout_time_from', 'checkout_time_to'], 'safe'],
            [['booking_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['booking_status_id' => 'id']],
            [['booking_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['booking_type_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination ID'),
            'booking_type_id' => Yii::t('app', 'Booking Flags'),
            'booking_status_id' => Yii::t('app', 'Booking Status'),
            'youngest_age' => Yii::t('app', 'Youngest Age'),
            'oldest_free_age' => Yii::t('app', 'Oldest Free Age'),
            'payment_methods_gauarantee' => Yii::t('app', 'Payment Methods Accepted for Booking Guarantee'),
            'payment_methods_checkin' => Yii::t('app', 'Payment Methods Accepted at Check-in'),
            'checkin_time_from' => Yii::t('app', 'Checkin Time From'),
            'checkin_time_to' => Yii::t('app', 'Checkin Time To'),
            'checkout_time_from' => Yii::t('app', 'Checkout Time From'),
            'checkout_time_to' => Yii::t('app', 'Checkout Time To'),
            'time_group' => Yii::t('app', 'Estimated Arrival Times'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingStatus()
    {
        return $this->hasOne(BookingStatuses::className(), ['id' => 'booking_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingType()
    {
        return $this->hasOne(BookingTypes::className(), ['id' => 'booking_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }
}

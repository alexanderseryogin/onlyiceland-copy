<?php

use yii\db\Migration;

class m161006_114642_insert_booking_confirmation extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `booking_confirmation_types` (`name`) 
        VALUES ('Auto Confirmation with Credit Card Number'),
        ('Request Only with Credit Card Number'),
        ('Request Only and Requires Manual Action'),
        ('Auto Confirmation without Payment/Deposit'),
        ('Auto Confirmation with 10% Deposit'),
        ('Auto Confirmation with 25% Deposit'),
        ('Auto Confirmation with 50% Deposit'),
        ('Auto Confirmation when Paid in Full'),
        ('Blocked and no new Bookings Permitted'),
        ('Auto Confirmation and Non-Refundable')

        ;");
    }

    public function down()
    {
        echo "m161006_114642_insert_booking_confirmation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

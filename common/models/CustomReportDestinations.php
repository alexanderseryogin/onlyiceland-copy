<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_report_destinations".
 *
 * @property integer $id
 * @property integer $destination_id
 * @property integer $custom_report_id
 *
 * @property CustomReportColumns $customReport
 */
class CustomReportDestinations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_report_destinations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['destination_id', 'custom_report_id'], 'integer'],
            [['custom_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomReports::className(), 'targetAttribute' => ['custom_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'destination_id' => 'Destination ID',
            'custom_report_id' => 'Custom Report ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReport()
    {
        return $this->hasOne(CustomReports::className(), ['id' => 'custom_report_id']);
    }

    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'destination_id']);
    }
}

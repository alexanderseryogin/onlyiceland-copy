<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\BookingsItems;
use common\models\BookingDateFinances;
use common\models\DestinationAddOns;
use common\models\PaymentMethodCheckin;
use common\models\BookingItemGroup;
use common\models\BookingGroupFinance;
// use yii\helpers\Url;
// $booking_dates = $model->bookingDatesSortedAndNotNull;
$currentDate =  date('d-m-Y');
// $destination_id = $model->provider_id;
$total_debit = 0;
foreach ($bookingGroupModel->bookingItemGroups as $bookingGroupItem) 
{
	$total_debit += $bookingGroupItem->bookingItem->balance;
}

// $total_debit = BookingDateFinances::find()->where(['booking_group_id' => $bookingGroupModel->id])
// 										  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
// 										  ->sum('final_total');
// echo "<pre>";
// print_r($bookingGroupModel);
// exit();
$total_credit = BookingGroupFinance::find()->where(['booking_group_id' => $bookingGroupModel->id])
										  ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
										  ->sum('amount');
// $total_debit = empty($total_debit)?0:$total_debit;
$total_credit =  empty($total_credit)?0:$total_credit;		

$balance_due = $total_debit - $total_credit; 

// echo "<pre>";
// print_r($bookingGroupModel->bookingItemGroups);
// exit();

?>
<script type="text/javascript">
	var global_payment = <?=$total_debit?>;
	console.log(global_payment);
</script>

<div class="portlet box green-meadow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-minus"></i>&nbsp;&nbsp;&nbsp;Charges / Debits </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
           <!--  <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="" class="fullscreen"> </a>
            <a href="javascript:;" class="reload"> </a> -->
        </div>
    </div>
    <div class="portlet-body"> 
    	<!-- <div class="row">
		    <div class="col-md-4 col-md-offset-5">
		          <h3><b>Charges</b></h3>
		    </div>  
		</div> -->
		<a href="javascript:void(0)" id="add_custom_charge" class="btn btn-default pull-right hide">Add Custom Charge</a>
		<a href="javascript:void(0)" id="add_add_on_btn" class="btn btn-primary pull-right hide">Add new Add-On/Upsell</a>
		
		<!-- <a href="javascript:void(0)" id="test_button" class="btn btn-primary pull-right">Test</a> -->
		<br>
		<!-- <input type="button" class="btn btn-success" ></input>
		<input type="button" class="btn btn-primary" ></input> -->
		<div id="table_charges_div" class="table_charges_div">
			<table id="charges_table" class="table">
			    <thead>
			      <tr>
			        <!-- <th>Id </th> -->
			        <th style="width: 15%;text-align: center;">Booking Item id</th>
			        <th style="width: 80%;">Item Name</th>
			        <th style="width: 30%;">Balance Due</th>
			         <th style="width: 4%;"></th>
			      <!--  <th style="width: 8%;"></th>
			        <th style="width: 6%;"></th>
			        <th style="width: 8%;"></th>
			        <th style="width: 6%;"> </th> -->
			        <th style="width: 10%;"> </th>
			        <th style="width: 10%;"> </th>
			        <th style="width: 10%;">  </th>
			        <th style="width: 5%;"> </th>
			      </tr>
			    </thead>
			    <tbody id="charges-row">
			    <?php 
			    		//$counter = 1;
			    		// foreach ($booking_dates as $booking_date) {
		    			if(!empty($bookingGroupModel->bookingItemGroups)){
		    				
		    				foreach ($bookingGroupModel->bookingItemGroups as $bookingGroupItem) {
			    ?>
			    	<tr>
			    		<td style="text-align: center;">
		    				<a href="<?=Url::to(['bookings/update', 'id'=> $bookingGroupItem->bookingItem->id])?>"><?= isset($bookingGroupItem->bookingItem->id)?$bookingGroupItem->bookingItem->id:"" ?></a>
		    			<td style="">
		    				<?= isset($bookingGroupItem->bookingItem->item->itemType->name)?$bookingGroupItem->bookingItem->item->itemType->name:"" ?>
		    			</td>
		    			<td style="text-align: right;">
		    				<?= isset($bookingGroupItem->bookingItem->balance)?Yii::$app->formatter->asDecimal($bookingGroupItem->bookingItem->balance,0):"" ?>
		    			</td>
			    	</tr>
		    	<?php
		    			}
		    		}
		    	?>
			 	
			    </tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-8" >
				<p style="text-align: right;"><b>Total Charges / Debits:</b></p>
			</div>
			<div class="col-md-2 ">
				<input id="total_charges" class="form-control" type="text" readonly style="width: 65%;text-align: right"; value="<?=Yii::$app->formatter->asDecimal($bookingGroupModel->balance,0)?>">
			</div>
		</div>
    </div>
</div>

<br>


<div class="portlet box blue" >
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus" style="margin-top:-1px"></i>&nbsp;&nbsp;&nbsp;Payments / Credits</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
           <!--  <a href="#portlet-config" data-toggle="modal" class="config"> </a>
            <a href="" class="fullscreen"> </a>
            <a href="javascript:;" class="reload"> </a> -->
        </div>
    </div>
    <div class="portlet-body"> 
    	<a href="javascript:void(0)" id="add_custom_credit_btn" class="btn btn-default pull-right hide">Add Custom Credit</a>
    	<a href="javascript:void(0)" id="add_credit_btn" class="btn btn-primary pull-right hide">Add new Credit</a>   	
    	<br>
    	<div class="table_payments_div">
	    	<table id="payments_table" class="table">
			    <thead>
			      <tr>
			        <!-- <th>Id </th> -->
			        <th style="width: 1%;"><i class="fa fa-eye"  title="Hide on Receipt" data-toggle="tooltip"></th>
			        <th style="width: 10%">Date</th>
			        <th style="width: 40%;">Payment / Credit Type</th>
			        <th style="width: 10%;">Payment Type</th>
			        <th style="width: 9%;">Deposit %</th>
			        <th style="width: 6%;"></th>
			        <th style="width: 5%;"></th>
			        <th style="width: 10%;">Payment / Credit Amount</th>
			        <th style="width: 5%;"> Actions</th>
			      </tr>
			    </thead>
			    <tbody id="payments-row">
			    	<?php 
			    		//$counter = 1;
			    		// foreach ($booking_dates as $booking_date) {
		    			if(!empty($bookingGroupModel->groupPayments)){
		    				// $count = count($booking_date->bookingDateCharges)*count($booking_dates);
		    				foreach ($bookingGroupModel->groupPayments as $bookingGroupFinance) {
								if(0){
			    	?>
			    	<!-- <tr id="<?=$bookingGroupFinance->id?>" class="">
			    		<td>
			    			<input class="checkbox" type="checkbox" name="" <?= ($bookingDateFinance->show_receipt == BookingDateFinances::HIDE_RECEIPT)?"checked":"" ?> >
			    		</td>
			    		<td>
			    		
					      <input   class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=date('d-m-Y',strtotime($bookingDateFinance->date))?>" >
					        		
			    		</td>
		    			<td>
		    				<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" >
		    			</td>
		    						    			
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			<input class="form-control total" type="text" name="" style="text-align: right;" value="<?= isset($bookingDateFinance->final_total)?Yii::$app->formatter->asDecimal($bookingDateFinance->final_total,0):"0"?>"  readonly>
			    		</td>
			    		<td>
			    			<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-payments hide" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus " aria-hidden="true" ></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments hide" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>

			    		</td>
			    	</tr> -->
			    	<?php } else { ?>
			    	<tr id="<?=$bookingGroupFinance->id?>" class="custom">
			    		<td>
			    			<input class="checkbox" type="checkbox" name="" <?= ($bookingGroupFinance->show_receipt == BookingDateFinances::HIDE_RECEIPT)?"checked":"" ?> >
			    		</td>
			    		<td>
			    		
					      <input   class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=date('d-m-Y',strtotime($bookingGroupFinance->date))?>">
					        		
			    		</td>
		    			<?php 
		    				$booking_group_items = BookingItemGroup::find()->where(['booking_group_id' => $bookingGroupModel->id])->all();
					        $destinations_arr = array();
					        foreach ($booking_group_items as $item) 
					        {
					            if(!in_array($item->bookingItem->provider_id, $destinations_arr))
					            {
					                array_push($destinations_arr, $item->bookingItem->provider_id);
					            }
					        }
		    				$all_payment_methods = PaymentMethodCheckin::find()->where(['in','provider_id',$destinations_arr])->all();
		    				//$selected_payment_methods = PaymentMethodCheckin::findOne(['id' => $bookingDateFinance->price_description]);
		    			?>
		    			<!-- <td>
		    				<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" >
		    			</td> -->
		    			<?php 
		    					echo '<td class="hasSelect">';?>
		    					<div class="form-group" style="margin-bottom: 0px;">
			    					<div class="input-group" style="text-align:left">
			    						<span class="input-group-btn">
							                <a href="javascript:;" class="btn btn-default btn-small show_dropdown" >
							                   <i class="fa fa-caret-down" aria-hidden="true"></i></a>
							            </span>
							            <!-- <input type="text" class="form-control" name="username1" id="username1_input"> -->
							            <input class="form-control" type="text" name="" value="<?=isset($bookingGroupFinance->price_description)?$bookingGroupFinance->price_description:""?>" >
<!-- 
		    					<button class="btn btn-default btn-small show_dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></button>
		    					<input class="form-control" type="text" name="" value="<?= isset($bookingDateFinance->price_description)?$bookingDateFinance->price_description:"" ?>" style="float: right; width: 92%;"> -->
		    					<?php 
		    					echo '<select data-placeholder="Select an Option" class="form-control payment_method_dropdown " style="width:100%">';
					            echo '<option></option>';
				                foreach ($all_payment_methods as $payment_method) 
				                {
				                	
				                	echo '<option value="'.$payment_method->id.'">'.$payment_method->pm->name.'</option>';
				                	
				                }
					            echo '</select></div></div></td>';
		    			?>
			    			
			    		<td>
			    			<?php if($bookingGroupFinance->payment_type){?>
			    				<span class="label label-info">Deposit</span>
			    			<?php } else{?>
			    				<span class="label label-default">Normal</span>
			    			<?php }?>
			    		</td>
			    		<td >
			    			<?php if($bookingGroupFinance->payment_type){?>
			    				<?=$bookingGroupFinance->deposit_percent_age?>
			    			<?php } else{?>
			    				
			    			<?php }?>
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			
			    		</td>
			    		<td>
			    			<input id="total" class="form-control total" type="text" name="" style="text-align: right;" value="<?= isset($bookingGroupFinance->amount)?Yii::$app->formatter->asDecimal($bookingGroupFinance->amount,0):"0"?>" >
			    		</td>
			    		<td>
			    			<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-payments hide" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus " aria-hidden="true" ></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>

			    		</td>
			    	</tr>
			    	<?php 		
			    				}	
			    			}
			    		}
			    	 ?>
			    </tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-8" >
				<p style="text-align: right;"><b>Total Payments / Credits:</b></p>
			</div>
			<div class="col-md-2 " >
				<input id="total_payment" class="form-control" type="text" readonly  style="width: 65%;text-align: right";  value="<?=Yii::$app->formatter->asDecimal($total_credit,0)?>">
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-1 col-md-offset-8" style="width: 12.5%;">
				<p><b>Balance Due</b></p>
			</div>
			<div class="col-md-2 ">
				<input id="total_due" class="form-control" type="text" readonly style="width: 90%;text-align: right";>
			</div>
		</div> -->
    </div>
</div>
<div class="row">
	<div class="col-md-2 col-md-offset-8" >
		<p style="font-size:20px;text-align: right;"><b>Balance Due:</b></p>
	</div>
	<div class="col-md-1 " style="text-align: right;">
		<span style="font-size:20px;text-align:right"><b id="total_due"><?=Yii::$app->formatter->asDecimal($bookingGroupModel->balance,0)?></b></span>
		
	</div>
</div>

<?php
$add_payment_entry = Url::to(['/bookings/add-group-payment-entry']);
$delete_payment_entry = Url::to(['/bookings/delete-group-payment-entry']);
$calculate_deposit = Url::to(['/bookings/calculate-deposit']);
$calculate_percentage = Url::to(['/bookings/calculate-percentage']);
$this->registerJs("


		    $(document).on('change','.payment_method_dropdown',function()
		    {
		   		var id = $(this).val();
		   		var input_this = $(this);
		   		//alert($(this).find(':selected').text());
		   		var type= $(this).find(':selected').attr('data-type')
		   		//alert(type);
		   		console.log('id :'+id);
		   		var charge_cell = $(this).parent().nextAll();
		   		var prev_charge_cell = $(this).prevAll();
		   		console.log(charge_cell.eq(0).children('input'));
		   		prev_charge_cell.eq(0).removeClass('hide');
			    prev_charge_cell.eq(0).val(input_this.find(':selected').text())
			    prev_charge_cell.eq(0).trigger('change');
			    input_this.select2().data('select2').\$container.addClass('hide');
			    	
		    });

		    $(document).on('change','.payment_percentage_dropdown',function()
		    {
		   		var id = $(this).val();
		   		var input_this = $(this);
		   		//alert($(this).find(':selected').text());
		   		var type= $(this).find(':selected').attr('data-type')
		   		//alert(type);
		   		console.log('id :'+id);
		   		var charge_cell = $(this).parent().nextAll();
		   		var prev_charge_cell = $(this).prevAll();
		   		console.log(charge_cell.eq(0).children('input'));
		   		prev_charge_cell.eq(0).removeClass('hide');
			    prev_charge_cell.eq(0).val(input_this.find(':selected').text())
			    prev_charge_cell.eq(0).trigger('change');
			    input_this.select2({minimumResultsForSearch: -1,}).data('select2').\$container.addClass('hide');
			    	
		    });


		    $(document).on('click','#add_credit_btn',function()
		    {
		   		//alert('clicked');
		    	var htmlCode = '".BookingsItems::getGroupPaymentmethodsDropdown($bookingGroupModel->id)."';
		    	//alert(htmlCode);
		    	if(htmlCode == '')
		    	{
		    		alert('No payment methods for this destination');
		    	}
		    	else
		    	{
		    		$('#payments-row').append(htmlCode);
        			$('.calendar').datepicker(); 
        			//$('.payment_method_dropdown');
        			// $('.total').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        			//$('.calendar').datepicker();
        			$('[data-toggle=\'tooltip\']').tooltip(); 
        			$('.payment_method_dropdown').select2({
		    									placeholder: 'Select an option',
  												//allowClear: true,
		    									width: '90%',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });
					$('.payment_percentage_dropdown').select2({
												minimumResultsForSearch: -1,
		    									placeholder: 'Select an option',
  												//allowClear: true,
		    									width: '90%',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 }); 
		    	}
		    	$('.payment-switch').bootstrapSwitch();
        		
        		//alert('htmlCode');
		    });

		    ////////////////////////////// Testing Function ////////////////////////////////////
		    $(document).ready(function(){
		    // 	var last_row = $('#payments_table').find('tr').last();
      //  		var last_td = last_row.find('td');
       		
		    // console.log('width : '+last_td.eq(7).find('input').width());
		    // console.log($('.total').width());
		    // console.log(document.getElementById('total').offsetWidth);
		    	////////// Payments Row //////////////

		    	var htmlCode = '".BookingsItems::getGroupPaymentmethodsDropdown($bookingGroupModel->id)."';
        		$('#payments-row').append(htmlCode);
        		$('.payment-switch').bootstrapSwitch();

        		$('.calendar').datepicker();
        		//$('.total').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        		$('.payment_method_dropdown').select2({
		    									placeholder: 'Select an option',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2().data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });
				$('.payment_percentage_dropdown').select2({
												minimumResultsForSearch: -1,
		    									placeholder: 'Select an option',
		    									containerCssClass : 'hide custom_class_payment' })
		    									.on('select2:close', function (e) {
									    			// log('select2:close', e);
									    			console.log('event triggered');
									    			$(this).select2({
												minimumResultsForSearch: -1,}).data('select2').\$container.addClass('hide');
									    			$(this).prev().removeClass('hide');
									    			console.log($(this));
									    			// elements.eq(1).select2().data('select2').\$container.addClass('hide');
									    			// elements.eq(0).removeClass('hide');
									    	 });

        		$('[data-toggle=\'tooltip\']').tooltip(); 

	    	    
		    });


		    ///////////////////////////////////// On change of Save and Delete of Payment and Charges Rows //////////////////////////////////


		    /////////////////////// Payments / Credits Button //////////////////////////////

		    $(document).on('click','.save-dynamic-row-payments',function(){
		    	var row = $(this).parent().parent();
		    	var id;
		    	console.log(row);
		    	if(row.attr('id') == undefined)
		    	{
		    		id = 0;
		    	}
		    	else
		    	{
		    		id = row.attr('id');
		    	}
		    	console.log('id: '+id);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');


                if(tds.hasClass('hasSelect'))
                {
                   // alert('has Select');
                    var object = 
                    {
                    	'booking_group_id' : ".$bookingGroupModel->id.",
                    	'id'	: id,
                    	'type'	: 2,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'date'    : tds.eq(1).children('input').first().val(),
                        'description' : tds.eq(2).find('input').first().val(),
                        'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                        'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                        'deposit_percentage' : tds.eq(4).find('input').val(),
                        // 'price'        : tds.eq(4).children('input').first().val(),
                        // 'vat'          : tds.eq(5).children('input').first().val(),
                        // 'tax'          : tds.eq(6).children('input').first().val(),
                    };
                }
                else
                {
                   // alert('No Select');
                    var object = 
                    {
                    	'booking_group_id' : ".$bookingGroupModel->id.",
                    	'id'	: id,
                    	'type'	: 2,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'date'    : tds.eq(1).children('input').first().val(),
                        'description' : tds.eq(2).find('input').first().val(),
                        'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                        'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                        'deposit_percentage' : tds.eq(4).find('input').val(),
                        // 'price'        : tds.eq(4).children('input').first().val(),
                        // 'vat'          : tds.eq(5).children('input').first().val(),
                        // 'tax'          : tds.eq(6).children('input').first().val(),
                    };
                }
                
               	//tds.eq(7).children('input').first().trigger('change');
                $.ajax(
		        {
		            type: 'POST',
		            data: {
		            	credit : object,
		            },
		            url: '$add_payment_entry',
		            async: true,
		            beforeSend: function(){
		     			App.blockUI({target:'.table_payments_div', animate: !0});
				    },
		            success: function(data)
		            {
		               data = JSON.parse(data);
		               if(data.id == 0)
		               {
		               		alert('price is 0');
		               }
		               else
		               {
		               		//alert('Record Updated');
		               		row.attr('id',data.id);
		               		$('#total_charges').val(data.total_debit);
		               		$('#total_payment').val(data.total_credit);
		               		$('#total_due').empty().html(data.balance_due);
		               		$('#balance_due').empty().html(data.total_debit);
		               		$('#table_charges_div').empty().html(data.charges);
		               		if(data.balance_due == 0)
		               		{
		               			$('#total_due').css('color','green');
		               			$('#balance_due').css('color','green');
		               		}
		               		else if(data.balance_due < 0)
		               		{
		               			$('#total_due').css('color','orange');
		               			$('#balance_due').css('color','orange');
		               		}
		               		else
		               		{
		               			$('#total_due').css('color','black');
		               			$('#balance_due').css('color','black');
		               		}

		               		global_payment = data.total_debit;
		               		$('#add_credit_btn').trigger('click');

		               		tds.eq(8).html('');
		               		tds.eq(8).append('<a href=\"javascript:void(0)\" class=\"btn btn-icon-only red delete-dynamic-row-payments\" title=\"Delete\" data-toggle=\"tooltip\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></a>');
		               		var last_row = $('#payments_table').find('tr').last();
		               		var last_td = last_row.find('td');

		               		last_td.eq(7).children('input').first().val(data.balance_due);
		               		
		               		if(tds.eq(3).find('input').bootstrapSwitch('state'))
		               		{
		               			tds.eq(3).empty().html('<span class=\"label label-info\">Deposit</span>');
		               			var p_age = tds.eq(4).children('select').find(':selected').val();
		               			tds.eq(4).empty().append(data.percentage);
		               		}
		               		else
		               		{
		               			tds.eq(3).empty().html('<span class=\"label label-default\">Normal</span>');
		               		}
		               		
		               }
		            },
		            complete: function()
		            {
		                App.unblockUI('.table_payments_div');
		                // alert('unblocked')
		            },
		            error: function()
		            {
		                alert('error');
		            }
		        });

		    });


		    $(document).on('click','.delete-dynamic-row-payments',function(){
		    	// if (confirm('Are you sure, You want to remove')) {
			    var row = $(this).parent().parent();
			    	var id;
			    	console.log(row);
			    	if(row.attr('id') == undefined)
			    	{
			    		id = 0;
			    		row.remove();
			    	}
			    	else
			    	{
			    		id = row.attr('id');
			    		$.ajax(
				        {
				            type: 'POST',
				            data: {
				            	id : id,
				            	booking_group_id : ".$bookingGroupModel->id.",
				            },
				            url: '$delete_payment_entry',
				            async: true,
				            beforeSend: function(){
				     			App.blockUI({target:'.table_payments_div', animate: !0});
						    },
				            success: function(data)
				            {
				            	data = JSON.parse(data);
				               	if(data.id == 0)
				               	{
				               		alert('Error');
				               	}
				               	else
				               	{
				               		row.remove();
				               		//alert('Record Updated');
				               		$('#total_charges').val(data.total_debit);
				               		$('#total_payment').val(data.total_credit);
				               		$('#total_due').empty().html(data.balance_due);
				               		$('#balance_due').empty().html(data.balance_due);
				               		$('#table_charges_div').empty().html(data.charges);
				               		if(data.balance_due == 0)
				               		{
				               			$('#total_due').css('color','green');
				               			$('#balance_due').css('color','green');
				               		}
				               		else if(data.balance_due < 0)
				               		{
				               			$('#total_due').css('color','orange');
				               			$('#balance_due').css('color','orange');
				               		}
				               		else
				               		{
				               			$('#total_due').css('color','black');
				               			$('#balance_due').css('color','black');
				               		}
				               		global_payment = data.total_debit;
				               		var last_row = $('#payments_table').find('tr').last();
				               		var last_td = last_row.find('td');
				               		last_td.eq(7).children('input').first().val(data.balance_due);
				               		if(last_td.eq(3).find('input').bootstrapSwitch('state'))
				               		{
				               			updatePercentage(last_row);
				               		}
				               		
				               	}
				            	
				               	
				            },
				            complete: function()
				            {
				                App.unblockUI('.table_payments_div');
				                // alert('unblocked')
				            },
				            error: function()
				            {
				                alert('error');
				            }
				        });
			    	}
		    		$(this).parent().parent().remove();
				// } 
		    });

		    //////////////////////////////////////// NEW JS TO Handel dropdown and input box ///////////////////////////////////
		    $(document).on('click','.show_dropdown',function(){
		    	//alert('clicked_on');
		    	var elements = $(this).parent().nextAll();
		    	console.log($(this).parent().nextAll());
		    	elements.eq(0).addClass('hide');
		    	console.log(elements.eq(1).data('select2'));
		    	//$(elements.eq(1).data('select2').options.options.containerCssClass).removeClass('hide');
		    	//$(elements.eq(1).select2('container')).removeClass('hide');
		    	elements.eq(1).select2().data('select2').\$dropdown.removeClass('hide');
		    	//elements.eq(1).select2().data('select2').\$container.addClass('custom_class_add_on');
		    	elements.eq(1).select2().select2('open');

		    	// elements.eq(1).select2().select2('open');
		    	// elements.eq(1).on('select2:close', function (e) {
		    	// 		// log('select2:close', e);
		    	// 		console.log('event triggered');
		    	// 		elements.eq(1).select2().data('select2').\$container.addClass('hide');
		    	// 		elements.eq(0).removeClass('hide');
		    	//  });
		    	console.log(elements.eq(1).data('select2'));
		    });

		    $(document).on('click','.show_dropdown2',function(){
		    	//alert('clicked_on');
		    	var elements = $(this).parent().nextAll();
		    	console.log($(this).parent().nextAll());
		    	elements.eq(0).addClass('hide');
		    	console.log(elements.eq(1).data('select2'));
		    	//$(elements.eq(1).data('select2').options.options.containerCssClass).removeClass('hide');
		    	//$(elements.eq(1).select2('container')).removeClass('hide');
		    	elements.eq(1).select2().data('select2').\$dropdown.removeClass('hide');
		    	//elements.eq(1).select2().data('select2').\$container.addClass('custom_class_add_on');
		    	elements.eq(1).select2({minimumResultsForSearch: -1,}).val('').select2('open');

		    	// elements.eq(1).select2().select2('open');
		    	// elements.eq(1).on('select2:close', function (e) {
		    	// 		// log('select2:close', e);
		    	// 		console.log('event triggered');
		    	// 		elements.eq(1).select2().data('select2').\$container.addClass('hide');
		    	// 		elements.eq(0).removeClass('hide');
		    	//  });
		    	console.log(elements.eq(1).data('select2'));
		    });


		    $(document).on('change','.payment_description',function(){
		    	//alert('changed');
		    	console.log('payment_description change');
		    	var row = $(this).parent().parent().parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
		    	var total = tds.eq(7).children('input').first().inputmask('unmaskedvalue');
		    	console.log(tds.eq(8).children('button'));

		    	if(total != '' && total != 0 && $(this).val() != '' )
		    	{
		    		console.log('payment if');
		    		tds.eq(8).children('button').removeAttr('disabled');
		    	}
		    	else
		    	{
		    		console.log('payment else');
		    		tds.eq(8).children('button').attr('disabled',true);
		    	}
		    });

		    $(document).on('change','.total',function(){
		    	console.log('total change');
		    	var row = $(this).parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
		    	var object;

		    	var tds = row.find('td');
		    	var total = $(this).val();
		    	console.log(tds.eq(2).find('input').first().val());
		    	if(total != '' && total != 0 && tds.eq(2).find('input').first().val() != '' )
		    	{
		    		console.log('total if');
		    		tds.eq(8).children('button').removeAttr('disabled');
		    	}
		    	else
		    	{
		    		console.log('total else');
		    		tds.eq(8).children('button').attr('disabled',true);
		    	}
		    	if(tds.eq(3).find('input').bootstrapSwitch('state'))
		    	{
		    		updatePercentage(row);
		    	}
		    	console.log(tds.eq(3).find('input').bootstrapSwitch('state'));
		    });
		    // $('.payment-switch').on('switchChange.bootstrapSwitch', function (event, state) {
		    	
		    // 	var row = $(this).parent().parent();
		    // 	var id;
		    // 	console.log(row);

		    // 	var tds = row.find('td');
		    // 	console.log(tds);
		    // 	var object;
		    // }); 

			$(document).on('switchChange.bootstrapSwitch', '.payment-switch', function (event, state) {
				console.log('total change');
		    	var row = $(this).parent().parent().parent().parent();
		    	var id;
		    	console.log(row);

		    	var tds = row.find('td');
		    	console.log(tds);
			    if ($(this).bootstrapSwitch('state')) {     
			        console.log($(this).data('on-text'));
			        var percentage =  tds.eq(4).find('input').val();
			        var amount =  tds.eq(7).find('input').val();
					console.log(percentage);

					//if(amount != '' && amount > 0)
					if(amount != '')
					{
						updatePercentage(row);
					}
					else
					{
						if(percentage != '' && percentage > 0)
						{
							updateAmount(row);
						}
						
					}
			        
			        tds.eq(4).children('.deposit_percentage').removeClass('hide');
			    }else {
			        console.log($(this).data('off-text'));
			        tds.eq(4).children('.deposit_percentage').addClass('hide');
			        tds.eq(7).children('input').first().val(global_payment);
			    }
			});

			$(document).on('change','.deposit_amount',function(){
				
				var row = $(this).parent().parent().parent().parent();
				console.log(row.attr('class'));
				console.log('row :'+row);
				updateAmount(row);
			})

			function updateAmount(row)
			{	
				var tds = row.find('td');
				console.log('tds :'+tds);
				var percentage =  tds.eq(4).find('input').val();
				console.log(percentage);
				if(percentage != '' && percentage > 0)
				{
					$.ajax(
			        {
			            type: 'POST',
			            data: {
			            	percentage : percentage,
			            	booking_group_id : ".$bookingGroupModel->id.",
			            },
			            url: '$calculate_deposit',
			            async: true,
			            beforeSend: function(){
			     			App.blockUI({target:'.table_payments_div', animate: !0});
					    },
			            success: function(data)
			            {
			               data = JSON.parse(data);
			              
			               tds.eq(7).children('input').first().val(data.deposit_amount);			               		
			            },
			            complete: function()
			            {
			                App.unblockUI('.table_payments_div');
			                // alert('unblocked')
			            },
			            error: function()
			            {
			                alert('error');
			            }
			        });	
				}
					

			}

			function updatePercentage(row)
			{	
				var tds = row.find('td');
				console.log('tds :'+tds);
				var amount =  tds.eq(7).find('input').val();
				console.log(amount);
				if(amount != '' && amount > -1)
				{
					$.ajax(
			        {
			            type: 'POST',
			            data: {
			            	amount : amount,
			            	booking_group_id : ".$bookingGroupModel->id.",
			            },
			            url: '$calculate_percentage',
			            async: true,
			            beforeSend: function(){
			     			//App.blockUI({target:'.table_payments_div', animate: !0});
					    },
			            success: function(data)
			            {
			               data = JSON.parse(data);
			              
			              tds.eq(4).find('input').first().val(data.deposit_percentage);			               		
			            },
			            complete: function()
			            {
			                //App.unblockUI('.table_payments_div');
			                // alert('unblocked')
			            },
			            error: function()
			            {
			                alert('error');
			            }
			        });	
				}
					

			}


		    
	");

	

?>
<?php

use yii\db\Migration;

class m161209_070657_create_open_hours_exception extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `providers_open_hours_exceptions` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `date` int(11) DEFAULT NULL,
          `opening` time DEFAULT NULL,
          `closing` time DEFAULT NULL,
          `switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
          `provider_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `FK_providers_open_hours_exceptions_provider_id` (`provider_id`),
          CONSTRAINT `FK_providers_open_hours_exceptions_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    public function down()
    {
        echo "m161209_070657_create_open_hours_exception cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

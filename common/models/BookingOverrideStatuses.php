<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_override_statuses".
 *
 * @property integer $id
 * @property string $label
 * @property string $background_color
 * @property string $text_color
 * @property integer $type
 */
class BookingOverrideStatuses extends \yii\db\ActiveRecord
{
    const TYPE_CUSTOM = 1;
    const TYPE_DEFAULT = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_override_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['label', 'background_color', 'text_color'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'background_color' => 'Background Color',
            'text_color' => 'Text Color',
            'type' => 'Type',
        ];
    }
}

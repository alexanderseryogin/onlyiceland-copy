<?php

use yii\db\Migration;

class m170822_075645_alter_table_bookable_item_add_field_bookable_item_long_nots extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookable_items`   
                          ADD COLUMN `bookable_item_default_notes` LONGTEXT NULL AFTER `bookable_item_notes`;
                        ');
    }

    public function down()
    {
        echo "m170822_075645_alter_table_bookable_item_add_field_bookable_item_long_nots cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

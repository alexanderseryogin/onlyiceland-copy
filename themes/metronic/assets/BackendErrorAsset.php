<?php

namespace metronic\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BackendErrorAsset extends AssetBundle
{
    public $sourcePath = '@metronic/resources';
    public $css = [
        // GLOBAL MANDATORY STYLES
        'global/plugins/font-awesome/css/font-awesome.min.css',
        'global/plugins/simple-line-icons/simple-line-icons.min.css',
        'global/plugins/bootstrap/css/bootstrap.min.css',
        'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',

        // THEME GLOBAL STYLES
        'global/css/components.min.css',
        'global/css/plugins.min.css',
        
        //Login page
        'pages/css/error.min.css',
    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [

        // BEGIN CORE PLUGINS 
        //'global/plugins/jquery.min.js',
        'global/plugins/bootstrap/js/bootstrap.min.js',
        'global/plugins/js.cookie.min.js',
        'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'global/plugins/jquery.blockui.min.js',
        'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',

        // BEGIN THEME GLOBAL SCRIPTS
        'global/scripts/app.min.js',

    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
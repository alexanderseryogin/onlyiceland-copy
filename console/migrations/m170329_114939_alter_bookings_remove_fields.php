<?php

use yii\db\Migration;

class m170329_114939_alter_bookings_remove_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` DROP COLUMN `provider_id`, DROP COLUMN `estimated_arrival_time_id`, DROP COLUMN `rate_conditions`, DROP COLUMN `arrival_date`, DROP COLUMN `departure_date`, DROP COLUMN `no_of_nights`, DROP COLUMN `created_at`, DROP COLUMN `updated_at`, DROP COLUMN `deleted_at`, CHANGE `user_id` `group_user_id` INT(11) NULL COMMENT 'fk of users', DROP INDEX `FK_bookings_provider_id`, DROP INDEX `FK_bookings_estimated_arrival_time_id`, DROP FOREIGN KEY `FK_bookings_estimated_arrival_time_id`, DROP FOREIGN KEY `FK_bookings_provider_id`; 
        ");
    }

    public function down()
    {
        echo "m170329_114939_alter_bookings_remove_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161028_044934_create_table_attraction_types_fields extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `attraction_type_fields`( `id` INT(11) NOT NULL AUTO_INCREMENT, `type` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `at_id` INT(11), `required` TINYINT(1), `label` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_attraction_type_fields_at_id` FOREIGN KEY (`at_id`) REFERENCES `attraction_types`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161028_044934_create_table_attraction_types_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

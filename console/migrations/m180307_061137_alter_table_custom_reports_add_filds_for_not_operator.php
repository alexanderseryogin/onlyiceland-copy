<?php

use yii\db\Migration;

class m180307_061137_alter_table_custom_reports_add_filds_for_not_operator extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_reports`   
                          ADD COLUMN `status_not_operator` INT(11) NULL AFTER `statuses_operator`,
                          ADD COLUMN `flag_not_operator` INT(11) NULL AFTER `flags_operator`,
                          ADD COLUMN `referer_not_operator` INT(11) NULL AFTER `referers_operator`;
                        ');
    }

    public function safeDown()
    {
        echo "m180307_061137_alter_table_custom_reports_add_filds_for_not_operator cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180307_061137_alter_table_custom_reports_add_filds_for_not_operator cannot be reverted.\n";

        return false;
    }
    */
}

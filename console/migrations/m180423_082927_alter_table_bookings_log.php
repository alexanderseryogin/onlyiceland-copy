<?php

use yii\db\Migration;

class m180423_082927_alter_table_bookings_log extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bookings_log`   
                          ADD COLUMN `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP   NULL AFTER `new_value`;
                        ');
    }   

    public function safeDown()
    {
        echo "m180423_082927_alter_table_bookings_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_082927_alter_table_bookings_log cannot be reverted.\n";

        return false;
    }
    */
}

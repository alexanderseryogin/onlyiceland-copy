<?php

use common\models\BookingTypes;
use common\models\BookingStatuses;
use common\models\BookingRules;
use common\models\BookingConfirmationTypes;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\Destinations;

?>

<div class="tab-pane" id="rules">

    <?php
        if(!$booking_rules_model->isNewRecord)
        {
            $this->registerJs("
                
                option = '$booking_rules_model->request_booking_status';
                if(option=='block')
                {
                    $('#block').attr('checked','checked');
                }
                else
                {
                    $('#not_block').attr('checked','checked');
                }

                /* value = '$booking_rules_model->general_booking_cancellation';
                $('#booking_cancellation').val(value).trigger('change');*/
            ");
        }
        else
        {
            $this->registerJs("
                    $('#block').attr('checked','checked');
                ");
        }
    ?>

        “Request” Booking Status

        <div class="mt-radio-inline">
            <label class="mt-radio mt-radio-outline">
                <input type="radio" id="block" name="BookingRules[request_booking_status]" value="block" > Block
                <span></span>
            </label>
            <label class="mt-radio mt-radio-outline">
                <input type="radio" id="not_block" name="BookingRules[request_booking_status]" value="not_block">Do not Block
                <span></span>
            </label>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($booking_rules_model, 'routine_booking_confirmation', [
                'inputOptions' => [
                    'id' => 'routine-booking-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
                ])->dropdownList(ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name'),['prompt'=>'Select a Confirmation Type']) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($booking_rules_model, 'nearterm_booking_confirmation', [
                'inputOptions' => [
                    'id' => 'nearterm-booking-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
                ])->dropdownList(ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name'),['prompt'=>'Select a Confirmation Type']) ?>
            </div>
        </div>

        <label class=" control-label">Near-term Bookings Allowed in Advance</label>
        <div class="form-group">
            <input id="near-term-bookings" value="<?=$booking_rules_model->nearterm_booking_advance ?>" type="text" name="BookingRules[nearterm_booking_advance]" />
        </div>

        <label class=" control-label">Same Day Booking Cutoff Time</label>
        <div class="form-group">
            <input id="same-day-bookings" value="<?=$booking_rules_model->same_day_booking_cutoff ?>" type="text" name="BookingRules[same_day_booking_cutoff]" />
        </div>

        <div class="row">
            <div class="col-xs-4">
                <label class="control-label" >General Booking Cancellation</label>
                <div class="form-group" >
                    <select style="width: 100%;" class="form-control" id="booking_cancellation" name="BookingRules[general_booking_cancellation]" >
                        <?php
                            foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                            {
                                echo '<option value="'.$key.'">'.$value.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-4">
                <?= $form->field($booking_rules_model, 'new_booking_type', [
                'inputOptions' => [
                    'id' => 'new-booking-type-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
                ])->dropdownList(ArrayHelper::map(BookingTypes::find()->all(),'id','label'),['prompt'=>'Select a Type']) ?>
            </div>
            <div class="col-xs-4">
                <?= $form->field($booking_rules_model, 'new_booking_status', [
                'inputOptions' => [
                    'id' => 'new-booking-status-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
                ])->dropdownList(ArrayHelper::map(BookingStatuses::find()->all(),'id','label'),['prompt'=>'Select a Status']) ?>
            </div>
        </div>
</div>
<?php

use yii\db\Migration;

class m161028_122729_create_table_attraction_type_fields_data extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `attraction_type_fields_data`( `id` INT(11) NOT NULL AUTO_INCREMENT, `attraction_id` INT(11), `atf_id` INT(11), `value` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_att_field_data_attraction_id` FOREIGN KEY (`attraction_id`) REFERENCES `attractions`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_att_field_data_atf_id` FOREIGN KEY (`atf_id`) REFERENCES `attraction_type_fields`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
            ");
    }

    public function down()
    {
        echo "m161028_122729_create_table_attraction_type_fields_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

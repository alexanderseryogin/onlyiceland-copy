<?php

use yii\db\Migration;

class m170322_130812_alter_bookings_items_upsell extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_upsell_items` ADD COLUMN `extra_bed_quantity` INT(11) NULL AFTER `bookings_items_id`; 
        ");
    }

    public function down()
    {
        echo "m170322_130812_alter_bookings_items_upsell cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

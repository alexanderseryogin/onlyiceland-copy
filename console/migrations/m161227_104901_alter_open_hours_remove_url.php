<?php

use yii\db\Migration;

class m161227_104901_alter_open_hours_remove_url extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours` DROP COLUMN `url`; 
            ALTER TABLE `providers` ADD COLUMN `url` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `description`; 
        ");
    }

    public function down()
    {
        echo "m161227_104901_alter_open_hours_remove_url cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

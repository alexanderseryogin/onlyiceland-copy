<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BookableItems */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookable Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookable-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tab_href' => $tab_href,
        'bed_type' => $bed_type,
        'quantity' => $quantity,
        'amenities' => $amenities,
    ]) ?>

</div>

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\Admin4AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

Admin4AppAsset::register($this);
$path = \Yii::$app->request->BaseUrl . '/themes/metronic';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
	    <meta charset="<?= Yii::$app->charset ?>">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="onlyiceland site description" name="description" />
        <meta content="M. Saqib Zafar" name="author" />

	    <?= Html::csrfMetaTags() ?>
	    <title><?= Html::encode($this->title) ?></title>

        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <link rel="shortcut icon" href="favicon.ico" /> 
        <style>
            .summary {
                float:right;
                margin-bottom:5px;
            }
        </style>
		<?php $this->head() ?>
    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->beginBody() ?>
        <!-- BEGIN HEADER -->
        <?= $this->render('header_nav')?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?= $this->render('sidebar')?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1><?= $this->params['title'] ?>
                            <small><?= $this->params['subTitle'] ?></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->

                        <!-- BEGIN PAGE TOOLBAR -->
                        <?php
                            //toolbar for theme settings
                            //echo $this->render('page_toolbar');
                        ?>
                        <!-- END PAGE TOOLBAR -->

                    </div>
                    <!-- END PAGE HEAD-->

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <?= $this->render('breadcrumbs')?>
                    <!-- END PAGE BREADCRUMB -->

                    <!-- BEGIN PAGE BASE CONTENT -->
                    <?= Alert::widget() ?>
                        <?= $content ?>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

            <!-- BEGIN QUICK SIDEBAR -->
            <?php
                //quick_sidebar should also be uncommented in the header_nav.php to use it
                //echo $this->render('quick_sidebar');
            ?>
            <!-- END QUICK SIDEBAR -->

        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <?= $this->render('footer')?>
        <!-- END FOOTER -->

        <!-- BEGIN QUICK NAV -->
        <?php
            //echo $this->render('quick_nav'); 
        ?>
        <!-- END QUICK NAV -->

        <!--[if lt IE 9]>
		<script src="<?php echo $path; ?>/assets/global/plugins/respond.min.js"></script>
		<script src="<?php echo $path; ?>/assets/global/plugins/excanvas.min.js"></script> 
		<script src="<?php echo $path; ?>/assets/global/plugins/ie8.fix.min.js"></script> 
		<![endif]-->
    	<?php $this->endBody() ?>
    </body>
	
</html>
<?php $this->endPage() ?>
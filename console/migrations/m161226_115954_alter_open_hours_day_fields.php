<?php

use yii\db\Migration;

class m161226_115954_alter_open_hours_day_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours` DROP COLUMN `mon_opening_second`, DROP COLUMN `mon_closing_second`, DROP COLUMN `mon_split_service`, DROP COLUMN `tue_opening`, DROP COLUMN `tue_closing`, DROP COLUMN `tue_switch`, DROP COLUMN `tue_opening_second`, DROP COLUMN `tue_closing_second`, DROP COLUMN `tue_split_service`, DROP COLUMN `wed_opening`, DROP COLUMN `wed_closing`, DROP COLUMN `wed_switch`, DROP COLUMN `wed_opening_second`, DROP COLUMN `wed_closing_second`, DROP COLUMN `wed_split_service`, DROP COLUMN `thu_opening`, DROP COLUMN `thu_closing`, DROP COLUMN `thu_switch`, DROP COLUMN `thu_opening_second`, DROP COLUMN `thu_closing_second`, DROP COLUMN `thu_split_service`, DROP COLUMN `fri_opening`, DROP COLUMN `fri_closing`, DROP COLUMN `fri_switch`, DROP COLUMN `fri_opening_second`, DROP COLUMN `fri_closing_second`, DROP COLUMN `fri_split_service`, DROP COLUMN `sat_opening`, DROP COLUMN `sat_closing`, DROP COLUMN `sat_switch`, DROP COLUMN `sat_opening_second`, DROP COLUMN `sat_closing_second`, DROP COLUMN `sat_split_service`, DROP COLUMN `sun_opening`, DROP COLUMN `sun_closing`, DROP COLUMN `sun_switch`, DROP COLUMN `sun_opening_second`, DROP COLUMN `sun_closing_second`, DROP COLUMN `sun_split_service`, ADD COLUMN `day` INT(11) NULL AFTER `provider_id`, CHANGE `mon_opening` `opening` TIME NULL, CHANGE `mon_closing` `closing` TIME NULL, ADD COLUMN `hours_24` TINYINT(1) NULL AFTER `closing`, CHANGE `mon_switch` `open` TINYINT(1) NULL; 
        ");
    }

    public function down()
    {
        echo "m161226_115954_alter_open_hours_day_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

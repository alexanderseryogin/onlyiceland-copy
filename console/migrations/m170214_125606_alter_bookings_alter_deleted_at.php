<?php

use yii\db\Migration;

class m170214_125606_alter_bookings_alter_deleted_at extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` CHANGE `deleted_at` `deleted_at` INT(11) DEFAULT 0 NULL; ");
    }

    public function down()
    {
        echo "m170214_125606_alter_bookings_alter_deleted_at cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

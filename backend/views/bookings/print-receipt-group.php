<?php
/**
 * @var \common\models\BookingGroup $booking_group
 * @var \common\models\BookingGroup $bookings
 */

    $grand_total = 0;
    $vat = 0;
    $vat_amount = 0;
    $net = 0;
    $gross = 0;
?>

<?php foreach ($bookings as $booking) { ?>
    <div style="page-break-after:always">
        <?= $this->render('print-receipt', ['booking' => $booking]) ;?>
    </div >
<?php } ?>

<h2>For group num. <?= $booking_group->id ;?></h2>

<!-- Charges -->
<table class="table">
    <thead>
        <tr>
            <th>Description</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Net</th>
            <th>VAT %</th>
            <th>VAT</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($booking_group->bookingGroupCharges as $item) { ?>
            <?php
                $grand_total += $item->final_total;
                $vat = $item->vat;
                $vat_amount += $item->vat_amount;
                $net = $item->amount - $item->vat_amount;
                $gross += $item->final_total;
            ?>
            <tr>
                <td>
                    <?= $item->price_description;?>
                </td>
                <td>
                    <?= $item->quantity;?>
                </td>
                <td class="text-right">
                    <?= number_format($item->amount, 0, ',', '.'); ?> kr
                </td>
                <td class="text-right">
                    <?= number_format($net, 0, ',', '.'); ?> kr
                </td>
                <td>
                   <?= $vat;?>%
                </td>
                <td class="text-right">
                    <?= number_format($item->vat_amount, 0, ',', '.'); ?> kr
                </td>
                <td class="text-right">
                    <?= number_format($item->final_total, 0, ',', '.'); ?> kr
                </td>
            </tr>
        <?php } ?>
            <tr>
                <th>Grand Total</th>
                <th colspan="6" class="text-right"><?= number_format($grand_total, 0, ',', '.');?> kr</th>
            </tr>
    </tbody>
</table>

<!-- Payments -->
<table class="table">
    <tbody>
        <?php foreach ($booking_group->bookingGroupPayments as $item) { ?>
            <?php
                $grand_total -= $item->final_total
            ?>
            <tr>
                <td>
                    <?= $item->price_description;?>
                </td>
                <td colspan="6" class="text-right">
                    - <?= number_format($item->final_total, 0, ',', '.'); ?> kr
                </td>
            </tr>
        <?php } ?>
            <tr>
                <th>Outstanding amount</th>
                <th colspan="6" class="text-right"><?= number_format($grand_total, 0, ',', '.');?> kr</th>
            </tr>
    </tbody>
</table>

<hr >
<!-- Summary-->
<table class="table table-no-bordered text-right">
    <tr>
        <td>
            <b>
                VAT %
            </b>
        </td>
        <td>
            <b>
                Net
            </b>
        </td>
        <td>
            <b>
                VAT
            </b>

        </td>
        <td>
            <b>
                Gross
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <?= number_format($vat, 0, ',', '.');?>
        </td>
        <td>
            <?= number_format($net, 0, ',', '.');?> kr
        </td>
        <td>
            <?= number_format($vat_amount, 0, ',', '.');?> kr
        </td>
        <td>
            <?= number_format($gross, 0, ',', '.');?> kr
        </td>
    </tr>
</table>
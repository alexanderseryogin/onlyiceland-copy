<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_group_finance_details".
 *
 * @property integer $id
 * @property integer $booking_group_finance_id
 * @property integer $booking_date_finance_id
 *
 * @property BookingGroupFinance $bookingGroupFinance
 * @property BookingDateFinances $bookingDateFinance
 */
class BookingGroupFinanceDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_group_finance_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_group_finance_id', 'booking_date_finance_id'], 'integer'],
            [['booking_group_finance_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroupFinance::className(), 'targetAttribute' => ['booking_group_finance_id' => 'id']],
            [['booking_date_finance_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingDateFinances::className(), 'targetAttribute' => ['booking_date_finance_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_group_finance_id' => 'Booking Group Finance ID',
            'booking_date_finance_id' => 'Booking Date Finance ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroupFinance()
    {
        return $this->hasOne(BookingGroupFinance::className(), ['id' => 'booking_group_finance_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingDateFinance()
    {
        return $this->hasOne(BookingDateFinances::className(), ['id' => 'booking_date_finance_id']);
    }
}

<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'no-reply@nuna64.com',
    'user.passwordResetTokenExpire' => 3600,
];

<?php

use yii\db\Migration;

class m170714_090509_alter_table_destinations_add_field_active extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `providers`   
                          ADD COLUMN `active` INT(6) NULL AFTER `pricing`;
                        ');
    }

    public function down()
    {
        echo "m170714_090509_alter_table_destinations_add_field_active cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

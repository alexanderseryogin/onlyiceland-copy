<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Destinations;
use common\models\BookableItems;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\BookingStatuses;
use yii\web\View;
use yii\helpers\Url;
use common\models\Rates;
use common\models\BookingDates;
use common\models\BookingTypes;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingsItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bookings');
$this->params['breadcrumbs'][] = $this->title;

$Checkbox = [
    0 => '',
    1 => 'checked'
];

$BookingTypes = [
    0 => 'Active',
    1 => 'Deleted'
];

$filter_flag = 0;

if($filter_flag)
{
    $this->registerJs("
            $('#filters_div').removeClass('hide-filters');
            $('#filters_div').addClass('show-filters');");
}

?>

<style type="text/css">
    .show-filters{ display: block; }
    .hide-filters{ display: none; }
</style>

<div class="bookings-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BOOKING'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
        <div class="pull-right" style="margin: 8px;">
            Filters &nbsp;<input type="checkbox" id="filters_switch" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size="mini" disabled="" <?php echo $Checkbox[$filter_flag]  ?>>
        </div>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'bookings-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-scrollable'],
        'columns' => [

            // [
            //     'attribute' => 'booking_number',
            //     'label' => 'Number',
            //     'value' => function($data)
            //     {
            //         return $data->booking_number;
            //     },
            //     'contentOptions' => function($data)
            //     {
            //         return ['style' => 'min-width:80px; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
            //     },
            //     'headerOptions' =>['style' => 'min-width:80px;'],
            // ],
            [
                'attribute' => 'id',
                'format' =>'raw',
                'label' => 'Number',
                'value' => function($data)
                {
                    
                    $html='';
                    // if(!empty($data->comments) && $data->comments!=NULL)
                    // {
                    //     //$path = Yii::getAlias('@web').'/../uploads/icons/comments_red.PNG';
                    //     $html.='<i id="'.$data->id.'" class="fa fa-commenting-o fa-3x" style="color:red;margin-top:7px;" data="'.$data->comments.'"  data-toggle = "tooltip" title ="'.$data->comments.'",> </i>';

                    // }

                    return $html.$data->id;
                },
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
                'contentOptions' => function($data)
                {
                    $color = isset($data->item->text_color) ? $data->item->text_color : '#000000';
                    $backgroundColor = isset($data->item->background_color) ? $data->item->background_color : '#FFFFFF';
                    return ['style' => 'text-align:right;width:2%; color:'.$color.'; background:'.$backgroundColor.';' ];
                },
                //'headerOptions' =>['style' => 'width:30px;'],
            ],
            [
                'attribute' => 'provider_id',
                'label' => 'Destination',
                'value' => function($data)
                {
                    return $data->provider->name;
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[provider_id]', 
                    $searchModel['provider_id'], 
                    ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Destination', 
                        'id'=>'provider_dropdown', 
                        'class'=>'form-control',
                    ]),
                'headerOptions' =>['style' => 'min-width:300px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'min-width:300px; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            [
                'attribute' => 'item_id',
                'label' => 'Item',
                'value' => function($data)
                {
                    return $data->item->itemType->name;
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[item_id]', 
                    $searchModel['item_id'], 
                    ArrayHelper::map(BookableItems::find()->where(['provider_id' => $searchModel['provider_id']])->all(),'id','itemType.name'), 
                    [
                        'prompt' => 'Select an Item', 
                        'id'=>'item_dropdown', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' => function($data)
                {
                    return ['style' => 'min-width:300px; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
                'headerOptions' =>['style' => 'min-width:300px;'],
            ],
            [
                'header' => 'Arrival<span class="fa fa-arrow-right"></span>Departure',
                'format' => 'raw',
                'value' => function($data)
                {
                    return date('d/m/Y',strtotime($data->arrival_date)).'<span class="fa fa-arrow-right"></span>'.date('d/m/Y',strtotime($data->departure_date));
                },
                'headerOptions' =>['style' => 'min-width:220px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'min-width:220px; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' =>['style' => 'min-width:70px;'],
                'contentOptions' =>['style' => 'min-width:70px;'],
                'template' => '{restore}',

                'buttons' => [
                    'restore' => function ($url, $model) {
                        return Html::a('<i class="fa fa-undo" aria-hidden="true"></i>', Url::to(['restore', 'id'=>$model->id]), 
                        [
                            'title' => 'Restore',
                            'aria-label' => 'Restore',
                            'data-pjax' => '0',
                        ]);
                    }
                ],
                /*'visibleButtons' => [
                    // show update button for item who is master
                    'update' => function ($model, $key, $index) {
                        return $model->master == 1 ? true : false;
                    },
                ]*/
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php

$this->registerJs("
jQuery(document).ready(function() {

    $('#filters_switch').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            $('#filters_div').removeClass('hide-filters');
            $('#filters_div').addClass('show-filters');
        }
        else
        {
            $('#filters_div').removeClass('show-filters');
            $('#filters_div').addClass('hide-filters');
        }
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#item_dropdown').select2({
        placeholder: \"Select an Item\",
        allowClear: true
    });

    $('#rates_dropdown').select2({
        placeholder: \"Select a Rate\",
        allowClear: true
    });

    $('#user_dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true
    });

    $('#status_dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#flag_dropdown').select2({
        placeholder: \"Select a Flag\",
        allowClear: true
    });

    $('#bookings-gridview').on('pjax:end', function() 
    {

        $('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
        });

        $('#item_dropdown').select2({
            placeholder: \"Select an Item\",
            allowClear: true
        });

        $('#rates_dropdown').select2({
            placeholder: \"Select a Rate\",
            allowClear: true
        });

        $('#user_dropdown').select2({
            placeholder: \"Select a User\",
            allowClear: true
        });

        $('#status_dropdown').select2({
            placeholder: \"Select a Status\",
            allowClear: true
        });

        $('#flag_dropdown').select2({
            placeholder: \"Select a Flag\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>

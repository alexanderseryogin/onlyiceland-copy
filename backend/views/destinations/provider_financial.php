<?php

Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
Use common\models\DestinationCategories;
Use common\models\DestinationSubCategories;
Use common\models\Destinations;
use common\models\DestinationsFinancial;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

?>

<div class="tab-pane" id="financial">

    <?php
        if(!$provider_financial_model->isNewRecord)
        {
            $today = date('d/m/Y',$provider_financial_model->license_expiration_date);
        }
        else
            $today = date('d/m/Y'); 
    ?>

    <div class="row">

        <div class="col-xs-2">
            <label class="control-label">License Expiration Date</label>
            <div class="form-group" >
                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?=$today?>" name="DestinationsFinancial[license_expiration_date]" />
            </div>
        </div>

        <div class="col-xs-2">
            <?= $form->field($provider_model, 'provider_category_id', [
            'inputOptions' => [
                'id' => 'provider-category-dropdown',
                'class' => 'form-control',
                'style' => 'width: 100%;'
                ]
            ])->dropdownList(ArrayHelper::map(DestinationCategories::find()->all(),'id','name'),['prompt'=>'Select a Category']) ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($provider_model, 'provider_sub_category_id', [
            'inputOptions' => [
                'id' => 'provider-sub-category-dropdown',
                'class' => 'form-control',
                'style' => 'width: 100%;'
                ]
            ])->dropdownList(ArrayHelper::map(DestinationSubCategories::find()->all(),'id','name'),['prompt'=>'Select a Sub-Category']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-2">
            <?= $form->field($provider_financial_model, 'bank_info', [
                                        'inputOptions' => [
                                            'id' => 'bank_info-input',
                                            'class' => 'form-control'
                                            ]
                                        ])->textInput(['maxlength' => true])->hint(' e.g. xxxx-xx-xxxxxx') ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($provider_financial_model, 'lodging_tax_per_night', [
                'inputOptions' => [
                    'id' => 'lodging_tax_per_night',
                    'class' => 'form-control'
                    ]
                ])->textInput(['maxlength' => true])->hint(' e.g. 9.999') ?>

            <input type="text" value="<?=$provider_financial_model->lodging_tax_per_night?>" name="DestinationsFinancial[server_lodging_tax_per_night]" hidden>
        </div>
        <div class="col-xs-2" >
            <?= $form->field($provider_financial_model, 'vat_rate', [
                                        'inputOptions' => [
                                            'id' => 'vat_rate-input',
                                            'class' => 'form-control'
                                            ]
                                        ])->textInput(['maxlength' => true])->hint(' e.g. xx,xx') ?>

            <input type="text" value="<?=$provider_financial_model->vat_rate?>" name="DestinationsFinancial[server_vat_rate]" hidden>
        </div>
    </div>

    <?php
        if(!$provider_financial_model->isNewRecord && !empty($provider_financial_model->license_image))
        {
            $path = Yii::getAlias('@web') . '/../uploads/Destinations/'.$provider_model->id.'/licenses';
            $file = $path . '/'.$provider_financial_model->license_image;
            echo '<div> Previous License Image: <a target="_blank" href='.$file.' download><i style="margin-top:10px;" class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a></div><br>';
        }
    ?>

    <?= $form->field($provider_financial_model, 'pdfFile')->fileInput() ?>

</div>
<?php
namespace backend\controllers;

use Yii;
use backend\components\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class LogsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
        ];
    }

    public function actionAuditLog()
    {
        return $this->render('log');
        // echo "here";
        // exit();
    }
}
<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-practical-a-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'baseUrl' => '@realspaces/resources',
                //'basePath' => '@app/themes/basic',
                'pathMap' => [
                    '@frontend/views' => '@realspaces/views'
                ]
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                /*routing for pretty url*/
                'region/<region_name:\w+>/<town_name:\w+>' => 'search/iceland-town-destinations',
                'region/<region_name:\w+(-\w+)*>' => 'search/region-destinations',
            ],
        ],
    ],
    'modules' => [
        'provider-admin' => [
            'class' => 'frontend\modules\admin\property',
        ],
    ],
    'params' => $params,
];

<?php

use yii\db\Migration;

class m170227_084119_alter_booking_items extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookings_items` DROP COLUMN `rates_id`, DROP INDEX `bookings_items_rates_id`, DROP FOREIGN KEY `bookings_items_rates_id`; 
            CREATE TABLE `bookings_items_rates`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `bookings_item_id` INT(11) UNSIGNED, `rates_id` INT(11), `pricing_type` TINYINT(1), `no_of_children` INT(11), `no_of_adults` INT(11), `single` INT(11), `double` INT(11), `triple` INT(11), `quad` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_bookings_items_rates_booking_item_id` FOREIGN KEY (`bookings_item_id`) REFERENCES `bookings_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_bookings_items_rates_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
            ALTER TABLE `bookings_discount_codes` DROP COLUMN `booking_id`, ADD COLUMN `price` INT(11) NULL AFTER `discount_id`, ADD COLUMN `bookings_items_rates_id` INT(11) UNSIGNED NULL AFTER `price`, DROP INDEX `FK_bookings_discount_codes_booking_id`, DROP FOREIGN KEY `FK_bookings_discount_codes_booking_id`, ADD CONSTRAINT `FK_bookings_discount_codes_bookings_items_rates_id` FOREIGN KEY (`bookings_items_rates_id`) REFERENCES `bookings_items_rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE; 
            ALTER TABLE `bookings_upsell_items` DROP COLUMN `booking_id`, ADD COLUMN `price` INT(11) NULL AFTER `upsell_id`, ADD COLUMN `bookings_items_rates_id` INT(11) UNSIGNED NULL AFTER `price`, DROP INDEX `FK_bookings_upsell_items_booking_id`, DROP FOREIGN KEY `FK_bookings_upsell_items_booking_id`, ADD CONSTRAINT `FK_bookings_upsell_items_bookings_items_rates_id` FOREIGN KEY (`bookings_items_rates_id`) REFERENCES `bookings_items_rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE; 
            ALTER TABLE `bookings_items` ADD COLUMN `total` INT(11) NULL AFTER `confirmation_id`; 

            ALTER TABLE `bookings_items` ADD COLUMN `rates_id` INT(11) NULL AFTER `confirmation_id`, ADD COLUMN `pricing_type` TINYINT(1) NULL AFTER `rates_id`, ADD COLUMN `no_of_children` INT(11) NULL AFTER `pricing_type`, CHANGE `total` `no_of_adults` INT(11) NULL AFTER `no_of_children`, ADD COLUMN `single` INT(11) NULL AFTER `no_of_adults`, ADD COLUMN `double` INT(11) NULL AFTER `single`, ADD COLUMN `triple` INT(11) NULL AFTER `double`, ADD COLUMN `quad` INT(11) NULL AFTER `triple`, ADD CONSTRAINT `FK_bookings_items_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 

            ALTER TABLE `bookings_upsell_items` CHANGE `bookings_items_rates_id` `bookings_items_id` INT(11) UNSIGNED NULL; 
            ALTER TABLE `bookings_upsell_items` ADD CONSTRAINT `FK_bookings_upsell_items_bookings_items_id` FOREIGN KEY (`bookings_items_id`) REFERENCES `bookings_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, DROP FOREIGN KEY `FK_bookings_upsell_items_bookings_items_rates_id`; 

            ALTER TABLE `bookings_discount_codes` CHANGE `bookings_items_rates_id` `bookings_items_id` INT(11) UNSIGNED NULL; 
            ALTER TABLE `bookings_discount_codes` ADD CONSTRAINT `FK_bookings_discount_codes_bookings_items_id` FOREIGN KEY (`bookings_items_id`) REFERENCES `bookings_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, DROP FOREIGN KEY `FK_bookings_discount_codes_bookings_items_rates_id`; 
            DROP TABLE `bookings_items_rates`; 
            ALTER TABLE `bookings_items` DROP COLUMN `triple`, DROP COLUMN `quad`, CHANGE `single` `person_no` INT(11) NULL, CHANGE `double` `person_price` INT(11) NULL; 
            ALTER TABLE `bookings_items` ADD COLUMN `total` INT(11) NULL AFTER `person_price`; 
        "); 
    }

    public function down()
    {
        echo "m170227_084119_alter_booking_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

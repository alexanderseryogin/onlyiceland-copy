<?php
/* @var $this yii\web\View */
$this->title = 'Standard Reports';
?>
<div class="standard-report-top" id="standard-reports-top">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Select Date Range</span>
                    </div>
                    <ul class="nav nav-tabs standard-report-top">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body destination_portlet">
                        <div class="tab-content">
	                    	<div class="tab-pane active" id="guests-housekeeping">
		                        <?= $this->render('header', [
							    	// 'model' => $model,
							    	// 'viewDetailsRecord' => $viewDetailsRecord,
							    	// 'destination_items' => $destination_items,
							    	// 'check'             => $check
							    ]) ?>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>


<div class="guests-portlet" id="guests-portlet">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Guests & Housekeeping</span>
                    </div>
                    <ul class="nav nav-tabs booking_tabs">
                        <!-- <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->
  

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body destination_portlet">
                        <div class="tab-content">
	                    	<div class="tab-pane active" id="guests-housekeeping">
		                        <?= $this->render('guests-housekeeping', [
							    	// 'model' => $model,
							    	// 'viewDetailsRecord' => $viewDetailsRecord,
							    	// 'destination_items' => $destination_items,
							    	// 'check'             => $check
							    ]) ?>
							</div>
						</div>
					</div>   
                </div>
            </div>
        </div>
    </div>
</div>

<div class="occupancy-portlet" id="occupancy-portlet">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Occupancy</span>
                    </div>
                    <ul class="nav nav-tabs booking_tabs">
                        <!-- <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->
  

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body destination_portlet">
                        <div class="tab-content">
							<div class="tab-pane active" id="occupancy">
		                        <?= $this->render('occupancy', [
                                    // 'model' => $model,
                                    // 'viewDetailsRecord' => $viewDetailsRecord,
                                    // 'destination_items' => $destination_items,
                                    // 'check'             => $check
                                ]) ?>
							</div>
							
						</div>
					</div>   
                </div>
            </div>
        </div>
    </div>
</div>

<div class="standard-report" id="standard-reports">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Financial</span>
                    </div>
                    <ul class="nav nav-tabs booking_tabs">
                        <!-- <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->
  

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body destination_portlet">
                        <div class="tab-content">
							<div class="tab-pane active" id="financial">
		                        <?= $this->render('financial', [
							    	// 'model' => $model,
							    	// 'viewDetailsRecord' => $viewDetailsRecord,
							    	// 'destination_items' => $destination_items,
							    	// 'check'             => $check
							    ]) ?>
							</div>
						</div>
					</div>   
                </div>
            </div>
        </div>
    </div>
</div>

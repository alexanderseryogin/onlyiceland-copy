<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .user-login-5 .login-container > .login-content p {
        color: #ED6B75;
        font-size: 15px;
        line-height: 22px;
    }
    .login-info {
        color: #a0a9b4;
        font-size: 15px;
        line-height: 22px;
    }
    .user-login-5 .login-container > .login-content > .login-form .form-control {
        margin-bottom: 5px;
    }

    .pretty-input {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: #a0a9b4 #a0a9b4 -moz-use-text-color;
        border-image: none;
        border-style: none none solid;
        border-width: medium medium 1px;
        color: #868e97;
        font-size: 14px;
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 5px 0;
        width: 100%;
    }
    .user-login-5 .alert {
        margin-top: 5px;
    }
</style>
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <img class="login-logo login-6" style="max-height:50px;" src="<?= Yii::getAlias('@web')?>/assets_b/img/oi_logo.png" />
            <div class="login-content">
                <h1><?= Html::encode($this->title) ?></h1>
                <span class="login-info"> Please choose your new password: </span>
                <div class="alert alert-info">
                    Password must contain one or more uppercase, lowercase and special characters, and at least one number, and must meet minimum strength requirements.
                </div>
                <?= Alert::widget() ?>
                
                <!-- BEGIN FORGOT PASSWORD FORM -->
                <?php $form = ActiveForm::begin([
                    'id' => 'reset-password-form',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'tag'=>'span'
                        ]
                    ]
                ]); ?>
                    <br>
                    <?= $form->field($model, 'password')->passwordInput([
                                //'required' => true,
                                'autofocus' => true,
                                'autocomplete' => "off",
                                'placeholder' => 'New Password',
                                'id'=>'password-input',
                                'class' => "form-control form-control-solid placeholder-no-fix form-group pretty-input"
                            ]) ?>

                    <div class="form-actions">
                        <a href="<?= \yii\helpers\Url::to(['/site/login']) ?>" class="btn blue btn-outline">
                            Back
                        </a>
                        <?= Html::submitButton('Reset', ['class' => 'btn blue uppercase pull-right', 'id' => 'reset', 'disabled' => true]) ?>
                    </div>
                <?php ActiveForm::end(); ?>
                <!-- END FORGOT PASSWORD FORM -->

            </div>
            <?= $this->render('_login_footer') ?>
        </div>
        <div class="col-md-6 bs-reset">
            <div class="login-bg"> 
                
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs("
var initialized = false;
var input = $('#password-input');

input.keydown(function () {
    if (initialized === false) {
        // set base options
        input.pwstrength({
            raisePower: 1.4,
            minChar: 8,
            verdicts: [\"Weak\", \"Normal\", \"Medium\", \"Strong\", \"Very Strong\"],
            scores: [17, 26, 40, 50, 60],
            common: {
                onKeyUp: function (evt, data) {
                    if(data.score >= 50) {
                        $('#reset').prop('disabled', false);
                    }
                    else{
                        $('#reset').prop('disabled', true);
                    }
                }
            }
        });

        // add your own rule to calculate the password strength
        input.pwstrength(\"addRule\", \"demoRule\", function (options, word, score) {
            return word.match(/[a-z].[0-9]/) && score;
        }, 10, true);

        // set as initialized 
        initialized = true;
    }
});
$(document).ready(function() {
    // init background slide images
    $('.login-bg').backstretch([
        \"".Yii::getAlias('@uploadsUrl')."/login/bg2.jpg\",
        \"".Yii::getAlias('@uploadsUrl')."/login/bg3.jpg\",
        \"".Yii::getAlias('@uploadsUrl')."/login/bg4.jpg\"
        ], {
          fade: 1000,
          duration: 8000
        }
    );
});
", \yii\web\View::POS_END);
?>
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_item_group_cart".
 *
 * @property string $id
 * @property string $booking_item_id
 * @property string $booking_group_id
 *
 * @property BookingGroupCart $bookingGroup
 * @property BookingsItemsCart $bookingItem
 */
class BookingItemGroupCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_item_group_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_item_id', 'booking_group_id'], 'integer'],
            [['booking_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroupCart::className(), 'targetAttribute' => ['booking_group_id' => 'id']],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItemsCart::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_item_id' => 'Booking Item ID',
            'booking_group_id' => 'Booking Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroup()
    {
        return $this->hasOne(BookingGroupCart::className(), ['id' => 'booking_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItemsCart::className(), ['id' => 'booking_item_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_special_requests".
 *
 * @property string $id
 * @property integer $request_id
 * @property string $booking_date_id
 *
 * @property SpecialRequests $request
 * @property BookingDates $bookingDate
 */
class BookingSpecialRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_special_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'booking_date_id'], 'integer'],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpecialRequests::className(), 'targetAttribute' => ['request_id' => 'id']],
            [['booking_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingDates::className(), 'targetAttribute' => ['booking_date_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'request_id' => Yii::t('app', 'Request ID'),
            'booking_date_id' => Yii::t('app', 'Booking Date ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(SpecialRequests::className(), ['id' => 'request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingDate()
    {
        return $this->hasOne(BookingDates::className(), ['id' => 'booking_date_id']);
    }
}

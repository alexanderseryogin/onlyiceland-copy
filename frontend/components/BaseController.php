<?php

namespace frontend\components;

use Yii;

class BaseController extends \yii\web\Controller
{
    public function beforeAction($event)
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

    	// check if user has reset his/her initial password
        if (!Yii::$app->user->isGuest)	
        {
            if(Yii::$app->user->identity->first_login_reset_flag == 0)
            {
                if($action != 'first-login-reset')
                {
                    echo "<p style='display:none;'>string</p>";
                    $this->redirect(['/site/first-login-reset']);
                }
            }
        }

    	// intialize page titles
        $this->view->params['title'] = '';
        $this->view->params['subTitle'] = '';

        return parent::beforeAction($event);
    }

}

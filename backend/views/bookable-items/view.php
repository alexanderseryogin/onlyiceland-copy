<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BookableItems */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookable Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookable-items-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'provider_id',
            'item_type_id',
            'item_quantity',
            'how_booked',
            'item_names:ntext',
            'shortest_period',
            'longest_period',
            'no_of_guests',
            'max_adults',
            'max_children',
            'max_extra_beds',
            'max_baby_beds',
            'min_age_participation',
            'max_age_participation',
            'max_free_age',
            'background_color',
            'text_color',
            'include_in_groups',
        ],
    ]) ?>

</div>

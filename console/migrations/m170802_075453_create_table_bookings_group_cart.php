<?php

use yii\db\Migration;

class m170802_075453_create_table_bookings_group_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_group_cart` (
                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                      `group_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `group_user_id` int(11) DEFAULT NULL,
                      `group_guest_user_id` int(11) unsigned DEFAULT NULL,
                      `confirmation_id` int(11) DEFAULT NULL,
                      `cancellation_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `flag_id` int(11) DEFAULT NULL,
                      `status_id` int(11) DEFAULT NULL,
                      `travel_partner_id` int(11) unsigned DEFAULT NULL,
                      `voucher_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `reference_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `offers_id` int(11) DEFAULT NULL,
                      `estimated_arrival_time_id` int(11) unsigned DEFAULT NULL,
                      `balance` double DEFAULT NULL,
                      `comments` longtext COLLATE utf8_unicode_ci,
                      PRIMARY KEY (`id`),
                      CONSTRAINT `FK_booking_group_cart_confirmation_id` FOREIGN KEY (`confirmation_id`) REFERENCES `booking_confirmation_types` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_group_cart_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_group_cart_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_group_cart_offers_id` FOREIGN KEY (`offers_id`) REFERENCES `offers` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_group_cart_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_booking_group_cart_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `destination_travel_partners` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_bookings_group_cart_guest_user_id` FOREIGN KEY (`group_guest_user_id`) REFERENCES `booking_guests_cart` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
                      CONSTRAINT `FK_bookings_group_cart_user_id` FOREIGN KEY (`group_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
                    ) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
    }   

    public function down()
    {
        echo "m170802_075453_create_table_bookings_group_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

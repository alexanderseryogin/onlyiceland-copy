<?php
use common\models\Rates;
use common\models\RatesOverride;
?>

<?php
	if($rate->destination->pricing == 1)
	{
		$rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id]);

		echo '<input id="rate" data-rate="'.$rate->id.'" hidden>';
		echo '<input id="rate_date" value="'.$date.'" hidden>';
		$counter = 1;
		foreach ($rate->ratesCapacityPricings as $value) 
		{
			if(empty($rate_override))
			{
				$price = $value->price;
				if(isset($percentage))
				{
					$price = ( $percentage /100 * $price);
				}
				// will add check to test override price here//
				// $override = 
				$price = Yii::$app->formatter->asDecimal($price);
			}
			else
			{
				if($counter==1)
                {
                    $price = $rate_override->single;
                }
                if($counter==2)
                {
                    $price = $rate_override->double;
                }
                if($counter==3)
                {
                    $price = $rate_override->triple;
                }
                if($counter==4)
                {
                    $price = $rate_override->quad;
                }
				if(isset($percentage))
				{
					// $price = $price+( $percentage /100 * $price);

					$price = $percentage /100 * $price;
				}
				// will add check to test override price here//
				// $override = 
				$price = Yii::$app->formatter->asDecimal($price);
			}
			
			if($counter == 2)
			{
				echo '<div class="col-md-3" style="padding:0;">
                        <div class="form-group">
                            <label>'.Rates::$numberWords[$value->person_no-1].'</label>
                            <input class="form-control input-sm input-small input-inline cpricing" data-person="'.$value->person_no.'" type="" name=""  value="'.$price.'">
                        </div>
                    </div>';
			}
			else
			{
				echo '<div class="col-md-3" >
                        <div class="form-group">
                            <label>'.Rates::$numberWords[$value->person_no-1].'</label>
                            <input class="form-control input-sm input-small input-inline cpricing" data-person="'.$value->person_no.'"  type="" name=""  value="'.$price.'">
                        </div>
                    </div>';
			}
			
            $counter++;
		}
	}
	else
	{
		$rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id]);
		echo '<input id="rate" data-rate="'.$rate->id.'" hidden>';
		echo '<input id="rate_date" value="'.$date.'" hidden>';
		$counter = 1;
		if(empty($rate_override))
		{
			$item_price_first_adult = $rate->item_price_first_adult;
        
        
            $item_price_additional_adult = $rate->item_price_additional_adult;
        
        
            $item_price_first_child = $rate->item_price_first_child;
        
        
            $item_price_additional_child = $rate->item_price_additional_child;

			if(isset($percentage))
			{
				// $price = $price+( $percentage /100 * $price);

				// $item_price_first_adult = $item_price_first_adult +( $percentage /100 *$item_price_first_adult);
  
    //             $item_price_additional_adult = $item_price_additional_adult +( $percentage /100 *$item_price_additional_adult);
            
    //             $item_price_first_child = $item_price_first_child +( $percentage /100 *$item_price_first_child);
            
    //             $item_price_additional_child = $item_price_additional_child +( $percentage /100 *$item_price_additional_child);

				$item_price_first_adult = ( $percentage /100 *$item_price_first_adult);
  
                $item_price_additional_adult = ( $percentage /100 *$item_price_additional_adult);
            
                $item_price_first_child = ( $percentage /100 *$item_price_first_child);
            
                $item_price_additional_child = ( $percentage /100 *$item_price_additional_child);
			}
			// will add check to test override price here//
			// $override = 
			$item_price_first_adult = Yii::$app->formatter->asDecimal($item_price_first_adult);
			$item_price_additional_adult = Yii::$app->formatter->asDecimal($item_price_additional_adult);
			$item_price_first_child = Yii::$app->formatter->asDecimal($item_price_first_child);
			$item_price_additional_child = Yii::$app->formatter->asDecimal($item_price_additional_child);
		}
		else
		{
            $item_price_first_adult = $rate_override->item_price_first_adult;
        
        
            $item_price_additional_adult = $rate_override->item_price_additional_adult;
        
        
            $item_price_first_child = $rate_override->item_price_first_child;
        
        
            $item_price_additional_child = $rate_override->item_price_additional_child;

			if(isset($percentage))
			{
				// $price = $price+( $percentage /100 * $price);

				// $item_price_first_adult = $item_price_first_adult +( $percentage /100 *$item_price_first_adult);
  
    //             $item_price_additional_adult = $item_price_additional_adult +( $percentage /100 *$item_price_additional_adult);
            
    //             $item_price_first_child = $item_price_first_child +( $percentage /100 *$item_price_first_child);
            
    //             $item_price_additional_child = $item_price_additional_child +( $percentage /100 *$item_price_additional_child);

					$item_price_first_adult = ( $percentage /100 *$item_price_first_adult);
	  
	                $item_price_additional_adult = ( $percentage /100 *$item_price_additional_adult);
	            
	                $item_price_first_child = ( $percentage /100 *$item_price_first_child);
	            
	                $item_price_additional_child = ( $percentage /100 *$item_price_additional_child);
			}
			// will add check to test override price here//
			// $override = 
			$item_price_first_adult = Yii::$app->formatter->asDecimal($item_price_first_adult);
			$item_price_additional_adult = Yii::$app->formatter->asDecimal($item_price_additional_adult);
			$item_price_first_child = Yii::$app->formatter->asDecimal($item_price_first_child);
			$item_price_additional_child = Yii::$app->formatter->asDecimal($item_price_additional_child);
			// will add check to test override price here//
			// $override = 
			// $price = Yii::$app->formatter->asDecimal($price);
		}
		echo '<div class="col-md-3" >
                <div class="form-group">
                    <label>First Adult</label>
                    <input class="form-control input-sm input-small input-inline first_adult cpricing" type="" name=""  value="'.$item_price_first_adult.'">
                </div>
            </div>';
	
		echo '<div class="col-md-3" style="padding:0;">
                <div class="form-group">
                    <label> Additional Adult</label>
                    <input class="form-control input-sm input-small input-inline additional_adult cpricing"  type="" name=""  value="'.$item_price_additional_adult.'">
                </div>
            </div>';
	
		echo '<div class="col-md-3" >
                <div class="form-group">
                    <label>First Child</label>
                    <input class="form-control input-sm input-small input-inline first_child cpricing"  type="" name=""  value="'.$item_price_first_child.'">
                </div>
            </div>';
        echo '<div class="col-md-3" >
                <div class="form-group">
                    <label>Additional Child</label>
                    <input class="form-control input-sm input-small input-inline additional_adult cpricing"   type="" name=""  value="'.$item_price_additional_child.'">
                </div>
            </div>';
	}
			
	
?>
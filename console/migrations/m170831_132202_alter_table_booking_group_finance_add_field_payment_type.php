<?php

use yii\db\Migration;

class m170831_132202_alter_table_booking_group_finance_add_field_payment_type extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_date_finances`   
                          ADD COLUMN `payment_type` INT(11) DEFAULT 0  NULL AFTER `added_from_group`;
                        ');
    }

    public function down()
    {
        echo "m170831_132202_alter_table_booking_group_finance_add_field_payment_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

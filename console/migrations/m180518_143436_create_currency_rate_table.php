<?php

use common\models\CurrencyRate;
use yii\db\Migration;

/**
 * Handles the creation of table `currency_rate`.
 */
class m180518_143436_create_currency_rate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('currency_rate', [
            'id' => $this->primaryKey(),
            'date' => $this->string(255)->notNull(),
            'code' => $this->string(255)->notNull(),
            'rate' => $this->double()->notNull(),
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::EUR,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::USD,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::GBP,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::CAD,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::DKK,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::NOK,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::SEK,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::CHF,
            'rate' => 0
        ]);

        $this->insert('currency_rate', [
            'date' => date('U'),
            'code' => CurrencyRate::JPY,
            'rate' => 0
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('currency_rate');
    }
}

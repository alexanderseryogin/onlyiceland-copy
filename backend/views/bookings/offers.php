
<?php
	use common\models\Offers;
	
	$offers = Offers::find()
            ->where(['provider_id' => $provider_id])
            ->all();
    
	if(!empty($offers))
	{
		echo "<option value=''></option>";
		foreach ($offers as $key =>  $offer) 
	    {
	        echo "<option value='".$offer->id."'>".$offer->name." - ".$offer->commission."%</option>";  
	    }
	}
	else
		echo '';  

?>


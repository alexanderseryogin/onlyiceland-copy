
<?php
	use common\models\DestinationsSpecialRequests;

	$requests = DestinationsSpecialRequests::find()
            ->where(['provider_id' => $provider_id])
            ->all();
    
	if(!empty($requests))
	{
		echo '<label>Special Requests</label><br>';
		foreach ($requests as $key =>  $request) 
	    {
	    	echo  '<label><input class="special-requests" data="'.$request->request_id.'" value="'.$request->request_id.'" style="margin-left:10px;" type="checkbox"> '.$request->request->label.'</label>';
	    }
	}
	else 
	{
	 	echo '<span class="label label-danger"> Whoops! </span>
        <span>&nbsp;  No Special Requests found </span>';
	} 
?>


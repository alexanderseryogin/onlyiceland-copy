<?php

use yii\db\Migration;

class m161220_062434_create_table_providers_meal_plan extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_meal_plan`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `pfo_breakfast` INT(11), `pfo_lunch` INT(11), `pfo_dinner` INT(11), `sfo_morning_snack` INT(11), `sfo_afternoon_snack` INT(11), `sfo_evening_snack` INT(11), `abo_tea` INT(11), `abo_fountain` INT(11), `abo_beer` INT(11), `abo_liquor` INT(11), `abo_minibar` INT(11), PRIMARY KEY (`id`), CONSTRAINT `providers_meal_plan_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161220_062434_create_table_providers_meal_plan cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

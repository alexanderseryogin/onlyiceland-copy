<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Dashboard';
?>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 blue">
            <div class="visual">
                <i class="fa fa-bar-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="cu_r" data-value="<?= $reservations; ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Current Reservations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 blue-madison">
            <div class="visual">
                <i class="fa fa-bar-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="fu_r" data-value="<?= $reservations; ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Future Reservations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 blue-steel">
            <div class="visual">
                <i class="fa fa-bar-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="co_r" data-value="<?= $reservations; ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Completed Reservations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 blue-soft">
            <div class="visual">
                <i class="fa fa-bar-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="ca_r" data-value="<?= $reservations; ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Cancelled Reservations </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 red">
            <div class="visual">
                <i class="fa fa-line-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="r_cr_24" data-value="<?= $reservations24hours; ?>" data-counter="counterup"></span> </div>
                <div class="desc"> Reservations Created last 24 hours </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 red-mint">
            <div class="visual">
                <i class="fa fa-line-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="r_ca_24" data-value="<?= $reservations24hours; ?>" data-counter="counterup"></span> </div>
                <div class="desc"> Reservations Cancelled last 24 hours </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 red-soft">
            <div class="visual">
                <i class="fa fa-line-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="r_de" data-value="<?= $reservations24hours; ?>" data-counter="counterup"></span> </div>
                <div class="desc"> Deleted Reservations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 red-intense">
            <div class="visual">
                <i class="fa fa-line-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="r_de_24" data-value="<?= $reservations24hours; ?>" data-counter="counterup"></span> </div>
                <div class="desc"> Reservations Deleted last 24 hours </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 green">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="t_us" data-value="<?= $users ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Total Registered Users </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 green-soft">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="t_us_24" data-value="<?= $users ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> New Registered Users last 24 hours </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 green-dark">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="mru" data-value="<?= $users ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Country With Most Registered Users </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 green-steel">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="mru_24" data-value="<?= $users ?>" data-counter="counterup"></span>
                </div>
                <div class="desc"> Country With Most Registered Users Last 24 Hours </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 purple">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="t_de" data-value="<?= $providers ?>" data-counter="counterup"></span> 
                </div>
                <div class="desc"> Total Destinations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 purple-seance">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="t_acc" data-value="<?= $providers ?>" data-counter="counterup"></span> 
                </div>
                <div class="desc"> Total Accommodations </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 purple-studio">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="t_re" data-value="<?= $providers ?>" data-counter="counterup"></span> 
                </div>
                <div class="desc"> Total Restaurants </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 purple-medium">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="t_si" data-value="<?= $providers ?>" data-counter="counterup"></span> 
                </div>
                <div class="desc"> Total Sights </div>
            </div>
        </a>
    </div>
</div>


<?php
$first_rowUrl = Url::to(['/site/first-row-data']);
$second_rowUrl = Url::to(['/site/second-row-data']);
$third_rowUrl = Url::to(['/site/third-row-data']);
$fourth_rowUrl = Url::to(['/site/fourth-row-data']);
$this->registerJs("

    $(document).ready(function(){
        $.ajax(
                {
                    type: 'GET',
                    url: '$first_rowUrl',
                    success: function(result)
                    {
                        data = jQuery.parseJSON( result );
                        console.log(data);
                        $('#cu_r').html(data.current_reservations);
                        $('#fu_r').html(data.future_reservations);
                        $('#co_r').html(data.completed_reservations);
                        $('#ca_r').html(data.canceled_reservations);
                    },
                });
        $.ajax(
                {
                    type: 'GET',
                    url: '$second_rowUrl',
                    success: function(result)
                    {
                        data = jQuery.parseJSON( result );
                        console.log(data);
                        $('#r_de').html(data.deleted_reservatrions);
                        $('#r_de_24').html(data.deleted_reservatrionsr_last_24);
                        $('#r_ca_24').html(data.reservations_canceled_last_24);
                        $('#r_cr_24').html(data.reservations_created_last_24);
                    },
                });
        $.ajax(
                {
                    type: 'GET',
                    url: '$third_rowUrl',
                    success: function(result)
                    {
                        data = jQuery.parseJSON( result );
                        console.log(data);
                        $('#t_us').html(data.total_users);
                        $('#t_us_24').html(data.total_users_24);
                        if(data.country_mru == null)
                        {
                            $('#mru').html('0');
                        }
                        else
                        {
                            $('#mru').html(data.country_mru+' '+data.max_count_mru);
                        }
                        if(data.country_mru_24 == null)
                        {
                            $('#mru_24').html('0');
                        }
                        else
                        {
                            $('#mru_24').html(data.country_mru_24+' '+data.max_count_mru_24);
                        }
                        
                    },
                });

        $.ajax(
                {
                    type: 'GET',
                    url: '$fourth_rowUrl',
                    success: function(result)
                    {
                        data = jQuery.parseJSON( result );
                        console.log(data);
                        $('#t_de').html(data.total_destinations);
                        $('#t_acc').html(data.total_accomodations);
                        $('#t_re').html(data.total_resturants);
                        $('#t_si').html(data.total_sights);
                    },
                });

    });

        


");


?>
<?php

use yii\db\Migration;

class m170118_092321_alter_bookable_items_add_bed_type extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookable_items` ADD COLUMN `bed_type_id` INT(11) UNSIGNED NULL AFTER `min_age_adults`, ADD COLUMN `quantity` INT(11) NULL AFTER `bed_type_id`, ADD CONSTRAINT `FK_bookable_items_bed_type_id` FOREIGN KEY (`bed_type_id`) REFERENCES `bed_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
            ALTER TABLE `bookable_items` DROP FOREIGN KEY `FK_bookable_items_sleeping_arrangement_id`; 
            ALTER TABLE `bookable_items` ADD CONSTRAINT `FK_bookable_items_sleeping_arrangement_id` FOREIGN KEY (`sleeping_arrangement_id`) REFERENCES `sleeping_arrangements`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ');
    }

    public function down()
    {
        echo "m170118_092321_alter_bookable_items_add_bed_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

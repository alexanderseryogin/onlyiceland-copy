<?php

use yii\db\Migration;

class m170121_084405_alter_bookable_items_remove_bed_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` DROP COLUMN `bed_type_id`, DROP COLUMN `quantity`, DROP INDEX `FK_bookable_items_bed_type_id`, DROP FOREIGN KEY `FK_bookable_items_bed_type_id`; 
        ");
    }

    public function down()
    {
        echo "m170121_084405_alter_bookable_items_remove_bed_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

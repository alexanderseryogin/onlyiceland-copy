<?php

use yii\db\Migration;

class m180223_061415_alter_table_custom_report_columns extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_report_columns`   
                          ADD COLUMN `table_name` VARCHAR(256) NULL AFTER `custom_report_id`;
                        ');
    }

    public function safeDown()
    {
        echo "m180223_061415_alter_table_custom_report_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180223_061415_alter_table_custom_report_columns cannot be reverted.\n";

        return false;
    }
    */
}

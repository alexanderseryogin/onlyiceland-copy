<?php

use yii\helpers\Url;
use yii\web\View;
use common\models\BookingDateFinances;

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$currentDate =  date('d-m-Y');
$destination_id = $model->provider_id;

$total_debit = BookingDateFinances::find()->where(['booking_item_id' => $model->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

$total_credit = BookingDateFinances::find()->where(['booking_item_id' => $model->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                          ->sum('final_total');
$total_debit = empty($total_debit)?0:$total_debit;
$total_credit =  empty($total_credit)?0:$total_credit;

$balance_due = $total_debit - $total_credit;

?>

<script type="text/javascript">
    var pricing_type = <?=$model->pricing_type?>;
    var total_nights = 1;
    var item_id = <?=$model->item_id?>;
    var org_item_id = <?=$model->item_id?>;
    var booking_item_id = <?=$model->id?>;
    //var update_id = <?=$model->id?>;
    var global_remove_rate_conditions = <?=$model->rate_conditions?>;
    var redirect_check = '<?=$check?>';
    var housekeeping_statuses_array = <?=json_encode($viewDetailsRecord['housekeeping_status_id'])?>;
    //data-date-start-date="0d
</script>

<div class="row">
    <div class="col-sm-3 hide-fields">
        <label class="control-label"><strong>Arrival Date/Departure Date</strong></label>
        <div class="form-group" >
            <div class="input-group input-medium date-picker input-daterange" data-date-format="dd/mm/yyyy">
                <input type="text" class="form-control" id="arrival_date" value="<?=date('d/m/Y',strtotime($model->arrival_date))?>" name="BookingsItems[arrival_date]" required>
                <span class="input-group-addon"> to </span>
                <input type="text" class="form-control" id="departure_date" value="<?=date('d/m/Y',strtotime($model->departure_date))?>" name="BookingsItems[departure_date]" required>
            </div>
        </div>
    </div>
    <div class="col-sm-1" style="margin-top:30px;margin-left: -50px; ">
        <button id="today" type="button" class="btn btn-xs btn-primary">TODAY</button>
    </div>
    <div id="remove_rate_conditions_div" class="col-sm-3 hide">
        <label style="margin-top:30px;"><input id="remove_rate_conditions"  type="checkbox" <?php echo $Options[$model->rate_conditions] ?>> Remove Rate Conditions</label>
    </div>

    <div class="col-sm-offset-2 col-sm-3 pull-right">
        <div class="col-sm-6 " style="margin-top: -19px;">
            <!-- <button id="copy-btn" type="button" class="btn btn-default pull-right" >Copy as New</button> -->
            <br><br>
            <button id="check_availability" type="button" class="btn btn-primary pull-right" disabled="true">
                Check Availability
            </button>
        </div>
        <div class="col-sm-6">
            <div class="btn-group ">
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:;" style="margin-top:22px;"> Copy as New
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <?php foreach ($destination_items as $key => $value) {
                        ?>
                        <li id=<?= $key ?> class="destination_items" data=<?= $key ?> value= <?= $key ?>>
                            <?php if ($key == $model->item_id) {
                                ?>
                                <a href="javascript:;"><b><?= $value ?></b>
                                    <!--  <span class="badge badge-success"> 3 </span> -->
                                </a>
                            <?php } else {
                                ?>
                                <a href="javascript:;"> <?= $value ?>
                                    <!--  <span class="badge badge-success"> 3 </span> -->
                                </a>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

</div>
<div id="error_div"></div>
<br>
<?= $this->render('booked_item_rates', [
        'model' => $model,
        'viewDetailsRecord' => $viewDetailsRecord,
        'destination_items' => $destination_items,
        'check'             => $check,
        'balance_due'       => $balance_due,
]) ?>


<?php

$list_item_rates_for_update_url = Url::to(['/bookings/list-item-rates-for-update']);
$calculate_item_price = Url::to(['/bookings/calculate-item-price']);
$update_booked_item = Url::to(['/bookings/update-booked-item']);
$copyPage = Url::to(['/bookings/copy']);
$indexPage = Url::to(['/bookings/index','pageNo' => $pageNo]);
$calPage = Url::to(['/bookings/calendar']);
$schPage = Url::to(['/bookings/booking-issues','destination_id' => $model->provider_id]);
$savePage = Url::to(['/bookings/update','id' => $model->id]);
$get_item_names_list = Url::to(['/bookings/item-type-list']);


$this->registerJs("
jQuery(document).ready(function() 
{
    $('.booking_tabs li').click(function()
    {
        var href = $(this).find('a:first').attr('href');
        $('#tab_href').val(href);

    });
    //alert(window.location.host+'/onlyiceland-is/backend/bookings/update?id='+booking_item_id+'&tab_href='+$('#tab_href').val());
    var childTriggerClick = 0;

    //$('#submit_button').attr('disabled','disabled');
    $('.dropdown-quick-sidebar-toggler').css('display','block');

    var selectedItems = [];
    var selectedRatesAdults = [];
    var selectedRatesChild = [];
    var selectedRatesCapacityPricing = [];

    $(document).on('change','.item-name',function(){
        if($(this).children('option:selected').data('data') == 0)
        {
            //alert($(this).val());
            if (confirm('Continuing will OVERBOOK this item. Continue?')) {
                //alert('Thanks for confirming');
                $('.previous').val($(this).val());
            } else {
                $(this).val($('.previous').val());
            }
        }
        $('.previous').val($(this).val());
    });

    $(document).on('select2:select','.housekeeping-status', function (evt) 
    {
        var match=0;
        for (var i = 0; i < $(this).val().length; i++) {
           if ($(this).val()[i] == 1) 
                match = 1;
        }
        if(match)
        {
            $('.housekeeping-status > option').removeAttr('selected');
            $('.housekeeping-status > option').trigger('change');
            $('.housekeeping-status').select2().val(1).trigger('change');
        }
       
    });

    $('#travel_partner_dropdown').select2({
        placeholder: \"Select a Travel Partner\",
        allowClear: true
    });

    $('#offers_dropdown').select2({
        placeholder: \"Select an Offer\",
        allowClear: true
    });

    $('.user-dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true,
    });

    $('.guest-country').select2({
        placeholder: \"Select a Country\",
        allowClear: true,
    });

    $('#adults_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#childs_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });


    // ********************************************************************************************************************************************************* Js for Dynamically created Elements *********************************** //

    var total = 0;

    function setPriceToZero(rate_id,date)
    {
        $('.item-total-input-'+rate_id+'-'+date).val(0);
        $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
        $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
        $('.item-total-nights-price-'+rate_id+'-'+date).html('');
    }

    // ******************** Calculate Price for Adults ******************** //

    $(document).on('change','.adults-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var date = $(this).attr('date');

        // ******** Unselect other dropdowns on select current ********* //

        var no_of_adults = $(this).val();
        var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

        var temp_arr = '';
        var temp_rate_id = '';
        var temp_date = '';

        $('.children-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adults-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adult-dropdown-'+rate_id+'-'+date).val(no_of_adults);
        $('.child-dropdown-'+rate_id+'-'+date).val(no_of_children);

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ******************** Calculate Price for Children ******************** //

    $(document).on('change','.children-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var date = $(this).attr('date');

        // ******** Unselect other dropdowns on select current ********* //

        var no_of_children = $(this).val();
        var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();

        var temp_arr = '';
        var temp_rate_id = '';
        var temp_date = '';

        $('.children-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adults-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adult-dropdown-'+rate_id+'-'+date).val(no_of_adults);
        $('.child-dropdown-'+rate_id+'-'+date).val(no_of_children);

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Capacity Pricing **************** //
    
    $(document).on('change','.capacity-pricing',function()
    {
        var checked = $(this).is(':checked');
        var person_no = $(this).attr('data');

        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var date = $(this).attr('date');

        var temp_arr = '';
        var temp_rate_id = '';

        $('.capacity-pricing-'+org_item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('id').split(':');
            temp_rate_id = temp_arr[1];

            $(this).prop('checked', false);
            setPriceToZero(temp_rate_id,date);
        });

        if(checked)
        {
            $(this).prop('checked',true);
            $('.save-item').attr('disabled',false);
            $('.update-item').attr('disabled',false);             
        }
        else
        {
            var exists = false;
            $('.capacity-pricing').each(function()
            {
                if($(this).is(':checked'))
                {
                    exists = true;
                }
            });
            if(!exists)
            {
                $('.save-item').attr('disabled','disabled');
                $('.update-item').attr('disabled','disabled');
            }
            $(this).prop('checked', false);
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // on change of custom rate price of capacity pricing //
    $(document).on('change','.custom_rate',function()
    {

        var arr = $(this).attr('id').split(':');
        var item_id = arr[1];
        var rate_id = arr[2]
        var date = $(this).attr('date');
        var custom_rate = $(this).val();

        var data = $(this).attr('data');

        var checkbox;
        checkbox = $(this).prev();
        for (i=0; i < data; i++) 
        { 
            checkbox = checkbox.prev();
        }

        // var checkbox_id = item_id+':'+rate_id+':'+data;
        //alert(checkbox.val());

        if(checkbox.is(':checked'))
        {
            //alert('checked');
            // var arr = $(this).attr('id').split(':');
            // var item_id = arr[1];
            // var rate_id = arr[2]
            // var date = $(this).attr('date');
            // var custom_rate = $(this).val();
            
            calculatePriceForChangedElements(rate_id,item_id,date);
        }
        // else
        // {
        //     alert('not checked');
        // }
        // var arr = $(this).attr('id').split(':');
        // var item_id = arr[1];
        // var rate_id = arr[2]
        // var date = $(this).attr('date');
        // var custom_rate = $(this).val();
        
        // calculatePriceForChangedElements(rate_id,item_id,date);
       

    });

    // ************** Show item dropdowns on click view button ******************** //

    $(document).on('click','.view-details',function()
    {
        var item_id = $(this).attr('data');

        if($(this).find('span').text() == 'View Details')
        {
            $('.rate-details-fields-'+item_id).css('display', 'block');
            $(this).find('span').text('Hide Details');

            $(this).removeClass('btn-default');
            $(this).addClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-down');
            $(this).find('i').addClass('fa-chevron-up');
        }
        else if($(this).find('span').text()  == 'Hide Details')
        {
            $('.rate-details-fields-'+item_id).css('display', 'none');
            $(this).find('span').text('View Details');

            $(this).addClass('btn-default');
            $(this).removeClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-up');
            $(this).find('i').addClass('fa-chevron-down');
        }
    });

    // ************** Calculate Price for Upsell Items **************** //
    
    $(document).on('change','.upsell-items',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var upsell_id = $(this).attr('data');
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).css('display','none');
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Upsell Item Quantity ****************** //

    $(document).on('change','.upsell-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Discount Codes ****************** //

    $(document).on('change','.discount-codes',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var discount_id = $(this).attr('data');
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).css('display','none');
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Discount Code Quantity ****************** //

    $(document).on('change','.discount-code-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Common function to calculate price **************** //

    function calculatePriceForChangedElements(rate_id = '', item_id = '',date = '')
    {
        var custom_rate = '';
        var no_of_adults = 0;
        var no_of_children = 0;

        var upsell_ids =[];
        var discount_ids =[];

        var travel_partner_id = '';
        var offer_id = '';
        
        var person_no = 0;
        total_nights = 1;

        var quantity = $('.item-quantities-'+item_id+'-'+date).val();

        // ******** get Upsell Items Id ********* //

        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
        $(upsellItemsElement).each(function()
        {
            if(this.checked)
            {
                var upsell_id = $(this).attr('data');
                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
            }
        });

        // ******** get Discount Id********* //

        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
        $(discountCodesElement).each(function()
        {
            if(this.checked)
            {
                var discount_id = $(this).attr('data');
                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
            }
        });

        if(discount_ids.length == 0)
            discount_ids = '';

        if(upsell_ids.length == 0)
            upsell_ids = '';

        if($('#travel_partner_dropdown').val()!='')
            travel_partner_id = $('#travel_partner_dropdown').val();

        if($('#offers_dropdown').val()!='')
            offer_id = $('#offers_dropdown').val();

        if(pricing_type) // *********** Capacity Pricing ********** //
        {
            var capacity_flag = 0;

            var capacityPricingElement = $('.cpricing-'+rate_id+'-'+date);
            $(capacityPricingElement).each(function()
            {
                if(this.checked)
                {
                    capacity_flag = 1;

                    person_no = $(this).attr('data');
                    var sibs = $(this).siblings();

                    $.each( sibs, function() {
                        tr = $(this).val();
                        if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                        {
                            custom_rate = tr.replace('.','');
                        }
                        
                    });
                }
            });

            if(capacity_flag)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'custom_rate': custom_rate,
                                'person_no' : person_no,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                                'dates': $('.item-date-'+item_id+'-'+date).val()
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                        //$('.item-total-nights-price-'+rate_id+'-'+date).html('<br>= '+data.item_total_nights_price);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });  
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+date).val(0);
                $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
                $('.item-total-nights-price-'+rate_id+'-'+date).html('');
            }
        }
        else // *********** First/Additional Pricing ********** //
        {
            no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
            no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

            if(no_of_children > 0 || no_of_adults > 0)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'no_of_adults' : no_of_adults,
                                'no_of_children' : no_of_children,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                                'dates': $('.item-date-'+item_id+'-'+date).val()
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                        //$('.item-total-nights-price-'+rate_id+'-'+date).html('<br>= '+data.item_total_nights_price);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+date).val(0);
                $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
                $('.item-total-nights-price-'+rate_id+'-'+date).html('');
            }
        }
    }

    // ******************** Calculate Price for Travel Partner and Offer ******************** //

    $('#travel_partner_dropdown').on('select2:select', function (evt) 
    {
        $('.vou_ref_div').removeClass('hide');
        $('#offers_dropdown').attr('disabled','disabled');
        updateItemPrices();
    });

    $('#travel_partner_dropdown').on('select2:unselecting ', function (e) 
    {
        $('.vou_ref_div').addClass('hide');
        $('#travel_partner_dropdown').val('');

        $('#offers_dropdown').attr('disabled',false);
        updateItemPrices();
    });

    $('#offers_dropdown').on('select2:select', function (evt) 
    {
        $('#travel_partner_dropdown').attr('disabled','disabled');
        updateItemPrices();
    });

    $('#offers_dropdown').on('select2:unselecting ', function (e) 
    {
        $('#offers_dropdown').val('');
        $('#travel_partner_dropdown').attr('disabled',false);
        updateItemPrices();
    });

    function updateItemPrices()
    {
        $('.helper-field').each(function()
        {
            var arr = $(this).val().split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];

            calculatePriceForChangedElements(rate_id,item_id,date);
        });
    }

    function getAllDates(item_id)
    {
        var allDates = [];
        $('.item-quantity-'+item_id).each(function()
        {
            var select_error_flag = 1;
            var date = $(this).attr('date');

            if(pricing_type)
            {
                $('.capacity-pricing-'+item_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        select_error_flag = 0;
                        allDates.push($('.item-date-'+item_id+'-'+date).val());
                        return false;
                    }
                });
            }
            else
            {
                $('.helper-field-'+item_id+'-'+date).each(function()
                {
                    var arr = $(this).val().split(':');
                    var rate_id = arr[1];

                    var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                    var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                    if(no_of_adults>0 || no_of_children>0)
                    {
                        select_error_flag = 0;
                        allDates.push($('.item-date-'+item_id+'-'+date).val());
                        return false;
                    }
                });
            }
        });
        return allDates;
    }

    // ************** Update Booked Item ********************//

    function getDisableDiscountCheckbox()
    {
        var checkboxes = $('.disable_discount_checkbox');
        var array = {};
        $.each(checkboxes, function() {
            if($(this).attr('name')) {
                array[$(this).attr('name')] = $(this).prop('checked') ? 1 : 0;
            }
        });
        return array;
    }
    
    $(document).on('click', '.disable_discount_checkbox', function() {
        var discount_amount = $(this).parents('tr').find('.discount_amount');
        if($(this).prop('checked')) {
            discount_amount.val(0);
            discount_amount.attr('disabled', 'disabled');
        } else {
            discount_amount.removeAttr('disabled');
        }
    });

    $(document).on('click','.update-item',function()
    {
        var bookingItems = [];
        var item_id = $(this).attr('data');
        //var item_id = $('#item-type option:selected').val();
        var allDates = getAllDates(item_id);
        var custom_rate = '';

        $('.helper-field-'+item_id).each(function()
        {
            var arr = $(this).val().split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];

            if(pricing_type) // *********** Capacity Pricing ********** //
            {
                no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                $('.cpricing-'+rate_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        var person_no = $(this).attr('data');
                        var beds_combinations_id = '';
                        var upsell_ids =[];
                        var discount_ids =[];
                        var special_requests_ids = [];
                        var quantity = $('.item-quantities-'+item_id+'-'+date).val();

                        //getting price from input custom rate //
                        var sibs = $(this).siblings();

                        $.each( sibs, function() {
                            tr = $(this).val();
                            if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                            {
                                custom_rate = tr.replace('.','');
                            }
                            
                        });

                        //########## get guests data ###########//

                        var guest_first_name = $('.guest-first-name-'+item_id).val();
                        var guest_last_name  = $('.guest-last-name-'+item_id).val();
                        var guest_country    = $('.guest-country-'+item_id).val();

                        var voucher_no = $('.voucher_no-'+item_id).val();
                        var reference_no = $('.reference_no-'+item_id).val();
                        var comment = $('.comment-'+item_id).val();
                        var notes = $('.notes-'+item_id).val();

                        // ******** get Upsell Items Id ********* //

                        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
                        $(upsellItemsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var upsell_id = $(this).attr('data');
                                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
                            }
                        });

                        // ******** get Discount Id********* //

                        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
                        $(discountCodesElement).each(function()
                        {
                            if(this.checked)
                            {
                                var discount_id = $(this).attr('data');
                                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
                            }
                        });

                        // ******** get bed combination id********* //

                        var bedCombinationElement = $('.beds-combinations-'+item_id);
                        $(bedCombinationElement).each(function()
                        {
                            if(this.checked)
                            {
                                beds_combinations_id = $(this).attr('data');
                            }
                        });

                        // ******** get Special Requests Id ********* //

                        /*var specialRequestsElement = $('.special-requests');
                        $(specialRequestsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var special_request_id = $(this).attr('data');
                                special_requests_ids.push(special_request_id);
                            }
                        });*/

                        if(discount_ids.length == 0)
                            discount_ids = '';
                            
                        if(upsell_ids.length == 0)
                            upsell_ids = '';

                        var object = 
                        {
                            'date' : $('.item-date-'+item_id+'-'+date).val(),
                            'remove_rate_conditions' : global_remove_rate_conditions,
                            'estimated_arrival_time_id' : $('.estimated-arrival-time-dropdown').val(),
                            'item_id': item_id,
                            'rate_id': rate_id,
                            'no_of_adults' : no_of_adults,
                            'no_of_children' : no_of_children,
                            'custom_rate': custom_rate,
                            'person_no' : person_no,
                            'discount_ids' : discount_ids,
                            'upsell_ids' : upsell_ids,
                            'quantity' : quantity,
                            'travel_partner_id' : $('#travel_partner_dropdown').val(),
                            'voucher_no':voucher_no,
                            'reference_no':reference_no,
                            'comment': comment,
                            'notes' : notes,
                            'offer_id' : $('#offers_dropdown').val(),
                            'user_id' : $('.user-'+item_id).val(),
                            'confirmation_id' : $('.booking-confirmation-'+item_id).val(),
                            'booking_cancellation' : $('.booking-cancellation-'+item_id).val(),
                            'flag_id' : $('.booking-flag-'+item_id).val(),
                            'status_id' : $('.booking-status-'+item_id).val(),
                            'housekeeping_status_id' : $('.housekeeping-status-'+item_id).val(),
                            'total_nights' : 1,
                            'pricing_type' : pricing_type,
                            'price_json' : $('.price-json-'+rate_id+'-'+date).val(),
                            'total' : $('.item-total-input-'+rate_id+'-'+date).val(),
                            'beds_combinations_id' : beds_combinations_id,
                            'special_requests_ids' : special_requests_ids,
                            'guest_first_name' : guest_first_name,
                            'guest_last_name' : guest_last_name,
                            'guest_country' : guest_country,
                            'item_name_id': $('.item-name-'+item_id).val(),
                        };

                        bookingItems.push(object);
                    }
                });
            }
            else // *********** First/Additional Pricing ********** //
            {
                no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                if(no_of_children > 0 || no_of_adults > 0)
                {
                    var beds_combinations_id = '';
                    var upsell_ids =[];
                    var discount_ids =[];
                    var special_requests_ids = [];
                    var quantity = $('.item-quantities-'+item_id+'-'+date).val();

                    //########## get guests data ###########//

                    var guest_first_name = $('.guest-first-name-'+item_id).val();
                    var guest_last_name  = $('.guest-last-name-'+item_id).val();
                    var guest_country    = $('.guest-country-'+item_id).val();

                    var voucher_no = $('.voucher_no-'+item_id).val();
                    var reference_no = $('.reference_no-'+item_id).val();
                    var comment = $('.comment-'+item_id).val();
                    var notes = $('.notes-'+item_id).val();

                    // ******** get Upsell Items Id ********* //

                    var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
                    $(upsellItemsElement).each(function()
                    {
                        if(this.checked)
                        {
                            var upsell_id = $(this).attr('data');
                            upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
                        }
                    });

                    // ******** get Discount Id********* //

                    var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
                    $(discountCodesElement).each(function()
                    {
                        if(this.checked)
                        {
                            var discount_id = $(this).attr('data');
                            discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
                        }
                    });

                    // ******** get bed combination id********* //

                    var bedCombinationElement = $('.beds-combinations-'+item_id);
                    $(bedCombinationElement).each(function()
                    {
                        if(this.checked)
                        {
                            beds_combinations_id = $(this).attr('data');
                        }
                    });

                    // ******** get Special Requests Id ********* //

                    /*var specialRequestsElement = $('.special-requests');
                    $(specialRequestsElement).each(function()
                    {
                        if(this.checked)
                        {
                            var special_request_id = $(this).attr('data');
                            special_requests_ids.push(special_request_id);
                        }
                    });*/

                    if(discount_ids.length == 0)
                        discount_ids = '';

                    if(upsell_ids.length == 0)
                        upsell_ids = '';

                    var object = 
                    {
                        'date' : $('.item-date-'+item_id+'-'+date).val(),
                        'remove_rate_conditions' : global_remove_rate_conditions,
                        'estimated_arrival_time_id' : $('.estimated-arrival-time-dropdown').val(),
                        'item_id': $('#item-type option:selected').val(),
                        'rate_id': rate_id,
                        'no_of_adults' : no_of_adults,
                        'no_of_children' : no_of_children,
                        'discount_ids' : discount_ids,
                        'upsell_ids' : upsell_ids,
                        'quantity' : quantity,
                        'travel_partner_id' : $('#travel_partner_dropdown').val(),
                        'voucher_no':voucher_no,
                        'reference_no':reference_no,
                        'comment': comment,
                        'notes' : notes,
                        'offer_id' : $('#offers_dropdown').val(),
                        'user_id' : $('.user-'+item_id).val(),
                        'confirmation_id' : $('.booking-confirmation-'+item_id).val(),
                        'booking_cancellation' : $('.booking-cancellation-'+item_id).val(),
                        'flag_id' : $('.booking-flag-'+item_id).val(),
                        'status_id' : $('.booking-status-'+item_id).val(),
                        'housekeeping_status_id' : $('.housekeeping-status-'+item_id).val(),
                        'total_nights' : total_nights,
                        'pricing_type' : pricing_type,
                        'price_json' : $('.price-json-'+rate_id+'-'+date).val(),
                        'total' : $('.item-total-input-'+rate_id+'-'+date).val(),
                        'beds_combinations_id' : beds_combinations_id,
                        'special_requests_ids' : special_requests_ids,
                        'guest_first_name' : guest_first_name,
                        'guest_last_name' : guest_last_name,
                        'guest_country' : guest_country,
                        'item_name_id': $('.item-name-'+item_id).val(),
                    };

                    bookingItems.push(object);
                }
            }
        });
        //////////////////////////// Getting Charges and Payment Details //////////////////////////////

        ///////////////////////// charges table //////////////////////////////

        var charges_table = $('#charges_table tbody');
        var charges_data =[];
        charges_table.find('tr').each(function (i) {

            if($(this).hasClass('custom'))
            {
                var id;
                if($(this).attr('id') == undefined)
                {
                    id = 0;
                }
                else
                {
                    id = $(this).attr('id');
                }
                var tds = $(this).find('td');
                if(tds.hasClass('hasSelect'))
                {
                    //alert('has Select');
                    var object = 
                    {
                        'id'    : id,
                        'item'  : tds.eq(3).find('select').find(':selected').attr('data-type'),
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'discount_disable' : tds.eq(1).children('input').first().is(':checked') ? 1 : 0,
                        'date'    : tds.eq(2).children('input').first().val(),
                        'description' : tds.eq(3).find('input').first().val(),
                        'quantity'    : tds.eq(4).children('input').first().val(),
                        'price'       : tds.eq(5).children('input').first().val(),
                        'vat'         : tds.eq(6).children('input').first().val(),
                        'tax'         : tds.eq(7).children('input').first().val(),
                        'discount_amount'         : tds.eq(9).children('input').first().val(),
                        'vat_amount'         : tds.eq(10).children('input').first().val(),
                        'final_total'         : tds.eq(11).children('input').first().val(),
                    };
                }
                else
                {
                    //alert('No Select');
                    var object = 
                    {
                        'id'    : id,
                        'item'  : 4,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'discount_disable' : tds.eq(1).children('input').first().is(':checked') ? 1 : 0,
                        'date'    : tds.eq(2).children('input').first().val(),
                        'description' : tds.eq(3).find('input').first().val(),
                        'quantity'    : tds.eq(4).children('input').first().val(),
                        'price'       : tds.eq(5).children('input').first().val(),
                        'vat'         : tds.eq(6).children('input').first().val(),
                        'tax'         : tds.eq(7).children('input').first().val(),
                        'discount_amount'         : tds.eq(9).children('input').first().val(),
                        'vat_amount'         : tds.eq(10).children('input').first().val(),
                        'final_total'         : tds.eq(11).children('input').first().val(),
                    };
                }
                
                charges_data.push(object);
            }
                
        });

        ////////////////////////////// payments table //////////////////////////

        // var payments_table = $('#payments_table tbody');
        // var payments_data =[];
        // payments_table.find('tr').each(function (i){

        //     if($(this).hasClass('custom'))
        //     {
        //         var id;
        //         if($(this).attr('id') == undefined)
        //         {
        //             id = 0;
        //         }
        //         else
        //         {
        //             id = $(this).attr('id');
        //         }

        //         var tds = $(this).find('td');
        //         if(tds.hasClass('hasSelect'))
        //         {
        //            // alert('has Select');
        //             var object = 
        //             {
        //                 'id'    :id,
        //                 'receipt' : tds.eq(0).children('input').first().is(':checked'),
        //                 'date'    : tds.eq(1).children('input').first().val(),
        //                 'description' : tds.eq(2).find('select').first().val(),
        //                 'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
        //                 // 'price'        : tds.eq(4).children('input').first().val(),
        //                 // 'vat'          : tds.eq(5).children('input').first().val(),
        //                 // 'tax'          : tds.eq(6).children('input').first().val(),
        //             };
        //         }
        //         else
        //         {
        //            // alert('No Select');
        //             var object = 
        //             {
        //                 'id'    : id,
        //                 'receipt' : tds.eq(0).children('input').first().is(':checked'),
        //                 'date'    : tds.eq(1).children('input').first().val(),
        //                 'description' : tds.eq(2).find('input').first().val(),
        //                 'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
        //                 // 'price'        : tds.eq(4).children('input').first().val(),
        //                 // 'vat'          : tds.eq(5).children('input').first().val(),
        //                 // 'tax'          : tds.eq(6).children('input').first().val(),
        //             };
        //         }
                
        //         charges_data.push(object);
        //     }
                

                
        // });

        var payments_table = $('#payments_table tbody');
        var payments_data =[];
        payments_table.find('tr').each(function (i){
            if($(this).hasClass('custom'))
            {
                var id;
                if($(this).attr('id') == undefined)
                {
                    id = 0;
                }
                else
                {
                    id = $(this).attr('id');
                }

                if($(this).hasClass('has-switch'))
                {
                    var tds = $(this).find('td');

                    if(tds.hasClass('hasSelect'))
                    {
                       // alert('has Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                            'deposit_percentage' : tds.eq(4).find('input').val(),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }
                    else
                    {
                       // alert('No Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                            'deposit_percentage' : tds.eq(4).find('input').val(),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }   
                }
                else
                {
                    var tds = $(this).find('td');

                    if(tds.hasClass('hasSelect'))
                    {
                       // alert('has Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }
                    else
                    {
                       // alert('No Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }
                }
                    
                
                payments_data.push(object);
            }
                
        });

        $.ajax(
        {
            type: 'POST',
            data: {
                bookingItems:bookingItems,
                allDates:allDates,
                booking_item_id: $('#booking_item_id').val(),
               charges_data : charges_data,
               payments_data : payments_data
            },
            url: '$update_booked_item',
            async: false,
            success: function(data)
            {
                if(redirect_check == 'cal')
                {
                    window.location.replace('$calPage');
                }
                else if(redirect_check == 'sch')
                {
                    window.location.replace('$schPage'+'&exp_date='+allDates[0]);
                }
                else
                {
                    window.location.replace('$indexPage');
                }
                
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    // on save press //
    $(document).on('click','.save-item',function()
    {
        var bookingItems = [];
        var item_id = $(this).attr('data');
        //var item_id = $('#item-type option:selected').val();
        var item_id_selected = $('#item-type option:selected').val();
        var allDates = getAllDates(item_id);
        var custom_rate = '';

        $('.helper-field-'+item_id).each(function()
        {
            var arr = $(this).val().split(':');
            //var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];

            if(pricing_type) // *********** Capacity Pricing ********** //
            {
                no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                $('.cpricing-'+rate_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        var person_no = $(this).attr('data');
                        var beds_combinations_id = '';
                        var upsell_ids =[];
                        var discount_ids =[];
                        var special_requests_ids = [];
                        var quantity = $('.item-quantities-'+item_id+'-'+date).val();

                        //getting price from input custom rate //
                        var sibs = $(this).siblings();

                        $.each( sibs, function() {
                            tr = $(this).val();
                            if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                            {
                                custom_rate = tr.replace('.','');
                            }
                            
                        });

                        //########## get guests data ###########//

                        var guest_first_name = $('.guest-first-name-'+item_id).val();
                        var guest_last_name  = $('.guest-last-name-'+item_id).val();
                        var guest_country    = $('.guest-country-'+item_id).val();

                        var voucher_no = $('.voucher_no-'+item_id).val();
                        var reference_no = $('.reference_no-'+item_id).val();
                        var comment = $('.comment-'+item_id).val();
                        var notes = $('.notes-'+item_id).val();

                        // ******** get Upsell Items Id ********* //

                        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
                        console.log('upsellItemsElement', upsellItemsElement);
                        $(upsellItemsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var upsell_id = $(this).attr('data');
                                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
                            }
                        });

                        // ******** get Discount Id********* //

                        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
                        $(discountCodesElement).each(function()
                        {
                            if(this.checked)
                            {
                                var discount_id = $(this).attr('data');
                                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
                            }
                        });

                        // ******** get bed combination id********* //

                        var bedCombinationElement = $('.beds-combinations-'+item_id);
                        $(bedCombinationElement).each(function()
                        {
                            if(this.checked)
                            {
                                beds_combinations_id = $(this).attr('data');
                            }
                        });

                        // ******** get Special Requests Id ********* //

                        /*var specialRequestsElement = $('.special-requests');
                        $(specialRequestsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var special_request_id = $(this).attr('data');
                                special_requests_ids.push(special_request_id);
                            }
                        });*/
                        
                        console.log('upsell_ids', upsell_ids);

                        if(discount_ids.length == 0)
                            discount_ids = '';

                        if(upsell_ids.length == 0)
                            upsell_ids = '';

                        var object = 
                        {
                            'date' : $('.item-date-'+item_id+'-'+date).val(),
                            'remove_rate_conditions' : global_remove_rate_conditions,
                            'estimated_arrival_time_id' : $('.estimated-arrival-time-dropdown').val(),
                            'item_id': $('#item-type').val(),
                            'rate_id': rate_id,
                            'no_of_adults' : no_of_adults,
                            'no_of_children' : no_of_children,
                            'custom_rate': custom_rate,
                            'person_no' : person_no,
                            'discount_ids' : discount_ids,
                            'upsell_ids' : upsell_ids,
                            'quantity' : quantity,
                            'travel_partner_id' : $('#travel_partner_dropdown').val(),
                            'voucher_no':voucher_no,
                            'reference_no':reference_no,
                            'comment': comment,
                            'notes': notes,
                            'offer_id' : $('#offers_dropdown').val(),
                            'user_id' : $('.user-'+item_id).val(),
                            'confirmation_id' : $('.booking-confirmation-'+item_id).val(),
                            'booking_cancellation' : $('.booking-cancellation-'+item_id).val(),
                            'flag_id' : $('.booking-flag-'+item_id).val(),
                            'status_id' : $('.booking-status-'+item_id).val(),
                            'housekeeping_status_id' : $('.housekeeping-status-'+item_id).val(),
                            'total_nights' : 1,
                            'pricing_type' : pricing_type,
                            'price_json' : $('.price-json-'+rate_id+'-'+date).val(),
                            'total' : $('.item-total-input-'+rate_id+'-'+date).val(),
                            'beds_combinations_id' : beds_combinations_id,
                            'special_requests_ids' : special_requests_ids,
                            'guest_first_name' : guest_first_name,
                            'guest_last_name' : guest_last_name,
                            'guest_country' : guest_country,
                            'item_name_id': $('.item-name-'+item_id).val(),
                        };

                        bookingItems.push(object);
                    }
                });
            }
            else // *********** First/Additional Pricing ********** //
            {
                no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                if(no_of_children > 0 || no_of_adults > 0)
                {
                    var beds_combinations_id = '';
                    var upsell_ids =[];
                    var discount_ids =[];
                    var special_requests_ids = [];
                    var quantity = $('.item-quantities-'+item_id+'-'+date).val();

                    //########## get guests data ###########//

                    var guest_first_name = $('.guest-first-name-'+item_id).val();
                    var guest_last_name  = $('.guest-last-name-'+item_id).val();
                    var guest_country    = $('.guest-country-'+item_id).val();

                    var voucher_no = $('.voucher_no-'+item_id).val();
                    var reference_no = $('.reference_no-'+item_id).val();
                    var comment = $('.comment-'+item_id).val();
                    var notes = $('.notes-'+item_id).val();

                    // ******** get Upsell Items Id ********* //

                    var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
                    $(upsellItemsElement).each(function()
                    {
                        if(this.checked)
                        {
                            var upsell_id = $(this).attr('data');
                            upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
                        }
                    });

                    // ******** get Discount Id********* //

                    var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
                    $(discountCodesElement).each(function()
                    {
                        if(this.checked)
                        {
                            var discount_id = $(this).attr('data');
                            discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
                        }
                    });

                    // ******** get bed combination id********* //

                    var bedCombinationElement = $('.beds-combinations-'+item_id);
                    $(bedCombinationElement).each(function()
                    {
                        if(this.checked)
                        {
                            beds_combinations_id = $(this).attr('data');
                        }
                    });

                    // ******** get Special Requests Id ********* //

                    /*var specialRequestsElement = $('.special-requests');
                    $(specialRequestsElement).each(function()
                    {
                        if(this.checked)
                        {
                            var special_request_id = $(this).attr('data');
                            special_requests_ids.push(special_request_id);
                        }
                    });*/

                    if(discount_ids.length == 0)
                        discount_ids = '';

                    if(upsell_ids.length == 0)
                        upsell_ids = '';

                    var object = 
                    {
                        'date' : $('.item-date-'+item_id+'-'+date).val(),
                        'remove_rate_conditions' : global_remove_rate_conditions,
                        'estimated_arrival_time_id' : $('.estimated-arrival-time-dropdown').val(),
                        'item_id': $('#item-type').val(),
                        'rate_id': rate_id,
                        'no_of_adults' : no_of_adults,
                        'no_of_children' : no_of_children,
                        'discount_ids' : discount_ids,
                        'upsell_ids' : upsell_ids,
                        'quantity' : quantity,
                        'travel_partner_id' : $('#travel_partner_dropdown').val(),
                        'voucher_no':voucher_no,
                        'reference_no':reference_no,
                        'comment': comment,
                        'notes': notes,
                        'offer_id' : $('#offers_dropdown').val(),
                        'user_id' : $('.user-'+item_id).val(),
                        'confirmation_id' : $('.booking-confirmation-'+item_id).val(),
                        'booking_cancellation' : $('.booking-cancellation-'+item_id).val(),
                        'flag_id' : $('.booking-flag-'+item_id).val(),
                        'status_id' : $('.booking-status-'+item_id).val(),
                        'housekeeping_status_id' : $('.housekeeping-status-'+item_id).val(),
                        'total_nights' : total_nights,
                        'pricing_type' : pricing_type,
                        'price_json' : $('.price-json-'+rate_id+'-'+date).val(),
                        'total' : $('.item-total-input-'+rate_id+'-'+date).val(),
                        'beds_combinations_id' : beds_combinations_id,
                        'special_requests_ids' : special_requests_ids,
                        'guest_first_name' : guest_first_name,
                        'guest_last_name' : guest_last_name,
                        'guest_country' : guest_country,
                        'item_name_id': $('.item-name-'+item_id).val(),
                        'disable_discount_checkbox' : 2
                    };

                    bookingItems.push(object);
                }
            }
        });

        //////////////////////////// Getting Charges and Payment Details //////////////////////////////

        ///////////////////////// charges table //////////////////////////////

        var charges_table = $('#charges_table tbody');
        var charges_data =[];
        charges_table.find('tr').each(function (i) {

            if($(this).hasClass('custom'))
            {
                var id;
                if($(this).attr('id') == undefined)
                {
                    id = 0;
                }
                else
                {
                    id = $(this).attr('id');
                }

                var tds = $(this).find('td');
                if(tds.hasClass('hasSelect'))
                {
                    //alert('has Select');
                    var object = 
                    {
                        'id'    : id,
                        'item'  : tds.eq(3).find('select').find(':selected').attr('data-type'),
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'discount_disable' : tds.eq(1).children('input').first().is(':checked') ? 1 : 0,
                        'date'    : tds.eq(2).children('input').first().val(),
                        'description' : tds.eq(3).find('input').first().val(),
                        'quantity'    : tds.eq(4).children('input').first().val(),
                        'price'       : tds.eq(5).children('input').first().val(),
                        'vat'         : tds.eq(6).children('input').first().val(),
                        'tax'         : tds.eq(7).children('input').first().val(),
                        'discount_amount'         : tds.eq(8).children('input').first().val(),
                        'vat_amount'         : tds.eq(10).children('input').first().val(),
                        'final_total'         : tds.eq(11).children('input').first().val(),
                    };
                }
                else
                {
                   // alert('No Select');
                    var object = 
                    {
                        'id'    : id,
                        'item'  : 4,
                        'receipt' : tds.eq(0).children('input').first().is(':checked'),
                        'discount_disable' : tds.eq(1).children('input').first().is(':checked') ? 1 : 0,
                        'date'    : tds.eq(2).children('input').first().val(),
                        'description' : tds.eq(3).find('input').first().val(),
                        'quantity'    : tds.eq(4).children('input').first().val(),
                        'price'       : tds.eq(5).children('input').first().val(),
                        'vat'         : tds.eq(6).children('input').first().val(),
                        'tax'         : tds.eq(7).children('input').first().val(),
                        'discount_amount'         : tds.eq(9).children('input').first().val(),
                        'vat_amount'         : tds.eq(10).children('input').first().val(),
                        'final_total'         : tds.eq(11).children('input').first().val(),
                    };
                }
                
                charges_data.push(object);
            }
                
                
        });

        ////////////////////////////// payments table //////////////////////////

        var payments_table = $('#payments_table tbody');
        var payments_data =[];
        payments_table.find('tr').each(function (i){
            if($(this).hasClass('custom'))
            {
                var id;
                if($(this).attr('id') == undefined)
                {
                    id = 0;
                }
                else
                {
                    id = $(this).attr('id');
                }

                if($(this).hasClass('has-switch'))
                {
                    var tds = $(this).find('td');

                    if(tds.hasClass('hasSelect'))
                    {
                       // alert('has Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                            'deposit_percentage' : tds.eq(4).find('input').val(),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }
                    else
                    {
                       // alert('No Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            'payment_type' : tds.eq(3).find('input').bootstrapSwitch('state'),
                            'deposit_percentage' : tds.eq(4).find('input').val(),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }   
                }
                else
                {
                    var tds = $(this).find('td');

                    if(tds.hasClass('hasSelect'))
                    {
                       // alert('has Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }
                    else
                    {
                       // alert('No Select');
                        var object = 
                        {
                            'id'    : id,
                            'receipt' : tds.eq(0).children('input').first().is(':checked'),
                            'date'    : tds.eq(1).children('input').first().val(),
                            'description' : tds.eq(2).find('input').first().val(),
                            'total'   : tds.eq(7).children('input').first().inputmask('unmaskedvalue'),
                            // 'price'        : tds.eq(4).children('input').first().val(),
                            // 'vat'          : tds.eq(5).children('input').first().val(),
                            // 'tax'          : tds.eq(6).children('input').first().val(),
                        };
                    }
                }
                    
                
                payments_data.push(object);
            }
                
        });

        $.ajax(
        {
            type: 'POST',
            data: {
                bookingItems:bookingItems, 
                allDates:allDates, 
                booking_item_id: $('#booking_item_id').val(),
                charges_data : charges_data,
                payments_data : payments_data
            },
            url: '$update_booked_item',
            async: true,
            success: function(data)
            {
                var redirect_url = '".Url::base()."/bookings/update?id='+booking_item_id+'&tab_href='+$('#tab_href').val();
                if(window.location.host == 'localhost')
                {
                    var redirect_url2 = '".Url::to(['/bookings/update','id' => $model->id])."';
                    $(location).attr('href',redirect_url2+'&tab_href='+$('#tab_href').val());
                }
                else
                {
                    if(redirect_url == window.location.href )
                    {
                        location.reload();
                    }
                    else
                    {
                        location.replace('".Url::base()."/bookings/update?id='+booking_item_id+'&tab_href='+$('#tab_href').val());
                        location.reload();
                    }
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    // Copy as new functionality //
    $(document).on('click','.destination_items',function()
    {
        //alert($(this).val());
        var item_id = $(this).val();
        $.ajax(
        {
            type: 'POST',
            data: {item_id:item_id, booking_item_id: $('#booking_item_id').val()},
            url: '".$copyPage."',
            async: false,
            success: function(data)
            {
               //alert(data);
               window.location.replace(data);
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    function initRatesJs()
    {
        $('.total-tooltips').tooltip();
    }

    initRatesJs();

    $('.rate-details-dropdowns').select2({
        placeholder: \"Select an Option\",
    });

    $('.housekeeping-status').select2({
            multiple: true,
            placeholder: 'Select a Status',
        });
    $('.housekeeping-status').select2().val(housekeeping_statuses_array).trigger('change');

    function getFirstSelectedRateDate()
    {
        var first_date = '';

        $('.item-quantity').each(function()
        {
            var item_id = $(this).attr('data');
            var select_error_flag = 1;
            var date = $(this).attr('date');

            if(pricing_type)
            {
                $('.capacity-pricing-'+item_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        select_error_flag = 0;
                        first_date = date;
                        return false;
                    }
                });
            }
            else
            {
                $('.helper-field-'+item_id+'-'+date).each(function()
                {
                    var arr = $(this).val().split(':');
                    var rate_id = arr[1];

                    var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                    var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                    if(no_of_adults>0 || no_of_children>0)
                    {
                        select_error_flag = 0;
                        first_date = date;
                        return false;
                    }
                });
            }

            if(!select_error_flag)
                return false;
        });

        return first_date;
    }

    $('#check_availability').click(function()
    {
        checkAvailability();
    });


    function checkAvailability()
    {
        $('.alert-danger').css('display','none');
        //alert($('#item-type option:selected').val());
        var id = '".$model->id."';
        
        // *************** send ajax call to show rates Dynamically *************** //

        var remove_rate_conditions = 0;

        if($('#remove_rate_conditions').prop('checked') == true)
        {
            global_remove_rate_conditions = 1;
            remove_rate_conditions = 1;
        }
            
        App.blockUI({target:\".bookings-update\", animate: !0});

        var object = {
                        'item_id' : $('#item-type option:selected').val(),
                        //'item_id' : $('.save-item').attr('data'),
                        'arrival_date' : $('#arrival_date').val(),
                        'departure_date' : $('#departure_date').val(),
                        'remove_rate_conditions' : remove_rate_conditions,
                        'booking_item_id' : booking_item_id,
                        'first_date': getFirstSelectedRateDate(),
                    };

        $.ajax(
        {
            type: 'POST',
            data: object,
            url: '$list_item_rates_for_update_url',
            success: function(data)
            {
                App.unblockUI(\".bookings-update\");

                data = jQuery.parseJSON( data );
                total_nights = 1;

                //alert(data.item_pannel)
                // $('#item-pannel').empty().html(data.item_pannel);
                if(data.date_exception_flag == 1)
                {
                    $('#error_div').empty().html(`<div class='capacity-error alert-danger alert fade in'>This destination is closed on one or more of the dates selected, please change your dates and try again.</div>`);
                        $('.add-item').attr('disabled','disabled');
                    //alert('This destination is closed on one or more of the dates selected, please change your dates and try again.');
                }
                else
                {
                    $('#error_div').empty().html('');
                    $('.available-rates').empty().html(data.item_rates);

                    //it waas commented before  uncomment if we dont want to show unavailable units//


                    //$('.item-name').empty().html(data.item_names);
                    initRatesJs();
                }
                
            },
            error: function()
            {
                App.unblockUI(\".bookings-update\");
                alert('error');
            }
        });
    }

    // on change item type dropdown //
     $('#item-type').on('change',function()
    {
        $('.alert-danger').css('display','none');
        //alert($('#item-type option:selected').val());

        var id = '".$model->id."';
        
        // *************** send ajax call to show rates Dynamically *************** //

        var remove_rate_conditions = 0;

        if($('#remove_rate_conditions').prop('checked') == true)
        {
            global_remove_rate_conditions = 1;
            remove_rate_conditions = 1;
        }
            
        App.blockUI({target:\".bookings-update\", animate: !0});

        var object = {
                        'item_id' : $('#item-type option:selected').val(),
                        'arrival_date' : $('#arrival_date').val(),
                        'departure_date' : $('#departure_date').val(),
                        'remove_rate_conditions' : remove_rate_conditions,
                        'booking_item_id' : booking_item_id,
                        'first_date': getFirstSelectedRateDate(),
                    };

        $.ajax(
        {
            type: 'POST',
            data: object,
            url: '$get_item_names_list',
            success: function(data)
            {
                App.unblockUI(\".bookings-update\");

                data = jQuery.parseJSON( data );
                total_nights = 1;

                //alert(data.item_names);
                 $('.item-name').empty().html(data.item_names);
                // $('.available-rates').empty().html(data.item_rates);
                // $('.item-name').empty().html(data.item_names);
                //checkAvailability();
                initRatesJs();
            },
            error: function()
            {
                App.unblockUI(\".bookings-update\");
                alert('error');
            }
        });
    });


    $('#arrival_date').on('change',function()
    {   
        var new_date = moment($(this).val(),'D/M/YYYY').add('days', 1);
        $('#departure_date').datepicker('setDate',new_date.format('DD/MM/YYYY'));

        var arr_dt = '".date('d/m/Y',strtotime($model->arrival_date))."';
        var dep_dt =  '".date('d/m/Y',strtotime($model->departure_date))."';

    });

    $('#departure_date').on('change',function()
    {
        var arr_dt = '".date('d/m/Y',strtotime($model->arrival_date))."';
        var dep_dt =  '".date('d/m/Y',strtotime($model->departure_date))."';

        if($(this).val() != dep_dt || $('#arrival_date').val() != arr_dt )
        {
            $('#check_availability').attr('disabled',false);
            $('#remove_rate_conditions_div').removeClass('hide');
        }
        else
        {
            $('#check_availability').attr('disabled','disabled');
            $('#remove_rate_conditions_div').addClass('hide');
        }
    });


    $('#today').click(function()
    {
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
       // alert(date);
        $('#arrival_date').datepicker('setDate',date);
    });

    $('#remove_rate_conditions').change(function()
    {
        if(this.checked)
        {
            if(confirm('Are you sure to Remove Rate Restrictions ?'))
            {
                checkAvailability();
            }
            else
            {
                $(this).prop('checked', false);
            }
        }
        else
        {
            if(confirm('Are you sure to Apply Rate Restrictions ?'))
            {
                checkAvailability();
            }
            else
            {
                $(this).prop('checked', true);
            }
        }
    });

});",View::POS_END);
if($balance_due == 0)
    {
        $this->registerJs("
            $('#balance_due').css('color','green');
        ");
    }
    if($balance_due < 0)
    {
        $this->registerJs("
            $('#balance_due').css('color','orange');
        ");
    }
?>
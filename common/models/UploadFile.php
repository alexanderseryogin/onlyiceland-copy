<?php

namespace common\models;
use yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadFile extends Model
{
    /**
     * @var UploadedFile
     */
    public $FileToUpload;
    public $FileName;
    public $path;
    
    public function upload($user_id,$fileType)
    {
        $file = explode('.',$this->FileToUpload['name']);
        $size = count($file);

        if ($file[$size-1]=='jpg' || $file[$size-1]=='jpeg' || $file[$size-1]=='png') 
        {

            if(!file_exists(Yii::getAlias('@app').'/../uploads'))
            {
                mkdir(Yii::getAlias('@app').'/../uploads', 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/users'))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/users', 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/users/'.$user_id))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/users/'.$user_id, 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/users/'.$user_id.'/'.$fileType))
            {
                 mkdir(Yii::getAlias('@app').'/../uploads/users/'.$user_id.'/'.$fileType, 0777, true);
            }

            $this->FileName = $file[0].time().'.'.$file[$size-1];

            $path = Yii::getAlias('@app').'/../uploads/users/'.$user_id.'/'.$fileType.'/'.$this->FileName;

             move_uploaded_file($this->FileToUpload['tmp_name'], $path);

             return true;
        } 
        else 
        {
            return false;
        }
    }

    public function uploadFile($module,$id,$folder)
    {
        $file = explode('.',$this->FileToUpload->name);
        $size = count($file);

            if(!file_exists(Yii::getAlias('@app').'/../uploads'))
	        {
	        	mkdir(Yii::getAlias('@app').'/../uploads', 0777, true);
	        }

	        if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module))
	        {
	            mkdir(Yii::getAlias('@app').'/../uploads/'.$module, 0777, true);
	        }

	        if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id))
	        {
	            mkdir(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id, 0777, true);
	        }

	        if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder))
	        {
	       		 mkdir(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder, 0777, true);
	        }

            $this->FileName = $module.'-'.Yii::$app->security->generateRandomString(8).'-'.time().'.'.$file[$size-1];

        	$path = Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder.'/'.$this->FileName;

        	move_uploaded_file($this->FileToUpload->tempName, $path);

            return true;
    }

    public function uploadImage($module,$id,$folder)
    {
        $file = explode('.',$this->FileToUpload['file']['name']);
        $size = count($file);

        if ($file[$size-1]=='jpg' || $file[$size-1]=='jpeg' || $file[$size-1]=='png') 
        {
            if(!file_exists(Yii::getAlias('@app').'/../uploads'))
            {
                mkdir(Yii::getAlias('@app').'/../uploads', 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/'.$module, 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id, 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder))
            {
                 mkdir(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder, 0777, true);
            }

            $this->FileName = $module.'-'.Yii::$app->security->generateRandomString(8).'-'.time().'.'.$file[$size-1];

            $path = Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder.'/'.$this->FileName;

            move_uploaded_file($this->FileToUpload['file']['tmp_name'], $path);

            return true;
        } 
        else 
        {
            return false;
        }
    }

    public function uploadFileObject($module,$id,$folder)
    {
        $file = explode('.',$this->FileToUpload->name);
        $size = count($file);
        $fileType = strtolower($file[$size-1]);

        if($fileType=='csv') 
        {
            $path = $this->createPath($module,$id,$folder,$file,$size);
            return move_uploaded_file($this->FileToUpload->tempName, $path);
        } 
        else
        {
            return false;
        }
    }

    public function createPath($module,$id,$folder,$file,$size)
    {
        if(!file_exists(Yii::getAlias('@app').'/../uploads'))
        {
            mkdir(Yii::getAlias('@app').'/../uploads', 0777, true);
        }

        if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module))
        {
            mkdir(Yii::getAlias('@app').'/../uploads/'.$module, 0777, true);
        }

        if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id))
        {
            mkdir(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id, 0777, true);
        }

        if(!file_exists(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder))
        {
             mkdir(Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder, 0777, true);
        }

        $this->FileName = $module.'-'.Yii::$app->security->generateRandomString(8).'-'.time().'.'.strtolower($file[$size-1]);

        $path = Yii::getAlias('@app').'/../uploads/'.$module.'/'.$id.'/'.$folder.'/'.$this->FileName;

        return $path;
    }
}
        	



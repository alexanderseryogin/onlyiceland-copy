<?php

use yii\db\Migration;

class m180403_113604_alter_table_travel_partners extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `travel_partner`   
                          ADD COLUMN `calculation` INT(11) NULL AFTER `billing_contact`;
                        ');
    }

    public function safeDown()
    {
        echo "m180403_113604_alter_table_travel_partners cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180403_113604_alter_table_travel_partners cannot be reverted.\n";

        return false;
    }
    */
}

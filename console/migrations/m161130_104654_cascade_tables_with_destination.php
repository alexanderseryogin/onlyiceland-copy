<?php

use yii\db\Migration;

class m161130_104654_cascade_tables_with_destination extends Migration
{
    public function up()
    {
        $this->execute("
                ALTER TABLE `booking_policies` DROP FOREIGN KEY `FK_booking_policies_provider_id`;
                ALTER TABLE `booking_policies` ADD CONSTRAINT `FK_booking_policies_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `booking_rules` DROP FOREIGN KEY `FK_booking_rules_provider_id`; 
                ALTER TABLE `booking_rules` ADD CONSTRAINT `FK_booking_rules_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `providers_financial` DROP FOREIGN KEY `FK_financial_provider_id`; 
                ALTER TABLE `providers_financial` ADD CONSTRAINT `FK_financial_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 
            ");
    }

    public function down()
    {
        echo "m161130_104654_cascade_tables_with_destination cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m180131_074422_create_table_booking_override_statuses extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `booking_override_statuses`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `label` VARCHAR(256),
                          `background_color` VARCHAR(256),
                          `text_color` VARCHAR(256),
                          `type` INT(11),
                          PRIMARY KEY (`id`)
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180131_074422_create_table_booking_override_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180131_074422_create_table_booking_override_statuses cannot be reverted.\n";

        return false;
    }
    */
}

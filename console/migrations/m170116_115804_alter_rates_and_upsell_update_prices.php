<?php

use yii\db\Migration;

class m170116_115804_alter_rates_and_upsell_update_prices extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates` CHANGE `item_price_first_adult` `item_price_first_adult` INT(11) NULL, CHANGE `item_price_additional_adult` `item_price_additional_adult` INT(11) NULL, CHANGE `item_price_first_child` `item_price_first_child` INT(11) NULL, CHANGE `item_price_additional_child` `item_price_additional_child` INT(11) NULL, CHANGE `min_price` `min_price` INT(11) NULL, CHANGE `max_price` `max_price` INT(11) NULL; 

            ALTER TABLE `upsell_items` CHANGE `amount` `amount` INT(11) NOT NULL; 
        ");
    }

    public function down()
    {
        echo "m170116_115804_alter_rates_and_upsell_update_prices cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170427_134848_create_booking_group_special_requests extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `booking_group_special_requests`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `booking_group_id` INT(11) UNSIGNED, `special_request_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_booking_group_special_requests_booking_group_id` FOREIGN KEY (`booking_group_id`) REFERENCES `booking_group`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_booking_group_special_requests_sp_id` FOREIGN KEY (`special_request_id`) REFERENCES `special_requests`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170427_134848_create_booking_group_special_requests cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
    use common\models\AddOn;
    use yii\web\View;

    $Checkbox = [
        0 => '',
        1 => 'checked'
    ];
    $isChecked = 0;

?>

<?php

            //echo "<pre>";
            //print_r($destination_add_ons);
            //exit();
            
    ?>

<style type="text/css">
    .align-center {
        text-align: center;
    }
    table tr th {
        text-align: center;
    }
    .free{background: #DFF0D8;}
    .extra{background: #FCF8E3}
    .not_available{background: #FFEB89}
    .not_display{background: #F2DEDE}
    .table-scrollable {border: none !important; overflow-x: hidden !important;}
    .dataTables_scrollBody {overflow-x: hidden !important;}
    table.dataTable thead .sorting_desc {
        background-image: none;
    }
    table.dataTable thead .sorting_asc {
        background-image: none;
    }
</style>

<!-- BEGIN ACCORDION PORTLET-->

<div class="tab-pane" id="add_ons">
    <table class="table table-striped table-bordered table-hover order-column" id="destination_add_ons">
        <thead>
            <tr>
                <th></th>
                <th>Add-on Name</th>
                <th>Add-on Description</th>
                <th>Add-on Type</th>
                <th>Discountable?</th>
                <th>Price</th>
                <th>VAT %</th>
                <th>Tax Amount</th>
                <th>Fee Amount</th>
                <th>Commissionable?</th>

            </tr>
        </thead>
        <tbody>
            <?php
                $add_ons = AddOn::find()->all();
                $i=0;
                $comission = 0;
                $name = '';
                $description = '';
                $type = '';
                $discountable = '';
                $commissionable = 0;
                $price = 0;
                $vat = 0;
                $tax = 0;
                $fee = 0;

                if(!empty($add_ons))
                {
                    foreach ($add_ons as $key => $value) 
                    {
                        if($provider_model->isNewRecord )
                        {
                            $name = $value->name;
                            $description = $value->description;
                            $type   = $value->type;
                            $discountable   = $value->discountable;
                            $commissionable   = $value->commissionable;
                        }
                        else
                        {
                            if(isset($destination_add_ons) && isset($destination_add_ons[$value->id] ) )
                            {
                                $name           = $destination_add_ons[$value->id]['name'];
                                $description    = $destination_add_ons[$value->id]['description'];
                                $type           = $destination_add_ons[$value->id]['type'];
                                $discountable   = $destination_add_ons[$value->id]['discountable'];
                                $price          = $destination_add_ons[$value->id]['price'];
                                $vat            = $destination_add_ons[$value->id]['vat'];
                                $tax            = $destination_add_ons[$value->id]['tax'];
                                $fee            = $destination_add_ons[$value->id]['fee'];
                                $commissionable = $destination_add_ons[$value->id]['commissionable'];

                                // $comission = $destination_travel_partners[$value->id]['comission'];
                                // $primary_contact = $destination_travel_partners[$value->id]['primary_contact'];
                                // $billing_contact = $destination_travel_partners[$value->id]['billing_contact'];
                                // $booking_email   = $destination_travel_partners[$value->id]['booking_email_address'];
                                // $invoice_email   = $destination_travel_partners[$value->id]['invoice_email_address'];
                                $isChecked = 1;
                            }
                            else
                            {
                                $name = $value->name;
                                $description = $value->description;
                                $type   = $value->type;
                                $discountable   = $value->discountable;
                                $commissionable   = $value->commissionable;
                                $price = 0;
                                $vat = 0;
                                $tax = 0;
                                $fee = 0;
                                $isChecked = 0;
                                $commission = 0;
                            }
                        }

                        echo '<tr>';
                        echo '<td class="align-center"><label><input name="Destinations[add_ons_checkbox][]" '.$Checkbox[$isChecked].' value="'.$value->id.'" type="checkbox"></label></td>';

                        echo '<td><input type="text" class="form-control" name="Destinations[add_on_details]['.$value->id.'][name]" value="'.$name.'" ></td>';
                        echo '<td><input type="text" class="form-control" name="Destinations[add_on_details]['.$value->id.'][description]" value="'.$description.'" ></td>';
                        echo '<td><select class="form-control select2 type" name="Destinations[add_on_details]['.$value->id.'][type]">';
                        if($type==0)
                        {
                            echo '<option value="0" selected>Fixed Price</option>
                            <option value="1" >Percentage</option>';
                        }
                        else
                        {
                            echo '<option value="0" >Fixed Price</option>
                            <option value="1" selected>Percentage</option>';
                        }
                                  
                        echo '</select></td>';
                        echo '<td><select class="form-control select2 discountable" name="Destinations[add_on_details]['.$value->id.'][discountable]">';
                        if($discountable == 1)
                        {
                            echo '<option value="1" selected>Yes</option>
                                  <option value="0" >No</option>';
                        }
                        else
                        {
                            echo '<option value="1" >Yes</option>
                                  <option value="0" selected>No</option>';
                        }
                                  
                        echo '</select></td>';
                        // echo '  <td>
                        //             <input type="text" class="form-control" name="Destinations[travel_partners_details]['.$value->id.'][booking_email]" value="'.$booking_email.'" >
                        //         </td>';
                        echo 
                        '<td>
                            
                            <input type="text" class="form-control" name="Destinations[add_on_details]['.$value->id.'][price]" style="text-align:right" value="'.$price.'" >
                        </td>

                        <td>
                            <input type="text" class="form-control add_on_vat " value="'.$vat.'" style="text-align:right">
                            <input type="text" name="Destinations[add_on_details]['.$value->id.'][vat]" style="text-align:right" value="'.$vat.'" hidden>
                        </td>

                        <td>
                            
                            <input type="text" class="form-control" name="Destinations[add_on_details]['.$value->id.'][tax]" value="'.$tax.'" style="text-align:right">
                        </td>

                        <td>
    
                            <input type="text"  class="form-control" name="Destinations[add_on_details]['.$value->id.'][fee]" value="'.$fee.'" style="text-align:right">
                        </td>
                        
                        <td class="align-center">
                            <select class="form-control select2 discountable" name="Destinations[add_on_details]['.$value->id.'][commissionable]">';
                            if($commissionable == 1)
                            {
                                echo '<option value="1" selected>Yes</option>
                                      <option value="0" >No</option>';
                            }
                            else
                            {
                                echo '<option value="1" >Yes</option>
                                      <option value="0" selected>No</option>';
                            }

                            echo '</select>
                        </td>             
                        ';
                       
                        echo '</tr>';
                        $i++;
                    }
                }
                
            ?>
        </tbody>
    </table>
</div>

<?php
$this->registerJs("
$('.type').select2();
$('.discountable').select2();
$('.add_on_vat').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

$('.add_on_vat').on('change',function()
    {
        //alert($(this).val());
        $(this).siblings('input').val($(this).val());
    });

function initDataTableAddOn(tableSelector)
{
    return $(tableSelector).DataTable({
            language: {
                aria: {
                    sortAscending: \": activate to sort column ascending\",
                    sortDescending: \": activate to sort column descending\"
                },
                emptyTable: \"No data available in table\",
                info: \"Showing _TOTAL_ amenities\",
                infoEmpty: \"No amenities found\",
                infoFiltered: \"(filtered 1 from _MAX_ total amenities)\",
                lengthMenu: \"_MENU_ amenities\",
                search: \"Search:\",
                zeroRecords: \"No matching records found\"
            },
            info: false,
            scrollY: 300,
            scrollCollapse: true,
            //scroller: !0,
            statesave : false,
            columns: [
                { 'width': '5%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                
            ],
            order: [
                [1, \"asc\"]
            ],
            \"columnDefs\": [
                { \"orderable\": false, \"targets\": [0,2,3,4,5,6,7] }
            ],
            lengthMenu: [
                [10, 15, 20, -1],
                [10, 15, 20, \"All\"]
            ],
            paging: false,
            retrieve: true
        }); 
}

function redrawDataTableAddOns(tableSelector)
{
    var table = initDataTableAddOn(tableSelector);
    table.draw();
}

function redrawDataTableAddOn()
{
    redrawDataTableAddOns('#destination_add_ons');
}

jQuery(document).ready(function() 
{
    initDataTableAddOn('#destination_add_ons');
    setTimeout(function()
    { 
        redrawDataTableAddOn(); 
    }, 500);
    
});",View::POS_END);
?>
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_report_flags".
 *
 * @property integer $id
 * @property integer $flag_id
 * @property integer $custom_report_id
 *
 * @property CustomReports $customReport
 */
class CustomReportFlags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_report_flags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flag_id', 'custom_report_id'], 'integer'],
            [['custom_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomReports::className(), 'targetAttribute' => ['custom_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flag_id' => 'Flag ID',
            'custom_report_id' => 'Custom Report ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReport()
    {
        return $this->hasOne(CustomReports::className(), ['id' => 'custom_report_id']);
    }
}

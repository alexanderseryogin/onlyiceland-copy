<?php
use common\models\BookingDates;
use yii\grid\GridView;
use yii\helpers\Html;
$bookableItems = $destination->bookableItems;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<table class="table table-striped table-bordered table2excel">
    <!-- <thead>
      <tr>
        <th><?=$destination->name?></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </thead> -->
    <tbody>
     <?php
        $total_arr = array();
        $total_arr['total_occupancy'] = 0;
        $total_arr['booked_count'] = 0;
        $total_arr['avail_count'] = 0;
        // $total_arr['occupancy_count'] = 0;

        for ($i=0; $i < count($destination->bookableItems)+2 ; $i++) 
        { 
            if($i ==  0)
            {
                echo "<tr>";
                echo "<td><b>".$destination->name."</b></td>";
                foreach ($report as $key => $value) 
                {
                    echo "<td style='text-align:center;'>".$key."</td>";
                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                    // echo "<td style='border-left: 0px;'></td>";
                    
                }
                echo "<td style='text-align:center;'>Total</td>";
                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                // echo "<td style='border-left: 0px;'></td>";
                echo "</tr>";
            }   
            else
            {
                if($i < count($destination->bookableItems)+1)
                {
                    $b_item = $bookableItems[$i-1];
                    echo "<tr>";
                    echo "<td>".$b_item->itemType->name."</td>";
                    // echo "<pre>";
                    //     print_r($report);
                    //     exit();
                    // $booked_count = 0;
                    // $avail_count = 0;
                    $total_occupancy = 0;
                    $booked_count = 0;
                    $avail_count = 0;
                    foreach ($report as $key => $value) 
                    {
                        $item = $value[$b_item->itemType->name];
                        $total_qty = $item['booked']+$item['avail'];
                        $occupancy = round(($item['booked']/$total_qty)*100,2);
                        echo "<td style='text-align:right'>".$occupancy.' %'."</td>";
                        // echo "<td style='text-align:right'>".$item['booked']."</td>";
                        // echo "<td style='text-align:right'>".$item['avail']."</td>";
                        // echo "<td style='text-align:right'>".round($item['occupancy'],2).' %' ."</td>";
                        $total_occupancy += round($occupancy,2);
                        $booked_count += $item['booked'];
                        $avail_count += $item['avail'];
                    }
                    $total_arr['booked_count'] += $booked_count;
                    $total_arr['avail_count'] += $avail_count;
                    $total_arr['total_occupancy'] += round($total_occupancy/count($report),2);
                    // $total_arr['booked_count'] += $booked_count;
                    // $total_arr['avail_count'] += $avail_count;
                    //$total_arr['occupancy_count'] += round($guests_occupancy/count($report),2);

                    // echo "<td style='text-align:right'>".$guests_count."</td>";
                    // echo "<td style='text-align:right'>".$booked_count."</td>";
                    // echo "<td style='text-align:right'>".$avail_count."</td>";
                    echo "<td style='text-align:right'>".round($total_occupancy/count($report),2).' %'."</td>";
                    echo "</tr>";

                }
                else
                {
                    // echo "<pre>";
                    // print_r($total_arr);
                    echo "<tr>";
                    echo "<td>Total</td>";
                    // echo "<pre>";
                    // print_r($report);
                    // exit();
                    
                    foreach ($report as $key => $value) 
                    {
                        // $guests_count = 0;
                        $booked_count = 0;
                        $avail_count = 0;
                        $total_occupancy = 0;
                        $total_count = 0;
                        foreach ($destination->bookableItems as $item) 
                        {
                            // $total_qty = $value[$item->itemType->name]['booked']+$value[$item->itemType->name]['avail'];

                            // $occupancy = round(($value[$item->itemType->name]['booked']/$total_qty)*100,2);

                            // $total_occupancy += $occupancy;

                            $booked_count += $value[$item->itemType->name]['booked'];
                            $total_count += $value[$item->itemType->name]['booked']+$value[$item->itemType->name]['avail']; 
                            // $avail_count += $value[$item->itemType->name]['avail'];
                        }
                        $total_rooms = 0;
                        foreach ($destination->bookableItems as  $value)
                        {
                            $total_rooms = $total_rooms + $value->item_quantity; 
                        }
                        $occupancy = round(($total_occupancy/$total_rooms),2);
                         echo "<td style='text-align:right'>".round(($booked_count/$total_count)*100,2).' %'."</td>";

                    }
                    // echo "<td style='text-align:right'>".$total_arr['guests_count']."</td>";
                    // echo "<td style='text-align:right'>".$total_arr['booked_count']."</td>";
                    // echo "<td style='text-align:right'>".$total_arr['avail_count']."</td>";
                    $total_rooms = 0;
                    foreach ($destination->bookableItems as  $value)
                    {
                        $total_rooms = $total_rooms + $value->item_quantity; 
                    }
                    echo "<td style='text-align:right'>".round(($total_arr['booked_count']/($total_arr['booked_count']+$total_arr['avail_count']))*100,2).' %'."</td>";

                    // echo "<td style='text-align:right'>".round($total_arr['total_occupancy']/count($destination->bookableItems),2).' %'."</td>";
                    // */
                    echo "</tr>";

                }
                                                                    
            }
            
    
        }
    ?>
    </tbody>
</table>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>

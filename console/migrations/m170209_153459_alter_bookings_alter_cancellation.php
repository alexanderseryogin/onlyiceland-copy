<?php

use yii\db\Migration;

class m170209_153459_alter_bookings_alter_cancellation extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` CHANGE `cancellation_id` `cancellation_id` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL; 
        ");
    }

    public function down()
    {
        echo "m170209_153459_alter_bookings_alter_cancellation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

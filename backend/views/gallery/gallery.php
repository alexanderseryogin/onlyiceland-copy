    <?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;
    use dosamigos\fileupload\FileUploadUI;

    $this->title = Yii::t('app', 'Gallery');
    $this->params['breadcrumbs'][] = $this->title;

    $this->registerJs("

$(document).on('ready pjax:success', function () {
  $('.ajaxDelete').on('click', function (e) {
    e.preventDefault();
    var deleteUrl     = $(this).attr('delete-url');
    var pjaxContainer = $(this).attr('pjax-container');
    var result = confirm('Are you sure you want to delete this item?');
            
              if (result) {
                $.ajax({
                  url:   deleteUrl,
                  type:  'post',
                  error: function (xhr, status, error) {
                    alert('There was an error with your request.' 
                          + xhr.responseText);
                  }
                }).done(function (data) {
                  $.pjax.reload({container: '#' + $.trim(pjaxContainer)});
                });
              }
  });
});

");

    ?>
    <style type="text/css">
        .preview img{
            max-width:100px !important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-picture-o"></i>Gallery
                    </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#portlet_tab_1" data-toggle="tab"> Gallery </a>
                            </li>
                            <li>
                                <a href="#portlet_tab_2" data-toggle="tab"> Upload Images </a>
                            </li>
                        </ul>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="portlet_tab_1">
                            <?php Pjax::begin([
                                'id' => 'pjax-list',
                            ]); ?>    
                            <?= GridView::widget([
                                    'dataProvider' => $gallery,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'picture_name',
                                            'format' => 'raw',
                                            'filter' => 'none',
                                            'value' => function($data){
                                                return Html::img(\Yii::getAlias('@web') . "/../uploads/gallery/".$data['picture_name'],
                                                    ['width' => '150px', 'max-height'=>'150px']);
                                            },
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'contentOptions' =>['style' => 'width:10%'],
                                            'template' => '{view} {delete}',
                                            'buttons' => [
                                                'delete' => function ($url, $model) {
                                                    return (Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            false,
                                                            [
                                                                'class'          => 'ajaxDelete',
                                                                'delete-url'     => $url,
                                                                'pjax-container' => 'pjax-list',
                                                                'title'          => Yii::t('app', 'Delete')
                                                            ]
                                                        ));
                                                }
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                        </div>
                        <div class="tab-pane" id="portlet_tab_2">
                                <?php $form = ActiveForm::begin(); ?>
                                <?= FileUploadUI::widget([
                                    'model' => $model,
                                    'attribute' => 'picture_name',
                                    'url' => ['gallery/image-upload', 'id' => $id, 'type' => $type],

                                    'fieldOptions' => [
                                    'accept' => 'image/*'
                                    ],
                                    'clientOptions' => [
                                    'maxFileSize' => 2000000
                                    ],
                        // ...
                                    'clientEvents' => [
                                    'fileuploaddone' => 'function(e, data) {
                                        console.log(e);
                                        console.log(data);
                                    }',
                                    'fileuploadfail' => 'function(e, data) {
                                        console.log(e);
                                        console.log(data);
                                    }',
                                    ],
                                    ]);
                                    ?>
                                    <?php ActiveForm::end(); ?>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

                      
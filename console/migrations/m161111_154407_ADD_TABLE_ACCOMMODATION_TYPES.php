<?php

use yii\db\Migration;

class m161111_154407_ADD_TABLE_ACCOMMODATION_TYPES extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `accommodation_types`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(512) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; ");
    }

    public function down()
    {
        echo "m161111_154407_ADD_TABLE_ACCOMMODATION_TYPES cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

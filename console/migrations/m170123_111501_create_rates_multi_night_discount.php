<?php

use yii\db\Migration;

class m170123_111501_create_rates_multi_night_discount extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `rates_multi_night_discount`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `rates_id` INT(11), `nights` INT(11), `percent` INT(11), `per_night` INT(11), `once_off` INT(11), `price_cap` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_rates_multi_night_discount_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170123_111501_create_rates_multi_night_discount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

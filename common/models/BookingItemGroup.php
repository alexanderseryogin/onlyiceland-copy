<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_item_group".
 *
 * @property string $id
 * @property string $booking_item_id
 * @property string $booking_group_id
 *
 * @property BookingGroup $bookingGroup
 * @property BookingsItems $bookingItem
 */
class BookingItemGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_item_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_item_id', 'booking_group_id'], 'integer'],
            [['booking_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroup::className(), 'targetAttribute' => ['booking_group_id' => 'id']],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItems::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'booking_item_id' => Yii::t('app', 'Booking Item ID'),
            'booking_group_id' => Yii::t('app', 'Booking Group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroup()
    {
        return $this->hasOne(BookingGroup::className(), ['id' => 'booking_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItems::className(), ['id' => 'booking_item_id']);
    }
}

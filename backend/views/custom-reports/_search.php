<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CustomReportsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="custom-reports-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'report_title') ?>

    <?= $form->field($model, 'report_color') ?>

    <?= $form->field($model, 'report_icon') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'arrival_date_to') ?>

    <?php // echo $form->field($model, 'arrival_date_from') ?>

    <?php // echo $form->field($model, 'departure_date_to') ?>

    <?php // echo $form->field($model, 'departure_date_from') ?>

    <?php // echo $form->field($model, 'booking_date_to') ?>

    <?php // echo $form->field($model, 'booking_date_from') ?>

    <?php // echo $form->field($model, 'sort_by') ?>

    <?php // echo $form->field($model, 'show_max') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

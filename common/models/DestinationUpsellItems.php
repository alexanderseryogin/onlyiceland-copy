<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "provider_upsell_items".
 *
 * @property string $id
 * @property integer $provider_id
 * @property integer $upsell_id
 * @property integer $price
 *
 * @property Providers $provider
 * @property UpsellItems $upsell
 */
class DestinationUpsellItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provider_upsell_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'upsell_id'], 'integer'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['upsell_id'], 'exist', 'skipOnError' => true, 'targetClass' => UpsellItems::className(), 'targetAttribute' => ['upsell_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
            'upsell_id' => Yii::t('app', 'Upsell ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpsell()
    {
        return $this->hasOne(UpsellItems::className(), ['id' => 'upsell_id']);
    }
}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingStatusesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Booking Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-statuses-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BOOKING STATUS'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id'=>'statuses-grid','timeout' => 1000000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'label',
                'value' => function($data)
                {
                    return $data->label;
                },
                'contentOptions' => function($data)
                {
                    return ['style' => 'color:'.$data->text_color.'; background:'.$data->background_color.';' ];
                },
            ],
            'background_color',
            'text_color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php

use yii\db\Migration;

class m170222_093028_alter_pricing_option_in_destination extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_policies` DROP COLUMN `pricing`; 
            ALTER TABLE `providers` ADD COLUMN `pricing` TINYINT(4) NULL AFTER `star_rating`; ");
    }

    public function down()
    {
        echo "m170222_093028_alter_pricing_option_in_destination cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

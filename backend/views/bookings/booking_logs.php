<?php
use common\models\BookingsLog;

$logs = BookingsLog::find()->where(['booking_id' => $model->id])->orderby(['created_at' => 'SORT_ASC'])->all();

?>


<div>
	<table id="logs-table" class="table">
	    <thead>
	      <tr>
	        <!-- <th>Id </th> -->
	        <th style="width: 10%">Time</th>
	        <th style="width: 10%;">User</th>
	        <th style="width: 20%;">Setting</th>
	        <th style="width: 30%;">Old Value</th>
	        <th style="width: 30%;">New Value</th>
	    
	      </tr>
	    </thead>
	    <tbody >
	    	<?php
	    		if(!empty($logs))
	    		{
	    			foreach ($logs as $log) 
	    			{
	    				echo "<tr>";
	    				echo "<td>".$log->created_at."</td>";
	    				echo "<td>".$log->user->username."</td>";
	    				echo "<td>".$log->setting."</td>";
	    				echo "<td>".$log->old_value."</td>";
	    				echo "<td>".$log->new_value."</td>";
	    				echo "</tr>";
	    			}
	    		}
	    		else
	    		{
	    			// echo "<h3>No Logs for this booking</h3>";
	    		}
	    	?>
	    </tbody>
	</table>
</div>

<?php

$this->registerJs("
	$('#logs-table').DataTable( {
	    'columnDefs': [
	    { 'width': '10%' },
	    { 'width': '10%' },
	    { 'width': '20%' },
	    { 'width': '30%' },
	    { 'width': '30%' },
	  ]
	} );
");

	

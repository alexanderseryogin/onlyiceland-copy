<?php

use yii\db\Migration;

class m170802_073815_create_table_bookings_upsell_items_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `bookings_upsell_items_cart` (
                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                      `booking_date_id` int(11) unsigned DEFAULT NULL,
                      `upsell_id` int(11) DEFAULT NULL,
                      `price` int(11) DEFAULT NULL,
                      `extra_bed_quantity` int(11) DEFAULT NULL,
                      PRIMARY KEY (`id`),
                      CONSTRAINT `FK_bookings_upsell_items_cart_upsell_id` FOREIGN KEY (`upsell_id`) REFERENCES `upsell_items` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
                      CONSTRAINT `FK_bui_bdate_cart_id` FOREIGN KEY (`booking_date_id`) REFERENCES `booking_dates_cart` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
                    ) ENGINE=InnoDB AUTO_INCREMENT=854 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
                    ');
    }

    public function down()
    {
        echo "m170802_073815_create_table_bookings_upsell_items_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170113_111855_alter_rates_amenities_add_can_be_banner extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates_amenities` ADD COLUMN `can_be_banner` TINYINT(1) NULL AFTER `type`; ");
    }

    public function down()
    {
        echo "m170113_111855_alter_rates_amenities_add_can_be_banner cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

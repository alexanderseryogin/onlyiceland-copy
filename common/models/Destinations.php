<?php

namespace common\models;

use Yii;
use common\models\PropertyPaymentMethods;
use common\models\DestinationsStaff;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "providers".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property integer $provider_category_id
 * @property integer $provider_type_id
 * @property integer $provider_admin_id
 * @property string $phone
 * @property string $fax
 * @property string $booking_email
 * @property string $website
 * @property string $kennitala
 * @property string $address
 * @property integer $country_id
 * @property integer $city_id
 * @property integer $state_id
 * @property string $postal_code
 * @property string $latitude
 * @property string $longitude
 * @property string $business_name
 * @property string $business_address
 * @property integer $business_country_id
 * @property integer $business_city_id
 * @property integer $business_state_id
 * @property string $business_postal_code
 * @property string $vat_id
 *
 * @property BookingPolicies[] $bookingPolicies
 * @property User $providerAdmin
 * @property Cities $businessCity
 * @property Countries $businessCountry
 * @property States $businessState
 * @property ProviderCategories $providerCategory
 * @property Cities $city
 * @property Countries $country
 * @property Regions $region
 * @property States $state
 * @property ProviderTypes $providerType
 * @property ProvidersFinancial[] $providersFinancials
 */
class Destinations extends \yii\db\ActiveRecord
{
    public $same_address;
    public $providers_staff;
    public static $all_special_requests='';
    public $special_requests;

    public $server_phone='';
    public $common_server_phone='';
    public $server_fax='';
    public $destination_title='';
    
    public $providers_tags;
    public $suitable_tags;
    public $difficulty_tags;
    public $cuisine;
    public $dish;
    public $type;
    public $dietary_restriction;
    public $meal;
    public $price;
    public $feature;
    public $alcohol;
    public $good_for;

    public $upsell_items='';

    public $free_amenities;
    public $extra_amenities;
    public $not_amenities;
    public $amenities;
    public $amenities_banners;
    public $transparent_background;

    public $image;

    public $resturant_tags;

    // use to get selected value in dropdown
    public $year='',$month='',$week_day='',$month_day='';
    public $day_setting='';

    // use to display values in dropdown
    public static $years = 
    [
        '2016'=> '2016',
        '2017'=> '2017',
        '2018'=> '2018',
        '2019'=> '2019',
        '2020'=> '2020',
        '2021'=> '2021',
        '2022'=> '2022',
        '2023'=> '2023',
        '2024'=> '2024',
        '2025'=> '2025',
        '2026'=> '2026'
    ];

    public static $week_days = 
    [
        '1'=>'Monday',
        '2'=>'Tuesday',
        '3'=>'Wednesday',
        '4'=>'Thursday',
        '5'=>'Friday',
        '6'=>'Saturday',
        '7'=>'Sunday'
    ];

    public static $generalBookingCancellation = [

        '30' => '1 month before check in',
        '42' => '6 weeks before check in',
        '60' => '2 months before check in',
        '75' => '10 weeks before check in',
        '90' => '3 months before check in',
        '120' => '4 months before check in',
        '150' => '5 months before check in',
        '180' => '6 months before check in',
        '210' => '7 months before check in',
        '240' => '8 months before check in',
        '270' => '9 months before check in',
        '300' => '10 months before check in',
        '330' => '11 months before check in',
        '360' => '12 months before check in',
    ];

    public static $pricing = [
        0 => 'First/Additional',
        1 => 'Capacity pricing',
    ];

    public static $month_days= '';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['same_address','transparent_background'], 'boolean'],
            [['name','provider_admin_id','provider_type_id','phone','address','city_id','country_id','state_id','postal_code','description'],'required'],

            [['provider_admin','booking_email','kennitala'],'required',
            'when' => function ($model) 
            {
                if(isset($model->provider_type_id))
                {
                    $obj = DestinationTypes::findOne(['id' => $model->provider_type_id]);
                    return  $obj->name == 'Accommodation';
                }
            },
            'whenClient' => "function (attribute, value) {
                return $('#select2-provider-type-dropdown-container').attr('title') == 'Accommodation';
            }"],

            [['background_color', 'text_color'], 'string', 'max' => 11],
            [['url'], 'string'],
            ['url', 'url', 'defaultScheme' => 'http'],

            [['address', 'business_address','server_fax','server_phone','common_server_phone','destination_title','featured_image','description', 'vat_id'], 'string'],
            [['banner','offers_accommodation','star_rating','pricing'],'integer'],
            [['active','use_housekeeping'],'safe'],
            [['latitude', 'longitude'], 'number'],
            [['name', 'event_place','phone', 'fax', 'booking_email', 'website', 'kennitala', 'postal_code', 'business_name', 'business_postal_code'], 'string', 'max' => 256],
            [['provider_admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['provider_admin_id' => 'id']],
            [['provider_admin'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['provider_admin' => 'id']],
            [['business_city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['business_city_id' => 'id']],
            [['business_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['business_country_id' => 'id']],
            [['business_state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['business_state_id' => 'id']],
            [['provider_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationCategories::className(), 'targetAttribute' => ['provider_category_id' => 'id']],
            [['provider_sub_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationSubCategories::className(), 'targetAttribute' => ['provider_sub_category_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['provider_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTypes::className(), 'targetAttribute' => ['provider_type_id' => 'id']],
            ['website', 'url', 'defaultScheme' => 'http'],
            [['accommodation_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccommodationTypes::className(), 'targetAttribute' => ['accommodation_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tag_id' => 'id']],
            [['booking_email'],'email'],

            ['accommodation_id', 'required', 'when' => function ($model) 
            {
                    return $model->getDestinationTypeName() == 'Accommodation';
            }, 'whenClient' => "function (attribute, value) {
                return $('#select2-provider-type-dropdown-container').attr('title')=='Accommodation';
            }"],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png','jpg']],
        ];
    }

    public function getAccomodation()
    {
        $acc = DestinationTypes::findOne(['name' => 'Accommodation']);
        return $acc->id;
    }

    public function getDestinationTypeName()
    {
        $acc = DestinationTypes::findOne(['id' => $this->provider_type_id]);
        return $acc->name;
    }

    public function getDestinationTags()
    {
        return $this->hasMany(DestinationsTags::className(), ['provider_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'same_address' => Yii::t('app', 'Use same address for business?'),
            'name' => Yii::t('app', 'Name of Destination'),
            'provider_category_id' => Yii::t('app', 'License Category'),
            'provider_sub_category_id' => Yii::t('app', 'License Sub-Category'),
            'provider_type_id' => Yii::t('app', 'Destination Type'),
            'accommodation_id' => Yii::t('app', 'Accommodation Type'),
            'tag_id' => Yii::t('app', 'Tags'),
            'provider_admin_id' => Yii::t('app', 'Owner'),
            'provider_admin' => Yii::t('app', 'Destination Admin'),
            'phone' => Yii::t('app', 'Phone'),
            'fax' => Yii::t('app', 'Fax'),
            'booking_email' => Yii::t('app', 'Booking Email'),
            'website' => Yii::t('app', 'Website'),
            'kennitala' => Yii::t('app', 'Kennitala'),
            'address' => Yii::t('app', 'Address'),
            'country_id' => Yii::t('app', 'Country'),
            'city_id' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'business_name' => Yii::t('app', 'Business Name'),
            'business_address' => Yii::t('app', 'Business Address'),
            'business_country_id' => Yii::t('app', 'Business Country'),
            'business_city_id' => Yii::t('app', 'Business City'),
            'business_state_id' => Yii::t('app', 'Business State'),
            'business_postal_code' => Yii::t('app', 'Business Postal Code'),
            'providers_staff' => Yii::t('app', 'Destination Staff'),
            'image' => Yii::t('app', 'Upload Featured Image'),
            'description' => Yii::t('app', 'Destination Description'),
            'providers_tags' => Yii::t('app', 'Descriptive Tags'),
            'suitable_tags' => Yii::t('app', 'Suitable Tags'),
            'difficulty_tags' => Yii::t('app', 'Difficulty Tags'),
            'transparent_background' => Yii::t('app', 'Transparent Background'),
            'pricing' => Yii::t('app', 'Pricing'),
            'vat_id' => Yii::t('app', 'VAT ID'),
            'resturant_tags' => Yii::t('app', 'Cuisines'),
        ];
    }

    public function getPaymentMethodsGuarantee() {
        return $this->hasMany(PropertyPaymentMethods::className(), ['id' => 'pm_id'])
          ->viaTable('payment_method_guarantee', ['provider_id' => 'id']);
    }

    public function getPaymentMethodsCheckin() {
        return $this->hasMany(PropertyPaymentMethods::className(), ['id' => 'pm_id'])
          ->viaTable('payment_method_checkin', ['provider_id' => 'id']);
    }

    public function getAccommodation() 
    { 
        return $this->hasOne(AccommodationTypes::className(), ['id' => 'accommodation_id']); 
    } 

    public function getTag() 
    { 
        return $this->hasOne(Tags::className(), ['id' => 'tag_id']); 
    } 

    public function getBookableItems()
    {
        return $this->hasMany(BookableItems::className(), ['provider_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingPolicies()
    {
        return $this->hasOne(BookingPolicies::className(), ['provider_id' => 'id']);
    }

    public function getBookingRules()
    {
        return $this->hasMany(BookingRules::className(), ['provider_id' => 'id']);
    }

    public function getDestinationsStaff()
    {
        return $this->hasMany(DestinationsStaff::className(), ['provider_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinationAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'provider_admin_id']);
    }

    public function getDestinationsAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'provider_admin']);
    }

    public function getDestinationsAmenities()
    {
        return $this->hasMany(DestinationsAmenities::className(), ['provider_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'business_city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'business_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessState()
    {
        return $this->hasOne(States::className(), ['id' => 'business_state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinationCategory()
    {
        return $this->hasOne(DestinationCategories::className(), ['id' => 'provider_category_id']);
    }
    
    public function getDestinationSubCategory()
    {
        return $this->hasOne(DestinationSubCategories::className(), ['id' => 'provider_sub_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinationType()
    {
        return $this->hasOne(DestinationTypes::className(), ['id' => 'provider_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinationsFinancials()
    {
        return $this->hasOne(DestinationsFinancial::className(), ['provider_id' => 'id']);
    }

    public function getDestinationtravelpartners()
    {
        return $this->hasMany(DestinationTravelPartners::className(), ['destination_id' => 'id']);
    }

    public function getDestinationAddOns()
    {
        return $this->hasMany(DestinationAddOns::className(), ['destination_id' => 'id']);
    }

    public static function getBookingCancellationArray()
    {
        $booking_cancellation_arr = [];

        $booking_cancellation_arr['always'] = "Always";
        $booking_cancellation_arr['1'] = "1 day before check in";

        for ($i=2; $i <= 29 ; $i++) 
        {
            $booking_cancellation_arr[$i] = $i.' days before check in';
        }

        $booking_cancellation_arr['30'] = '1 month before check in';
        $booking_cancellation_arr['45'] = '6 weeks before check in';
        $booking_cancellation_arr['60'] = '2 months before check in';
        $booking_cancellation_arr['75'] = '10 weeks before check in';
        $booking_cancellation_arr['90'] = '3 months before check in';
        $booking_cancellation_arr['120'] = '4 months before check in';
        $booking_cancellation_arr['150'] = '5 months before check in';
        $booking_cancellation_arr['180'] = '6 months before check in';
        $booking_cancellation_arr['210'] = '7 months before check in';
        $booking_cancellation_arr['240'] = '8 months before check in';
        $booking_cancellation_arr['270'] = '9 months before check in';
        $booking_cancellation_arr['300'] = '10 months before check in';
        $booking_cancellation_arr['330'] = '11 months before check in';
        $booking_cancellation_arr['360'] = '12 months before check in';
        $booking_cancellation_arr['never'] = "Never";

        return $booking_cancellation_arr;
    }

    public static function getWestIcelandTowns()
    {
        $stateModel = States::findOne(['name' => 'Vesturland']);
        return $stateModel->cities;
    }

    public static function getSouthIcelandTowns()
    {
        $stateModel = States::findOne(['name' => 'Suðurland']);
        return $stateModel->cities;
    }

    public static function getEastIcelandTowns()
    {
        $stateModel = States::findOne(['name' => 'Austurland']);
        return $stateModel->cities;
    }

    public static function getNorthWestIcelandTowns()
    {
        $stateModel = States::findOne(['name' => 'Norðurland vestra']);
        return $stateModel->cities;
    }

    public static function getCapitalAreaTowns()
    {
        $stateModel = States::findOne(['name' => 'Höfuðborgarsvæðið']);
        return $stateModel->cities;
    }

    public static function getAirportTowns()
    {
        $stateModel = States::findOne(['name' => 'Suðurnes']);
        return $stateModel->cities;
    }

    public static function getWestFjordsTowns()
    {
        $stateModel = States::findOne(['name' => 'Vestfirðir']);
        return $stateModel->cities;
    }

    public static function getNorthEastTowns()
    {
        $stateModel = States::findOne(['name' => 'Norðurland eystra']);
        return $stateModel->cities;
    }
}

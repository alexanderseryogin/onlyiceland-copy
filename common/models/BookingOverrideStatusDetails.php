<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_override_status_details".
 *
 * @property integer $id
 * @property string $date
 * @property integer $bookable_item_id
 * @property string $bookable_item_name_id
 * @property integer $booking_override_status_id
 *
 * @property BookableItems $bookableItem
 * @property BookableItemsNames $bookableItemName
 * @property BookingOverrideStatuses $bookingOverrideStatus
 */
class BookingOverrideStatusDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_override_status_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['bookable_item_id', 'bookable_item_name_id', 'booking_override_status_id','destination_id'], 'integer'],
            [['bookable_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_item_id' => 'id']],
            [['bookable_item_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItemsNames::className(), 'targetAttribute' => ['bookable_item_name_id' => 'id']],
            [['booking_override_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingOverrideStatuses::className(), 'targetAttribute' => ['booking_override_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'bookable_item_id' => 'Bookable Item ID',
            'bookable_item_name_id' => 'Bookable Item Name ID',
            'booking_override_status_id' => 'Booking Override Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItemName()
    {
        return $this->hasOne(BookableItemsNames::className(), ['id' => 'bookable_item_name_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingOverrideStatus()
    {
        return $this->hasOne(BookingOverrideStatuses::className(), ['id' => 'booking_override_status_id']);
    }

    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'destination_id']);
    }
}

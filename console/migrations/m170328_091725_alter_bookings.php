<?php

use yii\db\Migration;

class m170328_091725_alter_bookings extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` DROP COLUMN `travel_partner_id`, DROP COLUMN `offers_id`, DROP INDEX `FK_bookings_offers_id`, DROP INDEX `FK_bookings_travel_partner_id`, DROP FOREIGN KEY `FK_bookings_offers_id`, DROP FOREIGN KEY `FK_bookings_travel_partner_id`; 
        ");
    }

    public function down()
    {
        echo "m170328_091725_alter_bookings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

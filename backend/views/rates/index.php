<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

use common\models\BookableItems;
use common\models\Destinations;
use common\models\RatesDates;
use common\models\RatesCapacityPricing;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RatesSearch */
/* @var $dataDestination yii\data\ActiveDataDestination */

$this->title = Yii::t('app', 'Rates');
$this->params['breadcrumbs'][] = $this->title;

$update_page_url = Url::to(['/rates/update']);
$red = '#ff0000';
$green = '#00ff00';
$Checkbox = [
    0 => '',
    1 => 'checked'
];
$ratesUrl = Url::to(['/rates/index']);
?>

<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>
<script type="text/javascript">
    var str=res='';
    var arr= new Array();
</script>

<div class="main-div">
    <div class="rates-index">

        
        
        <!-- <input name="Bookings[booking_status][]"  value="'.$value->id.'" style="margin-left:10px;" type="checkbox" '.$Checkbox[$checked].'>
        <input name="Bookings[booking_status][]"  value="'.$value->id.'" style="margin-left:10px;" type="checkbox" '.$Checkbox[$checked].'> -->
        <p>
            <?= Html::a(Yii::t('app', 'ADD NEW RATE'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
        </p>
       
        <div class="row" >
            <div class="col-md-2">
                 <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="col-md-2" style="top:30px;">
                <label><b>Show Expired Rates</b></label>
                <input class="cus_search " name="expired_rates"   style="margin-left:10px;" type="checkbox" <?=isset($params['expired_rates'])?"checked":"" ?> >
            </div>
            <div class="col-md-2" style="top:30px;">
                <label><b>Show Current Rates</b></label>
                <input class="cus_search " name="current_rates"   style="margin-left:10px;" type="checkbox" <?=isset($params['current_rates'])?"checked":"" ?> >
            </div>
            <div class="col-md-2" style="top:30px;">
                <label><b>Show Future Rates</b></label>
                <input class="cus_search" name="future_rates"  style="margin-left:10px;" type="checkbox" <?=isset($params['future_rates'])?"checked":""?> >
            </div>
        </div>

    <?php Pjax::begin(['id' => 'rates-gridview','timeout' => 1000000, 'enablePushState' => false, 'class' => 'pjaxContainer']); ?>    
        

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['class' => 'grid-view table-scrollable'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                [
                    'attribute' => 'provider_id',
                    'label' => 'Destination Name',
                    'value' => function($data)
                    {
                        if(isset($data->destination->name) && !empty($data->destination->name))
                            return $data->destination->name;
                        else
                            return '';
                    },
                    'filter' => Html::dropDownList(
                        'RatesSearch[provider_id]', 
                        $searchModel['provider_id'], 
                        ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                        [
                            'prompt' => 'Select a Destination', 
                            'id'=>'provider_id', 
                            'class'=>'form-control'
                        ]),
                    'headerOptions' =>['class' =>'grid_sort', 'style' => 'min-width:250px;'],
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'min-width:250px;color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                    'filterInputOptions' => ['id' => 'provider_id','class' => 'form-control'],
                ],            
                [
                    'attribute' => 'name',
                    'label' => 'Rate Name',
                    'value' => function($data)
                    {
                        if(isset($data->name) && !empty($data->name))
                            return $data->name;
                        else
                            return '';
                    },
                    'headerOptions' =>['class' =>'grid_sort','style' => 'min-width:250px;'],
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'min-width:250px;color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                    'filterInputOptions' => ['id' => 'name','class' => 'form-control'],
                ],
                [
                    'attribute' => 'unit_id',
                    'label' => 'Item Name',
                    'value' => function($data)
                    {
                        if(isset($data->unit->itemType->name) && !empty($data->unit->itemType->name))
                            return $data->unit->itemType->name;
                        else
                            return '';
                    },
                    'filter' => Html::dropDownList(
                        'RatesSearch[unit_id]', 
                        $searchModel['unit_id'], 
                        ArrayHelper::map(BookableItems::find()->where(['provider_id' => $searchModel['provider_id']])->all(),'id','itemType.name'), 
                        [
                            'prompt' => 'Select an Item', 
                            'id'=>'unit_id', 
                            'class'=>'form-control'
                        ]),
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'min-width:300px;color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                    'headerOptions' =>['class' =>'grid_sort','style' => 'min-width:300px;'],
                    'filterInputOptions' => ['id' => 'unit_id','class' => 'form-control'],
                ],
                [
                    'attribute' => 'from_to_period',
                    'label' => 'Rate Period',
                    'value' => function($data)
                    {
                        $date_1 = RatesDates::find()->where(['rate_id' => $data->id])->orderBy('night_rate ASC')->one();
                        $date_2 = RatesDates::find()->where(['rate_id' => $data->id])->orderBy('night_rate DESC')->one();
                        if(!empty($date_1) && !empty($date_2))
                            return date('d/m/Y',strtotime($date_1->night_rate)).'-'.date('d/m/Y',strtotime($date_2->night_rate));
                        else
                            return '';
                    },
                    //'filter' => '<input type="text" class="form-control" name="RatesSearch[from_to_period]" id="daterange" />',
                    'headerOptions' =>['style' => 'min-width:200px;'],
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'min-width:150px; text-align: center; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'published',
                    'label' => 'PUB',
                    'value' => function($data)
                    {
                        if($data->published == 1)
                            return ' ';
                        else
                            return ' ';
                    },
                    //'filter' => '<input type="text" class="form-control" name="RatesSearch[from_to_period]" id="daterange" />',
                    'headerOptions' =>['style' => 'min-width:10px;'],
                    'contentOptions' => function($data)
                    {
                        if($data->published == 1)
                        {
                            return ['style' => 'min-width:10px; text-align: center; color:'.$data->unit->text_color.'; background:#00ff00;' ];
                        }
                        else
                        {
                            return ['style' => 'min-width:10px; text-align: center; color:'.$data->unit->text_color.'; background:#ff0000;' ];
                        }
                        
                    },
                ],
                [
                    'attribute' => 'vat_rate',
                    'label' => 'VAT%',
                    // 'format' => ['decimal'],
                    'value' => function($data)
                    {
                        if(isset($data->vat_rate))
                        {
                            
                            return str_replace('.', ',', $data->vat_rate);
                        }
                        else
                        {
                            return '- - -';
                        }
                        
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right;color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'lodging_tax_per_night',
                    'label' => 'GNT',
                    // 'format' => ['decimal'],
                    'value' => function($data)
                    {
                        if(isset($data->lodging_tax_per_night))
                        {
                            return Yii::$app->formatter->asDecimal($data->lodging_tax_per_night, "ISK");
                        }
                        else
                        {
                            return '- - -';
                        }
                        
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right;color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_first_adult',
                    'label' => 'SINGLE',
                    // 'format' => ['decimal'],
                    'value' => function($data)
                    {
                        if($data->destination->pricing == 1)
                        {
                            $rcp = RatesCapacityPricing::findOne(['rates_id' => $data->id, 'person_no' => 1 ]);
                            return (!empty($rcp))?$rcp->price:'- - -';
                        }
                        else
                        {
                            return '- - -';
                        }
                        
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_first_adult',
                    'label' => 'DOUBLE',
                    //'format' => ['decimal'],
                    'value' => function($data)
                    {
                        if($data->destination->pricing == 1)
                        {
                            $rcp = RatesCapacityPricing::findOne(['rates_id' => $data->id, 'person_no' => 2 ]);
                            return (!empty($rcp))?$rcp->price:'- - -';
                        }
                        else
                        {
                            return '- - -';
                        }
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_first_adult',
                    'label' => 'TRIPLE',
                    //'format' => ['decimal'],
                    'value' => function($data)
                    {
                        if($data->destination->pricing == 1)
                        {
                            $rcp = RatesCapacityPricing::findOne(['rates_id' => $data->id, 'person_no' => 3 ]);
                            return (!empty($rcp))?$rcp->price:'- - -';
                        }
                        else
                        {
                            return '- - -';
                        }
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_first_adult',
                    'label' => 'QUAD',
                   // 'format' => ['decimal'],
                    'value' => function($data)
                    {
                        if($data->destination->pricing == 1)
                        {
                            $rcp = RatesCapacityPricing::findOne(['rates_id' => $data->id, 'person_no' => 4 ]);
                            return (!empty($rcp))?$rcp->price:'- - -';
                        }
                        else
                        {
                            return '- - -';
                        }
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_first_adult',
                    'label' => 'First AD',
                   // 'format' => ['decimal'],
                    'value' => function($data)
                    {
                        return ($data->item_price_first_adult == NULL)?'- - -':$data->item_price_first_adult;
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_additional_adult',
                    'label' => 'Add’l AD',
                    //'format' => ['decimal'],
                    'value' => function($data)
                    {
                        return ($data->item_price_additional_adult == NULL)?'- - -':$data->item_price_additional_adult;
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_first_child',
                    'label' => 'First CH',
                    //'format' => ['decimal'],
                    'value' => function($data)
                    {
                        return ($data->item_price_first_child == NULL)?'- - -':$data->item_price_first_child;
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'item_price_additional_child',
                    'label' => 'Add’l CH',
                  //  'format' => ['decimal'],
                    'value' => function($data)
                    {
                        return ($data->item_price_additional_child == NULL)?'- - -':$data->item_price_additional_child;
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'min_price',
                    'label' => 'Min Price',
                    'format' => ['decimal'],
                    'value' => function($data)
                    {
                        return $data->min_price;
                    },
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'width:1%;text-align: right; color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],
                [
                    'attribute' => 'min_max_booking',
                    'header' => 'MIN <span class="fa fa-arrow-right"></span> MAX',
                    'value' => function($data)
                    {
                        if(!empty($data->min_max_booking))
                        {
                            $arr = explode(';', $data->min_max_booking);
                            return $arr[0].'<span class="fa fa-arrow-right"></span>'.$arr[1];
                        }
                        return '';
                    },
                    'format' => 'raw',
                    'headerOptions' =>['style' => 'width:1%;'],
                    'contentOptions' => function($data)
                    {
                        return ['style' => 'color:'.$data->unit->text_color.'; background:'.$data->unit->background_color.';' ];
                    },
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' =>['style' => 'min-width:70px;'],
                    'contentOptions' =>['style' => 'min-width:100px;'],

                    'template' => '{duplicate}{copy}{view}{update}{delete}',
                    'buttons' => [
                                    'duplicate' => function ($url, $model, $key) {
                                            return '<a class="duplicate" style="text-decoration: none;" aria-label="duplicate" data-id='.$model->id.' title="duplicate" name="duplicate[]">
                                                        <span class="glyphicon glyphicon-copy"></span>
                                                    </a>';
                                        },
                                    'copy' => function ($url, $model, $key) {
                                        return '<a style="text-decoration: none;" aria-label="copy" data-id='.$model->id.' title="copy" name="copy[]">
                                                    <i class="fa fa-files-o" aria-hidden="true"></i>
                                                </a>';

                                    },
                                    'delete' => function ($url, $data)
                                    {   
                                        
                                        return '<a data-pjax="#rates-gridview" aria-label="Delete" data-confirm="Are you sure you want to delete?" title="Delete" href="'.$url.'">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        </a>';
                                        
                                    },
                                   
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?></div>
</div>
<script type="text/javascript">
    var path = "<?= Url::to(['rates/duplicate-rate']) ?>";
    var redirect = "<?= Yii::getAlias('@web').'/rates/update?id='?>";

</script>

<?php
// $searchUrl = yii::$app->urlManager()->createUrl('rates/index');

$this->registerJs("
jQuery(document).ready(function() {
    //App.blockUI({target:'.main-div', animate: !0});
    history.pushState(null, '', location.href.split('?')[0]);

    var filtersId = ['name','provider_id','unit_id'];
    var url = getUrl(filtersId, 'RatesSearch', '20');
    var counter=0;
    $('.cus_search').each(function(i, obj) {
        if($(this).is(':checked'))
        {
            if(counter == 0)
            {
                url=url+'&'+$(this).attr('name')+'='+'1';
            }
            else
            {
                url=url+'&'+$(this).attr('name')+'='+'1';
            }
            counter++;
        }
    });             
    $.pjax.defaults.timeout = false;
    console.log(url);

    $.pjax.reload({container: '#rates-gridview', url: url});

    // $('.duplicate').parent().addClass('cancel');
    var pageLoadCounter = 0;
    var MAX_PAGE_LOADS = 20;
    $('#rates-gridview').on('pjax:success', function (e, xhr, settings) {
        // // URI can be found at https://github.com/medialize/URI.js
        // var uri = URI(settings.url);

        // // Remove _pjax from query string before reloading
        // uri.removeSearch('_pjax');

        // location.href = uri.toString();
        // alert('here')

        // return false;
        // alert($(document).loaction.hash);
        // document.location.hash = '';
        console.log(window.location.href);
        if (window.location.href.indexOf('?') > -1) {
            history.pushState('', document.title, window.location.pathname);
        }
        App.unblockUI('.main-div');
        //return false;

    
    });
    function getUrl(idsArr, modelName, perPage) {
        console.log('idsArr '+idsArr);
        console.log('modelName '+modelName);
        console.log('perPage '+perPage);
        var pageNo = $('.pagination .active a').html();
        if(!pageNo)
            pageNo = 1;
        var url = '".$ratesUrl."'+'?page='+pageNo+'&per-page='+perPage;
        for (i = 0; i < idsArr.length; i++) {
            var value = $( '#'+idsArr[i] ).val();
            if(value)

              url += '&'+modelName+'['+idsArr[i]+']='+value;
        }
        $('.grid_sort').each(function(){
           // console.log($(this).children().first());
            console.log('sort by :'+$(this).children().first().attr('data-sort'))
            if($(this).children().first().hasClass('asc') || $(this).children().first().hasClass('desc') )
            {
                console.log('in sort if');
                var var_sort = $(this).children().first().attr('data-sort');
                if(var_sort.indexOf('-') >= 0)
                {
                    var_sort = var_sort.substring(1);
                }
                else
                {
                    var_sort = '-'+var_sort;
                }
                url += '&sort='+var_sort;
            }
            else
            {
                console.log('in sort else');
            } 
        });
        console.log('url after sort'+url)
        return url;
    }

    $(document).on('change','.cus_search',function(){
        App.blockUI({target:'.main-div', animate: !0});
        history.pushState(null, '', location.href.split('?')[0]);

        var filtersId = ['name','provider_id','unit_id'];
        var url = getUrl(filtersId, 'RatesSearch', '20');
        console.log('url : '+url);
        var counter=0;
        $('.cus_search').each(function(i, obj) {
            if($(this).is(':checked'))
            {
                if(counter == 0)
                {
                   url=url+'&'+$(this).attr('name')+'='+'1';
                }
                else
                {
                    url=url+'&'+$(this).attr('name')+'='+'1';
                }
                counter++;
            }
        });             
        $.pjax.defaults.timeout = false;
        console.log(url);

        $.pjax.reload({container: '#rates-gridview', url: url});
       // $('.grid-view').yiiGridView('update');
        $('.duplicate').parent().addClass('cancel');
                //document.location.hash = '';
    });

    var date = new Date();

    $('#daterange').daterangepicker(
    {
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear(),
        endDate: (date.getDate()+1)+'/'+(date.getMonth()+1)+'/'+date.getFullYear(),
    });

    $('a[name=\"duplicate[]\"]').click(function()
    {
        console.log('clicked : duplicate');
        data_id = $(this).attr('data-id');

        var Object = {
                        data_id : data_id,
                        action: 'duplicate',
                    }
        $.ajax(
        {
            type: 'POST',
            url: path,
            data: Object,
            success: function(result)
            {
                //$.pjax.reload({container:'#rates-gridview'});  //Reload GridView
                App.blockUI({target:'.main-div', animate: !0});
                history.pushState(null, '', location.href.split('?')[0]);

                var filtersId = ['name','provider_id','unit_id'];
                var url = getUrl(filtersId, 'RatesSearch', '20');
                console.log('before duplicate url : '+url);
                var counter=0;
                $('.cus_search').each(function(i, obj) {
                    if($(this).is(':checked'))
                    {
                        if(counter == 0)
                        {
                            url=url+$(this).attr('name')+'='+'1';
                        }
                        else
                        {
                            url=url+'&'+$(this).attr('name')+'='+'1';
                        }
                        counter++;
                    }
                });             
                $.pjax.defaults.timeout = false;
                console.log(url);

                $.pjax.reload({container: '#rates-gridview', url: url});
                $('.grid-view').yiiGridView('applyFilter');
                $('.duplicate').parent().addClass('cancel');
            },
        });
    });

    $('a[name=\"copy[]\"]').click(function()
    {
        console.log('clicked : copy');
        data_id = $(this).attr('data-id');

        var Object = {
                        data_id : data_id,
                        action: 'copy',
                    }
        $.ajax(
        {
            type: 'POST',
            url: path,
            data: Object,
            success: function(result)
            {
                
            },
        });
    });

    $('#unit_id').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#provider_id').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#daterange').change(function()
    {
        str = $('#daterange').val();
        res = str.split('-');
        arr[0] = res[0];
        arr[1] = res[1];
    });

    $('#rates-gridview').on('pjax:end', function() {

        $('#daterange').daterangepicker(
        {
            locale: {
              format: 'DD/MM/YYYY'
            },
            startDate: date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear(),
            endDate: (date.getDate()+1)+'/'+(date.getMonth()+1)+'/'+date.getFullYear(),
        });

        $('#unit_id').select2({
        placeholder: \"Select a Type\",
        allowClear: true
        });

        $('#provider_id').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
        });

        $('a[name=\"duplicate[]\"]').click(function()
        {
            data_id = $(this).attr('data-id');

            var Object = {
                            data_id : data_id,
                            action: 'duplicate',
                        }
            $.ajax(
            {
                type: 'POST',
                url: path,
                data: Object,
                success: function(result)
                {
                    //$.pjax.reload({container:'#rates-gridview'});  //Reload GridView
                    App.blockUI({target:'.main-div', animate: !0});
                    history.pushState(null, '', location.href.split('?')[0]);

                    var filtersId = ['name','provider_id','unit_id'];
                    var url = getUrl(filtersId, 'RatesSearch', '20');
                    console.log('before duplicate url : '+url);
                    var counter=0;
                    $('.cus_search').each(function(i, obj) {
                        if($(this).is(':checked'))
                        {
                            if(counter == 0)
                            {
                                url=url+$(this).attr('name')+'='+'1';
                            }
                            else
                            {
                                url=url+'&'+$(this).attr('name')+'='+'1';
                            }
                            counter++;
                        }
                    });             
                    $.pjax.defaults.timeout = false;
                    console.log(url);

                    $.pjax.reload({container: '#rates-gridview', url: url});
                    $('.grid-view').yiiGridView('applyFilter');
                    $('.duplicate').parent().addClass('cancel');
                },
            });
        });

        $('a[name=\"copy[]\"]').click(function()
        {
            data_id = $(this).attr('data-id');

            var Object = {
                            data_id : data_id,
                            action: 'copy',
                        }
            $.ajax(
            {
                type: 'POST',
                url: path,
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#rates-gridview'});  //Reload GridView
                    $('.duplicate').parent().addClass('cancel');
                },
            });
        });

    });
    $(document).on('click','.table-striped tbody tr td',function()
    {
        //console.log('here');
        //console.log($(this).next());       
        if($(this).children().eq(0).hasClass('duplicate'))
        {
                console.log('in if');
        }
        else
        {
            console.log('in else');
            window.location.replace('$update_page_url?id='+$(this).parent().attr('data-key'));
        }
        
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>
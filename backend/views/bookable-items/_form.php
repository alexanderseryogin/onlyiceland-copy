<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Pjax;

Use common\models\Destinations;
use common\models\BookableItemTypes;
use common\models\BookableItemsImages;
use common\models\Amenities;
use common\models\SleepingArrangements;
use common\models\BookingConfirmationTypes;
use common\models\BookingStatuses;
use common\models\BookingTypes;
use common\models\BookableItems;
use common\models\HousekeepingStatus;

// echo "<pre>";
// print_r($model->housekeeping_status_array);
// exit();
// ********** Maintain state of previous tabs
// echo "<pre>";
//                                                     print_r(HousekeepingStatus::find()->all());
//                                                     exit();
// echo "<pre>";
// print_r($tab_href);
// exit();
if(!empty($tab_href))
{
    $this->registerJs("
        $('.bookable_tabs li').each(function()
        {
            var href_val = $(this).find('a').attr('href');
            console.log(href_val);
            var tab_activate = '$tab_href';
            console.log(tab_activate);
            $('#tab_href').val(tab_activate);

            if(href_val==tab_activate)
            {
                $(this).addClass('active');
                $(this).find('a').attr('aria-expanded','true');
                $(tab_activate).addClass('active');
            }
            else
            {
                $(this).removeClass('active');
                $(this).find('a').attr('aria-expanded','false');
                $(href_val).removeClass('active');
            }
        });
    ");
}

// ********** Maintain state of amenities

if(isset($model->general_booking_cancellation) && !empty($model->general_booking_cancellation))
{
    $this->registerJs('
            var temp = "'.$model->general_booking_cancellation.'";
            $("#booking_cancellation").val(temp).trigger("change");
        ');
}

if(!$model->isNewRecord)
{
    if(isset($model->housekeeping_status_array) && !empty($model->housekeeping_status_array))
    {

        $this->registerJs("
                var housekeeping_statuses_array = ".json_encode($model->housekeeping_status_array).";
                console.log(housekeeping_statuses_array);
                $('.housekeeping_status').select2().val(housekeeping_statuses_array).trigger('change');
            ");
    }
}



if($model->isAccommodation==1)
{
    $this->registerJs('
                        //$("#max_guests_adults_children_field").hide();
                        $("#bed_type_field").show();
                        $("#bed_combinations_field").show();
                        $("#sleeping_arrangement_field").show();'
                    );
}
else if($model->isAccommodation==0)
{
    $this->registerJs('
                        //$("#max_guests_adults_children_field").show();
                        $("#bed_type_field").hide();
                        $("#bed_combinations_field").hide();
                        $("#sleeping_arrangement_field").hide();
                    ');
}
else
{
    $this->registerJs("
                        $('#bed_type_field').hide();
                        $('#bed_combinations_field').hide();
                    ");
}

if($model->isNewRecord)
    $dataProviderBedType = '';
?>
<style>
    .hint-block{
        color:blue;
    }
    .preview_box
    {
        height: 35px; 
        width: 100px;
        font-size: 20px;
        padding-left: 10px;
        padding-top: 4px;
        margin-top: 25px;
    }
</style>

<script type="text/javascript">
var isNewRecord = '<?= $model->isNewRecord ?>';
</script>

<div class="bookable-items-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Bookable Items</span>
                        </div>
                        <ul class="nav nav-tabs bookable_tabs">
                            <li class="active">
                                <a href="#items" data-toggle="tab">Information</a>
                            </li>
                            <li>
                                <a href="#details" data-toggle="tab">Details</a>
                            </li>
                            <li>
                                <a href="#item_names" data-toggle="tab">Item Names</a>
                            </li>
                            <li>
                                <a href="#amenities" data-toggle="tab">Amenities</a>
                            </li>
                            <li>
                                <a href="#late-check-in" data-toggle="tab">Late Check-in Info</a>
                            </li>   
                            <li>
                                <a href="#photos" data-toggle="tab">Photos</a>
                            </li>

                        </ul>
                    </div>

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                        'options' => ['enctype' => 'multipart/form-data'],
                        'id' => 'bookable_form',
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <input type="text" name="BookableItems[tab_href]" id="tab_href" hidden="">

                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="items">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'provider_id', [
                                            'inputOptions' => [
                                                    'id' => 'provider_dropdown',
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%'
                                                    ]
                                        ])->dropdownList(ArrayHelper::map(Destinations::find()->all(),'id','name'),['prompt'=>'Select a Destination','onchange' => 'App.blockUI({target: ".bookable-items-form", animate: !0}); $.post("'.Yii::$app->urlManager->createUrl('bookable-items/list-destination-values?provider_id=').'"+$(this).val(), function( data ) 
                                            {
                                                data = JSON.parse(data);

                                                if(data.accommodation_type)
                                                {
                                                    $("#max_guests_adults_children_field").hide();
                                                    $("#bed_type_field").show();
                                                    $("#bed_combinations_field").show();
                                                    $("#sleeping_arrangement_field").show();
                                                }
                                                else
                                                {
                                                    $("#max_guests_adults_children_field").show();
                                                    $("#bed_type_field").hide();
                                                    $("#bed_combinations_field").hide();
                                                    $("#sleeping_arrangement_field").hide();
                                                }

                                                if(isNewRecord==1)
                                                {
                                                    $("#amenities_div").html(data.amenities);
                                                    $("#type_dropdown").val(data.flag).trigger("change");
                                                    $("#status_dropdown").val(data.status).trigger("change");
                                                    $("#confirmation_dropdown").val(data.confirmation).trigger("change");

                                                    // Call sliders update method with any params
                                                    var slider = $("#participation_age").data("ionRangeSlider");
                                                    slider.update({
                                                        from: data.yng,
                                                        to:data.olf,
                                                    });

                                                    if(data.general_booking!="Not Found")
                                                    {
                                                        $("#booking_cancellation").val(data.general_booking).trigger("change");
                                                    }
                                                    else
                                                    {
                                                        $("#booking_cancellation").val("always").trigger("change");
                                                    } 
                                                }
                                            }).done(function() {
                                                App.unblockUI(".bookable-items-form");
                                              })
                                              .fail(function() {
                                                App.unblockUI(".bookable-items-form");
                                                alert( "error" );
                                              });'
                                    ]) ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'item_type_id', [
                                            'inputOptions' => [
                                                    'id' => 'item_type_dropdown',
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%'
                                                    ]
                                        ])->dropdownList(ArrayHelper::map(BookableItemTypes::find()->all(),'id','name'),['prompt'=>'Select an Item Name']) ?>
                                    </div>
                                    <div class="col-xs-3" id="sleeping_arrangement_field">
                                        <?= $form->field($model, 'sleeping_arrangement_id', [
                                            'inputOptions' => [
                                                    'id' => 'sleeping_arrangement_dropdown',
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%'
                                                    ]
                                        ])->dropdownList(ArrayHelper::map(SleepingArrangements::find()->all(),'id','name'),['prompt'=>'Select a Sleeping Arrangement']) ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'bathroom', [
                                            'inputOptions' => [
                                                    'id' => 'bathroom_dropdown',
                                                    'class' => 'form-control',
                                                    'style' => 'width: 100%'
                                                    ]
                                        ])->dropdownList(BookableItems::$bathroom) ?>
                                    </div>
                                </div>

                                <?php
                                        $this->registerJs("
                                                
                                                option = '$model->how_booked';
                                                if(option=='')
                                                {
                                                    $('#booked_per_night').attr('checked','checked');
                                                }
                                                else
                                                {
                                                    $('#'+option).attr('checked','checked');
                                                }
                                            ");
                                ?>

                                <label class=" control-label">How Booked</label>
                                <div class="mt-radio-inline">

                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="booked_per_night" name="BookableItems[how_booked]" value="booked_per_night" > Booked Per Night
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="booked_per_day" name="BookableItems[how_booked]" value="booked_per_day" > Booked Per Day
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="booked_per_hour" name="BookableItems[how_booked]" value="booked_per_hour" > Booked Per Hour
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="booked_by_time" name="BookableItems[how_booked]" value="booked_by_time" > Booked By Start Time
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="booked_by_length" name="BookableItems[how_booked]" value="booked_by_length" > Booked By Length
                                        <span></span>
                                    </label>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <?= $form->field($model, 'description',[
                                                                'inputOptions' => [
                                                                    'class' => 'form-control',
                                                                    'style' => 'resize: none;',
                                                                    ]
                                                                ])->textarea(['rows' => 6]) ?>
                                    </div>
                                </div>

                                <?php
                                    if(empty($model->shortest_period))
                                        {
                                            $model->shortest_period = 1;
                                            $model->longest_period = 180;
                                        }

                                        $this->registerJs("
                                                    $('#bookable_periods').ionRangeSlider({
                                                        type:'double',
                                                        grid:!0,
                                                        min: 1,
                                                        max: 180,
                                                        from: $model->shortest_period,
                                                        to: $model->longest_period,
                                                    });
                                                ");
                                ?>

                                <label class=" control-label">Minimum and Maximum Bookable Period (in days)</label>
                                <div class="form-group">
                                    <input id="bookable_periods" type="text" name="BookableItems[bookable_periods]" />
                                </div>

                                <div class="row">
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'booking_confirmation_type_id', [
                                            'inputOptions' => [
                                                'id' => 'confirmation_dropdown',
                                                'class' => 'form-control',
                                                'style' => 'width: 100%'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name'),['prompt'=>'Select a Confirmation Type']) ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'booking_status_id', [
                                            'inputOptions' => [
                                                'id' => 'status_dropdown',
                                                'class' => 'form-control',
                                                'style' => 'width: 100%',
                                                ]
                                            ])->dropdownList(ArrayHelper::map(BookingStatuses::find()->all(),'id','label'),['prompt'=>'Select a Status']) ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'booking_type_id', [
                                            'inputOptions' => [
                                                'id' => 'type_dropdown',
                                                'class' => 'form-control',
                                                'style' => 'width: 100%',
                                                ]
                                            ])->dropdownList(ArrayHelper::map(BookingTypes::find()->all(),'id','label'),['prompt'=>'Select a Flag']) ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label" >Default Free Booking Cancellation Period</label>
                                        <div class="form-group" >
                                            <select style="width: 100%;" class="form-control" id="booking_cancellation" name="BookableItems[general_booking_cancellation]" >
                                                <?php
                                                    foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                                                    {
                                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="control-label">Housekeeping Status</label>
                                        <div class="form-group" >
                                            <select style="width: 100%;" class="form-control housekeeping_status" multiple="true" name="BookableItems[bookable_item_housekeeping][]">
                                                <!-- <option value="">Select a Status</option> -->
                                                <?php
                                                    $housekeepingStatuses = ArrayHelper::map(HousekeepingStatus::find()->all(),'id','label');
                                                    // echo "<pre>";
                                                    // print_r($housekeepingStatuses);
                                                    // exit();
                                                    if(!empty($housekeepingStatuses))
                                                    {
                                                            foreach ($housekeepingStatuses as $key => $value) 
                                                            {
                                                                echo  '<option class="remove-selected" value="'.$key.'" >'.$value.'</option>';
                                                            }
                                                            

                                                    }
                    
                                                        
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                        
                                </div>

                                <?php
                                    $this->registerJs("                             
                                        option = '$model->no_of_guests';
                                        if(option=='')
                                        {
                                            $('#yes').attr('checked','checked');
                                        }
                                        else
                                        {
                                            $('#'+option).attr('checked','checked');
                                        }
                                    ");
                                ?>

                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class=" control-label">Get Number of Guests</label>
                                        <div class="mt-radio-inline">

                                            <label class="mt-radio mt-radio-outline">
                                                <input type="radio" id="yes" name="BookableItems[no_of_guests]" value="yes" > Yes
                                                <span></span>
                                            </label>
                                            <label class="mt-radio mt-radio-outline">
                                                <input type="radio" id="no" name="BookableItems[no_of_guests]" value="no" > No
                                                <span></span>
                                            </label>

                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <?php 
                                    if(empty($model->background_color))
                                    {
                                        $model->background_color='#ff0000';
                                    }
                                    if(empty($model->text_color))
                                    {
                                        $model->text_color='#000000';
                                    }

                                    $this->registerJs("
                                            $('#preview_box')[0].style.backgroundColor = '$model->background_color';
                                            $('#preview_box')[0].style.color = '$model->text_color';
                                            if('$model->transparent_background' == 1)
                                            {
                                                $('.background-color').hide();
                                            }
                                            ");
                                ?>

                                <div class="row">
                                    <div class="col-xs-1">
                                        <div id="preview_box" class="preview_box"><strong>Example</strong></div>
                                    </div>
                                    <div class="col-xs-1">
                                        <label class=" control-label">Text Color</label>
                                        <div class="input-group color colorpicker-component">
                                            <input type="text" id="text_color" name="BookableItems[text_color]" class="form-control"  value=<?=$model->text_color?>>
                                        </div>
                                        
                                    </div>
                                    <div class="col-xs-1 background-color">
                                        <label class="control-label">Background Color</label>
                                        <div class="input-group colorpicker-component">
                                            <input type="text" id="background_color" name="BookableItems[background_color]" class="form-control"  value=<?=$model->background_color?>>
                                        </div>
                                    </div>
                                    <div class="col-xs-2" style="margin-top: 32px;">
                                        <?= $form->field($model, 'transparent_background')->checkbox() ?>
                                    </div>
                                    
                                </div><br>

                                <?php
                                        $this->registerJs("
                                                
                                                option = '$model->include_in_groups';
                                                if(option=='')
                                                {
                                                    $('#yes_in_report').attr('checked','checked');
                                                }
                                                else
                                                {
                                                    $('#'+option+'_in_report').attr('checked','checked');
                                                }
                                            ");
                                ?>

                                <label class=" control-label">Include in Reports</label>
                                <div class="mt-radio-inline">
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="yes_in_report" name="BookableItems[include_in_groups]" value="yes" > Yes
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" id="no_in_report" name="BookableItems[include_in_groups]" value="no" > No
                                        <span></span>
                                    </label>
                                </div>

                                <?php
                                    if(!empty($model->featured_image))
                                    {
                                        echo '<img src='.Yii::getAlias('@web').'/../uploads/bookable-items/'.$model->id.'/images/'.$model->featured_image.' height="120" width="150">';
                                        echo '<br>'; 
                                    }
                                ?>

                                <?= $form->field($model, 'image')->fileInput() ?>

                                <div class="form-group">
                                    <?php
                                        if(!$model->isNewRecord)
                                        { ?>
                                            <button type="button" class="btn btn-primary" id="save-btn">Update</button>
                                    <?php     }
                                    ?>
                                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                    <a class="btn btn-default" href="<?= Url::to(['/bookable-items']) ?>" >Cancel</a>
                                </div>

                                <?php ActiveForm::end(); ?>

                            </div>

                            <?=  
                                $this->render('details',[
                                    'model' => $model,
                                    'form' => $form,
                                    'bed_type' => $bed_type,
                                    'quantity' => $quantity,
                                    'dataProviderBedType' => $dataProviderBedType,
                                ]); 
                            ?>

                            <?=  
                                $this->render('item_names',[
                                    'model' => $model,
                                    'form' => $form,
                                ]); 
                            ?>

                            <?= $this->render('amenities',
                                    [ 
                                        'model' => $model,
                                        'amenities' => $amenities,
                                    ])?>
                            <?= $this->render('late_check_in',
                                    [ 
                                        'model' => $model,
                                        'form' => $form,
                                    ])?>

                            <div class="tab-pane" id="photos">
                                <form action="<?= Url::to(['/bookable-items/upload-images']) ?>" class="dropzone dropzone-file-area" method="POST" enctype="multipart/form-data" id="my-dropzone" style="width: 900px; margin-top: 50px;">
                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                                    <h3 class="sbold">Drop files here or click to upload</h3>
                                </form>
                                <br>
                                <div class="form-group">
                                    <?php
                                        if(!$model->isNewRecord)
                                        { ?>
                                            <button type="button" class="btn btn-primary common-save-btn" >Update</button>
                                    <?php     }
                                    ?>
                                    <a class="<?= $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?>" id="submit_button" ><?=$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save') ?></a>
                                    <a class="btn btn-default" href="<?= Url::to(['/bookable-items']) ?>" >Cancel</a>
                                </div>
                                <br>

                                <?php 

                                    if(isset($dataProvider) && !empty($dataProvider))
                                    {?>
                                        <?php Pjax::begin(['id' => 'images-gridview','timeout' => 3000000, 'enablePushState' => false]) ?>

                                            <?= GridView::widget([
                                            'dataProvider' => $dataProvider,
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],

                                                [
                                                    'label' => 'Images',
                                                    'format' =>'raw',
                                                    'value' => function($data)
                                                    {
                                                        if(!empty($data))
                                                        {
                                                            $path = Yii::getAlias('@web').'/../uploads/bookable-items/'.$data->bookable_items_id.'/images/'.$data->image;
                                                            
                                                            //return Html::img( $path, $options = [
                                                                //'style' => 'max-height:100px'] );
                                                            return '<a name="pop[]"> <img src="'.$path.'" style="max-height:100px" > </a>';
                                                        }
                                                    },
                                                ],

                                                [
                                                    'label' => 'Description',
                                                    'value' => function($data)
                                                    {
                                                        return $data->description;
                                                    },
                                                    'contentOptions' => ['style' => 'max-width:200px; overflow: hidden; text-overflow: ellipsis;']
                                                ],

                                                [
                                                    'label' => 'Order',
                                                    'format' => 'raw',
                                                    'value' => function($data)
                                                    {
                                                        if($data->isFirstRecord())
                                                        {
                                                            return '<div style="display:inline-block">
                                                                    <span class="badge badge-primary">'.$data->display_order.'</span>
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <a name="down[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>';
                                                        }
                                                        else if($data->isLastRecord())
                                                        {
                                                           return '<div style="display:inline-block">
                                                                    <span class="badge badge-primary">'.$data->display_order.'</span>
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <a name="up[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>';
                                                        }
                                                        else
                                                        {
                                                            return '<div style="display:inline-block">
                                                                    <span class="badge badge-primary">'.$data->display_order.'</span>
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <a name="up[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a name="down[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>';
                                                        }
                                                    },
                                                    'contentOptions' => ['style' => 'max-width: 30px;']
                                                ],


                                                  ['class' => 'yii\grid\ActionColumn',
                                                  'template' => '{update}{delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $model, $key) {
                                                               return '<a style="text-decoration: none;" aria-label="update" data-id='.$model->id.' title="update" name="update[]">
                                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                                        </a>';
                                                          },
                                                          'delete' => function ($url, $model, $key) {
                                                               return '<a style="text-decoration: none;" aria-label="delete" data-id='.$model->id.' title="delete" name="delete[]">
                                                                        <span class="glyphicon glyphicon-trash"></span>
                                                                        </a>';
                                                          },

                                                        ]
                                                    ],
                                                ],
                                            ]); ?>

                                        <?php Pjax::end() ?>


                                <?php 
                                }
                                ?>
                            </div>
                            

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="fa fa-exclamation-triangle"></i> Warning!</h4>
            </div>
            <div class="modal-body">
                <h3> Are you sure to delete image? </h3>
            </div>
            <div class="modal-footer">
                <button type="button" id="yes1" class="btn blue invite_user_send">Yes</button>
                <button type="button" id="no1" class="btn default" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="update-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4> Update Image Description</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="" enctype="multipart/form-data">
                    <textarea class="form-control" rows="6" style="resize: none;" id="image-description"></textarea>
                    <input type="file" id="image" name="file">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="update_grid" class="btn blue invite_user_send">Update</button>
                <button type="button" id="no1" class="btn default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="max-width: 570px; max-height: 600px;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php

// *************************** Set logic of all the ion sliders ***************************** //

// ***************** Maximum Possible Number of Extra Beds AND/OR Baby Beds/Cribs ******************** //

if($model->max_extra_beds > 0 || $model->max_extra_beds == '0')
{
    $extra_from = $model->max_extra_beds;
}
else
{
    $extra_from = 0;
}

$baby_from = !empty($model->max_baby_beds)? $model->max_baby_beds : 0;
$max_extra_range = !empty($model->max_extra_baby_beds)? $model->max_extra_baby_beds : 0;

// ***************** Minimum Number of Guests & Maximum Number of Guests ******************** //

if($model->max_adults == '0')
{
    $adult_from = 0;
}
else if($model->max_adults > 0 )
{
    $adult_from = $model->max_adults;
}
else
{
    $adult_from = 1;
}

$child_from = !empty($model->max_children)? $model->max_children : 0;

if(empty($model->max_guests))
{
    $from_max_guests = '0';
    $to_max_guests = '1';
    $max_range = '1';
}
else
{
    $arr = explode(';', $model->max_guests);
    $from_max_guests = $arr[0];
    $to_max_guests = $arr[1];
    $max_range = $arr[1];
}

if(empty($model->age_range_children))
{
    $from_age_range_children = '1';
    $to_age_range_children = '2';
}
else
{
    $arr = explode(';', $model->age_range_children);
    $from_age_range_children = $arr[0];
    $to_age_range_children = $arr[1];
}

if(empty($model->min_age_adults))
{
    $from_age_range_adults = '3' ;
    $to_age_range_adults = '4';
}
else
{
    $arr = explode(';', $model->min_age_adults);
    $from_age_range_adults = $arr[0];
    $to_age_range_adults = $arr[1];
}

$image_url = Url::to(['/bookable-items/update-image']);
$bed_types_html_url = Url::to(['/bookable-items/get-bed-types-html']);
$this->registerJs("
jQuery(document).ready(function() {


    $('.housekeeping_status').select2({
        multiple: true,
        placeholder: 'Select a Status',
        //width:'100%',
        //data: z 
    });
    $(document).on('select2:select','.housekeeping_status', function (evt) 
    {
        //alert('here');
        console.log($(this).val());
        console.log(typeof($(this).val()));
        console.log('lengt: '+$(this).val().length);

        var match=0;
        for (var i = 0; i < $(this).val().length; i++) {
           if ($(this).val()[i] == 1) 
                match = 1;
        }
        console.log('match :'+ match);
        if(match)
        {
            $('.housekeeping_status > option').removeAttr('selected');
            $('.housekeeping_status > option').trigger('change');
            $('.housekeeping_status').select2().val(1).trigger('change');
        }
       
    });

    $('#notes').summernote({height:300,toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize',['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]});
    $('#default_notes').summernote({height:300,toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize',['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]});

    $('#save-btn').on('click',function(){
        //alert('clicked');
        var input = $('<input>')
               .attr('type', 'hidden')
               .attr('name', 'save').val('1');
            $('#bookable_form').append($(input));
            $('#bookable_form').submit();

    });
    $('.common-save-btn').on('click',function(){
        //alert('clicked');
        var input = $('<input>')
               .attr('type', 'hidden')
               .attr('name', 'save').val('1');
            $('#bookable_form').append($(input));
            $('#bookable_form').submit();

    });

    var selected_max_free_age = '';
    var selected_age_range_children = '';

    $('#max_free_age').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 100,
        from_max:96,
        onFinish: function (data) 
        {
            selected_max_free_age = data.from;

            // Save slider instance to var
            var slider = $('#age_range_children').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                from: selected_max_free_age + 1,
                to: selected_max_free_age + 2,
                from_min: selected_max_free_age + 1,
            });

            // Save slider instance to var
            var slider = $('#min_age_adults').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                from: selected_max_free_age + 3,
                from_min: selected_max_free_age + 3,
                to: selected_max_free_age + 4,
            });
        },
    });

    $('#age_range_children').ionRangeSlider({
        type:'double',
        grid:false,
        min: 0,
        max: 100,
        to_max:98,
        from: $from_age_range_children,
        from_min: $from_age_range_children,
        to:$to_age_range_children,

        onFinish: function (data) 
        {
            selected_age_range_children = data.to;

            // Save slider instance to var
            var slider = $('#min_age_adults').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                from: selected_age_range_children + 1,
                from_min: selected_age_range_children + 1,
                to: selected_age_range_children + 2,
            });
        },
    });

    $('#min_age_adults').ionRangeSlider({
        type:'double',
        grid:false,
        min: 0,
        max: 100,
        from: $from_age_range_adults,
        from_min: $from_age_range_adults,
        to:$to_age_range_adults,
    });

    var image_url = '$image_url';

    $('#image').AjaxFileUpload(
    {
        action: image_url,
    });

    $('a[name=\"pop[]\"]').on('click', function() 
    {
       $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
       $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
    });

    $('a[name=\"up[]\"]').click(function()
    {
        row_id = $(this).attr('data-id');

        var Object = {
                        row_id : row_id,
                        action : 'up',
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'arrange-data',
            data: Object,
            success: function(result)
            {
                 $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"down[]\"]').click(function()
    {
        row_id = $(this).attr('data-id');

        var Object = {
                        row_id : row_id,
                        action : 'down',
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'arrange-data',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    var image_id;

    $('#images-gridview').on('pjax:end', function() 
    {
        $('a[name=\"pop[]\"]').on('click', function() 
        {
           $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
           $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        });
    
        $('a[name=\"up[]\"]').click(function()
        {
            row_id = $(this).attr('data-id');

            var Object = {
                            row_id : row_id,
                            action : 'up',
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'arrange-data',
                data: Object,
                success: function(result)
                {
                     $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        $('a[name=\"down[]\"]').click(function()
        {
            row_id = $(this).attr('data-id');

            var Object = {
                            row_id : row_id,
                            action : 'down',
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'arrange-data',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        $('a[name=\"update[]\"]').click(function(e) 
        {
            $('#update-modal').modal('show');
            $('#image').val('');

            image_id = $(this).attr('data-id');

            var Object = {
                            image_id : image_id,
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'get-description',
                data: Object,
                success: function(result)
                {
                    $('#image-description').val(result); 
                },
            });
        });

        $('a[name=\"delete[]\"]').click(function(e) 
        {
            image_id = $(this).attr('data-id');

            $('#delete-modal').modal('show');
        });
    });

    $('#submit_button').click(function()
    {
        $('#bookable_form').submit();
    });

    $('.common-submit-button').click(function()
    {
        $('#bookable_form').submit();
    });

    $('a[name=\"update[]\"]').click(function(e) 
    {
        $('#update-modal').modal('show');
        $('#image').val('');

        image_id = $(this).attr('data-id');

        var Object = {
                        image_id : image_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'get-description',
            data: Object,
            success: function(result)
            {
                $('#image-description').val(result); 
            },
        });
    });

    $('#update_grid').click(function() 
    {
        $('#update-modal').modal('hide');

        var Object = {
                        image_id : image_id,
                        description: $('#image-description').val(),
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'set-description',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"delete[]\"]').click(function(e) 
    {
        image_id = $(this).attr('data-id');

        $('#delete-modal').modal('show');
    });

    $('#yes1').click(function()
    {
        $('#delete-modal').modal('hide');

        var Object = {
                        image_id : image_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'delete-image',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });


    Dropzone.options.myAwesomeDropzone = {
      paramName: 'file', // The name that will be used to transfer the file
      maxFilesize: 10, // MB
    };

    $('#background_color').colorpicker().on('changeColor', function (e) {
                  $('#preview_box')[0].style.backgroundColor = e.color.toHex();
              });

    $('#text_color').colorpicker().on('changeColor', function (e) {
                  $('#preview_box')[0].style.color = e.color.toHex();
              });

    $('#min_price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
    $('#max_price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('#min_price').on('change',function()
    {
        $(this).parent('div').siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

    $('#max_price').on('change',function()
    {
        $(this).parent('div').siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

    $('#item_quantity').ionRangeSlider({
        grid:!0,
        min: 1,
        max: 99,
        onFinish: function (data) 
        {
            var quantity = data.from;
            var rows_count = $('.item-name-row').length;
            var total_rows = 0;

            if(quantity>rows_count)
            {
                console.log('quantity is greater');
                $('.item-name-row').show();
                total_rows = quantity - rows_count;

                for (var i=1; i <=total_rows ; i++)
                {
                    var row = '<tr class=\"item-name-row\">';

                    row += '<td><i class=\"fa fa-arrows\" aria-hidden=\"true\" style=\"margin: 10px;\"></i><input type=\"text\" name=\"BookableItems[item_names]['+(i+rows_count-1)+'][item_number]\" value=\"Item '+(i+rows_count)+'\" hidden>';

                    row += '<input type=\"text\" name=\"BookableItems[item_names]['+(i+rows_count-1)+'][item_order]\" class=\"order\" value=\"'+(i+rows_count)+'\" hidden></td>';

                    row += '<td><input type=\"text\" class=\"form-control\" name=\"BookableItems[item_names]['+(i+rows_count-1)+'][item_name]\" value=\"Item '+(i+rows_count)+'\"></td></tr>';

                    $('#item_names_body').append(row);
                }
            }
            else if(quantity<rows_count)
            {
                console.log('quantity is less');
                $('.item-name-row').hide();
                $('.item-name-row').each(function(index)
                {
                    if(index+1 <= quantity)
                        $(this).show();
                });
            }
            else
            {
                console.log('quantity is equal');
                $('.item-name-row').show();
            }
        },
    });

    $('#quantity').ionRangeSlider({
        min: 1,
        max: 10,
    });
    
    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#item_type_dropdown').select2({
        placeholder: \"Select an Item Type\",
        allowClear: true
    });

    $('#sleeping_arrangement_dropdown').select2({
        placeholder: \"Select a Sleeping Arrangement\",
        allowClear: true
    });

    $('#bed_type_dropdown').select2({
        placeholder: \"Select a Bed Type\",
        allowClear: true
    });

    $('#booking_cancellation').select2({
        placeholder: \"Select a Guest Policy\",
        allowClear: true,
    });

    $('#confirmation_dropdown').select2({
        placeholder: \"Select a Confirmation\",
        allowClear: true,
    });

    $('#type_dropdown').select2({
        placeholder: \"Select a Flag\",
        allowClear: true,
    });

    $('#status_dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true,
    });

    $('#bathroom_dropdown').select2({
        placeholder: \"Select an Option\",
        //allowClear: true,
    });

    $('.bookable_tabs li').click(function()
    {   
        console.log('clicked');

        var href = $(this).find('a:first').attr('href');
        console.log(href);
        document.getElementById('tab_href').value = href;
        // $('#tab_href').val(href);

        if(href == '#amenities')
        {
            setTimeout(function(){ 
                if (typeof redrawAmenitiesDataTable == 'function') {
                    redrawAmenitiesDataTable(); 
                }
            }, 500);
        } 
    });

    var selected_max_guests = $max_range;
    var selected_max_extra_baby_beds = $max_extra_range;

    $('#max_guests').ionRangeSlider({
        type:'double',
        grid:!0,
        min: 0,
        max: 100,
        to_min:1,
        from:$from_max_guests,
        to:$to_max_guests,
        onFinish: function (data) 
        {
            selected_max_guests = data.to;

            // Save slider instance to var
            var slider = $('#max_children').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                max: selected_max_guests,
            });

            // Save slider instance to var
            var slider = $('#max_adults').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                max: selected_max_guests,
            });
        },
    });

    $('#max_adults').ionRangeSlider({
        grid:false,
        min: 0,
        max: $max_range,
        from: $adult_from,
    });

    $('#max_children').ionRangeSlider({
        grid:false,
        min: 0,
        max: $max_range,
        from: $child_from,
    });

    // ******************* Add Functionality of Max Extra Baby Beds

    $('#max_extra_baby_beds').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 99,
        onFinish: function (data) 
        {
            selected_max_extra_baby_beds = data.from;

            // Save slider instance to var
            var slider = $('#max_extra_beds').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                max: data['from'],
                //from: 0,
                //from:data['from'],
            });

            // Save slider instance to var
            var slider = $('#max_baby_beds').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                max: data['from'],
                //from:0
            });
        },
    });

    $('#max_extra_beds').ionRangeSlider({
        grid:false,
        min: 0,
        max: $max_extra_range,
        from: $extra_from,

        onFinish: function (data) 
        {
            /*temp = selected_max_extra_baby_beds - data.from;
            
            // Save slider instance to var
            var slider = $('#max_baby_beds').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                from: temp,
            });*/
        },
    });

    $('#max_baby_beds').ionRangeSlider({
        grid:false,
        min: 0,
        max: $max_extra_range,
        from: $baby_from,

        onFinish: function (data) 
        {
            /*temp = selected_max_extra_baby_beds - data.from;
            
            // Save slider instance to var
            var slider = $('#max_extra_beds').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                from: temp,
            });*/
        },
    });

    $('#bookableitems-transparent_background').click(function()
    {
        if(this.checked)
        {
            $('.background-color').hide();
            $('#preview_box')[0].style.backgroundColor = 'transparent';
        }
        else
        {
            $('.background-color').show();
            $('#preview_box')[0].style.backgroundColor = '#ff0000';
        }
    });

    // **************** Bed Types Script ***************//

    $('.add-dynamic-field').on('click',function()
    {
        $.ajax(
        {
            url: '$bed_types_html_url',
            success: function(data)
            {
                data = JSON.parse(data);
                $('#bed_types_dynamic_div').append(data.bed_types_html);

                initSelect2();
                initIonRangeSlider();

            },
            error: function()
            {
                alert('error');
            }
        });
    });

    $('.row').on('click','.delete-dynamic-field',function(event)
    {
        $(this).closest('.row').remove();
    });

    function initSelect2()
    {
        $('.dynamic-bed-types').select2({
            placeholder: \"Select a Bed Type\",
            allowClear: true
        });
    }

    function initIonRangeSlider()
    {
        $('.dynamic-quantity').ionRangeSlider({
            min: 1,
            max: 10,
        });
    }

    initSelect2();
    initIonRangeSlider();

    var bed_type_row_id='';

    $('a[name=\"bed_type_delete[]\"]').click(function()
    {
        bed_type_row_id = $(this).attr('data-id');
        $('#delete-bed-type-modal').modal('show');
    });

    $('#yes_bed_type').click(function()
    {
        $('#delete-bed-type-modal').modal('hide');

        var Object = {
                        id : bed_type_row_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'delete-bed-type',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#bed_type-gridview'});  //Reload GridView
            },
        });
    });

    $('#bed_type-gridview').on('pjax:end', function() 
    {
        $('a[name=\"bed_type_delete[]\"]').click(function()
        {
            bed_type_row_id = $(this).attr('data-id');
            $('#delete-bed-type-modal').modal('show');
        });
    });

});",View::POS_END);



?>


<?php

use yii\db\Migration;

class m161209_060947_alter_open_hours_days_add_url extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours_days` ADD COLUMN `url` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `sun_switch`; 
        ");
    }

    public function down()
    {
        echo "m161209_060947_alter_open_hours_days_add_url cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

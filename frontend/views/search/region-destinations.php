<?php

use yii\widgets\LinkPager;
use common\models\BookableItemsImages;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationTypes;

$this->title = 'Region Destinations';
$path = '//placehold.it/1200x260&amp;text=IMAGE+PLACEHOLDER';
if(!empty($state_record->image))
{
  $path = Yii::getAlias('@web').'/uploads/states/'.$state_record->state_id.'/images/'.$state_record->image;
}
?>

<div class="site-showcase">
    <!-- Start Page Header -->
    <div class="parallax page-header" style="background-image:url(<?=$path?>); height: 450px;">
    </div>
    <!-- End Page Header -->
</div>

  <!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">

              	<!-- ************ Show region  ************ -->

                <div class="col-sm-10 col-sm-offset-1">

                    <div class="single-agent">
                          <div class="counts pull-right"><strong><?=$total_destinations?></strong><span>Destinations</span></div>
                          <h2 class="page-title"><?=$state_record->state->name?></h2>
                          <div class="row">
                              <div class="col-sm-12">
                                      <p><?=$state_record->description?></p>
                              </div>
                          </div>
                    </div>

                    <div class="spacer-20"></div>

                    <!-- Start Related Properties -->
                    <div class="block-heading">
                        <h4><span class="heading-icon"><i class="fa fa-th-list"></i></span>Latest destinations listed in <?=$state_record->state->name?></h4>
                        <div class="toggle-view pull-right">
                         <?php
                          // check call for list view or grid view

                          if(isset($gridview) && $gridview==1)
                          {
                            echo '<a class="active" id="grid_view"><i class="fa fa-th-large"></i></a>
                                    <a id="list_view"><i class="fa fa-th-list"></i></a>';
                          }
                          else
                          {
                            echo '<a id="grid_view"><i class="fa fa-th-large"></i></a>
                                    <a id="list_view" class="active"><i class="fa fa-th-list"></i></a>';
                          }
                         ?>
                        </div>
                    </div>

                    <?php 
                      if(empty($models))
                      { 
                        echo'<h4>No Record Found..... </h4>';
                      }
                    ?>

                    <!-- ************ Grid view  ************ -->

                    <?php if(isset($gridview) && $gridview==1): ?>

                      <?=
                        $this->render('gridview',[
                             'models' => $models, // model of provider
                             'banners' => $banners
                          ]);
                      ?>

                    <?php else: ?>
                            
                        <!-- ************ List view  ************ -->    
                      <?=
                        $this->render('listview',[
                            'models' => $models,
                            'banners' => $banners
                        ]);
                      ?>

                    <?php endif ?>

                      <?= // display pagination
                        LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                      ?>

                  </div>

            </div>
        </div>
    </div>
</div>

<?php

$url =  Url::to(['/'],true);
$updateSessionUrl =  Url::to(['/search/update-session'],true);
$region_name = str_replace(' ', '-', $region_name);

$this->registerJs("
jQuery(document).ready(function() 
{
  $('#list_view').click(function()
  {
    var Object = {
                    view: 0,
                }
    $.ajax(
    {
        type: 'POST',
        url: '$updateSessionUrl',
        data: Object,
        success: function(result)
        {
          location.replace('$url'+'$region_name');
        },
    });
  });

  $('#grid_view').click(function()
  {
    var Object = {
                    view: 1,
                }
    $.ajax(
    {
        type: 'POST',
        url: '$updateSessionUrl',
        data: Object,
        success: function(result)
        {
          location.replace('$url'+'$region_name');
        },
    });

  });

});",View::POS_END);
?>
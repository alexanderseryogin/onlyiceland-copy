<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_meal_plan".
 *
 * @property string $id
 * @property integer $provider_id
 * @property integer $pfo_breakfast
 * @property integer $pfo_lunch
 * @property integer $pfo_dinner
 * @property integer $sfo_morning_snack
 * @property integer $sfo_afternoon_snack
 * @property integer $sfo_evening_snack
 * @property integer $abo_tea
 * @property integer $abo_fountain
 * @property integer $abo_bear
 * @property integer $abo_liquor
 * @property integer $abo_minibar
 *
 * @property Providers $provider
 */
class DestinationsMealPlan extends \yii\db\ActiveRecord
{
    public static $meal_options_arr =
    [
        0 =>'Included',
        1 =>'Available',
        2 =>'Prior Request Only',
        3 =>'Please Inquire',
        4 =>'Do Not Display'
    ]; 

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_meal_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'pfo_breakfast', 'pfo_lunch', 'pfo_dinner', 'sfo_morning_snack', 'sfo_afternoon_snack', 'sfo_evening_snack', 'abo_tea', 'abo_fountain', 'abo_beer', 'abo_liquor', 'abo_minibar'], 'integer'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
            'pfo_breakfast' => Yii::t('app', 'Pfo Breakfast'),
            'pfo_lunch' => Yii::t('app', 'Pfo Lunch'),
            'pfo_dinner' => Yii::t('app', 'Pfo Dinner'),
            'sfo_morning_snack' => Yii::t('app', 'Sfo Morning Snack'),
            'sfo_afternoon_snack' => Yii::t('app', 'Sfo Afternoon Snack'),
            'sfo_evening_snack' => Yii::t('app', 'Sfo Evening Snack'),
            'abo_tea' => Yii::t('app', 'Abo Tea'),
            'abo_fountain' => Yii::t('app', 'Abo Fountain'),
            'abo_beer' => Yii::t('app', 'Abo Bear'),
            'abo_liquor' => Yii::t('app', 'Abo Liquor'),
            'abo_minibar' => Yii::t('app', 'Abo Minibar'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }
}

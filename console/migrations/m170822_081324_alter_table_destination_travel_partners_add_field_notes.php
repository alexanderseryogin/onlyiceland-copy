<?php

use yii\db\Migration;

class m170822_081324_alter_table_destination_travel_partners_add_field_notes extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `destination_travel_partners`   
                          ADD COLUMN `notes` LONGTEXT NULL AFTER `billing_contact`;
                        ');
    }

    public function down()
    {
        echo "m170822_081324_alter_table_destination_travel_partners_add_field_notes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
//use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
//echo Yii::$app->request->baseUrl; exit;
?>
<style>
    .user-login-5 .login-container > .login-content p {
        color: #ED6B75;
        font-size: 15px;
        line-height: 22px;
    }
    .login-info {
        color: #a0a9b4;
        font-size: 15px;
        line-height: 22px;
    }
    .user-login-5 .login-container > .login-content > .login-form .form-control {
        margin-bottom: 5px;
    }
</style>
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <img class="login-logo login-6" style="max-height:50px;" src="<?= Yii::getAlias('@web')?>/assets_b/img/oi_logo.png" />
            <div class="login-content">
                <h1><?= Html::encode($this->title) ?></h1>
                <span class="login-info"> Please fill out the following fields to login: </span>
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form', 
                    'options' =>[
                         'class' => 'login-form'
                    ],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'tag'=>'span'
                        ]
                    ]
                    ]); ?>
                    <!-- <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span><?= $form->errorSummary($model); ?></span>
                    </div> -->
                    <div class="row">
                        <div class="col-xs-6">
                            <!-- <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="username" required/> -->
                            <?= $form->field($model, 'username')->textInput([
                                'required' => true,
                                'autofocus' => true,
                                'autocomplete' => "off",
                                'placeholder' => 'Username',
                                'class' => "form-control form-control-solid placeholder-no-fix form-group"
                            ])->label(false) ?> 
                        </div>
                        <div class="col-xs-6">
                            <!-- <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="password" required/> -->
                            <?= $form->field($model, 'password')->passwordInput([
                                'required' => true,
                                'template' => "{input}",
                                'autocomplete' => "off",
                                'placeholder' => 'Password',
                                'class' => "form-control form-control-solid placeholder-no-fix form-group"
                            ])->label(false) ?> 
                        </div>
                    </div>
                    <div class="row" style="margin-top:30px;">
                        <div class="col-sm-4">
                            <!-- <label class="rememberme mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="remember" value="1" /> Remember me
                                <span></span>
                            </label> -->
                            <?= $form->field($model, 'rememberMe')->checkbox([
                                'template' => "<label class=\"rememberme mt-checkbox mt-checkbox-outline\">\n{input} Remember me<span></span>\n{hint}\n{error}</label>"
                                ])?>
                                
                        </div>
                        <div class="col-sm-8 text-right">
                            <div class="forgot-password">
                                <a href="<?= \yii\helpers\Url::to(['/site/request-password-reset']) ?>" class="forget-password">
                                    Forgot your password?
                                </a>
                            </div>
                            <br><br>
                            <!-- <button class="btn blue" type="submit">Sign In</button> -->
                            <?= Html::submitButton('Sign In', ['class' => 'btn blue', 'name' => 'login-button']) ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>

                <!-- BEGIN FORGOT PASSWORD FORM -->
                <!-- <form class="forget-form" action="javascript:;" method="post">
                    <h3>Forgot Password ?</h3>
                    <p> Enter your e-mail address below to reset your password. </p>
                    <div class="form-group">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                    <div class="form-actions">
                        <button type="button" id="back-btn" class="btn blue btn-outline">Back</button>
                        <button type="submit" class="btn blue uppercase pull-right">Submit</button>
                    </div>
                </form> -->
                <!-- END FORGOT PASSWORD FORM -->
            </div>
            <div class="login-footer">
                <div class="row bs-reset">
                    <div class="col-xs-5 bs-reset">
                        <ul class="login-social">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-dribbble"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-7 bs-reset">
                        <div class="login-copyright text-right">
                            <p>Copyright &copy; OnlyIceland </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 bs-reset">
            <div class="login-bg"> 
                
            </div>
        </div>
    </div>
</div>
<?php 
    $this->registerJs("var Login = function() {

    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            messages: {
                username: {
                    required: \"Username is required.\"
                },
                password: {
                    required: \"Password is required.\"
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        $('.forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        $('#forget-password').click(function(){
            $('.login-form').hide();
            $('.forget-form').show();
        });

        $('#back-btn').click(function(){
            $('.login-form').show();
            $('.forget-form').hide();
        });
    }

 
  

    return {
        //main function to initiate the module
        init: function() {

            handleLogin();

            // init background slide images
            $('.login-bg').backstretch([
                \"".Yii::getAlias('@uploadsUrl')."/login/bg2.jpg\",
                \"".Yii::getAlias('@uploadsUrl')."/login/bg3.jpg\",
                \"".Yii::getAlias('@uploadsUrl')."/login/bg4.jpg\"
                ], {
                  fade: 1000,
                  duration: 8000
                }
            );

            $('.forget-form').hide();

        }

    };

}();

$(document).ready(function() {
    Login.init();
});
", \yii\web\View::POS_END);
?>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\UserProfile;
use yii\helpers\ArrayHelper;
Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
use frontend\components\Helpers;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Profile');
$this->params['breadcrumbs'][] = $this->title;

\metronic\assets\ProviderAdminUserProfileAsset::register($this);
?>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet bordered">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?php if(empty(Yii::$app->user->identity->profile->profile_picture)){echo Yii::getAlias('@web').'/frontend/assets/img/dummy_profile.jpg';}else{echo Yii::getAlias('@web').'/uploads/user-profile/'.Yii::$app->user->identity->profile->profile_picture;} ?>" class="img-responsive" alt=""/> 
                </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            <?php
                            if(isset($model->title))
                                echo Helpers::ProfileTitleTypes($model->title)." ";

                            if(isset($model->first_name))
                                echo $model->first_name." ";

                            if(isset($model->last_name))
                                echo $model->last_name." ";
                            ?>                            
                        </div>
                        <div class="profile-usertitle-job">
                            <?php 
                            if(isset($model->company_name))
                                echo $model->company_name;
                            ?>
                        </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <!--
                <div class="profile-userbuttons">
                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                </div>-->
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <!--
                <div class="profile-usermenu">
                        <ul class="nav">
                            <li>
                                <a href="page_user_profile_1.html">
                                    <i class="icon-home"></i> Overview </a>
                            </li>
                            <li class="active">
                                    <a href="page_user_profile_1_account.html">
                                        <i class="icon-settings"></i> Account Settings </a>
                            </li>
                            <li>
                                    <a href="page_user_profile_1_help.html">
                                        <i class="icon-info"></i> Help </a>
                            </li>
                        </ul>
                </div>
                -->
            <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light bordered">
                <!-- STAT -->
                <!--
                                <div class="row list-separated profile-stat">
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> 37 </div>
                                        <div class="uppercase profile-stat-text"> Projects </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> 51 </div>
                                        <div class="uppercase profile-stat-text"> Tasks </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="uppercase profile-stat-title"> 61 </div>
                                        <div class="uppercase profile-stat-text"> Uploads </div>
                                    </div>
                                </div>
                            -->
                            <!-- END STAT -->
                            <div>
                                <h4 class="profile-desc-title">Address</h4>
                                <span class="profile-desc-text"> 
                                    <?php
                                    if(isset($model->address))
                                        echo $model->address.", ";

                                    if(isset($model->postal_code))
                                        echo $model->postal_code.", ";

                                    if(isset($model->city_id))
                                        echo $model->city->name.", ";

                                    if(isset($model->state_id))
                                        echo $model->state->name.", ";

                                    if(isset($model->country_id))
                                        echo $model->country->name;

                                            // echo isset($model->address) ? $model->address.", " : "";
                                            // echo isset($model->postal_code) ? $model->postal_code.", " : "";
                                            // echo isset($model->city_id) ? $model->city->name.", " : "";
                                            // echo isset($model->state_id) ? $model->state->name.", " : "";
                                            // echo isset($model->country_id) ? $model->country->name : "";
                                    ?> 
                                </span>

                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-phone"></i>
                                    <?php 
                                    if(isset($model->telephone_number))
                                        echo $model->telephone_number;
                                    ?>
                                </div>
                                    <!--
                                    <div class="margin-top-20 profile-desc-link">
                                        <i class="fa fa-globe"></i>
                                        <a href="http://www.keenthemes.com">www.keenthemes.com</a>
                                    </div>
                                    <div class="margin-top-20 profile-desc-link">
                                        <i class="fa fa-twitter"></i>
                                        <a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
                                    </div>
                                    <div class="margin-top-20 profile-desc-link">
                                        <i class="fa fa-facebook"></i>
                                        <a href="http://www.facebook.com/keenthemes/">keenthemes</a>
                                    </div>
                                -->
                            </div>
                        </div>
                        <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Profile Update</span>
                            </div>
                            <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab">Change Picture</a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                    </li>
                                    <!--       
                                    <li>
                                        <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                    </li>
                                -->
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <?php $form = ActiveForm::begin(); ?>

                                    <?= $form->field($model, 'title')->dropDownList([
                                        UserProfile::TITLE_MR => Helpers::ProfileTitleTypes(UserProfile::TITLE_MR),
                                        UserProfile::TITLE_MS => Helpers::ProfileTitleTypes(UserProfile::TITLE_MS),
                                        UserProfile::TITLE_MRS => Helpers::ProfileTitleTypes(UserProfile::TITLE_MRS),
                                        UserProfile::TITLE_DR => Helpers::ProfileTitleTypes(UserProfile::TITLE_DR),
                                    ]); ?>

                                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

                                    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

                                    <?php /*echo $form->field($model, 'city_id')->textInput() ?>

                                    <?php echo $form->field($model, 'state_id')->textInput() ?>

                                    <?php echo $form->field($model, 'country_id')->textInput()*/ ?>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <?= $form->field($model, 'country_id', [
                                            'inputOptions' => [
                                                'id' => 'countries-dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                                                'prompt'=>'Select a Country',
                                                'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('provider-admin/properties/liststates?country_id=').'"+$(this).val(), function( data ) {
                                                      $("#states-dropdown").html( data );
                                                    });'
                                            ]) ?>      
                                        </div>
                                        <div class="col-xs-4">
                                            <?= $form->field($model, 'state_id', [
                                            'inputOptions' => [
                                                'id' => 'states-dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(States::find()->where(['id'=>$model->state_id])->all(),'id','name'),[
                                                'prompt'=>'Select a State',
                                                'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('provider-admin/properties/listcities?state_id=').'"+$(this).val(), function( data ) {
                                                      $("#cities-dropdown").html( data );
                                                    });'
                                            ]) ?>  
                                        </div>
                                        <div class="col-xs-4">
                                            <?= $form->field($model, 'city_id', [
                                            'inputOptions' => [
                                                'id' => 'cities-dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(Cities::find()->where(['id'=>$model->city_id])->all(),'id','name'),[
                                                'prompt'=>'Select a City',
                                                
                                            ]) ?> 
                                        </div>
                                    </div>


                                    <?= $form->field($model, 'telephone_number', [
                                            'inputOptions' => [
                                                'id' => 'telephone_number',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 800px'
                                                ],
                                                'template' => "{label}<br>{input}"
                                            ])->textInput()?>

                                    <div class="margiv-top-10">
                                        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn green']) ?>
                                        <a href="javascript:;" class="btn default"> Cancel </a>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane" id="tab_1_2">
                                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="height: 150px;">
                                                        <img src="<?php if(empty(Yii::$app->user->identity->profile->profile_picture)){echo Yii::getAlias('@web').'/frontend/assets/img/dummy_profile.jpg';}else{echo Yii::getAlias('@web').'/uploads/user-profile/'.Yii::$app->user->identity->profile->profile_picture;} ?>" class="img-responsive" alt=""/> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                        <div>
                                                            <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <!--
                                                            <span class="fileinput-exists"> Change </span>
                                                            -->
                                                            <input type="hidden">
                                                                <?php echo  Html::activeInput('file', $image_model,'imageFile'); ?>
                                                            </span>
                                                            <!--
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                -->
                                                            </div>
                                                        </div>
                                                        <!--
                                                        <div class="clearfix margin-top-10">
                                                            <span class="label label-danger">NOTE! </span>
                                                            <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                        </div>
                                                        -->
                                                    </div>
                                                    <div class="margin-top-10">
                                                        <button class="btn green">Submit</button>
                                                        <a href="javascript:;" class="btn default"> Cancel </a>

                                                        <?php ActiveForm::end() ?>
                                                    </div>
                                            </div>
                                            <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane" id="tab_1_3">
                                    <?php $form = ActiveForm::begin([
                                        'id' => 'reset-password-form',
                                        'fieldConfig' => [
                                            'options' => [
                                                'tag'=>'span'
                                            ]
                                        ]
                                    ]); ?>
                                    <div class="margin-top-10">
                                    <?= $form->field($password_model, 'current_password')->passwordInput([
                                                //'required' => true,
                                                'autocomplete' => "off",
                                                'placeholder' => 'Current Password',
                                                'id'=>'password-input-current',
                                                'class' => "form-control"
                                            ]) ?>
                                    </div>
                                    <div class="margin-top-10">
                                    <?= $form->field($password_model, 'new_password')->passwordInput([
                                                //'required' => true,
                                                'autocomplete' => "off",
                                                'placeholder' => 'New Password',
                                                'id'=>'password-input-new',
                                                'class' => "form-control",
                                                'style' => "margin-bottom: 10px;"
                                            ]) ?>
                                    </div>
                                    <div class="margin-top-10">
                                        <?= Html::submitButton('Change Password', ['class' => 'btn green', 'id' => 'change-password', 'disabled' => true]) ?>
                                        <a href="javascript:;" class="btn default"> Cancel </a>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                    <!--
                                    <form action="#">
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input type="password" class="form-control" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input type="password" class="form-control" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Re-type New Password</label>
                                            <input type="password" class="form-control" /> </div>
                                        <div class="margin-top-10">
                                            <a href="javascript:;" class="btn green"> Change Password </a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                    -->
                                </div>
                                <!-- END CHANGE PASSWORD TAB -->
                                <!-- PRIVACY SETTINGS TAB -->
                                                    <!--
                                                        <div class="tab-pane" id="tab_1_4">
                                                            <form action="#">
                                                                <table class="table table-light table-hover">
                                                                    <tr>
                                                                        <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios1" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios1" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios11" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios11" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios21" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios21" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios31" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios31" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn red"> Save Changes </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    -->
                                <!-- END PRIVACY SETTINGS TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {
    
    //$('#telephone_number').intlTelInput();

    $('#telephone_number').intlTelInput({
      initialCountry: 'auto',
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });



});",View::POS_END);
?>

<?php 
//Javascript for password strength checker
$this->registerJs("
var initialized = false;
var input = $('#password-input-new');

input.keydown(function () {
    if (initialized === false) {
        // set base options
        input.pwstrength({
            raisePower: 1.4,
            minChar: 8,
            verdicts: [\"Weak\", \"Normal\", \"Medium\", \"Strong\", \"Very Strong\"],
            scores: [17, 26, 40, 50, 60],
            common: {
                onKeyUp: function (evt, data) {
                    if(data.score >= 50) {
                        $('#change-password').prop('disabled', false);
                    }
                    else{
                        $('#change-password').prop('disabled', true);
                    }
                }
            }
        });

        // add your own rule to calculate the password strength
        input.pwstrength(\"addRule\", \"demoRule\", function (options, word, score) {
            return word.match(/[a-z].[0-9]/) && score;
        }, 10, true);

        // set as initialized 
        initialized = true;
    }
});
", \yii\web\View::POS_END);
?>
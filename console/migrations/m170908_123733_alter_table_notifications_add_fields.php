<?php

use yii\db\Migration;

class m170908_123733_alter_table_notifications_add_fields extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `notifications`   
                          ADD COLUMN `destination_id` INT(11) NULL AFTER `id`,
                          ADD COLUMN `status` INT(11) DEFAULT 0 NULL AFTER `message`,
                          ADD COLUMN `user_type` INT(11) NULL AFTER `status`,
                          ADD FOREIGN KEY (`destination_id`) REFERENCES `providers`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
                        ');
    }

    public function down()
    {
        echo "m170908_123733_alter_table_notifications_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

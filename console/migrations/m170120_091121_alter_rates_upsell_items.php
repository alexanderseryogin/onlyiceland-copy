<?php

use yii\db\Migration;

class m170120_091121_alter_rates_upsell_items extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `provider_upsell_items` DROP COLUMN `price`; 
            ALTER TABLE `rates_upsell` ADD COLUMN `price` INT(11) NULL AFTER `upsell_id`; 
        ");
    }

    public function down()
    {
        echo "m170120_091121_alter_rates_upsell_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

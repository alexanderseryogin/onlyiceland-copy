<?php

use yii\db\Migration;

class m180326_130646_alter_table_custom_reports extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_reports`   
                          ADD COLUMN `show_sub_totals` INT(11) DEFAULT 2  NULL AFTER `group_by`;
                        ');
    }

    public function safeDown()
    {
        echo "m180326_130646_alter_table_custom_reports cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180326_130646_alter_table_custom_reports cannot be reverted.\n";

        return false;
    }
    */
}

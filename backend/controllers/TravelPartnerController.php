<?php

namespace backend\controllers;

use Yii;
use common\models\TravelPartner;
use common\models\TravelPartnerSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TravelPartnerController implements the CRUD actions for TravelPartner model.
 */
class TravelPartnerController extends BaseController
{
    /**
     * @inheritdoc
     */
    

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TravelPartner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TravelPartnerSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params) && !isset($params['page']))
        {
            if(isset($params['TravelPartnerSearch']) && !$this->isArrayEmpty($params['TravelPartnerSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TravelPartner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TravelPartner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TravelPartner();
        $model->calculation = TravelPartner::CALC_DISCOUNT;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->phone = $model->server_phone;
            $model->emergency_number = $model->server_emergency;
            /*$model->kennitala = str_replace('_', '', $model->kennitala);
            $model->kennitala = rtrim($model->kennitala, '-');
*/
            $model->save();
            /*print_r($model->getErrors());
            exit;*/

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TravelPartner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->phone = $model->server_phone;
            $model->emergency_number = $model->server_emergency;
            /*$model->kennitala = str_replace('_', '', $model->kennitala);
            $model->kennitala = rtrim($model->kennitala, '-');*/

            $model->save();
            Yii::$app->session->setFlash('success','Record is updated Successfully.');

            if(isset($_POST['save']))
            {
                return $this->redirect(['update','id' => $model->id]);
            }
            else
            {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TravelPartner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TravelPartner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TravelPartner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TravelPartner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

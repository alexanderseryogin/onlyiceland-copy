<?php

use yii\db\Migration;

class m170320_073714_create_beds_combinations extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `beds_combinations`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `combination` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `icon` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170320_073714_create_beds_combinations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

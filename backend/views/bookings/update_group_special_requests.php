
<?php
	use common\models\DestinationsSpecialRequests;
	$Checkbox = [
	    0 => '',
	    1 => 'checked'
	];
	
	$requests = DestinationsSpecialRequests::find()
            ->where(['provider_id' => $provider_id])
            ->all();
    
	if(!empty($requests))
	{
		echo '<label><strong>Special Requests</strong></label><br>';
		foreach ($requests as $key =>  $request) 
	    {
	    	$checked = 0; 

	    	if(isset($checked_special_requests) && !empty($checked_special_requests) && in_array($request->request_id, $checked_special_requests))
	    	{
	    		$checked = 1; 
	    	}

	    	?>
 
	    	<label>
	    		<input name="BookedItems[<?php echo $model_id?>][special_requests][]" data="<?php echo $request->request_id?>" value="<?php echo $request->request_id?>" style="margin-left:10px;" <?php echo $Checkbox[$checked]?> type="checkbox"> <?php echo $request->request->label?>
	    	</label>

	    	<?php
	    }
	}
	else 
	{
	 	echo '<span class="label label-danger"> Whoops! </span>
        <span>&nbsp;  No Special Requests found </span>';
	} 
?>


<?php

use yii\db\Migration;

class m161013_111915_create_table_providers_staff extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_staff`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11) NOT NULL, `staff_id` INT(11) NOT NULL, PRIMARY KEY (`id`), CONSTRAINT `FK_providers_staff_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_providers_staff_staff_id` FOREIGN KEY (`staff_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161013_111915_create_table_providers_staff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161208_071200_create_table_destination_open_hours_days extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `providers_open_hours_days` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `provider_id` int(11) DEFAULT NULL,
              `mon_opening` time DEFAULT NULL,
              `mon_closing` time DEFAULT NULL,
              `mon_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              `tue_opening` time DEFAULT NULL,
              `tue_closing` time DEFAULT NULL,
              `tue_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              `wed_opening` time DEFAULT NULL,
              `wed_closing` time DEFAULT NULL,
              `wed_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              `thu_opening` time DEFAULT NULL,
              `thu_closing` time DEFAULT NULL,
              `thu_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              `fri_opening` time DEFAULT NULL,
              `fri_closing` time DEFAULT NULL,
              `fri_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              `sat_opening` time DEFAULT NULL,
              `sat_closing` time DEFAULT NULL,
              `sat_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              `sun_opening` time DEFAULT NULL,
              `sun_closing` time DEFAULT NULL,
              `sun_switch` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `FK_providers_open_hours_days_provider_id` (`provider_id`),
              CONSTRAINT `FK_providers_open_hours_days_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
    }

    public function down()
    {
        echo "m161208_071200_create_table_destination_open_hours_days cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

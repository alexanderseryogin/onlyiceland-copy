<?php

use yii\db\Migration;

class m161201_080251_create_table_attraction_and_tags extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `attraction_and_tags`( `id` INT(11) NOT NULL AUTO_INCREMENT, `attr_id` INT(11), `tag_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_attraction_and_tags_attr_id` FOREIGN KEY (`attr_id`) REFERENCES `attractions`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_attraction_and_tags_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `attraction_tags`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161201_080251_create_table_attraction_and_tags cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

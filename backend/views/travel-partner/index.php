<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
Use common\models\Countries;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TravelPartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Travel Partners');
$this->params['breadcrumbs'][] = $this->title;

$update_page_url = Url::to(['/travel-partner/update']);
?>
<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>
<div class="travel-partner-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW TRAVEL PARTNER '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'travel-partner-gridview','timeout' => 10000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'company_name',
            // 'commission',
            'primary_contact',
            'billing_contact',
            'booking_email:email',
            'invoice_email:email',
            'kennitala',
            'phone',
            'emergency_number',
            [
                'attribute' => 'country_id',
                'value' => function($data){
                    return $data->country->name;
                },
                'filter' => Html::dropDownList(
                    'TravelPartnerSearch[country_id]', 
                    $searchModel['country_id'], 
                    ArrayHelper::map(Countries::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Country', 
                        'id'=>'countries-dropdown', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'min-width:120px'],
            ],

            [   
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>['style' => 'min-width:70px;'],
                'headerOptions' =>['style' => 'min-width:70px;'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });
    
    $('#travel-partner-gridview').on('pjax:end', function() {

        $('#countries-dropdown').select2({
            placeholder: \"Select a Country\",
            allowClear: true
        });
        
    });

    $(document).on('click','.table-striped tbody tr',function()
    {
        window.location.replace('$update_page_url?id='+$(this).attr('data-key'));
    });
    
    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>

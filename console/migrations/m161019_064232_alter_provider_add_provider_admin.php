<?php

use yii\db\Migration;

class m161019_064232_alter_provider_add_provider_admin extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` CHANGE `provider_admin_id` `provider_admin_id` INT(11) NULL COMMENT 'owner', ADD COLUMN `provider_admin` INT(11) NULL COMMENT 'provider_admin' AFTER `provider_admin_id`, ADD CONSTRAINT `FK_provider_provider_admin` FOREIGN KEY (`provider_admin`) REFERENCES `user`(`id`) ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m161019_064232_alter_provider_add_provider_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170123_072552_alter_discount_code_update_amount extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `discount_code` CHANGE `amount` `amount` INT(11) NULL; ");
    }

    public function down()
    {
        echo "m170123_072552_alter_discount_code_update_amount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<table id="charges_table" class="table">
    <thead>
      <tr>
        <!-- <th>Id </th> -->
        <th style="width: 15%;text-align: center;">Booking Item id</th>
        <th style="width: 80%;">Item Name</th>
        <th style="width: 30%;">Balance Due</th>
         <th style="width: 4%;"></th>
      <!--  <th style="width: 8%;"></th>
        <th style="width: 6%;"></th>
        <th style="width: 8%;"></th>
        <th style="width: 6%;"> </th> -->
        <th style="width: 10%;"> </th>
        <th style="width: 10%;"> </th>
        <th style="width: 10%;">  </th>
        <th style="width: 5%;"> </th>
      </tr>
    </thead>
    <tbody id="charges-row">
    <?php 
    		//$counter = 1;
    		// foreach ($booking_dates as $booking_date) {
			if(!empty($bookingGroupModel->bookingItemGroups)){
				
				foreach ($bookingGroupModel->bookingItemGroups as $bookingGroupItem) {
    ?>
    	<tr>
    		<td style="text-align: center;">
				<?= isset($bookingGroupItem->bookingItem->id)?$bookingGroupItem->bookingItem->id:"" ?>
			<td >
				<?= isset($bookingGroupItem->bookingItem->item->itemType->name)?$bookingGroupItem->bookingItem->item->itemType->name:"" ?>
			</td>
			<td style="text-align: right;">
				<?= isset($bookingGroupItem->bookingItem->balance)?Yii::$app->formatter->asDecimal($bookingGroupItem->bookingItem->balance,0):"" ?>
			</td>
    	</tr>
	<?php
			}
		}
	?>
 	
    </tbody>
</table>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\HousekeepingStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- <div class="housekeeping-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abbreviation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'background_color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text_color')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> -->

<div class="portlet light ">
<div class="housekeeping-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?php 
        if(empty($model->background_color) && $model->isNewRecord)
        {
            $model->background_color='ff6161';
        }
        if(empty($model->text_color) && $model->isNewRecord)
        {
            $model->text_color='ff6161';
        }
    ?>
    <label class="control-label">Abbreviation</label>
    <?= $form->field($model, 'abbreviation')->textInput(['maxlength' => true])->label(false) ?>

    <label class="control-label">Background Color</label>
    <div class="form-group">
        <input type="text" id="hue-demo1" name="HousekeepingStatus[background_color]" class="form-control demo" data-control="hue"  value=<?=$model->background_color?>> 
    </div>

    <label class=" control-label">Text Color</label>
    <div class="form-group">
        <input type="text" id="hue-demo3" name="HousekeepingStatus[text_color]" class="form-control demo" data-control="hue" value=<?=$model->text_color?>>
    </div>

     <?php
            if(!empty($model->icon))
            {
                echo '<img src='.Yii::getAlias('@web').'/../uploads/housekeeping-status/'.$model->id.'/icons/'.$model->icon.' height="50" width="50">';
                echo '<br>'; 
            }
        ?>

    <?= $form->field($model, 'temp_icon')->fileInput() ?>

    <span class="label label-danger"> NOTE! </span>
    <span>&nbsp; Recommended Dimensions less or equal (50x50) </span><br><br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/housekeeping-status']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>


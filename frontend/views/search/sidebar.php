<?php
	
use common\models\BookableItems;
use common\models\Destinations;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\DestinationTypes;
use common\models\AccommodationTypes;

?>

<div class="sidebar left-sidebar col-md-3">
    <form id="filter_form" action="<?= Url::to(['/search/items'])?>" method="GET" >
        <input id="region_id" type="text" name="region_id" hidden="">
        <input id="amenities" type="text" name="amenities" hidden="">
        <input id="accommodation_id" type="text" name="accommodation_id" hidden="">
        <input id="accommodation_types" type="text" name="accommodation_types" hidden="">
        <input id="food_types" type="text" name="food_types" hidden="">

        <button id="search_up" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
        <br><br>

        <?php if($provider_type == "" || $provider_type == 11) { ?>
        <!-- Check-in / Checkout -->
        <div class="widget sidebar-widget">
            <div class="full-search-form">
                <h4 class="widgettitle"><?= \Yii::t('app', 'Check-in / Checkout') ?></h4>
                <div >
                    <div class="form-group">
                        <div class="checkin-calendar" style="display: table">
                            <input type="text" class="form-control" value="<?= $date_checkin ? $date_checkin : '';?>" name="date_checkin" id="date_from" placeholder="<?= date('m/d/Y');?>" autocomplete="off">

                            <span class="input-group-addon"> <?= \Yii::t('app', 'to') ?> </span>

                            <input type="text" class="form-control" value="<?= $date_checkout ? $date_checkout : '';?>" name="date_checkout" id="date_to" placeholder="<?= date('m/d/Y',  strtotime("+1 day"));?> " autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <!-- Regions -->
        <div class="widget sidebar-widget">
            <div class="full-search-form">
                <h4 class="widgettitle"><?= \Yii::t('app', 'FILTER BY REGIONS') ?> <span class="fa fa-plus" id="show_regions"></span></h4>
                <div id="regions_checklist">
                    <?php
                    foreach ($regions as $key => $region)
                    {
                        $query1 = Destinations::find()->where(['state_id' => $key]);
                        $result = ArrayHelper::map($query1->all(), 'id', 'id');
                        $items_count = $query1->count();

                        echo '<div style="margin-top:10px; left:-20px;" class="checkbox">
                                      <label>';

                        if(isset($region_id) && !empty($region_id) && in_array($key, $region_id))
                        {
                            echo '<input type="checkbox"  value='.$key.' checked class="regions" id='.$key.'>';
                        }
                        else
                        {
                            echo '<input type="checkbox" value='.$key.' class="regions" id='.$key.'>';
                        }
                        echo  '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        '.$region.' <span class="badge badge-info pull-right">'.$items_count.'</span>
                                      </label>
                                   </div>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php if($provider_type == "") { ?>
        <!-- Destinations types -->
        <div class="widget sidebar-widget">
            <div class="full-search-form">
                <h4 class="widgettitle"><?= \Yii::t('app', 'FILTER BY DESTINATION TYPES') ?> <span class="fa fa-plus" id="show_destinations"></span></h4>

              <!-- ************ Show drop down of Destination types  ************ -->

                <div class="provider_types">
                    <?php
                        $types = DestinationTypes::find()->orderBy('name ASC')->all();
                        $result = ArrayHelper::map($types, 'id', 'name');

                        foreach ($result as $key => $value) { ?>
                            <div style="margin-top:10px; left:-20px;" class="checkbox">
                                <label>
                                <?php if ($key == $provider_type) { ?>
                                    <input class="destinations" type="checkbox" value='<?= $key ;?>' name="destination_type" checked>
                                <?php } else { ?>
                                    <input class="destinations" type="checkbox" value='<?= $key ;?>' name="destination_type">
                                <?php } ?>
                                    <span class="cr">
                                    <i class="cr-icon glyphicon glyphicon-ok"></i>
                                </span>
                                    <?= $value; ?>
                                </label>
                            </div>
                        <?php } ?>
                </div>

                <!-- ************ Show checklist of accommodations  ************ -->

                <div id="accommodation_checklist">
                    <h4 class="widgettitle"><?= \Yii::t('app', 'Accommodation') ?></h4>

                    <?php
                        $all_accommodations = AccommodationTypes::find()->orderBy('name ASC')->all();
                        $all_accommodations = ArrayHelper::map($all_accommodations,'id','name');

                        foreach ($all_accommodations as $key => $one_accommodation)
                        {
                            $query1 = Destinations::find()->where(['accommodation_id' => $key])->all();
                            $result = ArrayHelper::map($query1, 'id', 'id');
                            $items_count = BookableItems::find()->where(['provider_id' => $result])->count();

                            echo '<div style="margin-top:10px; left:-20px;" class="checkbox">
                                      <label>';

                            if(isset($accommodation_id) && !empty($accommodation_id) && in_array($key, $accommodation_id))
                            {
                                echo '<input type="checkbox"  value='.$key.' class="accommodations" checked id='.$key.'>';
                                $this->registerJs("$('#accommodation_checklist').show()");
                            }
                            else
                            {
                                echo '<input type="checkbox" value='.$key.' class="accommodations" id='.$key.'>';
                            }
                            echo  '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                        '.$one_accommodation.' <span class="badge badge-info pull-right">'.$items_count.'</span>
                                      </label>
                                   </div>';
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php } else {?>
            <input id="destination_type" type="text" name="destination_type" hidden="" value="<?= $provider_type;?>">
        <?php } ?>

        <!-- Amenities -->
        <div class="widget sidebar-widget" id="amenities_checklist">
            <div class="full-search-form">
                <h4 class="widgettitle"><?= \Yii::t('app', 'Amenities') ?> <span class="fa fa-plus" id="show_amenities"></span></h4>

                <div >
                    <div class="form-group" >
                        <?php foreach ($all_amenities as $amenity) { ?>
                            <div style="margin-top:10px; left:-20px;" class="checkbox">
                                <label>
                                    <?php if(in_array($amenity->id, $amenities)) { ?>
                                        <input type="checkbox" value='<?= $amenity->id ;?>' class="amenities" id='<?= $amenity->id ;?>' checked>
                                    <?php } else { ?>
                                        <input type="checkbox" value='<?= $amenity->id ;?>' class="amenities" id='<?= $amenity->id ;?>'>
                                    <?php } ?>
                                    <span class="cr">
                                        <i class="cr-icon glyphicon glyphicon-ok"></i>
                                    </span>
                                    <?= $amenity->name; ?>
                                    <span class="badge badge-info pull-right"><?= $amenity->getProvidersAmenities()->count();?></span>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Accommodation types -->
        <div class="widget sidebar-widget" id="accommodation_types_checklist">
            <div class="full-search-form">
                <h4 class="widgettitle"><?= \Yii::t('app', 'Accommodation types') ?> <span class="fa fa-plus" id="show_accommodation_types"></span></h4>

                <div >
                    <div class="form-group" >
                        <?php foreach ($accommodation_types_list as $type) { ?>
                            <div style="margin-top:10px; left:-20px;" class="checkbox">
                                <label>
                                    <?php if(in_array($type->id, $accommodation_types)) {?>
                                        <input type="checkbox" value='<?= $type->id ;?>' class="accommodation_types" id='<?= $type->id ;?>' checked>
                                    <?php } else { ?>
                                        <input type="checkbox" value='<?= $type->id ;?>' class="accommodation_types" id='<?= $type->id ;?>'>
                                    <?php } ?>
                                    <span class="cr">
                                        <i class="cr-icon glyphicon glyphicon-ok"></i>
                                    </span>
                                    <?= $type->name; ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if($provider_type == 6) {?>
            <?=
                $this->render('_restaurant', [
                    'food_types' => $food_types,
                    'food_types_list' => $food_types_list
                ]);
            ?>
        <?php } ?>

        <button id="search" class="btn btn-primary btn-block"><i class="fa fa-search"></i> <?= \Yii::t('app', 'Search') ?> </button>
    </form>
    <br>
</div>

<?php if($provider_type == 11) {
    echo $this->registerJs("$('#amenities_checklist').show()");
    echo $this->registerJs("$('#accommodation_types_checklist').show()");
} ?>
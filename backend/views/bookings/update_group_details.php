<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\BookingsItems;
use common\models\BookableBedsCombinations;
use common\models\RatesCapacityPricing;
use common\models\Rates;
use common\models\BookableBedTypes;
use common\models\BookingsDiscountCodes;
use common\models\BookingsUpsellItems;
use common\models\User;
use common\models\BookingConfirmationTypes;
use common\models\BookingTypes;
use common\models\Destinations;
use common\models\BookingStatuses;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\EstimatedArrivalTimes;
use common\models\BookingSpecialRequests;
use common\models\Countries;
use common\models\BookingItemGroup;

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$provider_id = '';
$items_ids_arr = [];
$items_ids_str = '';

$items = BookingItemGroup::find()->joinWith('bookingItem')->where(['booking_group_id' => $group_id, 'deleted_at' => 0])->all();

//********** maintaining string to show on captaion wether user will show or group guest text will show *********//

$group_string = $bookingGroupModel->group_name;

if(!empty($bookingGroupModel->group_guest_user_id))
{
    $group_string .= !empty($bookingGroupModel->groupGuestUser->first_name)?' - '.$bookingGroupModel->groupGuestUser->first_name:'';
    $group_string .= !empty($bookingGroupModel->groupGuestUser->last_name)?' - '.$bookingGroupModel->groupGuestUser->last_name:'';
    $group_string .= !empty($bookingGroupModel->groupGuestUser->country->name)?' - '.$bookingGroupModel->groupGuestUser->country->name:'';
}
else
{
    $group_string .= !empty($bookingGroupModel->groupUser->profile->first_name)?' - '.$bookingGroupModel->groupUser->profile->first_name:'';
    $group_string .= !empty($bookingGroupModel->groupUser->profile->last_name)?' - '.$bookingGroupModel->groupUser->profile->last_name:'';
    $group_string .= !empty($bookingGroupModel->groupUser->profile->country_id)?' - '.$bookingGroupModel->groupUser->profile->country->name:'';
}


?>
 <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md" style="width: 100%;">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?php echo $group_string; ?></span>
                        <a id="group_settings" class="btn-sm btn-warning pull-right">Update Group Details</a>
                    </div>
                    <ul class="nav nav-tabs bookings_tabs">
                        <li class="active">
                            <!-- <a href="#general_info" data-toggle="tab">General info</a> -->
                        </li>
                    </ul>
                </div>
                
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="col-md-6" style="margin-left: -15px;">
                                    <select style="width: 100%;" class="form-control booking-items-list" >
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <a id="add-to-group" class="btn btn-sm btn-success" style="" >Add to Group</a>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <span style="font-size:20px;text-align:right"><b>Group Balance:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="balance_due"><?php echo Yii::$app->formatter->asDecimal($bookingGroupModel->balance,0);?></span></b></span>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
<div class="modal fade bs-modal-lg" id="add-user-modal" tabindex="1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New User</h4>
            </div>

            <div class="modal-body user-modal-body">

                <div class="alert alert-danger modal-errors-block"></div>
                
                <div class="row">
                    <div class="col-sm-3">
                        <label class=" control-label">Username</label>
                        <input id="username" type="text" class="form-control user-modal-field">
                        <div class="modal-help-block">Username cannot be blank.</div>
                    </div>
                    <div class="col-sm-3">
                        <label class=" control-label">Email</label>
                        <input id="email" type="email" class="form-control user-modal-field" >
                        <div class="modal-help-block">Email cannot be blank.</div>
                        <div class="email-help-block">Email is not a valid email address.</div>
                    </div>
                    <div class="col-sm-3">
                        <label class=" control-label">First Name</label>
                        <input id="fname" type="text" class="form-control user-modal-field">
                        <div class="modal-help-block">First Name cannot be blank.</div>
                    </div>
                    <div class="col-sm-3">
                        <label class=" control-label">Last Name</label>
                        <input id="lname" type="text" class="form-control user-modal-field">
                        <div class="modal-help-block">Last Name cannot be blank.</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Country</label>
                        <div class="form-group" >
                            <select style="width: 100%;" class="form-control" id="country_dropdown" >
                                <?php
                                    $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                    foreach ($countries as $key => $value) 
                                    {
                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">State</label>
                        <div class="form-group" >
                            <select style="width: 100%;" class="form-control" id="state_dropdown" >
                                <option value=''></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">City</label>
                        <div class="form-group" >
                            <select style="width: 100%;" class="form-control" id="city_dropdown" >
                                <option value=''></option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success mt-ladda-btn ladda-button create-btn" data-style="slide-down">
                    Create
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- ************************* Booking Item Group View **************************** -->

<?php $form = ActiveForm::begin([
                    'errorSummaryCssClass' => 'alert alert-danger',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'action' => ['bookings/update-individual-items'],
                ]); ?>

<?php
	if(!empty($items))
	{
		foreach ($items as $item)
		{
			echo $this->render('booking_item_group_view', [
                'id' => $item->booking_item_id,
            ]);

            $provider_id = $item->bookingItem->provider_id;
            $items_ids_arr[] = $item->booking_item_id;
		}
	}	
?>

<button type="submit" class="btn btn-primary update-item">Update</button>
<a class="btn btn-default" href="<?= Url::to(['/bookings/index','pageNo' => $pageNo]) ?>" >Cancel</a>

<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin([
                    'errorSummaryCssClass' => 'alert alert-danger',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'action' => ['bookings/update-all'],
                ]); ?>

<!-- ************************* Group modal **************************** -->

<div id="group-modal" class="modal fade bs-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Group/All Items Settings</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class=" control-label">Group Name</label>
                        <input id="modal_group_name" name="BookingGroup[group_name]" type="text" value="<?php echo ($bookingGroupModel->group_name== null)?$group_name:$bookingGroupModel->group_name ?>" class="form-control">
                    </div>
                </div>
                <br>
                <!-- ####### adding radio button in group modal #######-->
                <div class="row">
                    <div class="col-sm-2">
                        <input id="modal-guest-radio" class="modal-user-radio" type="radio" name="BookingGroup[selected_user]" value="guest_user" <?=($bookingGroupModel->group_guest_user_id != null)?'checked':''?> > Guest<br>
                    </div>
                    <div class="col-sm-2">
                       <input id="modal-user-radio" class="modal-user-radio" type="radio" name="BookingGroup[selected_user]" value="user" <?=($bookingGroupModel->group_user_id != null)?'checked':''?>> User<br>
                    </div>
                </div>
                <!-- #######  #######-->
                <br>
                <div class="row hide" id="modal-user-dropdown" >
                    <div class="col-sm-11">
                        <?= $form->field($bookingGroupModel, 'group_user_id', [
                            'inputOptions' => [
                                'id' => 'user_dropdown',
                                'class' => 'form-control',
                                'style' => 'width: 100%',
                                ]
                        ])->dropdownList(ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_USER])->all(),
                            'id',
                            function($model, $defaultValue) 
                            {
                                if(empty($model->profile->country_id))
                                    $country = '';
                                else
                                    $country = ' - '.$model->profile->country->name;

                                return $model->username.' - '.$model->email.$country;
                            }
                        ),['prompt'=>'Select a User']) ?>
                    </div>
                    <div class="col-sm-1" style="margin-top:35px;">
                        <a id="add_user" ><i class="fa fa-plus fa-2x" style="color: #EC971F" aria-hidden="true"></i></a>
                    </div>
                </div>
                <!-- ####### adding guest details in group modal #######-->
                <div class="row" id="modal-guest-form">
                    <div class="col-sm-4">
                        <label class="control-label">Guest First Name</label>
                        <div class="form-group" >
                            <input type="text" id="modal-guest-first-name" class="form-control" name="BookingGroup[guest_first_name]" value="<?php echo !empty($bookingGroupModel->group_guest_user_id)?$bookingGroupModel->groupGuestUser->first_name:''?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Guest Last Name</label>
                        <div class="form-group" >
                            <input type="text" id="modal-guest-last-name" class="form-control" name="BookingGroup[guest_last_name]" value="<?php echo !empty($bookingGroupModel->group_guest_user_id)?$bookingGroupModel->groupGuestUser->last_name:''?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Guest Country</label>
                        <div class="form-group" >
                            <select id="modal-guest-country-dropdown" style="width: 100%;" class="form-control" name="BookingGroup[guest_country]">
                                <?php
                                    $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                    foreach ($countries as $key => $value) 
                                    {
                                        if($bookingGroupModel->group_guest_user_id != null && !empty($bookingGroupModel->groupGuestUser->country_id))
                                        {
                                            $selected = ($value == $bookingGroupModel->groupGuestUser->country->name) ? 2 : 0 ;
                                        }
                                        else
                                        {
                                            $selected = ($value == 'Iceland') ? 2 : 0 ;
                                        }
                                        
                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- #######  #######-->

                <?php
                    echo $this->render('update_all_modal',[
                            'bookingGroupModel' => $bookingGroupModel,
                            'provider_id' => $provider_id,
                            'items_ids' => implode(',', $items_ids_arr),
                        ]);
                ?>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success mt-ladda-btn ladda-button group-btn" data-style="slide-down">
                    Update Group Data Only
                </button>
                <button type="submit" class="btn btn-info mt-ladda-btn ladda-button update-all-btn" data-style="slide-down">
                    Update All Items
                </button>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php

$add_user_url = Url::to(['/bookings/add-user']);
$calculate_item_price = Url::to(['/bookings/calculate-item-price']);
$indexPage = Url::to(['/bookings/index','pageNo' => $pageNo]);
$searchBookingItemsUrl = Url::to(['/bookings/search-booking-item']);
$delete_item_url = Url::to(['/bookings/delete-booking-item-from-group']);
$link_item_url = Url::to(['/bookings/link-item-to-group']);
$unlink_item_url = Url::to(['/bookings/unlink-item-from-group']);

$this->registerJs("
jQuery(document).ready(function() 
{
    var provider_id = '$provider_id';

    // ********** Update all JS *************** //

    $(document).on('click', '#update_all', function (e) {
        $('#update_all_modal').modal('show');
    });

    $('.update-all-dropdowns').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('.update-all-offers-dropdown').select2({
        placeholder: \"Select an Offer\",
        allowClear: true
    });

    $('.update-all-travel-partner-dropdown').select2({
        placeholder: \"Select a Travel Partner\",
        allowClear: true
    });

    $(document).on('click','.group-btn',function()
    {
        $('#setting_type').val('update-group');
    });

    $(document).on('click','.update-all-btn',function()
    {
        $('#setting_type').val('update-all');
    });

    $('.update-all-travel-partner-dropdown').on('select2:select', function (evt) 
    {
        $('.update-all-vou_ref_div').removeClass('hide');
        $('.update-all-offers-dropdown').attr('disabled','disabled');
    });

    $('.update-all-travel-partner-dropdown').on('select2:unselecting ', function (e) 
    {
        $('.update-all-vou_ref_div').addClass('hide');
        $('.update-all-offers-dropdown').attr('disabled',false);
        $('.update-all-travel-partner-dropdown').val('');
    });

    $('.update-all-offers-dropdown').on('select2:select', function (evt) 
    {
        $('.update-all-travel-partner-dropdown').attr('disabled','disabled');
    });

    $('.update-all-offers-dropdown').on('select2:unselecting ', function (e) 
    {
        $('.update-all-travel-partner-dropdown').attr('disabled',false);
        $('.update-all-offers-dropdown').val('');
    });

    function updateItemPrices(model_id = '')
    {
        $('.helper-field').each(function()
        {
            var arr = $(this).val().split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];

            calculatePriceForChangedElements(rate_id,item_id,date,model_id);
        });
    }

    // ***************************************** //

    $(document).on('click','#group_settings',function()
    {
        $('#group-modal').modal('show');
    });

    $('body').on('click', '.odom-submit', function (e) {
        $(this.form).submit();
        $('#group-modal').modal('hide');
    });

    $('#user_dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true,
        dropdownParent: $('#group-modal')
    });

    $('#country_dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true,
        dropdownParent: $('#add-user-modal')
    });

    $('#state_dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true,
        dropdownParent: $('#add-user-modal')
    });

    $('#city_dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true,
        dropdownParent: $('#add-user-modal')
    });

    $('#modal-guest-country-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true,
        dropdownParent: $('#group-modal')
    });

    $('#modal_view_details').trigger('click');

    $('.booking-items-list').select2({
        placeholder: \"Select a Booking Item\",
        width: '100%',
        minimumInputLength: 1,
        allowClear: true,
        ajax: {
            url: '$searchBookingItemsUrl',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    group_id: group_id,
                    provider_id:provider_id,
                };
            },
        processResults: function (data, params) {
          return {
            results: data.items,
          };
        },
        cache: true
      },
    });

    $('.booking-items-list').on('select2:unselecting ', function (e) 
    {
        $(this).val('');
    });

    $('.modal-guest-country').select2({
        placeholder: \"Select a Travel Partner\",
        allowClear: true
    });

    $('.modal-rate-details-dropdowns').select2({
        placeholder: \"Select an Option\",
    });

    $('.modal-user-dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true
    });

    $('.offers-dropdown').select2({
        placeholder: \"Select an Offer\",
        allowClear: true
    });

    $('.travel-partner-dropdown').select2({
        placeholder: \"Select a Travel Partner\",
        allowClear: true
    });

    $('.travel-partner-dropdown').on('select2:select', function (evt) 
    {
        //alert('here');
        // var arr = $(this).attr('data').split(':');
        // var item_id = arr[0];
        // var rate_id = arr[1];
        var model_id =  $(this).attr('data');

        $('.offers-dropdown-'+model_id).attr('disabled','disabled');
        //calculatePriceForChangedElements(item_id,rate_id,model_id);
        $('.vou_ref_div-'+model_id).removeClass('hide');
        updateItemPrices(model_id);
    });

    $('.travel-partner-dropdown').on('select2:unselecting ', function (e) 
    {
        //var arr = $(this).attr('data').split(':');
        // var item_id = arr[0];
        // var rate_id = arr[1];
        var model_id =  $(this).attr('data');

        $('.offers-dropdown-'+model_id).attr('disabled',false);
        $('.travel-partner-dropdown-'+model_id).val('');

        //calculatePriceForChangedElements(item_id,rate_id,model_id);
        $('.vou_ref_div-'+model_id).addClass('hide');
        updateItemPrices(model_id);
    });

    $('.offers-dropdown').on('select2:select', function (evt) 
    {
        //var arr = $(this).attr('data').split(':');
        // var item_id = arr[0];
        // var rate_id = arr[1];
        var model_id =  $(this).attr('data');

        $('.travel-partner-dropdown-'+rate_id+'-'+model_id).attr('disabled','disabled');
        //calculatePriceForChangedElements(item_id,rate_id,model_id);
        updateItemPrices(model_id);
    });

    $('.offers-dropdown').on('select2:unselecting ', function (e) 
    {
        //var arr = $(this).attr('data').split(':');
        // var item_id = arr[0];
        // var rate_id = arr[1];
        var model_id =  $(this).attr('data');

        $('.travel-partner-dropdown-'+rate_id+'-'+model_id).attr('disabled',false);
        $('.offers-dropdown-'+rate_id+'-'+model_id).val('');

        //calculatePriceForChangedElements(item_id,rate_id,model_id);
        updateItemPrices(model_id);
    });

    $('.total-tooltips').tooltip();

    // ************** Calculate Price for Item Quantity **************** //

    $(document).on('change','.item-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });

    // ************** Calculate Price for Capacity Pricing **************** //

    /* ///////////////// OLD JS BEGIN /////////////////////////
    $(document).on('change','.capacity-pricing',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var checked = $(this).is(':checked');

        var capacityPricingElement = $('.cpricing-'+rate_id+'-'+model_id);
        $(capacityPricingElement).prop('checked', false);

        if(checked)
        {
            $(this).prop('checked',true);
        }
        else
        {
            $(this).prop('checked', false);
        }

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });

    // ************** Calculate Price for Adults Dropdown ****************** //

    $(document).on('change','.adults-dropdown',function()
    {   

        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];
        console.log(item_id+' '+rate_id+' '+model_id);

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });

    // ************** Calculate Price for Children Dropdown ****************** //
    
    $(document).on('change','.children-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });
      ////////////////////OLD JS END /////////////////////////*/

    /*///////////////////// NEW JS BEGIN //////////////////////*/

        $(document).on('change','.adults-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var date = $(this).attr('date');

        // ******** Unselect other dropdowns on select current ********* //

        var no_of_adults = $(this).val();
        var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

        var temp_arr = '';
        var temp_rate_id = '';
        var temp_date = '';

        $('.children-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adults-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adult-dropdown-'+rate_id+'-'+date).val(no_of_adults);
        $('.child-dropdown-'+rate_id+'-'+date).val(no_of_children);

        calculatePriceForChangedElements(rate_id,item_id,date,model_id);
    });

    // ******************** Calculate Price for Children ******************** //

    $(document).on('change','.children-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var date = $(this).attr('date');

        // ******** Unselect other dropdowns on select current ********* //

        var no_of_children = $(this).val();
        var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();

        var temp_arr = '';
        var temp_rate_id = '';
        var temp_date = '';

        $('.children-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date,model_id);
            }
        });

        $('.adults-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adult-dropdown-'+rate_id+'-'+date).val(no_of_adults);
        $('.child-dropdown-'+rate_id+'-'+date).val(no_of_children);

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Capacity Pricing **************** //
    
    $(document).on('change','.capacity-pricing',function()
    {
        var checked = $(this).is(':checked');
        var person_no = $(this).attr('data');

        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[3];

        var date = $(this).attr('date');

        var temp_arr = '';
        var temp_rate_id = '';

        $('.capacity-pricing-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('id').split(':');
            temp_rate_id = temp_arr[1];

            $(this).prop('checked', false);
            setPriceToZero(temp_rate_id,date);
        });

        if(checked)
        {
            $(this).prop('checked',true);             
        }
        else
        {
            $(this).prop('checked', false);
        }

        calculatePriceForChangedElements(rate_id,item_id,date,model_id);
    });

    function setPriceToZero(rate_id,date)
    {
        $('.item-total-input-'+rate_id+'-'+date).val(0);
        $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
        $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
        $('.item-total-nights-price-'+rate_id+'-'+date).html('');
    }


    /*///////////////////// NEW JS END ///////////////////////*/

    /* ////////////////////// OLD JS BEGIN /////////////////////////
    // ************** Calculate Price for Upsell Items **************** //
    
    $(document).on('change','.upsell-items',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });

    // ************** Calculate Price for Upsell Item Quantity ****************** //

    $(document).on('change','.upsell-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });

    ///////////////////////// OLD JS END /////////////////////////*/

    /////////////////////////// NEW JS BEGIN /////////////////////

    $(document).on('change','.upsell-items',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var upsell_id = $(this).attr('data');
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).css('display','none');
        }

        calculatePriceForChangedElements(rate_id,item_id,date,model_id);
    });

    // ************** Calculate Price for Upsell Item Quantity ****************** //

    $(document).on('change','.upsell-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var date = $(this).attr('date');

        calculatePriceForChangedElements(rate_id,item_id,date,model_id);
    });

    ////////////////////////// NEW JS END ///////////////////

    // ************** Calculate Price for Discount Code Quantity ****************** //
    /* /////////////////////////// OLD JS BEGIN //////////////////////////
    $(document).on('change','.discount-code-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });

    // ************** Calculate Price for Discount Codes ****************** //

    $(document).on('change','.discount-codes',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        calculatePriceForChangedElements(item_id,rate_id,model_id);
    });
    ////////////////////////// OLD JS END ///////////////////////////*/


    //////////////////////// NEW JS BEGIN //////////////////////////

        $(document).on('change','.discount-codes',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var discount_id = $(this).attr('data');
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).css('display','none');
        }

        calculatePriceForChangedElements(rate_id,item_id,date,model_id);
    });

    // ************** Calculate Price for Discount Code Quantity ****************** //

    $(document).on('change','.discount-code-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var model_id = arr[2];

        var date = $(this).attr('date');

        calculatePriceForChangedElements(rate_id,item_id,date,model_id);
    });

    //////////////////////// NEW JS END /////////////////////////

    // ************** Common function to calculate price **************** //
    /*///////////////////////////// OLD FUNCTION ////////////////////////
    function calculatePriceForChangedElements(item_id = '', rate_id = '', model_id = '')
    {

        var no_of_adults = 0;
        var no_of_children = 0;

        var total_nights = $('.total-nights').val();
        var pricing_type = $('.pricing-type').val();

        alert(pricing_type);
        var upsell_ids =[];
        var discount_ids =[];

        var travel_partner_id = '';
        var offer_id = '';

        var person_no = 0;

        var item_id = item_id;
        var rate_id = rate_id;

        var quantity = $('.item-quantity-'+rate_id+'-'+model_id).val();

        // ******** get Upsell Items Id ********* //

        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+model_id);
        $(upsellItemsElement).each(function()
        {
            if(this.checked)
            {
                var upsell_id = $(this).attr('data');
                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+model_id).val());
            }
        });

        // ******** get Discount Id********* //

        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+model_id);
        $(discountCodesElement).each(function()
        {
            if(this.checked)
            {
                var discount_id = $(this).attr('data');
                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+model_id).val());
            }
        });

        if(discount_ids.length == 0)
            discount_ids = '';

        if(upsell_ids.length == 0)
            upsell_ids = '';

        if($('.travel-partner-dropdown-'+rate_id+'-'+model_id).val()!='')
            travel_partner_id = $('.travel-partner-dropdown-'+rate_id+'-'+model_id).val();

        if($('.offers-dropdown-'+rate_id+'-'+model_id).val()!='')
            offer_id = $('.offers-dropdown-'+rate_id+'-'+model_id).val();

        if(pricing_type == '1') // *********** Capacity Pricing ********** //
        {
            var capacity_flag = 0;

            var capacityPricingElement = $('.cpricing-'+rate_id+'-'+model_id);
            $(capacityPricingElement).each(function()
            {
                if(this.checked)
                {
                    capacity_flag = 1;
                    person_no = $(this).attr('data');
                }
            });

            if(capacity_flag)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'person_no' : person_no,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+model_id).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+model_id).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+model_id).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+model_id).val(data.orgPrice);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });  
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+model_id).val(0);
                $('.item-total-'+rate_id+'-'+model_id).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+model_id).siblings('span').html('0');
            }
        }
        else // *********** First/Additional Pricing ********** //
        {
            no_of_adults = $('.adult-dropdown-'+rate_id+'-'+model_id).val();
            no_of_children = $('.child-dropdown-'+rate_id+'-'+model_id).val();

            if(no_of_children > 0 || no_of_adults > 0)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'no_of_adults' : no_of_adults,
                                'no_of_children' : no_of_children,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+model_id).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+model_id).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+model_id).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+model_id).val(data.orgPrice);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+model_id).val(0);
                $('.item-total-'+rate_id+'-'+model_id).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+model_id).siblings('span').html('0');
            }
        }
    }
    ////////////////////////////OLD FUNCTION END //////////////////////*/

    ////////////////// NEW FUNCTION BEGIN ////////////////////////

    function calculatePriceForChangedElements(rate_id = '', item_id = '',date = '',model_id = '')
    {
        //alert(model_id);
        var custom_rate = '';
        var no_of_adults = 0;
        var no_of_children = 0;

        var upsell_ids =[];
        var discount_ids =[];

        var travel_partner_id = '';
        var offer_id = '';
        
        var person_no = 0;
        total_nights = 1;

        var quantity = $('.item-quantities-'+item_id+'-'+date).val();

        // ******** get Upsell Items Id ********* //

        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
        $(upsellItemsElement).each(function()
        {
            if(this.checked)
            {
                var upsell_id = $(this).attr('data');
                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
            }
        });

        // ******** get Discount Id********* //

        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
        $(discountCodesElement).each(function()
        {
            if(this.checked)
            {
                var discount_id = $(this).attr('data');
                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
            }
        });

        if(discount_ids.length == 0)
            discount_ids = '';

        if(upsell_ids.length == 0)
            upsell_ids = '';

        //alert($('.travel-partner-dropdown-'+model_id).val());
        if($('.travel-partner-dropdown-'+model_id).val()!='')
            travel_partner_id = $('.travel-partner-dropdown-'+model_id).val();

        if($('.offers-dropdown-'+model_id).val()!='')
            offer_id = $('.offers-dropdown-'+model_id).val();

        if(pricing_type) // *********** Capacity Pricing ********** //
        {
            var capacity_flag = 0;

            var capacityPricingElement = $('.cpricing-'+rate_id+'-'+date);
            $(capacityPricingElement).each(function()
            {
                if(this.checked)
                {
                    capacity_flag = 1;

                    person_no = $(this).attr('data');
                    var sibs = $(this).siblings();

                    $.each( sibs, function() {
                        tr = $(this).val();
                        console.log(tr);
                        if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                        {
                            console.log('jhere');
                            custom_rate = tr.replace('.','');
                        }
                        
                    });

                }
                console.log('custom rate : '+custom_rate);
            });

            if(capacity_flag)
            {
                //alert('travel partner :'+ travel_partner_id);
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'custom_rate': custom_rate,
                                'person_no' : person_no,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                                'dates': $('.item-date-'+item_id+'-'+date).val()
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                        //$('.item-total-nights-price-'+rate_id+'-'+date).html('<br>= '+data.item_total_nights_price);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });  
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+date).val(0);
                $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
                $('.item-total-nights-price-'+rate_id+'-'+date).html('');
            }
        }
        else // *********** First/Additional Pricing ********** //
        {
            no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
            no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

            if(no_of_children > 0 || no_of_adults > 0)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'no_of_adults' : no_of_adults,
                                'no_of_children' : no_of_children,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                                'dates': $('.item-date-'+item_id+'-'+date).val()
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                        //$('.item-total-nights-price-'+rate_id+'-'+date).html('<br>= '+data.item_total_nights_price);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+date).val(0);
                $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
                $('.item-total-nights-price-'+rate_id+'-'+date).html('');
            }
        }
    }

    /////////////////// NEW FUNCTION END ////////////////////////

    //########### when the page loads first time setting modal values ############//
    var sr=$(`[name='BookingGroup[selected_user]']:checked`).val();
    if(sr == 'user')
    {   
        $('#modal-user-dropdown').removeClass('hide');
        $('#modal-guest-form').addClass('hide');
    }
    else
    {   $('#modal-guest-form').removeClass('hide');
        $('#modal-user-dropdown').addClass('hide');
    }

    //############ on change for radio buttons in modal ##############//

    $(document).on('change','input[type=radio]',function(){
        var sr=$(`[name='BookingGroup[selected_user]']:checked`).val();
        if(sr == 'user')
        {   
            $('#modal-user-dropdown').removeClass('hide');
            $('#modal-guest-form').addClass('hide');
        }
        else
        {   $('#modal-guest-form').removeClass('hide');
            $('#modal-user-dropdown').addClass('hide');
        }
    });

    // ************** Show item dropdowns on click view button ******************** //
    

    $(document).on('click','.view-details',function()
    {
        var item_id = $(this).attr('data');

        if($(this).find('span').text() == 'View Details')
        {
            $('.rate-details-fields-'+item_id).css('display', 'block');
            $(this).find('span').text('Hide Details');

            $(this).removeClass('btn-default');
            $(this).addClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-down');
            $(this).find('i').addClass('fa-chevron-up');
        }
        else if($(this).find('span').text()  == 'Hide Details')
        {
            $('.rate-details-fields-'+item_id).css('display', 'none');
            $(this).find('span').text('View Details');

            $(this).addClass('btn-default');
            $(this).removeClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-up');
            $(this).find('i').addClass('fa-chevron-down');
        }
    });

    $('#add_user').click(function()
    {
        $('#add-user-modal').modal('show');

        $('.user-modal-body').find('input').each(function()
        {
            $(this).val('');
        });

        $('.modal-help-block').hide();
        $('.email-help-block').hide();
        $('.modal-errors-block').hide();

        $('#country_dropdown').val(100).trigger('change');
    });

    $('.create-btn').click(function()
    {
        var flag = 0;

        $('.user-modal-body').find('input').each(function()
        {
            if($(this).attr('id')!='lname' && $(this).val()=='')
            {
                $(this).siblings('.modal-help-block').show();
                flag = 1;
            }
        });

        if(!flag)
        {
            $('.modal-help-block').hide();

            if(!isValidEmailAddress($('#email').val()))
            {
                $('.email-help-block').show();
            }
            else
            {
                $('.email-help-block').hide();

                // *************** send ajax call to add User Dynamically *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'username' : $('#username').val(),
                                'email' : $('#email').val(),
                                'fname' : $('#fname').val(),
                                'lname' : $('#lname').val(),
                                'country_id' : $('#country_dropdown').val(),
                                'state_id' : $('#state_dropdown').val(),
                                'city_id' : $('#city_dropdown').val(),
                            };

                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$add_user_url',
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        data = jQuery.parseJSON( data );

                        if(data.error == '1')
                        {
                            $('.modal-errors-block').empty().html(data.error_str);
                            $('.modal-errors-block').show();
                        }
                        else
                        {
                            $('#add-user-modal').modal('hide');
                            $('#user_dropdown').empty().html(data.users);
                            $('.user-dropdown').empty().html(data.users);
                            $('#user_dropdown').val(data.user_id).trigger('change');
                        }
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
        }
    });

    $(document).on('click','#delete-item',function()
    {
        if(confirm('Are you sure you want to delete this item ?'))
        {
            $.ajax(
            {
                type: 'GET',
                url: '$delete_item_url?id='+$(this).attr('data'),
                success: function(data)
                {
                    location.reload();
                },
                error: function()
                {
                    alert('error');
                }
            });
        }
    });

    $(document).on('click','#add-to-group',function()
    {
        var booking_item_id = $('.booking-items-list').val();

        if(booking_item_id=='')
        {
            alert('Please select a booking item');
        }
        else
        {
            // if(confirm('Are you sure you want to link item to this group ?'))
            // {
                var object = {
                                    'id' : booking_item_id,
                                    'group_id' : group_id,
                                };

                $.ajax(
                {
                    type: 'POST',
                    url: '$link_item_url',
                    data: object,
                    success: function(data)
                    {
                        location.reload();
                    },
                    error: function()
                    {
                        alert('error');
                    }
                });
            // }
        }
    });

    $(document).on('click','#unlink-item',function()
    {
        // if(confirm('Are you sure you want to unlink item from this group ?'))
        // {
            $.ajax(
            {
                type: 'GET',
                url: '$unlink_item_url?id='+$(this).attr('data'),
                success: function(data)
                {
                    alert(data);
                },
            });
        // }
    });

});",View::POS_END);
?>

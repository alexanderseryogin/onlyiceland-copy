<?php

use yii\db\Migration;

class m180316_121621_alter_table_bookings_items_add_fields extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `no_of_booking_days` INT(11) NULL AFTER `reference_no`,
                          ADD COLUMN `averged_total` FLOAT(11) NULL AFTER `no_of_booking_days`,
                          ADD COLUMN `net_total` FLOAT(11) NULL AFTER `averged_total`,
                          ADD COLUMN `gross_total` FLOAT(11) NULL AFTER `net_total`,
                          ADD COLUMN `paid_amount` FLOAT(11) NULL AFTER `gross_total`;
                        ');
    }

    public function safeDown()
    {
        echo "m180316_121621_alter_table_bookings_items_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180316_121621_alter_table_bookings_items_add_fields cannot be reverted.\n";

        return false;
    }
    */
}

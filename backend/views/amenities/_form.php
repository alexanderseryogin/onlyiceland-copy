<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Amenities;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model common\models\Amenities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portlet light ">
	<div class="amenities-form">

	    <?php $form = ActiveForm::begin([
	        'errorSummaryCssClass' => 'alert alert-danger',
	        'options' => ['enctype' => 'multipart/form-data'],
	    ]); ?>

	    <div class="row">
	    	<div class="col-sm-6">
	    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	    	</div>

	    	<div class="col-sm-6">
	    		<?= $form->field($model, 'type', [
                        'inputOptions' => [
                                        'id' => 'amenities_type_dropdown',
                                        'class' => 'form-control',
                                        'style' => 'width: 100%'
                                        ]
                ])->dropdownList(Amenities::$types,['prompt'=>'Select a Type']) ?>
	    	</div>
	    </div>

	    <?php
	        if(!empty($model->icon))
	        {
	            echo '<img src='.Yii::getAlias('@web').'/../uploads/amenities/'.$model->id.'/icons/'.$model->icon.' height="50" width="50">';
	            echo '<br>'; 
	        }
	    ?>

	    <?= $form->field($model, 'temp_icon')->fileInput() ?>

	    <span class="label label-danger"> NOTE! </span>
	    <span>&nbsp; Recommended Dimensions less or equal (50x50) </span><br><br>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	        <a class="btn btn-default" href="<?= Url::to(['/amenities']) ?>" >Cancel</a>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#amenities_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

});",View::POS_END);
?>

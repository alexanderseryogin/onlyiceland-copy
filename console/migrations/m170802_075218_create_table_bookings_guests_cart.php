<?php

use yii\db\Migration;

class m170802_075218_create_table_bookings_guests_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_guests_cart` (
                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                      `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                      `country_id` int(11) DEFAULT NULL,
                      PRIMARY KEY (`id`),
                      KEY `FK_booking_guests_cart_country_id` (`country_id`),
                      CONSTRAINT `FK_booking_guests_cart_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL
                    ) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
    }

    public function down()
    {
        echo "m170802_075218_create_table_bookings_guests_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace console\controllers;
use Yii;
use DateTime;
use common\models\Destinations;
use common\models\Notifications;
use common\models\User;

class NunaCommand extends \yii\console\Controller
{
    public function actionWeeklyNotification()
    {
        $this->NoRateAvailableNotification(); // weekly
    }

    public function NoRateAvailableNotification()
    {
        /*Yii::$app->mailer->compose()
        ->setFrom([Yii::$app->params['supportEmail'] => 'Onlyiceland'])
        ->setTo('granjur.ali@gmail.com')
        ->setSubject('No Rate Available Notification')
        ->setTextBody('Plain text content')
        ->send();*/

        foreach(Destinations::find()->each(10) as $key => $destination)  // get all destinations
        {
            if(!empty($destination->bookableItems))
            {
                foreach ($destination->bookableItems as $key => $bookableItem) // get all bookable items
                {
                    Notifications::generateNoRateAvailableNotifications($bookableItem);
                }
            }
        }
    }
}

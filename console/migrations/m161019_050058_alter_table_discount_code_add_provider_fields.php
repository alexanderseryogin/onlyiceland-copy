<?php

use yii\db\Migration;

class m161019_050058_alter_table_discount_code_add_provider_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `discount_code` ADD COLUMN `provider_id` INT(11) NULL AFTER `active`, ADD COLUMN `provider_type_id` INT(11) NULL AFTER `provider_id`, ADD COLUMN `state_id` INT(11) NULL AFTER `provider_type_id`, ADD CONSTRAINT `FK_discount_code_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_discount_code_provider_type_id` FOREIGN KEY (`provider_type_id`) REFERENCES `provider_types`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_discount_code_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m161019_050058_alter_table_discount_code_add_provider_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

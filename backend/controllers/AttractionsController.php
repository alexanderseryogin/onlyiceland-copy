<?php

namespace backend\controllers;

use Yii;
use common\models\Attractions;
use common\models\AttractionsSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;
use common\models\UploadFile;

use common\models\AttractionsImages;
use common\models\AttractionTypeFields;
use common\models\AttractionTypeFieldsData;
use common\models\AttractionAndTags;
/**
 * AttractionsController implements the CRUD actions for Attractions model.
 */
class AttractionsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'upload-images' => ['POST'],
                ],
            ],
        ];
    }

    public function actionListFields()
    {
        $type_id = $_GET['type_id'];
        $attraction_id = $_GET['model_id'];
        $data = '';

        $fields = AttractionTypeFields::find()
                ->where(['at_id' => $type_id])
                ->all();

        foreach ($fields as $field) 
        {
            echo '<label class="control-label">'.$field->label.'</label>';
            echo '<div class="form-group">';
            $field->label = strtolower(str_replace(' ','_', $field->label));

            if(!empty($_GET['model_id']))
            {
                $data = AttractionTypeFieldsData::find()
                        ->where(['attraction_id' => $attraction_id])
                        ->andWhere(['atf_id' => $field->id])
                        ->one();
            }
            $value = isset($data->value)?$data->value:'';

            echo '<input type="'.$field->type.'" class="form-control" value = "'.$value.'" name="'.$field->label.'" id="'.$field->label.'">';
            echo '</div>'; 
        }        
    }

    public function actionArrangeData()
    {
        if($_POST['action']=='up')
        {
            $model = AttractionsImages::findOne(['id'=>$_POST['row_id']]);
            $previous_model = AttractionsImages::find()
                                                    ->where(['attraction_id' => $model->attraction_id])
                                                    ->andWhere(['<','display_order',$model->display_order])
                                                    ->orderBy('display_order DESC')
                                                    ->one();

            $temp = $model->display_order;
            $model->display_order = $previous_model->display_order;
            $previous_model->display_order = $temp;

            $model->save();
            $previous_model->save();
        }
        else if($_POST['action']=='down')
        {
            $model = AttractionsImages::findOne(['id'=>$_POST['row_id']]);
            $next_model = AttractionsImages::find()
                                                    ->where(['attraction_id' => $model->attraction_id])
                                                    ->andWhere(['>','display_order',$model->display_order])
                                                    ->orderBy('display_order ASC')
                                                    ->one();

            $temp = $model->display_order;
            $model->display_order = $next_model->display_order;
            $next_model->display_order = $temp;

            $model->save();
            $next_model->save();
        }
    }

    public function actionGetDescription()
    {
        $model = AttractionsImages::findOne(['id'=>$_POST['image_id']]);
        return $model->description;
    }

    public function actionSetDescription()
    {
        $model = AttractionsImages::findOne(['id'=>$_POST['image_id']]);
        $model->description = $_POST['description'];
        $model->save();
        return ;
    }

    public function actionUploadImages()
    {
        $uploadFile = new UploadFile();

        $session = Yii::$app->session;

        $uploadFile->FileToUpload = $_FILES;

        if($uploadFile->uploadImage('attractions','temp','images'))
        {
            if(!empty($_SESSION['images']))
            {
                $count = count($_SESSION['images']);
            }
            else
            {
                $count=0;
            }
            
            $_SESSION['images'][$count] = $uploadFile->FileName;
        }
    }

    /**
     * Lists all Attractions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttractionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Attractions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Attractions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Attractions();

        if($model->load(Yii::$app->request->post())) 
        {
            /*echo '<pre>';
            print_r($_POST);
            exit;*/

            $model->phone = $model->server_phone;
            $arr = explode('_', $model->price);
            $model->price =  $arr[count($arr)-1];

            $model->save();

            $model->tags = $_POST['Attractions']['tags'];

            if(!empty($model->tags))
            {
                foreach ($model->tags as $key => $value) 
                {
                    $tags_model = new AttractionAndTags();
                    $tags_model->attr_id = $model->id;
                    $tags_model->tag_id = $value;
                    $tags_model->save();
                }
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id, 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id.'/images'))
            {
                 mkdir(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id.'/images', 0777, true);
            }

            $srcPath = Yii::getAlias('@app'). '/../uploads/attractions/temp/images/';
            $destPath = Yii::getAlias('@app').'/../uploads/attractions/'.$model->id.'/images/';

            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
            {
                foreach ($_SESSION['images'] as $key => $value) 
                {
                    if(copy($srcPath.$value, $destPath.$value))
                    {
                        $obj = new AttractionsImages();
                        $obj->attraction_id = $model->id;
                        $obj->image = $value;

                        $last_model = AttractionsImages::find()
                                                    ->where(['attraction_id' => $model->id])
                                                    ->orderBy('display_order DESC')
                                                    ->one();
                        if(!empty($last_model))
                        {
                            $obj->display_order = $last_model->display_order+1;
                        }
                        else
                        {
                            $obj->display_order = 1;
                        }

                        $obj->save();

                        unlink($srcPath.$value);
                    }   
                }

                $this->removeDirectory($srcPath);
            }
            
            $errors['attraction'] = $model->getErrors();

            return $this->redirect(['index']);
        } 
        else 
        {
            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                unset($_SESSION['images']);

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function removeDirectory($path) 
    {
        $files = glob($path . '/*');
        foreach ($files as $file) 
        {
          is_dir($file) ? removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }


    /**
     * Updates an existing Attractions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $get_tags = AttractionAndTags::find()->where(['attr_id' => $id])->all();

        if(!empty($get_tags))
        {
            foreach ($get_tags as $key => $value) 
            {
                $saved_tags[] = $value['tag_id'];
            }
            $model->tags = $saved_tags;
        }

        $searchModel = new AttractionsSearch();
        $dataProvider = $searchModel->searchImages($id);

        if ($model->load(Yii::$app->request->post())) 
        {
            $model->phone = $model->server_phone;
            $arr = explode('_', $model->price);
            $model->price =  $arr[count($arr)-1];

            $model->save(); 

            // *************** Save Tags *************** //

            AttractionAndTags::deleteAll(['attr_id' => $model->id]);

            $model->tags = $_POST['Attractions']['tags'];

            if(!empty($model->tags))
            {
                foreach ($model->tags as $key => $value) 
                {
                    $tags_model = new AttractionAndTags();
                    $tags_model->attr_id = $model->id;
                    $tags_model->tag_id = $value;
                    $tags_model->save();
                }
            }

            // *************** Save Images *************** //

            if(!file_exists(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id, 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id.'/images'))
            {
                 mkdir(Yii::getAlias('@app').'/../uploads/attractions/'.$model->id.'/images', 0777, true);
            }

            $srcPath = Yii::getAlias('@app'). '/../uploads/attractions/temp/images/';
            $destPath = Yii::getAlias('@app').'/../uploads/attractions/'.$model->id.'/images/';

            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
            {
                foreach ($_SESSION['images'] as $key => $value) 
                {
                    if(copy($srcPath.$value, $destPath.$value))
                    {
                        $obj = new AttractionsImages();
                        $obj->attraction_id = $model->id;
                        $obj->image = $value;

                        $last_model = AttractionsImages::find()
                                                    ->where(['attraction_id' => $model->id])
                                                    ->orderBy('display_order DESC')
                                                    ->one();
                        if(!empty($last_model))
                        {
                            $obj->display_order = $last_model->display_order+1;
                        }
                        else
                        {
                            $obj->display_order = 1;
                        }

                        $obj->save();

                        unlink($srcPath.$value);
                    }   
                }

                $this->removeDirectory($srcPath);
            }
            
            $errors['BookableItems'] = $model->getErrors();

            return $this->redirect(['index']);
        } 
        else 
        {
            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                unset($_SESSION['images']);

            return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionDeleteImage()
    {
        $image = AttractionsImages::findOne(['id'=>$_POST['image_id']]);

        if(!empty($image))
        {
            $Path = Yii::getAlias('@app').'/../uploads/attractions/'.$image->attraction_id.'/images/'.$image->image;
            unlink($Path);
            $image->delete();
        }
    }

    /**
     * Deletes an existing Attractions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Attractions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attractions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attractions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

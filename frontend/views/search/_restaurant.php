<?php

?>

<!-- FILTER BY FOOD/DRINK TYPE -->
<div class="widget sidebar-widget" id="food_types_checklist">
    <div class="full-search-form">
        <h4 class="widgettitle"><?= \Yii::t('app', 'FILTER BY FOOD/DRINK TYPE') ?> <span class="fa fa-plus" id="show_food_types"></span></h4>

        <div >
            <div class="form-group" >
                <?php foreach ($food_types_list as $type) { ?>
                    <div style="margin-top:10px; left:-20px;" class="checkbox">
                        <label>
                            <?php if(in_array($type->id, $food_types)) {?>
                                <input type="checkbox" value='<?= $type->id ;?>' class="food_types" id='<?= $type->id ;?>' checked>
                            <?php } else { ?>
                                <input type="checkbox" value='<?= $type->id ;?>' class="food_types" id='<?= $type->id ;?>'>
                            <?php } ?>
                            <span class="cr">
                                <i class="cr-icon glyphicon glyphicon-ok"></i>
                            </span>
                            <?= $type->name; ?>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
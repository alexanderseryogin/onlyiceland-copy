<?php

use yii\db\Migration;

class m170410_072402_create_booking_dates extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `booking_dates`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `booking_item_id` INT(11) UNSIGNED, `item_id` INT(11), `date` DATE, `quantity` INT(11), PRIMARY KEY (`id`), CONSTRAINT `Fk_booking_dates_booking_item_id` FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_booking_dates_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170410_072402_create_booking_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

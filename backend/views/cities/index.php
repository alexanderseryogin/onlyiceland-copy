<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\States;
use common\models\Countries;
use common\models\CountriesSearch;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = $this->title;

$session = Yii::$app->session;
$session->open();

?>
<script type="text/javascript">

</script>
<div class="cities-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW CITY'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        Pjax::begin(['id' => 'cities-gridview','timeout' => 1000000, 'enablePushState' => false]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'state_id',
            [
                'attribute' => 'country_id',
                'label' => 'Country',
                'value' => function($data){
                    return $data->state->country->name;
                },
                'filter' => Html::dropDownList(
                    'CitiesSearch[country_id]', 
                    $searchModel['country_id'], 
                    ArrayHelper::map(Countries::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Country', 
                        'id'=>'CitiesSearch-countries', 
                        'class'=>'form-control',
                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/liststates-grid?country_id=').'"+$(this).val(), function( data ) 
                            {
                                $("#CitiesSearch-states").html(data );
                            });'
                    ]),
                'contentOptions' =>['style' => 'width:20%'],
            ],
            [
                'attribute' => 'state_id',
                'value' => function($data){
                    return $data->state->name;
                },
                'filter' => Html::dropDownList(
                    'CitiesSearch[state_id]', 
                    $searchModel['state_id'], 
                    ArrayHelper::map(States::find()->where(['country_id' => $searchModel['country_id']])->all(),'id','name'), 
                    [
                        'prompt' => 'Select State', 
                        'id'=>'CitiesSearch-states', 
                        'class'=>'form-control',

                    ]),
                'contentOptions' =>['style' => 'width:20%'],
            ],

            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<?php
$this->registerJs("
jQuery(document).ready(function() {
    
    // select2 for states list
    $('#CitiesSearch-states').select2({
        placeholder: \"Select State\",
        allowClear: true
    });
    $('#CitiesSearch-countries').select2({
        placeholder: \"Select Country\",
    });
    $('#cities-gridview').on('pjax:end', function() {
        //$.pjax.reload({container:'#countries'});
        $('#CitiesSearch-states').select2({
            placeholder: \"Select State\",
            allowClear: true
        });

        $('#CitiesSearch-countries').select2({
        placeholder: \"Select Country\",
        });
    });

    var country_id = $('#CitiesSearch-countries').val();

    if(country_id!='')
    {
        $('#CitiesSearch-countries').val(country_id).trigger('change');
    }

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>
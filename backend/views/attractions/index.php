<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
Use common\models\Cities;
Use common\models\States;
Use common\models\DestinationTypes;
use yii\helpers\ArrayHelper;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AttractionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Attractions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attractions-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW ATTRACTION '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'attractions-gridview','timeout' => 10000, 'enablePushState' => false]); ?>       <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'attr_type_id',
                'value' => function($data)
                {
                    if(isset($data->attractionType->name) && !empty($data->attractionType->name))
                        return $data->attractionType->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'AttractionsSearch[attr_type_id]', 
                    $searchModel['attr_type_id'], 
                    ArrayHelper::map(DestinationTypes::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'type_dropdown', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'min-width:20%'],
            ],
            'name',
            [
                'attribute' => 'state_id',
                'value' => function($data){
                    return $data->state->name;
                },
                'filter' => Html::dropDownList(
                    'AttractionsSearch[state_id]', 
                    $searchModel['state_id'], 
                    ArrayHelper::map(States::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a State', 
                        'id'=>'states_dropdown', 
                        'class'=>'form-control',

                    ]),
                'contentOptions' =>['style' => 'min-width:20%'],
            ],
            [
                'attribute' => 'city_name',
                'label'=> 'City',
                'value' => function($data)
                {
                    if(isset($data->city->name) && !empty($data->city->name))
                        return $data->city->name;
                    else
                        return '';
                },
                'contentOptions' =>['style' => 'min-width:20%'],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#states_dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });


    $('#attractions-gridview').on('pjax:end', function() {

        $('#type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });

        $('#states_dropdown').select2({
            placeholder: \"Select a State\",
            allowClear: true
        });
    });

});",View::POS_END);
?>

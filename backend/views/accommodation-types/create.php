<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AccommodationTypes */

$this->title = Yii::t('app', 'Create Accommodation Types');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accommodation Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accommodation-types-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

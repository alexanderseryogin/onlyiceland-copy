<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\AddOn;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AddOn */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- <div class="add-on-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'discountable')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> -->

<style>
    .hint-block{
        color:blue;
    }
</style>

<div class="add-on-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-xs-6">

                                <?= $form->field($model, 'name') ?>

                                <?= $form->field($model, 'type')->dropdownList([
                                    AddOn::TYPE_FIXED => "Fixed Price", 
                                    AddOn::TYPE_PERCENTAGE => "Percentage", 
                                ]
                                ,[
                                    'prompt'=>'Select an Option',
                                    'id' => 'type_dropdown',
                                    'class' => 'form-control',
                                    'style' => 'max-width: 500px'
                                    
                                ]) ?>

                               
                            </div>

                            <div class="col-xs-6">

                               <?= $form->field($model, 'discountable')->dropdownList([
                                    AddOn::DISCOUNTABLE => "Yes", 
                                    AddOn::NOT_DISCOUNTABLE => "No", 
                                ]
                                ,[
                                    'prompt'=>'Select an Option',
                                    'id' => 'discountable_dropdown',
                                    'class' => 'form-control',
                                    'style' => 'max-width: 500px'
                                ]) ?>


                               <?= $form->field($model, 'commissionable')->dropdownList([
                                    1 => "Yes",
                                    0 => "No",
                                ]
                                ,[
                                    'prompt'=>'Select an Option',
                                    'id' => 'commissionable_dropdown',
                                    'class' => 'form-control',
                                    'style' => 'max-width: 500px'
                                ]) ?>
                            </div>
                                
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                            <?= $form->field($model, 'description',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            ]
                                                        ])->textarea(['rows' => 8]) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            <a class="btn btn-default" href="<?= Url::to(['/add-on']) ?>" >Cancel</a>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#discountable_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#period_dropdown').select2({
        placeholder: \"Select a Period\",
        allowClear: true
    });

    $('#commissionable_dropdown').select2({
        placeholder: \"Select a Period\",
        allowClear: true
    });

});",View::POS_END);
?>


<?php

use yii\db\Migration;

class m161031_104121_alter_upsellitems_add_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `upsell_items` ADD COLUMN `applies_to` INT(11) NULL AFTER `amount`, ADD COLUMN `period` INT(11) NULL AFTER `applies_to`; 
        ");
    }

    public function down()
    {
        echo "m161031_104121_alter_upsellitems_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
use common\models\CustomReportColumns;
use common\models\BookingDateFinances;
use common\models\BookingsItems;
use common\models\CustomReports;    
$columns = CustomReportColumns::getColumns();
$alias = CustomReportColumns::getAlias();

$row_count = 0;
function bubble_Sort($my_array )
{
    do
    {
        $swapped = false;
        for( $i = 0, $c = count( $my_array ) - 1; $i < $c; $i++ )
        {
            if( $my_array[$i]['date'] > $my_array[$i + 1]['date'] )
            {
                list( $my_array[$i + 1], $my_array[$i] ) =
                        array( $my_array[$i], $my_array[$i + 1] );
                $swapped = true;
            }
        }
    }
    while( $swapped );
return $my_array;
}
?>
<h3><b><?=$title?></b> <h3>

<?php 
    if($group_by == 0)
    {
?>
        <table class="table table-striped table-bordered table2excel">
            <thead>
              <tr>
                <?php
                    foreach ($selected_columns as $column) 
                    {
                        if($column != 9)
                        {
                            echo "<th>";
                            echo $columns[$column]['label'];
                            echo "</th>";
                        }
                    }
                ?>
              </tr>
            </thead>
            <tbody>
                <?php
                    if(empty($result))
                    {
                        $check = array_search(9, $selected_columns);
                        if($check === false)
                        {
                            $colspan = count($selected_columns);
                        }
                        else
                        {
                            $colspan = count($selected_columns) - 1;
                        }
                        echo "<tr>";
                            echo "<td colspan='".$colspan."'>";
                            echo "<b>No Record Found</b>";
                            echo "</td>";
                        echo "</tr>";
                    }
                    else
                    {
                        $notes_exist = false;
                        $check = array_search(9, $selected_columns);
                        if($check === false)
                        {
                            $colspan = count($selected_columns);
                        }
                        else
                        {
                            $colspan = count($selected_columns) - 1;
                            $notes_exist = true;
                        }
                        $sum_row_data = array();
                        $sum_debit = 0;
                        $sum_payment = 0;
                        foreach ($result as $data) 
                        {
                            if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns) || in_array(17, $selected_columns) || in_array(18, $selected_columns) || in_array(36, $selected_columns) || in_array(37, $selected_columns) || in_array(38, $selected_columns) || in_array(39, $selected_columns) || in_array(40, $selected_columns))
                            {
                                $class ='display:none;';
                            }
                            else
                            {
                                $class = '';
                            }

                            echo "<tr style='".$class."'><div style='".$class."'>";
                            $notes = '';
                            $booking_item_id = isset($data['id'])?$data['id']:'';

                            $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                            $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                            $booking_item = BookingsItems::findOne(['id' => $booking_item_id]);
                            
                            $sum_debit += $booking_item_charges_total;
                            $sum_payment += $booking_item_payments_total;
                            $row_data = array();
                            // echo $booking_item_id;
                            // exit();
                            foreach ($selected_columns as $column) 
                            {
                                if($column != 9)
                                {
                                    $style = $columns[$column]['style'];
                                    echo "<td style='".$style."'>";
                                    // echo "<td>";
                                    if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40)
                                    {
                                        if($columns[$column]['is_foreign_key'] == 0)
                                        {
                                            if($columns[$column]['is_amount'] == 0)
                                            {
                                                
                                                if(isset($columns[$column]['timestamp']))
                                                {
                                                    echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                        $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                    // echo $virtual_column_val;        
                                                }
                                                else
                                                {
                                                    echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                    $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                }
                                            }
                                            else
                                            {
                                                if(isset($columns[$column]['virtual_column']))
                                                {
                                                    // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                    if($column == 16)
                                                    {
                                                        echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                        // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                    }
                                                    if($column ==19)
                                                    {
                                                        echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                        // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                    }
                                                }
                                                else
                                                {
                                                    echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                    $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                    if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                    {
                                                        $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                        $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                    }
                                                    else
                                                    {
                                                        $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                        $sum_row_data[$columns[$column]['db_col']] = $val;
                                                    }

                                                }
                                                
                                            }
                                            
                                        }
                                        else
                                        {
                                            if(empty($data[$columns[$column]['db_col']]))
                                            {
                                                echo "- - - -";

                                                $row_data[$columns[$column]['db_col']] = '- - - -';
                                            }
                                            else
                                            {
                                                $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                echo $value;

                                                $row_data[$columns[$column]['db_col']] = $value;
                                            }
                                            
                                        }
                                    }
                                    echo "</td>";

                                }
                                else
                                {
                                    $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                }
                            }
                            echo "</div></tr>";
                            // echo "<pre>";
                            // print_r($row_data);
                            // exit();
                            if($booking_item_id != '')
                            {
                                $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                $charges_arr = array();
                                $payments_arr = array();

                                if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns))
                                {
                                    $charges_arr = $booking_item_charges;
                                }

                                if(in_array(17, $selected_columns) || in_array(18, $selected_columns) )
                                {
                                    $payments_arr = $booking_item_payments;
                                }

                                $merged_arr = array_merge($charges_arr,$payments_arr);
                                // echo "<pre>";
                                $merged_arr =bubble_Sort($merged_arr);
                                    $row_count = 0;
                                    $row_count2 = 0;
                                foreach ($merged_arr as $arr) 
                                {
                                    echo "<tr>";
                                    $col_count = 0;

                                    foreach ($selected_columns as $column)
                                    {
                                        $col_count++;
                                        if(array_key_exists($columns[$column]['db_col'], $arr))
                                        {
                                            $value = $arr[$columns[$column]['db_col']];
                                            if($column == 1 )
                                            {
                                                echo "<td style='".$columns[$column]['style']."'>";
                                                if($column == 1 && $row_count2 < 1)
                                                {
                                                    
                                                    echo $row_data['id'];
                                                    
                                                    $row_count2++;
                                                }
                                                echo "</td>";
                                            }
                                            else
                                            {
                                                if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16 ||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                {
                                                    if($columns[$column]['is_amount'] == 0)
                                                    { 
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo $value;
                                                        echo "</td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                        echo "</td>";
                                                    }
                                                }
                                                else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                {
                                                    if($columns[$column]['is_amount'] == 0)
                                                    { 
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo $value;
                                                        echo "</td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo Yii::$app->formatter->asDecimal($value);
                                                        echo "</td>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "<td>";
                                                    echo "</td>";
                                                }

                                                    
                                                
                                            }

                                        }
                                        else
                                        {
                                            echo "<td style='".$columns[$column]['style']."'>";
                                            if($row_count < 1 )
                                            {
                                                if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31)
                                                {
                                                    echo $row_data[$columns[$column]['db_col']];
                                                }
                                            }
                                            echo "</td>";
                                        }
                                    }
                                    $row_count++;
                                    echo "</tr>";
                                }

                                if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns) || in_array(17, $selected_columns) || in_array(18, $selected_columns) )
                                {
                                    echo "<tr>";
                                    foreach ($selected_columns as $column)
                                    {
                                        $border = '';
                                        if(count($result) >1 )
                                        {
                                            $border =  'border-bottom: solid 3px';
                                        }
                                        echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                        if($column == 16)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                        }
                                        if($column == 19)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                        }
                                        if($column == 10)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                        }
                                        if($column == 28)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                        }
                                        if($column == 29)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                        }
                                        if($column == 30)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                        }
                                        if($column == 31)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                        }
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                }
                               
                                
                            }
                             

                            if($notes_exist && $notes !='')
                            {
                                echo "<tr>";
                                    echo "<td colspan='".$colspan."'>";
                                    echo $notes;
                                    echo "</td>";
                                echo "</tr>";
                            }
                        }

                        if((in_array(10, $selected_columns) || in_array(28, $selected_columns)|| in_array(29, $selected_columns) || in_array(30, $selected_columns) || in_array(31, $selected_columns)  || in_array(16, $selected_columns) || in_array(19, $selected_columns)) && (count($result) > 1 ) )
                        {
                            
                            echo "<tr>";
                            foreach ($selected_columns as $column)
                            {
                                if($column == 16)
                                {
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_debit/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_debit;
                                    }
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else if($column == 19)
                                {
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_payment/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_payment;
                                    }
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                {
                                    // echo "here";
                                    // exit();
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_row_data[$columns[$column]['db_col']];
                                    }
                                    
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else
                                {
                                    
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    // echo $column;
                                    echo "</td>";
                                }
                               
                            }
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
<?php
    }
    else if($group_by == 1) 
    {
?>
        <table class="table table-striped table-bordered table2excel">
            <thead>
              <!-- <tr> -->
                <?php
                    /*
                    foreach ($selected_columns as $column) 
                    {
                        if($column != 9)
                        {
                            echo "<th>";
                            echo $columns[$column]['label'];
                            echo "</th>";
                        }
                    }
                    */
                ?>
              <!-- </tr> -->
            </thead>
            <tbody>
                <?php
                    if(empty($result))
                    {
                        $check = array_search(9, $selected_columns);
                        if($check === false)
                        {
                            $colspan = count($selected_columns);
                        }
                        else
                        {
                            $colspan = count($selected_columns) - 1;
                        }
                        echo "<tr>";
                            echo "<td colspan='".$colspan."'>";
                            echo "<b>No Record Found</b>";
                            echo "</td>";
                        echo "</tr>";
                    }
                    else
                    {
                        $notes_exist = false;
                        $check = array_search(9, $selected_columns);
                        if($check === false)
                        {
                            $colspan = count($selected_columns);
                        }
                        else
                        {
                            $colspan = count($selected_columns) - 1;
                            $notes_exist = true;
                        }
                        $sum_row_data = array();
                        $sum_debit = 0;
                        $sum_payment = 0;
                        $b_item_count = 0;
                        foreach ($result as $key => $data) 
                        {
                            if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns) || in_array(17, $selected_columns) || in_array(18, $selected_columns) || in_array(36, $selected_columns) || in_array(37, $selected_columns) || in_array(38, $selected_columns) || in_array(39, $selected_columns) || in_array(40, $selected_columns) )
                            {
                                $class ='display:none;';
                            }
                            else
                            {
                                $class = '';
                            }

                            if($b_item_count == 0)
                            {

                                echo "<tr>";
                                    echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                echo "<tr>";
                                echo "<tr>";
                                foreach ($selected_columns as $column) 
                                {
                                    if($column != 9)
                                    {
                                        echo "<td>";
                                        echo "<b>".$columns[$column]['label']."</b>";
                                        echo "</td>";
                                    }
                                }
                                echo "</tr>";
                                $b_item_count++;
                            }
                            else
                            {
                                if( $data['item_id'] != $result[$key-1]['item_id'])
                                {
                                    echo "<tr>";
                                        echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                    echo "<tr>";
                                    echo "<tr>";
                                    foreach ($selected_columns as $column) 
                                    {
                                        if($column != 9)
                                        {
                                            echo "<td>";
                                            echo "<b>".$columns[$column]['label']."</b>";
                                            echo "</td>";
                                        }
                                    }
                                    echo "</tr>";
                                }
                            }

                            echo "<tr style='".$class."'><div style='".$class."'>";
                            $notes = '';
                            $booking_item_id = isset($data['id'])?$data['id']:'';

                            $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                            $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                            $booking_item = BookingsItems::findOne(['id' => $booking_item_id]);
                            
                            $sum_debit += $booking_item_charges_total;
                            $sum_payment += $booking_item_payments_total;
                            $row_data = array();
                            // echo $booking_item_id;
                            // exit();
                            foreach ($selected_columns as $column) 
                            {
                                if($column != 9)
                                {
                                    $style = $columns[$column]['style'];
                                    echo "<td style='".$style."'>";
                                    // echo "<td>";
                                    if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40)
                                    {
                                        if($columns[$column]['is_foreign_key'] == 0)
                                        {
                                            if($columns[$column]['is_amount'] == 0)
                                            {
                                                
                                                if(isset($columns[$column]['timestamp']))
                                                {
                                                    echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                        $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                    // echo $virtual_column_val;        
                                                }
                                                else
                                                {
                                                    echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                    $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                }
                                            }
                                            else
                                            {
                                                if(isset($columns[$column]['virtual_column']))
                                                {
                                                    // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                    if($column == 16)
                                                    {
                                                        echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                        // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                    }
                                                    if($column ==19)
                                                    {
                                                        echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                        // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                    }
                                                }
                                                else
                                                {
                                                    echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                    $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                    if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                    {
                                                        $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                        $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                    }
                                                    else
                                                    {
                                                        $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                        $sum_row_data[$columns[$column]['db_col']] = $val;
                                                    }

                                                }
                                                
                                            }
                                            
                                        }
                                        else
                                        {
                                            if(empty($data[$columns[$column]['db_col']]))
                                            {
                                                echo "- - - -";

                                                $row_data[$columns[$column]['db_col']] = '- - - -';
                                            }
                                            else
                                            {
                                                $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                echo $value;

                                                $row_data[$columns[$column]['db_col']] = $value;
                                            }
                                            
                                        }
                                    }
                                    echo "</td>";

                                }
                                else
                                {
                                    $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                }
                            }
                            echo "</div></tr>";
                            // echo "<pre>";
                            // print_r($row_data);
                            // exit();
                            if($booking_item_id != '')
                            {
                                $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                $charges_arr = array();
                                $payments_arr = array();

                                if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns))
                                {
                                    $charges_arr = $booking_item_charges;
                                }

                                if(in_array(17, $selected_columns) || in_array(18, $selected_columns) )
                                {
                                    $payments_arr = $booking_item_payments;
                                }

                                $merged_arr = array_merge($charges_arr,$payments_arr);
                                // echo "<pre>";
                                $merged_arr =bubble_Sort($merged_arr);
                                    $row_count = 0;
                                    $row_count2 = 0;
                                foreach ($merged_arr as $arr) 
                                {
                                    echo "<tr>";
                                    $col_count = 0;

                                    foreach ($selected_columns as $column)
                                    {
                                        $col_count++;
                                        if(array_key_exists($columns[$column]['db_col'], $arr))
                                        {
                                            $value = $arr[$columns[$column]['db_col']];
                                            if($column == 1 )
                                            {
                                                echo "<td style='".$columns[$column]['style']."'>";
                                                if($column == 1 && $row_count2 < 1)
                                                {
                                                    
                                                    echo $row_data['id'];
                                                    
                                                    $row_count2++;
                                                }
                                                echo "</td>";
                                            }
                                            else
                                            {
                                                if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16 ||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                {
                                                    if($columns[$column]['is_amount'] == 0)
                                                    { 
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo $value;
                                                        echo "</td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                        echo "</td>";
                                                    }
                                                }
                                                else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                {
                                                    if($columns[$column]['is_amount'] == 0)
                                                    { 
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo $value;
                                                        echo "</td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo Yii::$app->formatter->asDecimal($value);
                                                        echo "</td>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "<td>";
                                                    echo "</td>";
                                                }

                                                    
                                                
                                            }

                                        }
                                        else
                                        {
                                            echo "<td style='".$columns[$column]['style']."'>";
                                            if($row_count < 1 )
                                            {
                                                if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31)
                                                {
                                                    echo $row_data[$columns[$column]['db_col']];
                                                }
                                            }
                                            echo "</td>";
                                        }
                                    }
                                    $row_count++;
                                    echo "</tr>";
                                }

                                if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns) || in_array(17, $selected_columns) || in_array(18, $selected_columns) )
                                {
                                    echo "<tr style='border-bottom: solid red 3px;'>";
                                    foreach ($selected_columns as $column)
                                    {
                                        $border = '';
                                        if(count($result) >1 )
                                        {
                                            if($b_item_count ==0 )
                                            {
                                                
                                            }
                                            else
                                            {
                                                if(isset($result[$key+1]))
                                                {
                                                    if($data['item_id'] != $result[$key+1]['item_id'])
                                                    {
                                                        $border =  'border-bottom: solid 3px';
                                                    }
                                                }
                                            }
                                            // $border =  'border-bottom: solid 3px';
                                        }
                                        echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                        if($column == 16)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                        }
                                        if($column == 19)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                        }
                                        if($column == 10)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                        }
                                        if($column == 28)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                        }
                                        if($column == 29)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                        }
                                        if($column == 30)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                        }
                                        if($column == 31)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                        }
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                }
                               
                                
                            }
                             

                            if($notes_exist && $notes !='')
                            {
                                echo "<tr>";
                                    echo "<td colspan='".$colspan."'>";
                                    echo $notes;
                                    echo "</td>";
                                echo "</tr>";
                            }
                        }

                        if((in_array(10, $selected_columns) || in_array(28, $selected_columns)|| in_array(29, $selected_columns) || in_array(30, $selected_columns) || in_array(31, $selected_columns)  || in_array(16, $selected_columns) || in_array(19, $selected_columns)) && (count($result) > 1 ) )
                        {
                            
                            echo "<tr>";
                            foreach ($selected_columns as $column)
                            {
                                if($column == 16)
                                {
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_debit/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_debit;
                                    }
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else if($column == 19)
                                {
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_payment/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_payment;
                                    }
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                {
                                    // echo "here";
                                    // exit();
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_row_data[$columns[$column]['db_col']];
                                    }
                                    
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else
                                {
                                    
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    // echo $column;
                                    echo "</td>";
                                }
                               
                            }
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
<?php
    }
    else if($group_by == 2) 
    {
?>
        <table class="table table-striped table-bordered table2excel">
            <thead>
              <tr>
                <?php
                    
                    foreach ($selected_columns as $column) 
                    {
                        if($column != 9)
                        {
                            echo "<th>";
                            echo $columns[$column]['label'];
                            echo "</th>";
                        }
                    }
                    
                ?>
              </tr>
            </thead>
            <tbody>
                <?php
                    if(empty($result))
                    {
                        $check = array_search(9, $selected_columns);
                        if($check === false)
                        {
                            $colspan = count($selected_columns);
                        }
                        else
                        {
                            $colspan = count($selected_columns) - 1;
                        }
                        echo "<tr>";
                            echo "<td colspan='".$colspan."'>";
                            echo "<b>No Record Found</b>";
                            echo "</td>";
                        echo "</tr>";
                    }
                    else
                    {
                        $notes_exist = false;
                        $check = array_search(9, $selected_columns);
                        if($check === false)
                        {
                            $colspan = count($selected_columns);
                        }
                        else
                        {
                            $colspan = count($selected_columns) - 1;
                            $notes_exist = true;
                        }
                        $sum_row_data = array();
                        $sum_debit = 0;
                        $sum_payment = 0;
                        $b_item_count = 0;
                        foreach ($result as $key => $data) 
                        {
                            if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns) || in_array(17, $selected_columns) || in_array(18, $selected_columns) || in_array(36, $selected_columns) || in_array(37, $selected_columns) || in_array(38, $selected_columns) || in_array(39, $selected_columns) || in_array(40, $selected_columns))
                            {
                                $class ='display:none;';
                            }
                            else
                            {
                                $class = '';
                            }

                            $border_groups = '';
                            if($b_item_count == 0)
                            {

                                // echo "<tr>";
                                //     echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                // echo "<tr>";
                                // echo "<tr>";
                                // foreach ($post['CustomReports']['columns'] as $column) 
                                // {
                                //     if($column != 9)
                                //     {
                                //         echo "<td>";
                                //         echo "<b>".$columns[$column]['label']."</b>";
                                //         echo "</td>";
                                //     }
                                // }
                                // echo "</tr>";
                                $b_item_count++;
                            }
                            else
                            {
                                if( $data['booking_group_id'] != $result[$key-1]['booking_group_id'])
                                {
                                    // echo "here";
                                    // exit();
                                    $border_groups = 'border-top:solid 3px';
                                    // echo "<tr>";
                                    //     echo "<td colspan =".$colspan."><b>".CustomReportColumns::getItemName($data['item_id'])."</b></td>";
                                    // echo "<tr>";
                                    // echo "<tr>";
                                    // foreach ($post['CustomReports']['columns'] as $column) 
                                    // {
                                    //     if($column != 9)
                                    //     {
                                    //         echo "<td>";
                                    //         echo "<b>".$columns[$column]['label']."</b>";
                                    //         echo "</td>";
                                    //     }
                                    // }
                                    // echo "</tr>";
                                }
                            }

                            echo "<tr style='".$class."' style='".$border_groups."'><div style='".$class."'>";
                            $notes = '';
                            $booking_item_id = isset($data['id'])?$data['id']:'';

                            $booking_item_charges_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->sum('final_total');

                            $booking_item_payments_total = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->sum('final_total');
                            $booking_item = BookingsItems::findOne(['id' => $booking_item_id]);
                            
                            $sum_debit += $booking_item_charges_total;
                            $sum_payment += $booking_item_payments_total;
                            $row_data = array();
                            // echo $booking_item_id;
                            // exit();
                            foreach ($selected_columns as $column) 
                            {
                                if($column != 9)
                                {
                                    $style = $columns[$column]['style'];
                                    echo "<td style='".$style."'>";
                                    // echo "<td>";
                                    if($column != 13 && $column != 14 &&$column != 15 && $column != 17 && $column != 18 && $column != 36 && $column != 37 && $column != 38 && $column != 39 && $column != 40)
                                    {
                                        if($columns[$column]['is_foreign_key'] == 0)
                                        {
                                            if($columns[$column]['is_amount'] == 0)
                                            {
                                                
                                                if(isset($columns[$column]['timestamp']))
                                                {
                                                    echo empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);

                                                        $row_data[$columns[$column]['db_col']] =  empty($data[$columns[$column]['db_col']])?'- - - -':date('Y-m-d',$data[$columns[$column]['db_col']]);
                                                    // echo $virtual_column_val;        
                                                }
                                                else
                                                {
                                                    echo empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];

                                                    $row_data[$columns[$column]['db_col']] = empty($data[$columns[$column]['db_col']])?'- - - -':$data[$columns[$column]['db_col']];
                                                }
                                            }
                                            else
                                            {
                                                if(isset($columns[$column]['virtual_column']))
                                                {
                                                    // echo empty($data[$columns[$column]['virtual_column']])?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['virtual_column']]);
                                                    if($column == 16)
                                                    {
                                                        echo empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);

                                                        // $row_data[$columns[$column]['db_col']] = empty($booking_item_charges_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                                    }
                                                    if($column ==19)
                                                    {
                                                        echo empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);

                                                        // $row_data[$columns[$column]['db_col']] = empty($booking_item_payments_total)?'- - - -':Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                                    }
                                                }
                                                else
                                                {
                                                    echo (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                    $row_data[$columns[$column]['db_col']] = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?'- - - -':Yii::$app->formatter->asDecimal($data[$columns[$column]['db_col']]);

                                                    if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                                    {
                                                        $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];

                                                        $sum_row_data[$columns[$column]['db_col']] = $sum_row_data[$columns[$column]['db_col']] + $val;
                                                    }
                                                    else
                                                    {
                                                        $val = (empty($data[$columns[$column]['db_col']]) && $data[$columns[$column]['db_col']] !=0)?0:$data[$columns[$column]['db_col']];
                                                        $sum_row_data[$columns[$column]['db_col']] = $val;
                                                    }

                                                }
                                                
                                            }
                                            
                                        }
                                        else
                                        {
                                            if(empty($data[$columns[$column]['db_col']]))
                                            {
                                                echo "- - - -";

                                                $row_data[$columns[$column]['db_col']] = '- - - -';
                                            }
                                            else
                                            {
                                                $value = CustomReportColumns::getColumnValue($column,$data[$columns[$column]['db_col']]);
                                                echo $value;

                                                $row_data[$columns[$column]['db_col']] = $value;
                                            }
                                            
                                        }
                                    }
                                    echo "</td>";

                                }
                                else
                                {
                                    $notes = empty($data[$columns[$column]['db_col']])?'':$data[$columns[$column]['db_col']];
                                }
                            }
                            echo "</div></tr>";
                            // echo "<pre>";
                            // print_r($row_data);
                            // exit();
                            if($booking_item_id != '')
                            {
                                $booking_item_charges = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id, 'type' => 1 ])->asArray()->orderBy('date')->all();

                                $booking_item_payments = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id,'type' => 2 ])->asArray()->orderBy('date')->all();

                                $charges_arr = array();
                                $payments_arr = array();

                                if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns))
                                {
                                    $charges_arr = $booking_item_charges;
                                }

                                if(in_array(17, $selected_columns) || in_array(18, $selected_columns) )
                                {
                                    $payments_arr = $booking_item_payments;
                                }

                                $merged_arr = array_merge($charges_arr,$payments_arr);
                                // echo "<pre>";
                                $merged_arr =bubble_Sort($merged_arr);
                                    $row_count = 0;
                                    $row_count2 = 0;
                                foreach ($merged_arr as $arr) 
                                {
                                    echo "<tr>";
                                    $col_count = 0;

                                    foreach ($selected_columns as $column)
                                    {
                                        $col_count++;
                                        if(array_key_exists($columns[$column]['db_col'], $arr))
                                        {
                                            $value = $arr[$columns[$column]['db_col']];
                                            if($column == 1 )
                                            {
                                                echo "<td style='".$columns[$column]['style']."'>";
                                                if($column == 1 && $row_count2 < 1)
                                                {
                                                    
                                                    echo $row_data['id'];
                                                    
                                                    $row_count2++;
                                                }
                                                echo "</td>";
                                            }
                                            else
                                            {
                                                if($arr['type'] == 1 && ($column == 13 || $column == 14 || $column == 15 ||$column == 16 ||$column == 36 ||$column == 37 ||$column == 38 ||$column == 39 ||$column == 40) )
                                                {
                                                    if($columns[$column]['is_amount'] == 0)
                                                    { 
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo $value;
                                                        echo "</td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo empty($value)?0:Yii::$app->formatter->asDecimal($value);
                                                        echo "</td>";
                                                    }
                                                }
                                                else if($arr['type'] == 2 && ($column == 17 || $column == 18 || $column == 19) )
                                                {
                                                    if($columns[$column]['is_amount'] == 0)
                                                    { 
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo $value;
                                                        echo "</td>";
                                                    }
                                                    else
                                                    {
                                                        echo "<td style='".$columns[$column]['style']."'>";
                                                            echo Yii::$app->formatter->asDecimal($value);
                                                        echo "</td>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "<td>";
                                                    echo "</td>";
                                                }

                                                    
                                                
                                            }

                                        }
                                        else
                                        {
                                            echo "<td style='".$columns[$column]['style']."'>";
                                            if($row_count < 1 )
                                            {
                                                if($column != 10 && $column != 28 && $column != 29 && $column != 30 && $column != 31)
                                                {
                                                    echo $row_data[$columns[$column]['db_col']];
                                                }
                                            }
                                            echo "</td>";
                                        }
                                    }
                                    $row_count++;
                                    echo "</tr>";
                                }

                                if(in_array(13, $selected_columns) || in_array(14, $selected_columns)|| in_array(15, $selected_columns) || in_array(17, $selected_columns) || in_array(18, $selected_columns) )
                                {
                                    echo "<tr style='border-bottom: solid red 3px;'>";
                                    foreach ($selected_columns as $column)
                                    {
                                        $border = '';
                                        if(count($result) >1 )
                                        {
                                            if($b_item_count ==0 )
                                            {
                                                
                                            }
                                            else
                                            {
                                                if(isset($result[$key+1]))
                                                {
                                                    if($data['booking_group_id'] != $result[$key+1]['booking_group_id'])
                                                    {
                                                        $border =  'border-bottom: solid 3px';
                                                    }
                                                }
                                            }
                                            // $border =  'border-bottom: solid 3px';
                                        }
                                        echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                        if($column == 16)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item_charges_total);
                                        }
                                        if($column == 19)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item_payments_total);
                                        }
                                        if($column == 10)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->balance);
                                        }
                                        if($column == 28)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->paid_amount);
                                        }
                                        if($column == 29)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->gross_total);
                                        }
                                        if($column == 30)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->averaged_total);
                                        }
                                        if($column == 31)
                                        {
                                            echo Yii::$app->formatter->asDecimal($booking_item->net_total);
                                        }
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                }
                               
                                
                            }
                             

                            if($notes_exist && $notes !='')
                            {
                                echo "<tr>";
                                    echo "<td colspan='".$colspan."'>";
                                    echo $notes;
                                    echo "</td>";
                                echo "</tr>";
                            }
                        }

                        if((in_array(10, $selected_columns) || in_array(28, $selected_columns)|| in_array(29, $selected_columns) || in_array(30, $selected_columns) || in_array(31, $selected_columns)  || in_array(16, $selected_columns) || in_array(19, $selected_columns)) && (count($result) > 1 ) )
                        {
                            
                            echo "<tr>";
                            foreach ($selected_columns as $column)
                            {
                                if($column == 16)
                                {
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_debit/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_debit;
                                    }
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else if($column == 19)
                                {
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_payment/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_payment;
                                    }
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else if(array_key_exists($columns[$column]['db_col'], $sum_row_data))
                                {
                                    // echo "here";
                                    // exit();
                                    if($sum_average == 2)
                                    {
                                        $val = round($sum_row_data[$columns[$column]['db_col']]/count($result));
                                    }
                                    else
                                    {
                                        $val = $sum_row_data[$columns[$column]['db_col']];
                                    }
                                    
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    echo Yii::$app->formatter->asDecimal($val);
                                    echo "</td>";
                                }
                                else
                                {
                                    
                                    echo "<td style='".$columns[$column]['style'].' '.$border."'>";
                                    // echo $column;
                                    echo "</td>";
                                }
                               
                            }
                            echo "</tr>";
                        }
                    }
                ?>
            </tbody>
        </table>

<?php
    }
?>

<p style="font-size: 12px">Total Records : <?=count($result)?></p>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
<?php

use yii\db\Migration;

class m170117_114402_alter_bookable_change_max_guests_type extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookable_items` CHANGE `max_guests` `max_guests` VARCHAR(10) NULL; ');
    }

    public function down()
    {
        echo "m170117_114402_alter_bookable_change_max_guests_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

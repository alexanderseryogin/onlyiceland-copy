<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_items".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $item_type_id
 * @property integer $item_quantity
 * @property string $how_booked
 * @property integer $shortest_period
 * @property integer $longest_period
 * @property string $min_price
 * @property string $max_price
 * @property string $no_of_guests
 * @property integer $max_adults
 * @property integer $max_children
 * @property integer $max_extra_beds
 * @property integer $max_baby_beds
 * @property integer $min_age_participation
 * @property integer $max_age_participation
 * @property integer $max_free_age
 * @property string $background_color
 * @property string $text_color
 * @property string $include_in_groups
 *
 * @property BookableItemTypes $itemType
 * @property Providers $provider
 */
class BookableItems extends \yii\db\ActiveRecord
{
    public $bookable_periods;
    public $age_participation;
    public $image;
    
    public $amenities;
    public $amenities_banners;

    public $server_min_price;
    public $server_max_price;

    public $transparent_background;
    public $isAccommodation=2;

    public $beds_combinations = [];
    public $item_names = [];

    public $housekeeping_status_array = [];

    public static $bathroom = 
    [
        0 => 'Private',
        1 => 'Shared',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transparent_background'], 'boolean'],
            /*[['provider_id', 'item_type_id','sleeping_arrangement_id', 'item_quantity', 'shortest_period', 'longest_period','max_adults', 'max_children', 'max_extra_beds', 'max_baby_beds', 'min_age_participation', 'max_age_participation', 'max_free_age','max_extra_baby_beds','server_min_price','server_max_price'], 'integer'],*/
            /*[['provider_id', 'item_type_id', 'item_quantity','how_booked', 'shortest_period', 'longest_period','max_extra_beds', 'max_baby_beds', 'max_free_age','background_color', 'text_color','include_in_groups','min_price', 'max_price','description'], 'required'],
            [['how_booked', 'min_price', 'max_price', 'no_of_guests', 'include_in_groups','general_booking_cancellation'], 'string', 'max' => 256],
            [['background_color', 'text_color'], 'string', 'max' => 11],*/

            [['provider_id', 'item_type_id','sleeping_arrangement_id', 'item_quantity', 'shortest_period', 'longest_period','max_adults', 'max_children', 'max_extra_beds', 'max_baby_beds', 'min_age_participation', 'max_age_participation', 'max_free_age','max_extra_baby_beds','bathroom','default_bed_combinations_id'], 'integer'],

            [['provider_id', 'item_type_id', 'item_quantity','how_booked', 'shortest_period', 'longest_period','max_extra_beds', 'max_baby_beds', 'max_free_age','background_color', 'text_color','include_in_groups','description'], 'required'],

            [['age_range_children', 'min_age_adults','max_guests'], 'string', 'max' => 10],
            [['bookable_item_notes','bookable_item_default_notes'],'string'],
            [['bookable_periods','featured_image','description','age_participation'], 'string'],

            [['how_booked', 'no_of_guests', 'include_in_groups','general_booking_cancellation'], 'string', 'max' => 256],
            [['background_color', 'text_color'], 'string', 'max' => 11],

            [['item_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItemTypes::className(), 'targetAttribute' => ['item_type_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png','jpg']],
            [['sleeping_arrangement_id'], 'exist', 'skipOnError' => true, 'targetClass' => SleepingArrangements::className(), 'targetAttribute' => ['sleeping_arrangement_id' => 'id']],
            [['booking_confirmation_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['booking_confirmation_type_id' => 'id']], 
            [['booking_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['booking_status_id' => 'id']], 
            [['booking_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['booking_type_id' => 'id']], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination'),
            'transparent_background' => Yii::t('app', 'Transparent Background'),
            'sleeping_arrangement_id' => Yii::t('app', 'Sleeping Arrangement'),
            'item_type_id' => Yii::t('app', 'Item Type'),
            'item_quantity' => Yii::t('app', 'Item Quantity'),
            'how_booked' => Yii::t('app', 'How Booked'),
            'shortest_period' => Yii::t('app', 'Shortest Period'),
            'longest_period' => Yii::t('app', 'Longest Period'),
            'min_price' => Yii::t('app', 'Minimum Price'),
            'max_price' => Yii::t('app', 'Maximum Price'),
            'no_of_guests' => Yii::t('app', 'No Of Guests'),
            'max_adults' => Yii::t('app', 'Max Adults'),
            'max_children' => Yii::t('app', 'Max Children'),
            'max_extra_beds' => Yii::t('app', 'Max Extra Beds'),
            'max_baby_beds' => Yii::t('app', 'Max Baby Beds'),
            'min_age_participation' => Yii::t('app', 'Min Age Participation'),
            'max_age_participation' => Yii::t('app', 'Max Age Participation'),
            'max_free_age' => Yii::t('app', 'Max Free Age'),
            'background_color' => Yii::t('app', 'Background Color'),
            'text_color' => Yii::t('app', 'Text Color'),
            'include_in_groups' => Yii::t('app', 'Include In Reports'),
            'image' => Yii::t('app', 'Upload Featured Image'),
            'description' => Yii::t('app', 'Item Description'),
            'max_extra_baby_beds' => Yii::t('app', 'Maximum Possible Number of Extra Beds AND/OR Baby Beds/Cribs'),
            'booking_confirmation_type_id' => Yii::t('app', 'Default Booking Confirmation Type'),
            'booking_status_id' => Yii::t('app', 'Default Booking Status'),
            'booking_type_id' => Yii::t('app', 'Default Booking Flag'),
            'bathroom' => Yii::t('app', 'Bathroom'),
            'bookable_item_notes' => Yii::t('app', 'Long Notes'),
            'bookable_item_default_notes' => Yii::t('app', 'Default Notes'),
        ];
    }

    public function getAccomodationId()
    {
        $acc = DestinationTypes::findOne(['name' => 'Accommodation']);
        return $acc->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemType()
    {
        return $this->hasOne(BookableItemTypes::className(), ['id' => 'item_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    public function getImage()
    {
        return $this->hasOne(BookableItemsImages::className(), ['bookable_items_id' => 'id']);
    }

    public function setDefaultValues()
    {
        $this->max_guests = 0;
        $this->max_adults = 1;
        $this->max_children = 0;

        $this->max_extra_baby_beds = 0;
        $this->max_extra_beds = 0;
        $this->max_baby_beds = 0;
        
        $this->max_free_age = 0;
        $this->bathroom = 1;
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getBookableAmenities() 
   { 
       return $this->hasMany(BookableAmenities::className(), ['item_id' => 'id']); 
   } 

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getOffers()
   {
       return $this->hasMany(Offers::className(), ['item_id' => 'id']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getRates()
   {
       return $this->hasMany(Rates::className(), ['unit_id' => 'id']);
   }

   public function getSleepingArrangement()
   {
       return $this->hasOne(SleepingArrangements::className(), ['id' => 'sleeping_arrangement_id']);
   }

   public function getBookingConfirmationType()
   {
       return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'booking_confirmation_type_id']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getBookingStatus()
   {
       return $this->hasOne(BookingStatuses::className(), ['id' => 'booking_status_id']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getBookingType()
   {
       return $this->hasOne(BookingTypes::className(), ['id' => 'booking_type_id']);
   }

    public function getBookableItemsNames()
    {
        return $this->hasMany(BookableItemsNames::className(), ['bookable_item_id' => 'id']);
    }
    public function getBookableItemsHousekeepingStatus()
    {
        return $this->hasMany(BookableItemsHousekeepingStatus::className(), ['bookable_item_id' => 'id']);
    }
}

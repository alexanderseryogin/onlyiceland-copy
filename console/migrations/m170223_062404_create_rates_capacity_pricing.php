<?php

use yii\db\Migration;

class m170223_062404_create_rates_capacity_pricing extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `rates_capacity_pricing`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `rates_id` INT(11), `person_no` INT(11), `price` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_rates_capacity_pricing_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170223_062404_create_rates_capacity_pricing cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

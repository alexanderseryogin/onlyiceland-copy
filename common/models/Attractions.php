<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attractions".
 *
 * @property integer $id
 * @property string $name
 * @property integer $state_id
 * @property integer $city_id
 * @property string $description
 * @property string $address
 * @property string $phone
 * @property string $price
 *
 * @property AttractionTypes $at
 * @property Cities $city
 * @property States $state
 * @property AttractionsImages[] $attractionsImages
 */
class Attractions extends \yii\db\ActiveRecord
{
    public $server_phone='';
    public $tags='';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attractions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attr_type_id'], 'integer'],
            [['name', 'description', 'address','attr_type_id', 'state_id','city_id'], 'required'],
            [['description', 'address'], 'string'],
            [['name', 'phone', 'price','server_phone'], 'string', 'max' => 256],
            [['attr_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTypes::className(), 'targetAttribute' => ['attr_type_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attr_type_id' => Yii::t('app', 'Attraction Types'),
            'name' => Yii::t('app', 'Name'),
            'state_id' => Yii::t('app', 'State'),
            'city_id' => Yii::t('app', 'City'),
            'description' => Yii::t('app', 'Description'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'price' => Yii::t('app', 'Price'),
            'tags' => Yii::t('app', 'Attraction Tags'),
        ];
    }

    public function getAttractionAndTags()
    {
        return $this->hasMany(AttractionAndTags::className(), ['attr_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAttractionType()
    {
       return $this->hasOne(DestinationTypes::className(), ['id' => 'attr_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractionsImages()
    {
        return $this->hasMany(AttractionsImages::className(), ['attraction_id' => 'id']);
    }
}

<?php

use yii\db\Migration;

class m170718_062838_alter_table_rates_add_fields_to_and_from extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `rates`   
                          ADD COLUMN `to` DATE NULL AFTER `lodging_tax_per_night`,
                          ADD COLUMN `from` DATE NULL AFTER `to`;
                        ');
    }

    public function down()
    {
        echo "m170718_062838_alter_table_rates_add_fields_to_and_from cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

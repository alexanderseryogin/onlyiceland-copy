<?php

use yii\db\Migration;

class m161021_125906_drop_tables extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `properties` DROP COLUMN `region_id`, DROP COLUMN `property_type_id`, DROP INDEX `FK_properties_region_id`, DROP INDEX `FK_properties_property_type_id`, DROP FOREIGN KEY `FK_properties_property_type_id`, DROP FOREIGN KEY `FK_properties_region_id`; 

            DROP TABLE `property_types`; 

            ALTER TABLE `providers` DROP COLUMN `region_id`, DROP INDEX `FK_provider_region_id`, DROP FOREIGN KEY `FK_provider_region_id`; 

            ALTER TABLE `places_of_interest` DROP COLUMN `region_id`, DROP INDEX `FK_places_of_interest_region_id`, DROP FOREIGN KEY `FK_places_of_interest_region_id`; 

            DROP TABLE `regions`;
            ");
    }

    public function down()
    {
        echo "m161021_125906_drop_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

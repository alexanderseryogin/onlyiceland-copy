<?php

use yii\db\Migration;

class m170725_070500_alter_table_bookings_group_add_fields_balance extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group`   
                          ADD COLUMN `balance` DOUBLE NULL AFTER `estimated_arrival_time_id`;
                        ');
    }

    public function down()
    {
        echo "m170725_070500_alter_table_bookings_group_add_fields_balance cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

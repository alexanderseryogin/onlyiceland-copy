<?php

use yii\db\Migration;

class m180223_053249_create_table_custom_report_destinations extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `custom_report_destinations`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `destination_id` INT(11),
                          `custom_report_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`custom_report_id`) REFERENCES `custom_reports`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180223_053249_create_table_custom_report_destinations cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180223_053249_create_table_custom_report_destinations cannot be reverted.\n";

        return false;
    }
    */
}

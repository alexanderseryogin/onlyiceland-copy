<?php

use yii\db\Migration;

class m170531_074002_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("
          ALTER TABLE `bookings_items`   
          ADD COLUMN `flag_id` INT(11) NULL AFTER `departure_date`,
          ADD COLUMN `status_id` INT(11) NULL AFTER `flag_id`,
          CHANGE `rate_conditions` `rate_conditions` TINYINT(1) NULL  AFTER `status_id`,
          ADD CONSTRAINT `bookings_items_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          ADD CONSTRAINT `bookings_items_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
            ");
    }

    public function down()
    {
        echo "m170531_074002_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

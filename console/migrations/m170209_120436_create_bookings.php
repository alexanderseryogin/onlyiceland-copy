<?php

use yii\db\Migration;

class m170209_120436_create_bookings extends Migration
{
    public function up()
    {
        $this->execute("
                CREATE TABLE `bookings` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `user_id` int(11) DEFAULT NULL COMMENT 'fk of users',
              `provider_id` int(11) DEFAULT NULL COMMENT 'fk of destinations',
              `item_id` int(11) DEFAULT NULL COMMENT 'fk of bookable items',
              `arrival_date` date DEFAULT NULL,
              `departure_date` date DEFAULT NULL,
              `travel_partner_id` int(11) DEFAULT NULL COMMENT 'fk of travel partner',
              `price` int(11) DEFAULT NULL,
              `discount_id` int(11) DEFAULT NULL COMMENT 'fk of discount',
              `offers_id` int(11) DEFAULT NULL COMMENT 'fk of offers',
              `status_id` int(11) DEFAULT NULL,
              `flag_id` int(11) DEFAULT NULL,
              `cancellation_id` int(11) DEFAULT NULL,
              `confirmation_id` int(11) DEFAULT NULL,
              `created_at` int(11) DEFAULT NULL,
              `updated_at` int(11) DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `FK_bookings_user_id` (`user_id`),
              KEY `FK_bookings_provider_id` (`provider_id`),
              KEY `FK_bookings_item_id` (`item_id`),
              KEY `FK_bookings_travel_partner_id` (`travel_partner_id`),
              KEY `FK_bookings_discount_id` (`discount_id`),
              KEY `FK_bookings_offers_id` (`offers_id`),
              CONSTRAINT `FK_bookings_discount_id` FOREIGN KEY (`discount_id`) REFERENCES `discount_code` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
              CONSTRAINT `FK_bookings_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
              CONSTRAINT `FK_bookings_offers_id` FOREIGN KEY (`offers_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
              CONSTRAINT `FK_bookings_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
              CONSTRAINT `FK_bookings_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `travel_partner` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
              CONSTRAINT `FK_bookings_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
            ");
    }

    public function down()
    {
        echo "m170209_120436_create_bookings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

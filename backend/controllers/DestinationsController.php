<?php

namespace backend\controllers;

use Yii;
use common\models\Destinations;
use common\models\DestinationsSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\helpers\ArrayHelper;
use common\models\Cities;
use common\models\States;
use common\models\Countries;
use common\models\BookingPolicies;
use common\models\DestinationsFinancial;
use common\models\BookingRules;
use common\models\UploadFile;
use common\models\User;
use common\models\UserProfile;
use common\models\PropertyPaymentMethods;
use common\models\PaymentMethodCheckin;
use common\models\PaymentMethodGuarantee;
use common\models\DestinationsStaff;
use common\models\SpecialRequests;
use common\models\DestinationsSpecialRequests;
use common\models\BookableItems;
use common\models\DestinationsImages;
use common\models\DestinationsTags;
use common\models\Tags;
use common\models\UpsellItems;
use yii\filters\AccessControl;
use common\models\DestinationsShopping;
use common\models\DestinationsSight;
use common\models\DestinationsVehicleRental;
use common\models\DestinationsOpenHoursDays;
use common\models\DestinationsOpenHoursExceptions;
use common\models\DestinationsMealPlan;
use common\models\DestinationsOpenAllYear;
use common\models\DestinationsAmenities;
use common\models\Amenities;
use common\models\DestinationTypes;
use common\models\DestinationUpsellItems;
use common\models\AccommodationTypes;
use common\models\DestinationTravelPartners;
use common\models\DestinationAddOns;
/**
 * DestinationsController implements the CRUD actions for Destinations model.
 */
class DestinationsController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [ 
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'update-image' => ['POST'],
                    'upload-images' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'update-image' || $action->id == 'update-exception-record' || $action->id == 'arrange-data' || $action->id == 'get-description' || $action->id == 'set-description' || $action->id == 'get-destination-record' || $action->id == 'upload-images' || $action->id == 'delete-image' || $action->id == 'delete-exception' || $action->id == 'delete-all') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionUpdateExceptionRecord()
    {
        $model = DestinationsOpenHoursExceptions::findOne(['id' => $_POST['exception_id']]);

        if(strtotime($_POST['opening']) > strtotime($_POST['closing']))
        {
            return 'error';
        }

        $model->opening = $_POST['opening'];
        $model->closing = $_POST['closing'];

        $model->save();
    }

    public function actionArrangeData()
    {
        if($_POST['action']=='up')
        {
            $model = DestinationsImages::findOne(['id'=>$_POST['row_id']]);
            $previous_model = DestinationsImages::find()
                                                    ->where(['provider_id' => $model->provider_id])
                                                    ->andWhere(['<','display_order',$model->display_order])
                                                    ->orderBy('display_order DESC')
                                                    ->one();

            $temp = $model->display_order;
            $model->display_order = $previous_model->display_order;
            $previous_model->display_order = $temp;

            $model->save();
            $previous_model->save();
        }
        else if($_POST['action']=='down')
        {
            $model = DestinationsImages::findOne(['id'=>$_POST['row_id']]);
            $next_model = DestinationsImages::find()
                                                    ->where(['provider_id' => $model->provider_id])
                                                    ->andWhere(['>','display_order',$model->display_order])
                                                    ->orderBy('display_order ASC')
                                                    ->one();

            $temp = $model->display_order;
            $model->display_order = $next_model->display_order;
            $next_model->display_order = $temp;

            $model->save();
            $next_model->save();
        }
    }

    public function actionGetDescription()
    {
        $model = DestinationsImages::findOne(['id'=>$_POST['image_id']]);
        return $model->description;
    }

    public function actionSetDescription()
    {
        $model = DestinationsImages::findOne(['id'=>$_POST['image_id']]);
        $model->description = $_POST['description'];

        $session = Yii::$app->session;
        $srcPath = Yii::getAlias('@app'). '/../uploads/Destinations/temp/images/';
        $destPath = Yii::getAlias('@app').'/../uploads/Destinations/'.$model->provider_id.'/images/';

        if(isset($_SESSION['image']) && !empty($_SESSION['image']))
        {
            if(file_exists($srcPath.$_SESSION['image']))
            {
                if(copy($srcPath.$_SESSION['image'], $destPath.$_SESSION['image']))
                {
                    if(!empty($model->image))
                    {
                        $Path = Yii::getAlias('@app').'/../uploads/Destinations/'.$model->provider_id.'/images/'.$model->image;
                        if(file_exists($Path))
                            unlink($Path);

                        $Path = Yii::getAlias('@app').'/../uploads/Destinations/'.$model->provider_id.'/images/thumb_'.$model->image;
                        if(file_exists($Path))
                            unlink($Path);
                    }
                    $model->image = $_SESSION['image'];

                    // create thumbnail of image

                    Image::thumbnail($destPath.$_SESSION['image'], 90,90)
                    ->save($destPath.'thumb_'.$_SESSION['image'], ['quality' => 80]);

                    $this->removeDirectory($srcPath);
                    unset($_SESSION['image']);
                }
            }       
        }

        $model->save();
        return ;
    }

    public function actionUpdateImage()
    {
        $srcPath = Yii::getAlias('@app'). '/../uploads/Destinations/temp/images/';
        if(is_dir($srcPath))
        {
            $this->removeDirectory($srcPath);
        }   

        $uploadFile = new UploadFile();
        $session = Yii::$app->session;
        $_SESSION['image'] = '';
        $error='';

        if(isset($_FILES['file'])) 
        {
            $uploadFile->FileToUpload = $_FILES;

            if($uploadFile->uploadImage('Destinations','temp','images'))
            {   
                $_SESSION['image']= $uploadFile->FileName;
            }
            else
            {
                $error = 'Invalid Image';
            }
        }

        echo json_encode(array(
            'name'  => $_SESSION['image'],
            'error' => $error,
        ));
    }

    public function actionGetDestinationRecord()
    {
        $user = User::findOne(['id' => $_POST['id']]);
        $profile = UserProfile::findOne(['user_id'=>$user->id]);

        $provider_record_arr['email'] = $user->email;
        $provider_record_arr['kennitala'] = $profile->business_id_number;
        $provider_record_arr['business_name'] = $profile->company_name;
        $provider_record_arr['phone'] = $profile->telephone_number;
        $provider_record_arr['address'] = $profile->address;
        $provider_record_arr['postal_code'] = $profile->postal_code;
        $provider_record_arr['country_id'] = $profile->country_id;
        $provider_record_arr['state_id'] = $profile->state_id;
        $provider_record_arr['city_id'] = $profile->city_id;
        
        return json_encode($provider_record_arr, JSON_PRETTY_PRINT);
    }

    /**
     * Lists all Providers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DestinationsSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['DestinationsSearch']) && !$this->isArrayEmpty($params['DestinationsSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Destinations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUploadImages()
    {
        $uploadFile = new UploadFile();

        $session = Yii::$app->session;

        $uploadFile->FileToUpload = $_FILES;

        if($uploadFile->uploadImage('Destinations','temp','images'))
        {
            if(!empty($_SESSION['images']))
            {
                $count = count($_SESSION['images']);
            }
            else
            {
                $count=0;
            }
            
            $_SESSION['images'][$count] = $uploadFile->FileName;
        }
    }

    /**
     * Creates a new Destinations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /*echo '<pre>';
        print_r($_POST);
        exit;*/
        $tab_href = '';

        $provider_model = new Destinations();
        $booking_policies_model = new BookingPolicies();
        $provider_financial_model = new DestinationsFinancial();
        $booking_rules_model = new BookingRules();
        $destination_hours_exceptions_model = new DestinationsOpenHoursExceptions();
        $destination_meal_plan = new DestinationsMealPlan();
        $destination_open_all_year = new DestinationsOpenAllYear();
        $accommodation_model = AccommodationTypes::findOne(['name' => 'Hotel']);

        $booking_policies_model->booking_flag_updated = 0;

        $allIds = Amenities::find()->select('id')->column();
        $ids = DestinationsAmenities::find()->select('amenities_id')->where(['provider_id' => $provider_model->id])->column();
        $ids = array_merge($ids, array_diff($allIds, $ids));
        $amenities = Amenities::find()
            ->orderBy(new \yii\db\Expression('FIELD (id, '.implode(',', $ids).')'))
            ->all();

        $provider_model->offers_accommodation = 1;
        $provider_model->active = 1;
        $provider_model->use_housekeeping = 1;
        $currentDate = date('d/m/Y');

        $openingTime = '00:00';
        $closingTime = '00:00';
        $currentTime = '15:00';

        $from_second = '18:00';
        $to_second = '22:00';

        $open_hours = [
            'form' => 'create',
            'hours' => [
                'monday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'tuesday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'wednesday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'thursday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'friday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'saturday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'sunday' => [
                    'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ]
            ],
            'exceptions' => [
                'date_from' => '',
                'date_to' => '',
                'opening' => [
                    0 => $openingTime,
                ],
                'closing' => [
                    0 => $closingTime,
                ],
                'state' => 1,
                'hours_24' => 0,
            ]
        ];

        $meal_options = [

            'pfo_breakfast' => 4,
            'pfo_lunch' => 4,
            'pfo_dinner' => 4,
            'sfo_morning_snack' => 4,
            'sfo_afternoon_snack' => 4,
            'sfo_evening_snack' => 4,
            'abo_tea' => 4,
            'abo_fountain' => 4,
            'abo_beer' => 4,
            'abo_liquor' => 4,
            'abo_minibar' => 4
        ];

        // Payment methods
        $methods = PropertyPaymentMethods::find()->all();
        foreach ($methods as $key => $value) 
        {
            BookingPolicies::$methodsGuarantee[$value['id']] = $value['name'];
            BookingPolicies::$methodsCheckin[$value['id']] = $value['name'];
        }

        // Special Request 
        $requests = SpecialRequests::find()->orderBy('label')->all();
        foreach ($requests as $key => $value) 
        {
            Destinations::$all_special_requests[$value['id']] = $value['label'];
        }
        $travel_partners_arr = array();
        $add_ons_arr =array();
        if($provider_model->load(Yii::$app->request->post())) 
        {
            // echo "<pre>";
            // print_r($_POST);
            // exit();
            $flag = 0;
            $accommodation = 0;
            $post = Yii::$app->request->post();

            // *************** Validate Provider Model *************** //

            $provider_model->phone = $provider_model->server_phone;
            $provider_model->fax = $provider_model->server_fax;

            $tab_href = $_POST['Destinations']['tab_href'];
            $provider_model->providers_staff = $_POST['Destinations']['providers_staff'];
            $provider_model->providers_tags = $_POST['Destinations']['providers_tags'];
            $provider_model->suitable_tags = $_POST['Destinations']['suitable_tags'];
            $provider_model->difficulty_tags = $_POST['Destinations']['difficulty_tags'];
            $provider_model->special_requests = $_POST['Destinations']['special_requests'];
            $provider_model->upsell_items = $_POST['Destinations']['upsell_items'];

            $provider_model->amenities = isset($_POST['Destinations']['amenities_radio']) && !empty($_POST['Destinations']['amenities_radio'])? $_POST['Destinations']['amenities_radio']: NULL;

            $provider_model->amenities_banners = isset($_POST['Destinations']['amenities_checkbox']) && !empty($_POST['Destinations']['amenities_checkbox'])? $_POST['Destinations']['amenities_checkbox']: NULL;

            if($provider_model->transparent_background)
                $provider_model->background_color = 'transparent';

            if($provider_model->provider_type_id != $provider_model->getAccomodation())
            {
                $provider_model->accommodation_id = '';
                if(isset($_POST['Destinations']['offers_accommodation']))
                {
                    $provider_model->offers_accommodation = 1;
                }
                else
                {
                    $provider_model->offers_accommodation = 0;
                }
            }
            else
            {
                $accommodation = 1;
                $provider_model->offers_accommodation = 1;

                $booking_policies_model->load(Yii::$app->request->post());
                $booking_rules_model->load(Yii::$app->request->post());
                $provider_financial_model->load(Yii::$app->request->post());

                $provider_financial_model->lodging_tax_per_night = $this->removeLeadingZero($provider_financial_model->server_lodging_tax_per_night);
                $provider_financial_model->vat_rate = $provider_financial_model->server_vat_rate;

                // *************** Validate Booking Policies Model *************** //

                $booking_policies_model->booking_flag_updated = isset($_POST['BookingPolicies']['booking_flag_updated'])?1:0;

                $in_from = strtotime($booking_policies_model->checkin_time_from);
                $in_to = strtotime($booking_policies_model->checkin_time_to);

                $out_from =  strtotime($booking_policies_model->checkout_time_from);
                $out_to =  strtotime($booking_policies_model->checkout_time_to);

                if($in_from >= $in_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Check-in From Time must be earlier than Check-in To Time.');
                }

                if($out_from >= $out_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Checkout From Time must be earlier than Checkout To Time.');
                }

                if($in_from >= $in_to && $out_from >= $out_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Check-in From Time must be earlier than Check-in To Time. Checkout From Time must be earlier than Checkout To Time');
                }

                // **************** Load Meal Plan

                if(isset($post['DestinationsMealPlan']))
                {
                    foreach ($post['DestinationsMealPlan'] as $key => $value) 
                    {
                        $meal_options[$key] = $value;
                    }
                }
            }

            // **************** Load and validate Days Model

            $open_hours_error = [];
            $open_hours_error_second = [];

            $post['DestinationsOpenHoursDays'] = !isset($post['DestinationsOpenHoursDays'])?[]:$post['DestinationsOpenHoursDays'];

            if(isset($post['DestinationsOpenHoursDays']))
            {
                $open_hours_data = $post['DestinationsOpenHoursDays'];

                foreach ($open_hours['hours'] as $oKey => $oValue) // oKey = day name
                {
                    if(!isset($open_hours_data[$oKey]))
                    {
                        $open_hours['hours'][$oKey]['opening'][0] = NULL;
                        $open_hours['hours'][$oKey]['closing'][0] = NULL;
                        $open_hours['hours'][$oKey]['state'] = 0;
                        $open_hours['hours'][$oKey]['hours_24'] = 0;
                    }
                    else
                    {
                        if(isset($open_hours_data[$oKey]['hours_24']))
                        {
                            $open_hours['hours'][$oKey]['opening'][0] = '00:00';
                            $open_hours['hours'][$oKey]['closing'][0] = '00:00';
                            $open_hours['hours'][$oKey]['state'] = 1;
                            $open_hours['hours'][$oKey]['hours_24'] = 1;
                        }
                        else if(isset($open_hours_data[$oKey]['state']))
                        {
                            for ($i=0; $i < count($open_hours_data[$oKey]['opening']) ; $i++) 
                            { 
                                $open_hours['hours'][$oKey]['opening'][$i] = $open_hours_data[$oKey]['opening'][$i];
                                $open_hours['hours'][$oKey]['closing'][$i] = $open_hours_data[$oKey]['closing'][$i];
                                $open_hours['hours'][$oKey]['state'] = 1;
                                $open_hours['hours'][$oKey]['hours_24'] = 0;
                            
                                $from = strtotime($open_hours['hours'][$oKey]['opening'][$i]);
                                $to = strtotime($open_hours['hours'][$oKey]['closing'][$i]);

                                if($open_hours['hours'][$oKey]['closing'][$i] != '00:00')
                                {
                                    if($from > $to)
                                        $open_hours_error[] = ucfirst($oKey).' ['.($i+1).']';
                                }
                            }
                        }
                        else
                        {
                            $open_hours['hours'][$oKey]['opening'][0] = NULL;
                            $open_hours['hours'][$oKey]['closing'][0] = NULL;
                            $open_hours['hours'][$oKey]['state'] = 0;
                            $open_hours['hours'][$oKey]['hours_24'] = 0;
                        }
                    }
                }
            }

            /*echo '<pre>';
            print_r($open_hours);
            exit;*/

            // *************** Load and validate exceptions model

            $temp ='';
            $open_hours_exceptions_error = array();

            if(isset($post['DestinationsOpenHoursExceptions']))
            {
                $exceptions = $post['DestinationsOpenHoursExceptions']['exceptions'];

                if(isset($exceptions['hours_24']))
                {
                    $open_hours['exceptions']['date_from'] = $exceptions['date_from'];
                    $open_hours['exceptions']['date_to'] = $exceptions['date_to'];
                    $open_hours['exceptions']['opening'][0] = '00:00';
                    $open_hours['exceptions']['closing'][0] = '00:00';
                    $open_hours['exceptions']['state'] = 1;
                    $open_hours['exceptions']['hours_24'] = 1;
                }
                else if(isset($exceptions['state']))
                {
                    $open_hours['exceptions']['date_from'] = $exceptions['date_from'];
                    $open_hours['exceptions']['date_to'] = $exceptions['date_to'];
                    $open_hours['exceptions']['state'] = 1;
                    $open_hours['exceptions']['hours_24'] = 0;

                    for ($i=0; $i < count($exceptions['opening']) ; $i++) 
                    { 
                        $open_hours['exceptions']['opening'][$i] = $exceptions['opening'][$i];
                        $open_hours['exceptions']['closing'][$i] = $exceptions['closing'][$i];
                    
                        $from = strtotime($exceptions['opening'][$i]);
                        $to = strtotime($exceptions['closing'][$i]);

                        if($exceptions['closing'][$i] != '00:00')
                        {
                            if($from > $to)
                            $open_hours_exceptions_error[] = $i+1;
                        }
                        
                    }
                }
                else
                {
                    $open_hours['exceptions']['date_from'] = $exceptions['date_from'];
                    $open_hours['exceptions']['date_to'] = $exceptions['date_to'];
                    $open_hours['exceptions']['opening'][0] = NULL;
                    $open_hours['exceptions']['closing'][0] = NULL;
                    $open_hours['exceptions']['state'] = 0;
                    $open_hours['exceptions']['hours_24'] = 0;
                }
            }

            if(isset($post['DestinationsOpenAllYear']['selected_months']))
            {
                $destination_open_all_year->selected_months = $post['DestinationsOpenAllYear']['selected_months'];
            }

            /*echo '<pre>';
            print_r($open_hours);
            exit;*/

            if(!empty($open_hours_error) && $flag == 0)
            {
                $flag = 1;
                Yii::$app->session->setFlash('error', '( ' . implode(' , ', $open_hours_error) . ' ) Opening Time must be earlier than Closing Time.');
            }
            else if(!empty($open_hours_exceptions_error) && $flag == 0)
            {
                $flag = 1;
                Yii::$app->session->setFlash('error', 'In Date Exceptions: ( ' . implode(' , ', $open_hours_exceptions_error) . ' ) Opening Time must be earlier than Closing Time.');
            }

            if(isset($post['Destinations']['travel_partners_checkbox']))
            {
                
                foreach ($post['Destinations']['travel_partners_checkbox'] as $value) // oKey = day name
                {
                    $checked = $value;
                    if($post['Destinations']['travel_partners_details'][$value]['comission']/100 > 100 )
                    {
                        $flag = 1;
                        Yii::$app->session->setFlash('error', 'In Travel Partners: Commission is percentage, it cannot be greater than 100.');
                    }
                }
            }





            //$flag = 1;
            $provider_model->active = (!isset($_POST['Destinations']['active']))? 0 : 1 ;
            if($flag)
            {
                return $this->render('create', [
                    'provider_model' => $provider_model,
                    'booking_policies_model' => $booking_policies_model,
                    'provider_financial_model' => $provider_financial_model,
                    'booking_rules_model' => $booking_rules_model,
                    'destination_open_all_year' => $destination_open_all_year,
                    'open_hours' => $open_hours,
                    'meal_options' => $meal_options,
                    'tab_href' => $tab_href,
                    'travel_partners' => $travel_partners_arr,
                    'destination_add_ons' => $add_ons_arr,
                    'amenities' => $amenities,
                ]);
            }

            // *************** Save Provider Model *************** //
            $provider_model->active = (!isset($_POST['Destinations']['active']))? 0 : 1 ;

            $provider_model->use_housekeeping = (!isset($_POST['Destinations']['use_housekeeping']))? 0 : 1 ;

            if($provider_model->save())
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($provider_model,'image')) 
                {
                    if($uploadFile->uploadFile('Destinations',$provider_model->id,'images'))
                    {
                        $provider_model->featured_image = $uploadFile->FileName;
                        $provider_model->update();
                    }
                }

                if(!empty($provider_model->special_requests))
                {
                    foreach ($provider_model->special_requests as $key => $value) 
                    {
                        $request = new DestinationsSpecialRequests();
                        $request->provider_id = $provider_model->id;
                        $request->request_id = $value;
                        $request->save(); 
                    }
                }

                if(!empty($provider_model->upsell_items))
                {
                    foreach ($provider_model->upsell_items as $key => $value) 
                    {
                        $obj = new DestinationUpsellItems();
                        $obj->provider_id = $provider_model->id;
                        $obj->upsell_id = $value;
                        $obj->save(); 
                    }
                }



                // ******************* If Offers Accommodations *********************//

                if($provider_model->offers_accommodation)
                {
                    // *************** Save Tags ans Provider Staff ****************//

                    if(!empty($provider_model->providers_staff))
                    {
                        foreach ($provider_model->providers_staff as $key => $value) 
                        {
                            $staff = new DestinationsStaff();
                            $staff->provider_id = $provider_model->id;
                            $staff->staff_id = $value;
                            $staff->save(); 
                        }
                    }

                    // *************** Save Tags ************* //

                    for ($i=0; $i <=2 ; $i++) 
                    {
                        $tags_type = ''; 
                        switch ($i) 
                        {
                            case 0:
                                $tags_type = 'providers_tags';
                                break;
                            case 1:
                                $tags_type = 'suitable_tags';
                                break;
                            case 2:
                                $tags_type = 'difficulty_tags';
                                break;
                        }

                        if(!empty($provider_model->$tags_type))
                        {
                            foreach ($provider_model->$tags_type as $key => $value) 
                            {
                                $tag = new DestinationsTags();
                                $tag->provider_id = $provider_model->id;
                                $tag->tag_id = $value;
                                $tag->tag_type = $i;
                                $tag->save();  
                            }
                        }
                    }
                }
                else
                {
                    $provider_model->booking_email = NULL;
                    $provider_model->provider_admin = NULL;
                    $provider_model->kennitala = NULL;
                    $provider_model->update();
                }

                if($accommodation)
                {
                    // ******************* If Accommodation type is Hotel ***************//

                    if($provider_model->accommodation_id != $accommodation_model->id)
                    {
                        $provider_model->star_rating = NULL;
                        $provider_model->update();
                    }

                    // *************** Save Amenities ************* //

                    if(is_null($provider_model->amenities_banners))
                        $provider_model->amenities_banners = [];

                    if(!empty($provider_model->amenities))
                    {
                        foreach ($provider_model->amenities as $key => $value) 
                        {
                            $arr = explode(',', $value);

                            $obj = new DestinationsAmenities();
                            $obj->provider_id = $provider_model->id;
                            $obj->amenities_id = $arr[0];
                            $obj->type = $arr[1];
                            $obj->can_be_banner = in_array($arr[0], $provider_model->amenities_banners)?1:0;
                            $obj->save(); 
                        }
                    }

                    // adding tarvel partners
                   
                    if(isset($post['Destinations']['travel_partners_checkbox']))
                    {
                        
                        foreach ($post['Destinations']['travel_partners_checkbox'] as $value) // oKey = day name
                        {
                            $checked = $value;

                            $new_travel_destination = new DestinationTravelPartners();
                            $new_travel_destination->destination_id = $provider_model->id;
                            $new_travel_destination->travel_partner_id = $value;

                            $firstPart = substr($post['Destinations']['travel_partners_details'][$value]['comission'], 0,2);
                            $secondPart = substr($post['Destinations']['travel_partners_details'][$value]['comission'], 2,strlen($post['Destinations']['travel_partners_details'][$value]['comission']));

                            // $provider_financial_model->vat_rate =  $firstPart.'.'.$secondPart;

                            // $new_travel_destination->comission = $post['Destinations']['travel_partners_details'][$value]['comission'];
                            $new_travel_destination->comission = $firstPart.'.'.$secondPart;
                            $new_travel_destination->booking_email_address = $post['Destinations']['travel_partners_details'][$value]['booking_email'];
                            $new_travel_destination->invoice_email_address = $post['Destinations']['travel_partners_details'][$value]['invoice_email'];
                            $new_travel_destination->primary_contact = $post['Destinations']['travel_partners_details'][$value]['primary_contact'];
                            $new_travel_destination->billing_contact = $post['Destinations']['travel_partners_details'][$value]['billing_contact'];
                            $new_travel_destination->notes = $post['Destinations']['travel_partners_details'][$value]['notes'];

                            $new_travel_destination->save();
                        }
                    }
                    // adding add ons

                    if(isset($post['Destinations']['add_ons_checkbox']))
                    {

                        foreach ($post['Destinations']['add_ons_checkbox'] as $value) // oKey = day name
                        {
                            // echo "<pre>";
                            // echo "here";
                            // print_r($post['Destinations']['add_on_details'][$value]);
                            // exit();
                            $checked = $value;

                            $new_destination_add_on = new DestinationAddOns();
                            $new_destination_add_on->destination_id = $provider_model->id;
                            $new_destination_add_on->add_on_id = $value;

                            $new_destination_add_on->name = $post['Destinations']['add_on_details'][$value]['name'];
                            $new_destination_add_on->description = $post['Destinations']['add_on_details'][$value]['description'];
                            $new_destination_add_on->type = $post['Destinations']['add_on_details'][$value]['type'];
                            $new_destination_add_on->discountable = $post['Destinations']['add_on_details'][$value]['discountable'];
                            $new_destination_add_on->commissionable = $post['Destinations']['add_on_details'][$value]['commissionable'];
                            $new_destination_add_on->price = $post['Destinations']['add_on_details'][$value]['price'];
                            $new_destination_add_on->vat = str_replace(',','.', $post['Destinations']['add_on_details'][$value]['vat']);
                            $new_destination_add_on->tax = $post['Destinations']['add_on_details'][$value]['tax'];
                            $new_destination_add_on->fee = $post['Destinations']['add_on_details'][$value]['fee'];

                            if(!$new_destination_add_on->save(false))
                            {
                                echo "<pre>";
                                print_r($new_destination_add_on->errors);
                                exit();
                            }
                        }
                    }

                    // *************** Save Booking Policies Model *************** //

                    $booking_policies_model->provider_id = $provider_model->id;

                    if(!empty($booking_policies_model->payment_methods_gauarantee))
                    {
                        foreach ($booking_policies_model->payment_methods_gauarantee as $key => $value) 
                        {
                            $guarantee = new PaymentMethodGuarantee();
                            $guarantee->provider_id = $provider_model->id;
                            $guarantee->pm_id = $value;
                            $guarantee->save();
                        }
                    }

                    if(!empty($booking_policies_model->payment_methods_checkin))
                    {
                        foreach ($booking_policies_model->payment_methods_checkin as $key => $value) 
                        {
                            $checkin = new PaymentMethodCheckin();
                            $checkin->provider_id = $provider_model->id;
                            $checkin->pm_id = $value;
                            $checkin->save();
                        }
                    }

                    $booking_policies_model->payment_methods_gauarantee ='';
                    $booking_policies_model->payment_methods_checkin='';
                    $booking_policies_model->save();

                    $errors['policies'] = $booking_policies_model->getErrors();


                    // *************** Save Booking Rules Model *************** //

                    $booking_rules_model->provider_id = $provider_model->id;
                    $booking_rules_model->save();

                    $errors['rules'] = $booking_rules_model->getErrors();

                    // *************** Save Providers Financial Model *************** //

                    $provider_financial_model->provider_id = $provider_model->id;
                    $provider_financial_model->license_expiration_date = strtotime($provider_financial_model->license_expiration_date);

                    if(strlen($provider_financial_model->vat_rate) > 2)
                    {
                        $firstPart = substr($provider_financial_model->vat_rate, 0,2);
                        $secondPart = substr($provider_financial_model->vat_rate, 2,strlen($provider_financial_model->vat_rate));

                        $provider_financial_model->vat_rate =  $firstPart.'.'.$secondPart;
                    }

                    $uploadFile = new UploadFile();

                    if ($uploadFile->FileToUpload = UploadedFile::getInstance($provider_financial_model,'pdfFile')) 
                    {
                        if($uploadFile->uploadFile('Destinations',$provider_model->id,'licenses'))
                        {
                            $provider_financial_model->license_image = $uploadFile->FileName;
                        }
                    }
                    
                    $provider_financial_model->save();

                    $errors['financial'] = $provider_financial_model->getErrors();
                }
                else
                {
                    $booking_rules_model->provider_id = $provider_model->id;
                    $booking_rules_model->save();
                    $booking_policies_model->provider_id = $provider_model->id;
                    $booking_policies_model->save();
                    $provider_financial_model->provider_id = $provider_model->id;
                    $provider_financial_model->license_expiration_date = strtotime(date('m/d/Y'));
                    $provider_financial_model->save();

                    $errors['financial'] = $provider_financial_model->getErrors();
                    $errors['rules'] = $booking_rules_model->getErrors();
                    $errors['policies'] = $booking_policies_model->getErrors();
                }

                // *************** Save Meal Plan Model *************** //

                $destination_meal_plan->provider_id = $provider_model->id;

                foreach ($meal_options as $key => $value) 
                {
                    $destination_meal_plan->$key = $value;
                }

                $destination_meal_plan->save();

                // *************** Save Photos *************** //

                if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                {
                    if(!file_exists(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id))
                    {
                        mkdir(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id, 0777, true);
                    }
                    if(!file_exists(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images'))
                    {
                         mkdir(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images', 0777, true);
                    }

                    $srcPath = Yii::getAlias('@app'). '/../uploads/Destinations/temp/images/';
                    $destPath = Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images/';

                    foreach ($_SESSION['images'] as $key => $value) 
                    {
                        if(copy($srcPath.$value, $destPath.$value))
                        {
                            $obj = new DestinationsImages();
                            $obj->provider_id = $provider_model->id;
                            $obj->image = $value;

                            $last_model = DestinationsImages::find()
                                                        ->where(['provider_id' => $provider_model->id])
                                                        ->orderBy('display_order DESC')
                                                        ->one();
                            if(!empty($last_model))
                            {
                                $obj->display_order = $last_model->display_order+1;
                            }
                            else
                            {
                                $obj->display_order = 1;
                            }

                            $obj->save();

                            if(file_exists($srcPath.$value))
                                unlink($srcPath.$value);
                        }   
                    }

                    $this->removeDirectory($srcPath);
                }

                // *************** Save Open Hours Days Model *************** //

                $errors_array = [];
                
                foreach ($open_hours['hours'] as $key => $value)
                {
                    for ($i=0; $i < count($value['opening']) ; $i++) 
                    {
                        $destination_hours_days_model = new DestinationsOpenHoursDays();

                        $destination_hours_days_model->provider_id = $provider_model->id;
                        $destination_hours_days_model->open = $value['state'];
                        $destination_hours_days_model->hours_24 = $value['hours_24'];

                        switch ($key) 
                        {
                            case 'monday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Monday;
                               break;

                            case 'tuesday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Tuesday;
                               break;

                            case 'wednesday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Wednesday;
                               break;

                            case 'thursday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Thursday;
                               break;

                            case 'friday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Friday;
                               break;

                            case 'saturday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Saturday;
                               break;

                            case 'sunday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Sunday;
                               break;
                        }

                        $destination_hours_days_model->opening = $value['opening'][$i];
                        $destination_hours_days_model->closing = $value['closing'][$i];

                        $destination_hours_days_model->save();
                        $errors_array[] = $destination_hours_days_model->getErrors();
                    }
                }

                // echo '<pre>';
                // print_r($open_hours);
                // exit;

                // *************** Save Exceptions Models ***************//

                if(isset($post['DestinationsOpenHoursExceptions']) && !empty($open_hours['exceptions']['date_from']) && !empty($open_hours['exceptions']['date_to']))
                {
                    $open_hours['exceptions']['date_from'] = str_replace('/', '-', $open_hours['exceptions']['date_from']);
                    $destination_hours_exceptions_model->date = date('Y-m-d',strtotime($open_hours['exceptions']['date_from']));

                    $open_hours['exceptions']['date_to'] = str_replace('/', '-', $open_hours['exceptions']['date_to']);
                    $date2 = date('Y-m-d',strtotime($open_hours['exceptions']['date_to']));

                    $date = $destination_hours_exceptions_model->date;

                    $date1=date_create($destination_hours_exceptions_model->date);
                    $date2=date_create($date2);

                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a")+1;

                    for ($i=0; $i < $difference ; $i++) 
                    {
                        $temp_date = '';

                        if($i==0)
                        {
                            $temp_date = $date;
                        }
                        else
                        {
                            $date = strtotime("+1 day", strtotime($date));
                            $temp_date=  date('Y-m-d',$date);
                            $date = date("Y-m-d", $date);
                        }

                        for ($j=0; $j < count($open_hours['exceptions']['opening']) ; $j++) 
                        {
                            $destination_hours_exceptions_model = new DestinationsOpenHoursExceptions();

                            $destination_hours_exceptions_model->opening = $open_hours['exceptions']['opening'][$j];
                            $destination_hours_exceptions_model->closing = $open_hours['exceptions']['closing'][$j];
                            $destination_hours_exceptions_model->state = $open_hours['exceptions']['state'];
                            $destination_hours_exceptions_model->hours_24 = $open_hours['exceptions']['hours_24'];
                            $destination_hours_exceptions_model->provider_id = $provider_model->id;
                            $destination_hours_exceptions_model->date = $temp_date;

                            $destination_hours_exceptions_model->save();
                        } 
                    }
                }
                    
                $errors['days'] = $destination_hours_days_model->getErrors();

                // *************** Save Open All Year Model ***************//

                $destination_open_all_year->provider_id = $provider_model->id;

                if(!empty($destination_open_all_year->selected_months))
                {
                    foreach ($destination_open_all_year->selected_months as $key => $value) 
                    {
                        switch ($value) 
                        {
                            case 1:
                                $destination_open_all_year->jan = 1;
                                break;
                            case 2:
                                $destination_open_all_year->feb = 1;
                                break;
                            case 3:
                                $destination_open_all_year->march = 1;
                                break;
                            case 4:
                                $destination_open_all_year->april = 1;
                                break;
                            case 5:
                                $destination_open_all_year->may = 1;
                                break;
                            case 6:
                                $destination_open_all_year->june = 1;
                                break;
                            case 7:
                                $destination_open_all_year->july = 1;
                                break;
                            case 8:
                                $destination_open_all_year->august = 1;
                                break;
                            case 9:
                                $destination_open_all_year->sep = 1;
                                break;
                            case 10:
                                $destination_open_all_year->oct = 1;
                                break;
                            case 11:
                                $destination_open_all_year->nov = 1;
                                break;
                            case 12:
                                $destination_open_all_year->dec = 1;
                                break;
                        }
                    }
                }
                $destination_open_all_year->save();

                if($provider_model->provider_type_id == 6)
                {
                    if(!empty($_POST['Destinations']['resturant_tags']))
                    {
                        foreach ($_POST['Destinations']['resturant_tags'] as $resturant_tag) 
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 3;
                            $tag->save();  
                        }
                    }
                    if(!empty($_POST['Destinations']['dish']))
                    {
                        foreach ($_POST['Destinations']['dish'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 4;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['type']))
                    {
                        foreach ($_POST['Destinations']['type'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 5;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['dietary_restriction']))
                    {
                        foreach ($_POST['Destinations']['dietary_restriction'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 6;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['meal']))
                    {
                        foreach ($_POST['Destinations']['meal'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 7;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['price']))
                    {
                        foreach ($_POST['Destinations']['price'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 8;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['feature']))
                    {
                        foreach ($_POST['Destinations']['feature'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 9;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['alcohol']))
                    {
                        foreach ($_POST['Destinations']['alcohol'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 10;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['good_for']))
                    {
                        foreach ($_POST['Destinations']['good_for'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 11;
                            $tag->save();
                        }
                    }
                }

                Yii::$app->session->setFlash('success', 'Destination is created successfully');   
            }

            $errors['provider'] = $provider_model->getErrors();

            return $this->redirect(['index']);
        } 
        else 
        {
            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                unset($_SESSION['images']);

            return $this->render('create', [
                'provider_model' => $provider_model,
                'booking_policies_model' => $booking_policies_model,
                'provider_financial_model' => $provider_financial_model,
                'booking_rules_model' => $booking_rules_model,
                'destination_open_all_year' => $destination_open_all_year,
                'open_hours' => $open_hours,
                'meal_options' => $meal_options,
                'tab_href' => $tab_href,
                'travel_partners' => $travel_partners_arr,
                'destination_add_ons' => $add_ons_arr,
                'amenities' => $amenities,
            ]);
        }
    }

    public function removeDirectory($path) 
    {
        $files = glob($path . '/*');
        foreach ($files as $file) 
        {
          is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }

    /**
     * Updates an existing Destinations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$tab_href='#details',$flash=null)
    {
       /* echo '<pre>';
        print_r($_POST);
        exit;*/
        $tab_href = $tab_href;

        $searchModel = new DestinationsSearch();
        $dataProvider = $searchModel->searchImages($id);

        $searchModelException = new DestinationsSearch();
        $dataProviderException = $searchModelException->searchExceptions($id);

        $provider_model = $this->findModel($id);
        $prev_destination_type = $provider_model->destinationType->name;

        if($prev_destination_type == 'Restaurant')
        {
            $arr = array();
            $dish_arr = array();
            $type_arr = array();
            $dietary_arr = array();
            $meal_arr = array();
            $price_arr = array();
            $feature_arr = array();
            $alcohol_arr = array();
            $good_for_arr = array();
            $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id])->all();
            foreach ($rest_tags as $value) 
            {
                if($value->tag_type == 3) {
                    array_push($arr, $value->tag_id);
                }
                if($value->tag_type == 4) {
                    array_push($dish_arr, $value->tag_id);
                }
                if($value->tag_type == 5) {
                    array_push($type_arr, $value->tag_id);
                }
                if($value->tag_type == 6) {
                    array_push($dietary_arr, $value->tag_id);
                }
                if($value->tag_type == 7) {
                    array_push($meal_arr, $value->tag_id);
                }
                if($value->tag_type == 8) {
                    array_push($price_arr, $value->tag_id);
                }
                if($value->tag_type == 9) {
                    array_push($feature_arr, $value->tag_id);
                }
                if($value->tag_type == 10) {
                    array_push($alcohol_arr, $value->tag_id);
                }
                if($value->tag_type == 11) {
                    array_push($good_for_arr, $value->tag_id);
                }
            }
            $provider_model->resturant_tags = $arr;
            $provider_model->dish = $dish_arr;
            $provider_model->type = $type_arr;
            $provider_model->dietary_restriction = $dietary_arr;
            $provider_model->meal = $meal_arr;
            $provider_model->price = $price_arr;
            $provider_model->feature = $feature_arr;
            $provider_model->alcohol = $alcohol_arr;
            $provider_model->good_for = $good_for_arr;

        }
        
        if(preg_match('/\s/',$prev_destination_type))
        {
            $arr = explode(' ', $prev_destination_type);
            $prev_destination_type = $arr[0];  
        }

        // ********** Dynamic Tabs
        $booking_policies_model = BookingPolicies::findOne(['provider_id' => $provider_model->id]);
        $booking_policies_model->booking_flag_updated = ($booking_policies_model->booking_flag_updated == null)?0:$booking_policies_model->booking_flag_updated;
        $provider_financial_model = DestinationsFinancial::findOne(['provider_id' => $provider_model->id]);
        $booking_rules_model = BookingRules::findOne(['provider_id' => $provider_model->id]);

        $accommodation_model = AccommodationTypes::findOne(['name' => 'Hotel']);

        $allIds = Amenities::find()->select('id')->column();
        $ids = DestinationsAmenities::find()->select('amenities_id')->where(['provider_id' => $provider_model->id])->column();
        $ids = array_merge($ids, array_diff($allIds, $ids));
        $amenities = Amenities::find()
            ->orderBy(new \yii\db\Expression('FIELD (id, '.implode(',', $ids).')'))
            ->all();

        // ********** Meal Plan
        $destination_meal_plan = DestinationsMealPlan::findOne(['provider_id' => $provider_model->id]);
        $destination_open_all_year = DestinationsOpenAllYear::findOne(['provider_id' => $provider_model->id]);

        if(empty($destination_open_all_year))
        {
            $destination_open_all_year = new DestinationsOpenAllYear();
        }

        $currentTime = '17:00';
        $currentDate = date('d/m/Y');
        $openingTime = '00:00';//date('HH:i', strtotime('-1 hour'));
        $closingTime = '00:00';

        $open_hours = [
            'form' => 'update',
            'provider_id' => $id,
            'hours' => [
                'monday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'tuesday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'wednesday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'thursday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'friday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'saturday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
                'sunday' => [
                    'opening' => [
                    ],
                    'closing' => [
                    ],
                    'state' => 1,
                    'hours_24' => 0,
                ],
            ],
            'exceptions' => [
                'date_from' => '',
                'date_to' => '',
                'opening' => [
                        0 => $openingTime,
                    ],
                    'closing' => [
                        0 => $closingTime,
                    ],
                'state' => 1,
                'hours_24' => 0,
            ]
        ];

        $meal_options = [

            'pfo_breakfast' => 4,
            'pfo_lunch' => 4,
            'pfo_dinner' => 4,
            'sfo_morning_snack' => 4,
            'sfo_afternoon_snack' => 4,
            'sfo_evening_snack' => 4,
            'abo_tea' => 4,
            'abo_fountain' => 4,
            'abo_beer' => 4,
            'abo_liquor' => 4,
            'abo_minibar' => 4
        ];

        if(!empty($destination_meal_plan))
        {
            foreach ($destination_meal_plan as $key => $value) 
            {
                if($key!='provider_id')
                    $meal_options[$key] = $value;
            }
        }
        else
        {
            $destination_meal_plan = new DestinationsMealPlan();
        }

        // ********** Multiple Relation Model

        $get_guarantee = PaymentMethodGuarantee::find()->where(['provider_id' => $id])->all();
        $get_checkin = PaymentMethodCheckin::find()->where(['provider_id' => $id])->all();

        $get_providers_tags = DestinationsTags::find()->where(['provider_id' => $id,'tag_type'=>0])->all();
        $get_suitable_tags = DestinationsTags::find()->where(['provider_id' => $id,'tag_type'=>1])->all();
        $get_difficulty_tags = DestinationsTags::find()->where(['provider_id' => $id,'tag_type'=>2])->all();

        $get_amenities = DestinationsAmenities::find()->where(['provider_id' => $id])->orderBy('id')->all();

        $get_staff = DestinationsStaff::find()->where(['provider_id' => $id])->all();
        $get_requests = DestinationsSpecialRequests::find()->where(['provider_id' => $id])->all();
        $get_upsell_items = DestinationUpsellItems::find()->where(['provider_id' => $id])->all();

        for ($i=0; $i <=2 ; $i++) 
        {
            $tags_get = 'get_';
            $tags_type = ''; 
            switch ($i) 
            {
                case 0:
                    $tags_type = 'providers_tags';
                    break;
                case 1:
                    $tags_type = 'suitable_tags';
                    break;
                case 2:
                    $tags_type = 'difficulty_tags';
                    break;
            }

            $tags_get = $tags_get.$tags_type;

            if(!empty(${$tags_get}))
            {
                $saved = array();
                foreach (${$tags_get} as $key => $value) 
                {
                    $saved[] = $value['tag_id'];
                }
                $provider_model->$tags_type = $saved;
            }
        }

        if(!empty($get_amenities))
        {
            $saved_amenities = array();
            $saved_banners = array();

            foreach ($get_amenities as $key => $value) 
            {
                $temp = array();
                $temp[] = $value['amenities_id'];
                $temp[] = $value['type'];
                $temp = implode(',', $temp);
                $saved_amenities[$value['amenities_id']] = $temp;

                if($value['can_be_banner'])
                    $saved_banners[] = $value['amenities_id'];
            }
            $provider_model->amenities = $saved_amenities;
            $provider_model->amenities_banners = $saved_banners;
        }

        if(!empty($get_staff))
        {
            foreach ($get_staff as $key => $value) 
            {
                $saved_staff[] = $value['staff_id'];
            }
            $provider_model->providers_staff = $saved_staff;
        }

        if(!empty($get_requests))
        {
            foreach ($get_requests as $key => $value) 
            {
                $saved_requests[] = $value['request_id'];
            }
            $provider_model->special_requests = $saved_requests;
        }

        if(!empty($get_upsell_items))
        {
            foreach ($get_upsell_items as $key => $value) 
            {
                $saved[] = $value['upsell_id'];
            }
            $provider_model->upsell_items = $saved;
        }

        $saved_guarantee ='';
        $saved_checkin = '';

        if(!empty($get_guarantee))
        {
            foreach ($get_guarantee as $key => $value) 
            {
                $saved_guarantee[] = $value['pm_id'];
            }
            $booking_policies_model->payment_methods_gauarantee = $saved_guarantee;
        }
        if(!empty($get_checkin))
        {
            foreach ($get_checkin as $key => $value) 
            {
                $saved_checkin[] = $value['pm_id'];
            }
            $booking_policies_model->payment_methods_checkin = $saved_checkin; 
        } 

        $payment_methods_arr='';

        $methods = PropertyPaymentMethods::find()->all();
        foreach ($methods as $key => $value) 
        {
            BookingPolicies::$methodsGuarantee[$value['id']] = $value['name'];
            BookingPolicies::$methodsCheckin[$value['id']] = $value['name'];
        }

        $requests = SpecialRequests::find()->orderBy('label')->all();
        foreach ($requests as $key => $value) 
        {
            Destinations::$all_special_requests[$value['id']] = $value['label'];
        }

        // getting travel partners of this destination
        $travel_partners = $provider_model->destinationtravelpartners;
        // echo "<pre>";
        // print_r($travel_partners);
        // exit();
        $travel_partners_arr = array();
        foreach ($travel_partners as $value) {
            // $travel_partners_arr[$value->travel_partner_id]['comission'] = $value->comission;
            $commission =  str_replace('.', '', $value->comission);
            
            $commission = $commission.str_repeat("0",4-strlen($commission));
            // $travel_partners_arr[$value->travel_partner_id]['comission'] = str_replace('.', '', $value->comission);
            $travel_partners_arr[$value->travel_partner_id]['comission'] = $commission;
            $travel_partners_arr[$value->travel_partner_id]['booking_email_address'] = $value->booking_email_address;
            $travel_partners_arr[$value->travel_partner_id]['invoice_email_address'] = $value->invoice_email_address;
            $travel_partners_arr[$value->travel_partner_id]['primary_contact'] = $value->primary_contact;
            $travel_partners_arr[$value->travel_partner_id]['billing_contact'] = $value->billing_contact;
            $travel_partners_arr[$value->travel_partner_id]['notes'] = $value->notes;
        }

        $add_ons = $provider_model->destinationAddOns;
        // echo "<pre>";
        // print_r($travel_partners);
        // exit();
        $add_on_arr = array();
        foreach ($add_ons as $value) {
            $add_on_arr[$value->add_on_id]['name'] = $value->name;
            $add_on_arr[$value->add_on_id]['description'] = $value->description;
            $add_on_arr[$value->add_on_id]['type'] = $value->type;
            $add_on_arr[$value->add_on_id]['discountable'] = $value->discountable;
            $add_on_arr[$value->add_on_id]['commissionable'] = $value->commissionable;
            $add_on_arr[$value->add_on_id]['vat'] = $value->vat;
            $temp = explode('.', $value->vat);
            if(isset($temp[1]))
            {
                $temp[1].='00000';
                $temp[1] = substr($temp[1], 0, 2);
                $temp[0].=$temp[1];
                $add_on_arr[$value->add_on_id]['vat'] = $temp[0];
            }
            else
            {
                $add_on_arr[$value->add_on_id]['vat'].='.00';
            }
            //$add_on_arr[$value->add_on_id]['vat'] = $value->vat;
            $add_on_arr[$value->add_on_id]['tax'] = $value->tax;
            $add_on_arr[$value->add_on_id]['price'] = $value->price;
            $add_on_arr[$value->add_on_id]['fee'] = $value->fee;
        }

        if($provider_model->load(Yii::$app->request->post())) 
        {
            // echo '<pre>';
            // print_r($_POST);
            // exit;

            $flag = 0;
            $accommodation = 0;
            $post = Yii::$app->request->post();

            // *************** Validate Provider Model *************** //

            $provider_model->phone = $provider_model->server_phone;
            $provider_model->fax = $provider_model->server_fax;

            $tab_href = $_POST['Destinations']['tab_href'];
            $provider_model->url = $_POST['Destinations']['url'];
            $provider_model->providers_staff = $_POST['Destinations']['providers_staff'];
            $provider_model->providers_tags = $_POST['Destinations']['providers_tags'];
            $provider_model->suitable_tags = $_POST['Destinations']['suitable_tags'];
            $provider_model->difficulty_tags = $_POST['Destinations']['difficulty_tags'];
            $provider_model->special_requests = $_POST['Destinations']['special_requests'];
            $provider_model->upsell_items = $_POST['Destinations']['upsell_items'];

            $provider_model->amenities = isset($_POST['Destinations']['amenities_radio']) && !empty($_POST['Destinations']['amenities_radio'])? $_POST['Destinations']['amenities_radio']: NULL;

            $provider_model->amenities_banners = isset($_POST['Destinations']['amenities_checkbox']) && !empty($_POST['Destinations']['amenities_checkbox'])? $_POST['Destinations']['amenities_checkbox']: NULL;

            if($provider_model->transparent_background)
                $provider_model->background_color = 'transparent';

            if($provider_model->provider_type_id != $provider_model->getAccomodation())
            {
                $provider_model->accommodation_id = '';

                if(isset($_POST['Destinations']['offers_accommodation']))
                {
                    $provider_model->offers_accommodation = 1;
                }
                else
                {
                    $provider_model->offers_accommodation = 0;
                }
            }
            else
            {
                $accommodation = 1;
                $provider_model->offers_accommodation = 1;

                $booking_policies_model->load(Yii::$app->request->post());
                $booking_rules_model->load(Yii::$app->request->post());
                $provider_financial_model->load(Yii::$app->request->post());

                $provider_financial_model->lodging_tax_per_night = $this->removeLeadingZero($provider_financial_model->server_lodging_tax_per_night);
                $provider_financial_model->vat_rate = $provider_financial_model->server_vat_rate;

                // *************** Validate Booking Policies Model *************** //

                $provider_financial_model->license_expiration_date = strtotime($provider_financial_model->license_expiration_date);

                $booking_policies_model->booking_flag_updated = isset($_POST['BookingPolicies']['booking_flag_updated'])?1:0;

                $in_from = strtotime($booking_policies_model->checkin_time_from);
                $in_to = strtotime($booking_policies_model->checkin_time_to);

                $out_from =  strtotime($booking_policies_model->checkout_time_from);
                $out_to =  strtotime($booking_policies_model->checkout_time_to);

                if($in_from >= $in_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Check-in From Time must be earlier than Check-in To Time.');
                }

                if($out_from >= $out_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Checkout From Time must be earlier than Checkout To Time.');
                }

                if($in_from >= $in_to && $out_from >= $out_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Check-in From Time must be earlier than Check-in To Time. Checkout From Time must be earlier than Checkout To Time');
                }

                // **************** Load Meal Plan

                if(isset($post['DestinationsMealPlan']))
                {
                    foreach ($post['DestinationsMealPlan'] as $key => $value) 
                    {
                        $meal_options[$key] = $value;
                    }
                }
            }

            // **************** Load and validate Days Model

            $open_hours_error = [];
            $open_hours_error_second = [];

            $post['DestinationsOpenHoursDays'] = !isset($post['DestinationsOpenHoursDays'])?[]:$post['DestinationsOpenHoursDays'];

            if(isset($post['DestinationsOpenHoursDays']))
            {
                $open_hours_data = $post['DestinationsOpenHoursDays'];

                foreach ($open_hours['hours'] as $oKey => $oValue) // oKey = day name
                {
                    if(!isset($open_hours_data[$oKey]))
                    {
                        $open_hours['hours'][$oKey]['opening'][0] = NULL;
                        $open_hours['hours'][$oKey]['closing'][0] = NULL;
                        $open_hours['hours'][$oKey]['state'] = 0;
                        $open_hours['hours'][$oKey]['hours_24'] = 0;
                    }
                    else
                    {
                        if(isset($open_hours_data[$oKey]['hours_24']))
                        {
                            $open_hours['hours'][$oKey]['opening'][0] = '00:00';
                            $open_hours['hours'][$oKey]['closing'][0] = '00:00';
                            $open_hours['hours'][$oKey]['state'] = 1;
                            $open_hours['hours'][$oKey]['hours_24'] = 1;
                        }
                        else if(isset($open_hours_data[$oKey]['state']))
                        {
                            for ($i=0; $i < count($open_hours_data[$oKey]['opening']) ; $i++) 
                            { 
                                $open_hours['hours'][$oKey]['opening'][$i] = $open_hours_data[$oKey]['opening'][$i];
                                $open_hours['hours'][$oKey]['closing'][$i] = $open_hours_data[$oKey]['closing'][$i];
                                $open_hours['hours'][$oKey]['state'] = 1;
                                $open_hours['hours'][$oKey]['hours_24'] = 0;
                            
                                $from = strtotime($open_hours['hours'][$oKey]['opening'][$i]);
                                $to = strtotime($open_hours['hours'][$oKey]['closing'][$i]);

                                $tempTo = $open_hours['hours'][$oKey]['closing'][$i];

                                if($tempTo != '00:00')
                                {
                                    if($from > $to )
                                    $open_hours_error[] = ucfirst($oKey).' ['.($i+1).']';
                                }   
                            }
                        }
                        else
                        {
                            $open_hours['hours'][$oKey]['opening'][0] = NULL;
                            $open_hours['hours'][$oKey]['closing'][0] = NULL;
                            $open_hours['hours'][$oKey]['state'] = 0;
                            $open_hours['hours'][$oKey]['hours_24'] = 0;
                        }
                    }
                }
            }

            // *************** Load and validate exceptions model

            $temp ='';
            $open_hours_exceptions_error = array();

            if(isset($post['DestinationsOpenHoursExceptions']))
            {
                $exceptions = $post['DestinationsOpenHoursExceptions']['exceptions'];

                if(isset($exceptions['hours_24']))
                {
                    $open_hours['exceptions']['date_from'] = $exceptions['date_from'];
                    $open_hours['exceptions']['date_to'] = $exceptions['date_to'];
                    $open_hours['exceptions']['opening'][0] = '00:00';
                    $open_hours['exceptions']['closing'][0] = '00:00';
                    $open_hours['exceptions']['state'] = 1;
                    $open_hours['exceptions']['hours_24'] = 1;
                }
                else if(isset($exceptions['state']))
                {
                    $open_hours['exceptions']['date_from'] = $exceptions['date_from'];
                    $open_hours['exceptions']['date_to'] = $exceptions['date_to'];
                    $open_hours['exceptions']['state'] = 1;
                    $open_hours['exceptions']['hours_24'] = 0;

                    for ($i=0; $i < count($exceptions['opening']) ; $i++) 
                    { 
                        $open_hours['exceptions']['opening'][$i] = $exceptions['opening'][$i];
                        $open_hours['exceptions']['closing'][$i] = $exceptions['closing'][$i];
                    
                        $from = strtotime($exceptions['opening'][$i]);
                        $to = strtotime($exceptions['closing'][$i]);

                        $tempTo = $exceptions['closing'][$i];

                        if($tempTo != '00:00')
                        {
                            if($from > $to)
                                $open_hours_exceptions_error[] = $i+1;
                        }
                    }
                }
                else
                {
                    $open_hours['exceptions']['date_from'] = $exceptions['date_from'];
                    $open_hours['exceptions']['date_to'] = $exceptions['date_to'];
                    $open_hours['exceptions']['opening'][0] = NULL;
                    $open_hours['exceptions']['closing'][0] = NULL;
                    $open_hours['exceptions']['state'] = 0;
                    $open_hours['exceptions']['hours_24'] = 0;
                }

            }

            if(isset($post['DestinationsOpenAllYear']['selected_months']))
            {
                $destination_open_all_year->selected_months = $post['DestinationsOpenAllYear']['selected_months'];
            }

            /*echo '<pre>';
            print_r($open_hours);
            exit;*/

            // *************** Load and validate exceptions model

            if(!empty($open_hours_error) && $flag == 0)
            {
                $flag = 1;
                Yii::$app->session->setFlash('error', '( ' . implode(' , ', $open_hours_error) . ' ) Opening Time must be earlier than Closing Time.');
            }
            else if(!empty($open_hours_exceptions_error) && $flag == 0)
            {
                $flag = 1;
                Yii::$app->session->setFlash('error', 'In Exceptions: ( ' . implode(' , ', $open_hours_exceptions_error) . ' ) Opening Time must be earlier than Closing Time.');
            }

            if(isset($post['Destinations']['travel_partners_checkbox']))
            {
                
                foreach ($post['Destinations']['travel_partners_checkbox'] as $value) // oKey = day name
                {
                    $checked = $value;
                    // echo "<pre>";
                    // print_r($post['Destinations']['travel_partners_details'][$value]['comission']);
                    // exit();
                    if($post['Destinations']['travel_partners_details'][$value]['comission']/100 > 100 )
                    {
                        $flag = 1;
                        Yii::$app->session->setFlash('error', 'In Travel Partners: Commission is percentage, it cannot be greater than 100.');
                    }
                }
            }

            $provider_model->active = (!isset($_POST['Destinations']['active']))? 0 : 1 ;

            $provider_model->use_housekeeping = (!isset($_POST['Destinations']['use_housekeeping']))? 0 : 1 ;

            //$flag=1;
            if($flag)
            {
                return $this->render('update', [
                    'provider_model' => $provider_model,
                    'booking_policies_model' => $booking_policies_model,
                    'provider_financial_model' => $provider_financial_model,
                    'booking_rules_model' => $booking_rules_model,
                    'prev_destination_type' => $prev_destination_type,
                    'destination_open_all_year' => $destination_open_all_year,
                    'open_hours' => $open_hours,
                    'meal_options' => $meal_options,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'searchModelException' => $searchModelException,
                    'dataProviderException' => $dataProviderException,
                    'tab_href' => $tab_href,
                    'travel_partners' => $travel_partners_arr,
                    'destination_add_ons' => $add_on_arr,
                    'amenities' => $amenities,
                ]);
            }

            // *************** Save Provider Model *************** //
            $provider_model->active = (!isset($_POST['Destinations']['active']))? 0 : 1 ;

            $provider_model->use_housekeeping = (!isset($_POST['Destinations']['use_housekeeping']))? 0 : 1 ;
            
            if($provider_model->save())
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($provider_model,'image')) 
                {
                    if($uploadFile->uploadFile('Destinations',$provider_model->id,'images'))
                    {
                        if(!empty($provider_model->featured_image))
                        {
                            $prev_image = Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images/'.$provider_model->featured_image;

                            if(file_exists($prev_image))
                                unlink($prev_image);
                        }

                        $provider_model->featured_image = $uploadFile->FileName;
                        $provider_model->update();
                    }
                }

                if(!empty($get_staff))
                {
                    foreach ($get_staff as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($get_providers_tags))
                {
                    foreach ($get_providers_tags as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($get_suitable_tags))
                {
                    foreach ($get_suitable_tags as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($get_difficulty_tags))
                {
                    foreach ($get_difficulty_tags as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($get_requests))
                {
                    foreach ($get_requests as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($get_upsell_items))
                {
                    foreach ($get_upsell_items as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($provider_model->special_requests))
                {
                    foreach ($provider_model->special_requests as $key => $value) 
                    {
                        $request = new DestinationsSpecialRequests();
                        $request->provider_id = $provider_model->id;
                        $request->request_id = $value;
                        $request->save(); 
                    }
                }

                if(!empty($provider_model->upsell_items))
                {
                    foreach ($provider_model->upsell_items as $key => $value) 
                    {
                        $obj = new DestinationUpsellItems();
                        $obj->provider_id = $provider_model->id;
                        $obj->upsell_id = $value;
                        $obj->save(); 
                    }
                }

                // ******************* If Offers Accommodations *********************//

                if($provider_model->offers_accommodation)
                {
                    // *************** Save Tags ans Provider Staff ****************//

                    if(!empty($provider_model->providers_staff))
                    {
                        foreach ($provider_model->providers_staff as $key => $value) 
                        {
                            $staff = new DestinationsStaff();
                            $staff->provider_id = $provider_model->id;
                            $staff->staff_id = $value;
                            $staff->save(); 
                        }
                    }

                    // *************** Save Tags ************* //

                    for ($i=0; $i <=2 ; $i++) 
                    {
                        $tags_type = ''; 
                        switch ($i) 
                        {
                            case 0:
                                $tags_type = 'providers_tags';
                                break;
                            case 1:
                                $tags_type = 'suitable_tags';
                                break;
                            case 2:
                                $tags_type = 'difficulty_tags';
                                break;
                        }

                        if(!empty($provider_model->$tags_type))
                        {
                            foreach ($provider_model->$tags_type as $key => $value) 
                            {
                                $tag = new DestinationsTags();
                                $tag->provider_id = $provider_model->id;
                                $tag->tag_id = $value;
                                $tag->tag_type = $i;
                                $tag->save();  
                            }
                        }
                    }
                }
                else
                {
                    $provider_model->booking_email = NULL;
                    $provider_model->provider_admin = NULL;
                    $provider_model->kennitala = NULL;
                    $provider_model->update();
                }

                if($accommodation)
                {
                    // ******************* If Accommodation type is Hotel ***************//

                    if($provider_model->accommodation_id != $accommodation_model->id)
                    {
                        $provider_model->star_rating = NULL;
                        $provider_model->update();
                    }
                    
                    // *************** Save Amenities ************* //

                    if(is_null($provider_model->amenities_banners))
                        $provider_model->amenities_banners = [];
                    
                    if(!empty($provider_model->amenities))
                    {
                        DestinationsAmenities::deleteAll(['provider_id' => $id]);
                        foreach ($provider_model->amenities as $key => $value) 
                        {
                            $arr = explode(',', $value);

                            $obj = new DestinationsAmenities();
                            $obj->provider_id = $provider_model->id;
                            $obj->amenities_id = $arr[0];
                            $obj->type = $arr[1];
                            $obj->can_be_banner = in_array($arr[0], $provider_model->amenities_banners)?1:0;
                            $obj->save(); 
                        }
                    }

                    // adding tarvel partners
                   
                    if(isset($post['Destinations']['travel_partners_checkbox']))
                    {
                        $to_be_deleted = array();
                        foreach ($post['Destinations']['travel_partners_checkbox'] as $value) // oKey = day name
                        {
                            array_push($to_be_deleted, $value);
                        }       
                        // DestinationTravelPartners::deleteAll(['destination_id' => $provider_model->id]);
                        DestinationTravelPartners::deleteAll(['and', 
                                                                'destination_id = :type_id', 
                                                                ['not in', 'travel_partner_id', $to_be_deleted]], 
                                                                [':type_id' => $provider_model->id ]
                                                            );
                        foreach ($post['Destinations']['travel_partners_checkbox'] as $value) // oKey = day name
                        {
                            $checked = $value;

                            $new_travel_destination = DestinationTravelPartners::findOne(['travel_partner_id' => $value,'destination_id' => $provider_model->id]);
                            if(empty($new_travel_destination))
                            {
                                $new_travel_destination = new DestinationTravelPartners();
                            }
                            
                            $new_travel_destination->destination_id = $provider_model->id;
                            $new_travel_destination->travel_partner_id = $value;

                            $firstPart = substr($post['Destinations']['travel_partners_details'][$value]['comission'], 0,2);
                            $secondPart = substr($post['Destinations']['travel_partners_details'][$value]['comission'], 2,strlen($post['Destinations']['travel_partners_details'][$value]['comission']));

                            // $provider_financial_model->vat_rate =  $firstPart.'.'.$secondPart;

                            // $new_travel_destination->comission = $post['Destinations']['travel_partners_details'][$value]['comission'];
                            $new_travel_destination->comission = $firstPart.'.'.$secondPart;
                            $new_travel_destination->booking_email_address = $post['Destinations']['travel_partners_details'][$value]['booking_email'];
                            $new_travel_destination->invoice_email_address = $post['Destinations']['travel_partners_details'][$value]['invoice_email'];
                            $new_travel_destination->primary_contact = $post['Destinations']['travel_partners_details'][$value]['primary_contact'];
                            $new_travel_destination->billing_contact = $post['Destinations']['travel_partners_details'][$value]['billing_contact'];
                            $new_travel_destination->notes = $post['Destinations']['travel_partners_details'][$value]['notes'];

                            $new_travel_destination->save();
                        }
                    }
                    else
                    {
                        DestinationTravelPartners::deleteAll(['destination_id' => $provider_model->id]);
                    }

                    if(isset($post['Destinations']['add_ons_checkbox']))
                    {
                        DestinationAddOns::deleteAll(['destination_id' => $provider_model->id]);
                        foreach ($post['Destinations']['add_ons_checkbox'] as $value) // oKey = day name
                        {
                            // echo "<pre>";
                            // echo "here";
                            // print_r($post['Destinations']['add_on_details'][$value]);
                            // exit();
                            $checked = $value;

                            $new_destination_add_on = new DestinationAddOns();
                            $new_destination_add_on->destination_id = $provider_model->id;
                            $new_destination_add_on->add_on_id = $value;
                            //print_r($post['Destinations']['add_on_details']); die;

                            $new_destination_add_on->name = $post['Destinations']['add_on_details'][$value]['name'];
                            $new_destination_add_on->description = $post['Destinations']['add_on_details'][$value]['description'];
                            $new_destination_add_on->type = $post['Destinations']['add_on_details'][$value]['type'];
                            $new_destination_add_on->discountable = $post['Destinations']['add_on_details'][$value]['discountable'];
                            $new_destination_add_on->commissionable = $post['Destinations']['add_on_details'][$value]['commissionable'];
                            $new_destination_add_on->price = $post['Destinations']['add_on_details'][$value]['price'];
                            $new_destination_add_on->vat = str_replace(',','.', $post['Destinations']['add_on_details'][$value]['vat']);
                            $new_destination_add_on->tax = $post['Destinations']['add_on_details'][$value]['tax'];
                            $new_destination_add_on->fee = $post['Destinations']['add_on_details'][$value]['fee'];

                            if(!$new_destination_add_on->save(false))
                            {
                                echo "<pre>";
                                print_r($new_destination_add_on->errors);
                                exit();
                            }
                        }
                    }
                    else
                    {
                        DestinationAddOns::deleteAll(['destination_id' => $provider_model->id]);
                    }
                    
                    // *************** Save Booking Policies Model *************** //

                    $booking_policies_model->provider_id = $provider_model->id;

                    if(!empty($get_guarantee))
                    {
                        foreach ($get_guarantee as $key => $value) 
                        {
                            $value->delete();
                        }
                    }

                    if(!empty($get_checkin))
                    {
                        foreach ($get_checkin as $key => $value) 
                        {
                            $value->delete();
                        }
                    }

                    if(!empty($booking_policies_model->payment_methods_gauarantee))
                    {
                        foreach ($booking_policies_model->payment_methods_gauarantee as $key => $value) 
                        {
                            $guarantee = new PaymentMethodGuarantee();
                            $guarantee->provider_id = $provider_model->id;
                            $guarantee->pm_id = $value;
                            $guarantee->save();
                        }
                    }

                    if(!empty($booking_policies_model->payment_methods_checkin))
                    {
                        foreach ($booking_policies_model->payment_methods_checkin as $key => $value) 
                        {
                            $checkin = new PaymentMethodCheckin();
                            $checkin->provider_id = $provider_model->id;
                            $checkin->pm_id = $value;
                            $checkin->save();
                        }
                    }

                    $booking_policies_model->payment_methods_gauarantee ='';
                    $booking_policies_model->payment_methods_checkin='';
                    $booking_policies_model->save();

                    /*echo '<pre>';
                    print_r($booking_policies_model->getErrors());
                    exit;*/

                    $errors['policies'] = $booking_policies_model->getErrors();


                    // *************** Save Booking Rules Model *************** //

                    $booking_rules_model->provider_id = $provider_model->id;

                    $booking_rules_model->save();


                    $errors['rules'] = $booking_rules_model->getErrors();

                    // *************** Save Providers Financial Model *************** //

                    $provider_financial_model->provider_id = $provider_model->id;

                    if(strlen($provider_financial_model->vat_rate) > 2)
                    {
                        $firstPart = substr($provider_financial_model->vat_rate, 0,2);
                        $secondPart = substr($provider_financial_model->vat_rate, 2,strlen($provider_financial_model->vat_rate));

                        $provider_financial_model->vat_rate =  $firstPart.'.'.$secondPart;
                    }

                    $uploadFile = new UploadFile();

                    if ($uploadFile->FileToUpload = UploadedFile::getInstance($provider_financial_model,'pdfFile')) 
                    {
                        if($uploadFile->uploadFile('Destinations',$provider_model->id,'licenses'))
                        {
                            $provider_financial_model->license_image = $uploadFile->FileName;

                            $saved_financial_model = DestinationsFinancial::findOne(['provider_id' => $provider_model->id]);

                            if(!empty($saved_financial_model->license_image))
                            {
                                $path = Yii::getAlias('@webroot') . '/../uploads/Destinations/'.$provider_model->id.'/licenses';
                                $file = $path . '/'.$saved_financial_model->license_image;

                                if(file_exists($file))
                                    unlink($file);
                            }
                        }
                    }
                    
                    $provider_financial_model->save();

                    $errors['financial'] = $provider_financial_model->getErrors();

                    /*print_r($errors);
                    exit;*/
                }
                else
                {
                    $booking_rules_model->provider_id = $provider_model->id;
                    $booking_rules_model->save();
                    $booking_policies_model->provider_id = $provider_model->id;
                    $booking_policies_model->checkin_time_from = '';
                    $booking_policies_model->checkin_time_to = '';
                    $booking_policies_model->checkout_time_from = '';
                    $booking_policies_model->checkout_time_to = '';
                    $booking_policies_model->save();
                    $provider_financial_model->provider_id = $provider_model->id;
                    $provider_financial_model->license_expiration_date = strtotime(date('m/d/Y'));
                    $provider_financial_model->save();

                    $errors['financial'] = $provider_financial_model->getErrors();
                    $errors['rules'] = $booking_rules_model->getErrors();
                    $errors['policies'] = $booking_policies_model->getErrors();
                }

                // *************** Save Meal Plan Model *************** //

                $destination_meal_plan->provider_id = $provider_model->id;

                foreach ($meal_options as $key => $value) 
                {
                    $destination_meal_plan->$key = $value;
                }

                $destination_meal_plan->save();

                // *************** Save Photos *************** //

                if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                {
                    if(!file_exists(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id))
                    {
                        mkdir(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id, 0777, true);
                    }
                    if(!file_exists(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images'))
                    {
                         mkdir(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images', 0777, true);
                    }

                    $srcPath = Yii::getAlias('@app'). '/../uploads/Destinations/temp/images/';
                    $destPath = Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images/';

                    foreach ($_SESSION['images'] as $key => $value) 
                    {
                        if(copy($srcPath.$value, $destPath.$value))
                        {
                            $obj = new DestinationsImages();
                            $obj->provider_id = $provider_model->id;
                            $obj->image = $value;

                            $last_model = DestinationsImages::find()
                                                        ->where(['provider_id' => $provider_model->id])
                                                        ->orderBy('display_order DESC')
                                                        ->one();
                            if(!empty($last_model))
                            {
                                $obj->display_order = $last_model->display_order+1;
                            }
                            else
                            {
                                $obj->display_order = 1;
                            }

                            $obj->save();

                            // create thumbnail of image

                            Image::thumbnail($destPath.$value, 90,90)
                            ->save($destPath.'thumb_'.$value, ['quality' => 80]);

                            if(file_exists($srcPath.$value))
                                unlink($srcPath.$value);
                        }   
                    }
                    $this->removeDirectory($srcPath);
                }

                // *************** Save Open Hours Days Model *************** //

                $destination_hours_days_model = DestinationsOpenHoursDays::deleteAll(['provider_id' => $provider_model->id]);

                $errors_array = [];
                
                foreach ($open_hours['hours'] as $key => $value)
                {
                    for ($i=0; $i < count($value['opening']) ; $i++) 
                    {
                        $destination_hours_days_model = new DestinationsOpenHoursDays();

                        $destination_hours_days_model->provider_id = $provider_model->id;
                        $destination_hours_days_model->open = $value['state'];
                        $destination_hours_days_model->hours_24 = $value['hours_24'];

                        switch ($key) 
                        {
                            case 'monday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Monday;
                               break;

                            case 'tuesday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Tuesday;
                               break;

                            case 'wednesday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Wednesday;
                               break;

                            case 'thursday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Thursday;
                               break;

                            case 'friday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Friday;
                               break;

                            case 'saturday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Saturday;
                               break;

                            case 'sunday':
                               $destination_hours_days_model->day = DestinationsOpenHoursDays::Sunday;
                               break;
                        }

                        $destination_hours_days_model->opening = $value['opening'][$i];
                        $destination_hours_days_model->closing = $value['closing'][$i];

                        $destination_hours_days_model->save();
                        $errors_array[] = $destination_hours_days_model->getErrors();
                    }
                }

                // *************** Save Exceptions Models ***************//

                $temp_date = '';

                if(!empty($open_hours['exceptions']['date_from']) && !empty($open_hours['exceptions']['date_to']))
                {
                    $open_hours['exceptions']['date_from'] = str_replace('/', '-', $open_hours['exceptions']['date_from']);
                    $temp_date = date('Y-m-d',strtotime($open_hours['exceptions']['date_from']));

                    $open_hours['exceptions']['date_to'] = str_replace('/', '-', $open_hours['exceptions']['date_to']);
                    $date2 = date('Y-m-d',strtotime($open_hours['exceptions']['date_to']));

                    $date = $temp_date;

                    $date1=date_create($temp_date);
                    $date2=date_create($date2);

                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a")+1;

                    for ($i=0; $i < $difference ; $i++) 
                    {
                        $temp_date = '';

                        if($i==0)
                        {
                            $temp_date = $date;
                        }
                        else
                        {
                            $date = strtotime("+1 day", strtotime($date));
                            $temp_date=  date('Y-m-d',$date);
                            $date = date("Y-m-d", $date);
                        }

                        $prev_model = DestinationsOpenHoursExceptions::find()->where(['LIKE', 'date', $temp_date]) 
                                                                     ->andWhere(['=','provider_id', $provider_model->id])
                                                                     ->all();

                        if(!empty($prev_model))
                        {
                            foreach ($prev_model as $key => $value) 
                            {
                                $value->delete();
                            }
                        }

                        for ($j=0; $j < count($open_hours['exceptions']['opening']) ; $j++) 
                        {
                            $destination_hours_exceptions_model = new DestinationsOpenHoursExceptions();

                            $destination_hours_exceptions_model->opening = $open_hours['exceptions']['opening'][$j];
                            $destination_hours_exceptions_model->closing = $open_hours['exceptions']['closing'][$j];
                            $destination_hours_exceptions_model->state = $open_hours['exceptions']['state'];
                            $destination_hours_exceptions_model->hours_24 = $open_hours['exceptions']['hours_24'];
                            $destination_hours_exceptions_model->provider_id = $provider_model->id;
                            $destination_hours_exceptions_model->date = $temp_date;

                            $destination_hours_exceptions_model->save();

                        }
                    
                    }
                }

                // *************** Save Open All Year Model ***************//

                $destination_open_all_year->delete();

                $destination_open_all_year = new DestinationsOpenAllYear();
                $destination_open_all_year->provider_id = $provider_model->id;
                $destination_open_all_year->selected_months = $post['DestinationsOpenAllYear']['selected_months'];
                
                if(!empty($destination_open_all_year->selected_months))
                {
                    foreach ($destination_open_all_year->selected_months as $key => $value) 
                    {
                        switch ($value) 
                        {
                            case 1:
                                $destination_open_all_year->jan = 1;
                                break;
                            case 2:
                                $destination_open_all_year->feb = 1;
                                break;
                            case 3:
                                $destination_open_all_year->march = 1;
                                break;
                            case 4:
                                $destination_open_all_year->april = 1;
                                break;
                            case 5:
                                $destination_open_all_year->may = 1;
                                break;
                            case 6:
                                $destination_open_all_year->june = 1;
                                break;
                            case 7:
                                $destination_open_all_year->july = 1;
                                break;
                            case 8:
                                $destination_open_all_year->august = 1;
                                break;
                            case 9:
                                $destination_open_all_year->sep = 1;
                                break;
                            case 10:
                                $destination_open_all_year->oct = 1;
                                break;
                            case 11:
                                $destination_open_all_year->nov = 1;
                                break;
                            case 12:
                                $destination_open_all_year->dec = 1;
                                break;
                        }
                    }
                }
                $destination_open_all_year->save();

                if($provider_model->provider_type_id == 6)
                {

                    if(!empty($_POST['Destinations']['resturant_tags']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 3])->all();
                        foreach ($rest_tags as $value) 
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['resturant_tags'] as $resturant_tag) 
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 3;
                            $tag->save();  
                        }
                    }
                    if(!empty($_POST['Destinations']['dish']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 4])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['dish'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 4;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['type']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 5])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['type'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 5;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['dietary_restriction']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 6])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['dietary_restriction'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 6;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['meal']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 7])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['meal'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 7;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['price']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 8])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['price'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 8;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['feature']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 9])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['feature'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 9;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['alcohol']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 10])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['alcohol'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 10;
                            $tag->save();
                        }
                    }
                    if(!empty($_POST['Destinations']['good_for']))
                    {
                        $rest_tags = DestinationsTags::find()->where(['provider_id'=> $provider_model->id, 'tag_type' => 11])->all();
                        foreach ($rest_tags as $value)
                        {
                            $value->delete();
                        }
                        foreach ($_POST['Destinations']['good_for'] as $resturant_tag)
                        {
                            $tag = new DestinationsTags();
                            $tag->provider_id = $provider_model->id;
                            $tag->tag_id = $resturant_tag;
                            $tag->tag_type = 11;
                            $tag->save();
                        }
                    }
                }

                Yii::$app->session->setFlash('success', 'Destination info is updated successfully');
            }

            $errors['provider'] = $provider_model->getErrors();

            /*echo '<pre>';
            print_r($errors);
            exit;*/

            //return $this->redirect(['update','id' => $id, 'tab_href' => $tab_href]);
            if($_POST['check'] == '1')
            {
                return $this->redirect(['index']);
            }
            else
            {
                return $this->redirect(['update', 'id' => $provider_model->id]);    
            }
            
        } 
        else 
        {
            $provider_financial_model->vat_rate = str_replace('.', '', $provider_financial_model->vat_rate);

            // ********** Open Hours Tab
            $destination_hours_days_model = DestinationsOpenHoursDays::find()->where(['provider_id' => $provider_model->id])->all();

            $provider_model->destination_title = $provider_model->getDestinationTypeName();

            foreach ($destination_hours_days_model as $key => $value) 
            {
                $day_name = DestinationsOpenHoursDays::$days_arr[$value->day];
                $day_name = strtolower($day_name);

                $open_hours['hours'][$day_name]['opening'][] = date('H:i',strtotime($value->opening));
                $open_hours['hours'][$day_name]['closing'][] = date('H:i',strtotime($value->closing));

                $open_hours['hours'][$day_name]['state'] = $value->open;
                $open_hours['hours'][$day_name]['hours_24'] = $value->hours_24;
            }

            $destination_hours_days_model = new DestinationsOpenHoursDays();

            if($provider_model->background_color=='transparent')
                $provider_model->transparent_background = 1;

            // ********************** Populate Selected Months ********************** //

            $arr_month = ['jan','feb','march','april','may','june','july','august','sep','oct','nov','dec'];
            $count = 1;

            foreach ($arr_month as $key => $value) 
            {
                if($destination_open_all_year->$value)
                    $destination_open_all_year->selected_months[] = $count;
                $count++;
            }
            

            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
                unset($_SESSION['images']);

            return $this->render('update', [
                'provider_model' => $provider_model,
                'booking_policies_model' => $booking_policies_model,
                'provider_financial_model' => $provider_financial_model,
                'booking_rules_model' => $booking_rules_model,
                'prev_destination_type' => $prev_destination_type,
                'open_hours' => $open_hours,
                'meal_options' => $meal_options,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'searchModelException' => $searchModelException,
                'dataProviderException' => $dataProviderException,
                'destination_open_all_year' => $destination_open_all_year,
                'tab_href' => $tab_href,
                'travel_partners' => $travel_partners_arr,
                'destination_add_ons' => $add_on_arr,
                'amenities' => $amenities,
            ]);
        }
    }

    public function actionDeleteImage()
    {
        $image = DestinationsImages::findOne(['id'=>$_POST['image_id']]);

        if(!empty($image))
        {
            $Path = Yii::getAlias('@app').'/../uploads/Destinations/'.$image->provider_id.'/images/'.$image->image;
            if(file_exists($Path))
                unlink($Path);
            
            $thumbnail= Yii::getAlias('@app').'/../uploads/Destinations/'.$image->provider_id.'/images/thumb_'.$image->image;
            if(file_exists($thumbnail))
                unlink($thumbnail);
            $image->delete();
        }
    }

    public function actionDeleteException()
    {
        $model = DestinationsOpenHoursExceptions::findOne(['id'=>$_POST['id']]);
        $model->delete();
    }

    /**
     * Deletes an existing Destinations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $path = Yii::getAlias('@app'). '/../uploads/Destinations/'.$id;
        if(is_dir($path))
        {
            $this->removeDirectory($path);
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteAll()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        if (isset($_POST['keylist'])) {
            $records = $_POST['keylist']; 
            foreach ($records as $singleRecord) {
                $exception = DestinationsOpenHoursExceptions::findOne(['id' => $singleRecord]);
                if(!empty($exception))
                {
                    $exception->delete();
                }
            }
        }
        return true;
    }

    /**
     * Finds the Providers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Providers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Destinations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListstates($country_id){
        $states = States::find()
                ->where(['country_id' => $country_id])
                ->all();
        echo "<option disabled> - Select a State - </option>";
        foreach ($states as $state) {
            echo "<option value='".$state->id."'>".$state->name."</option>";  
        }        
    }

    public function actionListstatesGrid($country_id){
        $states = States::find()
                ->where(['country_id' => $country_id])
                ->all();
        echo "<option> - Select a State - </option>";
        foreach ($states as $state) {
            echo "<option value='".$state->id."'>".$state->name."</option>";  
        }        
    }

    public function actionListcities($state_id){
        $cities = Cities::find()
                ->where(['state_id' => $state_id])
                ->all();

        if(empty($cities))
        {
            echo "<option disabled> - No City Available - </option>";
        }
        else
        {
            echo "<option disabled> - Select a City - </option>";
            foreach ($cities as $city) {
                echo "<option value='".$city->id."'>".$city->name."</option>";  
            }   
        }      
    }

    public function actionListitems($provider_id)
    {
        $items = BookableItems::find()
                ->where(['provider_id' => $provider_id])
                ->all();

        if(!empty($items))
        {
            foreach ($items as $item) 
            {
                echo "<option value='".$item->id."'>".$item->itemType->name."</option>";  
            }  
        }      
    }

    public function removeLeadingZero($value)
    {
        if($value!='' && preg_match("/^0+$/", $value))
        {
            $value = '0';
        } 
        else 
        {
            $value = ltrim($value,'0');
        }
        return $value;
    }
    public function actionAddTravelPartner()
    {
            // echo "<pre>";
            // print_r($_POST);
            // exit();
        $destination_travel_partner = DestinationTravelPartners::findOne(['destination_id' => $_POST['destination_id'], 'travel_partner_id' =>$_POST['travel_partner_id']]);
        if(empty($destination_travel_partner))
        {
            $destination_travel_partner = new DestinationTravelPartners();
        }
        $destination_travel_partner->destination_id = $_POST['destination_id'];
        $destination_travel_partner->travel_partner_id = $_POST['travel_partner_id'];
        $destination_travel_partner->comission = $_POST['comission'];
        $destination_travel_partner->booking_email_address = $_POST['booking_email'];
        $destination_travel_partner->invoice_email_address = $_POST['invoice_email'];
        $destination_travel_partner->primary_contact = $_POST['primary_contact'];
        $destination_travel_partner->billing_contact = $_POST['billing_contact'];
        $destination_travel_partner->notes = $_POST['notes'];
        
        if(!$destination_travel_partner->save())
        {
            echo "<pre>";
            print_r($destination_travel_partner->errors);
            exit();
        }
    }
}

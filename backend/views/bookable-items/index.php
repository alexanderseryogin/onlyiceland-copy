<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

use common\models\BookableItemTypes;
use common\models\Destinations;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookableItemsSearch */
/* @var $dataDestination yii\data\ActiveDataDestination */

$this->title = Yii::t('app', 'Bookable Items');
$this->params['breadcrumbs'][] = $this->title;

$update_page_url = Url::to(['/bookable-items/update']);
?>

<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>

<div class="bookable-items-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BOOKABLE ITEM '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'bookable-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-scrollable'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'provider_id',
                'label' => 'Destination Name',
                'value' => function($data)
                {
                    if(isset($data->destination->name) && !empty($data->destination->name))
                        return $data->destination->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'BookableItemsSearch[provider_id]', 
                    $searchModel['provider_id'], 
                    ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Destination', 
                        'id'=>'provider_dropdown', 
                        'class'=>'form-control'
                    ]),
                'headerOptions' =>['style' => 'min-width:150px;'],
                'contentOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'item_type_id',
                'label' => 'Item Types',
                'value' => function($data)
                {
                    if(isset($data->itemType->name) && !empty($data->itemType->name))
                        return $data->itemType->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'BookableItemsSearch[item_type_id]', 
                    $searchModel['item_type_id'], 
                    ArrayHelper::map(BookableItemTypes::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'item_type_dropdown', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' => function($data)
                {
                    return ['style' => 'min-width:150px;color:'.$data->text_color.'; background:'.$data->background_color.';' ];
                },
                'headerOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'item_quantity',
                'label' => 'QTY',
                'value' => function($data)
                {
                    return $data->item_quantity;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'max_adults',
                'label' => 'AD',
                'value' => function($data)
                {
                    return $data->max_adults;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'max_children',
                'label' => 'CH',
                'value' => function($data)
                {
                    return $data->max_children;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'max_extra_beds',
                'label' => 'EB',
                'value' => function($data)
                {
                    return $data->max_extra_beds;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'max_baby_beds',
                'label' => 'BB',
                'value' => function($data)
                {
                    return $data->max_baby_beds;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'min_age_participation',
                'label' => 'MIN',
                'value' => function($data)
                {
                    return $data->min_age_participation;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'max_age_participation',
                'label' => 'MAX',
                'value' => function($data)
                {
                    return $data->max_age_participation;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            [
                'attribute' => 'max_free_age',
                'label' => 'FR',
                'value' => function($data)
                {
                    return $data->max_free_age;
                },
                'headerOptions' =>['style' => 'text-align:center;'],
                'contentOptions' =>['style' => 'text-align:right;'],
            ],
            'include_in_groups',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' =>['style' => 'min-width:70px;'],
                'contentOptions' =>['style' => 'min-width:70px;'],
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#item_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#bookable-gridview').on('pjax:end', function() {

        $('#item_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
        });

        $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
        });
    });
    $(document).on('click','.table-striped tbody tr',function()
    {
        window.location.replace('$update_page_url?id='+$(this).attr('data-key'));
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>

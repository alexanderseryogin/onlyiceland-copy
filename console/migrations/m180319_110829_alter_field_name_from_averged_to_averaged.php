<?php

use yii\db\Migration;

class m180319_110829_alter_field_name_from_averged_to_averaged extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          CHANGE `averged_total` `averaged_total` FLOAT NULL;
                        ');
    }

    public function safeDown()
    {
        echo "m180319_110829_alter_field_name_from_averged_to_averaged cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_110829_alter_field_name_from_averged_to_averaged cannot be reverted.\n";

        return false;
    }
    */
}

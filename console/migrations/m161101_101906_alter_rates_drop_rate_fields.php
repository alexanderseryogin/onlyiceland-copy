<?php

use yii\db\Migration;

class m161101_101906_alter_rates_drop_rate_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates` DROP COLUMN `first_night_rate`, DROP COLUMN `last_night_rate`; 
        ");
    }

    public function down()
    {
        echo "m161101_101906_alter_rates_drop_rate_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

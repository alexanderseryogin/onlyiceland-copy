<?php

use yii\db\Migration;

class m170721_074901_alter_table_bookings_dates_add_field_housekeeping_status extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_dates`   
                          ADD COLUMN `housekeeping_status_id` INT(11) NULL AFTER `status_id`,
                          ADD CONSTRAINT `FK_booking_dates_housekeeping_status_id` FOREIGN KEY (`housekeeping_status_id`) REFERENCES 
                          `housekeeping_status`(`id`) ON UPDATE SET NULL ON DELETE CASCADE;
                        ');
    }

    public function down()
    {
        echo "m170721_074901_alter_table_bookings_dates_add_field_housekeeping_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\BookingDates;
/* @var $this yii\web\View */
/* @var $model common\models\BookingsItems */

$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if($model->reference_no != null)
{
    $this->params['breadcrumbs'][] = $model->reference_no;
}
$item = $model->item;

$first_booking = BookingDates::findOne(['booking_item_id' => $model->id]);
$first_booking_date = $first_booking->date;
?>





<div class="bookable-items-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Update Booking Item</span>
                        </div>
                        <ul class="nav nav-tabs booking_tabs">
                            <li class="active">
                                <a href="#details" data-toggle="tab">Details</a>
                            </li>
                            <li >
                                <a href="#charges" data-toggle="tab">Charges & Payments</a>
                            </li>
                            <li >
                                <a href="#logs" data-toggle="tab">Logs</a>
                            </li>
                            <!-- <li>
                                <a href="#details" data-toggle="tab">Details</a>
                            </li> -->
      

                        </ul>
                    </div>

                    <div class="portlet-body">
                    	<div class="portlet-body destination_portlet">
	                        <div class="tab-content">
		                    	<div class="tab-pane active" id="details">
			                        <?= $this->render('update_form', [
								    	'model' => $model,
								    	'viewDetailsRecord' => $viewDetailsRecord,
								    	'destination_items' => $destination_items,
								    	'check'             => $check,
                                        'pageNo' => $pageNo
								    ]) ?>
								</div>
								<div class="tab-pane" id="charges">
			                        <?= $this->render('payment-charges', [
								    	'model' => $model,
								    	'viewDetailsRecord' => $viewDetailsRecord,
								    	'destination_items' => $destination_items,
								    	'check'             => $check
								    ]) ?>
								</div>
                                <div class="tab-pane" id="logs">
                                    <?= $this->render('booking_logs', [
                                        'model' => $model,
                                        'viewDetailsRecord' => $viewDetailsRecord,
                                        'destination_items' => $destination_items,
                                        'check'             => $check
                                    ]) ?>
                                </div>
							</div>
						</div>
                         <button type="button" data="<?php echo $item->id?>"  class=" btn btn-primary save-item update-item-<?php echo $item->id?>">Update</button>
                            <button type="button" data="<?php echo $item->id?>"  class=" btn btn-success update-item update-item-<?php echo $item->id?>">Save</button>
                            <?php if($check == 'cal') {?>
                                <a class="btn btn-default" href="<?= Url::to(['/bookings/calendar']) ?>" >Cancel</a>
                            <?php }elseif($check == 'sch'){?>
                                <a class="btn btn-default" href="<?= Url::to(['/bookings/booking-issues','destination_id' => $model->provider_id,'exp_date' => $first_booking_date]) ?>" >Cancel</a>
                            <?php }elseif($check == 'override_status_sch'){?>
                                <a class="btn btn-default" href="<?= Url::to(['/booking-override-statuses/override','destination_id' => $model->provider_id,'exp_date' => $first_booking_date]) ?>" >Cancel</a>
                            <?php }else{?>
                                <a class="btn btn-default" href="<?= Url::to(['/bookings/index','pageNo' => $pageNo]) ?>" >Cancel</a>
                            <?php } ?>
                          <a href="<?= yii::$app->getUrlManager()->createUrl(['bookings/delete', 'id' => $model->id,'pageNo' => $pageNo])?>" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="text" name="Bookings[tab_href]" id="tab_href" hidden="">

<!--  -->
<?php
$this->registerJs("

");
if(!empty($tab_href))
{
    $this->registerJs("
        $('.booking_tabs li').each(function()
        {
            var href_val = $(this).find('a').attr('href');
            var tab_activate = '$tab_href';
            $('#tab_href').val(tab_activate);
            //console.log($('#tab_href').val(tab_activate));
            if(href_val==tab_activate)
            {
                $(this).addClass('active');
                $(this).find('a').attr('aria-expanded','true');
                $(tab_activate).addClass('active');
            }
            else
            {
                $(this).removeClass('active');
                $(this).find('a').attr('aria-expanded','false');
                $(href_val).removeClass('active');
            }
        });

    ");
}
else
{
    $this->registerJs("
        $('.booking_tabs li').each(function()
        {
            var href_val = $(this).find('a').attr('href');
            //var tab_activate = '$tab_href';
            
            // console.log($('#tab_href').val(tab_activate));
            if($(this).hasClass('active'))
            {
                $('#tab_href').val(href_val);
            }
            console.log($('#tab_href').val());
        });

    ");
}


?>
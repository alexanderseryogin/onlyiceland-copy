<?php

use yii\db\Migration;

class m170721_122625_alter_table_housekepping_status_add_field_icon extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `housekeeping_status`   
                          ADD COLUMN `icon` TEXT NULL AFTER `label`;
                        ');
    }

    public function down()
    {
        echo "m170721_122625_alter_table_housekepping_status_add_field_icon cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\EstimatedArrivalTimes;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model common\models\EstimatedArrivalTimes */
/* @var $form yii\widgets\ActiveForm */
$Checkbox = [
    0 => '',
    1 => 'checked'
];

?>

<style type="text/css">
    .show-field, .show-time-div{ display: block; }
    .hide-field, .hide-time-div{ display: none; }
</style>

<div class="estimated-arrival-times-form">
	<div class="portlet light ">

	    <?php $form = ActiveForm::begin(); ?>

	    <div class="row">
	    	<div class="col-sm-3">
	    		<?= $form->field($model, 'time_group', [
                        'inputOptions' => [
                                        'id' => 'time_group_dropdown',
                                        'class' => 'form-control',
                                        'style' => 'width: 100%'
                                        ]
                ])->dropdownList(EstimatedArrivalTimes::$time_groups,['prompt'=>'Select a Time Group']) ?>
	    	</div>
	    	<div class="col-sm-9">
	    		<div class="col-sm-4 hide-field" id="text_div">
		    		<?= $form->field($model, 'text', [
	                        'inputOptions' => [
	                                        'id' => 'text_dropdown',
	                                        'class' => 'form-control',
	                                        'style' => 'width: 100%'
	                                        ]
	                ])->dropdownList(EstimatedArrivalTimes::$text,['prompt'=>'Select a Text']) ?>
		    	</div>
		    	<div class="col-sm-4 show-field" id="start_time_div">
		    		<div><label class="control-label">Start Time</label></div>
		    		<div class="form-group">
	                    <div class="input-icon">
	                        <i class="fa fa-clock-o"></i>
	                        <input type="text" id="start_time" value="<?= $model->start_time ?>" name="EstimatedArrivalTimes[start_time]" class="form-control" required> 
	                    </div>
	                </div>
		    	</div>
		    	<div class="col-sm-4">
		    		<div><label class="control-label">End Time</label></div>
		    		<div class="form-group">
	                    <div class="input-icon">
	                        <i class="fa fa-clock-o"></i>
	                        <input type="text" id="end_time" value="<?= $model->end_time ?>" name="EstimatedArrivalTimes[end_time]" class="form-control" required> 
	                    </div>
	                </div>
		    	</div>
		    	<div class="col-sm-4">
		    		Add Text in Start Time
		    		<div style="margin-top:15px;">
		    			<input type="checkbox" id="text_switch" name="EstimatedArrivalTimes[text_switch]" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size = "mini" <?php echo $Checkbox[$model->text_switch]  ?>>
		    		</div>
		    	</div>
	    	</div>
	    </div>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	        <a class="btn btn-default" href="<?= Url::to(['/estimated-arrival-times']) ?>" >Cancel</a>
	    </div>

	    <?php ActiveForm::end(); ?>
    </div>
</div>

<?php

if($model->time_group >= '0')
{
	$this->registerJs("
		$('.hide-time-div').addClass('show-time-div');
        $('.show-time-div').removeClass('hide-time-div');
    ");
}

if($model->text_switch)
{
	$this->registerJs("
		$('#text_div').removeClass('hide-field');
        $('#text_div').addClass('show-field');

        $('#start_time_div').removeClass('show-field');
        $('#start_time_div').addClass('hide-field');
    ");
}

$this->registerJs("
jQuery(document).ready(function() {

    $('#time_group_dropdown').select2({
        placeholder: \"Select a Time Group\",
    });

    $('#text_dropdown').select2({
        placeholder: \"Select a Time Group\",
    });

    $('#start_time').clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });   
 
    $('#start_time').click(function(e){   
        $(this).clockface('toggle');
    });

    $('#end_time').clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });   
 
    $('#end_time').click(function(e){   
        $(this).clockface('toggle');
    });

    $('#time_group_dropdown').change(function()
    {
    	/*
        $('.hide-time-div').addClass('show-time-div');
        $('.show-time-div').removeClass('hide-time-div');

        var time = $('#start_time').val();
        time = time.split(':');

        var hour = time[0];
        var minute = time[1];

        //alert(hour+':'+minute);

        switch ($('#time_group_dropdown').val()) 
    	{
    		case '0':
    				$('#start_time').val('00:00');
    				$('#end_time').val('01:00');
    			break;
    		case '1':
    				$('#start_time').val('00:00');
    				$('#end_time').val('02:00');
    			break;
    		case '2':
    				$('#start_time').val('00:00');
    				$('#end_time').val('04:00');
    			break;
    	}*/
    });

    // **************** Add logic to show time in end time according to time group ****************** //

    /*$('#start_time').on('hidden.clockface', function(e, data) 
    {
    	var hour = data.hour;
    	var minute = data.minute;

    	switch ($('#time_group_dropdown').val()) 
    	{
    		case '0':
    			hour += 1;
    			break;
    		case '1':
    			hour += 2;
    			break;
    		case '2':
    			hour += 4;
    			break;
    	}

    	if(hour>23)
    	{
    		hour = hour-24;
    	}
    	if(hour>=0 && hour<=9)
    	{
    		hour = '0'+hour;
    	}

    	if(minute>=0 && minute<=9)
    		minute = '0'+minute;

	  	$('#end_time').val(hour+':'+minute);
	});*/

    $('#text_switch').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            $('#text_div').removeClass('hide-field');
            $('#text_div').addClass('show-field');

            $('#start_time_div').removeClass('show-field');
            $('#start_time_div').addClass('hide-field');
        }
        else
        {
            $('#text_div').removeClass('show-field');
            $('#text_div').addClass('hide-field');

            $('#start_time_div').removeClass('hide-field');
            $('#start_time_div').addClass('show-field');
        }
    });

});",View::POS_END);
?>

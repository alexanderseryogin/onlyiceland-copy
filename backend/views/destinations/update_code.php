<?php
    
    if($provider_model->provider_type_id != $provider_model->getAccomodation())
            {
                $provider_model->accommodation_id = '';
            }

            $provider_model->phone = $provider_model->server_phone;
            $provider_model->fax = $provider_model->server_fax;

            $provider_model->save();

            $uploadFile = new UploadFile();

            if ($uploadFile->FileToUpload = UploadedFile::getInstance($provider_model,'image')) 
            {
                if($uploadFile->uploadFile('Destinations',$provider_model->id,'images'))
                {
                    if(!empty($provider_model->featured_image))
                    {
                        $prev_image = Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images/'.$provider_model->featured_image;
                        unlink($prev_image);
                    }

                    $provider_model->featured_image = $uploadFile->FileName;
                    $provider_model->update();
                }
            }


            if(!empty($get_staff))
            {
                foreach ($get_staff as $key => $value) 
                {
                    $value->delete();
                }
            }

            $provider_model->providers_staff = $_POST['Destinations']['providers_staff'];

            if(!empty($provider_model->providers_staff))
            {
                foreach ($provider_model->providers_staff as $key => $value) 
                {
                    $staff = new DestinationsStaff();
                    $staff->provider_id = $provider_model->id;
                    $staff->staff_id = $value;
                    $staff->save(); 
                }
            }

            if(!empty($get_tags))
            {
                foreach ($get_tags as $key => $value) 
                {
                    $value->delete();
                }
            }

            $provider_model->providers_tags = $_POST['Destinations']['providers_tags'];

            if(!empty($provider_model->providers_tags))
            {
                foreach ($provider_model->providers_tags as $key => $value) 
                {
                    $tag = new DestinationsTags();
                    $tag->provider_id = $provider_model->id;
                    $tag->tag_id = $value;
                    $tag->save();
                }
            }

            if(!empty($get_requests))
            {
                foreach ($get_requests as $key => $value) 
                {
                    $value->delete();
                }
            }

            $provider_model->special_requests = $_POST['Destinations']['special_requests'];

            if(!empty($provider_model->special_requests))
            {
                foreach ($provider_model->special_requests as $key => $value) 
                {
                    $request = new DestinationsSpecialRequests();
                    $request->provider_id = $provider_model->id;
                    $request->request_id = $value;
                    $request->save(); 
                }
            }

            if($booking_policies_model->load(Yii::$app->request->post()))
            {
                if(!empty($get_guarantee))
                {
                    foreach ($get_guarantee as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($get_checkin))
                {
                    foreach ($get_checkin as $key => $value) 
                    {
                        $value->delete();
                    }
                }

                if(!empty($booking_policies_model->payment_methods_gauarantee))
                {
                    foreach ($booking_policies_model->payment_methods_gauarantee as $key => $value) 
                    {
                        $guarantee = new PaymentMethodGuarantee();
                        $guarantee->provider_id = $provider_model->id;
                        $guarantee->pm_id = $value;
                        $guarantee->save();
                    }
                }

                if(!empty($booking_policies_model->payment_methods_checkin))
                {
                    foreach ($booking_policies_model->payment_methods_checkin as $key => $value) 
                    {
                        $checkin = new PaymentMethodCheckin();
                        $checkin->provider_id = $provider_model->id;
                        $checkin->pm_id = $value;
                        $checkin->save();
                    }
                }

                // add condition for check in and check out time

                $in_from =  strtotime($booking_policies_model->checkin_time_from);
                $in_to =  strtotime($booking_policies_model->checkin_time_to);

                $out_from =  strtotime($booking_policies_model->checkout_time_from);
                $out_to =  strtotime($booking_policies_model->checkout_time_to);


                $flag = 0;

                if($in_from >= $in_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Check-in From Time must be earlier than Check-in To Time.');
                }

                if($out_from >= $out_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Checkout From Time must be earlier than Checkout To Time.');
                }

                if($in_from >= $in_to && $out_from >= $out_to)
                {
                    $flag=1;
                    Yii::$app->session->setFlash('error', 'Check-in From Time must be earlier than Check-in To Time. Checkout From Time must be earlier than Checkout To Time');
                }

                if($flag)
                {
                    return $this->render('update', [
                        'provider_model' => $provider_model,
                        'booking_policies_model' => $booking_policies_model,
                        'provider_financial_model' => $provider_financial_model,
                        'booking_rules_model' => $booking_rules_model,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                }

                $booking_policies_model->payment_methods_gauarantee ='';
                $booking_policies_model->payment_methods_checkin='';

                $booking_policies_model->provider_id = $provider_model->id;
                $booking_policies_model->save();
                $errors['policies'] = $booking_policies_model->getErrors();
            }

            if($booking_rules_model->load(Yii::$app->request->post()))
            {
                $booking_rules_model->provider_id = $provider_model->id;

                $booking_rules_model->ebp_first_night_exception_period = strtotime($booking_rules_model->ebp_first_night_exception_period);

                $booking_rules_model->ebp_last_night_exception_period = strtotime($booking_rules_model->ebp_last_night_exception_period);

                $booking_rules_model->save();

                $errors['rules'] = $booking_rules_model->getErrors();
            }

            if($provider_financial_model->load(Yii::$app->request->post()))
            {
                $provider_financial_model->provider_id = $provider_model->id;
                $provider_financial_model->license_expiration_date = strtotime($provider_financial_model->license_expiration_date);

                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($provider_financial_model,'pdfFile')) 
                {
                    if($uploadFile->uploadFile('Destinations',$provider_model->id,'licenses'))
                    {
                        $provider_financial_model->license_image = $uploadFile->FileName;

                        $saved_financial_model = DestinationsFinancial::findOne(['provider_id' => $provider_model->id]);

                        if(!empty($saved_financial_model->license_image))
                        {
                            $path = Yii::getAlias('@webroot') . '/../uploads/Destinations/'.$provider_model->id.'/licenses';
                            $file = $path . '/'.$saved_financial_model->license_image;
                            unlink($file);
                        }
                    }
                }
                
                $provider_financial_model->save();

                $errors['financial'] = $provider_financial_model->getErrors();
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id, 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images'))
            {
                 mkdir(Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images', 0777, true);
            }

            $srcPath = Yii::getAlias('@app'). '/../uploads/Destinations/temp/images/';
            $destPath = Yii::getAlias('@app').'/../uploads/Destinations/'.$provider_model->id.'/images/';

            if(isset($_SESSION['images']) && !empty($_SESSION['images']))
            {
                foreach ($_SESSION['images'] as $key => $value) 
                {
                    if(copy($srcPath.$value, $destPath.$value))
                    {
                        $obj = new DestinationsImages();
                        $obj->provider_id = $provider_model->id;
                        $obj->image = $value;

                        $last_model = DestinationsImages::find()
                                                    ->where(['provider_id' => $provider_model->id])
                                                    ->orderBy('display_order DESC')
                                                    ->one();
                        if(!empty($last_model))
                        {
                            $obj->display_order = $last_model->display_order+1;
                        }
                        else
                        {
                            $obj->display_order = 1;
                        }

                        $obj->save();

                        unlink($srcPath.$value);
                    }   
                }
                $this->removeDirectory($srcPath);
            }

            Yii::$app->session->setFlash('success', 'Destination info is updated successfully');
            
            return $this->redirect(['update','id' => $id]);

?>
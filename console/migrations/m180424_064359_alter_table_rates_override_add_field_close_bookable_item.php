<?php

use yii\db\Migration;

class m180424_064359_alter_table_rates_override_add_field_close_bookable_item extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `rates_override`   
                          ADD COLUMN `bookable_item_closed` INT(11) NULL AFTER `item_price_additional_child`;
                        ');
    }

    public function safeDown()
    {
        echo "m180424_064359_alter_table_rates_override_add_field_close_bookable_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180424_064359_alter_table_rates_override_add_field_close_bookable_item cannot be reverted.\n";

        return false;
    }
    */
}

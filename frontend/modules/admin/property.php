<?php

namespace frontend\modules\admin;

use Yii;

/**
 * property-admin module definition class
 */
class property extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // setting metronic admin 4 theme 
        Yii::$app->view->theme = new \yii\base\Theme([
            'baseUrl' => '@metronic/resources',
            //'basePath' => '@app/themes/basic',
            'pathMap' => [
                '@frontend/views' => '@metronic/views/providerend'
            ]
        ]);

    }
}

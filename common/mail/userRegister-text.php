<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
use common\models\User;

if ($user->type != User::USER_TYPE_ADMIN)
{
	$loginLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/login', 'ulg'=>true]);
}
else
{
	$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login', 'ulg'=>true]);
}
?>
Hello <?= $user->username ?>,

Your password is <?= $password ?>

Your account has been created, follow the link below to login:

<?= $loginLink ?>

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates".
 *
 * @property integer $id
 * @property string $name
 * @property integer $provider_id
 * @property integer $booking_confirmation_type_id
 * @property integer $unit_id
 * @property string $rate_period
 * @property string $item_price_first_adult
 * @property string $item_price_additional_adult
 * @property string $item_price_first_child
 * @property string $item_price_additional_child
 * @property string $first_night_rate
 * @property string $last_night_rate
 * @property string $min_max_booking
 * @property string $rate_allowed
 * @property string $check_in_allowed
 * @property string $check_out_allowed
 * @property string $min_max_stay
 * @property integer $booking_status_id
 * @property integer $booking_flag_id
 *
 * @property BookingConfirmationTypes $bookingConfirmationType
 * @property BookingTypes $bookingFlag
 * @property BookingStatuses $bookingStatus
 * @property Providers $provider
 * @property BookableItems $unit
 */
class Rates extends \yii\db\ActiveRecord
{
    public static $week_days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    public static $discount_codes=[];
    public static $upsell_items=[];
    public $discount_code;
    public $upsell_checkbox;
    public $upsell_pricing;
    public $multi_night;
    
    public $first_night_rate='';
    public $last_night_rate='';

    public $amenities;
    public $amenities_banners;

    public $server_min_price;
    public $server_max_price;
    public $server_item_price_first_adult;
    public $server_item_price_first_child;
    public $server_item_price_additional_adult;
    public $server_item_price_additional_child;

    public $rates_capacity_pricing = [];

    public $server_lodging_tax_per_night;
    public $server_vat_rate;

    public static $numberWords = ['Single','Double','Triple','Quad'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id','name', 'booking_confirmation_type_id', 'unit_id','booking_status_id', 'booking_flag_id', 'rate_allowed', 'check_in_allowed', 'check_out_allowed','min_price','max_price'], 'required'],

            [['provider_id', 'booking_confirmation_type_id', 'unit_id', 'booking_status_id', 'booking_flag_id','published','server_lodging_tax_per_night',], 'integer'],
            [['server_vat_rate','to','from'],'safe'],
            [['first_night_rate', 'last_night_rate','item_price_first_adult', 'item_price_additional_adult', 'item_price_first_child', 'item_price_additional_child', 'min_price','max_price'], 'string'],

            [['min_max_booking', 'rate_allowed', 'check_in_allowed', 'check_out_allowed', 'min_max_stay'], 'string'],
            [['name', 'rate_period','general_booking_cancellation','vat_rate'], 'string', 'max' => 256],


            [['server_item_price_first_adult', 'server_item_price_additional_adult', 'server_item_price_first_child', 'server_item_price_additional_child', 'server_min_price','server_max_price'],'integer'],

            [['booking_confirmation_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['booking_confirmation_type_id' => 'id']],
            [['booking_flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['booking_flag_id' => 'id']],
            [['booking_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['booking_status_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'provider_id' => Yii::t('app', 'Destination Name'),
            'booking_confirmation_type_id' => Yii::t('app', 'Booking Confirmation Type'),
            'unit_id' => Yii::t('app', 'Item Name'),
            'rate_period' => Yii::t('app', 'Rate Period'),
            'item_price_first_adult' => Yii::t('app', 'Item Price First Adult'),
            'item_price_additional_adult' => Yii::t('app', 'Item Price Additional Adult'),
            'item_price_first_child' => Yii::t('app', 'Item Price First Child'),
            'item_price_additional_child' => Yii::t('app', 'Item Price Additional Child'),
            'first_night_rate' => Yii::t('app', 'First Night Rate'),
            'last_night_rate' => Yii::t('app', 'Last Night Rate'),
            'min_max_booking' => Yii::t('app', 'Min Max Booking'),
            'rate_allowed' => Yii::t('app', 'Rate Allowed'),
            'check_in_allowed' => Yii::t('app', 'Check-in Allowed'),
            'check_out_allowed' => Yii::t('app', 'Checkout Allowed'),
            'min_max_stay' => Yii::t('app', 'Min Max Stay'),
            'booking_status_id' => Yii::t('app', 'Booking Status'),
            'booking_flag_id' => Yii::t('app', 'Booking Flag'),
            'discount_code' => Yii::t('app', 'Discount/Voucher Codes Allowed'),
            'published' => Yii::t('app', 'Published'),
            'min_price' => Yii::t('app', 'Min Price'),
            'max_price' => Yii::t('app', 'Max Price'),
            'vat_rate' => Yii::t('app', 'Vat Rate'),
            'lodging_tax_per_night' => Yii::t('app', 'Lodging Tax per Night'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingConfirmationType()
    {
        return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'booking_confirmation_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingFlag()
    {
        return $this->hasOne(BookingTypes::className(), ['id' => 'booking_flag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingStatus()
    {
        return $this->hasOne(BookingStatuses::className(), ['id' => 'booking_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'unit_id']);
    }

    public function getDates()
    {
        return $this->hasMany(RatesDates::className(), ['rate_id' => 'id']);
    }

    public function getBookingsItems() 
    { 
        return $this->hasMany(BookingsItems::className(), ['rates_id' => 'id']); 
    }

    public function getRatesAmenities()
    {
        return $this->hasMany(RatesAmenities::className(), ['rates_id' => 'id']);
    }

    public function getRatesCapacityPricings()
    {
        return $this->hasMany(RatesCapacityPricing::className(), ['rates_id' => 'id']);
    }

    public function getRatesDiscountCodes() 
    { 
        return $this->hasMany(RatesDiscountCode::className(), ['rates_id' => 'id']); 
    } 
 
    public function getRatesMultiNightDiscounts() 
    { 
        return $this->hasMany(RatesMultiNightDiscount::className(), ['rates_id' => 'id']); 
    } 
 
    public function getRatesUpsells() 
    { 
        return $this->hasMany(RatesUpsell::className(), ['rates_id' => 'id']); 
    } 
}

<?php

namespace frontend\modules\admin\controllers;

use Yii;
use common\models\UserProfile;
use common\models\UserProfileSearch;
use frontend\modules\admin\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use frontend\models\ChangePasswordForm;
use frontend\models\ImageUploadForm;
use yii\web\UploadedFile;

/**
 * UserProfileController implements the CRUD actions for UserProfile model.
 */
class UserProfileController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
        ];
    }

    /**
     * Lists all UserProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->view->params['title'] = 'Your Profile';
        $this->view->params['subTitle'] = '  update your info here';

        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        
        //profile is missing, time to create new one
        if(!isset($user->profile)){
            $model = new UserProfile;
            $model->user_id = Yii::$app->user->identity->id;
            $model->save();
        } else
            $model = $user->profile;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Profile Updated Successfully.');
        }

        try {
            $password_model = new ChangePasswordForm(Yii::$app->user->identity->id);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        // if(Yii::$app->request->post() != null)
        // {
        //     var_dump(Yii::$app->request->post());
        //     exit();
        // }
        
        if ($password_model->load(Yii::$app->request->post()) && $password_model->validate() && $password_model->changePassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
        }

        try {
            $image_model = new ImageUploadForm(Yii::$app->user->identity->id);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (Yii::$app->request->isPost) {
            $image_model->imageFile = UploadedFile::getInstance($image_model, 'imageFile');
            if ($image_model->upload()) {
                Yii::$app->session->setFlash('success', 'Profile picture was changed.');
            }
        }

        return $this->render('profile', [
            'model' => $model,
            'password_model'  => $password_model,
            'image_model'   =>$image_model,
        ]);
    }

    /**
     * Displays a single UserProfile model.
     * @param integer $id
     * @return mixed
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new UserProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new UserProfile();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing UserProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Deletes an existing UserProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the UserProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

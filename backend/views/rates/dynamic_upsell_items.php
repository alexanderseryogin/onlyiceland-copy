<?php
    use common\models\DestinationUpsellItems;
    use common\models\UpsellItems;

    $Checkbox = [
        0 => '',
        1 => 'checked'
    ];
    $isChecked = 0;
?>

<?php

if(!empty($provider_id))
{
    $upsell_items = DestinationUpsellItems::find()->where(['provider_id' => $provider_id])->all();

    if(!empty($upsell_items))
    {?>
        <table class="table table-striped table-bordered table-hover order-column" id="rates_upsell_items">
            <thead>
                <tr>
                    <th>Show on Frontend</th>
                    <th>Upsell Items</th>
                    <th>Upsell Type</th>
                    <th>Pricing</th>
                    <th>Vat%</th>
                    <th>Tax</th>
                    <th>Fee</th>
                    <th>Discountable</th>
                    <th>Commissionable</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($upsell_items as $key => $value) 
                    {
                        $price = 0;
                        $vat = 11;
                        $tax = 0;
                        $fee = 0;
                        if(isset($upsell_pricing) && is_array($upsell_pricing) && !empty($upsell_pricing) && array_key_exists($value->upsell_id, $upsell_pricing))
                        {

                            $price = $upsell_pricing[$value->upsell_id]['price'];
                            $vat = $upsell_pricing[$value->upsell_id]['vat'];
                            $tax = $upsell_pricing[$value->upsell_id]['tax'];
                            $fee = $upsell_pricing[$value->upsell_id]['fee'];
                            $discount = $upsell_pricing[$value->upsell_id]['discountable'];
                            $comission = $upsell_pricing[$value->upsell_id]['commissionable'];

                            $isChecked = ($upsell_pricing[$value->upsell_id]['show_on_frontend']==1)?1:0;
                        }

                        echo '<tr>';
                        echo '<td class="align-center"><label><input name="Rates[upsell_pricing]['.$value->upsell_id.'][show_on_frontend]" '.$Checkbox[$isChecked].' value="'.$value->id.'" type="checkbox"></label></td>';
                        echo '<td>'.$value->upsell->name.'</td>';
                        echo '<td>'.UpsellItems::$types[$value->upsell->type].'</td>';
                        echo '<td>
                        <input type="text" class="form-control upsell_pricing" value="'.$price.'" style="text-align:right">
                        <input type="text" name="Rates[upsell_pricing]['.$value->upsell_id.'][price]" value="'.$price.'" hidden>
                        </td>';
                        echo '

                        <td>
                            <input type="text" class="form-control rate_upsell_vat " value="'.$vat.'" style="text-align:right">
                            <input type="text" name="Rates[upsell_pricing]['.$value->upsell_id.'][vat]" style="text-align:right" value="'.$vat.'" hidden>
                        </td>

                        <td>
                            
                            <input type="text" class="form-control" name="Rates[upsell_pricing]['.$value->upsell_id.'][tax]" value="'.$tax.'" style="text-align:right">
                        </td>

                        <td>
    
                            <input type="text"  class="form-control" name="Rates[upsell_pricing]['.$value->upsell_id.'][fee]" value="'.$fee.'" style="text-align:right">
                        </td>
                        
                        <td class="align-center">
                            <input name="Rates[upsell_pricing][' . $value->upsell_id . '][discountable]" ' . ($upsell_pricing[$value->upsell_id]['discountable'] == 1 ? 'checked' : '') . ' value="' . $value->id . '" type="checkbox"> 
                        </td>
 
                        <td class="align-center">
                            <input name="Rates[upsell_pricing][' . $value->upsell_id . '][commissionable]" ' . ($upsell_pricing[$value->upsell_id]['commissionable'] == 1 ? 'checked' : '') . ' value="' . $value->id . '" type="checkbox"> 
                        </td>

                        ';
                        echo '</tr>';
                    }
                ?>
            </tbody>
        </table>
<?php
    }
    else
    {
        echo '<span class="label label-danger"> Whoops! </span>
                        <span>&nbsp;  No Upsell Item found </span>';
    }   
}
else
{
    echo '<span class="label label-danger"> Whoops! </span>
                    <span>&nbsp;  No Upsell Item found </span>';
}
?>

<?php

$this->registerJs("
    $('.rate_upsell_vat').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('.rate_upsell_vat').on('change',function()
        {
            //alert($(this).val());
            $(this).siblings('input').val($(this).val());
        });
    ");

?>
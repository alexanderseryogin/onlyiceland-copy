<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_images".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property string $image
 * @property string $description
 * @property integer $display_order
 *
 * @property Providers $provider
 */
class DestinationsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'display_order'], 'integer'],
            [['image'], 'string', 'max' => 256],
            [['description'], 'string', 'max' => 500],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination ID'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'display_order' => Yii::t('app', 'Display Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    public function isFirstRecord()
    {
          $first = DestinationsImages::find()->where(['provider_id' => $this->provider_id])->orderBy('display_order ASC')->one();
          return $this->id == $first->id ? true : false;
    }

    public function isLastRecord()
    {
          $last = DestinationsImages::find()->where(['provider_id' => $this->provider_id])->orderBy('display_order DESC')->one();
          return $this->id == $last->id ? true : false;
    }
}

<?php

use yii\db\Migration;

class m170830_114955_create_table_booking_group_finance_details extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_group_finance_details`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `booking_group_finance_id` INT(11),
                          `booking_date_finance_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`booking_group_finance_id`) REFERENCES `booking_group_finance`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`booking_date_finance_id`) REFERENCES `booking_date_finances`(`id`) ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170830_114955_create_table_booking_group_finance_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

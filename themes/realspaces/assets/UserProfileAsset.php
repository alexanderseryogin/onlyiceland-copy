<?php

namespace realspaces\assets;

use yii\web\AssetBundle;
use yii\web\View;

class UserProfileAsset extends AssetBundle
{
    public $sourcePath = '@realspaces/resources';
    public $css = [

        'external-plugins/intl-tel-input/build/css/intlTelInput.css',
        // BEGIN PAGE LEVEL PLUGINS 
        'plugins/select2/css/select2.min.css',
        'plugins/select2/css/select2-bootstrap.min.css',

    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [
        'external-plugins/intl-tel-input/build/js/intlTelInput.min.js',
        'external-plugins/intl-tel-input/build/js/utils.js',
        'plugins/select2/js/select2.full.min.js',
    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
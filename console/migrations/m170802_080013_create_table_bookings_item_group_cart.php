<?php

use yii\db\Migration;

class m170802_080013_create_table_bookings_item_group_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_item_group_cart` (
                          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                          `booking_item_id` int(11) unsigned DEFAULT NULL,
                          `booking_group_id` int(11) unsigned DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          CONSTRAINT `booking_item_group_cart_bgid` FOREIGN KEY (`booking_group_id`) REFERENCES `booking_group_cart` (`id`) ON DELETE CASCADE ON UPDATE SET NULL,
                          CONSTRAINT `booking_item_group_cart_biid` FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items_cart` (`id`) ON DELETE CASCADE ON UPDATE SET NULL
                        ) ENGINE=InnoDB AUTO_INCREMENT=359 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
    }

    public function down()
    {
        echo "m170802_080013_create_table_bookings_item_group_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

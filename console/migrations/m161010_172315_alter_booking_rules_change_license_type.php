<?php

use yii\db\Migration;

class m161010_172315_alter_booking_rules_change_license_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_financial` CHANGE `license_expiration_date` `license_expiration_date` INT(11) NULL; 
        ");
    }

    public function down()
    {
        echo "m161010_172315_alter_booking_rules_change_license_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170908_091033_create_table_notification extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `notifications`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `booking_item_id` INT(11) UNSIGNED,
                          `notification_type` INT(11),
                          `message` LONGTEXT,
                          `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170908_091033_create_table_notification cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

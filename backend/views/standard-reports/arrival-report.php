<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$this->title = $title;
?>
<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - <?=$title?> (<?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?>)</span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
	                    	<div class="tab-pane active" id="arrival-report">
	                    	<!-- <h3><b>Arrival Reports <?=date('jS M Y',strtotime($report_from))?> - <?=date('jS M Y',strtotime($report_to))?></b> <h3> -->    
	                    		<?php
							        $gridColumns = [
							            // ['class' => 'yii\grid\SerialColumn'],
						            [
						        		'attribute' => 'Arrival',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return date('d.m.Y',strtotime($data->arrival_date));
						        		}
						        	],
						        	[
						        		'attribute' => 'Departure',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return date('d.m.Y',strtotime($data->departure_date));
						        		}
						        	],
						        	[
						        		'attribute' => '#',
						        		'value'		=> function($data)
						        		{
						        			$date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
									        $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
									        $diff=date_diff($date1,$date2);
									        $difference =  $diff->format("%a");
									        return $difference;
						        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			// return $booking_date->no_of_nights;
						        		}
						        	],
						        	[
						        		'format' => 'raw',
						        		'attribute' => 'Room Type',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return $data->item->itemType->name;
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
						                },
						        	],
						        	[
						        		'format' => 'raw',
						        		'attribute' => 'Unit Name',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return isset($data->item_name_id)?$data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:''):"- - - - - ";
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
						                },
						        	],
						        	[
						        		'attribute' => 'Guest Name',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return ($booking_date->guest_first_name != NULL && $booking_date->guest_last_name != NULL)?$booking_date->guest_first_name.' '.$booking_date->guest_last_name :"- - - - -";
						        		}
						        	],
						        	[
						        		'label' => 'A/C',
						        		'attribute' => 'A/C',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return $booking_date->no_of_adults.'/'.$booking_date->no_of_children;
						        		}
						        	],
						        	[
						        		'attribute' => 'Referrer',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return isset($data->travel_partner_id)?$data->travelPartner->travelPartner->company_name:"N/A";
						        		}
						        	],
						        	[
						        		'format' => 'raw',
						        		'label' => 'Balance Due',
						        		'attribute' => 'balance',
						        		'value'		=> function($data)
						        		{
						        			
						        			return Yii::$app->formatter->asDecimal($data->balance);
						        		}
						        	]


							        ];
							        echo ExportMenu::widget([
							        'showConfirmAlert' => false,	
							        'dataProvider' => $dataProvider,
							        'columns' => $gridColumns,
							        'target' => ExportMenu::TARGET_BLANK,
							        'exportConfig' => [
									    ExportMenu::FORMAT_TEXT => false,
    									ExportMenu::FORMAT_PDF => false,
    									ExportMenu::FORMAT_HTML => false,
    									ExportMenu::FORMAT_EXCEL => false,
									    ExportMenu::FORMAT_CSV => [
									        'label' => 'CSV',
									    ],
									    
									    ExportMenu::FORMAT_EXCEL_X => [
									        'label' => 'EXCEL',
									    ],

									],
									'fontAwesome' => true,
							        'dropdownOptions' => [
							            'label' => 'Export Report',
							            'class' => 'btn btn-success'
							        ],
							        'enableFormatter' =>false,
							        'showColumnSelector' => false,
							        //'asDropdown' => false,
							        'container'=>['class'=>'btn-group pull-left', 'style'=> 'margin: 5px'],
							        'filename' => ($report_type == 'arrival')?'arrival-report':'departure-report',
							        'exportRequestParam' => 'hello;',

							    ]);
							    ?>
							    <br><br><br>
	                    		<?= GridView::widget([

						        'id' => 'bookings_gridview',
						        'dataProvider' => $dataProvider,
						        //'filterModel' => $searchModel,
						        'options' => ['class' => 'grid-view','Gridlines' => "None",'BorderStyle'=> "None" ],
						        'layout'=>"\n{items}\n{pager}",
						        'columns' => [
						        	//'id',
					        		[
						        		'attribute' => 'Arrival',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return date('d.m.Y',strtotime($data->arrival_date));
						        		}
						        	],
						        	[
						        		'attribute' => 'Departure',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return date('d.m.Y',strtotime($data->departure_date));
						        		}
						        	],
						        	[
						        		'attribute' => '#',
						        		'value'		=> function($data)
						        		{
						        			$date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
									        $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
									        $diff=date_diff($date1,$date2);
									        $difference =  $diff->format("%a");
									        return $difference;
						        			// $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			// return $booking_date->no_of_nights;
						        		},
						        		'contentOptions' => function($data)
							            {
							    
							                return ['style' => 'text-align:right;' ];
							            },
						        		'headerOptions' =>[ 'style' => 'width:3%;'],
						        	],
						        	[
						        		'format' => 'raw',
						        		'attribute' => 'Room Type',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return $data->item->itemType->name;
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
						                },
						        		'headerOptions' =>[ 'style' => 'width:20%;'],
						        	],
						        	[
						        		'format' => 'raw',
						        		'attribute' => 'Unit Name',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return isset($data->item_name_id)?$data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:''):"- - - - - ";
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
						                },
						        	],
						        	[
						        		'attribute' => 'Guest Name',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			// if(empty($booking_date))
						        			// {
						        			// 	echo "<pre>";
						        			// 	print_r($data->id);
						        			// 	exit();
						        			// }
						        			return ($booking_date->guest_first_name != NULL && $booking_date->guest_last_name != NULL)?$booking_date->guest_first_name.' '.$booking_date->guest_last_name :"- - - - -";
						        		}
						        	],
						        	[
						        		'label' => 'A/C',
						        		'attribute' => 'A/C',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return $booking_date->no_of_adults.'/'.$booking_date->no_of_children;
						        		},
						        		'contentOptions' => function($data)
						                {
						        
						                    return ['style' => 'text-align:right' ];
						                },
						        		'headerOptions' =>[ 'style' => 'width:5%;'],

						        	],
						        	[
						        		'attribute' => 'Referrer',
						        		'value'		=> function($data)
						        		{
						        			$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        			return isset($data->travel_partner_id)?$data->travelPartner->travelPartner->company_name:"N/A";
						        		}
						        	],
						        	[
						        		'label' => 'Balance Due',
						        		'attribute' => 'balance',
						        		'value'		=> function($data)
						        		{
						        			
						        			return Yii::$app->formatter->asDecimal($data->balance);
						        		},
							            'contentOptions' => function($data)
							            {
							    
							                return ['style' => 'text-align:right;' ];
							            },
							            // 'headerOptions' => [ 'style' => 'width:5%;'],
						        	]


						        ],
						        'afterRow' => function($model, $key, $index) {
						        	//$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
						        	if(isset($model->comments) && !empty($model->comments))
						        	{
						        		return "<tr style='border-top:0px !important;'><td colspan=9 style='white-space: normal;word-wrap:break-word !important;'>".$model->comments."</td></tr>";
						        		// return Html::tag('tr',
								        //     Html::tag('td', $model->comments,['colspan' => 8],['style' => 'word-wrap: break-word;']),
								        //     ['style' => 'border-top: none !important;']
								        //     // .Html::tag('td', $model->father_dob)
								        //     //add more columns
								        // );
						        	}
								        
							    }
						        ]);?>
						        <p style="font-size: 12px">Total <?=($report_type == 'arrival')?'arrivals':'departures'?> : <?=$count?></p>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
$this->registerJs("
	$(document).ready(function(){
		$('#w3').append(
			`<li>
            	<a href=".yii::$app->getUrlManager()->createUrl(['standard-reports/arrival-pdf',
            		'destination_id' => $destination_id,
			    	'report_to' => $report_to,
			    	'report_from' => $report_from,
			    	'cancelled_bookings' => $cancelled_bookings,
			    	'report_type' => $report_type])." 
			    	target=\"_blank\"><i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>
			    PDF</a>
		    </li>`

		);
	});
");
$this->registerCss("
		.no-border{
			border-top : none !important;
		}
		.table{
			width:100%;
			table-layout: fixed;
		}
	");

?>
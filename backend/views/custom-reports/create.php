<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CustomReports */

$this->title = 'Create Custom Reports';
$this->params['breadcrumbs'][] = ['label' => 'Custom Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-reports-create">


    <?= $this->render('_form', [
        'model' => $model,
        'columns' =>$columns,
        'defaultColumns' => $defaultColumns
    ]) ?>

</div>

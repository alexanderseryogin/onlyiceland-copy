<?php

use yii\db\Migration;

class m160906_075537_create_table_categories extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `categories`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(512) NOT NULL, `parent_id` INT(11) DEFAULT NULL, `created_at` INT(11) NOT NULL, `updated_at` INT(11) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 


            ALTER TABLE `categories` ADD CONSTRAINT `FK_categories_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `categories`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m160906_075537_create_table_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

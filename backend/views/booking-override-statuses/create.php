<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BookingOverrideStatuses */

$this->title = 'Create Booking Override Statuses';
$this->params['breadcrumbs'][] = ['label' => 'Booking Override Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-override-statuses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

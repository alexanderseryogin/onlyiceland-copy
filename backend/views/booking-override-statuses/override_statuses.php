<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Destinations;
use common\models\BookableItems;
use common\models\BookingsItems;
use common\models\BookingOverrideStatuses;

$this->title = "Booking Override Statuses Scheduler";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings Override Statuses'), 'url' => ['index']];

$session = Yii::$app->session;
$session->open();
$params = '';

if(isset($session[Yii::$app->controller->id.'-booking-schedular-values']) && !empty($session[Yii::$app->controller->id.'-booking-schedular-values']))
{
    $params = json_decode($session[Yii::$app->controller->id.'-booking-schedular-values'], true);
    unset($session[Yii::$app->controller->id.'-booking-schedular-values']);
    //print_r($params);
}

?>

<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	#calendar {
		
		margin: 0 auto;
	}

</style>
<div class="row calendar-div">
    <div class="col-md-12">
        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md" style="width: 100%;">
                    <div class="row">
                		<div class="col-sm-4">
	                        <label class="control-label">Destinations</label>
	                        <div class="form-group" >
	                            <select id="provider_dropdown" style="width: 100%;" class="form-control">
	                            	<option value=""></option>
	                                <?php
	                                    $destinations = ArrayHelper::map(Destinations::find()->where(['active' => 1])->all(),'id','name');

                                        if($destination_id != null )
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if($destination_id == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }
                                        }
                                        else
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if(!empty($params) && $params['provider_id'] == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }    
                                        }
	                                    
	                                ?>
	                            </select>
	                        </div>
	                    </div>
                        
                	</div>
                </div>
                <ul class="nav nav-tabs bookings_tabs">
                    <li class="active">
                        <!-- <a href="#general_info" data-toggle="tab">General info</a> -->
                    </li>
                </ul>
            </div>
            
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <center><div class="loader"></div></center><br>
                        	<div id="error_div" class="alert-danger alert fade in" style="display: none">
								No item found.
							</div>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
                <br>        
            </div>
        </div>
    </div>
</div>
<div id='calendar'></div>

<div class="modal fade rates_override_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-md ">
    
      <!-- Modal content-->
       <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Manage Override Statuses</h4>
            </div>
            <div class="modal-body">
                <div id="blockui_sample_3_1_element">
                <!-- <div class="row rate_inputs" style="padding-top: 15px;">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>
                </div> -->
                <div class="row" style="padding-top: 10px;">
                    <!-- <div class="col-md-1" style="margin-left: 31px;">
                        <input type="checkbox" class="make-switch rates-switch" data-on-text="Percentage" data-off-text="Normal" data-size="small">
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <div class="form-group">
                            
                            <input class="form-control input-sm input-small input-inline" type="number" id='rate_percentage' disabled min="1" max="100">
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <select class="form-control override_status" data-placeholder="Select Status">
                            <?php
                                $override_statuses = BookingOverrideStatuses::find()->all();
                                if(!empty($override_statuses))
                                {
                                    foreach ($override_statuses as $value) 
                                    {
                                        ?>
                                        <option value="<?=$value->id?>"><?=$value->label?></option>
                                        <?php
                                    }
                                }
                                    
                            ?>
                        </select>
                    </div>

                    <div class="col-md-6 ">
                        <!-- <label class="control-label">Housekeeping Status</label> -->
                        <div class="button-group">
                            <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">Apply to <span class="caret"></span></button>
                            <ul class="dropdown-menu bookable_items">
                              
                            </ul>
                        </div>
                        <div class="form-group" >
                            
                        </div>
                    </div>
                    
                </div>

                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <h4><b>Set Date Range</b></h4>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group" >
                            <div class="input-group input-medium date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                <input type="text" class="form-control" id="rate_from" >
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" id="rate_to">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Mo</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='rate_mo' data-order="0" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Tu</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='tu' data-order="1" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>We</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='we' data-order="2" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Th</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='th' data-order="3" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Fr</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='fr' data-order="4" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Sa</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='sat' data-order="5" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Su</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='su' data-order="6" checked>
                        </div>
                    </div>
                </div>

                <hr>
              <!-- <p>Some text in the modal.</p> -->
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <button type="button" class="btn green pull-left" id="rate_save">Save</button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
            </div>
            </div>
        </div>
      
    </div>
</div>

<?php
// echo "<pre>";
// print_r($params);
// exit();

// else
// {
// 	echo "no params";
// 	exit();
// }
$get_destination_booked_items = Url::to(['/booking-override-statuses/get-destination-booked-items-schedular']);
$edit_or_drag_booked_item = Url::to(['/bookings/edit-or-drag-booked-item-schedular']);

$override_rate = Url::to(['/booking-override-statuses/override-status']);

$this->registerJs("
    var global_item_name_id = '';
	$(document).ready(function(){
        $('.override_status').select2({
            width: '250px',
            allowClear: true,
            placeholder: \"Select a Destination\",
        });
		$('#provider_dropdown').select2({
	        placeholder: \"Select a Destination\",
	        allowClear: true,
	    });
	
		$(document).on('change','#provider_dropdown',function()
	    {
	    	onChnageDropdown();
	    	
	    });

        $(document).on('click','#rate_save',function(){
            console.log('save pressed');
            OverrideRate();
        })


	    
	});

    $(document).on('hide.bs.modal','.rates_override_modal', function () {
        console.log($(this).attr('class'));
        // $('#rate_percentage').val('');
        // $('.rates-switch').bootstrapSwitch('state', false);
        $('.bookable_items_checkboxes').each(function(){
            $(this).attr('checked',false);
        });
        $('.rate_checkboxes').each(function(){
            $(this).prop('checked',true);
        });
        $('.override_status').val('').trigger('change');
    });

    $(document).on('click','.select_all_bookable_items',function(){
        $('.bookable_items_checkboxes').each(function(){
            $(this).prop('checked',true);
        });
    });
    $(document).on('click', '.bookable_items ', function (e) {
        console.log('here');
      e.stopPropagation();
    });
    $(document).on('click','.deselect_all_bookable_items',function(){
        $('.bookable_items_checkboxes').each(function(){
            $(this).attr('checked',false);
        });
    });


    function OverrideRate()
    {
        // App.blockUI({
        //         target: '#blockui_sample_3_1_element',
        //         overlayColor: 'none',
        //         animate: true
        //     });
        // App.startPageLoading({animate: true});
        var aChecked = $('input:checkbox:checked'); // assumes your checkboxes are in a form tag
        var aValues = [];
        $('.rate_checkboxes').each(function(){
            aValues[$(this).attr('data-order')] = $(this).is(':checked');
        });
        // var cpricing_values = [];
        // $('.cpricing').each(function(){
        //     cpricing_values.push($(this).val());
        // });
        // console.log(aValues);

        var bookable_items_checkboxes = [];
        $('.bookable_items_checkboxes').each(function(){
            if($(this).is(':checked'))
            {
                bookable_items_checkboxes.push($(this).val());
            }
            
        });

        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'override_status' : $('.override_status').val(),
                        // 'rate_id' : $('#rate').attr('data-rate'),
                        'date' : $('#rate_date').val(),
                        'rate_from' : $('#rate_from').val(),
                        'rate_to' : $('#rate_to').val(),
                        // 'override_type' : $('.rates-switch').bootstrapSwitch('state'),
                        // 'percentage' : $('#rate_percentage').val(),
                        'bookable_items' : bookable_items_checkboxes,
                        'rate_days' : aValues,
                        'global_item_name_id': global_item_name_id
                        // 'cpricing' : cpricing_values
                    };
        $.ajax({
            async: false,
            url: '$override_rate',
            data: object,
            type: 'POST',
            success: function (data) {
                 // App.unblockUI('#blockui_sample_3_1_element');
                data = jQuery.parseJSON( data );

                // console.log('date : '+".$date." );
                // var original_date =  '".$date."';
                console.log(data.date);
                original_date = data.date;
                var date_arr = original_date.split('-');

                var month = date_arr[2]-1;
                var scroll = parseInt(month)*190;

                console.log(data);
                // $('.rate_inputs').empty().html(data.rate_inputs)
                $('#myModal').modal('toggle');
                // $('#calendar').fullCalendar( 'destroy');
                // initFullCalendar([],data.resources,data.events,data.date,scroll,original_date);
                initFullCalendar(data.bookings_items,data.resources,data.date,scroll,original_date);
                
            },
            error: function () {
                alert('could not get the data');
            },
        });
        
    }

	function onChnageDropdown()
    {
    	App.blockUI({target:\".loader\", animate: !0});

        var object = {
                    	'destination_id' : $('#provider_dropdown').val(),
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$get_destination_booked_items',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                   	$('#error_div').css('display','block');
                   	$('#calendar').hide();
                    //$('#item_dropdown').empty().html('');
                    App.unblockUI(\".loader\");
                }
                else
                {
                	console.log('date : '+".$date." );
                    var original_date =  '".$date."';
                    original_date =original_date.toString();
                    var date_arr = original_date.split('-');

                    var month = date_arr[2]-1;
                	var scroll = parseInt(month)*240;
                	console.log('scroll : '+ scroll);

                   	$('#error_div').css('display','none');
                   	$('#calendar').show();
                    $('.bookable_items').empty().html(data.bookable_items);
                    App.unblockUI(\".loader\");
                   	console.log(jQuery.parseJSON(data.bookings_items));
                   	console.log(jQuery.parseJSON(data.resources));
                   	// console.log(jQuery.parseJSON(data.notifications));
                   	initFullCalendar(jQuery.parseJSON(data.bookings_items),jQuery.parseJSON(data.resources),data.date,scroll,original_date);
                    console.log($('.fc-content table .fc-widget-header div .fc-cell-content .fc-cell-text').text());
                    if($('.fc-content table .fc-widget-header div .fc-cell-content .fc-cell-text').text() == 'Resources')
                    {
                        $('.fc-content table .fc-widget-header div .fc-cell-content .fc-cell-text').text('Bookable Items');
                    }
                    

       //             	$('.fc-scroller').animate({
					  //   scrollLeft: '+='+scroll+'px'
					  // }, 'slow');
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    }

    function initFullCalendar(bookings_items,resources,date,scroll,original_date)
    {
        // alert('heraerasdra');
        console.log(bookings_items);
    	$('#calendar').fullCalendar( 'destroy' );

    	$('#calendar').fullCalendar({
			schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
			height: 700,
			//now: '2017-09-27',
			editable: false,
			aspectRatio: 1.8,
			scrollTime: '00:00',
			slotWidth: 250,
			// filterResourcesWithEvents: true, 
			header: {
                left: 'today prev,next',
                center: 'title',
                right: 'timelineWeek,timelineThreeDays,timelineMonth'
            },
            defaultView: 'timelineMonth',
            views: {
                timelineThreeDays: {
                    type: 'timeline',
                    duration: { days: 14 },        
                    slotDuration: {days: 1},
                },
                timelineWeek: {
                    type: 'timeline',
                    duration: { days: 7 },        
                    slotDuration: {days: 1},
                },


            },
			groupByResource: true,
			resourceAreaWidth: '25%',
			// resourceColumns: [
			// 	{
			// 		group: true,
			// 		labelText: 'Bookable Item',
			// 		field: 'item',
			// 		width: 250,
			// 	},
			// 	{
			// 		// group: true,
			// 		labelText: 'Unit',
			// 		field: 'title',
			// 		width: 150
			// 	},
			// 	// {
			// 	// 	labelText: 'Occupancy',
			// 	// 	field: 'occupancy'
			// 	// }
			// ],
			resourceGroupField: 'item',
            // resourceGroupField: 'id',
            resourcesInitiallyExpanded: false,
			resources: resources,
			events: bookings_items,
            eventOrder : '-title',
			eventResizeStart: function (event, jsEvent, ui, view) 
            {
                before_start = event.start.format();
                before_end = event.end.format();
                //console.log('RESIZE Before!! ' + before_start + ' to '+ before_end);
            },
            
            dayClick: function(date, jsEvent, view, resourceObj) {

                // alert('Date: ' + date.format());
                // alert('Resource ID: ' + resourceObj.id);
                $('#myModal').modal('show'); 

                $('#rate_from').datepicker('setDate', new Date(date.format()) );
                $('#rate_to').datepicker('setDate', new Date(date.format()) );

                $('#rate_from').datepicker({
                                              minDate: new Date(date.format())
                                            } );
                $('#rate_to').datepicker({
                                              minDate: new Date(date.format())
                                            } );
                global_item_name_id = resourceObj.bookable_item_name_id;

                // alert(global_item_name_id);
                // $('.bookable_items_checkboxes').each(function(){
                //     console.log(resourceObj);

                //     var current_input = $(this);
                //     if(current_input.val() == resourceObj.bookable_item_id)
                //     {
                //         // console.log('equal');
                //         // console.log('bookable item : '+$(this).val() );
                //         // console.log('resource : '+calEvent.bookable_item_id );
                //         current_input.prop('checked',true);
                //     }
                    
                // });

            },
            eventClick: function(calEvent, jsEvent, view) {
                // alert('Event: ' + calEvent.is_override_status);
                if(calEvent.is_override_status == 1)
                {
                    $('#myModal').modal('show'); 
                }
                

                $('#rate_from').datepicker('setDate', new Date(calEvent.start) );
                $('#rate_to').datepicker('setDate', new Date(calEvent.start) );

                $('.override_status').val(calEvent.booking_override_status_id).trigger('change');
                global_item_name_id = calEvent.resourceId;

            },
            resourceRender: function(resourceObj, labelTds, bodyTds) {
            	// console.log(bodyTds);
             //    console.log(labelTds.prev().attr('class'));
            	// typeof(resourceObj.background_color);
                console.log(resourceObj);
            	if (typeof(resourceObj.background_color) === 'undefined') 
            	{
				   
				}
            	else
            	{
                   
            		labelTds.parent().css('background', resourceObj.background_color);
            		labelTds.css('background', resourceObj.background_color);
                    labelTds.children().children().css('color', resourceObj.text_color);
                    labelTds.parent().css('color', resourceObj.text_color);
            	}
			    
			}
		});
        // console.log('in init full calendar');
        // console.log(original_date);
        $('#calendar').fullCalendar('gotoDate',original_date);
		$('.fc-scroller').animate({
	    	scrollLeft: '+='+scroll+'px'
	  	}, 'slow');
    }

    function editOrDragBookingItem(event,revertFunc)
    {
    	var resource = $('#calendar').fullCalendar( 'getEventResource', event );
    	console.log(resource);
    	//console.log($('#calendar').fullCalendar( 'getEventResource', event ))
        // if(confirm('Are you sure about this change?')) 
        // {
            var object = 
                {
                    'id': event.id,
                    'bookable_item_id':resource.bookable_item_id,
                    'item_name_id': resource.id,
                    'arrival_date' : event.start.format(),
                    'departure_date' : event.end.format(),

                };

            $.ajax(
            {
                type: 'POST',
                data: object,
                url: '$edit_or_drag_booked_item',
                success: function(data)
                {

                    if(data=='false')
                    {
                        //alert('This rate is not available '+event.start.format() +' - '+event.end.format());
                        alert('The rate is not available');
                        revertFunc();
                    }
                    else if(data == 'date_exception')
                    {
                        alert('you cannot book on this day');
                        revertFunc();
                    }
                    else
                    {
                    	data = jQuery.parseJSON(data);
                    	console.log(data.notifications);
                    	
                    	console.log('in else');

                    	// console.log('scroll date : '+data.first_date);
                    	// var scroll = parseInt(data.first_date)*240;
                    	// console.log('scroll : '+scroll);

                        original_date = (data.first_date).toString();
                        var date_arr = original_date.split('-');

                        var month = date_arr[2]-1;
                        var scroll = parseInt(month)*240;
                        console.log('scroll : '+ scroll);

                    	initFullCalendar(data.bookings_items,data.resources,data.date,data.closed_dates,data.notifications,scroll,original_date);

                    	//var noTime = $.fullCalendar.moment('2017-09-27');
                    	//$('#calendar').fullCalendar('gotoDate',noTime);
                    	//$('#calendar').fullCalendar('today');
                    	
                    }
                },
                error: function()
                {
                    alert('error');
                }
            });
       // }
        // else
        // {
        //     revertFunc();
        // }
    }
");

if($destination_id != null)
{
	$this->registerJs("
	    	$(document).ready(function(){
	    	
	    		onChnageDropdown();
	    	});
	    	
	    	//$('#provider_dropdown').trigger('change');
	    	//$('#provider_dropdown').change();
	    ");
}

else
{
	if(!empty($params) && isset($params['provider_id']) )
	{
	    $this->registerJs("
	    	$(document).ready(function(){
	    		// console.log('param exists'+".$params['provider_id'].");
	    		onChnageDropdown();
	    	});
	    	
	    	//$('#provider_dropdown').trigger('change');
	    	//$('#provider_dropdown').change();
	    ");
	}
}
$this->registerCss("
    #calendar {
        max-height: 1000px;
        margin: 50px auto;
    }
    // .fc-timeline-event {
    //     position: absolute;
    //     border-radius: 0;
    //     padding: 2px 0;
    //     margin-bottom: 1px;
    //     // height: 110%;
    //     // background-color: white !important;
    //     // border-color: #ddd !important;
    //     // border-bottom: none !important;
    //     // border-right: none !important;
    //     // border-left: none !important;
        
    // }
     .fc-timeline-event .fc-content  {
            // margin-top: 9px;
            text-align :center;
            font-size : 15px;
            cursor: context-menu;
            // cursor: pointer;
     }
     // displaying rates resources in 2 lines
    //  .fc-timeline td, .fc-timeline th {
    //     white-space: normal !important;
    // }

    ");


?>
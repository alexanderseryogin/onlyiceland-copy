<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use realspaces\assets\ThemeAsset;

/**
 * Main frontend application asset bundle.
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/frontend/assets';
    public $css = [
        'css/styles.css',
        'css/jquery-ui.min.css',
        'css/jquery-ui.theme.min.css',
        ];
    public $js = [
        'js/jquery-ui.min.js',
        'js/sidebar.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'realspaces\assets\ThemeAsset',
        //DateRangeAsset::class
    ];
}

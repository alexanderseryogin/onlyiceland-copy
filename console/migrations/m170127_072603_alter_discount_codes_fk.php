<?php

use yii\db\Migration;

class m170127_072603_alter_discount_codes_fk extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates_discount_code` DROP FOREIGN KEY `FK_rates_discount_code_rates_id`; 
            ALTER TABLE `rates_discount_code` ADD CONSTRAINT `FK_rates_discount_code_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON DELETE CASCADE; 
        ");
    }

    public function down()
    {
        echo "m170127_072603_alter_discount_codes_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

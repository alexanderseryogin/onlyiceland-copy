<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\DestinationTypes;
use common\models\Cities;
use common\models\States;
use common\models\AccommodationTypes;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DestinationsSearch */
/* @var $dataDestination yii\data\ActiveDataDestination */

$this->title = Yii::t('app', 'Destinations');
$this->params['breadcrumbs'][] = $this->title;
$update_page_url = Url::to(['/destinations/update']);
?>
<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>
<div class="providers-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW Destination'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    
<?php //Pjax::begin(['id' => 'providers-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-scrollable'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'name',
                'label' => 'Destination Name',
                'value' => function($data)
                {
                    return $data->name;
                },
                'contentOptions' =>['style' => 'min-width:150px;'],
                'headerOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'provider_type_id',
                'value' => function($data)
                {
                    if(isset($data->destinationType->name) && !empty($data->destinationType->name))
                        return $data->destinationType->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'DestinationsSearch[provider_type_id]', 
                    $searchModel['provider_type_id'], 
                    ArrayHelper::map(DestinationTypes::find()->orderBy('name')->all(),'id','name'), 
                    [
                        'prompt' => 'Select Type', 
                        'id'=>'ProvidersSearch-types', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'min-width:150px;'],
                'headerOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'accommodation_id',
                'value' => function($data)
                {
                    if(isset($data->accommodation->name) && !empty($data->accommodation->name))
                        return $data->accommodation->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'DestinationsSearch[accommodation_id]', 
                    $searchModel['accommodation_id'], 
                    ArrayHelper::map(AccommodationTypes::find()->orderBy('name')->all(),'id','name'), 
                    [
                        'prompt' => 'Select Type', 
                        'id'=>'ProvidersSearch-acc-types', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'min-width:170px;'],
                'headerOptions' =>['style' => 'min-width:170px;'],
            ],
            [
                'attribute' => 'state_id',
                'label'=> 'State/Region',
                'value' => function($data){
                    return isset($data->state) ? $data->state->name : '';
                },
                'filter' => Html::dropDownList(
                    'DestinationsSearch[state_id]', 
                    $searchModel['state_id'], 
                    ArrayHelper::map(States::find()->where(['country_id' => 100])->orderBy('name')->all(),'id','name'), 
                    [
                        'prompt' => 'Select State', 
                        'id'=>'states_dropdown', 
                        'class'=>'form-control',

                    ]),
                'contentOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'city_name',
                'label'=> 'City',
                'value' => function($data)
                {
                    if(isset($data->city->name) && !empty($data->city->name))
                        return $data->city->name;
                    else
                        return '';
                },
                'contentOptions' =>['style' => 'min-width:150px;'],
                'headerOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'youngest_age',
                'label' => 'YNG',
                'value' => function($data)
                {
                    if(isset($data->bookingPolicies->youngest_age) && !empty($data->bookingPolicies->youngest_age))
                        return $data->bookingPolicies->youngest_age;
                    else
                        return '';
                },
                
                'contentOptions' =>['style' => 'text-align:right; width:10%'],
                'headerOptions' =>['style' => 'min-width:60px; text-align: center'],
            ],
            [
                'attribute' => 'oldest_free_age',
                'label' => 'OLF',
                'value' => function($data)
                {
                    if(isset($data->bookingPolicies->oldest_free_age) && !empty($data->bookingPolicies->oldest_free_age))
                        return $data->bookingPolicies->oldest_free_age;
                    else
                        return '';
                },
                'contentOptions' =>['style' => 'text-align:right; width:10%'],
                'headerOptions' =>['style' => 'min-width:60px; text-align: center'],
            ],
            /*'booking_email:email',
            [
                'attribute' => 'license_type',
                'value' => function($data){
                    return $data->providersFinancials->license_type;
                },
                'contentOptions' =>['style' => 'width:10%'],
            ],*/
            [
                'attribute' => 'license_expiration',
                'label' => 'Lic. Exp.',
                'value' => function($data)
                {
                    if(isset($data->destinationsFinancials->license_expiration_date) && !empty($data->destinationsFinancials->license_expiration_date))
                        return date('d/m/Y',$data->destinationsFinancials->license_expiration_date);
                    else
                        return '';
                },
                'filter' => '<input class="form-control form-control-inline input-medium date-picker" type="text"  name="DestinationsSearch[license_expiration]" style="width:80px !important" />',
                'contentOptions' =>['style' => 'text-align:center; width:10%'],
            ],
            [
                'header' => '<a href="javascript:"> View License </a>',
                'attribute' => 'view_license',
                'format' => 'raw',
                'value' => function($data)
                {
                    if(isset($data->destinationsFinancials->license_image) && !empty($data->destinationsFinancials->license_image))
                    {
                        $path = Yii::getAlias('@web') . '/../uploads/Destinations/'.$data->id.'/licenses';
                        $file = $path . '/'.$data->destinationsFinancials->license_image;
                        return '<center><a target="_blank" href='.$file.' download><i style="margin-top:10px;" class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a></center>';
                    }
                    else
                        return '';
                },
            ],
            [
                'attribute' => 'checkin_time_from',
                'label' => 'Check-in From',
                'value' => function($data)
                {
                    if(isset($data->bookingPolicies->checkin_time_from) && !empty($data->bookingPolicies->checkin_time_from))
                        return date('H:i',strtotime($data->bookingPolicies->checkin_time_from));
                    else
                        return '';
                },
            ],
            [
                'attribute' => 'checkin_time_to',
                'label' => 'Check-in To',
                'value' => function($data)
                {
                    if(isset($data->bookingPolicies->checkin_time_to) && !empty($data->bookingPolicies->checkin_time_to))
                        return date('H:i',strtotime($data->bookingPolicies->checkin_time_to));
                    else
                        return '';
                },
            ],
            [
                'attribute' => 'checkout_time_from',
                'label' => 'Check-out From',
                'value' => function($data)
                {
                    if(isset($data->bookingPolicies->checkout_time_from) && !empty($data->bookingPolicies->checkout_time_from))
                        return date('H:i',strtotime($data->bookingPolicies->checkout_time_from));
                    else
                        return '';
                },
            ],
            [
                'attribute' => 'checkout_time_to',
                'label' => 'Check-out To',
                'value' => function($data)
                {
                    if(isset($data->bookingPolicies->checkout_time_to) && !empty($data->bookingPolicies->checkout_time_to))
                        return date('H:i',strtotime($data->bookingPolicies->checkout_time_to));
                    else
                        return '';
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                
                'headerOptions' =>['style' => 'min-width:70px;'],
                'contentOptions' =>['style' => 'min-width:70px;'],
            ],
        ],
    ]); ?>
<?php //Pjax::end(); ?></div>
<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#ProvidersSearch-types').select2({
            placeholder: \"Select Type\",
            allowClear: true
        });

    $('#ProvidersSearch-acc-types').select2({
            placeholder: \"Select Type\",
            allowClear: true
        });

    $('#city').select2({
            placeholder: \"Select City\",
            allowClear: true
        });

    $('#states_dropdown').select2({
            placeholder: \"Select State\",
            allowClear: true
        });

    $.removeCookie('active_tab');
    //$.cookie('active_tab','#details');

    $('#providers-gridview').on('pjax:end', function() {

        $('#ProvidersSearch-types').select2({
            placeholder: \"Select Type\",
            allowClear: true
        });
        $('#city').select2({
            placeholder: \"Select City\",
            allowClear: true
        });
        $('#states_dropdown').select2({
            placeholder: \"Select State\",
            allowClear: true
        });
    });

    $('#checkin_time_from').val('');
    $('#checkin_time_to').val('');
    $('#checkout_time_from').val('');
    $('#checkout_time_to').val('');

    $(document).on('click','.table-striped tbody tr',function()
    {
        window.location.replace('$update_page_url?id='+$(this).attr('data-key'));
    });

});",View::POS_END);
?>

<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$this->title = 'Occupancy Report';

?>
<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - Daily Occupancy Report (<?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?>)</span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
	                    	<div class="tab-pane active" id="occupancy-report">
                               <?php
                                    $gridColumns = [
                                        // ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'date',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return date('d.m.Y',strtotime($data['date'])).' '.Yii::t('app',date('l',strtotime($data['date'])));
                                            }
                                        ],
                                        [
                                            'attribute' => 'Arrivals',
                                            'value'     => function($data)
                                            {
                                                return $data['arrivals'];
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                            //'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],
                                        [
                                            'attribute' => 'Average Length of Stay',
                                            'value'     => function($data)
                                            {
                                                return str_replace('.', ',', $data['average_stay']);
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                            //'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],
                                        [
                                            'attribute' => 'Departures',
                                            'value'     => function($data)
                                            {
                                                return $data['departures'];
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                           // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],
                                        [
                                            'label' => 'Stay Through',
                                            'value'     => function($data)
                                            {
                                                return $data['stay_through'];
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                          //  'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],
                                        [
                                            'attribute' => 'Booked',
                                            'value'     => function($data)
                                            {
                                                return $data['booked_rooms'];
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                           // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],
                                        [
                                            'attribute' => 'Available',
                                            'value'     => function($data)
                                            {
                                                return $data['available_rooms'];
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                           // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],
                                        [
                                            'attribute' => 'Occupancy',
                                            'value'     => function($data)
                                            {
                                                return $data['occupancy']." %";
                                            },
                                            'contentOptions' => function($data)
                                            {
                                    
                                                return ['style' => 'text-align:right;' ];
                                            },
                                           // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                        ],

                                    ];
                                    echo ExportMenu::widget([
                                    'showConfirmAlert' => false,    
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'target' => ExportMenu::TARGET_BLANK,
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF => false,
                                        ExportMenu::FORMAT_HTML => false,
                                        ExportMenu::FORMAT_EXCEL => false,
                                        ExportMenu::FORMAT_CSV => [
                                            'label' => 'CSV',
                                        ],
                                        
                                        ExportMenu::FORMAT_EXCEL_X => [
                                            'label' => 'EXCEL',
                                        ],

                                    ],
                                    'fontAwesome' => true,
                                    'dropdownOptions' => [
                                        'label' => 'Export Report',
                                        'class' => 'btn btn-success'
                                    ],
                                    'enableFormatter' =>false,
                                    'showColumnSelector' => false,
                                    //'asDropdown' => false,
                                    'container'=>['class'=>'btn-group pull-left', 'style'=> 'margin: 5px'],
                                    'filename' => 'occupancy-report',
                                    'exportRequestParam' => 'hello;',

                                ]);
                                ?>
                                <br><br><br>
	               	           <?= GridView::widget([

                                'id' => 'bookings_gridview',
                                'dataProvider' => $dataProvider,
                                //'filterModel' => $searchModel,
                                'options' => ['class' => 'grid-view fixed-layout','Gridlines' => "None",'BorderStyle'=> "None"],
                                'layout'=>"\n{items}\n{pager}",
                                'columns' => [
                                    //'id',
                                    [
                                        'attribute' => 'date',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return date('d.m.Y',strtotime($data['date'])).' '.Yii::t('app',date('l',strtotime($data['date'])));
                                        }
                                    ],
                                    [
                                        'attribute' => 'Arrivals',
                                        'value'     => function($data)
                                        {
                                            return $data['arrivals'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                        //'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    [
                                        'attribute' => 'Average Length of Stay',
                                        'value'     => function($data)
                                        {
                                            return str_replace('.', ',', $data['average_stay']);
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                        //'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    [
                                        'attribute' => 'Departures',
                                        'value'     => function($data)
                                        {
                                            return $data['departures'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                       // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    [
                                        'label' => 'Stay Through',
                                        'value'     => function($data)
                                        {
                                            return $data['stay_through'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                      //  'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    [
                                        'attribute' => 'Booked',
                                        'value'     => function($data)
                                        {
                                            return $data['booked_rooms'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                       // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    [
                                        'attribute' => 'Available',
                                        'value'     => function($data)
                                        {
                                            return $data['available_rooms'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                       // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    [
                                        'attribute' => 'Occupancy',
                                        'value'     => function($data)
                                        {
                                            return $data['occupancy']." %";
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                       // 'headerOptions' =>[ 'style' => 'width:3%;'],
                                    ],
                                    

                                ],
                                // 'afterRow' => function($model, $key, $index) {
                                //     //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                //     if(isset($model->comments) && !empty($model->comments))
                                //     {
                                //         return "<tr style='border-top:0px !important;'><td colspan=10 style='white-space: normal;word-wrap:break-word !important;'>".$model->comments."</td></tr>";

                                //     }
                                        
                                // }
                                ]);?>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
$this->registerJs("
	$(document).ready(function(){
		$('#w3').append(
			`<li>
            	<a href=".yii::$app->getUrlManager()->createUrl(['standard-reports/occupancy-pdf',
            		'destination_id' => $destination_id,
			    	'report_to' => $report_to,
			    	'report_from' => $report_from,
			    	'cancelled_bookings' => $cancelled_bookings,
			    	'report_type' => $report_type])." 
			    	target=\"_blank\"><i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>
			    PDF</a>
		    </li>`

		);
	});
");

$this->registerCss("
		//table { table-layout:fixed; word-break:break-all; word-wrap:break-word; }
		.table{
			width:100%;
			table-layout: fixed;
		}
	");
?>
<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$this->title = 'Occupancy Report';
// echo "<pre>";
// print_r($report);
// exit();
?>
<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - Country Occupancy Report (<?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?>)</span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
	                    	<div class="tab-pane active" id="occupancy-report">
                               <?php
                                    $gridColumns = [
                                        // ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'country',
                                            'value'     => function($data)
                                            {
                                                // echo "<pre>";
                                                // print_r($data);
                                                // exit();
                                                return $data['country'];
                                            }
                                        ],
                                        [
                                            'attribute' => 'bookings',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return $data['bookings'];
                                            }
                                        ],
                                        [
                                            'attribute' => 'total_invoice',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return isset($data['total_invoice'])?$data['total_invoice']:0;
                                            }
                                        ],
                                        [
                                            'attribute' => 'revenue_per_booking',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return $data['revenue_per_booking'];
                                            }
                                        ],
                                        [
                                            'attribute' => 'no_of_nights',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return isset($data['no_of_nights'])?$data['no_of_nights']:0;
                                            }
                                        ],
                                        [
                                            'attribute' => 'average_stay',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return isset($data['average_stay'])?$data['average_stay']:0;
                                            }
                                        ],
                                        [
                                            'label' => 'Guests Coming',
                                            'attribute' => 'no_of_guests',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return $data['no_of_guests'];
                                            }
                                        ],
                                        [
                                            'attribute' => 'guest_nights',
                                            'value'     => function($data)
                                            {
                                                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                                return $data['guest_nights'];
                                            }
                                        ],
                                    ];
                                    echo ExportMenu::widget([
                                    'showConfirmAlert' => false,    
                                    'dataProvider' => $dataProvider,
                                    'columns' => $gridColumns,
                                    'target' => ExportMenu::TARGET_BLANK,
                                    'exportConfig' => [
                                        ExportMenu::FORMAT_TEXT => false,
                                        ExportMenu::FORMAT_PDF => false,
                                        ExportMenu::FORMAT_HTML => false,
                                        ExportMenu::FORMAT_EXCEL => false,
                                        ExportMenu::FORMAT_CSV => [
                                            'label' => 'CSV',
                                        ],
                                        
                                        ExportMenu::FORMAT_EXCEL_X => [
                                            'label' => 'EXCEL',
                                        ],

                                    ],
                                    'fontAwesome' => true,
                                    'dropdownOptions' => [
                                        'label' => 'Export Report',
                                        'class' => 'btn btn-success'
                                    ],
                                    'enableFormatter' =>false,
                                    'showColumnSelector' => false,
                                    //'asDropdown' => false,
                                    'container'=>['class'=>'btn-group pull-left', 'style'=> 'margin: 5px'],
                                    'filename' => 'country-occupancy-report',
                                    'exportRequestParam' => 'hello;',

                                ]);
                                ?>
                                <br><br><br>
	               	           <?= GridView::widget([

                                'id' => 'bookings_gridview',
                                'dataProvider' => $dataProvider,
                                //'filterModel' => $searchModel,
                                'options' => ['class' => 'grid-view fixed-layout','Gridlines' => "None",'BorderStyle'=> "None"],
                                'layout'=>"\n{items}\n{pager}",
                                'columns' => [
                                    //'id',
                                    [
                                        'attribute' => 'country',
                                        'value'     => function($data)
                                        {
                                            // echo "<pre>";
                                            // print_r($data);
                                            // exit();
                                            return $data['country'];
                                        }
                                    ],
                                    [
                                        'attribute' => 'bookings',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return $data['bookings'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    [
                                        'attribute' => 'total_invoice',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return isset($data['total_invoice'])?Yii::$app->formatter->asDecimal($data['total_invoice']):0;
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    [
                                        'attribute' => 'revenue_per_booking',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return Yii::$app->formatter->asDecimal($data['revenue_per_booking']);
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    [
                                        'attribute' => 'no_of_nights',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return isset($data['no_of_nights'])?$data['no_of_nights']:0;
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    [
                                        'attribute' => 'average_stay',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return isset($data['average_stay'])?$data['average_stay']:0;
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    [
                                        'label' => 'Guests Coming',
                                        'attribute' => 'no_of_guests',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return $data['no_of_guests'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    [
                                        'attribute' => 'guest_nights',
                                        'value'     => function($data)
                                        {
                                            //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                            return $data['guest_nights'];
                                        },
                                        'contentOptions' => function($data)
                                        {
                                
                                            return ['style' => 'text-align:right;' ];
                                        },
                                    ],
                                    

                                ],
                                // 'afterRow' => function($model, $key, $index) {
                                //     //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                                //     if(isset($model->comments) && !empty($model->comments))
                                //     {
                                //         return "<tr style='border-top:0px !important;'><td colspan=10 style='white-space: normal;word-wrap:break-word !important;'>".$model->comments."</td></tr>";

                                //     }
                                        
                                // }
                                ]);?>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
$this->registerJs("
	$(document).ready(function(){
		$('#w3').append(
			`<li>
            	<a href=".yii::$app->getUrlManager()->createUrl(['standard-reports/country-occupancy-pdf',
            		'destination_id' => $destination_id,
			    	'report_to' => $report_to,
			    	'report_from' => $report_from,
			    	'cancelled_bookings' => $cancelled_bookings,
			    	'report_type' => $report_type])." 
			    	target=\"_blank\"><i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>
			    PDF</a>
		    </li>`

		);
	});
");

$this->registerCss("
		//table { table-layout:fixed; word-break:break-all; word-wrap:break-word; }
		.table{
			width:100%;
			table-layout: fixed;
		}
	");
?>
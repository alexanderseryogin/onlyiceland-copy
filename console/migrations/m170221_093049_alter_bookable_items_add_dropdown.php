<?php

use yii\db\Migration;

class m170221_093049_alter_bookable_items_add_dropdown extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `booking_confirmation_type_id` INT(11) NULL AFTER `min_age_adults`, ADD COLUMN `booking_status_id` INT(11) NULL AFTER `booking_confirmation_type_id`, ADD COLUMN `booking_type_id` INT(11) NULL AFTER `booking_status_id`, ADD CONSTRAINT `FK_bookable_items_booking_confirmation_type_id` FOREIGN KEY (`booking_confirmation_type_id`) REFERENCES `booking_confirmation_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_bookable_items_booking_status_id` FOREIGN KEY (`booking_status_id`) REFERENCES `booking_statuses`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_bookable_items_booking_type_id` FOREIGN KEY (`booking_type_id`) REFERENCES `booking_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170221_093049_alter_bookable_items_add_dropdown cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Destinations;
use common\models\BookableItems;

$this->title = "Booking Scheduler";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];

$session = Yii::$app->session;
$session->open();
$params = '';

if(isset($session[Yii::$app->controller->id.'-booking-schedular-values']) && !empty($session[Yii::$app->controller->id.'-booking-schedular-values']))
{
    $params = json_decode($session[Yii::$app->controller->id.'-booking-schedular-values'], true);
    unset($session[Yii::$app->controller->id.'-booking-schedular-values']);
    //print_r($params);
}

?>

<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	#calendar {
		
		margin: 0 auto;
	}

</style>
<div class="row calendar-div">
    <div class="col-md-12">
        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md" style="width: 100%;">
                    <div class="row">
                		<div class="col-sm-4">
	                        <label class="control-label">Destinations</label>
	                        <div class="form-group" >
	                            <select id="provider_dropdown" style="width: 100%;" class="form-control">
	                            	<option value=""></option>
	                                <?php
	                                    $destinations = ArrayHelper::map(Destinations::find()->where(['active' => 1])->all(),'id','name');

                                        if($destination_id != null )
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if($destination_id == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }
                                        }
                                        else
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if(!empty($params) && $params['provider_id'] == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }    
                                        }
	                                    
	                                ?>
	                            </select>
	                        </div>
	                    </div>
                        
                	</div>
                </div>
                <ul class="nav nav-tabs bookings_tabs">
                    <li class="active">
                        <!-- <a href="#general_info" data-toggle="tab">General info</a> -->
                    </li>
                </ul>
            </div>
            
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <center><div class="loader"></div></center><br>
                        	<div id="error_div" class="alert-danger alert fade in" style="display: none">
								No item found.
							</div>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
                <br>        
            </div>
        </div>
    </div>
</div>
<div id='calendar'></div>


<?php
// echo "<pre>";
// print_r($params);
// exit();

// else
// {
// 	echo "no params";
// 	exit();
// }
$get_destination_booked_items = Url::to(['/bookings/get-destination-booked-items-schedular']);
$edit_or_drag_booked_item = Url::to(['/bookings/edit-or-drag-booked-item-schedular']);
$create_booking_page = Url::to(['/bookings/create-booking-page-redirect']);

$this->registerJs("
	$(document).ready(function(){
		$('#provider_dropdown').select2({
	        placeholder: \"Select a Destination\",
	        allowClear: true,
	    });
	
		$(document).on('change','#provider_dropdown',function()
	    {
	    	onChnageDropdown();
	    	
	    });
	    
	});

	function onChnageDropdown()
    {
    	App.blockUI({target:\".loader\", animate: !0});

        var object = {
                    	'destination_id' : $('#provider_dropdown').val(),
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$get_destination_booked_items',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                   	$('#error_div').css('display','block');
                   	$('#calendar').hide();
                    //$('#item_dropdown').empty().html('');
                    App.unblockUI(\".loader\");
                }
                else
                {
                	console.log('date : '+".$date." );
                    var original_date =  '".$date."';
                    original_date =original_date.toString();
                    var date_arr = original_date.split('-');

                    var month = date_arr[2]-1;
                	var scroll = parseInt(month)*240;
                	console.log('scroll : '+ scroll);

                   	$('#error_div').css('display','none');
                   	$('#calendar').show();
                   // $('#item_dropdown').empty().html(data.bookable_items);
                    App.unblockUI(\".loader\");
                   	console.log(jQuery.parseJSON(data.bookings_items));
                   	console.log(jQuery.parseJSON(data.resources));
                   	console.log(jQuery.parseJSON(data.notifications));
                   	initFullCalendar(jQuery.parseJSON(data.bookings_items),jQuery.parseJSON(data.resources),data.date,data.closed_dates,jQuery.parseJSON(data.notifications),scroll,original_date);

       //             	$('.fc-scroller').animate({
					  //   scrollLeft: '+='+scroll+'px'
					  // }, 'slow');
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    }

    function initFullCalendar(bookings_items,resources,date,closed_dates,notifications,scroll,original_date)
    {
        console.log(bookings_items);
    	$('#calendar').fullCalendar( 'destroy' );

    	$('#calendar').fullCalendar({
			schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
			height: 700,
			//now: '2017-09-27',
			editable: true,
			aspectRatio: 1.8,
			scrollTime: '00:00',
			slotWidth: 250,
			// filterResourcesWithEvents: true, 
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'timelineDay,timelineWeek,timelineThreeDays,timelineMonth'
			},
			defaultView: 'timelineMonth',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 14 }
				}
			},
			groupByResource: true,
			resourceAreaWidth: '30%',
			resourceColumns: [
				{
					group: true,
					labelText: 'Bookable Item',
					field: 'item',
					width: 250,
				},
				{
					// group: true,
					labelText: 'Unit',
					field: 'title',
					width: 150
				},
				// {
				// 	labelText: 'Occupancy',
				// 	field: 'occupancy'
				// }
			],
			resourceGroupField: 'id',
			resources: resources,
			events: bookings_items,
			eventResizeStart: function (event, jsEvent, ui, view) 
            {
                before_start = event.start.format();
                before_end = event.end.format();
                //console.log('RESIZE Before!! ' + before_start + ' to '+ before_end);
            },
            eventResize: function(event, delta, revertFunc) 
            {
                console.log('RESIZE After!! ' + event.start.format() + ' to '+ event.end.format());
        
                editOrDragBookingItem(event,revertFunc);
            },
            eventDrop: function(event, delta, revertFunc) 
            {
                editOrDragBookingItem(event,revertFunc);
            },
            dayRender: function (date, cell) {

                var d = new Date(date);
                
                console.log('in day render');
                console.log()

                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();
                //console.log(typeof(notifications));
                // console.log('curr_date'+curr_date + 'curr_month' + curr_month + 'curr_year' + curr_year);
                for (var i = 0; i < notifications.length; i++) 
                {
                    var new_c_date = new Date(notifications[i]);
                    var new_date = new_c_date.getDate();
                    var new_month = new_c_date.getMonth();
                    var new_year = new_c_date.getFullYear();

                    if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                    {
                        //console.log('multiple bookings');
                        cell.css('background-color','#DB7070');
                        
                         
                    }

                }

                for (var i = 0; i < closed_dates.length; i++) 
                {
                    var new_c_date = new Date(closed_dates[i]);
                    var new_date = new_c_date.getDate();
                    var new_month = new_c_date.getMonth();
                    var new_year = new_c_date.getFullYear();

                    if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                    {
                        console.log('exception_dates');
                        cell.css('background-color','red');
                        
                        
                        cell.prepend(`<span style='font-size:15px;margin-left:20px;'><b>Closed for new bookings</b></span>`);
                        // cell.prepend(`<a class='add_event_label' style='position:relative;top:0 ;left:0;display: block;font-size:12px;color:#000;cursor:pointer;'>(+)Add Event</a>`);                    
                    }
                    // else
                    // {
                    //     cell.prepend(`<a class='remove_event_label' style='position:relative;top:0;left:0;display: block;font-size:12px;color:#000;cursor:pointer;'>(-)Remove Event</a>`);
                    // }
                }
                
            },
            dayClick: function(date, jsEvent, view) 
            {   
                // var acn = jsEvent.target;
                // console.log(acn.getAttribute('class'));
                // if( acn.getAttribute('class') == 'add_event_label fa fa-times' || acn.getAttribute('class') == 'remove_event_label fa fa-check') 
                // {
                //     console.log(this);
                //     console.log(jsEvent.target);
                //     return;
                // }
                var check_flag = 0;
                var d = new Date(date);
                
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();

                for (var i = 0; i < closed_dates.length; i++) 
                {
                    var new_c_date = new Date(closed_dates[i]);
                    var new_date = new_c_date.getDate();
                    var new_month = new_c_date.getMonth();
                    var new_year = new_c_date.getFullYear();

                    if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                    {
                        check_flag = 1;
                    }
                }
                console.log(view.name);
                if(view.name == 'timelineMonth' && check_flag == 1) {
                    alert('you cannot book on this day');
                }
                else if(view.name == 'timelineMonth' && check_flag == 0)
                {
                    createBookingPage(date);
                }
                else
                {

                }


            },
            resourceRender: function(resourceObj, labelTds, bodyTds) {
            	//console.log('res obj : '+resourceObj.background_color);
            	//typeof(resourceObj.background_color);
            	if (typeof(resourceObj.background_color) === 'undefined') 
            	{
				   
				}
            	else
            	{
                   // console.log(labelTds);
            		labelTds.parent().css('background', resourceObj.background_color);
            		labelTds.css('background', resourceObj.background_color);
                    labelTds.children().children().css('color', resourceObj.text_color);
                    labelTds.parent().css('color', resourceObj.text_color);
            	}
			    
			}
		});
        $('#calendar').fullCalendar('gotoDate',original_date);
		$('.fc-scroller').animate({
	    	scrollLeft: '+='+scroll+'px'
	  	}, 'slow');
    }

    function editOrDragBookingItem(event,revertFunc)
    {
    	var resource = $('#calendar').fullCalendar( 'getEventResource', event );
    	console.log(resource);
    	//console.log($('#calendar').fullCalendar( 'getEventResource', event ))
        // if(confirm('Are you sure about this change?')) 
        // {
            var object = 
                {
                    'id': event.id,
                    'bookable_item_id':resource.bookable_item_id,
                    'item_name_id': resource.id,
                    'arrival_date' : event.start.format(),
                    'departure_date' : event.end.format(),

                };

            $.ajax(
            {
                type: 'POST',
                data: object,
                url: '$edit_or_drag_booked_item',
                success: function(data)
                {

                    if(data=='false')
                    {
                        //alert('This rate is not available '+event.start.format() +' - '+event.end.format());
                        alert('The rate is not available');
                        revertFunc();
                    }
                    else if(data == 'date_exception')
                    {
                        alert('you cannot book on this day');
                        revertFunc();
                    }
                    else
                    {
                    	data = jQuery.parseJSON(data);
                    	console.log(data.notifications);
                    	
                    	console.log('in else');

                    	// console.log('scroll date : '+data.first_date);
                    	// var scroll = parseInt(data.first_date)*240;
                    	// console.log('scroll : '+scroll);

                        original_date = (data.first_date).toString();
                        var date_arr = original_date.split('-');

                        var month = date_arr[2]-1;
                        var scroll = parseInt(month)*240;
                        console.log('scroll : '+ scroll);

                    	initFullCalendar(data.bookings_items,data.resources,data.date,data.closed_dates,data.notifications,scroll,original_date);

                    	//var noTime = $.fullCalendar.moment('2017-09-27');
                    	//$('#calendar').fullCalendar('gotoDate',noTime);
                    	//$('#calendar').fullCalendar('today');
                    	
                    }
                },
                error: function()
                {
                    alert('error');
                }
            });
       // }
        // else
        // {
        //     revertFunc();
        // }
    }

    function createBookingPage(date)
    {
        var object = 
            {
                'provider_id': $('#provider_dropdown').val(),
                'arrival_date' : date.format(),
            };

        $.ajax(
        {
            type: 'POST',
            data: object,
            url: '$create_booking_page',
            success: function(data)
            {
                alert('success');
            },
            error: function()
            {
            }
        });
    }
");

if($destination_id != null)
{
	$this->registerJs("
	    	$(document).ready(function(){
	    	
	    		onChnageDropdown();
	    	});
	    	
	    	//$('#provider_dropdown').trigger('change');
	    	//$('#provider_dropdown').change();
	    ");
}

else
{
	if(!empty($params) && isset($params['provider_id']) )
	{
	    $this->registerJs("
	    	$(document).ready(function(){
	    		console.log('param exists'+".$params['provider_id'].");
	    		onChnageDropdown();
	    	});
	    	
	    	//$('#provider_dropdown').trigger('change');
	    	//$('#provider_dropdown').change();
	    ");
	}
}
$this->registerCss("
	#calendar {
		max-height: 1000px;
		margin: 50px auto;
	}
	");


?>
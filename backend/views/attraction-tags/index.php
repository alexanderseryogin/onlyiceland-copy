<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AttractionTagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Attraction Tags');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attraction-tags-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW ATTRACTION TAG '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'attraction-tags-gridview','timeout' => 10000, 'enablePushState' => false]); ?>      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

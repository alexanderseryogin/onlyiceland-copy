<?php
use common\models\BookingDates;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<?= GridView::widget([

    'id' => 'bookings_gridview',
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'options' => ['class' => 'grid-view','Gridlines' => "None",'BorderStyle'=> "None" ],
    'layout'=>"\n{items}",
    'columns' => [
        [
            'attribute' => 'Arrival',
            'value'     => function($data)
            {
                $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return date('d.m.Y',strtotime($data->arrival_date));
            }
        ],
        [
            'attribute' => 'Departure',
            'value'     => function($data)
            {
                $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return date('d.m.Y',strtotime($data->departure_date));
            }
        ],
        [
            'attribute' => '#',
            'value'     => function($data)
            {
                $date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
                $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
                $diff=date_diff($date1,$date2);
                $difference =  $diff->format("%a");
                return $difference;
                // $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                // return $booking_date->no_of_nights;
            }
        ],
        [
            'label' => '?',
            'value' => function($data)
            {
                if(strtotime($data->departure_date) < strtotime(date('Y-m-d')))
                {
                    return 0;
                }
                else
                {
                    $date1 = new DateTime(date('Y-m-d'));
                    $date2 = new DateTime($data->departure_date);

                    $diff = $date2->diff($date1)->format("%a");
                    return $diff;   
                }
                
            }
        ],
        [
            'format' => 'raw',
            'attribute' => 'Room Type',
            'value'     => function($data)
            {
                $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return $data->item->itemType->name;
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
            },
        ],
        [
            'format' => 'raw',
            'attribute' => 'Unit Name',
            'value'     => function($data)
            {
                $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return isset($data->item_name_id)?$data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:''):"- - - - - ";
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'white-space: normal;word-wrap:break-word !important;' ];
            },
        ],
        [
            'attribute' => 'Guest Name',
            'value'     => function($data)
            {
                $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return ($booking_date->guest_first_name != NULL && $booking_date->guest_last_name != NULL)?$booking_date->guest_first_name.' '.$booking_date->guest_last_name :"- - - - -";
            }
        ],
        [
            'label' => 'A/C',
            'attribute' => 'A/C',
            'value'     => function($data)
            {
                $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return $booking_date->no_of_adults.'/'.$booking_date->no_of_children;
            }
        ],
        [
            'attribute' => 'Duration',
            'value'     => function($data)
            {
                $date1=date_create(date('Y-m-d', strtotime($data->arrival_date)));
                $date2=date_create(date('Y-m-d', strtotime($data->departure_date)));
                $diff=date_diff($date1,$date2);
                $difference =  $diff->format("%a");
                // return $difference;
                // $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return 'Staying '.$difference.(($difference >1)?' Nights':' Night');
                // $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                // return 'Staying '.$booking_date->no_of_nights.(($booking_date->no_of_nights >1)?' Nights':' Night');
            }
        ],
        [
            'label' => 'Balance Due',
            'attribute' => 'balance',
            'value'     => function($data)
            {
                
                return Yii::$app->formatter->asDecimal($data->balance);
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
        ]
 

    ],
    'tableOptions' =>['class' => 'table table-striped table-bordered', 'style' =>'width:100%;table-layout: fixed;'],
    'afterRow' => function($model, $key, $index) {
        //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
        if(isset($model->comments) && !empty($model->comments))
        {
            return "<tr style='border-top:0px !important;'><td colspan=10 style='word-wrap:break-word !important;'>".$model->comments."</td></tr>";
            // return Html::tag('tr',
            //     Html::tag('td', $model->comments,['colspan' => 8]),
            //     ['style' => 'border-top: none !important;']
            //     // .Html::tag('td', $model->father_dob)
            //     //add more columns
            // );
        }
            
    }
    ]);?>

<p style="font-size: 12px">Total Occupied Rooms : <?=$count?></p>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
<?php

use yii\db\Migration;

class m170427_103917_alter_booking_group extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_group` ADD COLUMN `confirmation_id` INT(11) NULL AFTER `group_guest_user_id`, ADD COLUMN `cancellation_id` INT(11) NULL AFTER `confirmation_id`, ADD COLUMN `flag_id` INT(11) NULL AFTER `cancellation_id`, ADD COLUMN `status_id` INT(11) NULL AFTER `flag_id`, ADD COLUMN `travel_partner_id` INT(11) NULL AFTER `status_id`, ADD COLUMN `offers_id` INT(11) NULL AFTER `travel_partner_id`, ADD COLUMN `estimated_arrival_time_id` INT(11) UNSIGNED NULL AFTER `offers_id`, ADD CONSTRAINT `FK_booking_group_confirmation_id` FOREIGN KEY (`confirmation_id`) REFERENCES `booking_confirmation_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_booking_group_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_booking_group_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_booking_group_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `travel_partner`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_booking_group_offers_id` FOREIGN KEY (`offers_id`) REFERENCES `offers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_booking_group_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170427_103917_alter_booking_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

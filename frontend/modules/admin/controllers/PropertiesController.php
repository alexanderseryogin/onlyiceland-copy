<?php

namespace frontend\modules\admin\controllers;

use Yii;
use frontend\modules\admin\models\Properties;
use frontend\modules\admin\models\PropertiesSearch;
use frontend\modules\admin\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Cities;
use common\models\States;
use common\models\Countries;

/**
 * PropertiesController implements the CRUD actions for Properties model.
 */
class PropertiesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Properties models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Properties model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Properties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Properties();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {

            if($model->same_address){
                $model->business_address = $model->address;
                $model->business_postal_code = $model->postal_code;
                $model->business_city_id = $model->city_id;
                $model->business_state_id = $model->state_id;
                $model->business_country_id = $model->country_id;
            }

            $model->payment_methods_for_gauarantee = implode(",", $post['Properties']['payment_methods_for_gauarantee']);
            $model->payment_methods_checkin = implode(",", $post['Properties']['payment_methods_checkin']);
            
            if($model->save()){
                return $this->redirect(['index']);
            }
        } 
            
        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Updates an existing Properties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        
        $model->payment_methods_for_gauarantee = explode(",", $model->payment_methods_for_gauarantee);
        $model->payment_methods_checkin = explode(",", $model->payment_methods_checkin);

        if ($model->load($post)) {

            if($model->same_address){
                $model->business_address = $model->address;
                $model->business_postal_code = $model->postal_code;
                $model->business_city_id = $model->city_id;
                $model->business_state_id = $model->state_id;
                $model->business_country_id = $model->country_id;
            }

            $model->payment_methods_for_gauarantee = implode(",", $post['Properties']['payment_methods_for_gauarantee']);
            $model->payment_methods_checkin = implode(",", $post['Properties']['payment_methods_checkin']);
            
            if($model->save()){
                return $this->redirect(['index']);
            }
        } 
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Properties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Properties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Properties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Properties::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListstates($country_id){
        $states = States::find()
                ->where(['country_id' => $country_id])
                ->all();
        echo "<option> - Choose a State - </option>";
        foreach ($states as $state) {
            echo "<option value='".$state->id."'>".$state->name."</option>";  
        }        
    }

    public function actionListcities($state_id){
        $cities = Cities::find()
                ->where(['state_id' => $state_id])
                ->all();
        echo "<option> - Choose a City - </option>";
        foreach ($cities as $city) {
            echo "<option value='".$city->id."'>".$city->name."</option>";  
        }        
    }
}

<?php
namespace backend\components;

use common\models\User;
use common\models\UserProfile;
use common\models\PropertyPaymentMethods;
use common\models\BookingOverrideStatuses;

class Helpers
{
    /**
    * status html or lables
    **/
    public static function statusLables($status, $type = null){
        
        $html = '';

        if(isset($status)){
            switch($status){
                case User::STATUS_DELETED:
                    $html = isset($type) ? 'Inactive' : '<span class="label label-default">Inactive</span>';
                break;
                case User::STATUS_ACTIVE:
                    $html = isset($type) ? 'Active' : '<span class="label label-primary">Active</span>';
                break; 
            }
        }
    
        return $html;
    }

    /**
    * status html or lables
    **/
    public static function ProfileTitleTypes($type){
        
        $html = '';

        if(isset($type)){
            switch($type){
                case UserProfile::TITLE_EMPTY:
                    $html = 'Select Title';
                break;
                case UserProfile::TITLE_MR:
                    $html = 'Mr.';
                break;
                case UserProfile::TITLE_MS:
                    $html = 'Ms.';
                break;
                case UserProfile::TITLE_MRS:
                    $html = 'Mrs.';
                break; 
                case UserProfile::TITLE_DR:
                    $html = 'Dr.';
                break;  
            }
        }
    
        return $html;
    }

    /**
    * status html or lables
    **/
    public static function presetLables($reset_status, $type = null){
        
        $html = '';

        if(isset($reset_status)){
            switch($reset_status){
                case User::INITIAL_PASSWORD_RESET_PENDING:
                    $html = isset($type) ? 'Pending' : '<span class="label label-danger">Pending</span>';
                break;
                case User::INITIAL_PASSWORD_RESET_COMPLETE:
                    $html = isset($type) ? 'Complete' : '<span class="label label-success">Complete</span>';
                break; 
            }
        }
    
        return $html;
    }
    
    /**
    * user type html or lables
    **/
    public static function userLables($uType, $type = null){
        
        $html = '';
        
        if(isset($uType)){
            switch($uType){
                case User::USER_TYPE_ADMIN:
                    $html = isset($type) ? 'Administrator' : '<span class="label label-info">Administrator</span>';
                break;
                case User::USER_TYPE_PMANAGER:
                    $html = isset($type) ? 'Destination Manager' : '<span class="label label-warning">Destination Manager</span>';
                break;
                case User::USER_TYPE_USER:
                    $html = isset($type) ? 'User' : '<span class="label label-success">User</span>';
                break;
                case User::USER_TYPE_STAFF:
                    $html = isset($type) ? 'Staff' : '<span class="label label-danger">Staff</span>';
                break;
                case User::USER_TYPE_OWNER:
                    $html = isset($type) ? 'Owner' : '<span class="label label-primary">Owner</span>';
                break;
            }
        }

        return $html;
    }

    public static function AmenitiesLables($aType){
        
        $html = '';
        
        if(isset($aType)){
            switch($aType){
                case 0:
                    $html = '<span class="label label-success">Free</span>';
                break;
                case 1:
                    $html = '<span class="label label-warning">Available at extra cost</span>';
                break;
                case 2:
                    $html = '<span class="label label-danger">Not available</span>';
                break;
                case 3:
                    $html = '<span class="label label-primary">Do not display</span>';
                break;
                case 4:
                    $html = '<span class="label label-default">Display without comment</span>';
                break;
            }
        }

        return $html;
    }

    /**
    * user type html or lables
    **/
    public static function propertyPaymentMethodLables($mType, $type = null){
        
        $html = '';
        
        if(isset($mType)){
            switch($mType){
                case PropertyPaymentMethods::TYPE_GUARANTEE:
                    $html = isset($type) ? 'Guarantee' : '<span class="label label-info">Guarantee</span>';
                break;
                case PropertyPaymentMethods::TYPE_CHECKIN:
                    $html = isset($type) ? 'Checkin' : '<span class="label label-primary">Checkin</span>';
                break;
                case PropertyPaymentMethods::TYPE_GUARANTEE_CHECKIN:
                    $html = isset($type) ? 'Guarantee + Checkin' : '<span class="label label-success">Guarantee + Checkin</span>';
                break;
            }
        }

        return $html;
    }

    public static function overridestatusesLables($uType, $type = null){
        
        $html = '';
        
        if(isset($uType)){
            switch($uType){
                case BookingOverrideStatuses::TYPE_CUSTOM:
                    $html = isset($type) ? 'Custom' : '<span class="label label-info">Custom</span>';
                break;
                case BookingOverrideStatuses::TYPE_DEFAULT:
                    $html = isset($type) ? 'Default' : '<span class="label label-warning">Default</span>';
                break;
    
            }
        }

        return $html;
    }
}
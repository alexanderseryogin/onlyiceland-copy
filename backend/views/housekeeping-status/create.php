<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HousekeepingStatus */

$this->title = 'Create New Housekeeping Status';
$this->params['breadcrumbs'][] = ['label' => 'Housekeeping Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="housekeeping-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
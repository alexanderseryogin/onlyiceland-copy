<?php

namespace metronic\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ProviderAdminUserProfileAsset extends AssetBundle
{
    public $sourcePath = '@metronic/resources';
    public $css = [

        'pages/css/profile.min.css',
        'external-plugins/intl-tel-input/build/css/intlTelInput.css',
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [
        'external-plugins/intl-tel-input/build/js/intlTelInput.min.js',
        'external-plugins/intl-tel-input/build/js/utils.js'
    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
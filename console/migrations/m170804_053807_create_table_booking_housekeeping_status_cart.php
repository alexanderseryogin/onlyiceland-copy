<?php

use yii\db\Migration;

class m170804_053807_create_table_booking_housekeeping_status_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_housekeeping_status_cart`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `booking_item_id` INT(11) UNSIGNED,
                          `housekepping_status_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items_cart`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
                          FOREIGN KEY (`housekepping_status_id`) REFERENCES `housekeeping_status`(`id`) ON UPDATE CASCADE ON DELETE SET NULL
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170804_053807_create_table_booking_housekeeping_status_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

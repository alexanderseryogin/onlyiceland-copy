<?php

use yii\db\Migration;

class m170615_074223_alter_table_bookings_dates_change_travel_partner_id_constraint extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_dates`  
                          DROP FOREIGN KEY `FK_booking_dates_travel_partner_id`;
                        ');
        $this->execute('ALTER TABLE `booking_dates`   
                          CHANGE `travel_partner_id` `travel_partner_id` INT(11) UNSIGNED NULL;
                        ');
        // $this->execute('ALTER TABLE `booking_dates`  
        //                   ADD CONSTRAINT `FK_booking_dates_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `destination_travel_partners`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
        //                 ');
    }

    public function down()
    {
        echo "m170615_074223_alter_table_bookings_dates_change_travel_partner_id_constraint cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161012_122333_alter_table_pm_checkin extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `property_pm_checkin` CHANGE `property_id` `provider_id` INT(11) NOT NULL, CHANGE `property_pm_id` `pm_id` INT(11) NOT NULL, DROP INDEX `FK_property_pm_checkin_property_id`, DROP INDEX `FK_property_pm_checkin_property_pm_id`, DROP FOREIGN KEY `FK_property_pm_checkin_property_id`, DROP FOREIGN KEY `FK_property_pm_checkin_property_pm_id`, ADD CONSTRAINT `FK_pm_checkin_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE, ADD CONSTRAINT `FK_pm_checkin_pm_id` FOREIGN KEY (`pm_id`) REFERENCES `payment_methods`(`id`) ON DELETE CASCADE; 

            RENAME TABLE `property_pm_checkin` TO `payment_method_checkin`;
        ");
    }

    public function down()
    {
        echo "m161012_122333_alter_table_pm_checkin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$bookableItems = $destination->bookableItems;
// echo "<pre>";
// print_r($bookableItems[0]->itemType->name);
// exit;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<?php
foreach ($reports as $report) 
{
    // $width = 100;
    // if(count($report)<3)
    // {
    //     $count = 3-count($report)+1;
    //     $width = $count*20;
    //     $width +=40;
    // }
?>
<table class="table table-striped table-bordered">
    <tbody>
     <?php
        $total_arr = array();
        $total_arr['guests_count'] = 0;
        $total_arr['booked_count'] = 0;
        $total_arr['avail_count'] = 0;
        $total_arr['occupancy_count'] = 0;

        for ($i=0; $i < count($destination->bookableItems)+4 ; $i++) 
        { 
            if($i ==  0)
            {
                echo "<tr >";

                
                
                echo "<td style='width:300px'></td>";
                foreach ($report as $key => $value) 
                {
                    echo "<td colspan='4' style='border-right: none !important;width:300px;text-align:center;' >".$key."</td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    
                }
                echo "<td colspan='4' style='border-right: none !important;width:300px;text-align:center;' >Total</td>";
                if(count($report)<3)
                {
                    $count = 3-count($report);
                    for ($j=0; $j <$count ; $j++) 
                    { 
                        echo "<td colspan='4' style='border-right: none !important;width:300px' ></td>";
                    // echo "<td></td>";
                    }
                }
                echo "</tr>";
            }   
            elseif($i == 1)
            {
                echo "<tr>";
                echo "<td></td>";
                foreach ($report as $key => $value) 
                {
                    echo "<td>Guests</td>";
                    echo "<td>Booked</td>";
                    echo "<td>Available</td>";
                    echo "<td>Occupancy</td>";
                }
                echo "<td>Guests</td>";
                echo "<td>Booked</td>";
                echo "<td>Available</td>";
                echo "<td>Occupancy</td>";
                echo "</tr>";
            }
            elseif($i == 2)
            {
                echo "<tr>";
                echo "<td><b>".$destination->name."</b></td>";
                foreach ($report as $key => $value) 
                {
                    echo "<td colspan='4' style='border-right: none !important;'></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    
                }
                echo "<td colspan='4' style='border-right: none !important;'></td>";
                // echo "<td colspan='4' style='border-right: none !important;'>total</td>";
                echo "</tr>";
            }
            else
            {
                

                if($i < count($destination->bookableItems)+3)
                {
                    $b_item = $bookableItems[$i-3];
                    echo "<tr>";
                    echo "<td width='250px;'>".$b_item->itemType->name."</td>";
                    // echo "<pre>";
                    //     print_r($report);
                    //     exit();
                    $guests_count = 0;
                    $booked_count = 0;
                    $avail_count = 0;
                    $guests_occupancy = 0;
                    foreach ($report as $key => $value) 
                    {
                        $item = $value[$b_item->itemType->name];
                        echo "<td style='text-align:right'>".$item['guests']."</td>";
                        echo "<td style='text-align:right'>".$item['booked']."</td>";
                        echo "<td style='text-align:right'>".$item['avail']."</td>";
                        echo "<td style='text-align:right'>".round($item['occupancy'],2).' %' ."</td>";
                        $guests_count += $item['guests'];
                        $booked_count += $item['booked'];
                        $avail_count += $item['avail'];
                        $guests_occupancy += round($item['occupancy'],2);
                    }
                    $total_arr['guests_count'] += $guests_count;
                    $total_arr['booked_count'] += $booked_count;
                    $total_arr['avail_count'] += $avail_count;
                    $total_arr['occupancy_count'] += round($guests_occupancy/count($report),2);

                    echo "<td style='text-align:right'>".$guests_count."</td>";
                    echo "<td style='text-align:right'>".$booked_count."</td>";
                    echo "<td style='text-align:right'>".$avail_count."</td>";
                    echo "<td style='text-align:right'>".round($guests_occupancy/count($report),2).' %'."</td>";
                    echo "</tr>";

                }
                else
                {
                    // echo "<pre>";
                    // print_r($total_arr);
                    echo "<tr>";
                    echo "<td>Total</td>";
                    // echo "<pre>";
                    // print_r($report);
                    // exit();
                    foreach ($report as $key => $value) 
                    {
                        $guests_count = 0;
                        $booked_count = 0;
                        $avail_count = 0;
                        $guests_occupancy = 0;
                        // echo "<pre>";
                        // print_r($value);
                        // exit();
                        foreach ($destination->bookableItems as $item) 
                        {
                            $guests_count += $value[$item->itemType->name]['guests'];
                            $booked_count += $value[$item->itemType->name]['booked'];
                            $avail_count += $value[$item->itemType->name]['avail'];
                            $guests_occupancy += round($value[$item->itemType->name]['occupancy'],2);

                            // echo "g count ".$guests_count."<br>";
                            // echo "b count ".$booked_count."<br>";
                            // echo "a count ".$avail_count."<br>";
                            // echo "o count ".$guests_occupancy."<br>";
                        }
                        // exit();
                        echo "<td style='text-align:right'>".$guests_count."</td>";
                        echo "<td style='text-align:right'>".$booked_count."</td>";
                        echo "<td style='text-align:right'>".$avail_count."</td>";

                        $total_rooms = 0;
                        foreach ($destination->bookableItems as  $value)
                        {
                            $total_rooms = $total_rooms + $value->item_quantity; 
                        }
                        echo "<td style='text-align:right'>".round(($booked_count/$total_rooms)*100,2).' %'."</td>";
                        // $guests_count += $item['guests'];
                        // $booked_count += $item['booked'];
                        // $avail_count += $item['avail'];
                        // $guests_occupancy += round($item['occupancy'],2);
                    }
                    echo "<td style='text-align:right'>".$total_arr['guests_count']."</td>";
                    echo "<td style='text-align:right'>".$total_arr['booked_count']."</td>";
                    echo "<td style='text-align:right'>".$total_arr['avail_count']."</td>";
                    $total_rooms = 0;
                    foreach ($destination->bookableItems as  $value)
                    {
                        $total_rooms = $total_rooms + $value->item_quantity; 
                    }
                    echo "<td style='text-align:right'>".round(($total_arr['booked_count']/($total_arr['booked_count']+$total_arr['avail_count']))*100,2).' %'."</td>";
                    echo "</tr>";

                }
                                                                    
            }
            
    
        }
    ?>
    </tbody>
</table>
<?php
    echo "<br>";
}
?>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
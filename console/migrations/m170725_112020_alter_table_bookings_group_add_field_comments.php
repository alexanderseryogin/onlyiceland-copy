<?php

use yii\db\Migration;

class m170725_112020_alter_table_bookings_group_add_field_comments extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group`   
                          ADD COLUMN `comments` LONGTEXT NULL AFTER `balance`;
                        ');
    }

    public function down()
    {
        echo "m170725_112020_alter_table_bookings_group_add_field_comments cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\BookingStatuses */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="portlet light ">
<div class="booking-statuses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?php 
    	if(empty($model->background_color) && $model->isNewRecord)
    	{
    		$model->background_color='ff6161';
    	}
    	if(empty($model->text_color) && $model->isNewRecord)
    	{
    		$model->text_color='ff6161';
    	}
    ?>

    <label class="control-label">Background Color</label>
    <div class="form-group">
        <input type="text" id="hue-demo1" name="BookingStatuses[background_color]" class="form-control demo" data-control="hue"  value=<?=$model->background_color?>> 
    </div>

    <label class=" control-label">Text Color</label>
    <div class="form-group">
        <input type="text" id="hue-demo3" name="BookingStatuses[text_color]" class="form-control demo" data-control="hue" value=<?=$model->text_color?>>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/booking-statuses']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

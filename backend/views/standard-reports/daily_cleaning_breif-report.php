<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
use common\models\BookingsItems;

$title = 'Daily Cleaning Brief Report';
$this->title = $title;
?>

<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - <?=$title?> (<?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?>)</span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                      

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
                        	<div class="btn-group pull-left" style="margin: 5px">
                                <div class="btn-group">
                                <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                                    
                                </span></button>

                                    <ul id="w3" class="dropdown-menu">
                                        <li title="Comma Separated Values"><a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                        <li title="Microsoft Excel 2007+ (xlsx)"><a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                        <li>
                                            <a href="<?=yii::$app->getUrlManager()->createUrl(['standard-reports/daily-cleaning-brief-report-pdf','destination_id' => $destination_id,
                                                'report_to' => $report_to,
                                                'report_from' => $report_from,
                                                'cancelled_bookings' => $cancelled_bookings,
                                                'report_type' => $report_type,
                                                ])?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            PDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
	                    	<div class="tab-pane active" id="arrival-report">
	                    		<table class="table table-striped table-bordered daily_unit_report_table table2excel" >
	                    			<thead style="display: none;">
	                    				<tr>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    					<td>
	                    					</td>
	                    				</tr>
	                    				
	                    			</thead>
	                    			<tbody>
	                    			
				                    	<?php
				                    		$date = $report_from;
									       
									        $date1=date_create($report_to);
									        $date2=date_create($report_from);
									        $diff=date_diff($date1,$date2);
									        $difference =  $diff->format("%a");

									        for ($i=0; $i <= $difference ; $i++)
									        {
									            // $data_array = array();
									            // $average_stay = 0.0;

									            echo "<tr>";
										            echo "<td  style='font-size:25px;border-right: 0px;'>";
										            echo date('d.m.Y',strtotime($date));
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-left:0px;'>";
										            echo "</td>";
									            echo "</tr>";
									            if($cancelled_bookings)
									            {
									            	$bookings = BookingsItems::find()->where(['or',['arrival_date'=> $date],['departure_date'=>$date]])->andWhere(['deleted_at' => 0 ])->all();

									                $arrivals = BookingsItems::find()->where(['arrival_date' => $date])->andWhere(['deleted_at' => 0 ])->count();
									                $departures = BookingsItems::find()->where(['departure_date' => $date])->andWhere(['deleted_at' => 0 ])->count();


									                echo "<tr>";
										            echo "<td style='border-right: 0px;'>";
										            echo "<b>".$arrivals.' Arrivals'.' /'.$departures.' Departures'."</b>";
										            echo "</td>";
										            
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-left:0px;'>";
										            echo "</td>";
										            echo "</tr>";
									                if(!empty($bookings))
									                {
									                	echo "<tr>";
												            echo "<td>";
												            echo "<b>Item Name</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Item Type</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Bed Combination</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>A/C</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Booking #</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>No of nights</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Arrival</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Departure</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Notes</b>";
												            echo "</td>";
											            echo "</tr>";
									                	foreach ($bookings as $booking) 
									                	{
									                		echo "<tr>";

									                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
										                    if(!empty($booking->item_name_id))
										                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
										                    else
										                         $unit_name = '- - - - -'.' ';

												           
											            	echo "<td>";
												            echo $unit_name;
												            echo "</td>";

												            echo "<td>";
												            echo $booking->item->itemType->name;
												            echo "</td>";

												            $bookingDateModel = $booking->FirstBookingDate();

										                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations))
										                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
										                    else
										                         $bed_pref = '- - - - -'.' ';

												            echo "<td >";
												            echo $bed_pref;
												            echo "</td>";

												            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

												            echo "<td style='text-align:right'>";
												            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo $booking->id;
												            echo "</td>";


									                		$date1=date_create($booking->departure_date);
										                    $date2=date_create($booking->arrival_date);
										                    $diff=date_diff($date1,$date2);
										                    $difference1 =  $diff->format("%a");

									                		
											            	echo "<td style='text-align:right'>";
												            echo $difference1;
												            echo "</td>";
											            	
											            	echo "<td style='text-align:right'>";
												            echo date('d.m.Y', strtotime($booking->arrival_date));
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo date('d.m.Y', strtotime($booking->departure_date));
												            echo "</td>";
											            	
											            	echo "<td style='width: 15%;'>";
												            // echo ;
												            echo "</td>";

												            echo "</tr>";

									                	}
									                }


									                $bookings = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['deleted_at' => 0 ])->all();

									                $stay_through_count = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['deleted_at' => 0 ])->count();

									                echo "<tr>";
										            echo "<td style='border-right: 0px;'>";
										            echo "<b>".$stay_through_count.' Staying through'."</b>";
										            echo "</td>";
										            
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-left:0px;'>";
										            echo "</td>";
										            echo "</tr>";

											            

									                if(!empty($bookings))
									                {
									                	echo "<tr>";
												            echo "<td>";
												            echo "<b>Item Name</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Item Type</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Bed Combination</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>A/C</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Booking #</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>No of nights</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Arrival</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Departure</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Notes</b>";
												            echo "</td>";
											            echo "</tr>";
									                	foreach ($bookings as $booking) 
									                	{
									                		echo "<tr>";

									                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
										                    if(!empty($booking->item_name_id))
										                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
										                    else
										                         $unit_name = '- - - - -'.' ';

												           
											            	echo "<td>";
												            echo $unit_name;
												            echo "</td>";

												            echo "<td>";
												            echo $booking->item->itemType->name;
												            echo "</td>";

												            $bookingDateModel = $booking->FirstBookingDate();

										                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations) )
										                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
										                    else
										                         $bed_pref = '- - - - -'.' ';

												            echo "<td >";
												            echo $bed_pref;
												            echo "</td>";

												            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

												            echo "<td style='text-align:right'>";
												            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo $booking->id;
												            echo "</td>";


									                		$date1=date_create($booking->departure_date);
										                    $date2=date_create($booking->arrival_date);
										                    $diff=date_diff($date1,$date2);
										                    $difference1 =  $diff->format("%a");

									                		
											            	echo "<td style='text-align:right'>";
												            echo $difference1;
												            echo "</td>";
											            	
											            	echo "<td>";
												            echo date('d.m.Y', strtotime($booking->arrival_date));
												            echo "</td>";

												            echo "<td>";
												            echo date('d.m.Y', strtotime($booking->departure_date));
												            echo "</td>";
											            	
											            	echo "<td style='width: 15%;'>";
												            // echo ;
												            echo "</td>";

												            echo "</tr>";
									                	}
									                }


									                
									            }
									            else
									            {
									                
									                $bookings = BookingsItems::find()->where(['or',['arrival_date'=> $date],['departure_date'=>$date]])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->all();

									                $arrivals = BookingsItems::find()->where(['arrival_date' => $date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->count();
									                $departures = BookingsItems::find()->where(['departure_date' => $date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->count();

									                echo "<tr>";
										            echo "<td style='border-right: 0px;'>";
										            echo "<b>".$arrivals.' Arrivals'.' /'.$departures.' Departures'."</b>";
										            echo "</td>";
										            
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-left:0px;'>";
										            echo "</td>";
										            echo "</tr>";

										            

									                if(!empty($bookings))
									                {
									                	echo "<tr>";
												            echo "<td>";
												            echo "<b>Item Name</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Item Type</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Bed Combination</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>A/C</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Booking #</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>No of nights</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Arrival</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Departure</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Notes</b>";
												            echo "</td>";
											            echo "</tr>";
									                	foreach ($bookings as $booking) 
									                	{
									                		echo "<tr>";

									                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
										                    if(!empty($booking->item_name_id))
										                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
										                    else
										                         $unit_name = '- - - - -'.' ';

												           
											            	echo "<td>";
												            echo $unit_name;
												            echo "</td>";

												            echo "<td>";
												            echo $booking->item->itemType->name;
												            echo "</td>";

												            $bookingDateModel = $booking->FirstBookingDate();

										                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations))
										                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
										                    else
										                         $bed_pref = '- - - - -'.' ';

												            echo "<td >";
												            echo $bed_pref;
												            echo "</td>";

												            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

												            echo "<td style='text-align:right'>";
												            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo $booking->id;
												            echo "</td>";


									                		$date1=date_create($booking->departure_date);
										                    $date2=date_create($booking->arrival_date);
										                    $diff=date_diff($date1,$date2);
										                    $difference1 =  $diff->format("%a");

									                		
											            	echo "<td style='text-align:right'>";
												            echo $difference1;
												            echo "</td>";
											            	
											            	echo "<td style='text-align:right'>";
												            echo date('d.m.Y', strtotime($booking->arrival_date));
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo date('d.m.Y', strtotime($booking->departure_date));
												            echo "</td>";
											            	
											            	echo "<td style='width: 15%;'>";
												            // echo ;
												            echo "</td>";

												            echo "</tr>";
									                	}
									                }


									                $bookings = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->all();

									                $stay_through_count = BookingsItems::find()->where(['<=','arrival_date',$date])->andWhere(['>','departure_date',$date])->andWhere(['!=','status_id', 3 ])->andWhere(['deleted_at' => 0 ])->count();

									                echo "<tr>";
										            echo "<td style='border-right: 0px;'>";
										            echo "<b>".$stay_through_count.' Staying through'."</b>";
										            echo "</td>";
										            
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "</td>";
										            echo "<td style='border-right: 0px;border-left:0px;'>";
										            echo "</td>";
										            echo "<td style='border-left:0px;'>";
										            echo "</td>";
										            echo "</tr>";


									                if(!empty($bookings))
									                {
									                	echo "<tr>";
												            echo "<td>";
												            echo "<b>Item Name</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Item Type</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Bed Combination</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>A/C</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Booking #</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>No of nights</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Arrival</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Departure</b>";
												            echo "</td>";
												            echo "<td>";
												            echo "<b>Notes</b>";
												            echo "</td>";
											            echo "</tr>";
									                	foreach ($bookings as $booking) 
									                	{
									                		echo "<tr>";

									                		$booking_date = BookingDates::findOne(['booking_item_id' => $booking->id]);
										                    if(!empty($booking->item_name_id))
										                        $unit_name =  $booking->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
										                    else
										                         $unit_name = '- - - - -'.' ';

												           
											            	echo "<td>";
												            echo $unit_name;
												            echo "</td>";

												            echo "<td>";
												            echo $booking->item->itemType->name;
												            echo "</td>";

												            $bookingDateModel = $booking->FirstBookingDate();

										                    if(!empty($bookingDateModel) && isset($bookingDateModel->bedsCombinations))
										                        $bed_pref =  $bookingDateModel->bedsCombinations->combination;
										                    else
										                         $bed_pref = '- - - - -'.' ';

												            echo "<td >";
												            echo $bed_pref;
												            echo "</td>";

												            $booking_date = BookingDates::findOne(['booking_item_id' => $booking->id,'date' => $date]);

												            echo "<td style='text-align:right'>";
												            echo $bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children;
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo $booking->id;
												            echo "</td>";


									                		$date1=date_create($booking->departure_date);
										                    $date2=date_create($booking->arrival_date);
										                    $diff=date_diff($date1,$date2);
										                    $difference1 =  $diff->format("%a");

									                		
											            	echo "<td style='text-align:right'>";
												            echo $difference1;
												            echo "</td>";
											            	
											            	echo "<td style='text-align:right'>";
												            echo date('d.m.Y', strtotime($booking->arrival_date));
												            echo "</td>";

												            echo "<td style='text-align:right'>";
												            echo date('d.m.Y', strtotime($booking->departure_date));
												            echo "</td>";
											            	
											            	echo "<td style='width: 15%;'>";
												            // echo ;
												            echo "</td>";

												            echo "</tr>";
									                	}
									                }
									            }
									         
									            $date = strtotime("+1 day", strtotime($date));
									            $date = date("Y-m-d", $date);
									        }
				                    	?>
				                    </tbody>
	                    		</table>

						        
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>

<?php
$this->registerJs('
	$(document).ready(function(){

		// $(".daily_unit_report_table").DataTable();

		$("#excel-btn").click(function(){
        console.log("clicked");
      		$(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Daily Cleaning Breif Report",
            filename: "daily-cleaning-breif-report",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
    $("#csv-btn").click(function(){
        console.log("clicked");
        
          $(".table2excel").tableToCSV({
            name: "Daily Cleaning Breif Report",
            filename: "daily-cleaning-breif-report",
          });
    }); 
		
	});
');
$this->registerCss("
		//table { table-layout:fixed; word-break:break-all; word-wrap:break-word; }
		.table{
			width:100%;
			// table-layout: fixed;
		}
	");
?>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AttractionTags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attraction-tags-form">

    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">
                        
    					<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <div class="form-group" >
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id'=>'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            <a class="btn btn-default" href="<?= Url::to(['/attraction-tags']) ?>" >Cancel</a>
                        </div>

                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>

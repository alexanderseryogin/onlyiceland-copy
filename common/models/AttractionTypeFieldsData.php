<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attraction_type_fields_data".
 *
 * @property integer $id
 * @property integer $attraction_id
 * @property integer $atf_id
 * @property string $value
 *
 * @property AttractionTypeFields $atf
 * @property Attractions $attraction
 */
class AttractionTypeFieldsData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attraction_type_fields_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attraction_id', 'atf_id'], 'integer'],
            [['value'], 'string', 'max' => 256],
            [['atf_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttractionTypeFields::className(), 'targetAttribute' => ['atf_id' => 'id']],
            [['attraction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attractions::className(), 'targetAttribute' => ['attraction_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attraction_id' => Yii::t('app', 'Attraction ID'),
            'atf_id' => Yii::t('app', 'Atf ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtf()
    {
        return $this->hasOne(AttractionTypeFields::className(), ['id' => 'atf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttraction()
    {
        return $this->hasOne(Attractions::className(), ['id' => 'attraction_id']);
    }
}

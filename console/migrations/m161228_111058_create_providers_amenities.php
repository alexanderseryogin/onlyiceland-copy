<?php

use yii\db\Migration;

class m161228_111058_create_providers_amenities extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_amenities`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `amenities_id` INT(11) UNSIGNED, PRIMARY KEY (`id`), CONSTRAINT `FK_providers_amenities_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_providers_amenities_amenities_id` FOREIGN KEY (`amenities_id`) REFERENCES `amenities`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161228_111058_create_providers_amenities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

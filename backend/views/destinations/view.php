<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Destinations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Destinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="providers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'provider_category_id',
            'provider_type_id',
            'provider_admin_id',
            'phone',
            'fax',
            'booking_email:email',
            'website',
            'kennitala',
            'address:ntext',
            'country_id',
            'city_id',
            'state_id',
            'postal_code',
            'latitude',
            'longitude',
            'business_name',
            'business_address:ntext',
            'business_country_id',
            'business_city_id',
            'business_state_id',
            'business_postal_code',
            'vat_id'
        ],
    ]) ?>

</div>

<?php

namespace common\models;

use Yii;
use yii\web\JsExpression;
use common\models\PropertyPaymentMethods;
/**
 * This is the model class for table "properties".
 *
 * @property integer $id
 * @property integer $region_id
 * @property integer $owner_id
 * @property integer $property_type_id
 * @property string $name
 * @property string $address
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $country_id
 * @property string $postal_code
 * @property string $latitude
 * @property string $longitude
 * @property string $reservations_email
 * @property string $business_name
 * @property string $business_id_number
 * @property string $business_address
 * @property integer $business_city_id
 * @property integer $business_state_id
 * @property integer $business_country_id
 * @property string $business_postal_code
 * @property string $invoice_email
 * @property integer $youngest_age
 * @property string $payment_methods_for_gauarantee
 * @property string $payment_methods_checkin
 *
 * @property Cities $businessCity
 * @property Countries $businessCountry
 * @property States $businessState
 * @property Cities $city
 * @property Countries $country
 * @property PropertyTypes $propertyType
 * @property Regions $region
 * @property States $state
 * @property User $owner
 */
class Properties extends \yii\db\ActiveRecord
{
    public $same_address;

    public static $methodsGuarantee = ['American Express', 'Argencard', 'Diners Club', 'Discover', 'Euro/Mastercard', 'JCB', 'Switch', 'Visa'];
    public static $methodsCheckin = ['American Express', 'Argencard', 'Diners Club', 'Discover', 'Euro/Mastercard', 'JCB', 'Switch', 'Visa'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['same_address'], 'boolean'],
            //, 'city_id', 'state_id', 'country_id', 'business_city_id', 'business_state_id', 'business_country_id'
            [[ 'owner_id', 'property_type_id', 'youngest_age'], 'integer'],
            [['name', 'address', 'postal_code', 'latitude', 'longitude', 'reservations_email', 'business_id_number', 'invoice_email'], 'required'],
            //'business_address', 'business_postal_code',
            [['business_address', 'business_postal_code'], 'required', 'when' => function ($model) {
                return !$model->same_address;
            }, 'whenClient' => new JsExpression("
                function (attribute, value) {
                    return $('#properties-same_address').is(':unchecked');
                }
            ")],
            [['address', 'business_address', 'payment_methods_for_gauarantee', 'payment_methods_checkin'], 'string'],
            [['name'], 'string', 'max' => 1024],
            [['reservations_email', 'invoice_email'], 'email'],
            [['postal_code', 'latitude', 'longitude', 'business_id_number', 'business_postal_code'], 'string', 'max' => 256],
            [['reservations_email', 'business_name', 'invoice_email'], 'string', 'max' => 512],
            [['business_city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['business_city_id' => 'id']],
            [['business_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['business_country_id' => 'id']],
            [['business_state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['business_state_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['property_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyTypes::className(), 'targetAttribute' => ['property_type_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::className(), 'targetAttribute' => ['region_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'same_address' => Yii::t('app', 'Use same address for business?'),
            'region_id' => Yii::t('app', 'Region'),
            'owner_id' => Yii::t('app', 'Owner'),
            'property_type_id' => Yii::t('app', 'Property Type'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'city_id' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'reservations_email' => Yii::t('app', 'Reservations Email'),
            'business_name' => Yii::t('app', 'Business Name'),
            'business_id_number' => Yii::t('app', 'Kennitala'),
            'business_address' => Yii::t('app', 'Business Address'),
            'business_city_id' => Yii::t('app', 'Business City'),
            'business_state_id' => Yii::t('app', 'Business State'),
            'business_country_id' => Yii::t('app', 'Business Country'),
            'business_postal_code' => Yii::t('app', 'Business Postal Code'),
            'invoice_email' => Yii::t('app', 'Invoice Email'),
            'youngest_age' => Yii::t('app', 'Youngest Age'),
            'payment_methods_for_gauarantee' => Yii::t('app', 'Payment Methods For Guarantee'),
            'payment_methods_checkin' => Yii::t('app', 'Payment Methods Checkin'),
        ];
    }

    public function getPaymentMethodsGuarantee() {
        return $this->hasMany(PropertyPaymentMethods::className(), ['id' => 'property_pm_id'])
          ->viaTable('property_pm_guarantee', ['property_id' => 'id']);
    }

    public function getPaymentMethodsCheckin() {
        return $this->hasMany(PropertyPaymentMethods::className(), ['id' => 'property_pm_id'])
          ->viaTable('property_pm_checkin', ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'business_city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'business_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessState()
    {
        return $this->hasOne(States::className(), ['id' => 'business_state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyType()
    {
        return $this->hasOne(PropertyTypes::className(), ['id' => 'property_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}

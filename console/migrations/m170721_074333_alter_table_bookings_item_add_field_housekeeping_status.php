<?php

use yii\db\Migration;

class m170721_074333_alter_table_bookings_item_add_field_housekeeping_status extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `housekeeping_status_id` INT(11) NULL AFTER `status_id`;
                        ');
    }

    public function down()
    {
        echo "m170721_074333_alter_table_bookings_item_add_field_housekeeping_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

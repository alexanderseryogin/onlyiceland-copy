<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PlacesOfInterest */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Places Of Interests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="places-of-interest-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <p>
        <?= Html::img(\Yii::getAlias('@web') . "/../uploads/places-of-interest/".$model['title_picture'],
                ['width' => '200px', 'max-height'=>'150px', 'class' => 'img-responsive center-block']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            /*[
                'attribute'=>'title_picture',
                'value'=>\Yii::getAlias('@web') . "/../uploads/places-of-interest/".$model->title_picture,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],*/
            'name',
            'description:ntext',
        ],
    ]) ?>

</div>

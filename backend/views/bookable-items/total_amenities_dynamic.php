<?php
    use common\models\Amenities;
use kartik\sortable\Sortable;

$Checkbox = [
        0 => '',
        1 => 'checked'
    ];
?>
<div class="panel-body" style="overflow-y:auto;">
    <div style="height: 500px">
        <div class="row drop_header text-center">
            <div class="col-md-2"><strong>Amenities</strong></div>
            <div class="col-md-2"><strong>Free</strong></div>
            <div class="col-md-2"><strong>Available at extra cost</strong></div>
            <div class="col-md-2"><strong>Not available</strong></div>
            <div class="col-md-2"><strong>Do not display</strong></div>
            <div class="col-md-2"><strong>Can be banner</strong></div>
        </div>
        <?php
            $drop_items = array();
            foreach ($amenities as $key => $value)
            {
                $radioChecked = [
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                ];
                $isChecked = 0;

                if(!empty($selected_amenities) && isset($selected_amenities[$value->id]))
                {
                    $arr = explode(',', $selected_amenities[$value->id]);
                    if($arr[0] == $value->id)
                    {
                        switch($arr[1])
                        {
                            case 0:
                                $radioChecked[0] = 'checked';
                                break;

                            case 1:
                                $radioChecked[1] = 'checked';
                                break;

                            case 2:
                                $radioChecked[2] = 'checked';
                                break;

                            case 3:
                                $radioChecked[3] = 'checked';
                                break;
                        }
                    }

                    if(!empty($selected_banners))
                    {
                        if(in_array($value->id, $selected_banners))
                            $isChecked = 1;
                        else
                            $isChecked = 0;
                    }



                }
                $drop_items[] = ['content' => (string)$this->render('_dropitem', compact('value', 'radioChecked', 'Checkbox', 'isChecked'))];
            }
            echo Sortable::widget([
                'itemOptions'=>['class'=>'drop_line'],
                'items' => $drop_items
            ]);
        ?>
    </div>
</div>
<?php
    use common\models\Rates;
    use common\models\RatesDates;
    use common\models\RatesCapacityPricing;
    use common\models\Destinations;
    use yii\helpers\ArrayHelper;
    use common\models\BookingTypes;
    use common\models\BookingStatuses;
    use common\models\BookingConfirmationTypes;
    use common\models\User;
    use common\models\BookingsItems;
    use common\models\BookableBedTypes;
    use common\models\BedTypes;
    use common\models\BookableBedsCombinations;
    use common\models\BookingDates;
    use common\models\Countries;
    use common\models\BookableItemsNames;
    use common\models\BookingsDiscountCodes;
    use common\models\DestinationTravelPartners;
    use common\models\Offers;
    use common\models\EstimatedArrivalTimes;
    use common\models\BookingSpecialRequests;
    use common\models\BookingsUpsellItems;
    use yii\helpers\Url;
    use common\models\HousekeepingStatus;
    use common\models\RatesOverride;

    $item = $model->item;
    $arrival_date = $model->arrival_date;
    $departure_date = $model->departure_date;

    $arrival_date = str_replace('/', '-', $arrival_date);
    $arrival_date = date('Y-m-d',strtotime($arrival_date));

    $departure_date = str_replace('/', '-', $departure_date);
    $departure_date = date('Y-m-d',strtotime($departure_date));

    $arrival_day = date('N',strtotime($arrival_date));
    $departure_day = date('N',strtotime($departure_date));

    $date1=date_create($departure_date);
    $date2=date_create($arrival_date);
    $diff=date_diff($date1,$date2);
    $difference =  $diff->format("%a");

    $destinations_list = Destinations::find()->where(['active' => 1])->all();

    $Options = [
        0 => '',
        1 => 'checked',
        2 => 'selected',
        3 => 'disabled',
    ];

    $availableItemNames = BookingsItems::checkAvailableItemNamesIncludedSaved($item->id,$arrival_date,$difference,$model->id);
    $bookedDates = $model->bookingDatesSortedAndNotNull;
?>

<div id="item-pannel">
    <div class="panel-group accordion" id="accordion_item_rates">
        <div class="row" id="unit_panel_<?php echo $item->id?>">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading" style="background: <?php echo $item->background_color?>; color: <?php echo $item->text_color?>">
                        <h4 class="panel-title">
                            <div class="accordion-toggle" data="<?php echo $item->id?>" data-toggle="collapse" data-parent="#accordion_item_rates" href="#collapse_<?php echo $item->id?>">
                                <label style="font-size:28px;margin-bottom:0px;font-weight:bold"><?php echo $item->itemType->name; ?></label>
                                <span class="pull-right" style="font-size:28px;text-align:right"><b> ,GB: <span ><?php echo Yii::$app->formatter->asDecimal($model->bookingItemGroups->bookingGroup->balance,0);?></span></b></span>
                                <span class="pull-right" style="font-size:28px;text-align:right"><b>B: <span  id="balance_due"><?php echo Yii::$app->formatter->asDecimal($model->balance,0);?></span></b></span>
                            </div>
                        </h4>
                    </div>
                    <div id="collapse_<?php echo $item->id?>" class="panel-collapse collapse in" aria-expanded="true">
                        <div class="panel-body" style="overflow-y:auto;">

                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label"><strong>Destination</strong></label>
                                    <select  style="width: 100%;" class="form-control" disabled>
                                        <?php
                                            if(!empty($destinations_list))
                                            {

                                                foreach ($destinations_list as $value)
                                                {
                                                    $selected = ($value->id == $model->provider_id)?2:0;

                                                    echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->name.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label"><strong>Item Type(s)</strong></label>
                                    <select id="item-type" style="width: 100%;" class="form-control" >
                                        <?php
                                            if(!empty($destination_items))
                                            {

                                                foreach ($destination_items as $key => $value)
                                                {
                                                    $selected = ($key == $model->item_id)?2:0;

                                                    echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Item Name(s)</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control item-name item-name-<?php echo $item->id ?>" >
                                            <option value=""> - - - - - -</option>
                                            <?php
                                                $item_names = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item->id])->all(),'id','item_name');
                                                $previous =null;
                                                foreach ($item_names as $key => $value)
                                                {
                                                    $selected = ($key == $viewDetailsRecord['item_name_id'])?2:0;
                                                    if(in_array($key, $availableItemNames))
                                                    {
                                                        if(($key == $viewDetailsRecord['item_name_id']))
                                                        {
                                                            $previous = $key;
                                                        }



                                                        echo '<option data-data="1" value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                    else
                                                    {
                                                        echo '<option data-data="0" value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <input type="hidden" class="previous" value="<?=$previous?>">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <button style="margin-top: 30px;" data="<?php echo $item->id?>" type="button" class="btn btn-xs btn-default view-details">
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i> <span>View Details</span>
                                    </button>
                                </div>
                                <div class="col-sm-1">
                                    <?php
                                        if(!empty($model->comments) && $model->comments!=NULL)
                                        {
                                            $html ='<i class="fa fa-commenting-o fa-3x" style="color:red;margin-top:25px;margin-left:15px;"> </i>';
                                            echo $html;
                                        }
                                    ?>
                                </div>
                                <div class="col-md-2" style="margin-top:20px;">
                                    <!-- <span style="font-size:20px;text-align:right"><b>Balance:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span  id="balance_due"><?php echo Yii::$app->formatter->asDecimal($model->balance,0);?></span></b></span> -->
                                </div>
                            </div>

                            <div class="panel panel-default rate-details-fields-<?php echo $item->id?>" style="display: none; background: #F1F1F1">
                                <div class="panel-body">
                                    <?php
                                        if($item->destination->getDestinationTypeName()=='Accommodation')
                                        {
                                            $itemBedsCombinationsModel = BookableBedsCombinations::find()->where(['item_id' => $item->id])->all();
                                            if(!empty($itemBedsCombinationsModel))
                                            {
                                                echo '<label class="control-label rate-details-fields-'.$item->id.'" style="display: none;"><strong>Bed Preference:</strong></label>';
                                                echo '<div class="rate-details-fields-'.$item->id.'" style="display: block;">';
                                                    foreach ($itemBedsCombinationsModel as $key => $value)
                                                    {
                                                        $checked = ($value->beds_combinations_id == $viewDetailsRecord['beds_combinations_id'])?1:0;
                                                        $path = Yii::getAlias('@web').'/../uploads/beds-combinations/'.$value->beds_combinations_id.'/icons/'.$value->bedsCombinations->icon;
                                                        ?>
                                                        <label style="margin-left:10px;" class="mt-radio mt-radio-outline">
                                                            <input name="beds-combinations-<?php echo $item->id?>" data="<?php echo $value->beds_combinations_id?>" class="beds-combinations beds-combinations-<?php echo $item->id?>" value="<?php echo $value->beds_combinations_id?>" <?php echo $Options[$checked]?> type="radio"> <?php echo $value->bedsCombinations->combination?> <img src="<?php echo $path ?>" style="max-height:30px">
                                                            <span></span>
                                                        </label>
                                                        <?php
                                                    }
                                                echo '</div>';
                                            }
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label"><strong>User</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control user-dropdown user-<?php echo $item->id?>" >
                                                    <option value="">Select a User</option>
                                                    <?php
                                                        $users = ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_USER])->all(),
                                                        'id',
                                                        function($model, $defaultValue)
                                                        {
                                                            if(empty($model->profile->country_id))
                                                                $country = '';
                                                            else
                                                                $country = ' - '.$model->profile->country->name;
                                                            return $model->username.' - '.$model->email.$country;
                                                        });
                                                        if(!empty($users))
                                                        {
                                                            $selected = 0;
                                                            foreach ($users as $key => $value)
                                                            {
                                                                $selected = ($key==$viewDetailsRecord['user_id']) ? 2 : 0 ;
                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>First Name</strong></label>
                                            <div class="form-group" >
                                                <input type="text" class="form-control guest-first-name guest-first-name-<?php echo $item->id?>" value="<?=$viewDetailsRecord['guest_first_name']?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Last Name</strong></label>
                                            <div class="form-group" >
                                                <input type="text" class="form-control guest-last-name guest-last-name-<?php echo $item->id?>" value="<?=$viewDetailsRecord['guest_last_name']?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label"><strong>Country</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class=" form-control guest-country guest-country-<?php echo $item->id?>" >
                                                    <?php
                                                        $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                                        foreach ($countries as $key => $value)
                                                        {
                                                            $selected = ($key == $viewDetailsRecord['guest_country_id']) ? 2 : 0 ;
                                                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Booking Confirmation</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control rate-details-dropdowns booking-confirmation-<?php echo $item->id?>">
                                                    <?php
                                                        $bookingConfirmation =ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name');

                                                        if(!empty($bookingConfirmation))
                                                        {
                                                            foreach ($bookingConfirmation as $key => $value)
                                                            {
                                                                $selected = ($key==$viewDetailsRecord['confirmation_id']) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Cancellation Period for Bookings</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control rate-details-dropdowns booking-cancellation-<?php echo $item->id?>">
                                                    <?php
                                                        foreach (Destinations::getBookingCancellationArray() as $key => $value)
                                                        {
                                                            $selected = ($key==$viewDetailsRecord['booking_cancellation']) ? 2 : 0 ;

                                                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label"><strong>Flag</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control rate-details-dropdowns booking-flag-<?php echo $item->id?>">
                                                    <?php
                                                        $bookingTypes = ArrayHelper::map(BookingTypes::find()->all(),'id','label');

                                                        if(!empty($bookingTypes))
                                                        {
                                                            foreach ($bookingTypes as $key => $value)
                                                            {
                                                                $selected = ($key==$viewDetailsRecord['flag_id']) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label"><strong>Status</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control rate-details-dropdowns booking-status-<?php echo $item->id?>">
                                                    <?php
                                                        $bookingStatuses = ArrayHelper::map(BookingStatuses::find()->all(),'id','label');
                                                        if(!empty($bookingStatuses))
                                                        {
                                                            foreach ($bookingStatuses as $key => $value)
                                                            {
                                                                $selected = ($key==$viewDetailsRecord['status_id']) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 <?=($item->destination->use_housekeeping == 1)?'':'hide' ?>">
                                            <label class="control-label"><strong>Housekeeping Status</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control housekeeping-status housekeeping-status-<?php echo $item->id?>">
                                                    <?php
                                                        $housekeepingStatuses = ArrayHelper::map(HousekeepingStatus::find()->all(),'id','label');
                                                        if(!empty($housekeepingStatuses))
                                                        {
                                                            foreach ($housekeepingStatuses as $key => $value)
                                                            {
                                                                echo '<option value="'.$key.'" >'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        if(empty($model->travel_partner_id) && empty($model->offers_id))
                                        {
                                            $this->registerJs
                                            ("
                                                $('#offers_dropdown').attr('disabled',false);
                                                $('#travel_partner_dropdown').attr('disabled',false);
                                                $('.vou_ref_div').addClass('hide');
                                            ");
                                        }
                                        else if(empty($model->travel_partner_id) && !empty($model->offers_id))
                                        {
                                            $this->registerJs
                                            ("
                                                $('#offers_dropdown').attr('disabled',false);
                                                $('#travel_partner_dropdown').attr('disabled','disabled');
                                                $('.vou_ref_div').addClass('hide');
                                            ");
                                        }
                                        else
                                        {
                                            $this->registerJs
                                            ("
                                                $('#offers_dropdown').attr('disabled','disabled');
                                                $('#travel_partner_dropdown').attr('disabled',false);
                                                $('.vou_ref_div').removeClass('hide');
                                            ");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label class="control-label"><strong>Travel Partner</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" id="travel_partner_dropdown" class="form-control?>">
                                                    <option value=""> Select Referrer </option>
                                                    <?php
                                                        $travelPartners = DestinationTravelPartners::find()->where(['destination_id' => $model->provider_id])->all();

                                                        if(!empty($travelPartners))
                                                        {
                                                            foreach ($travelPartners as $key => $value)
                                                            {
                                                                $selected = ($value->id==$viewDetailsRecord['travel_partner_id']) ? 2 : 0 ;

                                                                echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->travelPartner->company_name.' - '.$value->comission.'%'.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label"><strong>Offers</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" id="offers_dropdown" class="form-control">
                                                    <option value=""> Select Offer </option>
                                                    <?php
                                                        $offers = Offers::find()->where(['provider_id'=>$model->provider_id])->all();

                                                        if(!empty($offers))
                                                        {
                                                            foreach ($offers as $key => $value)
                                                            {
                                                                $selected = ($value->id==$viewDetailsRecord['offers_id']) ? 2 : 0 ;

                                                                echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->name.' - '.$value->commission.'%'.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="control-label" ><strong>Estimated Arrival Time</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control rate-details-dropdowns estimated-arrival-time-dropdown">
                                                    <?php
                                                        if(!empty($model->provider_id))
                                                        {
                                                            $estimated_arrival_times = EstimatedArrivalTimes::find()->where(['time_group' => $model->provider->bookingPolicies->time_group])->all();

                                                            if(!empty($estimated_arrival_times))
                                                            {
                                                                foreach ($estimated_arrival_times as $key =>  $obj)
                                                                {
                                                                    $start_time = '';

                                                                    if(empty($obj->text))
                                                                    {
                                                                        $start_time = date('H:i',strtotime($obj->start_time));
                                                                    }
                                                                    else
                                                                    {
                                                                        $start_time = $obj->text;
                                                                    }
                                                                    $obj->end_time = date('H:i',strtotime($obj->end_time));

                                                                    $selected = ($obj->id==$viewDetailsRecord['estimated_arrival_time_id']) ? 2 : 0 ;

                                                                    echo "<option value='".$obj->id."' ".$Options[$selected].">".$start_time." - ".$obj->end_time."</option>";
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="vou_ref_div ">
                                            <div class="col-sm-3">
                                                <label class="control-label "><strong>Voucher #</strong></label>
                                                <div class="form-group" >
                                                    <input class="form-control voucher_no-<?php echo $item->id?>" value="<?=$viewDetailsRecord['voucher_no']?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label"><strong>Reference #</strong></label>
                                                <div class="form-group" >
                                                    <input class="form-control reference_no-<?php echo $item->id?>" value="<?=$viewDetailsRecord['reference_no']?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label"><strong>Comment</strong></label>
                                            <div class="form-group" >
                                            <textarea class="form-control comment-<?php echo $item->id?>" rows="3"><?=$viewDetailsRecord['comment']?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="control-label"><strong>Notes</strong></label>
                                            <div class="form-group" >
                                            <textarea class="form-control notes-<?php echo $item->id?>" rows="3"><?=$viewDetailsRecord['notes']?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="rate-details-fields-<?php echo $item->id?>" style="display: none;"><br></div>

                            <input type="hidden" id="booking_item_id" class="form-control" value="<?php echo $model->id ?>">
                            <div class="available-rates">
                        <table class="table table-bordered table-<?php echo $item->id?>">
                            <thead>
                                <tr>
                                    <strong>
                                        <th width="13%">Date</th>
                                        <th width="87%">Rates</th>
                                    </strong>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($bookedDates as $key => $bookedDate)
                            {
                                $rate = $bookedDate->rate;
                                $dateKey = $bookedDate->date;
                            ?>
                                <tr>
                                    <td>
                                        <?php
                                            echo date('d/m/Y',strtotime($dateKey)).'<br>';
                                        ?>
                                        <br>

                                        <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $item->id?>" class="form-control item-quantity item-quantity-<?php echo $item->id?> item-quantities-<?php echo $item->id.'-'.$dateKey?>" value="1">

                                        <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $item->id?>" class="form-control item-date-<?php echo $item->id.'-'.$dateKey?>" value="<?php echo $dateKey ?>">

                                    </td>
                                    <td>
                                        <div class="table-scrollable">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <strong>
                                                            <th width="20%">Rate</th>

                                                            <?php
                                                                if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                                                {
                                                                    echo '<th width="15%">Capacity Pricing </th>';
                                                                }
                                                                else if($rate->destination->pricing == 0)
                                                                {
                                                                    echo '<th width="10%">Adults</th><th width="10%">Children</th>';
                                                                }
                                                            ?>
                                                            <?php if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                                                { ?>
                                                                <th width="10%">Adults/Children</th>
                                                                <?php } ?>
                                                            <th width="22%">Available Discounts</th>
                                                            <th width="28%">Upsell Items</th>
                                                            <th width="10%">Total </th>
                                                        </strong>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                <tr>
                                                    <td  style="min-width:120px;">
                                                        <input type="text" value="<?php echo  $rate->name?>" hidden>
                                                        <?php echo  $rate->name.'<br><br>'?>

                                                        <input type="text" value="<?php echo $item->id.':'.$rate->id.':'.$dateKey?>" class="helper-field helper-field-<?php echo $item->id?> helper-field-<?php echo $item->id.'-'.$dateKey?>" hidden>
                                                    </td>

                                                    <?php


                                                    // ************* capacity pricing ************ //

                                                    if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                                    {
                                                    ?>
                                                        <td date="<?php echo $dateKey; ?>" class="capacity-pricing-box capacity-pricing-box-<?php echo $item->id.'-'.$dateKey?>">
                                                    <?php

                                                        $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $rate->id])->all();
                                                        $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $rate->id]);
                                                        if(!empty($rates_capacity_pricing))
                                                        {
                                                            foreach ($rates_capacity_pricing as $key => $value)
                                                            {
                                                                if($value->person_no >=1 && $value->person_no<=4)
                                                                {
                                                                    $checked = ($value->person_no == $bookedDate->person_no)? 1 : 0;

                                                                    $price = $value->price;
                                                                    $custom_rate = '';

                                                                    if($checked == 1 )
                                                                    {
                                                                        if($bookedDate->custom_rate != null)
                                                                        {
                                                                            $custom_rate = Yii::$app->formatter->asDecimal( $bookedDate->custom_rate, "ISK");
                                                                        }
                                                                        else
                                                                        {
                                                                            $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(!empty($rate_override))
                                                                        {
                                                                            if($key==0)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->single, "ISK");
                                                                            }
                                                                            if($key==1)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->double, "ISK");
                                                                            }
                                                                            if($key==2)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->triple, "ISK");
                                                                            }
                                                                            if($key==3)
                                                                            {
                                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->quad, "ISK");
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                                        }

                                                                    }

                                                                    // $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");

                                                                    $icon_user = '';

                                                                    for ($i=1; $i <= $value->person_no ; $i++)
                                                                    {
                                                                        $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                                    }
                                                                    ?>
                                                                    <label style="width: 100%">
                                                                        <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $value->person_no?>" id="<?php echo $item->id.':'.$rate->id.':'.$value->person_no?>" class="capacity-pricing capacity-pricing-<?php echo $item->id?> capacity-pricing-<?php echo $item->id.'-'.$dateKey?> cpricing-<?php echo $rate->id?> cpricing-<?php echo $rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $item->id.'-'.$dateKey?>" value="<?php echo $value->person_no.' : '.$price?>" <?php echo $Options[$checked]?>> <?php echo $icon_user?>
                                                                        <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$item->id.':'.$rate->id.':'.$value->person_no?>" value="<?=($checked == 1 && $bookedDate->custom_rate != null )?$custom_rate:$value->price?>" date="<?php echo $dateKey?>" style="text-align:right;width: 50%;float: right;<?=($checked == 1 && $bookedDate->custom_rate != null && $bookedDate->rate_added_through == 0 )?'color:red;':''?> <?=($bookedDate->custom_rate != null && $bookedDate->rate_added_through == 1 )?'color:orange;':''?>" data="<?php echo $value->person_no?>" >
                                                                    </label><br>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        echo '</td>';?>
                                                        <td style="min-width:20px;">
                                                            <label>AD</label>
                                                            <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    $adult_count = isset($bookedDate->item->max_adults)?$bookedDate->item->max_adults:10;
                                                                    for ($i=0; $i <= $adult_count ; $i++)
                                                                    {
                                                                        $selected = ($i == $bookedDate->no_of_adults)? 2 : 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                            <br>
                                                            <label>CH</label>
                                                            <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    $children_count = isset($bookedDate->item->max_children)?$bookedDate->item->max_children:10;
                                                                    for ($i=0; $i <= $children_count ; $i++)
                                                                    {
                                                                        $selected = ($i == $bookedDate->no_of_children)? 2 : 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>
                                                    <?php }
                                                    else if($rate->destination->pricing == 0)  // ************* First/Additional ************ //
                                                    {
                                                    ?>
                                                        <td style="min-width:50px;">
                                                            <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    for ($i=0; $i <= 20 ; $i++)
                                                                    {
                                                                        $selected = ($i == $bookedDate->no_of_adults)? 2 : 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>

                                                        <td style="min-width:50px;">
                                                            <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                                <?php
                                                                    for ($i=0; $i <= 20 ; $i++)
                                                                    {
                                                                        $selected = ($i == $bookedDate->no_of_children)? 2 : 0;
                                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </td>
                                                    <?php
                                                    }

                                                    $max_extra_beds = $item->max_extra_beds;
                                                    $bed_type_people = 0;

                                                    $selected_upsell_extra_bed_quantity = [];
                                                    $selected_discount_code_extra_bed_quantity = [];

                                                    if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                                    {
                                                        $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $item->id])->all();

                                                        if(!empty($itemBedTypesModel))
                                                        {
                                                            foreach ($itemBedTypesModel as $key => $value)
                                                            {
                                                                $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                                            }
                                                        }
                                                    }

                                                    // *********** Discount Codes ********** //

                                                    $checked_discount_codes = [];
                                                    $checked_upsell_items = [];

                                                    $get_discount_codes = BookingsDiscountCodes::find()->where(['booking_date_id' => $bookedDate->id])->all();

                                                    if(!empty($get_discount_codes))
                                                    {
                                                        foreach ($get_discount_codes as $key => $value)
                                                        {
                                                            $checked_discount_codes[$value['discount_id']] = $value['price'];
                                                            $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                                        }
                                                    }

                                                    $get_upsell_items = BookingsUpsellItems::find()->where(['booking_date_id' => $bookedDate->id])->all();

                                                    if(!empty($get_upsell_items))
                                                    {
                                                        foreach ($get_upsell_items as $key => $value)
                                                        {
                                                            $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                                            $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                                        }
                                                    }

                                                    echo $this->render('discount_codes',
                                                    [
                                                        'dateKey' => $dateKey,
                                                        'rates_id' => $rate->id,
                                                        'checked_discount_codes' => $checked_discount_codes,
                                                        'bed_type_people' => $bed_type_people,
                                                        'max_extra_beds' => $max_extra_beds,
                                                        'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                                        // 'model_id' => $model->id,
                                                        // 'bookedDate' =>$bookedDate,
                                                    ]);

                                                    // *********** Upsell Items ********** //

                                                    echo $this->render('upsell_items',
                                                    [
                                                        'dateKey' => $dateKey,
                                                        'rates_id' => $rate->id,
                                                        'checked_upsell_items' => $checked_upsell_items,
                                                        'max_extra_beds' => $max_extra_beds,
                                                        'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                                        // 'model_id' => $model->id,
                                                        // 'bookedDate' =>$bookedDate,
                                                    ]);

                                                    $price_json = '';
                                                    $orgTotal = 0;
                                                    $icelandTotal = 0;
                                                    $tooltips_title = 'Item Price';

                                                    if(!empty($bookedDate->total))
                                                    {
                                                        $orgTotal = $bookedDate->total;
                                                        $icelandTotal = Yii::$app->formatter->asDecimal($orgTotal,0);
                                                    }

                                                    if(!empty($bookedDate->price_json))
                                                    {
                                                        $arr = json_decode($bookedDate->price_json);

                                                        $tooltips_title = $tooltips_title . 'Quantity = '. $arr->quantity .' , ';
                                                        $tooltips_title = $tooltips_title . 'Total Nights = '. $arr->total_nights .' , ';
                                                        $tooltips_title = $tooltips_title . 'Rate = '. $arr->rate .' , ';
                                                        $tooltips_title = $tooltips_title . 'VAT % = '. $arr->vat .' , ';
                                                        $tooltips_title = $tooltips_title . 'x = '. $arr->x .' , ';
                                                        $tooltips_title = $tooltips_title . 'lodgingTax = '. $arr->lodgingTax .' , ';
                                                        $tooltips_title = $tooltips_title . 'y = '. $arr->y .' , ';
                                                        $tooltips_title = $tooltips_title . 'voucher_discount = '. $arr->voucher_discount .' , ';
                                                        $tooltips_title = $tooltips_title . 'travel_partner_discount = '. $arr->travel_partner_discount .' , ';
                                                        $tooltips_title = $tooltips_title . 'offer_discount = '. $arr->offer_discount .' , ';
                                                        $tooltips_title = $tooltips_title . 'amount_of_discount = '. $arr->amount_of_discount .' , ';
                                                        $tooltips_title = $tooltips_title . 'sub_total_1 = '. $arr->sub_total_1 .' , ';
                                                        $tooltips_title = $tooltips_title . 'sub_total_2 = '. $arr->sub_total_2 .' , ';
                                                        $tooltips_title = $tooltips_title . 'vat_amount = '. $arr->vat_amount .' , ';
                                                        $tooltips_title = $tooltips_title . 'Total Price = '. $arr->icelandPrice;

                                                        $price_json = str_replace('"', "'", $bookedDate->price_json);
                                                    }
                                                    else
                                                    {
                                                        $tooltips_title = 'Item Price';
                                                        $price_json = '';
                                                    }

                                                    ?>

                                                    <td>
                                                        <input class="item-total-input item-total-input-<?php echo $rate->id.'-'.$dateKey?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                                        <input type="text" value="<?php echo $price_json; ?>" class="price-json-<?php echo $rate->id.'-'.$dateKey?>" hidden>

                                                        <label>
                                                            <a href="javascript:;" data-placement="left" class="item-total-<?php echo $rate->id.'-'.$dateKey?> total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                                            </a>
                                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $icelandTotal?></span>
                                                        </label>
                                                        <label class="item-total-nights-price-<?php echo $rate->id.'-'.$dateKey?>"></label>
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>




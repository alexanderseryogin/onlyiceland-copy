<?php

use yii\db\Migration;

class m170216_090546_alter_booking_policies_add_time_group extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_policies` ADD COLUMN `time_group` INT(11) NULL AFTER `checkout_time_to`; 
        ");
    }

    public function down()
    {
        echo "m170216_090546_alter_booking_policies_add_time_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

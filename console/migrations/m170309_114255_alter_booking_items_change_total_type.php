<?php

use yii\db\Migration;

class m170309_114255_alter_booking_items_change_total_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` CHANGE `total` `total` DECIMAL(8,2) NULL; ");
    }

    public function down()
    {
        echo "m170309_114255_alter_booking_items_change_total_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

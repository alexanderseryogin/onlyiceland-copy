<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BookableItems;
use common\models\BookableItemsImages;

/**
 * BookableItemsSearch represents the model behind the search form about `common\models\BookableItems`.
 */
class BookableItemsSearch extends BookableItems
{
    public $provider_name;
    public $item_name;
    public $image;
    public $display_order;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_id', 'item_type_id', 'item_quantity', 'shortest_period', 'longest_period', 'max_adults', 'max_children', 'max_extra_beds', 'max_baby_beds', 'min_age_participation', 'max_age_participation', 'max_free_age'], 'integer'],
            [['item_name','provider_name','how_booked', 'no_of_guests', 'background_color', 'text_color', 'include_in_groups'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BookableItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query->joinWith(['destination','itemType']),//getDestination(),getItemType()
            'pagination' => [
                'page' => isset($params['page'])?($params['page']-1):0,
            ],
        ]);

        $dataProvider->sort->attributes['provider_name'] = [
        'asc' => ['providers.name' => SORT_ASC], //tableName.attributeName
        'desc' => ['providers.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['item_name'] = [
        'asc' => ['bookable_item_types.name' => SORT_ASC], //tableName.attributeName
        'desc' => ['bookable_item_types.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'item_type_id' => $this->item_type_id,
            'item_quantity' => $this->item_quantity,
            'shortest_period' => $this->shortest_period,
            'longest_period' => $this->longest_period,
            'max_adults' => $this->max_adults,
            'max_children' => $this->max_children,
            'max_extra_beds' => $this->max_extra_beds,
            'max_baby_beds' => $this->max_baby_beds,
            'min_age_participation' => $this->min_age_participation,
            'max_age_participation' => $this->max_age_participation,
            'max_free_age' => $this->max_free_age,
        ]);

        $query->andFilterWhere(['like', 'how_booked', $this->how_booked])
            ->andFilterWhere(['like', 'no_of_guests', $this->no_of_guests])
            ->andFilterWhere(['like', 'background_color', $this->background_color])
            ->andFilterWhere(['like', 'text_color', $this->text_color])
            ->andFilterWhere(['like', 'include_in_groups', $this->include_in_groups])
            ->andFilterWhere(['like', 'providers.name', $this->provider_name])
            ->andFilterWhere(['like', 'bookable_item_types.name', $this->item_name]);

        return $dataProvider;
    }


    public function searchImages($params)
    {
        $query = BookableItemsImages::find()->where(['bookable_items_id' => $params])->orderBy('display_order ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //$query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }

    public function searchBedTypes($params)
    {
        $query = BookableBedTypes::find()->where(['bookable_id' => $params]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_report_conditions".
 *
 * @property integer $id
 * @property integer $custom_report_id
 * @property integer $condition
 * @property string $value
 *
 * @property CustomReports $customReport
 */
class CustomReportConditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_report_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['custom_report_id', 'condition','column'], 'integer'],
            [['value','operator'], 'string', 'max' => 256],
            [['custom_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomReports::className(), 'targetAttribute' => ['custom_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'custom_report_id' => 'Custom Report ID',
            'condition' => 'Condition',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReport()
    {
        return $this->hasOne(CustomReports::className(), ['id' => 'custom_report_id']);
    }

    // public static function getConditions()
    // {
    //     $conditions = array(
    //         '1' => 
    //     );

    //     return $conditions;
    // }
}

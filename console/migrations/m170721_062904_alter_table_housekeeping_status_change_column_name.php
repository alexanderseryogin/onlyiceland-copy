<?php

use yii\db\Migration;

class m170721_062904_alter_table_housekeeping_status_change_column_name extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `housekeeping_status`   
                          CHANGE `text-color` `text_color` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL;
                        ');
    }

    public function down()
    {
        echo "m170721_062904_alter_table_housekeeping_status_change_column_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

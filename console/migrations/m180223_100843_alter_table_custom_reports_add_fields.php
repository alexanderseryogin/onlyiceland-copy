<?php

use yii\db\Migration;

class m180223_100843_alter_table_custom_reports_add_fields extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_reports`   
                          ADD COLUMN `statuses_operator` INT(11) NULL AFTER `show_max`,
                          ADD COLUMN `flags_operator` INT(11) NULL AFTER `statuses_operator`,
                          ADD COLUMN `referers_operator` INT(11) NULL AFTER `flags_operator`;
                        ');
    }   

    public function safeDown()
    {
        echo "m180223_100843_alter_table_custom_reports_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180223_100843_alter_table_custom_reports_add_fields cannot be reverted.\n";

        return false;
    }
    */
}

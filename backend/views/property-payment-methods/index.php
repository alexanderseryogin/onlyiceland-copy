<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\components\Helpers;
use common\models\PropertyPaymentMethods;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PropertyPaymentMethodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payment Methods');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-payment-methods-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW PAYMENT METHOD'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'property-payment-methods-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'type',
            /*[
                'attribute' => 'type',
                'value' => function($data) {
                    return Helpers::propertyPaymentMethodLables($data->type); 
                },
                'format' => 'raw',
                'filter' => Html::dropDownList(
                    'PropertyPaymentMethodsSearch[type]', 
                    $searchModel['type'], 
                    [
                        ""=>"",
                        PropertyPaymentMethods::TYPE_GUARANTEE => Helpers::propertyPaymentMethodLables(PropertyPaymentMethods::TYPE_GUARANTEE, true), 
                        PropertyPaymentMethods::TYPE_CHECKIN => Helpers::propertyPaymentMethodLables(PropertyPaymentMethods::TYPE_CHECKIN, true),
                        PropertyPaymentMethods::TYPE_GUARANTEE_CHECKIN => Helpers::propertyPaymentMethodLables(PropertyPaymentMethods::TYPE_GUARANTEE_CHECKIN, true),
                    ],
                    ['class'=>'form-control']),
                'contentOptions' =>['style' => 'width:8%'],
            ],*/

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

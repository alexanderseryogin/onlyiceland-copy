<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attractions_images".
 *
 * @property integer $id
 * @property integer $attraction_id
 * @property string $image
 * @property string $description
 * @property integer $display_order
 *
 * @property Attractions $attraction
 */
class AttractionsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attractions_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attraction_id', 'display_order'], 'integer'],
            [['image', 'description'], 'string', 'max' => 256],
            [['attraction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attractions::className(), 'targetAttribute' => ['attraction_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attraction_id' => Yii::t('app', 'Attraction ID'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'display_order' => Yii::t('app', 'Display Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttraction()
    {
        return $this->hasOne(Attractions::className(), ['id' => 'attraction_id']);
    }

    public function isFirstRecord()
    {
          $first = AttractionsImages::find()->where(['attraction_id' => $this->attraction_id])->orderBy('display_order ASC')->one();
          return $this->id == $first->id ? true : false;
    }

    public function isLastRecord()
    {
          $last = AttractionsImages::find()->where(['attraction_id' => $this->attraction_id])->orderBy('display_order DESC')->one();
          return $this->id == $last->id ? true : false;
    }
}

<?php

use yii\db\Migration;

class m170524_114052_alter_bookable_items extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookable_items` DROP COLUMN `item_names`; 
        ");
    }

    public function down()
    {
        echo "m170524_114052_alter_bookable_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

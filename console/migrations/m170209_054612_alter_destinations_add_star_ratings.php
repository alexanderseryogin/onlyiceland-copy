<?php

use yii\db\Migration;

class m170209_054612_alter_destinations_add_star_ratings extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` ADD COLUMN `star_rating` INT(11) NULL AFTER `offers_accommodation`; 
        ");
    }

    public function down()
    {
        echo "m170209_054612_alter_destinations_add_star_ratings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_housekeeping_status_cart".
 *
 * @property integer $id
 * @property string $booking_item_id
 * @property integer $housekepping_status_id
 *
 * @property BookingsItemsCart $bookingItem
 * @property HousekeepingStatus $housekeppingStatus
 */
class BookingHousekeepingStatusCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_housekeeping_status_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_item_id', 'housekeeping_status_id'], 'integer'],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItemsCart::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
            [['housekeeping_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HousekeepingStatus::className(), 'targetAttribute' => ['housekeeping_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_item_id' => 'Booking Item ID',
            'housekeeping_status_id' => 'Housekepping Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItemsCart::className(), ['id' => 'booking_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHousekeppingStatus()
    {
        return $this->hasOne(HousekeepingStatus::className(), ['id' => 'housekeeping_status_id']);
    }
}

<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Properties;

/**
 * PropertiesSearch represents the model behind the search form about `common\models\Properties`.
 */
class PropertiesSearch extends Properties
{
    public $user;
    public $city;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'property_type_id', 'business_city_id', 'youngest_age'], 'integer'],
            [[
                'name', 
                'address', 
                'postal_code', 
                'latitude', 
                'longitude', 
                'reservations_email', 
                'business_name', 
                'business_id_number', 
                'business_address', 
                'business_postal_code', 
                'invoice_email', 
                'payment_methods_for_gauarantee', 
                'payment_methods_checkin',
                'user',
                'city'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Properties::find();
        $query->joinWith(['city', 'owner']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['city'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['cities.name' => SORT_ASC],
            'desc' => ['cities.name' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['owner'] = [
            'asc' => ['user.username' => SORT_ASC],
            'desc' => ['user.username' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'owner_id' => $this->owner_id,
            'property_type_id' => $this->property_type_id,
            'city_id' => $this->city_id,
            'business_city_id' => $this->business_city_id,
            'youngest_age' => $this->youngest_age,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'reservations_email', $this->reservations_email])
            ->andFilterWhere(['like', 'business_name', $this->business_name])
            ->andFilterWhere(['like', 'business_id_number', $this->business_id_number])
            ->andFilterWhere(['like', 'business_address', $this->business_address])
            ->andFilterWhere(['like', 'business_postal_code', $this->business_postal_code])
            ->andFilterWhere(['like', 'invoice_email', $this->invoice_email])
            ->andFilterWhere(['like', 'payment_methods_for_gauarantee', $this->payment_methods_for_gauarantee])
            ->andFilterWhere(['like', 'payment_methods_checkin', $this->payment_methods_checkin])
            ->andFilterWhere(['like', 'cities.name', $this->city])
            ->andFilterWhere(['like', 'user.username', $this->user]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "estimated_arrival_times".
 *
 * @property string $id
 * @property integer $time_group
 * @property string $text
 * @property string $start_time
 * @property string $end_time
 */
class EstimatedArrivalTimes extends \yii\db\ActiveRecord
{
    public static $time_groups = ['Hourly','Bi-hourly','Quad-hourly'];
    public static $text = 
    [
        'before' => 'Before',
        'after' => 'After',
    ];

    public $text_switch;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estimated_arrival_times';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_group'],'required'],
            [['time_group'], 'integer'],
            [['start_time', 'end_time'], 'safe'],
            [['text'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'time_group' => Yii::t('app', 'Time Group'),
            'text' => Yii::t('app', 'Text'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
        ];
    }
}

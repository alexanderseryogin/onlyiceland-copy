<?php

use yii\db\Migration;

class m170623_072237_alter_table_bed_combinations_add_field_short_code extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `beds_combinations`   
                          ADD COLUMN `short_code` VARCHAR(10) NULL AFTER `icon`;
                        ');
    }

    public function down()
    {
        echo "m170623_072237_alter_table_bed_combinations_add_field_short_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

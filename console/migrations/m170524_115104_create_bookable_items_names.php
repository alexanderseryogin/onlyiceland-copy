<?php

use yii\db\Migration;

class m170524_115104_create_bookable_items_names extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `bookable_items_names`( `id` INT(11) UNSIGNED NOT NULL, `bookable_item_id` INT(11), `item_number` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `item_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ); 
            ALTER TABLE `bookable_items_names` ADD CONSTRAINT `FK_bookable_items_names_bookable_item_id` FOREIGN KEY (`bookable_item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE; 
            ALTER TABLE `only_iceland`.`bookable_items_names` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT; 

        ");
    }

    public function down()
    {
        echo "m170524_115104_create_bookable_items_names cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

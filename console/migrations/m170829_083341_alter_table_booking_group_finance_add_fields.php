<?php

use yii\db\Migration;

class m170829_083341_alter_table_booking_group_finance_add_fields extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group_finance`   
                          ADD COLUMN `price_description` LONGTEXT NULL AFTER `booking_group_id`,
                          ADD COLUMN `date` DATE NULL AFTER `price_description`,
                          ADD COLUMN `show_receipt` INT(11) NULL AFTER `amount`;
                        ');
    }

    public function down()
    {
        echo "m170829_083341_alter_table_booking_group_finance_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

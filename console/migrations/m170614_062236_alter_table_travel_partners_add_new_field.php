<?php

use yii\db\Migration;

class m170614_062236_alter_table_travel_partners_add_new_field extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `travel_partner`   
                          ADD COLUMN `business_contact` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `primary_contact`;
                        ');
    }

    public function down()
    {
        echo "m170614_062236_alter_table_travel_partners_add_new_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

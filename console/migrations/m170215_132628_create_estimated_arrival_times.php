<?php

use yii\db\Migration;

class m170215_132628_create_estimated_arrival_times extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `estimated_arrival_times`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `time_group` INT(11), `text` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `start_time` TIME, `end_time` TIME, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170215_132628_create_estimated_arrival_times cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

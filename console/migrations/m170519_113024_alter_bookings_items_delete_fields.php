<?php

use yii\db\Migration;

class m170519_113024_alter_bookings_items_delete_fields extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookings_items`   
              DROP COLUMN `rates_id`, 
              DROP COLUMN `master`, 
              DROP COLUMN `estimated_arrival_time_id`, 
              DROP COLUMN `no_of_nights`, 
              DROP COLUMN `user_id`, 
              DROP COLUMN `guest_id`, 
              DROP COLUMN `quantity`, 
              DROP COLUMN `booking_cancellation`, 
              DROP COLUMN `flag_id`, 
              DROP COLUMN `status_id`, 
              DROP COLUMN `confirmation_id`, 
              DROP COLUMN `no_of_children`, 
              DROP COLUMN `no_of_adults`, 
              DROP COLUMN `person_no`, 
              DROP COLUMN `person_price`, 
              DROP COLUMN `total`, 
              DROP COLUMN `price_json`, 
              DROP COLUMN `beds_combinations_id`, 
              DROP COLUMN `travel_partner_id`, 
              DROP COLUMN `offers_id`, 
              DROP COLUMN `guest_user_first_name`, 
              DROP COLUMN `guest_user_last_name`, 
              DROP COLUMN `guest_user_country`, 
              CHANGE `rate_conditions` `rate_conditions` TINYINT(1) NULL  AFTER `departure_date`,
              CHANGE `pricing_type` `pricing_type` TINYINT(1) NULL  AFTER `rate_conditions`,
              CHANGE `created_by` `created_by` INT(11) NULL  AFTER `pricing_type`, 
              drop index `bookings_items_flag_id`,
              drop index `bookings_items_status_id`,
              drop index `bookings_items_confirmation_id`,
              drop index `FK_bookings_items_rates_id`,
              drop index `bookings_items_user_id`,
              drop index `bookings_items_beds_combinations_id`,
              drop index `bookings_items_travel_partner_id`,
              drop index `bookings_items_offers_id`,
              drop index `bookings_estimated_arrival_time_id`,
              drop index `FK_bookings_items_guest_id`,
              drop index `FK_booking_items_country_id`,
              drop foreign key `FK_booking_items_country_id`,
              drop foreign key `FK_bookings_items_guest_id`,
              drop foreign key `FK_bookings_items_rates_id`,
              drop foreign key `bookings_estimated_arrival_time_id`,
              drop foreign key `bookings_items_beds_combinations_id`,
              drop foreign key `bookings_items_confirmation_id`,
              drop foreign key `bookings_items_flag_id`,
              drop foreign key `bookings_items_offers_id`,
              drop foreign key `bookings_items_status_id`,
              drop foreign key `bookings_items_travel_partner_id`,
              drop foreign key `bookings_items_user_id`;
            ");
    }

    public function down()
    {
        echo "m170519_113024_alter_bookings_items_delete_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

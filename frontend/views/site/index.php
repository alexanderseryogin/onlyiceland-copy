<?php
/** @var States $states */

$this->title = 'Home';

use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationTypes;
use common\models\AccommodationTypes;
use yii\helpers\ArrayHelper;
use common\models\States;
use common\models\Tags;

?>
<!-- Site Showcase -->
<div class="site-showcase">

    <div class="slider-mask overlay-transparent"></div>
    <!-- Start Hero Slider -->
    <div style="z-index: -1;" class="hero-slider flexslider clearfix" data-autoplay="yes" data-pagination="no"
         data-arrows="yes" data-style="fade" data-pause="yes">
        <ul class="slides">
            <li class="parallax"
                style="background-image:url(<?= Yii::getAlias('@web') . '/frontend/assets/img/slider-images/photo-1.png' ?>);">
            </li>
            <li class="parallax"
                style="background-image:url(<?= Yii::getAlias('@web') . '/frontend/assets/img/slider-images/photo-2.png' ?>);">
            </li>
            <li class="parallax"
                style="background-image:url(<?= Yii::getAlias('@web') . '/frontend/assets/img/slider-images/photo-3.png' ?>);">
            </li>
        </ul>

    </div>

    <!-- Map -->
    <div class="container" id="map_text">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div id="mapsvg"></div>
            </div>
            <div class="col-sm-6 hidden-xs" style=" padding-top: 100px;">
                <h1 class="heading">Welcome to Only Iceland</h1>
                <h4 class="heading">The world has many countries, yet only one Iceland. The world has many travel
                    booking sites, yet only one dedicated to Iceland. Created in Iceland for Iceland: Only Iceland.
                    We know local.™</h4>
            </div>
        </div>
    </div>

    <div class="hidden-xs hidden-sm" style="height: 100px;"></div>

    <!-- End Hero Slider -->
</div>

<!-- Start Content -->
<div class="main" role="main">
    <div id="content" class="content full">

        <!-- Site Search Module -->
        <div class="site-search-module">
            <div class="container">
                <div class="site-search-module-inside">
                    <form action="<?= Url::to(['/search/items']) ?>" method="GET">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <select name="region_id" class="form-control input-lg selectpicker">
                                        <option selected value="">Select Region</option>
                                        <?php
                                        foreach ($states as $key => $state) {
                                            echo '<option value="' . $state->id . '">' . $state->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <div class="provider_types">
                                        <select name="destination_type" id="provider_type"
                                                class="form-control input-lg selectpicker">
                                            <option selected value="">Select Destination Type</option>
                                            <?php
                                            $types = DestinationTypes::find()->orderBy('name ASC')->all();
                                            $result = ArrayHelper::map($types, 'id', 'name');

                                            foreach ($result as $key => $value) {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 accommodations">
                                    <div>
                                        <select name="accommodation_id" id="accommodation_id"
                                                class="form-control input-lg selectpicker">
                                            <option selected value="">Select Accommodation Type</option>
                                            <?php
                                            $types = AccommodationTypes::find()->orderBy('name ASC')->all();
                                            $result = ArrayHelper::map($types, 'id', 'name');

                                            foreach ($result as $key => $value) {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 resturants">
                                    <div>
                                        <select name="resturant_id" id="resturant_id"
                                                class="form-control input-lg selectpicker">
                                            <option selected value="">Select Restaurant Type</option>
                                            <?php
                                            $types = Tags::find()->where(['type' => 3])->orderBy('name ASC')->all();
                                            $result = ArrayHelper::map($types, 'id', 'name');

                                            foreach ($result as $key => $value) {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2 pull-right">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg"><i
                                                class="fa fa-search"></i> Search
                                    </button>
                                </div>
                                <div class="col-md-2 pull-right"><a href="#" id="ads-trigger"
                                                                    class="btn btn-default btn-block"><i
                                                class="fa fa-plus"></i> <span>Advanced</span></a></div>
                            </div>
                            <div class="row advance-search-div" style="display: none"> <!--  -->
                                <div class="col-md-2">
                                    <label>Min Beds</label>
                                    <select name="min_beds" class="form-control input-lg selectpicker">
                                        <option selected="" disabled>Any</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>Min Baths</label>
                                    <select name="min_baths" class="form-control input-lg selectpicker">
                                        <option selected="" disabled>Any</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>Min Price</label>
                                    <select name="min_price" class="form-control input-lg selectpicker">
                                        <option selected="" disabled>Any</option>
                                        <option>$1000</option>
                                        <option>$5000</option>
                                        <option>$10000</option>
                                        <option>$50000</option>
                                        <option>$100000</option>
                                        <option>$500000</option>
                                        <option>$1000000</option>
                                        <option>$3000000</option>
                                        <option>$5000000</option>
                                        <option>$10000000</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>Max Price</label>
                                    <select name="max_price" class="form-control input-lg selectpicker">
                                        <option selected="" disabled>Any</option>
                                        <option>$1000</option>
                                        <option>$5000</option>
                                        <option>$10000</option>
                                        <option>$50000</option>
                                        <option>$100000</option>
                                        <option>$500000</option>
                                        <option>$1000000</option>
                                        <option>$3000000</option>
                                        <option>$5000000</option>
                                        <option>$10000000</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label>Min Area (Sq Ft)</label>
                                    <input type="text" class="form-control input-lg" name="min_area_any"
                                           placeholder="Any">
                                </div>
                                <div class="col-md-2">
                                    <label>Max Area (Sq Ft)</label>
                                    <input type="text" class="form-control input-lg" name="max_area_any"
                                           placeholder="Any">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="visible-xs visible-sm" style="height: 30px;"></div>

        <?php
        $session = Yii::$app->session;
        $session->open();
        $session['gridview'] = 0;
        ?>

        <div class="featured-blocks">
            <div class="container">
                <div class="row">
                    <?php foreach ($states as $state) { ?>
                        <div class="col-md-3 col-sm-3">
                            <div class="region-block">
                                <img src="<?= '/uploads' . $state->img ?>" alt="<?= $state->description ;?>"
                                     class="img-thumbnail">
                                <div class="featured-block">
                                    <br>
                                    <h3><?= $state->description ;?></h3>
                                    <p>
                                        <?php
                                            $counter = 0;
                                            $cities = $state->cities;
                                            foreach ($cities as $city) {
                                                echo '<a href="' . Url::to(['/search/iceland-town-destinations', 'region_name' => $state->alias, 'town_name' => $city->name]) . '" class="towns">' . $city->name . '</a> ';
                                                if ($counter < count($cities) - 1) {
                                                    echo '&bull; ';
                                                }
                                                $counter++;
                                            }
                                        ?>
                                    </p><br>
                                    <a href="<?= Url::to(['/search/region-destinations', 'region_name' => $state->alias]) ?>" class="btn btn-primary"><?= $state->button_text;?></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="padding-tb45 bottom-blocks">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 features-list column">
                        <h3>Theme features</h3>
                        <ul>
                            <li>
                                <div class="icon"><i class="fa fa-umbrella"></i></div>
                                <div class="text">
                                    <h4>Lots of possibilities</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon"><i class="fa fa-list"></i></div>
                                <div class="text">
                                    <h4>Property list view</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon"><i class="fa fa-search"></i></div>
                                <div class="text">
                                    <h4>Advance Search Options</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon"><i class="fa fa-users"></i></div>
                                <div class="text">
                                    <h4>Agents Profile</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 popular-agent column">
                        <h3>Popular Agent</h3>
                        <a href="agent-detail.html"><img src="//placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER"
                                                         alt=""
                                                         class="img-thumbnail"></a>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <h4><a href="agent-detail.html">Brooklyn Coyle</a></h4>
                                <a href="agent-detail.html" class="btn btn-sm btn-primary">more details</a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <ul class="contact-info">
                                    <li><i class="fa fa-phone"></i> +87 6543 210</li>
                                    <li><i class="fa fa-envelope"></i> brook@gmail.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 latest-testimonials column">
                        <h3>Client Testimonials</h3>
                        <ul class="testimonials">
                            <li>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas
                                    rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Lorem ipsum
                                    dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus.
                                    Lorem
                                    ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                                    consectetur adipiscing.</p>
                                <img src="//placehold.it/80x80&amp;text=IMAGE+PLACEHOLDER" alt="Happy Client"
                                     class="testimonial-sender">
                                <cite>Mellisa - <strong>My company</strong>
                                    <br><a href="#">www.companyurl.com</a>
                                </cite>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

//$stateUrl =  Url::to(['/search/items'],true);
$stateUrl = '/region/';
$root = Yii::getAlias('@web');
$acc_value = DestinationTypes::findOne(['name' => 'Accommodation']);
$acc_id = $acc_value->id;
$rest_value = DestinationTypes::findOne(['name' => 'Restaurant']);
$rest_id = $rest_value->id;
$this->registerJs('
  jQuery(document).ready(function() 
  {
      $("#ads-trigger").click(function () 
      { 
        if($(this).hasClass("advanced")) 
        {
          $(this).removeClass("advanced");
          $(".advance-search-div").removeClass("hidden-xs hidden-sm");
          $(".advance-search-div").css("display", "none");

          $(".site-search-module-inside").animate({
            "height": "80px"
          });

          $(".site-search-module").animate({
            "height": "120px"
          });

          $(this).html("<i class=\'fa fa-plus\'></i> Advanced");
        } 
        else 
        { 
          $(this).addClass("advanced");
          $(".advance-search-div").addClass("hidden-xs hidden-sm");
          $(".advance-search-div").css("display", "block");

          $(".site-search-module-inside").animate({
            "height": "175px"
          });

          $(".site-search-module").animate({
            "height": "190px"
          });

          $(this).html("<i class=\'fa fa-minus\'></i> Basic");
        } 
        return false;
      });

      var acc_id = ' . $acc_id . ';
      var res_id = ' . $rest_id . ';
      $(".accommodations").hide();
      $(".resturants").hide();

      $(\'select[name="destination_type"]\').change(function ()
      {
        if($("#provider_type option:selected").val()==acc_id)
        {
          $(".accommodations").show();
        }
        else
        {
          $(".accommodations").hide();
        }

        if($("#provider_type option:selected").val()==res_id)
        {
          $(".resturants").show();
        }
        else
        {
          $(".resturants").hide();
        }
      });

      var root = "' . $root . '";
      svgUrl = root+"/themes/realspaces/resources/mapsvg/maps/geo-calibrated/iceland.svg";

      $("#mapsvg").mapSvg(
      {
        source: svgUrl,
        colors: {
                  background: "none",
                  selected: 40,
                  hover: 20,
                  base: "#00AAB5",
                },
        regions: 
        {
          "GL1": {
              data: {region_id:  4123},
              tooltip: "Glacier 1",
              href: "' . $stateUrl . 'Suðurland",
            },
          "GL2": {
              data: {region_id:  4122},
              tooltip: "Glacier 2",
              href: "' . $stateUrl . 'Norðurland-vestra",
            },
          " GL9": {
              data: {region_id: 4121},
              tooltip: "Glacier 9",
              href: "' . $stateUrl . 'Austurland",
            },
          "IS-1": {
              data: {region_id: 1658},
              tooltip: "Höfuðborgarsvæðið",
              href: "' . $stateUrl . 'Höfuðborgarsvæðið",
              fill: "#DE74AB",
            },
          "IS-2": {
              data: {region_id: 1663},
              tooltip: "Suðurnes",
              href: "' . $stateUrl . 'Suðurnes",
              fill: "#C28F31",
            },
          "IS-3": {
              data: {region_id: 1665},
              tooltip: "Vesturland",
              href: "' . $stateUrl . 'Vesturland",
              fill: "#ABC30A",
            },
          "IS-4": {
              data: {region_id: 1664},
              tooltip: "Vestfirðir",
              href: "' . $stateUrl . 'Vestfirðir",
              fill: "#EDD000",
            },
          "IS-5": {
              data: {region_id: 1661},
              tooltip: "Norðurland vestra",
              href: "' . $stateUrl . 'Norðurland-vestra",
              fill: "#FB7B53",
            },
          "IS-6": {
              data: {region_id: 1660},
              tooltip: "Norðurland eystra",
              href: "' . $stateUrl . 'Norðurland-eystra",
              fill: "#F1C258",
            },
          "IS-7": {
              data: {region_id: 1657},
              tooltip: "Austurland",
              href: "' . $stateUrl . 'Austurland",
              fill: "#C99AA2",
            },
          "IS-8": {
              data: {region_id: 1662},
              tooltip: "Suðurland",
              href: "' . $stateUrl . 'Suðurland",
              fill: "#B597C8",
            },
        }

      });
  });', View::POS_END);
?>
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_group_cart".
 *
 * @property string $id
 * @property string $group_name
 * @property integer $group_user_id
 * @property string $group_guest_user_id
 * @property integer $confirmation_id
 * @property string $cancellation_id
 * @property integer $flag_id
 * @property integer $status_id
 * @property string $travel_partner_id
 * @property string $voucher_no
 * @property string $reference_no
 * @property integer $offers_id
 * @property string $estimated_arrival_time_id
 * @property double $balance
 * @property string $comments
 *
 * @property BookingConfirmationTypes $confirmation
 * @property EstimatedArrivalTimes $estimatedArrivalTime
 * @property BookingTypes $flag
 * @property Offers $offers
 * @property BookingStatuses $status
 * @property DestinationTravelPartners $travelPartner
 * @property BookingGuestsCart $groupGuestUser
 * @property User $groupUser
 */
class BookingGroupCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_group_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_user_id', 'group_guest_user_id', 'confirmation_id', 'flag_id', 'status_id', 'travel_partner_id', 'offers_id', 'estimated_arrival_time_id'], 'integer'],
            [['balance'], 'number'],
            [['comments'], 'string'],
            [['group_name', 'cancellation_id', 'voucher_no', 'reference_no'], 'string', 'max' => 256],
            [['confirmation_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['confirmation_id' => 'id']],
            [['estimated_arrival_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => EstimatedArrivalTimes::className(), 'targetAttribute' => ['estimated_arrival_time_id' => 'id']],
            [['flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['flag_id' => 'id']],
            [['offers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offers::className(), 'targetAttribute' => ['offers_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['travel_partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTravelPartners::className(), 'targetAttribute' => ['travel_partner_id' => 'id']],
            [['group_guest_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGuestsCart::className(), 'targetAttribute' => ['group_guest_user_id' => 'id']],
            [['group_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['group_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'group_user_id' => 'Group User ID',
            'group_guest_user_id' => 'Group Guest User ID',
            'confirmation_id' => 'Confirmation ID',
            'cancellation_id' => 'Cancellation ID',
            'flag_id' => 'Flag ID',
            'status_id' => 'Status ID',
            'travel_partner_id' => 'Travel Partner ID',
            'voucher_no' => 'Voucher No',
            'reference_no' => 'Reference No',
            'offers_id' => 'Offers ID',
            'estimated_arrival_time_id' => 'Estimated Arrival Time ID',
            'balance' => 'Balance',
            'comments' => 'Comments',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmation()
    {
        return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'confirmation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimatedArrivalTime()
    {
        return $this->hasOne(EstimatedArrivalTimes::className(), ['id' => 'estimated_arrival_time_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlag()
    {
        return $this->hasOne(BookingTypes::className(), ['id' => 'flag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasOne(Offers::className(), ['id' => 'offers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(BookingStatuses::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravelPartner()
    {
        return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupGuestUser()
    {
        return $this->hasOne(BookingGuestsCart::className(), ['id' => 'group_guest_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupUser()
    {
        return $this->hasOne(User::className(), ['id' => 'group_user_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItemGroups()
    {
        return $this->hasMany(BookingItemGroupCart::className(), ['booking_group_id' => 'id']);
    }

    // public function getBookingGroupSpecialRequests() 
    // { 
    //     return $this->hasMany(BookingGroupSpecialRequests::className(), ['booking_group_id' => 'id']); 
    // } 
}

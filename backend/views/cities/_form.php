<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\States;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Cities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'state_id', [
    	'inputOptions' => [
    		'id' => 'state-dropdown',
    		'class' => 'form-control',
    		'style' => 'max-width: 500px'
    		]
    	])->dropdownList(ArrayHelper::map(States::find()->all(),'id','name'),['prompt'=>'Select a State']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/cities']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
jQuery(document).ready(function() {
    // select2 for state list
    $('#state-dropdown').select2({
        placeholder: \"Select a State\"
    });
});",View::POS_END);
?>
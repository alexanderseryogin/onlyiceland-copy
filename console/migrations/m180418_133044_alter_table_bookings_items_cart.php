<?php

use yii\db\Migration;

class m180418_133044_alter_table_bookings_items_cart extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bookings_items_cart`   
                          ADD COLUMN `notes` LONGTEXT NULL AFTER `comments`;
                        ');
    }

    public function safeDown()
    {
        echo "m180418_133044_alter_table_bookings_items_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180418_133044_alter_table_bookings_items_cart cannot be reverted.\n";

        return false;
    }
    */
}

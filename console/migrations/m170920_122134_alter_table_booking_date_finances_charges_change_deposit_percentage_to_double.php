<?php

use yii\db\Migration;

class m170920_122134_alter_table_booking_date_finances_charges_change_deposit_percentage_to_double extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group_finance`   
                          CHANGE `deposit_percent_age` `deposit_percent_age` DOUBLE NULL;
                        ');
    }

    public function down()
    {
        echo "m170920_122134_alter_table_booking_date_finances_charges_change_deposit_percentage_to_double cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

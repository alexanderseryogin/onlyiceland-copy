<?php

use yii\db\Migration;

class m161014_105809_create_table_providers_special_requests extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_special_requests`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `request_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_provider_request_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_provider_request_request_id` FOREIGN KEY (`request_id`) REFERENCES `special_requests`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161014_105809_create_table_providers_special_requests cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

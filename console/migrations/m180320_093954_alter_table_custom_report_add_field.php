<?php

use yii\db\Migration;

class m180320_093954_alter_table_custom_report_add_field extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_reports`   
                          ADD COLUMN `sum_average` INT(11) NULL AFTER `referer_not_operator`;
                        ');
    }       

    public function safeDown()
    {
        echo "m180320_093954_alter_table_custom_report_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180320_093954_alter_table_custom_report_add_field cannot be reverted.\n";

        return false;
    }
    */
}

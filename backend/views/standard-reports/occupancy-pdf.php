<?php
use common\models\BookingDates;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<?= GridView::widget([

    'id' => 'bookings_gridview',
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'options' => ['class' => 'grid-view fixed-layout','Gridlines' => "None",'BorderStyle'=> "None"],
    'layout'=>"\n{items}",
    'columns' => [
        //'id',
        [
            'attribute' => 'date',
            'value'     => function($data)
            {
                //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                return date('d.m.Y',strtotime($data['date'])).' '.Yii::t('app',date('l',strtotime($data['date'])));
            }
        ],
        [
            'attribute' => 'Arrivals',
            'value'     => function($data)
            {
                return $data['arrivals'];
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
            //'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        [
            'attribute' => 'Average Length of Stay',
            'value'     => function($data)
            {
                return str_replace('.', ',', $data['average_stay']);
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
            //'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        [
            'attribute' => 'Departures',
            'value'     => function($data)
            {
                return $data['departures'];
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
           // 'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        [
            'label' => 'Stay Through',
            'value'     => function($data)
            {
                return $data['stay_through'];
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
          //  'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        [
            'attribute' => 'Booked',
            'value'     => function($data)
            {
                return $data['booked_rooms'];
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
           // 'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        [
            'attribute' => 'Available',
            'value'     => function($data)
            {
                return $data['available_rooms'];
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
           // 'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        [
            'attribute' => 'Occupancy',
            'value'     => function($data)
            {
                return $data['occupancy']." %";
            },
            'contentOptions' => function($data)
            {
    
                return ['style' => 'text-align:right;' ];
            },
           // 'headerOptions' =>[ 'style' => 'width:3%;'],
        ],
        

    ],
    // 'afterRow' => function($model, $key, $index) {
    //     //$booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
    //     if(isset($model->comments) && !empty($model->comments))
    //     {
    //         return "<tr style='border-top:0px !important;'><td colspan=10 style='white-space: normal;word-wrap:break-word !important;'>".$model->comments."</td></tr>";

    //     }
            
    // }
]);?>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>

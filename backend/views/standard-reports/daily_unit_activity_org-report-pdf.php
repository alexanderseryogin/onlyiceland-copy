<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
use common\models\BookingsItems;

$title = 'Daily Unit Activity Report';
$this->title = $title;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<table class="table table-striped table-bordered daily_unit_report_table table2excel" >
	<!-- <thead style="display: none;">
		<tr>
			<th>
				
			</th>
			<th>
				
			</th>
			<th>
				
			</th>
			<th>
				
			</th>
			<th>
				
			</th>
			<th>
				
			</th>
			<th>
				
			</th>
			<th>
				
			</th>
		</tr>
		
	</thead> -->
	<tbody>
	
    	<?php
    		$date = $report_from;
	       
	        $date1=date_create($report_to);
	        $date2=date_create($report_from);
	        $diff=date_diff($date1,$date2);
	        $difference =  $diff->format("%a");

	        for ($i=0; $i <= $difference ; $i++)
	        {
	            echo "<tr>";
		            echo "<td  style='font-size:25px;border-right: 0px;'>";
		            echo date('d.m.Y',strtotime($date));
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-right: 0px;border-left:0px;'>";
		            echo "</td>";
		            echo "<td style='border-left:0px;'>";
		            echo "</td>";
	            echo "</tr>";
	            echo "<tr>";
		            echo "<td>";
		            echo "<b>Item Name</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>Item Type</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>Bed Combination</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>State</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>Guest Name</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>A/C</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>No of Nights</b>";
		            echo "</td>";
		            echo "<td>";
		            echo "<b>ETA</b>";
		            echo "</td>";
	            echo "</tr>";
	            if($cancelled_bookings)
	            {

	            	foreach ($destination->bookableItems as $bookable_item) 
	            	{
	            		foreach ($bookable_item->bookableItemsNames as $item_name) 
	            		{
	            			echo "<tr>";
				            echo "<td>";
				            echo $item_name->item_name;
				            echo "</td>";
				            
				            echo "<td >";
				            echo $bookable_item->itemType->name;
				            echo "</td>";

				            $bookingDateModel = BookingDates::findOne(['date' => $date,'item_id' => $bookable_item->id, 'item_name_id' => $item_name->id]);

		                    if(!empty($bookingDateModel))
		                        $bed_pref =  isset($bookingDateModel->beds_combinations_id)?$bookingDateModel->bedsCombinations->combination:'- - - - -';
		                    else
		                         $bed_pref = '- - - - -'.' ';

				            echo "<td >";
				            echo $bed_pref;
				            echo "</td>";

				            // $bookingDateModel = BookingDates::findOne(['date' => $date,'item_id' => $bookable_item->id, 'item_name_id' => $item_name->id]);
		                    $guest_string = '';

		                    if(!empty($bookingDateModel->user_id))
		                    {
		                        $guest_string .= $bookingDateModel->user->profile->first_name.' ';
		                        $guest_string .= $bookingDateModel->user->profile->last_name;
		                    }
		                    else
		                    {
		                    	if(isset($bookingDateModel))
		                    	{
		                    		$guest_string .= $bookingDateModel->guest_first_name.' ';
		                        	$guest_string .= $bookingDateModel->guest_last_name;
		                    	}
		                    	else
		                    	{
		                    		$guest_string = '- - - - -'.' ';
		                    	}
		                        
		                    }

		                    $arrival = BookingsItems::findOne(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id,'arrival_date' => $date]);

		                    $departure = BookingsItems::findOne(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id,'departure_date' => $date]);

		                    $occupied = BookingsItems::find()->where(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id])->andWhere(['<','arrival_date',$date])->andWhere(['>','departure_date',$date])->all();
		                    $state = '';
		                    if(!empty($arrival))
		                    {
		                    	$state = 'ARRIVAL';
		                    }
		                    elseif(!empty($departure))
		                    {
		                    	$state = 'DEPARTURE';
		                    }
		                    elseif (!empty($occupied)) 
		                    {
		                    	$state = 'OCCUPIED';
		                    }
		                    else
		                    {
		                    	$next_arrival = BookingsItems::find()->where(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id])->andWhere(['>','arrival_date',$date])->one();
		                    	if(!empty($next_arrival))
		                    	{
		                    		$state = 'VACANT - next arrival ('.date('d.m.Y', strtotime($next_arrival->arrival_date)).')';
		                    	}
		                    	else
		                    	{
		                    		$state = 'NO NEW ARRIVAL';
		                    	}
		                    	
		                    }
				            echo "<td >";
				            echo $state;
				            echo "</td>";

				            echo "<td >";
				            echo $guest_string;
				            echo "</td>";

				            echo "<td style='text-align:right'>";
				            echo isset($bookingDateModel)?$bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children:'- - - - -';
				            echo "</td>";

				            if (isset($bookingDateModel)) 
				            {
				            	$date1=date_create($bookingDateModel->bookingItem->departure_date);
			                    $date2=date_create($bookingDateModel->bookingItem->arrival_date);
			                    $diff=date_diff($date1,$date2);
			                    $difference1 =  $diff->format("%a");
				            }
				            else
				            {
				            	$difference1 = '- - - - -';
				            }
				            

				            echo "<td style='text-align:right'>";
				            echo $difference1;
				            echo "</td>";


				            if(isset($bookingDateModel->estimatedArrivalTime))
				            {
				            	$eta = $bookingDateModel->estimatedArrivalTime->start_time;
				            }
				            else
				            {
				            	$eta = '- - - - -';
				            }
				            echo "<td>";
				            echo $eta;
				            echo "</td>";

				            echo "</tr>";

				            if(isset($bookingDateModel))
				            {
				            	$comments = $bookingDateModel->bookingItem->comments;
				            }
				            else
				            {
				            	$comments = '';
				            }

				            if($comments != '')
				            {
				            	echo "<tr>";
						            echo "<td  colspan=8>" ;
						            echo $comments ;
						            echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-left:0px;'>";
						            echo "</td>";
				            	echo "</tr>";
				            }
	            		}
	            	}
	            }
	            else
	            {
	            	foreach ($destination->bookableItems as $bookable_item) 
	            	{
	            		foreach ($bookable_item->bookableItemsNames as $item_name) 
	            		{
	            			echo "<tr>";
				            echo "<td>";
				            echo $item_name->item_name;
				            echo "</td>";
				            
				            echo "<td >";
				            echo $bookable_item->itemType->name;
				            echo "</td>";

				            $bookingDateModel = BookingDates::findOne(['date' => $date,'item_id' => $bookable_item->id, 'item_name_id' => $item_name->id]);

		                    if(!empty($bookingDateModel))
		                        $bed_pref =  isset($bookingDateModel->beds_combinations_id)?$bookingDateModel->bedsCombinations->combination:'- - - - -';
		                    else
		                         $bed_pref = '- - - - -'.' ';

				            echo "<td >";
				            echo $bed_pref;
				            echo "</td>";

				            // $bookingDateModel = BookingDates::findOne(['date' => $date,'item_id' => $bookable_item->id, 'item_name_id' => $item_name->id]);
		                    $guest_string = '';

		                    if(!empty($bookingDateModel->user_id))
		                    {
		                        $guest_string .= $bookingDateModel->user->profile->first_name.' ';
		                        $guest_string .= $bookingDateModel->user->profile->last_name;
		                    }
		                    else
		                    {
		                    	if(isset($bookingDateModel))
		                    	{
		                    		$guest_string .= $bookingDateModel->guest_first_name.' ';
		                        	$guest_string .= $bookingDateModel->guest_last_name;
		                    	}
		                    	else
		                    	{
		                    		$guest_string = '- - - - -'.' ';
		                    	}
		                        
		                    }

		                    $arrival = BookingsItems::findOne(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id,'arrival_date' => $date]);

		                    $departure = BookingsItems::findOne(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id,'departure_date' => $date]);

		                    $occupied = BookingsItems::find()->where(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id])->andWhere(['<','arrival_date',$date])->andWhere(['>','departure_date',$date])->all();
		                    $state = '';
		                    if(!empty($arrival))
		                    {
		                    	$state = 'ARRIVAL';
		                    }
		                    elseif(!empty($departure))
		                    {
		                    	$state = 'DEPARTURE';
		                    }
		                    elseif (!empty($occupied)) 
		                    {
		                    	$state = 'OCCUPIED';
		                    }
		                    else
		                    {
		                    	$next_arrival = BookingsItems::find()->where(['item_id' => $bookable_item->id, 'item_name_id' => $item_name->id])->andWhere(['>','arrival_date',$date])->one();
		                    	if(!empty($next_arrival))
		                    	{
		                    		$state = 'VACANT - next arrival ('.date('d.m.Y', strtotime($next_arrival->arrival_date)).')';
		                    	}
		                    	else
		                    	{
		                    		$state = 'NO NEW ARRIVAL';
		                    	}
		                    }
				            echo "<td >";
				            echo $state;
				            echo "</td>";

				            echo "<td >";
				            echo $guest_string;
				            echo "</td>";

				            echo "<td style='text-align:right'>";
				            echo isset($bookingDateModel)?$bookingDateModel->no_of_adults.'/'.$bookingDateModel->no_of_children:'- - - - -';
				            echo "</td>";

				            if (isset($bookingDateModel)) 
				            {
				            	$date1=date_create($bookingDateModel->bookingItem->departure_date);
			                    $date2=date_create($bookingDateModel->bookingItem->arrival_date);
			                    $diff=date_diff($date1,$date2);
			                    $difference1 =  $diff->format("%a");
				            }
				            else
				            {
				            	$difference1 = '- - - - -';
				            }
				            

				            echo "<td style='text-align:right'>";
				            echo $difference1;
				            echo "</td>";

				            if(isset($bookingDateModel->estimatedArrivalTime))
				            {
				            	$eta = $bookingDateModel->estimatedArrivalTime->start_time;
				            }
				            else
				            {
				            	$eta = '- - - - -';
				            }

				            echo "<td>";
				            echo $eta;
				            echo "</td>";

				            echo "</tr>";

				            if(isset($bookingDateModel))
				            {
				            	$comments = $bookingDateModel->bookingItem->comments;
				            }
				            else
				            {
				            	$comments = '';
				            }

				            if($comments != '')
				            {
				            	echo "<tr>";
						            echo "<td  colspan=8>" ;
						            echo $comments ;
						            echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-right: 0px;border-left:0px;'>";
						            // echo "</td>";
						            // echo "<td style='border-left:0px;'>";
						            // echo "</td>";
				            	echo "</tr>";
				            }
	            		}
	            	}
	            }
	         
	            $date = strtotime("+1 day", strtotime($date));
	            $date = date("Y-m-d", $date);
	        }
    	?>
    </tbody>
</table>


<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
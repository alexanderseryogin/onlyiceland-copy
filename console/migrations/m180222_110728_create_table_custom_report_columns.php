<?php

use yii\db\Migration;

class m180222_110728_create_table_custom_report_columns extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `custom_report_columns`(
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `column` VARCHAR(256),
                          `custom_report_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`custom_report_id`) REFERENCES `custom_reports`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        );
                        ');
    }

    public function safeDown()
    {
        echo "m180222_110728_create_table_custom_report_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180222_110728_create_table_custom_report_columns cannot be reverted.\n";

        return false;
    }
    */
}

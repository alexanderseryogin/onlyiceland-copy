<?php

use yii\db\Migration;

class m170303_093533_alter_booking_items_add_master_field extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` CHANGE `rates_id` `rates_id` INT(11) NULL AFTER `item_id`, ADD COLUMN `master` TINYINT(1) NULL AFTER `rates_id`; 
        ");
    }

    public function down()
    {
        echo "m170303_093533_alter_booking_items_add_master_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

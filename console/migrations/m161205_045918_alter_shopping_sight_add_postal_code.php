<?php

use yii\db\Migration;

class m161205_045918_alter_shopping_sight_add_postal_code extends Migration
{
    public function up()
    {
        $this->execute("
                ALTER TABLE `providers_shopping` ADD COLUMN `postal_code` VARCHAR(256) NULL AFTER `price_range`; 
                ALTER TABLE `providers_sight` ADD COLUMN `postal_code` VARCHAR(256) NULL AFTER `price_range`; 
            ");
    }

    public function down()
    {
        echo "m161205_045918_alter_shopping_sight_add_postal_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

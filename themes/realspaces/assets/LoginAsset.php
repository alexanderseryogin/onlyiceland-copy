<?php

namespace realspaces\assets;

use yii\web\AssetBundle;
use yii\web\View;

class LoginAsset extends AssetBundle
{
    public $sourcePath = '@realspaces/resources';
    public $css = [
        //CSS
        'css/bootstrap.css',
        'css/style.css',
        'plugins/prettyphoto/css/prettyPhoto.css',
        'plugins/owl-carousel/css/owl.carousel.css',
        'plugins/owl-carousel/css/owl.theme.css',

        //Color Style
        'colors/color4.css',

        //Modernizer script
        'js/modernizr.js',
    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [

        //'js/jquery-2.0.0.min.js',
        'plugins/prettyphoto/js/prettyphoto.js',
        'plugins/owl-carousel/js/owl.carousel.min.js',
        'plugins/flexslider/js/jquery.flexslider.js',
        'js/helper-plugins.js',
        //'js/bootstrap.js',
        'js/waypoints.js',
        'js/init.js',
        'js/bootstrap-pwstrength/pwstrength-bootstrap.min.js',

    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
}
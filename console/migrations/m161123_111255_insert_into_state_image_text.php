<?php

use yii\db\Migration;

class m161123_111255_insert_into_state_image_text extends Migration
{
    public function up()
    {   
        $this->execute("
            INSERT INTO `state_image_text` (`state_id`) 
            VALUES  ('1657'),
            ('4123'),
            ('4122'),
            ('4121'),
            ('1658'),
            ('1660'),
            ('1661'),
            ('1662'),
            ('1663'),
            ('1664'),
            ('1665');

            ");
    }

    public function down()
    {
        echo "m161123_111255_insert_into_state_image_text cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

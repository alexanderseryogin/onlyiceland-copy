<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;

ob_start();
?>

<style>
    .hint-block{
        color:blue;
    }
    .show-fields{ display: block; }
    .hide-fields{ display: none; }
    .modal-help-block, .email-help-block
    {
        color:red;
    }
    .select2-search__field{
        width : 120px !important;
    }
</style>

<script type="text/javascript">
    var pricing_type = <?=!empty($model->provider)?$model->provider->pricing:0?>;
    var total_nights = 1;
</script>

<?php ?>
<?php $form = ActiveForm::begin([
                    'errorSummaryCssClass' => 'alert alert-danger',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
<div class="bookings-form" id="booking_form">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Bookings</span>
                    </div>
                    <ul class="nav nav-tabs bookings_tabs">
                        <li class="active">
                            <!-- <a href="#general_info" data-toggle="tab">General info</a> -->
                        </li>
                    </ul>
                </div>
                

                <?php echo $form->errorSummary($model); ?>

                <div class="portlet-body">
                    <div class="tab-content">
                        
                        <?php echo $this->render('general_info',
                                [
                                    'form' => $form, 
                                    'model' => $model,
                                    'bookingGroupModel' => $bookingGroupModel,
                                    'group_name'        => $group_name,  
                                ])?>

                        <?php
                            if(isset($dataProvider))
                            {
                                echo $this->render('audit_log',
                                [
                                    'dataProvider' => $dataProvider,
                                ]);
                            }
                        ?>
                    </div>
                    <br>        
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler" data-state = "0">
    <i class="icon-login"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
    <div class="page-quick-sidebar">
        <ul class="nav nav-tabs">
            <li class="active">
                <a class="quick-sidebar-title" href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Booking's Items
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active page-quick-sidebar-alerts" id="quick_sidebar_tab_1">
                <div class="page-quick-sidebar-alerts-list">
                    
                    <div class="bookings-items">
                        <?php echo $this->render('side_bar_cart_view')?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END QUICK SIDEBAR -->

<?php ActiveForm::end(); ?>

<div id="booking_item_modal" class="modal fade bs-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Booking Item</h4>
            </div>
            <div class="modal-body booking-item-modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary update-item">Update</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
            </div>
        </div>
    </div>
</div>

<?php

if(!$model->isNewRecord)
{
    $this->registerJs("
        $('#provider_dropdown').prop('disabled', true);

        $('.hide-fields').addClass('show-fields');
        $('.show-fields').removeClass('hide-fields');
        $('#details_note').hide();
    ");
}

if(!empty($model->provider_id))
{
    $this->registerJs("
        $('.hide-fields').addClass('show-fields');
        $('.show-fields').removeClass('hide-fields');
        $('#details_note').hide();
        $('#check_availability').trigger('click');
    ");

    if(empty($model->travel_partner_id) && empty($model->offers_id))
    {
        $this->registerJs
        ("
            $('#offers_dropdown').attr('disabled',false);
            $('#travel_partner_dropdown').attr('disabled',false);
        ");
    }
    else if(empty($model->travel_partner_id) && !empty($model->offers_id))
    {
        $this->registerJs
        ("
            $('#offers_dropdown').attr('disabled',false);
            $('#travel_partner_dropdown').attr('disabled','disabled');
        ");
    }
    else
    {
        $this->registerJs
        ("
            $('#offers_dropdown').attr('disabled','disabled');
            $('#travel_partner_dropdown').attr('disabled',false);
        ");
    }   
}

if(!empty($model->estimated_arrival_time_id))
{
    $this->registerJs("
        $('#estimated_arrival_time_dropdown').val('$model->estimated_arrival_time_id').change();
    ");
}

$add_user_url = Url::to(['/bookings/add-user']);
$list_items_rates_url = Url::to(['/bookings/list-items-rates']);
$list_states_url = Url::to(['/destinations/liststates']);
$list_cities_url = Url::to(['/destinations/listcities']);
$number_as_decimal = Url::to(['/bookings/number-as-decimal']);
$calculate_item_price = Url::to(['/bookings/calculate-item-price']);
$get_extra_beds_upsell_item = Url::to(['/bookings/get-extra-beds-upsell-item']);
$add_to_booking_cart = Url::to(['/bookings/add-to-booking-cart']);
$update_booking_cart_item = Url::to(['/bookings/update-booking-cart-item']);
$show_booking_item = Url::to(['/bookings/show-booking-item']);
$delete_booking_item = Url::to(['/bookings/delete-booking-item']);
$remove_cart_items_url = Url::to(['/bookings/delete-cart-items']);
$update_booking_cart_items_prices = Url::to(['/bookings/update-booking-cart-items-prices']);

$this->registerJs("
jQuery(document).ready(function() 
{
    var childTriggerClick = 0;

    //$('#submit_button').attr('disabled','disabled');
    $('.dropdown-quick-sidebar-toggler').css('display','block');

    var previous;

    // $(document).on('focus','.item-name', function () {
    //     // Store the current value on focus and on change
    //     previous = $(this).val();
    // }).change(function() {
    //     // Do something with the previous value after the change
    //     alert(previous);
    //     console.log('previous : '+previous);
    //     console.log('current: '+$(this).val());

    // });
    $(document).on('change','.item-name',function(){
        //alert($(this).val())
        console.log($(this).children('option:selected').data('data'));
        if($(this).children('option:selected').data('data') == 0)
        {
            //alert($(this).val());
            if (confirm('Continuing will OVERBOOK this item. Continue?')) {
                //alert('Thanks for confirming');
                $('.previous').val($(this).val());
            } else {
                $(this).val($('.previous').val());
            }
        }
        $('.previous').val($(this).val());
    });

    $(document).on('select2:select','.housekeeping-status', function (evt) 
    {
        //alert('here');
        console.log($(this).val());
        console.log(typeof($(this).val()));
        console.log('lengt: '+$(this).val().length);

        var match=0;
        for (var i = 0; i < $(this).val().length; i++) {
           if ($(this).val()[i] == 1) 
                match = 1;
        }
        console.log('match :'+ match);
        if(match)
        {
            $('.housekeeping-status > option').removeAttr('selected');
            $('.housekeeping-status > option').trigger('change');
            $('.housekeeping-status').select2().val(1).trigger('change');
        }
       
    });

    var selectedItems = [];
    var selectedRatesAdults = [];
    var selectedRatesChild = [];
    var selectedRatesCapacityPricing = [];
    var modal_flag = 0;

    var global_provider_id = '';
    var global_remove_rate_conditions = 0;
    var global_arrival_date = '';
    var global_departure_date = '';
    var global_estimated_arrival_time_id= '';

    var selected_arrival_date = '';
    var selected_departure_date = '';

    $('#user_dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true,
        dropdownParent: $('#group-modal')
    });

    $('#adults_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#childs_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
    });

    $('#estimated_arrival_time_dropdown').select2({
        placeholder: \"Select a Time Interval\",
    });

    $('#item_dropdown').select2({
        placeholder: \"Select a Bookable Item\",
        allowClear: true
    });

    $('#travel_partner_dropdown').select2({
        placeholder: \"Select a Travel Partner\",
        allowClear: true
    });

    $('#offers_dropdown').select2({
        placeholder: \"Select an Offer\",
        allowClear: true
    });

    $('#rates_dropdown').select2({
        placeholder: \"Select a Rate\",
        allowClear: true
    });

    $('#country_dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true,
        dropdownParent: $('#add-user-modal')
    });

    $('#state_dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true,
        dropdownParent: $('#add-user-modal')
    });

    $('#city_dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true,
        dropdownParent: $('#add-user-modal')
    });

    $('#modal-guest-country-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true,
        dropdownParent: $('#group-modal')
    });

    $('#provider_dropdown').change(function()
    {
        $('.hide-fields').addClass('show-fields');
        $('.show-fields').removeClass('hide-fields');
        $('#details_note').hide();
    });

    // $('.housekeeping-status').select2({
    //     multiple: true,
    //     placeholder: 'Select a Status',
    //     //width:'100%',
    //     //data: z 
    // });
    function numberAsDecimal(price)
    {
        if(price.length > 3)
        {
            var temp = price.length-3;
            var newPrice = '';

            for (var i = 0; i < price.length; i++) 
            {
                if (i == temp) 
                {
                    newPrice += '.';
                }
                newPrice += price[i];
            }
        }
        return newPrice;
    }

    function calculateGrandTotal()
    {
        var price = 0.0;

        $('.item-total-input').each(function()
        {
            price = parseFloat(price) +  parseFloat($(this).val());
        });

        if(price>0 || price <0)
        {
            $.ajax(
            {
                type: 'GET',
                url: '$number_as_decimal?price='+price,
                success: function(result)
                {
                    $('.grand-total').find('strong').html('Grand Total:&nbsp;&nbsp;&nbsp;'+result);
                },
            });
        }
        else
        {
            $('.grand-total').find('strong').html('Grand Total:&nbsp;&nbsp;&nbsp;'+0);
        }
    }

    // ********************************************************************************************************************************************************************************************** Js for Side bar ****************************************************************//

    $(document).on('click','.edit-booking-item',function()
    {
        $.ajax(
        {
            type: 'GET',
            url: '$show_booking_item?id='+$(this).attr('data'),
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                $('.booking-item-modal-body').empty().html(data.booking_item_view);

                $('#booking_item_modal').modal('show');
                initJsAfterBookingViewRender();
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    $(document).on('click','.delete-booking-item',function()
    {
        childTriggerClick = 1;
        //alert('delete');
        $.ajax(
        {
            type: 'GET',
            url: '$delete_booking_item?id='+$(this).attr('data'),
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);

                checkAvailability();

                if(!$('body').hasClass('page-quick-sidebar-open'))
                {
                    $('body').addClass('page-quick-sidebar-open');    
                }
            
            },
            error: function()
            {
                alert('error');
            }
        });
    });

    // ********************************************************************************************************************************************************************************************** Js for Booking Item Modal ******************************************************* //

    //############ on change for radio buttons in modal ##############//

    $(document).on('change','input[type=radio]',function(){
        var sr=$(`[name='selected_user']:checked`).val();
        if(sr == 'user')
        {   
            $('#modal-user-dropdown').removeClass('hide');
            $('#modal-guest-form').addClass('hide');
        }
        else
        {   $('#modal-guest-form').removeClass('hide');
            $('#modal-user-dropdown').addClass('hide');
        }
    });

    $(document).on('click','.modal-view-details',function()
    {
        var date = $(this).attr('date');
        var rate_id = $(this).attr('data');

        if($(this).find('span').text() == 'View Details')
        {
            $('.modal-rate-details-fields-'+rate_id+'-'+date).css('display', 'block');
            $(this).find('span').text('Hide Details');

            $(this).removeClass('btn-default');
            $(this).addClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-down');
            $(this).find('i').addClass('fa-chevron-up');
        }
        else if($(this).find('span').text()  == 'Hide Details')
        {
            $('.modal-rate-details-fields-'+rate_id+'-'+date).css('display', 'none');
            $(this).find('span').text('View Details');

            $(this).addClass('btn-default');
            $(this).removeClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-up');
            $(this).find('i').addClass('fa-chevron-down');
        }
    });

    // ************** Update Booking Cart Item ********************//

    $(document).on('click','.update-item',function()
    {
        var custom_rate ='';
        var bookingItems = [];
        var modal_pricing_type = $('.modal-pricing-type').val();

        $('.modal-helper-field').each(function()
        {
            var arr = $(this).val().split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];
            var id = arr[3];

            if($('.modal-item-quantity-'+rate_id+'-'+date).val()>0)
            {
                if(modal_pricing_type == '1') // *********** Capacity Pricing ********** //
                {
                    no_of_adults = $('.modal-adults-dropdown-'+rate_id+'-'+date).val();
                    no_of_children = $('.modal-children-dropdown-'+rate_id+'-'+date).val();
                    $('.modal-capacity-pricing-'+rate_id+'-'+date).each(function()
                    {
                        if($(this).is(':checked'))
                        {
                            var person_no = $(this).attr('data');
                            var beds_combinations_id = '';
                            var upsell_ids =[];
                            var discount_ids =[];
                            var special_requests_ids = [];
                            var quantity = $('.modal-item-quantity-'+rate_id+'-'+date).val();

                            //getting price from input custom rate //
                            var sibs = $(this).siblings();

                            $.each( sibs, function() {
                                tr = $(this).val();
                                console.log(tr);
                                if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                                {
                                    console.log('jhere');
                                    custom_rate = tr.replace('.','');
                                }
                                
                            });

                            //########## get guests data ###########//

                            var guest_first_name = $('.modal-guest-first-name-'+rate_id+'-'+date).val();
                            var guest_last_name  = $('.modal-guest-last-name-'+rate_id+'-'+date).val();
                            var guest_country    = $('.modal-guest-country-'+rate_id+'-'+date).val();

                            // ******** get Upsell Items Id ********* //

                            var upsellItemsElement = $('.modal-upsell-items-'+rate_id+'-'+date);
                            $(upsellItemsElement).each(function()
                            {
                                if(this.checked)
                                {
                                    var upsell_id = $(this).attr('data');
                                    upsell_ids.push(upsell_id+':'+$('.modal-upsell-extra-bed-quantity-dropdown-'+upsell_id+'-'+date).val());
                                }
                            });

                            // ******** get Discount Id********* //

                            var discountCodesElement = $('.modal-discount-codes-'+rate_id+'-'+date);
                            $(discountCodesElement).each(function()
                            {
                                if(this.checked)
                                {
                                    var discount_id = $(this).attr('data');
                                    discount_ids.push(discount_id+':'+$('.modal-discount-code-extra-bed-quantity-dropdown-'+discount_id+'-'+rate_id+'-'+date).val());
                                }
                            });

                            // ******** get bed combination id********* //

                            var bedCombinationElement = $('.modal-beds-combinations-'+rate_id+'-'+date);
                            $(bedCombinationElement).each(function()
                            {
                                if(this.checked)
                                {
                                    beds_combinations_id = $(this).attr('data');
                                }
                            });

                            // ******** get Special Requests Id ********* //

                            var specialRequestsElement = $('.modal-special-requests-'+date);
                            $(specialRequestsElement).each(function()
                            {
                                if(this.checked)
                                {
                                    var special_request_id = $(this).attr('data');
                                    special_requests_ids.push(special_request_id);
                                }
                            });

                            if(discount_ids.length == 0)
                                discount_ids = '';

                            if(upsell_ids.length == 0)
                                upsell_ids = '';

                            var object = 
                            {
                                'id' : id,
                                'person_no' : person_no,
                                'no_of_adults': no_of_adults,
                                'no_of_children': no_of_children,
                                'custom_rate':custom_rate,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'user_id' : $('.modal-user-dropdown-'+rate_id+'-'+date).val(),
                                'confirmation_id' : $('.modal-booking-confirmation-'+rate_id+'-'+date).val(),
                                'booking_cancellation' : $('.modal-booking-cancellation-'+rate_id+'-'+date).val(),
                                'flag_id' : $('.modal-booking-flag-'+rate_id+'-'+date).val(),
                                'status_id' : $('.modal-booking-status-'+rate_id+'-'+date).val(),
                                'pricing_type' : modal_pricing_type,
                                'price_json' : $('.modal-price-json-'+rate_id+'-'+date).val(),
                                'total' : $('.modal-item-total-input-'+rate_id+'-'+date).val(),
                                'beds_combinations_id' : beds_combinations_id,
                                'travel_partner_id' : $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).val(),
                                'offer_id' : $('.modal-offers-dropdown-'+rate_id+'-'+date).val(),
                                'estimated_arrival_time_id' : $('.modal-estimated-arrival-time-dropdown-'+rate_id+'-'+date).val(),
                                'special_requests_ids' : special_requests_ids,
                                'guest_first_name' : guest_first_name,
                                'guest_last_name' : guest_last_name,
                                'guest_country' : guest_country,
                            };

                            bookingItems.push(object);
                        }
                    });
                }
                else // *********** First/Additional Pricing ********** //
                {
                    no_of_adults = $('.modal-adults-dropdown-'+rate_id+'-'+date).val();
                    no_of_children = $('.modal-children-dropdown-'+rate_id+'-'+date).val();

                    if(no_of_children > 0 || no_of_adults > 0)
                    {
                        var beds_combinations_id = '';
                        var upsell_ids =[];
                        var discount_ids =[];
                        var special_requests_ids = [];
                        var quantity = $('.modal-item-quantity-'+rate_id+'-'+date).val();

                        //########## get guests data ###########//

                        var guest_first_name = $('.modal-guest-first-name-'+rate_id+'-'+date).val();
                        var guest_last_name  = $('.modal-guest-last-name-'+rate_id+'-'+date).val();
                        var guest_country    = $('.modal-guest-country-'+rate_id+'-'+date).val();

                        // ******** get Upsell Items Id ********* //

                        var upsellItemsElement = $('.modal-upsell-items-'+rate_id+'-'+date);
                        $(upsellItemsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var upsell_id = $(this).attr('data');
                                upsell_ids.push(upsell_id+':'+$('.modal-upsell-extra-bed-quantity-dropdown-'+upsell_id+'-'+date).val());
                            }
                        });

                        // ******** get Discount Id********* //

                        var discountCodesElement = $('.modal-discount-codes-'+rate_id+'-'+date);
                        $(discountCodesElement).each(function()
                        {
                            if(this.checked)
                            {
                                var discount_id = $(this).attr('data');
                                discount_ids.push(discount_id+':'+$('.modal-discount-code-extra-bed-quantity-dropdown-'+discount_id+'-'+rate_id+'-'+date).val());
                            }
                        });

                        // ******** get bed combination id********* //

                        var bedCombinationElement = $('.modal-beds-combinations-'+rate_id+'-'+date);
                        $(bedCombinationElement).each(function()
                        {
                            if(this.checked)
                            {
                                beds_combinations_id = $(this).attr('data');
                            }
                        });

                        // ******** get Special Requests Id ********* //

                        var specialRequestsElement = $('.modal-special-requests-'+date);
                        $(specialRequestsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var special_request_id = $(this).attr('data');
                                special_requests_ids.push(special_request_id);
                            }
                        });

                        if(discount_ids.length == 0)
                            discount_ids = '';

                        if(upsell_ids.length == 0)
                            upsell_ids = '';

                        var object = 
                        {
                            'id' : id,
                            'no_of_adults': no_of_adults,
                            'no_of_children': no_of_children,
                            'discount_ids' : discount_ids,
                            'upsell_ids' : upsell_ids,
                            'quantity' : quantity,
                            'user_id' : $('.modal-user-dropdown-'+rate_id+'-'+date).val(),
                            'confirmation_id' : $('.modal-booking-confirmation-'+rate_id+'-'+date).val(),
                            'booking_cancellation' : $('.modal-booking-cancellation-'+rate_id+'-'+date).val(),
                            'flag_id' : $('.modal-booking-flag-'+rate_id+'-'+date).val(),
                            'status_id' : $('.modal-booking-status-'+rate_id+'-'+date).val(),
                            'pricing_type' : modal_pricing_type,
                            'price_json' : $('.modal-price-json-'+rate_id+'-'+date).val(),
                            'total' : $('.modal-item-total-input-'+rate_id+'-'+date).val(),
                            'beds_combinations_id' : beds_combinations_id,
                            'travel_partner_id' : $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).val(),
                            'offer_id' : $('.modal-offers-dropdown-'+rate_id+'-'+date).val(),
                            'estimated_arrival_time_id' : $('.modal-estimated-arrival-time-dropdown-'+rate_id+'-'+date).val(),
                            'special_requests_ids' : special_requests_ids,
                            'guest_first_name' : guest_first_name,
                            'guest_last_name' : guest_last_name,
                            'guest_country' : guest_country,
                        };

                        bookingItems.push(object);
                    }
                }
            }
        });

        //console.log(bookingItems);

        $.ajax(
        {
            type: 'POST',
            data: {bookingItems:bookingItems},
            url: '$update_booking_cart_item',
            async: false,
            success: function(data)
            {
                App.unblockUI(\".bookings-form\");

                $('#booking_item_modal').modal('hide');
                checkAvailability();

                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);

                $('body').addClass('page-quick-sidebar-open');
                childTriggerClick = 1;
            },
            error: function()
            {
                App.unblockUI(\".bookings-form\");
                alert('error');
            }
        });
    });
    
    $(document).on('change','.modal-upsell-items',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForModalElements(rate_id,date);
    });

    $(document).on('change','.modal-upsell-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');
        
        calculatePriceForModalElements(rate_id,date);
    });

    $(document).on('change','.modal-discount-code-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForModalElements(rate_id,date);
    });

    $(document).on('change','.modal-discount-codes',function()
    {
        var discount_id = $(this).attr('data');
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.modal-discount-code-extra-bed-quantity-dropdown-'+discount_id+'-'+rate_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.modal-discount-code-extra-bed-quantity-dropdown-'+discount_id+'-'+rate_id+'-'+date).css('display','none');
        }

        calculatePriceForModalElements(rate_id,date);
    });

    $(document).on('change','.modal-adults-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForModalElements(rate_id,date);
    });
    
    $(document).on('change','.modal-children-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForModalElements(rate_id,date);
    });

    $(document).on('change','.modal-item-quantity',function()
    {
        var rate_id = $(this).attr('data');
        var date = $(this).attr('date');

        calculatePriceForModalElements(rate_id,date);
    });
    
    $(document).on('change','.modal-capacity-pricing',function()
    {
        var checked = $(this).is(':checked');

        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        var capacityPricingElement = $('.modal-capacity-pricing-'+rate_id+'-'+date);
        $(capacityPricingElement).prop('checked', false);

        if(checked)
        {
            $(this).prop('checked',true);               
        }
        else
        {
            $('.update-item').attr('disabled','disabled');
            $(this).prop('checked', false);
        }

        calculatePriceForModalElements(rate_id,date);
    });

    // ************** Common function to calculate price for modal **************** //

    function calculatePriceForModalElements(rate_id='',date='')
    {
        var custom_rate = '';
        var no_of_adults = 0;
        var no_of_children = 0;

        var upsell_ids =[];
        var discount_ids =[];

        var travel_partner_id = '';
        var offer_id = '';

        var person_no = 0;
        var pricing_type = $('.modal-pricing-type').val();
        var item_id = $('.modal-pricing-type').attr('data');

        var quantity = $('.modal-item-quantity-'+rate_id+'-'+date).val();

        // ******** get Upsell Items Id ********* //

        var upsellItemsElement = $('.modal-upsell-items-'+rate_id+'-'+date);
        $(upsellItemsElement).each(function()
        {
            if(this.checked)
            {
                var upsell_id = $(this).attr('data');
                upsell_ids.push(upsell_id+':'+$('.modal-upsell-extra-bed-quantity-dropdown-'+upsell_id+'-'+date).val());
            }
        });

        // ******** get Discount Id********* //

        var discountCodesElement = $('.modal-discount-codes-'+rate_id+'-'+date);
        $(discountCodesElement).each(function()
        {
            if(this.checked)
            {
                var discount_id = $(this).attr('data');
                discount_ids.push(discount_id+':'+$('.modal-discount-code-extra-bed-quantity-dropdown-'+discount_id+'-'+rate_id+'-'+date).val());
            }
        });

        if(discount_ids.length == 0)
            discount_ids = '';

        if(upsell_ids.length == 0)
            upsell_ids = '';

        if($('.modal-travel-partner-dropdown-'+rate_id+'-'+date).val()!='')
            travel_partner_id = $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).val();

        if($('.modal-offers-dropdown-'+rate_id+'-'+date).val()!='')
            offer_id = $('.modal-offers-dropdown-'+rate_id+'-'+date).val();

        if(pricing_type == '1') // *********** Capacity Pricing ********** //
        {
            var capacity_flag = 0;

            var capacityPricingElement = $('.modal-capacity-pricing-'+rate_id+'-'+date);
            $(capacityPricingElement).each(function()
            {
                if(this.checked)
                {
                    capacity_flag = 1;
                    person_no = $(this).attr('data');

                    var sibs = $(this).siblings();

                    $.each( sibs, function() {
                        tr = $(this).val();
                        console.log(tr);
                        if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                        {
                            console.log('jhere');
                            custom_rate = tr.replace('.','');
                        }
                        
                    });
                }
            });

            if(capacity_flag)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id' : item_id,
                                'rate_id' : rate_id,
                                'custom_rate': custom_rate,
                                'person_no' : person_no,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights': 1,
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.modal-price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.update-item').attr('disabled',false);
                        $('.modal-item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.modal-item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.modal-item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });  
            }
            else
            {
                $('.update-item').attr('disabled','disabled');
                $('.modal-item-total-input-'+rate_id+'-'+date).val(0);
                $('.modal-item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.modal-item-total-'+rate_id+'-'+date).siblings('span').html('0');
            }
        }
        else // *********** First/Additional Pricing ********** //
        {
            no_of_adults = $('.modal-adults-dropdown-'+rate_id+'-'+date).val();
            no_of_children = $('.modal-children-dropdown-'+rate_id+'-'+date).val();

            if(no_of_children > 0 || no_of_adults > 0)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id' : item_id,
                                'rate_id' : rate_id,
                                'no_of_adults' : no_of_adults,
                                'no_of_children' : no_of_children,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights': 1
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.modal-price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.update-item').attr('disabled',false);
                        $('.modal-item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.modal-item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.modal-item-total-input-'+rate_id+'-'+date).val(' '+data.orgPrice);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
            else
            {
                $('.update-item').attr('disabled','disabled');
                $('.modal-item-total-input-'+rate_id+'-'+date).val(0);
                $('.modal-item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.modal-item-total-'+rate_id+'-'+date).siblings('span').html('0');
            }
        }
    }

    function initJsAfterBookingViewRender()
    {
        $('.modal-rate-details-dropdowns').select2({
            placeholder: \"Select an Option\",
        });

        // $('.housekeeping-status').select2({
        //     multiple: true,
        //     placeholder: 'Select a Status',
        //     //width:'100%',
        //     //data: z 
        // });

        $('.modal-offers-dropdown').select2({
            placeholder: \"Select an Offer\",
            allowClear: true
        });

        $('.modal-guest-country').select2({
            placeholder: \"Select a Country\",
            allowClear: true
        });

        $('.modal-user-dropdown').select2({
            placeholder: \"Select a User\",
            allowClear: true
        });

        $('.modal-travel-partner-dropdown').select2({
            placeholder: \"Select a Travel Partner\",
            allowClear: true
        });

        $('.modal-travel-partner-dropdown').on('select2:select', function (evt) 
        {
            var arr = $(this).attr('data').split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = $(this).attr('date');

            $('.modal-offers-dropdown-'+rate_id+'-'+date).attr('disabled','disabled');

            calculatePriceForModalElements(rate_id,date);
        });

        $('.modal-travel-partner-dropdown').on('select2:unselecting ', function (e) 
        {   
            var arr = $(this).attr('data').split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = $(this).attr('date');

            $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).val('');
            $('.modal-offers-dropdown-'+rate_id+'-'+date).attr('disabled',false);

            calculatePriceForModalElements(rate_id,date);
        });

        $('.modal-offers-dropdown').on('select2:select', function (evt) 
        {   
            var arr = $(this).attr('data').split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = $(this).attr('date');

            $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).attr('disabled','disabled');

            calculatePriceForModalElements(rate_id,date);
        });

        $('.modal-offers-dropdown').on('select2:unselecting ', function (e) 
        {   
            var arr = $(this).attr('data').split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = $(this).attr('date');

            $('.modal-offers-dropdown-'+rate_id+'-'+date).val('');
            $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).attr('disabled',false);

            calculatePriceForModalElements(rate_id,date);
        });

        $('.modal-total-tooltips').tooltip();
    }

    // ********************************************************************************************************************************************************* Js for Dynamically created Elements *********************************** //

    var total = 0;

    function setPriceToZero(rate_id,date)
    {
        $('.item-total-input-'+rate_id+'-'+date).val(0);
        $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
        $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
        $('.item-total-nights-price-'+rate_id+'-'+date).html('');
    }

    // ******************** Calculate Price for Adults ******************** //

    $(document).on('change','.adults-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var date = $(this).attr('date');

        // ******** Unselect other dropdowns on select current ********* //

        var no_of_adults = $(this).val();
        var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

        var temp_arr = '';
        var temp_rate_id = '';
        var temp_date = '';

        $('.children-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adults-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adult-dropdown-'+rate_id+'-'+date).val(no_of_adults);
        $('.child-dropdown-'+rate_id+'-'+date).val(no_of_children);

        if(no_of_adults==0 && no_of_children==0)
        {
            //$('.add-item-'+item_id).attr('disabled','disabled');
        }

        // ******** Show group setting modal if more than 1 item selected ********* //

        if($(this).val() > 0 )
        {
            if(jQuery.inArray(rate_id, selectedRatesAdults ) == -1)
            {
                selectedRatesAdults.push(rate_id);

                $('.adult-dropdown-'+rate_id).each(function()
                {
                    temp_date = $(this).attr('date');

                    if(date!=temp_date)
                    {
                        $(this).val(no_of_adults);
                        $(this).trigger('change');
                    }
                });
            }

            if(jQuery.inArray(item_id, selectedItems ) == -1)
            {
                selectedItems.push(item_id);
                showGroupSettingModal(selectedItems);
            }
            var exists = false;
            $('.capacity-pricing-'+item_id+'-'+date).each(function()
            {
                if($(this).is(':checked'))
                {
                    exists = true;   
                }
            });
            if(!exists)
            {
                $('.add-item-'+item_id).attr('disabled', true);
            }
        }
        else
        {
            if(jQuery.inArray(item_id, selectedItems ) != -1)
            {
                selectedItems.pop(item_id);
            }
        }

        // if(no_of_adults > 0 || no_of_children > 0)
        // {
        //     // alert('here');
        //     alert(item_id);

        //     if(pricing_type == 0)
        //     {
        //         $('.add-item-'+item_id).attr('disabled', false);
        //     }
            
        // }
        // else
        // {
        //     if(pricing_type == 0)
        //     {
        //         $('.add-item-'+item_id).attr('disabled', true);
        //     }
        // }
        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ******************** Calculate Price for Children ******************** //

    $(document).on('change','.children-dropdown',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var date = $(this).attr('date');

        // ******** Unselect other dropdowns on select current ********* //

        var no_of_children = $(this).val();
        var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();

        var temp_arr = '';
        var temp_rate_id = '';
        var temp_date = '';

        $('.children-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adults-dropdown-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('data').split(':');
            temp_rate_id = temp_arr[1];

            if(rate_id!=temp_rate_id)
            {
                $(this).val(0);
                setPriceToZero(temp_rate_id,date);
            }
        });

        $('.adult-dropdown-'+rate_id+'-'+date).val(no_of_adults);
        $('.child-dropdown-'+rate_id+'-'+date).val(no_of_children);

        if(no_of_adults==0 && no_of_children==0)
        {
            //$('.add-item-'+item_id).attr('disabled','disabled');
        }

        // ******** Show group setting modal if more than 1 item selected ********* //

        if($(this).val() > 0 )
        {
            if(jQuery.inArray(rate_id, selectedRatesChild ) == -1)
            {
                selectedRatesChild.push(rate_id);

                $('.child-dropdown-'+rate_id).each(function()
                {
                    temp_date = $(this).attr('date');

                    if(date!=temp_date)
                    {
                        $(this).val(no_of_children);
                        $(this).trigger('change');
                    }
                });
            }

            if(jQuery.inArray(item_id, selectedItems ) == -1)
            {
                selectedItems.push(item_id);
                showGroupSettingModal(selectedItems);
            }
            var exists = false;
            $('.capacity-pricing-'+item_id+'-'+date).each(function()
            {
                if($(this).is(':checked'))
                {
                    exists = true;   
                }
            });
            if(!exists)
            {
                $('.add-item-'+item_id).attr('disabled', true);
            }
        }
        else
        {
            if(jQuery.inArray(item_id, selectedItems ) != -1)
            {
                selectedItems.pop(item_id);
            }
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Capacity Pricing **************** //
    
    $(document).on('change','.capacity-pricing',function()
    {
        var checked = $(this).is(':checked');
        var person_no = $(this).attr('data');

        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var date = $(this).attr('date');

        var temp_arr = '';
        var temp_rate_id = '';

        $('.capacity-pricing-'+item_id+'-'+date).each(function()
        {
            temp_arr = $(this).attr('id').split(':');
            temp_rate_id = temp_arr[1];

            $(this).prop('checked', false);
            setPriceToZero(temp_rate_id,date);
        });

        if(checked)
        {
            $('.add-item-'+item_id).attr('disabled', false);
            // console.log('hereasdasd');
            $('.adults-dropdown-'+item_id).val(person_no);

            if(jQuery.inArray(rate_id, selectedRatesCapacityPricing) == -1)
            {
                selectedRatesCapacityPricing.push(rate_id);

                $('.table-'+item_id).each(function()
                {
                    var temp_date = $(this).attr('date');
                    if(date!=temp_date)
                    {
                        if($('.cpricing-'+rate_id+'-'+temp_date).length>0)
                        {
                            $('.cpricing-'+rate_id+'-'+temp_date).each(function()
                            {
                                if($(this).attr('data') == person_no)
                                {
                                    $(this).prop('checked',true);
                                    $(this).trigger('change');
                                }
                            });
                        }
                        else
                        {
                            $('.capacity-pricing-'+item_id+'-'+temp_date+'-0').each(function()
                            {
                                if($(this).attr('data') == person_no)
                                {
                                    $(this).prop('checked',true);
                                    $(this).trigger('change');
                                }
                            });
                        }
                    }
                });
            }

            if(jQuery.inArray(item_id, selectedItems ) == -1)
            {
                selectedItems.push(item_id);
                showGroupSettingModal(selectedItems);
            }

            $(this).prop('checked',true);             
        }
        else
        {
            $('.add-item-'+item_id).attr('disabled','disabled');
            $('.adults-dropdown-'+item_id).val(0);

            if(jQuery.inArray(item_id, selectedItems ) != -1)
            {
                selectedItems.pop(item_id);
            }

            $(this).prop('checked', false);
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Show item dropdowns on click view button ******************** //

    $(document).on('click','.view-details',function()
    {
        var item_id = $(this).attr('data');

        if($(this).find('span').text() == 'View Details')
        {
            $('.rate-details-fields-'+item_id).css('display', 'block');
            $(this).find('span').text('Hide Details');

            $(this).removeClass('btn-default');
            $(this).addClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-down');
            $(this).find('i').addClass('fa-chevron-up');
        }
        else if($(this).find('span').text()  == 'Hide Details')
        {
            $('.rate-details-fields-'+item_id).css('display', 'none');
            $(this).find('span').text('View Details');

            $(this).addClass('btn-default');
            $(this).removeClass('btn-warning');

            $(this).find('i').removeClass('fa-chevron-up');
            $(this).find('i').addClass('fa-chevron-down');
        }
    });

    // ************** Calculate Price for Upsell Items **************** //
    
    $(document).on('change','.upsell-items',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var upsell_id = $(this).attr('data');
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).css('display','none');
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Upsell Item Quantity ****************** //

    $(document).on('change','.upsell-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Discount Codes ****************** //

    $(document).on('change','.discount-codes',function()
    {
        var arr = $(this).attr('id').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];

        var discount_id = $(this).attr('data');
        var date = $(this).attr('date');

        if(this.checked)
        {
            $('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).css('display','inline');
        }
        else
        {
            $('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).css('display','none');
        }

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // ************** Calculate Price for Discount Code Quantity ****************** //

    $(document).on('change','.discount-code-extra-bed-quantity',function()
    {
        var arr = $(this).attr('data').split(':');
        var item_id = arr[0];
        var rate_id = arr[1];
        var date = $(this).attr('date');

        calculatePriceForChangedElements(rate_id,item_id,date);
    });

    // on change of custom rate price of capacity pricing //
    $(document).on('change','.custom_rate',function()
    {

        var arr = $(this).attr('id').split(':');
        var item_id = arr[1];
        var rate_id = arr[2]
        var date = $(this).attr('date');
        var custom_rate = $(this).val();

        var data = $(this).attr('data');

        var checkbox;
        checkbox = $(this).prev();
        for (i=0; i < data; i++) 
        { 
            checkbox = checkbox.prev();
        }

        // var checkbox_id = item_id+':'+rate_id+':'+data;
        console.log(checkbox.val());
        //alert(checkbox.val());

        if(checkbox.is(':checked'))
        {
            //alert('checked');
            // var arr = $(this).attr('id').split(':');
            // var item_id = arr[1];
            // var rate_id = arr[2]
            // var date = $(this).attr('date');
            // var custom_rate = $(this).val();
            
            calculatePriceForChangedElements(rate_id,item_id,date);
        }
        // else
        // {
        //     alert('not checked');
        // }
        // var arr = $(this).attr('id').split(':');
        // var item_id = arr[1];
        // var rate_id = arr[2]
        // var date = $(this).attr('date');
        // var custom_rate = $(this).val();
        
        // calculatePriceForChangedElements(rate_id,item_id,date);
       

    });
    // on change of custom rate price of capacity pricing for modal//
    $(document).on('change','.modal-custom_rate',function()
    {
        //alert('here');
        var arr = $(this).attr('id').split(':');
        var item_id = arr[1];
        var rate_id = arr[2]
        var date = $(this).attr('date');
        var custom_rate = $(this).val();

        var data = $(this).attr('data');

        var checkbox;
        checkbox = $(this).prev().prev();
        for (i=0; i < data; i++) 
        { 
            checkbox = checkbox.prev();
            
        }

        // var checkbox_id = item_id+':'+rate_id+':'+data;
       // console.log(checkbox.val());
        //alert(checkbox.val());
        console.log(checkbox.attr('id'));
        if(checkbox.is(':checked'))
        {
            
            calculatePriceForModalElements(rate_id,date);
        }
        // else
        // {
        //     alert('not checked');
        // }
        // var arr = $(this).attr('id').split(':');
        // var item_id = arr[1];
        // var rate_id = arr[2]
        // var date = $(this).attr('date');
        // var custom_rate = $(this).val();
        
        // calculatePriceForChangedElements(rate_id,item_id,date);
       

    });
    // ************** Common function to calculate price **************** //

    function calculatePriceForChangedElements(rate_id = '', item_id = '',date = '',custom_rate='')
    {
        
        var no_of_adults = 0;
        var no_of_children = 0;

        var upsell_ids =[];
        var discount_ids =[];

        var travel_partner_id = '';
        var offer_id = '';
        
        var person_no = 0;
        total_nights = 1;

        var quantity = $('.item-quantities-'+item_id+'-'+date).val();

        // ******** get Upsell Items Id ********* //

        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
        $(upsellItemsElement).each(function()
        {
            if(this.checked)
            {
                var upsell_id = $(this).attr('data');
                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
            }
        });

        // ******** get Discount Id********* //

        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
        $(discountCodesElement).each(function()
        {
            if(this.checked)
            {
                var discount_id = $(this).attr('data');
                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
            }
        });

        if(discount_ids.length == 0)
            discount_ids = '';

        if(upsell_ids.length == 0)
            upsell_ids = '';

        if($('#travel_partner_dropdown').val()!='')
            travel_partner_id = $('#travel_partner_dropdown').val();

        if($('#offers_dropdown').val()!='')
            offer_id = $('#offers_dropdown').val();

        if(pricing_type) // *********** Capacity Pricing ********** //
        {
            var capacity_flag = 0;
            

            var capacityPricingElement = $('.cpricing-'+rate_id+'-'+date);
            $(capacityPricingElement).each(function()
            {
                if(this.checked)
                {
                    capacity_flag = 1;
                    person_no = $(this).attr('data');
                    console.log('person no: '+person_no);
                    //console.log('person this: '+ $(this).siblings());
                    var sibs = $(this).siblings();

                    $.each( sibs, function() {
                        tr = $(this).val();
                        console.log(tr);
                        if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                        {
                            console.log('jhere');
                            custom_rate = tr.replace('.','');
                        }
                        
                    });
                    // custom_rate = $(this).next();

                    // var counter = parseInt(person_no);
                    // for (i=0; i < counter; i++) 
                    // { 
                    //     custom_rate = custom_rate.next();
                    // }
                    // console.log(custom_rate);
                    // // custom_rate = $('#custom-rate:'+$(this).attr('id')).val();
                    // console.log('custom_rate: '+custom_rate.val());
                   // console.log('custom_rate: '+custom_rate);
                }
            });

            if(capacity_flag)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'custom_rate': custom_rate,
                                'person_no' : person_no,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                                'dates': $('.item-date-'+item_id+'-'+date).val()
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                        // $('.item-total-nights-price-'+rate_id+'-'+date).html('<br>= '+data.item_total_nights_price);
                        $('.item-total-nights-price-'+rate_id+'-'+date).html(' '+data.item_total_nights_price);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });  
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+date).val(0);
                $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
                $('.item-total-nights-price-'+rate_id+'-'+date).html('');
            }
        }
        else // *********** First/Additional Pricing ********** //
        {
            no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
            no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

            if(no_of_children > 0 || no_of_adults > 0)
            {
                // *************** send ajax call to calculate item price *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'item_id': item_id,
                                'rate_id': rate_id,
                                'no_of_adults' : no_of_adults,
                                'no_of_children' : no_of_children,
                                'discount_ids' : discount_ids,
                                'upsell_ids' : upsell_ids,
                                'quantity' : quantity,
                                'travel_partner_id' : travel_partner_id,
                                'offer_id' : offer_id,
                                'total_nights' : total_nights,
                                'dates': $('.item-date-'+item_id+'-'+date).val()
                            };
                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$calculate_item_price',
                    async: false,
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        $('.price-json-'+rate_id+'-'+date).val(data);
                        data = jQuery.parseJSON( data );

                        var str = '';
                        str = str + 'Quantity = '+ data.quantity +' , ';
                        str = str + 'Total Nights = '+ data.total_nights +' , ';
                        str = str + 'Rate = '+ data.rate +' , ';
                        str = str + 'VAT % = '+ data.vat +' , ';
                        str = str + 'x = '+ data.x +' , ';
                        str = str + 'lodgingTax = '+ data.lodgingTax +' , ';
                        str = str + 'y = '+ data.y +' , ';
                        str = str + 'voucher_discount = '+ data.voucher_discount +' , ';
                        str = str + 'travel_partner_discount = '+ data.travel_partner_discount +' , ';
                        str = str + 'offer_discount = '+ data.offer_discount +' , ';
                        str = str + 'amount_of_discount = '+ data.amount_of_discount +' , ';
                        str = str + 'sub_total_1 = '+ data.sub_total_1 +' , ';
                        str = str + 'sub_total_2 = '+ data.sub_total_2 +' , ';
                        str = str + 'vat_amount = '+ data.vat_amount +' , ';
                        //str = str + 'orgPrice = '+ data.orgPrice +' , ';
                        str = str + 'Total Price = '+ data.icelandPrice;

                        $('.item-total-'+rate_id+'-'+date).attr('data-original-title',str);
                        $('.item-total-'+rate_id+'-'+date).siblings('span').html(data.icelandPrice);
                        $('.item-total-input-'+rate_id+'-'+date).val(data.orgPrice);
                        // $('.item-total-nights-price-'+rate_id+'-'+date).html('<br>= '+data.item_total_nights_price);
                        $('.item-total-nights-price-'+rate_id+'-'+date).html(' '+data.item_total_nights_price);
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
            else
            {
                $('.item-total-input-'+rate_id+'-'+date).val(0);
                $('.item-total-'+rate_id+'-'+date).attr('data-original-title','Item Price');
                $('.item-total-'+rate_id+'-'+date).siblings('span').html('0');
                $('.item-total-nights-price-'+rate_id+'-'+date).html('');
            }
        }
    }

    // ******************** Calculate Price for Travel Partner and Offer ******************** //

    function updateItemPrices()
    {
        $('.helper-field').each(function()
        {
            var arr = $(this).val().split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];

            calculatePriceForChangedElements(rate_id,item_id,date);
        });
    }

    // ******************** Check any quantity set to zero ******************* //

    function anyQuantityHasZeroValue(item_id)
    {
        var quantity_error_flag = 0;

        $('.item-quantity-'+item_id).each(function()
        {
            if($(this).val()==0)
            {
                quantity_error_flag = 1;
                return false;
            }
        });
        return quantity_error_flag;
    }

    // ******************** Check same room selected or not ****************** //

    function checkSameRoomSelected(item_id)
    {
        var error_flag = 0;
        var first_element = '';

        $('.item-name-'+item_id).each(function(index)
        {
            if(index == 0)
                first_element = $(this).val();

            if($(this).val()!=first_element)
            {
                error_flag = 1;
                return false;
            }
        });
        return error_flag;
    }


    // ******************** Check any rate which is not selected ******************* //

    function anyRateNotSelected(item_id)
    {
        var select_error_flag = 1;

        $('.item-quantity-'+item_id).each(function()
        {
            select_error_flag = 1;
            var date = $(this).attr('date');

            if(pricing_type)
            {
                $('.capacity-pricing-'+item_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        select_error_flag = 0;
                        return false;
                    }
                });

                if(select_error_flag)
                {
                    return false;
                }
            }
            else
            {
                $('.helper-field-'+item_id+'-'+date).each(function()
                {
                    var arr = $(this).val().split(':');
                    var rate_id = arr[1];

                    var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                    var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                    if(no_of_adults>0 || no_of_children>0)
                    {
                        select_error_flag = 0;
                        return false;
                    }
                });

                if(select_error_flag)
                {
                    return false;
                }
            }
        });
        return select_error_flag;
    }

    function getAllDates(item_id)
    {
        var allDates = [];

        $('.item-quantity-'+item_id).each(function()
        {
            var select_error_flag = 1;
            var date = $(this).attr('date');

            if(pricing_type)
            {
                $('.capacity-pricing-'+item_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        select_error_flag = 0;
                        allDates.push($('.item-date-'+item_id+'-'+date).val());
                        return false;
                    }
                });

                if(select_error_flag)
                {
                    allDates.push($('.item-date-'+item_id+'-'+date).val());
                }
            }
            else
            {
                $('.helper-field-'+item_id+'-'+date).each(function()
                {
                    var arr = $(this).val().split(':');
                    var rate_id = arr[1];

                    var no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                    var no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                    if(no_of_adults>0 || no_of_children>0)
                    {
                        select_error_flag = 0;
                        allDates.push($('.item-date-'+item_id+'-'+date).val());
                        return false;
                    }
                });

                if(select_error_flag)
                {
                    allDates.push($('.item-date-'+item_id+'-'+date).val());
                }
            }
        });
        return allDates;
    }

    // ************** Add item into Booking Cart ********************//

    $(document).on('click','.add-item',function()
    {
        App.blockUI({target:\".bookings-form\", animate: !0});

        var bookingItems = [];
        var item_id = $(this).attr('data');
        var unit_panel_id = item_id;
        var allDates = getAllDates(item_id);
        //console.log(allDates);
        //App.unblockUI(\".bookings-form\");
        var custom_rate = '';
        $('.helper-field-'+item_id).each(function()
        {
            var arr = $(this).val().split(':');
            var item_id = arr[0];
            var rate_id = arr[1];
            var date = arr[2];
            //alert( $('.housekeeping-status-'+item_id).val());
            console.log($('.housekeeping-status-'+item_id).val());
            if(pricing_type) // *********** Capacity Pricing ********** //
            {
                no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                $('.cpricing-'+rate_id+'-'+date).each(function()
                {
                    if($(this).is(':checked'))
                    {
                        var person_no = $(this).attr('data');
                        var beds_combinations_id;
                        var upsell_ids =[];
                        var discount_ids =[];
                        var special_requests_ids = [];
                        var quantity = $('.item-quantities-'+item_id+'-'+date).val();

                        //getting custom_rate // 
                        

                        var sibs = $(this).siblings();

                        $.each( sibs, function() {
                            tr = $(this).val();
                            console.log(tr);
                            if($(this).attr('id') == 'custom-rate:'+item_id+':'+rate_id+':'+person_no)
                            {
                                console.log('jhere');
                                custom_rate = tr.replace('.','');
                            }
                            
                        });

                        console.log('add to cart custom_rate: '+custom_rate);

                        //########## get guests data ###########//

                        var guest_first_name = $('.guest-first-name-'+item_id).val();
                        var guest_last_name  = $('.guest-last-name-'+item_id).val();
                        var guest_country    = $('.guest-country-'+item_id).val();

                        //######## getting voucher,reference no and comment ########//

                        var voucher_no = $('.voucher_no-'+item_id).val();
                        var reference_no = $('.reference_no-'+item_id).val();
                        var comment = $('.comment-'+item_id).val();
                        var notes = $('.notes-'+item_id).val();

                        // ******** get Upsell Items Id ********* //

                        var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
                        $(upsellItemsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var upsell_id = $(this).attr('data');
                                upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
                            }
                        });

                        // ******** get Discount Id********* //

                        var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
                        $(discountCodesElement).each(function()
                        {
                            if(this.checked)
                            {
                                var discount_id = $(this).attr('data');
                                discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
                            }
                        });

                        // ******** get bed combination id********* //

                        var bedCombinationElement = $('.beds-combinations-'+item_id);
                        $(bedCombinationElement).each(function()
                        {
                           // console.log('item-id : '+ item_id);
                            //console.log('class : '+ $(this).attr('class'));
                            // console.log('value' $(this).prop('checked'));
                            //if(this.checked)
                            //if($(this).is(':checked'))
                            if($(this).is(':checked'))
                            {
                                beds_combinations_id = $(this).attr('data');
                                console.log('selected bed : '+ beds_combinations_id);
                            }
                            else
                            {
                                console.log('not selected bed : '+$(this).attr('data'));
                            }
                        });

                        // ******** get Special Requests Id ********* //

                        var specialRequestsElement = $('.special-requests');
                        $(specialRequestsElement).each(function()
                        {
                            if(this.checked)
                            {
                                var special_request_id = $(this).attr('data');
                                special_requests_ids.push(special_request_id);
                            }
                        });

                        if(discount_ids.length == 0)
                            discount_ids = '';

                        if(upsell_ids.length == 0)
                            upsell_ids = '';

                        var object = 
                        {
                            'date' : $('.item-date-'+item_id+'-'+date).val(),
                            'provider_id': global_provider_id,
                            'arrival_date' : global_arrival_date,
                            'departure_date' : global_departure_date,
                            'remove_rate_conditions' : global_remove_rate_conditions,
                            'estimated_arrival_time_id' : global_estimated_arrival_time_id,
                            'item_id': item_id,
                            'rate_id': rate_id,
                            'custom_rate': custom_rate,
                            'person_no' : person_no,
                            'no_of_adults' : no_of_adults,
                            'no_of_children' : no_of_children,
                            'discount_ids' : discount_ids,
                            'upsell_ids' : upsell_ids,
                            'quantity' : quantity,
                            'travel_partner_id' : $('#travel_partner_dropdown').val(),
                            'voucher_no':voucher_no,
                            'reference_no':reference_no,
                            'comment': comment,
                            'notes' : notes,
                            'offer_id' : $('#offers_dropdown').val(),
                            'user_id' : $('.user-'+item_id).val(),
                            'confirmation_id' : $('.booking-confirmation-'+item_id).val(),
                            'booking_cancellation' : $('.booking-cancellation-'+item_id).val(),
                            'flag_id' : $('.booking-flag-'+item_id).val(),
                            'status_id' : $('.booking-status-'+item_id).val(),
                            'housekeeping_status_id' : $('.housekeeping-status-'+item_id).val(),
                            'total_nights' : 1,
                            'pricing_type' : pricing_type,
                            'price_json' : $('.price-json-'+rate_id+'-'+date).val(),
                            'total' : $('.item-total-input-'+rate_id+'-'+date).val(),
                            'beds_combinations_id' : beds_combinations_id,
                            'special_requests_ids' : special_requests_ids,
                            'guest_first_name' : guest_first_name,
                            'guest_last_name' : guest_last_name,
                            'guest_country' : guest_country,
                            'item_name_id': $('.item-name-'+item_id).val(),
                        };

                        bookingItems.push(object);
                    }
                });
            }
            else // *********** First/Additional Pricing ********** //
            {
                no_of_adults = $('.adult-dropdown-'+rate_id+'-'+date).val();
                no_of_children = $('.child-dropdown-'+rate_id+'-'+date).val();

                if(no_of_children > 0 || no_of_adults > 0)
                {
                    var beds_combinations_id = '';
                    var upsell_ids =[];
                    var discount_ids =[];
                    var special_requests_ids = [];
                    var quantity = $('.item-quantities-'+item_id+'-'+date).val();

                    //########## get guests data ###########//

                    var guest_first_name = $('.guest-first-name-'+item_id).val();
                    var guest_last_name  = $('.guest-last-name-'+item_id).val();
                    var guest_country    = $('.guest-country-'+item_id).val();

                    var voucher_no = $('.voucher_no-'+item_id).val();
                    var reference_no = $('.reference_no-'+item_id).val();
                    var comment = $('.comment-'+item_id).val();
                    var notes = $('.notes-'+item_id).val();

                    // ******** get Upsell Items Id ********* //

                    var upsellItemsElement = $('.upsell-items-'+rate_id+'-'+date);
                    $(upsellItemsElement).each(function()
                    {
                        if(this.checked)
                        {
                            var upsell_id = $(this).attr('data');
                            upsell_ids.push(upsell_id+':'+$('.upsell-extra-bed-quantity-dropdown-'+rate_id+'-'+upsell_id+'-'+date).val());
                        }
                    });

                    // ******** get Discount Id********* //

                    var discountCodesElement = $('.discount-codes-'+rate_id+'-'+date);
                    $(discountCodesElement).each(function()
                    {
                        if(this.checked)
                        {
                            var discount_id = $(this).attr('data');
                            discount_ids.push(discount_id+':'+$('.discount-code-extra-bed-quantity-'+rate_id+'-'+discount_id+'-'+date).val());
                        }
                    });

                    // ******** get bed combination id********* //

                    var bedCombinationElement = $('.beds-combinations-'+item_id);
                    $(bedCombinationElement).each(function()
                    {
                        if(this.checked)
                        {
                            beds_combinations_id = $(this).attr('data');
                        }
                    });

                    // ******** get Special Requests Id ********* //

                    var specialRequestsElement = $('.special-requests');
                    $(specialRequestsElement).each(function()
                    {
                        if(this.checked)
                        {
                            var special_request_id = $(this).attr('data');
                            special_requests_ids.push(special_request_id);
                        }
                    });

                    if(discount_ids.length == 0)
                        discount_ids = '';

                    if(upsell_ids.length == 0)
                        upsell_ids = '';

                    var object = 
                    {
                        'date' : $('.item-date-'+item_id+'-'+date).val(),
                        'provider_id': global_provider_id,
                        'arrival_date' : global_arrival_date,
                        'departure_date' : global_departure_date,
                        'remove_rate_conditions' : global_remove_rate_conditions,
                        'estimated_arrival_time_id' : global_estimated_arrival_time_id,
                        'item_id': item_id,
                        'rate_id': rate_id,
                        'no_of_adults' : no_of_adults,
                        'no_of_children' : no_of_children,
                        'discount_ids' : discount_ids,
                        'upsell_ids' : upsell_ids,
                        'quantity' : quantity,
                        'travel_partner_id' : $('#travel_partner_dropdown').val(),
                        'voucher_no':voucher_no,
                        'reference_no':reference_no,
                        'comment': comment,
                        'notes' : notes,
                        'offer_id' : $('#offers_dropdown').val(),
                        'user_id' : $('.user-'+item_id).val(),
                        'confirmation_id' : $('.booking-confirmation-'+item_id).val(),
                        'booking_cancellation' : $('.booking-cancellation-'+item_id).val(),
                        'flag_id' : $('.booking-flag-'+item_id).val(),
                        'status_id' : $('.booking-status-'+item_id).val(),
                        'housekeeping_status_id' : $('.housekeeping-status-'+item_id).val(),
                        'total_nights' : total_nights,
                        'pricing_type' : pricing_type,
                        'price_json' : $('.price-json-'+rate_id+'-'+date).val(),
                        'total' : $('.item-total-input-'+rate_id+'-'+date).val(),
                        'beds_combinations_id' : beds_combinations_id,
                        'special_requests_ids' : special_requests_ids,
                        'guest_first_name' : guest_first_name,
                        'guest_last_name' : guest_last_name,
                        'guest_country' : guest_country,
                        'item_name_id': $('.item-name-'+item_id).val(),
                    };

                    bookingItems.push(object);
                }
            }
        });
        //alert(custom_rate);
        $.ajax(
        {
            type: 'POST',
            data: {bookingItems:bookingItems, allDates:allDates},
            url: '$add_to_booking_cart',
            async: false,
            success: function(data)
            {
                App.unblockUI(\".bookings-form\");
                checkAvailability(unit_panel_id);

                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);

                if(data.error==1)
                {
                    $('.alert-danger').find('ul').empty().html('There was an error saving this booking, looks like someone already booked 1 or more rooms you selected');
                    $('.alert-danger').css('display','block');
                    window.location.href='#booking_form';
                }
                
                $('body').addClass('page-quick-sidebar-open');
                childTriggerClick = 1;
            },
            error: function()
            {
                App.unblockUI(\".bookings-form\");
                alert('error');
            }
        });
    });

    // ******************** Calculate Price for Quantity ******************** //

    $(document).on('change','.item-quantity',function()
    {
        var item_id = $(this).attr('data');
        var rate_id = '';
        var date = $(this).attr('date');

        if($(this).val()==0)
            $('.add-item-'+item_id).attr('disabled','disabled');

        if(pricing_type)
        {
            $('.capacity-pricing-'+item_id+'-'+date).each(function()
            {
                if($(this).is(':checked'))
                {
                    var arr = $(this).attr('id').split(':');
                    rate_id = arr[1];
                    calculatePriceForChangedElements(rate_id,item_id,date);
                }
            });
        }
        else
        {
            $('.adults-dropdown-'+item_id+'-'+date).each(function()
            {
                if($(this).val()>0)
                {
                    var arr = $(this).attr('data').split(':');
                    rate_id = arr[1];
                    calculatePriceForChangedElements(rate_id,item_id,date);
                    return false;
                }
            });

            if(rate_id == '')
            {
                $('.children-dropdown-'+item_id+'-'+date).each(function()
                {
                    if($(this).val()>0)
                    {
                        var arr = $(this).attr('data').split(':');
                        rate_id = arr[1];
                        calculatePriceForChangedElements(rate_id,item_id,date);
                        return false;
                    }
                });
            }
        }
    });

    $('#travel_partner_dropdown').on('select2:select', function (evt) 
    {
        $('.vou_ref_div').removeClass('hide');
        $('#offers_dropdown').attr('disabled','disabled');
        updateItemPrices();

        $.ajax(
        {    
            url: '$update_booking_cart_items_prices?travel_partner_id='+$(this).val(),
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);

                if(data.empty==0)
                    $('body').addClass('page-quick-sidebar-open');
            },
        });
    });

    $('#travel_partner_dropdown').on('select2:unselecting ', function (e) 
    {
        $('.vou_ref_div').addClass('hide');
        $('#travel_partner_dropdown').val('');
        $('#offers_dropdown').attr('disabled',false);
        updateItemPrices();

        $.ajax(
        {    
            url: '$update_booking_cart_items_prices?travel_partner_id='+$(this).val(),
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);
                
                if(data.empty==0)
                    $('body').addClass('page-quick-sidebar-open');
            },
        });
    });

    $('#offers_dropdown').on('select2:select', function (evt) 
    {
        $('#travel_partner_dropdown').attr('disabled','disabled');
        updateItemPrices();

        $.ajax(
        {    
            url: '$update_booking_cart_items_prices?offers_id='+$(this).val(),
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);
                
                if(data.empty==0)
                    $('body').addClass('page-quick-sidebar-open');
            },
        });
    });

    $('#offers_dropdown').on('select2:unselecting ', function (e) 
    {
        $('#offers_dropdown').val('');
        $('#travel_partner_dropdown').attr('disabled',false);
        updateItemPrices();

        $.ajax(
        {    
            url: '$update_booking_cart_items_prices?offers_id='+$(this).val(),
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                $('.bookings-items').empty().html(data.bookings_items);
                
                if(data.empty==0)
                    $('body').addClass('page-quick-sidebar-open');
            },
        });
    });


    function initRatesJs()
    {
        //selectedItems = [];
        modal_flag = 0;

        $('.rate-details-dropdowns').select2({
            placeholder: \"Select an Option\",
        });

        // $('.housekeeping-status').select2({
        //     placeholder: 'Select Status',
        //     width:'100%',
        //     //data: z 
        // });
        $('.select2-search__field').css('width','120px !important');
        $('.user-dropdown').select2({
            placeholder: \"Select a User\",
            allowClear: true
        });
        $('.guest-country').select2({
            placeholder: \"Select a Country\",
            allowClear: true
        });

        $('.total-tooltips').tooltip();
    }

    initRatesJs();

    $('#check_availability').click(function()
    {
        checkAvailability();
    });

    function checkAvailability(unit_panel_id = '')
    {
        selectedItems = [];
        selectedRatesAdults = [];
        selectedRatesChild = [];
        selectedRatesCapacityPricing = [];

        $('.alert-danger').css('display','none');
        $('#selected_arrival_date').val($('#arrival_date').val());
        $('#selected_departure_date').val($('#departure_date').val());

        selected_arrival_date = $('#arrival_date').val();
        selected_departure_date = $('#departure_date').val();

        global_arrival_date = $('#arrival_date').val();
        global_departure_date = $('#departure_date').val();

        global_provider_id = $('#provider_dropdown').val();

        if($('#bookingsitemscart-rate_conditions').prop('checked') == true)
            global_remove_rate_conditions = 1;

        global_estimated_arrival_time_id= $('#estimated_arrival_time_dropdown').val();
        
        // *************** send ajax call to show rates Dynamically *************** //

        var remove_rate_conditions = 0;

        if($('#bookingsitemscart-rate_conditions').prop('checked') == true)
            remove_rate_conditions = 1;

        App.blockUI({target:\".bookings-form\", animate: !0});

        if($('#arrival_date').val()=='' || $('#departure_date').val()=='')
        {
            alert('Arrival Date/Departure Date cannot be blank.');
            App.unblockUI(\".bookings-form\");
        }
        else
        {
            var object = {
                            'provider_id' : $('#provider_dropdown').val(),
                            'arrival_date' : $('#arrival_date').val(),
                            'departure_date' : $('#departure_date').val(),
                            'remove_rate_conditions' : remove_rate_conditions,
                        };

            $.ajax(
            {
                type: 'POST',
                data: object,
                url: '$list_items_rates_url',
                success: function(data)
                {
                    App.unblockUI(\".bookings-form\");
                    data = jQuery.parseJSON( data );

                    total_nights = data.total_nights;

                    if(unit_panel_id != '')
                    {   
                        $('#hidden_available_rates').empty().html(data.items_rates);
                        $('.available-rates').find('#unit_panel_'+unit_panel_id).empty().html($('#hidden_available_rates').find('#unit_panel_'+unit_panel_id).html());

                        $('#hidden_available_rates').empty().html('empty');
                        $('.add-item-'+unit_panel_id).attr('disabled','disabled');
                    }
                    else if(data.date_exception_flag == 1)
                    {
                        $('.available-rates').empty().html(`<div class='capacity-error alert-danger alert fade in'>This destination is closed on one or more of the dates selected, please change your dates and try again.</div>`);
                        $('.add-item').attr('disabled','disabled');
                        //alert('This destination is closed on one or more of the dates selected, please change your dates and try again.');
                    }
                    else 
                    {   
                        $('.available-rates').empty().html(data.items_rates);
                        $('.add-item').attr('disabled','disabled');
                        $('.remove-selected').removeAttr('selected');
                        $('.remove-selected').removeClass('remove-selected');
                    }

                    $('#panel_handler').css('display','block');
                    initRatesJs();
                },
                error: function()
                {
                    App.unblockUI(\".bookings-form\");
                    alert('error');
                }
            });
        }
    }

    $('#country_dropdown').change(function()
    {
        $.ajax(
        {    
            url: '$list_states_url?country_id='+$(this).val(),
            success: function(data)
            {
                $('#state_dropdown').empty().html(data).trigger('change');
            },
        });
    });

    $('#state_dropdown').change(function()
    {
        $.ajax(
        {    
            url: '$list_cities_url?state_id='+$(this).val(),
            success: function(data)
            {
                $('#city_dropdown').empty().html(data);
            },
        });
    });

    $('.bookings_tabs li').click(function()
    {   
        var href = $(this).find('a:first').attr('href');
        $('#tab_href').val(href);
    });

    // *********** Create New User Dynamically ************* //

    $('#add_user').click(function()
    {
        $('#add-user-modal').modal('show');

        $('.user-modal-body').find('input').each(function()
        {
            $(this).val('');
        });

        $('.modal-help-block').hide();
        $('.email-help-block').hide();
        $('.modal-errors-block').hide();

        $('#country_dropdown').val(100).trigger('change');
    });

    $('.create-btn').click(function()
    {
        var flag = 0;

        $('.user-modal-body').find('input').each(function()
        {
            if($(this).attr('id')!='lname' && $(this).val()=='')
            {
                $(this).siblings('.modal-help-block').show();
                flag = 1;
            }
        });

        if(!flag)
        {
            $('.modal-help-block').hide();

            if(!isValidEmailAddress($('#email').val()))
            {
                $('.email-help-block').show();
            }
            else
            {
                $('.email-help-block').hide();

                // *************** send ajax call to add User Dynamically *************** //

                App.blockUI({target:\".bookings-form\", animate: !0});
                
                var object = {
                                'username' : $('#username').val(),
                                'email' : $('#email').val(),
                                'fname' : $('#fname').val(),
                                'lname' : $('#lname').val(),
                                'country_id' : $('#country_dropdown').val(),
                                'state_id' : $('#state_dropdown').val(),
                                'city_id' : $('#city_dropdown').val(),
                            };

                $.ajax(
                {
                    type: 'POST',
                    data: object,
                    url: '$add_user_url',
                    success: function(data)
                    {
                        App.unblockUI(\".bookings-form\");
                        data = jQuery.parseJSON( data );

                        if(data.error == '1')
                        {
                            $('.modal-errors-block').empty().html(data.error_str);
                            $('.modal-errors-block').show();
                        }
                        else
                        {
                            $('#add-user-modal').modal('hide');
                            $('#user_dropdown').empty().html(data.users);
                            $('.user-dropdown').empty().html(data.users);
                            $('#user_dropdown').val(data.user_id).trigger('change');
                        }
                    },
                    error: function()
                    {
                        App.unblockUI(\".bookings-form\");
                        alert('error');
                    }
                });
            }
        }
    });

    $('.user-modal-field').change(function()
    {
        if($(this).attr('id')!='lname' && $(this).val()=='')
        {
            $(this).siblings('.modal-help-block').show();
        }
        else
        {
            $(this).siblings('.modal-help-block').hide();

            if($(this).attr('id')=='email')
            {
                if(!isValidEmailAddress($(this).val()))
                {
                    $('.email-help-block').show();
                }
                else
                {
                    $('.email-help-block').hide();
                }
            }
        }
    });

    function isValidEmailAddress(emailAddress) 
    {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(emailAddress);
    }

    $('input[name = \"BookingsItems[arrival_date]\"]').on('change',function()
    {
        var new_date = moment($(this).val(),'D/M/YYYY').add('days', 1);
        $('#departure_date').datepicker('setDate',new_date.format('DD/MM/YYYY'));
    });

    $('#today').click(function()
    {
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

        $('#arrival_date').datepicker('setDate',date);
    });

    $('#bookingsitemscart-rate_conditions').change(function()
    {
        if(this.checked)
        {
            if(confirm('Are you sure to Remove Rate Restrictions ?'))
            {
                checkAvailability();
            }
            else
            {
                $(this).prop('checked', false);
            }
        }
        else
        {
            if(confirm('Are you sure to Apply Rate Restrictions ?'))
            {
                checkAvailability();
            }
            else
            {
                $(this).prop('checked', true);
            }
        }
    });

    $(document).on('click','#group_settings',function()
    {
        $('#group-modal').modal('show');
    });

    function showGroupSettingModal(selectedItems)
    {
        if(modal_flag == 0)
        {
            if(selectedItems.length == 2)
            {

                modal_flag = 1;
                $('#group-modal').modal('show');
            }
        }
    }

    $(window).on('scroll', function () {
        var scrollTop     = $(window).scrollTop(),
            elementOffset = $('.available-rates').offset().top,
            distance      = (elementOffset - scrollTop);

        if(distance < 30)
        {
            $('#grand_total_div').css('top', '50px');
        }
        else
        {
            $('#grand_total_div').css('top', distance+'px');
        }
    });

    $(document).on('click', function(evt) 
    {
        if($(evt.target).is('.page-quick-sidebar-alerts-list') || $(evt.target).is('.quick-sidebar-title')) 
        {
            childTriggerClick = 0;
        }
        else
        {
            if(childTriggerClick == 0)
            {
                if($('body').hasClass('page-quick-sidebar-open'))
                {
                  $('body').removeClass('page-quick-sidebar-open');
                }
            }

            childTriggerClick = 0;
        }

        //console.log('after'+childTriggerClick);
    });

    $(document).on('click','#cancel_btn',function()
    {
        if(confirm('Are you sure you want to delete all items from this booking session ?'))
        {
            $.ajax(
            {
                type: 'GET',
                url: '$remove_cart_items_url',
                success: function(data)
                {
                    data = jQuery.parseJSON( data );
                    $('.bookings-items').empty().html(data.bookings_items);
                    checkAvailability();
                },
                error: function()
                {
                    alert('error');
                }
            });
        }
    });

    $(document).on('click','#panel_handler',function()
    {
        var flag = 0;

        if($(this).text()== 'Expand All Items')
        {
            $(this).text('Collapse All Items');
            flag = 1;
        }
        else
        {
            $(this).text('Expand All Items');
        }

        $('.accordion-toggle').each(function()
        {
            var item_id = $(this).attr('data');

            if(flag)
            {
                $(this).removeClass('collapsed');
                $(this).attr('aria-expanded','true');
                $('#collapse_'+item_id).addClass('in');
                $('#collapse_'+item_id).attr('aria-expanded','true');
                $('#collapse_'+item_id).css('height','auto');
            }
            else
            {
                $('#collapse_'+item_id).removeClass('in');
                $('#collapse_'+item_id).attr('aria-expanded','false');
                $(this).attr('aria-expanded','false');
                $(this).addClass('collapsed');
            }
        });
    });

});",View::POS_END);
?>

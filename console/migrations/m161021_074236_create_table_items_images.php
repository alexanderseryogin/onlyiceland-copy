<?php

use yii\db\Migration;

class m161021_074236_create_table_items_images extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `bookable_items_images`( `id` INT(11) NOT NULL AUTO_INCREMENT, `bookable_items_id` INT(11), `image` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_bookable_items_images_item_id` FOREIGN KEY (`bookable_items_id`) REFERENCES `bookable_items`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161021_074236_create_table_items_images cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
	use common\models\DiscountCode;
	
	if(!empty($provider_id))
	{
		$discount_codes = DiscountCode::find()->where(['provider_id' => $provider_id])->all();

	    if(!empty($discount_codes))
		{
			foreach ($discount_codes as $key => $value) 
		    {
		    	$checked = 0;

		    	if(isset($checked_discount_codes) && !empty($checked_discount_codes) && in_array($value['id'], $checked_discount_codes) )
		    	{
		    		$checked = 'checked';
		    	}

		        echo '<label><input name="Rates[discount_codes][]" value="'.$value['id'].'" '.$checked.' style="margin-left:10px;" type="checkbox">  '.$value['name'].'</label>';
		    }
		}
		else
		{
			echo '<span class="label label-danger"> Whoops! </span>
	                        <span>&nbsp;  No Discount Code found </span>';
		}
	}
	else
	{
		echo '<span class="label label-danger"> Whoops! </span>
                        <span>&nbsp;  No Discount Code found </span>';
	}
		
?>
<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\models\PlacesOfInterest;

class POIImageUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var \common\models\UserProfile
     */
    private $_places_of_interest;

    public function __construct($id, $config = [])
    {
        if (!isset($id) || empty($id)) {
            throw new InvalidParamException('Places of interest Id cannot be blank.');
        }
        $this->_places_of_interest = PlacesOfInterest::findPOI($id);
        if (!$this->_places_of_interest) {
            throw new InvalidParamException('Invalid Places of interest ID.');
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload()
    {
        $places_of_interest = $this->_places_of_interest;
        if ($this->validate()) {
            $file_name = $this->_places_of_interest->id . '-' . $this->_places_of_interest->name . '.' . $this->imageFile->extension;
            $file_path = realpath(dirname(__FILE__).'../../../').'/uploads/places-of-interest/';
            if (!is_dir($file_path)) {
                mkdir($file_path);
            }
            $newfile = $file_path . $file_name;
            $oldfile = $file_path . $this->_places_of_interest->title_picture;

            if (is_file($oldfile))
            {
                unlink($oldfile);
            }
            
            $this->imageFile->saveAs($newfile);
            $places_of_interest->title_picture = $file_name;
            return $places_of_interest->save(false);
        } else {
            return false;
        }
    }
}
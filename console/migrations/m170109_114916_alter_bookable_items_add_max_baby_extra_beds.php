<?php

use yii\db\Migration;

class m170109_114916_alter_bookable_items_add_max_baby_extra_beds extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `max_extra_baby_beds` INT(11) NULL AFTER `max_children`; ");
    }

    public function down()
    {
        echo "m170109_114916_alter_bookable_items_add_max_baby_extra_beds cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

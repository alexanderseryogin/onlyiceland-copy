<?php
    set_time_limit(0);
return [
    'name' => 'Only Iceland',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'formatter' => [
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'numberFormatterOptions' => [
                NumberFormatter::MIN_FRACTION_DIGITS => 0,
                NumberFormatter::MAX_FRACTION_DIGITS => 3,
            ],
            'currencyCode' => 'ISK',
       ],
    ],
    'modules' => [
       'gridview' =>  [
            'class' => '\kartik\grid\Module',
        ],
        'audit' => 'bedezign\yii2\audit\Audit',
    ],
    'aliases' => [
        '@metronic' => '@themes/metronic',
        '@realspaces' => '@themes/realspaces',
    ],
];

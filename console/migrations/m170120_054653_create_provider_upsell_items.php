<?php

use yii\db\Migration;

class m170120_054653_create_provider_upsell_items extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `provider_upsell_items`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `upsell_id` INT(11), `price` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_provider_upsell_items_providre_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_provider_upsell_items_upsell_id` FOREIGN KEY (`upsell_id`) REFERENCES `upsell_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170120_054653_create_provider_upsell_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

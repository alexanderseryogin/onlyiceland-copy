<?php

use yii\db\Migration;

class m170104_053554_alter_bookable_items_add_sleeping_arrangement extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookable_items` ADD COLUMN `sleeping_arrangement_id` INT(11) UNSIGNED NULL AFTER `item_type_id`, ADD CONSTRAINT `FK_bookable_items_sleeping_arrangement_id` FOREIGN KEY (`sleeping_arrangement_id`) REFERENCES `sleeping_arrangements`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
                ALTER TABLE `bookable_items` DROP FOREIGN KEY `FK_bookable_items_item_type`; 
                ALTER TABLE `bookable_items` ADD CONSTRAINT `FK_bookable_items_item_type` FOREIGN KEY (`item_type_id`) REFERENCES `bookable_item_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
                ALTER TABLE `bookable_items` DROP FOREIGN KEY `FK_bookable_items_provider_id`; 
                ALTER TABLE `bookable_items` ADD CONSTRAINT `FK_bookable_items_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON UPDATE SET NULL ON DELETE CASCADE; 

        ");
    }

    public function down()
    {
        echo "m170104_053554_alter_bookable_items_add_sleeping_arrangement cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170720_081747_alter_table_bookable_items_add_field_bookable_item_notes extends Migration
{
    public function up()
    {
                $this->execute('ALTER TABLE `bookable_items`   
                          ADD COLUMN `bookable_item_notes` LONGTEXT NULL AFTER `item_type_id`;
                        ');
    }

    public function down()
    {
        echo "m170720_081747_alter_table_bookable_items_add_field_bookable_item_notes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace frontend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\admin\components\BaseController;
use frontend\models\InitialResetPasswordForm;

/**
 * Default controller for the `provider-admin` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Change password.
     */
    public function actionChangePassword()
    {
        try {
            $model = new InitialResetPasswordForm(Yii::$app->user->identity->id);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->redirect(['/provider-admin']);
        }

        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }
}

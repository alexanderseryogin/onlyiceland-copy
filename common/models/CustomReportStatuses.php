<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_report_statuses".
 *
 * @property integer $id
 * @property integer $status_id
 * @property integer $custom_report_id
 *
 * @property CustomReports $customReport
 */
class CustomReportStatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_report_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'custom_report_id'], 'integer'],
            [['custom_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomReports::className(), 'targetAttribute' => ['custom_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Status ID',
            'custom_report_id' => 'Custom Report ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReport()
    {
        return $this->hasOne(CustomReports::className(), ['id' => 'custom_report_id']);
    }
}

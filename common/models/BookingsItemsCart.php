<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\models\RatesOverride;
use common\models\BookingOverrideStatusDetails;
/**
 * This is the model class for table "bookings_items_cart".
 *
 * @property string $id
 * @property integer $booking_number
 * @property integer $provider_id
 * @property integer $item_id
 * @property string $item_name_id
 * @property string $arrival_date
 * @property string $departure_date
 * @property integer $flag_id
 * @property integer $status_id
 * @property integer $housekeeping_status_id
 * @property string $travel_partner_id
 * @property integer $offers_id
 * @property string $estimated_arrival_time_id
 * @property integer $rate_conditions
 * @property integer $pricing_type
 * @property string $comments
 * @property double $balance
 * @property string $voucher_no
 * @property string $reference_no
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property BookingDatesCart[] $bookingDatesCarts
 * @property User $createdBy
 * @property EstimatedArrivalTimes $estimatedArrivalTime
 * @property BookingTypes $flag
 * @property HousekeepingStatus $housekeepingStatus
 * @property BookableItems $item
 * @property BookableItemsNames $itemName
 * @property Offers $offers
 * @property Providers $provider
 * @property BookingStatuses $status
 * @property DestinationTravelPartners $travelPartner
 */
class BookingsItemsCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings_items_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'item_id', 'item_name_id', 'flag_id', 'status_id', 'housekeeping_status_id', 'travel_partner_id', 'offers_id', 'estimated_arrival_time_id', 'rate_conditions', 'pricing_type', 'created_by', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['arrival_date', 'departure_date'], 'safe'],
            [['comments','notes'], 'string'],
            [['balance'], 'number'],
            [['voucher_no', 'reference_no'], 'string', 'max' => 256],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['estimated_arrival_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => EstimatedArrivalTimes::className(), 'targetAttribute' => ['estimated_arrival_time_id' => 'id']],
            [['flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['flag_id' => 'id']],
            [['housekeeping_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HousekeepingStatus::className(), 'targetAttribute' => ['housekeeping_status_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['item_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItemsNames::className(), 'targetAttribute' => ['item_name_id' => 'id']],
            [['offers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offers::className(), 'targetAttribute' => ['offers_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']], 
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['travel_partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTravelPartners::className(), 'targetAttribute' => ['travel_partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            // 'booking_number' => 'Booking Number',
            'provider_id' => 'Provider ID',
            'item_id' => 'Item ID',
            'item_name_id' => 'Item Name ID',
            'arrival_date' => 'Arrival Date',
            'departure_date' => 'Departure Date',
            'flag_id' => 'Flag ID',
            'status_id' => 'Status ID',
            'housekeeping_status_id' => 'Housekeeping Status ID',
            'travel_partner_id' => 'Travel Partner ID',
            'offers_id' => 'Offers ID',
            'estimated_arrival_time_id' => 'Estimated Arrival Time ID',
            'rate_conditions' => 'Rate Conditions',
            'pricing_type' => 'Pricing Type',
            'comments' => 'Comments',
            'balance' => 'Balance',
            'voucher_no' => 'Voucher No',
            'reference_no' => 'Reference No',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getBookingDatesCarts()
    // {
    //     return $this->hasMany(BookingDatesCart::className(), ['booking_item_id' => 'id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getCreatedBy()
    // {
    //     return $this->hasOne(User::className(), ['id' => 'created_by']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getEstimatedArrivalTime()
    // {
    //     return $this->hasOne(EstimatedArrivalTimes::className(), ['id' => 'estimated_arrival_time_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getFlag()
    // {
    //     return $this->hasOne(BookingTypes::className(), ['id' => 'flag_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getHousekeepingStatus()
    // {
    //     return $this->hasOne(HousekeepingStatus::className(), ['id' => 'housekeeping_status_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getItem()
    // {
    //     return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getItemName()
    // {
    //     return $this->hasOne(BookableItemsNames::className(), ['id' => 'item_name_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getOffers()
    // {
    //     return $this->hasOne(Offers::className(), ['id' => 'offers_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getProvider()
    // {
    //     return $this->hasOne(Providers::className(), ['id' => 'provider_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getStatus()
    // {
    //     return $this->hasOne(BookingStatuses::className(), ['id' => 'status_id']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getTravelPartner()
    // {
    //     return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    // }

    public function getEstimatedArrivalTime()
    {
        return $this->hasOne(EstimatedArrivalTimes::className(), ['id' => 'estimated_arrival_time_id']);
    }

    public function getOffers()
    {
        return $this->hasOne(Offers::className(), ['id' => 'offers_id']);
    }
    
    public function getTravelPartner()
    {
        return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    }

    public function getStatus() 
    { 
       return $this->hasOne(BookingStatuses::className(), ['id' => 'status_id']); 
    } 

    public function getFlag()
    {
       return $this->hasOne(BookingTypes::className(), ['id' => 'flag_id']);
    }

    public function getItemName() 
    { 
        return $this->hasOne(BookableItemsNames::className(), ['id' => 'item_name_id']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    }

    public function getHouseKeepingStatus()
    {
        return $this->hasOne(HousekeepingStatus::className(), ['id' => 'housekeeping_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    public function getBookingDates()
    {
        return $this->hasMany(BookingDatesCart::className(), ['booking_item_id' => 'id'])
                    ->orderBy(['booking_dates_cart.date'=>SORT_ASC]);
    }

    public function getBookingDatesSortedAndNotNull()
    {
        return $this->hasMany(BookingDatesCart::className(), ['booking_item_id' => 'id'])
                    ->andWhere(['>','booking_dates_cart.rate_id', 0])
                    ->orderBy(['booking_dates_cart.date'=>SORT_ASC]);
    }

    // public function getBookingDatesSortedAndNotNull()
    // {
    //     return $this->hasMany(BookingDates::className(), ['booking_item_id' => 'id'])
    //                 ->where(['>','booking_dates.total', 0.00])
    //                 ->andWhere(['>','booking_dates.rate_id', 0])
    //                 ->orderBy(['booking_dates.date'=>SORT_ASC]);
    // }

   /**
    * @return \yii\db\ActiveQuery
    */
    public function getBookingItemGroups()
    {
       return $this->hasOne(BookingItemGroupCart::className(), ['booking_item_id' => 'id']);
    }

    public function FirstBookingDate()
    {
        return BookingDatesCart::findOne(['booking_item_id' => $this->id]);
    }

    public static function RateAllowedQualifies($arrival_date,$departure_date,$rateAllowedDays)
    {
        $rateAllowedDays = explode(';', $rateAllowedDays);
        $date = $arrival_date;

        $date1=date_create($departure_date);
        $date2=date_create($arrival_date);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a")+1;

        $bookingDays = [];

        for ($i=0; $i < $difference ; $i++) 
        {
            $day = date('N',strtotime($date));

            if(!in_array($day, $bookingDays))
            {
                $bookingDays[] = $day;
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }  

        foreach ($bookingDays as $key => $value) 
        {
            if(in_array($value-1, $rateAllowedDays))
            {
                return true;
            }
        }
        return false;
    }

    public static function itemsAvailability($difference,$arrival_date,$item_id)
    {
        $arr = [];
        $date = $arrival_date;
        $model = BookableItems::findOne(['id' => $item_id]);

        for ($i=0; $i < $difference ; $i++)
        {
            $booked_item_quantity = BookingDates::find()->joinWith('bookingItem')->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->sum('booking_dates.quantity');

            $cart_item_quantity = BookingsItems::find()->joinWith('bookingDates')->where(['created_by' => Yii::$app->user->identity->id, 'bookings_items.temp_flag' => 1, 'bookings_items.deleted_at'=>0, 'date' => $date, 'bookings_items.item_id' => $item_id])->sum('booking_dates.quantity');

            $arr[$date] = $model->item_quantity - $booked_item_quantity - $cart_item_quantity;

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $arr;  
    }

    public static function itemsAvailabilityExcludedCart($difference,$arrival_date,$item_id)
    {
        $arr = [];
        $date = $arrival_date;
        $model = BookableItems::findOne(['id' => $item_id]);

        for ($i=0; $i < $difference ; $i++)
        {
            $booked_item_quantity = BookingDates::find()->joinWith('bookingItem')->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->sum('booking_dates.quantity');

            $arr[$date] = $model->item_quantity - $booked_item_quantity;

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $arr;  
    }

    public static function itemsAvailabilityForCalendar($difference,$arrival_date,$item_id,$booked_item_id)
    {
        $arr = [];
        $date = $arrival_date;
        $model = BookableItems::findOne(['id' => $item_id]);

        for ($i=0; $i < $difference ; $i++)
        {
            $booked_item_quantity = BookingDates::find()
                                                ->joinWith('bookingItem')
                                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                                ->andWhere(['!=','booking_item_id',$booked_item_id])
                                                ->sum('booking_dates.quantity');

            $arr[$date] = $model->item_quantity - $booked_item_quantity;

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $arr;  
    }

    public static function totalAvailableRates($rates,$difference,$arrival_date,$departure_date)
    {
        $count = 0;
        foreach ($rates as $key => $rate) 
        {
            $min_max_stay_compatible = 0;

            if(!empty($rate->min_max_stay))
            {
                $arr = explode(';', $rate->min_max_stay);
                if($difference >= $arr[0] && $difference <= $arr[1])
                {
                    $min_max_stay_compatible = 1;
                }
            }

            $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
            $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

            if((($arrival_date >= $first_night && $arrival_date <= $last_night) ||
                ($departure_date >= $first_night && $departure_date <= $last_night) || 
                ($first_night >= $arrival_date && $first_night <= $departure_date) ||
                ($last_night >= $arrival_date && $last_night <= $departure_date)) &&
                $min_max_stay_compatible == 1)
            {
                $count++;    
            }
        }
        return $count;
    }

    public static function checkAvailableRates($item_id,$date,$difference,$remove_rate_condtions='')
    {
        $arr = [
            'availableRates' => '',
            'availableRatesIds' => '',
            'availableOverriddenRates' => '',
            'availableOverriddenRatesIds' => '',
            'overridden_rates_count' => ''
        ];

        $availableRates = [];
        $availableRatesIds = [];

        $availableOverriddenRates = [];
        $availableOverriddenRatesIds = [];

        $overridden_rates_count=0;

        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            for ($i=0; $i < $difference ; $i++)
            {
                foreach ($rates as $key => $rate) 
                {
                    $rate_arr  = [];
                    $min_max_stay_compatible = 0;

                    if(!empty($rate->min_max_stay))
                    {
                        $arr = explode(';', $rate->min_max_stay);
                        if($difference >= $arr[0] && $difference <= $arr[1])
                        {
                            $min_max_stay_compatible = 1;
                        }
                    }

                    $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
                    $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

                    if(($date >= $first_night && $date <= $last_night) && $min_max_stay_compatible == 1)
                    {
                        // showing overridden rate rates functionality //
                        // $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id]);
                        // if(empty($rate_override))
                        // {
                            $availableRatesIds[$date][] = $rate->id;
                            $availableRates[$date][]= $rate;
                        // }
                        // else
                        // {
                        //     $overridden_rates_count++;
                        //     $availableOverriddenRatesIds[$date][] = $rate->id;
                        //     $availableOverriddenRates[$date][]= $rate;
                        // }
                        
                        
                        
                    }
                }

                $date = strtotime("+1 day", strtotime($date));
                $date = date("Y-m-d", $date);
            }
            // echo "<pre>";
            // print_r($availableRatesIds);
            // exit();
            if(!empty($availableRatesIds))
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    $availableRatesIds[$key] = implode(',', $value);
                }
            }

            if(!empty($availableOverriddenRatesIds))
            {
                foreach ($availableOverriddenRatesIds as $key => $value) 
                {
                    $availableOverriddenRatesIds[$key] = implode(',', $value);
                }
            }
            // echo "<pre>";
            // print_r($availableRatesIds);
            // exit();
            $arr['availableRatesIds'] = $availableRatesIds;
            $arr['availableRates'] = $availableRates;

            $arr['availableOverriddenRatesIds'] = $availableOverriddenRatesIds;
            $arr['availableOverriddenRates'] = $availableOverriddenRates;

            // echo "<pre>";
            // print_r($availableRates);
            // exit();
            $new_overriddenratesarray = array();
            $new_overriddenratesarrayids = array();
            foreach ($availableOverriddenRates as $key => $value) 
            {
                if($rate->destination->pricing == 1)
                {
                    foreach ($value as  $rate) 
                    {
                        // $rate = $value[0];
                        $overridden_rate = RatesOverride::findOne(['date' => $key, 'rate_id' => $rate->id]);
                        if(empty($new_overriddenratesarray))
                        {
                            $overridden_rate_sum = (($overridden_rate->single == null)?0:$overridden_rate->single) + (($overridden_rate->double == null)?0:$overridden_rate->double) + (($overridden_rate->triple == null)?0:$overridden_rate->triple) +(($overridden_rate->quad == null)?0:$overridden_rate->quad);
                            $overridden_rate_sum = round($overridden_rate_sum);
                            $temp_array = array();
                            // array_push($temp_array, $overridden_rate);
                            // array_push($temp_array, $overridden_rate->date);
                            $temp_array[$overridden_rate->date][] = $overridden_rate;
                            
                            $new_overriddenratesarray[$overridden_rate_sum] = $temp_array;

                            $temp_array2 = array();
                            // array_push($temp_array, $overridden_rate);
                            // array_push($temp_array, $overridden_rate->date);
                            $temp_array2[$overridden_rate->date][] = $overridden_rate->rate_id;

                            $new_overriddenratesarrayids[$overridden_rate_sum] = $temp_array2;
                            // echo "<pre>";
                            // // print_r($new_overriddenratesarray);
                            // print_r($new_overriddenratesarrayids);
                            // exit();
                        }
                        else
                        {
                            $overridden_rate_sum = (($overridden_rate->single == null)?0:$overridden_rate->single) + (($overridden_rate->double == null)?0:$overridden_rate->double) + (($overridden_rate->triple == null)?0:$overridden_rate->triple) +(($overridden_rate->quad == null)?0:$overridden_rate->quad);
                            $temp_array = array();
                            // array_push($temp_array, $overridden_rate);
                            $overridden_rate_sum = round($overridden_rate_sum);
                            // array_push($temp_array, $overridden_rate->date);
                            $temp_array[$overridden_rate->date][] = $overridden_rate;

                            $temp_array2 = array();
                            $temp_array2[$overridden_rate->date][] = $overridden_rate->rate_id;

                            $check = true;
                            foreach ($new_overriddenratesarray as $key => $value) 
                            {
                                if($key == $overridden_rate_sum)
                                {
                                    $new_temp = [];
                                    $new_temp2 = [];
                                    // echo "<pre>";
                                    // print_r($new_overriddenratesarray[$overridden_rate_sum]);
                                    // exit();
                                    // foreach ($new_overriddenratesarray[$overridden_rate_sum] as $value) 
                                    // {
                                    //     $new_temp[$value->date] = $value;
                                    //     $new_temp2[$value->date] = $value->rate_id;
                                    // }

                                    foreach ($new_overriddenratesarray[$overridden_rate_sum] as $value) 
                                    {
                                        $rates = $value;
                                        // echo "<pre>";
                                        // print_r($rates);
                                        // exit();
                                        foreach ($rates as $new_rates) 
                                        {
                                            $new_temp[$new_rates->date][] = $new_rates;
                                            $new_temp2[$new_rates->date][] = $new_rates->rate_id;
                                        }
                                        
                                    }
                                    $new_temp[$overridden_rate->date][] = $overridden_rate;
                                    $new_overriddenratesarray[$overridden_rate_sum] = $new_temp;

                                    $new_temp2[$overridden_rate->date][] = $overridden_rate->rate_id;
                                    $new_overriddenratesarrayids[$overridden_rate_sum] = $new_temp2;
                                    // array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                                    $check = false;
                                    break;
                                }
                            }
                            if($check)
                            {
                                // if($tmp)
                                // array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                                $new_overriddenratesarray[$overridden_rate_sum] = $temp_array;
                                $new_overriddenratesarrayids[$overridden_rate_sum] = $temp_array2;

                            }
                            // if (array_key_exists($overridden_rate_sum,$new_overriddenratesarray))
                            // {
                            //    array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                            // }
                            // else
                            // {
                            //   array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                            // }

                        }
                    }
                        
                }
                else
                {
                    foreach ($value as  $rate) 
                    {
                        // $rate = $value[0];
                        $overridden_rate = RatesOverride::findOne(['date' => $key, 'rate_id' => $rate->id]);
                        if(empty($new_overriddenratesarray))
                        {
                            $overridden_rate_sum = (($overridden_rate->item_price_first_adult == null)?0:$overridden_rate->item_price_first_adult) + (($overridden_rate->item_price_additional_adult == null)?0:$overridden_rate->item_price_additional_adult) + (($overridden_rate->item_price_first_child == null)?0:$overridden_rate->item_price_first_child) +(($overridden_rate->item_price_additional_child == null)?0:$overridden_rate->item_price_additional_child);
                            $overridden_rate_sum = round($overridden_rate_sum);
                            $temp_array = array();
                            // array_push($temp_array, $overridden_rate);
                            // array_push($temp_array, $overridden_rate->date);
                            $temp_array[$overridden_rate->date][] = $overridden_rate;
                            
                            $new_overriddenratesarray[$overridden_rate_sum] = $temp_array;

                            $temp_array2 = array();
                            // array_push($temp_array, $overridden_rate);
                            // array_push($temp_array, $overridden_rate->date);
                            $temp_array2[$overridden_rate->date][] = $overridden_rate->rate_id;

                            $new_overriddenratesarrayids[$overridden_rate_sum] = $temp_array2;
                            // echo "<pre>";
                            // // print_r($new_overriddenratesarray);
                            // print_r($new_overriddenratesarrayids);
                            // exit();
                        }
                        else
                        {
                            $overridden_rate_sum = (($overridden_rate->item_price_first_adult == null)?0:$overridden_rate->item_price_first_adult) + (($overridden_rate->item_price_additional_adult == null)?0:$overridden_rate->item_price_additional_adult) + (($overridden_rate->item_price_first_child == null)?0:$overridden_rate->item_price_first_child) +(($overridden_rate->item_price_additional_child == null)?0:$overridden_rate->item_price_additional_child);
                            $temp_array = array();
                            // array_push($temp_array, $overridden_rate);
                            $overridden_rate_sum = round($overridden_rate_sum);
                            // array_push($temp_array, $overridden_rate->date);
                            $temp_array[$overridden_rate->date][] = $overridden_rate;

                            $temp_array2 = array();
                            $temp_array2[$overridden_rate->date][] = $overridden_rate->rate_id;

                            $check = true;
                            foreach ($new_overriddenratesarray as $key => $value) 
                            {
                                if($key == $overridden_rate_sum)
                                {
                                    $new_temp = [];
                                    $new_temp2 = [];
                                    // echo "<pre>";
                                    // print_r($new_overriddenratesarray[$overridden_rate_sum]);
                                    // exit();
                                    // foreach ($new_overriddenratesarray[$overridden_rate_sum] as $value) 
                                    // {
                                    //     $new_temp[$value->date] = $value;
                                    //     $new_temp2[$value->date] = $value->rate_id;
                                    // }

                                    foreach ($new_overriddenratesarray[$overridden_rate_sum] as $value) 
                                    {
                                        $rates = $value;
                                        // echo "<pre>";
                                        // print_r($rates);
                                        // exit();
                                        foreach ($rates as $new_rates) 
                                        {
                                            $new_temp[$new_rates->date][] = $new_rates;
                                            $new_temp2[$new_rates->date][] = $new_rates->rate_id;
                                        }
                                        
                                    }
                                    $new_temp[$overridden_rate->date][] = $overridden_rate;
                                    $new_overriddenratesarray[$overridden_rate_sum] = $new_temp;

                                    $new_temp2[$overridden_rate->date][] = $overridden_rate->rate_id;
                                    $new_overriddenratesarrayids[$overridden_rate_sum] = $new_temp2;
                                    // array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                                    $check = false;
                                    break;
                                }
                            }
                            if($check)
                            {
                                // if($tmp)
                                // array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                                $new_overriddenratesarray[$overridden_rate_sum] = $temp_array;
                                $new_overriddenratesarrayids[$overridden_rate_sum] = $temp_array2;

                            }
                            // if (array_key_exists($overridden_rate_sum,$new_overriddenratesarray))
                            // {
                            //    array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                            // }
                            // else
                            // {
                            //   array_push($new_overriddenratesarray[$overridden_rate_sum], $temp_array);
                            // }

                        }
                    }
                }
            }

            if(!empty($new_overriddenratesarrayids))
            {
                foreach ($new_overriddenratesarrayids as $key => $value) 
                {
                    foreach ($value as $key2 => $value2) 
                    {
                        $new_overriddenratesarrayids[$key][$key2] = implode(',', $value2);
                    }
                }
                
            }

            // echo "<pre>";
            // // print_r($new_overriddenratesarray);
            // print_r($new_overriddenratesarrayids);
            // exit();

            // $arr['availableOverriddenRatesMergeDates'] = $availableOverriddenRates;
            $arr['availableOverriddenRatesIds'] = $new_overriddenratesarrayids;
            $arr['availableOverriddenRates'] = $new_overriddenratesarray;
            $arr['overridden_rates_count'] = $overridden_rates_count;
            
            return $arr;
        }
        else
        {

            return $arr;
        }
    }

    public static function checkAvailableRatesForUpdate($item_id,$date,$difference,$savedDates)
    {
        $arr = [
            'availableRates' => '',
            'availableRatesIds' => '',
        ];
        $availableRates = [];
        $availableRatesIds = [];
        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            for ($i=0; $i < $difference ; $i++)
            {
                if(!in_array($date, $savedDates))
                {
                    foreach ($rates as $key => $rate) 
                    {
                        $rate_arr  = [];
                        $min_max_stay_compatible = 0;

                        if(!empty($rate->min_max_stay))
                        {
                            $arr = explode(';', $rate->min_max_stay);
                            if($difference >= $arr[0] && $difference <= $arr[1])
                            {
                                $min_max_stay_compatible = 1;
                            }
                        }

                        $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
                        $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

                        if(($date >= $first_night && $date <= $last_night) && $min_max_stay_compatible == 1)
                        {
                            $availableRatesIds[$date][] = $rate->id;
                            $availableRates[$date][]= $rate;
                        }
                    }
                }

                $date = strtotime("+1 day", strtotime($date));
                $date = date("Y-m-d", $date);
            }

            if(!empty($availableRatesIds))
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    $availableRatesIds[$key] = implode(',', $value);
                }
            }

            $arr['availableRatesIds'] = $availableRatesIds;
            $arr['availableRates'] = $availableRates;
            return $arr;
        }
        else
        {
            return $arr;
        }
    }

    public static function getSeasonGroupArray($item_id)
    {
        $groups = [];

        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            $x = 0;
            foreach ($rates as $key => $rate) 
            {
                $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
                $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

                $arr['start_date'] = $first_night;
                $arr['end_date'] = $last_night;

                if(!empty($groups))
                {
                    $flag = false;
                    foreach ($groups as $key => $value) 
                    {
                        if($first_night == $value['start_date'] && $last_night == $value['end_date'])
                        {
                            $flag = true;
                            break;
                        }
                    }

                    if($flag == false)
                        $groups[] = $arr;
                }

                if(!$x)
                {
                    $groups[] = $arr;
                    $x = 1;
                }
            }
            return $groups;
        }
        else
        {
            return $groups;
        }
    }

    public static function checkRateConditions($item_id,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions)
    {
        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            foreach ($rates as $key => $rate) 
            {
                $checkin_allowed = explode(';', $rate->check_in_allowed);
                $checkout_allowed = explode(';', $rate->check_out_allowed);

                if(!(self::RateAllowedQualifies($arrival_date,$departure_date,$rate->rate_allowed) && in_array($arrival_day-1, $checkin_allowed) && in_array($departure_day-1, $checkout_allowed)) && $remove_rate_conditions==0)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public static function checkRateConditionsForUpdate($availableRates,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions)
    {
        $rates = [];

        foreach ($availableRates as $dateKey => $rates) 
        {
            foreach ($rates as $key => $rate) 
            {
                if(!in_array($rate, $rates))
                {
                    array_push($rates, $rate);
                }
            }
        }

        if(!empty($rates))
        {
            foreach ($rates as $key => $rate) 
            {
                $checkin_allowed = explode(';', $rate->check_in_allowed);
                $checkout_allowed = explode(';', $rate->check_out_allowed);

                if(!(self::RateAllowedQualifies($arrival_date,$departure_date,$rate->rate_allowed) && in_array($arrival_day-1, $checkin_allowed) && in_array($departure_day-1, $checkout_allowed)) && $remove_rate_conditions==0)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public static function checkAvailableItemNamesIncludedCart($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()
                                        ->where(['bookable_item_id' => $item_id])
                                        ->orderBy('item_order ASC')
                                        ->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDatesCart::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items_cart.created_by' => Yii::$app->user->identity->id, 'bookings_items_cart.deleted_at'=>0, 'date' => $date, 'booking_dates_cart.item_id' => $item_id])->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        if(!empty($bookableItemNames))
        {
            foreach ($bookableItemNames as $key => $value) 
            {
                if(!in_array($value, $bookedItemNames))
                {
                    $availableItemNames[] = $value;
                }
            }
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNames($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        if(!empty($bookableItemNames))
        {
            foreach ($bookableItemNames as $key => $value) 
            {
                if(!in_array($value, $bookedItemNames))
                {
                    $availableItemNames[] = $value;
                }
            }
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNamesIncludedSaved($item_id,$date,$difference,$booking_item_id)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];
        
        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->andWhere(['!=','booking_item_id',$booking_item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        if(!empty($bookableItemNames))
        {
            foreach ($bookableItemNames as $key => $value) 
            {
                if(!in_array($value, $bookedItemNames))
                {
                    $availableItemNames[] = $value;
                }
            }
        }

        return $availableItemNames;
    }

    //////////////////////////////////////////////
    // This copy has item names in seperate rates and dates
    //////////////////////////////////////////////

    public static function checkAvailableItemNamesIncludedCartCopy($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.created_by' => Yii::$app->user->identity->id, 'bookings_items.temp_flag' => 1, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(isset($bookedItemNames[$date]))
                    {
                        if(!in_array($model->item_name_id, $bookedItemNames[$date]))
                            $bookedItemNames[$date][] = $model->item_name_id;
                    }
                    else
                    {
                        $bookedItemNames[$date][] = $model->item_name_id;
                    }
                }
            }
            else
            {
                $bookedItemNames[$date] = [];
            }

            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(isset($bookedItemNames[$date]))
                    {
                        if(!in_array($model->item_name_id, $bookedItemNames[$date]))
                            $bookedItemNames[$date][] = $model->item_name_id;
                    }
                    else
                    {
                        $bookedItemNames[$date][] = $model->item_name_id;
                    }
                }
            }

            if(!empty($bookableItemNames))
            {
                foreach ($bookableItemNames as $key => $value) 
                {
                    if(!in_array($value, $bookedItemNames[$date]))
                    {
                        $availableItemNames[$date][] = $value;
                    }
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNamesCopy($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(isset($bookedItemNames[$date]))
                    {
                        if(!in_array($model->item_name_id, $bookedItemNames[$date]))
                            $bookedItemNames[$date][] = $model->item_name_id;
                    }
                    else
                    {
                        $bookedItemNames[$date][] = $model->item_name_id;
                    }
                }
            }
            else
            {
                $bookedItemNames[$date] = [];
            }

            if(!empty($bookableItemNames))
            {
                foreach ($bookableItemNames as $key => $value) 
                {
                    if(!in_array($value, $bookedItemNames[$date]))
                    {
                        $availableItemNames[$date][] = $value;
                    }
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNamesforClosedDates($item_id,$date,$difference,$remove_rate_conditions)
    {
        $bookableItemNames = BookableItemsNames::find()
                                        ->where(['bookable_item_id' => $item_id])
                                        ->orderBy('item_order ASC')
                                        ->all();

        $bookable_item = BookableItems::findOne(['id' => $item_id]);
        $bookable_item_name_arr = [];
        // foreach ($bookableItemNames as $value) 
        // {
        //     array_push($bookable_item_name_arr, $value->id);

        // }
        $item_closed = array();
        $bookedItemNames = [];
        $availableItemNames = [];



        $original_date = $date;
        foreach ($bookable_item->bookableItemsNames as $item_name) 
        {
            $date = $original_date;
            $bookable_item_name_flag = true;
            $bookable_item_name_open_flag = true;
            for ($i=0; $i < $difference ; $i++)
            {
                $status_override = BookingOverrideStatusDetails::findOne(['date' => $date,'bookable_item_name_id' => $item_name->id,'booking_override_status_id' =>  3]);
                if(!empty($status_override) )
                {
                    if(isset($remove_rate_conditions) && $remove_rate_conditions == 0)
                    {
                        $bookable_item_name_flag = false;
                    }
                    
                }

                $unit_closed = UnitAvailability::findOne(['date' => $date, 'unit_id' => $item_name->id]);
                if(!empty($unit_closed))
                {
                    if(isset($remove_rate_conditions) && $remove_rate_conditions == 0)
                    {
                        $bookable_item_name_flag = false;
                    }
                    // $bookable_item_name_open_flag = false;
                }

                $date = strtotime("+1 day", strtotime($date));
                $date = date("Y-m-d", $date);
            }
            if($bookable_item_name_flag && $bookable_item_name_open_flag)
            {
                array_push($bookable_item_name_arr, $item_name);
            }
        }
            
            

        // if(!empty($bookableItemNames))
        // {
        //     foreach ($bookableItemNames as $key => $value) 
        //     {
        //         if(!in_array($value, $bookedItemNames))
        //         {
        //             $availableItemNames[] = $value;
        //         }
        //     }
        // }

        return $bookable_item_name_arr;
    }


    public function getBookingDateFinances()
    {
        return $this->hasMany(BookingDateFinancesCart::className(), ['booking_item_id' => 'id']);
    }

    public function getBookingDateCharges()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_item_id' => 'id'])
                    ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT]);
    }

    public function getBookingDatePayments()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_item_id' => 'id'])
                    ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT]);
    }

    public function getBookingHousekeepingStatus()
    {
        return $this->hasMany(BookingHousekeepingStatusCart::className(), ['booking_item_id' => 'id']);
    }


    public static function getChargesRow()
    {
        $html = "";
        $html .= '<tr>';
            $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
            $html.='<td> <input   class="form-control  calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
            $html.='<td><input class="form-control" type="text" name=""></td>';
            $html.='<td><input class="form-control" type="text" name="" value=1></td>';
            $html.='<td><input class="form-control price" type="text" name="" style="text-align: right;"></td>';
            $html.='<td><input class="form-control vat" type="text" name="" style="text-align: right;"></td>';
            $html.='<td><input class="form-control tax" type="text" name="" style="text-align: right;"></td>';
            $html.='<td><input class="form-control" type="text" name="" style="text-align: right;" readonly></td>';

            $html.= '<td><div class="col-sm-1" style="margin-top:7px;">';
                $html.= '<a href="javascript:void(0)" class="delete-dynamic-row-charges"><i class="fa fa-times fa-2x " style="color: red" aria-hidden="true"></i></a>';
            $html.= '</div></td>';
        $html .= '</tr>';    

        return $html;
    }

    public static function getAddOnDropdownRow($id)
    { 

        $add_ons = DestinationAddOns::find()->where(['destination_id' => $id])->all();

        // echo "<pre>";
        // print_r($add_ons);
        // exit(); 
        $html = "";
        if(!empty($add_ons))
        {
            $html .= '<tr>';
                $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
                $html.='<td> <input   class="form-control calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
                $html.='<td class="hasSelect">';
                $html.='<select class="form-control add_on_dropdown">';
                $html.='<option value=0 selected></option>';
                foreach ($add_ons as $add_on) 
                {
                    $html.='<option value="'.$add_on->id.'">'.$add_on->name.'</option>';
                }
                $html.='</select></td>';
                $html.='<td><input class="form-control" type="text" name="" value=1></td>';
                $html.='<td><input class="form-control price" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control vat" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control tax" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control" type="text" name="" style="text-align: right;" readonly></td>';

                $html.= '<td><div class="col-sm-1" style="margin-top:7px;">';
                    $html.= '<a href="javascript:void(0)" class="delete-dynamic-row-charges"><i class="fa fa-times fa-2x " style="color: red" aria-hidden="true"></i></a>';
                $html.= '</div></td>';
            $html .= '</tr>';
        }    

        return $html;
    }
    public static function getPaymentsRow()
    {
        $html = "";
        $html .= '<tr>';
            $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
            $html.='<td> <input   class="form-control  calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
            $html.='<td><input class="form-control" type="text" name=""></td>';
            $html.='<td></td>';
            $html.='<td></td>';
            $html.='<td></td>';
            $html.='<td></td>';
            $html.='<td><input class="form-control total" type="text" name="" style="text-align: right;"></td>';

            $html.= '<td><div class="col-sm-1" style="margin-top:7px;">';
                $html.= '<a class="delete-dynamic-row-payments" href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
            $html.= '</div></td>';
        $html .= '</tr>';    

        return $html;
    }

    public static function getPaymentmethodsDropdown($id)
    { 

        $payment_methods = PaymentMethodCheckin::find()->where(['provider_id' => $id])->all();

        
        $html = "";
        if(!empty($payment_methods))
        {
            $html .= '<tr>';
                $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
                $html.='<td> <input   class="form-control calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
                $html.='<td class="hasSelect">';
                $html.='<select class="form-control payment_method_dropdown">';
                $html.='<option value=0 selected></option>';
                foreach ($payment_methods as $payment_method) 
                {
                    $html.='<option value="'.$payment_method->id.'">'.$payment_method->pm->name.'</option>';
                }
                $html.='</select></td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='<td><input class="form-control total" type="text" name="" style="text-align: right;"></td>';

                $html.= '<td><div class="col-sm-1" style="margin-top:7px;">';
                    $html.= '<a href="javascript:void(0)" class="delete-dynamic-row-charges"><i class="fa fa-times fa-2x " style="color: red" aria-hidden="true"></i></a>';
                $html.= '</div></td>';
            $html .= '</tr>';
        }    
        // echo "<pre>";
        // print_r($html);
        // exit(); 
        return $html;
    }

}

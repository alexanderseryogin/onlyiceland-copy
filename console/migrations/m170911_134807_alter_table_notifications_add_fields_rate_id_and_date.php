<?php

use yii\db\Migration;

class m170911_134807_alter_table_notifications_add_fields_rate_id_and_date extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `notifications`   
                          ADD COLUMN `rate_id` INT NULL AFTER `bookable_item_id`,
                          ADD COLUMN `date` DATE NULL AFTER `rate_id`,
                          ADD FOREIGN KEY (`rate_id`) REFERENCES `rates`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
                        ');
    }

    public function down()
    {
        echo "m170911_134807_alter_table_notifications_add_fields_rate_id_and_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

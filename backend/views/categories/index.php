<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Categories;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            //'parent_id',
            [
                'attribute' => 'parent_id',
                'value' => function($data){
                    return isset($data->parent->name) ? $data->parent->name : 'N/A';
                },
                'filter' => Html::dropDownList(
                            'CategoriesSearch[parent_id]', 
                            $searchModel['parent_id'], 
                            ArrayHelper::map(Categories::find()->all(), 'id', 'name'), 
                            [
                                'prompt' => 'Select Category', 
                                'id'=>'CategoriesSearch-parent_id', 
                                'class'=>'form-control'
                            ]),
                'contentOptions' =>['style' => 'width:18%'],
            ],
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php

use yii\db\Migration;

class m161010_060027_alter_providers_add_sub_category extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` ADD COLUMN `provider_sub_category_id` INT(11) NULL AFTER `provider_category_id`, ADD CONSTRAINT `FK_provider_sub_category_id` FOREIGN KEY (`provider_sub_category_id`) REFERENCES `provider_sub_categories`(`id`) ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m161010_060027_alter_providers_add_sub_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

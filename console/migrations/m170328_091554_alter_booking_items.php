<?php

use yii\db\Migration;

class m170328_091554_alter_booking_items extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` ADD COLUMN `created_by` INT(11) NULL AFTER `booking_id`, ADD COLUMN `temp_flag` TINYINT(1) DEFAULT 1 NULL AFTER `created_by`, ADD COLUMN `travel_partner_id` INT(11) NULL AFTER `no_of_people`, ADD COLUMN `offers_id` INT(11) NULL AFTER `travel_partner_id`, ADD CONSTRAINT `bookings_items_created_by` FOREIGN KEY (`created_by`) REFERENCES `user`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, ADD CONSTRAINT `bookings_items_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `travel_partner`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `bookings_items_offers_id` FOREIGN KEY (`offers_id`) REFERENCES `offers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170328_091554_alter_booking_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HousekeepingStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Housekeeping Status Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="housekeeping-status-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add New Housekeeping Status', ['create'], ['class' => 'btn btn-primary pull-right']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'label',
            [
                'attribute' => 'label',
                'value' => function($data)
                {
                    return $data->label;
                },
                
                'contentOptions' => function($data)
                {
                    return ['style' => 'color:'.$data->text_color.'; background:'.$data->background_color.';' ];
                },
            ],
            'abbreviation',
            [
                'attribute' => 'icon',
                    'format' =>'raw',
                    'value' => function($data)
                    {
                        if(!empty($data->icon))
                        {
                            $path = Yii::getAlias('@web').'/../uploads/housekeeping-status/'.$data->id.'/icons/'.$data->icon;
                                                                
                            return '<a> <img src="'.$path.'" style="max-height:50px" > </a>';
                        }
                    },
                ],
            'background_color',
            'text_color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

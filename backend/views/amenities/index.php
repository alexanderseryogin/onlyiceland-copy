<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Amenities;
use yii\web\View;
use backend\components\Helpers;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AmenitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Amenities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amenities-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW AMENITIES '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'amenities-gridview','timeout' => 10000, 'enablePushState' => false]); ?>   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'label' => 'Icon',
                'format' =>'raw',
                'value' => function($data)
                {
                    if(!empty($data->icon))
                    {
                        $path = Yii::getAlias('@web').'/../uploads/amenities/'.$data->id.'/icons/'.$data->icon;
                                                            
                        return '<a> <img src="'.$path.'" style="max-height:50px" > </a>';
                    }
                },
            ],
            [
                'attribute' => 'type',
                'label' => 'Type',
                'format' => 'raw',
                'value' => function($data)
                {
                    if($data->type>=0)
                        return Helpers::AmenitiesLables($data->type);
                    return NULL;
                },
                'filter' => Html::dropDownList(
                    'AmenitiesSearch[type]', 
                    $searchModel['type'], 
                    Amenities::$types,
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'amenities_type_dropdown', 
                        'class'=>'form-control',
                        'style' => 'width:100%',
                    ]),
                'contentOptions' =>['style' => 'min-width:30%'],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#amenities_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#amenities-gridview').on('pjax:end', function() {

            $('#amenities_type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>
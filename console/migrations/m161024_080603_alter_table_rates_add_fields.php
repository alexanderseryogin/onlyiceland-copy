<?php

use yii\db\Migration;

class m161024_080603_alter_table_rates_add_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates` ADD COLUMN `published` VARCHAR(256) NULL AFTER `booking_flag_id`, ADD COLUMN `min_price` VARCHAR(256) NULL AFTER `published`; 
        ");
    }

    public function down()
    {
        echo "m161024_080603_alter_table_rates_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

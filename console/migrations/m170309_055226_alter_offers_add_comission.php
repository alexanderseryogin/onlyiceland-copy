<?php

use yii\db\Migration;

class m170309_055226_alter_offers_add_comission extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `offers` ADD COLUMN `commission` INT(11) NULL AFTER `show`; 
                        ALTER TABLE `travel_partner` CHANGE `commission` `commission` INT(11) NULL; 
        ");
    }

    public function down()
    {
        echo "m170309_055226_alter_offers_add_comission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

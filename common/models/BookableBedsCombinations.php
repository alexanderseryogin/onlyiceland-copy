<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_beds_combinations".
 *
 * @property string $id
 * @property integer $item_id
 * @property string $beds_combinations_id
 *
 * @property BedsCombinations $bedsCombinations
 * @property BookableItems $item
 */
class BookableBedsCombinations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_beds_combinations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'beds_combinations_id'], 'integer'],
            [['beds_combinations_id'], 'exist', 'skipOnError' => true, 'targetClass' => BedsCombinations::className(), 'targetAttribute' => ['beds_combinations_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'beds_combinations_id' => Yii::t('app', 'Beds Combinations ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBedsCombinations()
    {
        return $this->hasOne(BedsCombinations::className(), ['id' => 'beds_combinations_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    }
}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\BookableItemTypes;
use common\models\Destinations;
/* @var $this yii\web\View */
/* @var $searchModel common\models\NotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Notifications', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
<?php Pjax::begin(['id' => 'notification-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'booking_item_id',
            [
                'attribute' => 'destination_id',
                //'contentOptions' => ['class' => 'hide'],
                //'headerOptions' => ['class' => 'hide'],
                //'filterOptions' => ['class' => 'hide'],
                'label' => 'Destination',
                'value' => function($data)
                {
                    return $data->destination->name;
                },
                'filter' => Html::dropDownList(
                    'NotificationsSearch[destination_id]', 
                    $searchModel['destination_id'], 
                    ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Destination', 
                        'id'=>'provider_dropdown', 
                        'class'=>'form-control'
                    ]),
            ],
            [
                'attribute' => 'bookable_item_id',
                'label' => 'Bookable Item',
                'value' => function($data)
                {
                    if(isset($data->bookable_item_id ))
                        return $data->bookableItem->itemType->name;
                    else
                        return "NA";
                },
                'filter' => Html::dropDownList(
                    'NotificationsSearch[bookable_item_id]', 
                    $searchModel['bookable_item_id'], 
                    ArrayHelper::map(BookableItemTypes::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'bookable_item', 
                        'class'=>'form-control'
                    ]),
                // 'contentOptions' => function($data)
                // {
                //     return ['style' => 'min-width:150px;color:'.$data->text_color.'; background:'.$data->background_color.';' ];
                // },
                // 'headerOptions' =>['style' => 'min-width:150px;'],
            ],
            [
                'attribute' => 'notification_type',
                //'contentOptions' => ['class' => 'hide'],
                //'headerOptions' => ['class' => 'hide'],
                //'filterOptions' => ['class' => 'hide'],
                //'label' => 'Group',
                'filter' => Html::dropDownList(
                        'NotificationsSearch[notification_type]', 
                        $searchModel['notification_type'], 
                        [ 
                            1 => 'Rate not available',
                            2 => 'Destination Overbooking',
                            3 => 'Unit Multiple bookings',
                            4 => 'Unit not assigned',

                        ], 
                        [
                            'prompt' => 'Select an Item', 
                            'id'=>'noti_type_dropdown', 
                            'class'=>'form-control'
                        ]),
                'value' => function($data)
                {
                    return $data->getNotificationType();
                },
            ],
            [
                'format'    => 'raw',
                'attribute' => 'message',
                'value' => function($data)
                {
                    return '<a href="'.$data->getNotificationUrl().'">'.$data->message.'</a>';
                }
            ],
           // 'message:ntext',
            // [
            //     'attribute' => 'created_at',
            //     //'contentOptions' => ['class' => 'hide'],
            //     //'headerOptions' => ['class' => 'hide'],
            //     //'filterOptions' => ['class' => 'hide'],
            //     //'label' => 'Group',
            //     'value' => function($data)
            //     {
            //         return  gmdate("Y-m-d\TH:i:s\Z", strtotime($data->created_at));
            //     },
            // ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
    $this->registerJs("
        $('#noti_type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });

        $('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
        });

        $('#bookable_item').select2({
            placeholder: \"Select a Bookable Item\",
            allowClear: true
        });

        $('#notification-gridview').on('pjax:end', function() {

            $('#noti_type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
            });

            $('#provider_dropdown').select2({
                placeholder: \"Select a Destination\",
                allowClear: true
            });
            $('#bookable_item').select2({
                placeholder: \"Select a Destination\",
                allowClear: true
        });

        });
        $(document).on('pjax:complete', function() {
          $(document).find('.select2-container').hide();
        });

    ");

?>
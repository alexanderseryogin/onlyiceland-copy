<?php

use yii\db\Migration;

class m180418_114427_alter_table_bookings extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `notes` LONGTEXT NULL AFTER `comments`;
                        ');
    }

    public function safeDown()
    {
        echo "m180418_114427_alter_table_bookings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180418_114427_alter_table_bookings cannot be reverted.\n";

        return false;
    }
    */
}

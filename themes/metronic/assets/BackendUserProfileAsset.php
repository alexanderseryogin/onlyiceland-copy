<?php

namespace metronic\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BackendUserProfileAsset extends AssetBundle
{
    public $sourcePath = '@metronic/resources';
    public $css = [

        'pages/css/profile.min.css',
        'external-plugins/intl-tel-input/build/css/intlTelInput.css',
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        'global/plugins/jcrop/css/jquery.Jcrop.min.css',
        'pages/css/image-crop.min.css',

    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [
        'external-plugins/intl-tel-input/build/js/intlTelInput.min.js',
        'external-plugins/intl-tel-input/build/js/utils.js',
        'global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'global/plugins/jcrop/js/jquery.Jcrop.min.js',
        'pages/scripts/form-image-crop.min.js',
    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
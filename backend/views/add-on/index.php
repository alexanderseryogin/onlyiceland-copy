<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AddOnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Add-Ons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-on-index">

    <p>
        <?= Html::a(Yii::t('app', 'CREATE NEW ADD ON'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(['id' => 'addon-gridview','timeout' => 10000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            'description:ntext',
            [
                'attribute' => 'type',
                'value' => function($data) {
                    // echo "<pre>";
                    // print_r($data);
                    // exit;
                    return $data->getType(); 
                },
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'type', ["" => Yii::t("app","All"), "1" => Yii::t("app","Percentage"), '0' => Yii::t("app","Fixed Price")], ['class' => 'form-control','id'=>'type_dropdown']),
                'contentOptions' =>['style' => 'width:12%'],
            ],
            [
                'attribute' => 'discountable',
                'value' => function($data) {
                    // echo "<pre>";
                    // print_r($data);
                    // exit;
                    return $data->getDiscountable(); 
                },
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'discountable', ["" => Yii::t("app","All"), "1" => Yii::t("app","Yes"), '0' => Yii::t("app","No")], ['class' => 'form-control','id'=>'discountable_dropdown']),
                'contentOptions' =>['style' => 'width:12%'],
            ],
            [
                'attribute' => 'commissionable',
                'format' => 'boolean',
                'filter' => Html::activeDropDownList($searchModel, 'commissionable', ["" => Yii::t("app","All"), "1" => Yii::t("app","Yes"), '0' => Yii::t("app","No")], ['class' => 'form-control','id'=>'commissionable_dropdown']),
                'contentOptions' => ['style' => 'width:12%'],
            ],
            // 'type',
            // 'discountable',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#discountable_dropdown').select2({
        placeholder: \"Select a Value\",
        allowClear: true
    });

    $('#commissionable_dropdown').select2({
        placeholder: \"Select a Value\",
        allowClear: true
    });

    // $('#applies_dropdown').select2({
    //     placeholder: \"Select an Option\",
    //     allowClear: true
    // });

    // $('#period_dropdown').select2({
    //     placeholder: \"Select a Period\",
    //     allowClear: true
    // });

    $('#addon-gridview').on('pjax:end', function() {

        $('#type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });

        $('#discountable_dropdown').select2({
            placeholder: \"Select a Value\",
            allowClear: true
        });
        
        $('#commissionable_dropdown').select2({
            placeholder: \"Select a Value\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});");
?>

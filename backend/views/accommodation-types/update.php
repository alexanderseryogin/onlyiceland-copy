<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AccommodationTypes */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Accommodation Types',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accommodation Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="accommodation-types-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

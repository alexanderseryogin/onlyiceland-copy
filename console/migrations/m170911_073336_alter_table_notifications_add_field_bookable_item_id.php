<?php

use yii\db\Migration;

class m170911_073336_alter_table_notifications_add_field_bookable_item_id extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `notifications`   
                          ADD COLUMN `bookable_item_id` INT(11) NULL AFTER `booking_item_id`,
                          ADD FOREIGN KEY (`bookable_item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE;
                        ');
    }

    public function down()
    {
        echo "m170911_073336_alter_table_notifications_add_field_bookable_item_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

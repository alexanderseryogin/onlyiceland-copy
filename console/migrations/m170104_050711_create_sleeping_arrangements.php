<?php

use yii\db\Migration;

class m170104_050711_create_sleeping_arrangements extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `sleeping_arrangements`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `max_sleeping_capacity` INT(11) UNSIGNED, `intended_sleeping_capacity` INT(11) UNSIGNED, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170104_050711_create_sleeping_arrangements cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

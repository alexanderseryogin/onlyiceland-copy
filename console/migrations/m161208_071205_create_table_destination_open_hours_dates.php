<?php

use yii\db\Migration;

class m161208_071205_create_table_destination_open_hours_dates extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `providers_open_hours_dates` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `provider_id` int(11) DEFAULT NULL,
          `date_from` int(11) DEFAULT NULL,
          `date_to` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `FK_providers_open_hours_dates_provider_id` (`provider_id`),
          CONSTRAINT `FK_providers_open_hours_dates_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
    }

    public function down()
    {
        echo "m161208_071205_create_table_destination_open_hours_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

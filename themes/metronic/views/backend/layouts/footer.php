<?php
	//footer.php
?>
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 
    	<?= date('Y') ?> &copy; OnlyIceland
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
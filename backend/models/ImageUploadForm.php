<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\models\User;

class ImageUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var \common\models\UserProfile
     */
    private $_user_profile;
    private $_user;

    public function __construct($id, $config = [])
    {
        if (!isset($id) || empty($id)) 
        {
            throw new InvalidParamException('User Id cannot be blank.');
        }
        $this->_user = User::findIdentity($id);
        if (!$this->_user) 
        {
            throw new InvalidParamException('Invalid user ID.');
        }
        else
        {
            $this->_user_profile = $this->_user->profile;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }
    
    public function upload()
    {
        $profile = $this->_user_profile;
        if ($this->validate()) {
            $file_name = $this->_user->id . '-' . $this->_user->username . '.' . $this->imageFile->extension;
            //$file_path = realpath(dirname(__FILE__).'../../../').'/uploads/user-profile/';

            if(!file_exists(Yii::getAlias('@app').'/../uploads'))
            {
                mkdir(Yii::getAlias('@app').'/../uploads', 0777, true);
            }

            if(!file_exists(Yii::getAlias('@app').'/../uploads/user-profile'))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/user-profile', 0777, true);
            }

            /*if(!file_exists(Yii::getAlias('@app').'/../uploads/user-profile/'.$user_id))
            {
                mkdir(Yii::getAlias('@app').'/../uploads/users/'.$user_id, 0777, true);
            }*/
            $file_path = Yii::getAlias('@app').'/../uploads/user-profile/';

            $newfile = $file_path . $file_name;
            $oldfile = $file_path . $this->_user_profile->profile_picture;

            if (is_file($oldfile))
            {
                unlink($oldfile);
            }
            
            $this->imageFile->saveAs($newfile);
            $profile->profile_picture = $file_name;
            return $profile->save(false);
        } else {
            return false;
        }
    }
}
<?php

namespace common\models;

use Yii;
use common\models\BookableItems;
use common\models\Destinations;
/**
 * This is the model class for table "custom_report_columns".
 *
 * @property integer $id
 * @property string $column
 * @property integer $custom_report_id
 *
 * @property CustomReports $customReport
 */
class CustomReportColumns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    

    public static function tableName()
    {
        return 'custom_report_columns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['custom_report_id','column'], 'integer'],
            [['custom_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomReports::className(), 'targetAttribute' => ['custom_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'column' => 'Column',
            'custom_report_id' => 'Custom Report ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReport()
    {
        return $this->hasOne(CustomReports::className(), ['id' => 'custom_report_id']);
    }

    public static function getColumns()
    {
        $columns = array(

            '1' => ['db_col' => 'id', 'label' => 'Booking #', 'table' => 'bookings_items', 
                    'is_foreign_key' => 0,
                    'style' => 'text-align: right;',
                    'is_amount' => 0,
                   ],
            // '2' => ['db_col' => 'date', 'label' => 'Booking Date','table' => 'booking_dates',
            //         'is_foreign_key' => 0,
            //         'style' => 'text-align: right;',
            //         'is_amount' => 0,
            //         'virtual_column' => 'booking_date'
            //        ],
           '2' => ['db_col' => 'created_at', 'label' => 'Book Date','table' => 'bookings_items',
                    'is_foreign_key' => 0,
                    'style' => 'text-align: right;',
                    'is_amount' => 0,
                    'timestamp' => 1
                   ],
            '3' => ['db_col' => 'item_name_id', 'label' => 'Unit','table' => 'booking_dates',
                    'is_foreign_key' => 1,
                    'orignal_table' => [
                            'table' => 'bookable_items_names',
                            'col' => 'item_name'
                        ],
                    'style' => '',
                    'is_amount' => 0,
                   ],
            '4' => ['db_col' => 'rate_id', 'label' => 'Rate Name','table' => 'booking_dates',
                    'is_foreign_key' => 1,
                    'orignal_table' => [
                            'table' => 'rates',
                            'col' => 'name'
                        ],
                    'style' => '',
                    'is_amount' => 0,
                   ],
            '5' => ['db_col' => 'arrival_date', 'label' => 'ARR Date','table' => 'bookings_items',
                    'is_foreign_key' => 0,
                    'style' => 'text-align: right;',
                    'is_amount' => 0,
                   ],
            '6' => ['db_col' => 'departure_date', 'label' => 'DEP Date','table' => 'bookings_items',
                    'is_foreign_key' => 0,
                    'style' => 'text-align: right;',
                    'is_amount' => 0,
                   ],
            '7' => ['db_col' => 'offers_id', 'label' => 'Offer','table' => 'bookings_items',
                    'is_foreign_key' => 1,
                    'orignal_table' => [
                            'table' => 'offers',
                            'col' => 'name'
                        ],
                    'style' => '',
                    'is_amount' => 0,
                   ],
            '8' => ['db_col' => 'housekeeping_status_id', 'label' => 'HK Status','table' => 'bookings_items',
                     'is_foreign_key' => 0,   
                     'style' => '',
                     'is_amount' => 0,
                   ],
            '9' => ['db_col' => 'comments', 'label' => 'Comments','table' => 'bookings_items',
                    'is_foreign_key' => 0,  
                    'style' => '',   
                    'is_amount' => 0,
                   ],
            '10' => ['db_col' => 'balance', 'label' => 'Balance Due','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '11' => ['db_col' => 'no_of_children', 'label' => 'CH','table' => 'booking_dates',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '12' => ['db_col' => 'no_of_adults', 'label' => 'AD','table' => 'booking_dates',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '13' => ['db_col' => 'date', 'label' => 'Charge Date','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 0,
                    ],
            '14' => ['db_col' => 'price_description', 'label' => 'Charge Description','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '15' => ['db_col' => 'quantity', 'label' => 'Charge QTY','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 0,
                    ],
            '16' => ['db_col' => 'final_total', 'label' => 'Charge Total','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                     'virtual_column' => 'charge_total'
                    ],
            '17' => ['db_col' => 'date', 'label' => 'Payment Date','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 0,
                    ],
            '18' => ['db_col' => 'price_description', 'label' => 'Payment Description','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '19' => ['db_col' => 'final_total', 'label' => 'Payment Total','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                     'virtual_column' => 'payment_total'
                    ],
            '20' => ['db_col' => 'provider_id', 'label' => 'Destination','table' => 'bookings_items',
                     'is_foreign_key' => 1,
                     'orignal_table' => [
                                'table' => 'providers',
                                'col' => 'name'
                            ],
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '21' => ['db_col' => 'travel_partner_id', 'label' => 'Referrer','table' => 'bookings_items',
                     'is_foreign_key' => 1,
                     'orignal_table' => [
                                'table' => ['destination_travel_partners','travel_partner'],
                                'col' => ['travel_partner_id','company_name']
                            ],
                    'style' => '',
                    'is_amount' => 0,
                    ],
            '22' => ['db_col' => 'status_id', 'label' => 'Status','table' => 'bookings_items',
                     'is_foreign_key' => 1,
                     'orignal_table' => [
                                'table' => 'booking_statuses',
                                'col' => 'label'
                            ],
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '23' => ['db_col' => 'flag_id', 'label' => 'Flag','table' => 'bookings_items',
                     'is_foreign_key' => 1,
                     'orignal_table' => [
                                'table' => 'booking_types',
                                'col' => 'label'
                            ],
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '24' => ['db_col' => 'guest_first_name', 'label' => 'First Name','table' => 'booking_dates',
                     'is_foreign_key' => 0,
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '25' => ['db_col' => 'guest_last_name', 'label' => 'Last Name','table' => 'booking_dates',
                      'is_foreign_key' => 0,  
                      'style' => '',
                      'is_amount' => 0,
                    ],
            '26' => ['db_col' => 'date', 'label' => 'Booking Date','table' => 'booking_dates',
                    'is_foreign_key' => 0,
                    'style' => 'text-align: right;',
                    'is_amount' => 0,
                    // 'virtual_column' => 'booking_date'
                   ],
            '27' => ['db_col' => 'no_of_booking_days', 'label' => 'Nights','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '28' => ['db_col' => 'paid_amount', 'label' => 'Total Paid','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '29' => ['db_col' => 'gross_total', 'label' => 'Gross Total','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '30' => ['db_col' => 'averaged_total', 'label' => 'Averaged Total','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '31' => ['db_col' => 'net_total', 'label' => 'Net Total','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '32' => ['db_col' => 'booking_to_arrival', 'label' => 'Book🡒Stay','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '33' => ['db_col' => 'booking_to_cancellation', 'label' => 'Book🡒CNX','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '34' => ['db_col' => 'cancellation_to_arrival', 'label' => 'CNX🡒Stay','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '35' => ['db_col' => 'cancelled_at', 'label' => 'Cancel Date','table' => 'bookings_items',
                     'is_foreign_key' => 0, 
                     'style' => 'text-align: right;',
                     'is_amount' => 0,
                    ],
            '36' => ['db_col' => 'vat', 'label' => 'VAT %','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '37' => ['db_col' => 'tax', 'label' => 'Tax','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '38' => ['db_col' => 'discount', 'label' => 'Discount %','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '39' => ['db_col' => 'discount_amount', 'label' => 'Discount Amount','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '40' => ['db_col' => 'vat_amount', 'label' => 'VAT Amount','table' => 'booking_date_finances',
                     'is_foreign_key' => 0,
                     'style' => 'text-align: right;',
                     'is_amount' => 1,
                    ],
            '41' => ['db_col' => 'reference_no', 'label' => 'Original BK #', 'table' => 'bookings_items', 
                    'is_foreign_key' => 0,
                    'style' => 'text-align: right;',
                    'is_amount' => 0,
                   ],
            '42' => ['db_col' => 'guest_country_id', 'label' => 'Country','table' => 'booking_dates',
                     'is_foreign_key' => 1,
                     'orignal_table' => [
                                'table' => 'countries',
                                'col' => 'name'
                            ],
                     'style' => '',
                     'is_amount' => 0,
                    ],
            '43' => ['db_col' => 'notes', 'label' => 'Notes','table' => 'bookings_items',
                    'is_foreign_key' => 0,  
                    'style' => '',   
                    'is_amount' => 0,
                   ],

        );

        return $columns;
    }

    public static function getAlias()
    {
        $alias = array(

            'bookings_items' => 'BI',
            'booking_dates' => 'BD',
            'booking_date_finances' => 'BDF'

        );

        return $alias;
    }

    public static function getRelations()
    {
        $relations = array(

            'bookings_items'        =>  [ 
                                            'booking_dates' => ['my_col' => 'id', 'col' => 'booking_item_id', 'type' => '1' ],
                                            'booking_date_finances' => ['my_col' => 'id', 'col' => 'booking_item_id', 'type' => '1' ],
                                        ],
            'booking_dates'         =>  [ 
                                            'bookings_items' => ['my_col' => 'booking_item_id' ,'col' => 'id', 'type' => '0' ],
                                        ], 
            'booking_date_finances' =>  [ 
                                            'bookings_items' => ['my_col' => 'booking_item_id', 'col' => 'id', 'type' => '0' ],
                                        ]

        );
        return $relations;
    }

    public static function getDefaultColumns()
    {
        $columns = array(
            '2' => ['db_col' => 'booking_date', 'label' => 'Booking Date','table' => 'booking_dates'],
            '3' => ['db_col' => 'item_name_id', 'label' => 'Unit','table' => 'booking_dates'],
            '5' => ['db_col' => 'arrival_date', 'label' => 'Arrival','table' => 'bookings_items'],
            '6' => ['db_col' => 'departutre_date', 'label' => 'Departure','table' => 'bookings_items'],
            '25' => ['db_col' => 'guest_last_name', 'label' => 'Last Name','table' => 'booking_dates'],

        );

        return $columns;
    }

    public static function getColumnValue($key,$data)
    {
        // if($key == 21)
        // {
        //    echo "<pre>";
        //      echo "<br> key : ".$key." key here"."<br>";
        //     echo "Travel partner : ".$data." here"."<br>";
        //     print_r($data);
        //     exit(); 
        // }
            
         // 'orignal_table' => [
         //                        'table' => ['destination_travel_partners','travel_partner'],
         //                        'col' => ['travel_partner_id','company_name']
         //                    ],
        $columns = CustomReportColumns::getColumns();

        $connection = Yii::$app->getDb();

        if(is_array($columns[$key]['orignal_table']['table']))
        {
            $col_name = '';
            foreach ($columns[$key]['orignal_table']['table'] as $key1 => $value) 
            {
                $table_name = $value;
                $column = $columns[$key]['orignal_table']['col'][$key1];
                // echo $table_name.'<br>'.$column.'<br>';
                $sql = 'Select '.$column.' FROM '.$table_name.' WHERE id='.$data;
                // echo $sql;
                $command = $connection->createCommand($sql);

                $result = $command->queryOne();
                // if($key == 21)
                // {
                //    echo "<pre>";
                //      echo "<br> key : ".$key." key here"."<br>";
                //     // echo "Travel partner : ".$result." here"."<br>";
                //     print_r($result);
                //     exit(); 
                // }
                if(isset($result[$column] ) && !empty($result[$column]))
                {
                    $data = $result[$column];
                    // $col_name = $data;
                }
                else
                {
                    $data = '';
                }
            }


            if($data == '')
            {
                return '- - - -';
            }
            else
            {
                return $data;
            }
        }
        else
        {
            $table_name = $columns[$key]['orignal_table']['table'];
            $column = $columns[$key]['orignal_table']['col'];
            // echo $table_name.'<br>'.$column.'<br>';
            $sql = 'Select '.$column.' FROM '.$table_name.' WHERE id='.$data;
            // echo $sql;
            $command = $connection->createCommand($sql);

            $result = $command->queryOne();
            if(isset($result[$column] ) && !empty($result[$column]))
            {
                return $result[$column];
            }
            else
            {
                return '- - - -';
            }
        }

    }

    public static function getVirtualColumnValue($data)
    {
        if(empty($data))
        {
            return "- - - - - ";
        }
        else
        {
            $data = explode(',', $data);
            $col_arr = array();

            foreach ($data as $value) 
            {
                $index = array_search($value, $col_arr);
                if($index === false)
                {
                    array_push($col_arr, $value);
                }
            }

            if(empty($col_arr))
            {
                return "- - - - - ";
            }
            else
            {
                $ret_val = '';
                $count = 0;
                foreach ($col_arr as  $value) 
                {
                    $ret_val = $ret_val.$value;
                    $count++;
                    if($count < count($col_arr))
                    {
                        $ret_val = $ret_val.',';
                    }
                }

                return $ret_val;
            }
        }
    }

    public static function getItemName($data)
    {
        $item = BookableItems::findOne(['id' => $data]);
        if(!empty($item))
        {
            return $item->itemType->name;
        }
        else
        {
            return '-----';
        }
    }

    public static function getDestinationName($data)
    {
        $bookings_items = BookingsItems::findOne(['id' => $data]);

        $destination = Destinations::findOne(['id' => $bookings_items->provider_id]);
        if(!empty($destination))
        {
            return $destination->name;
        }
        else
        {
            return '-----';
        }
    }

    public static function getGroupName($data)
    {
        $bookings_items = BookingsItems::findOne(['id' => $data]);

        return $bookings_items->bookingItemGroups->bookingGroup->group_name;

        // if(!empty($destination))
        // {
        //     return $destination->name;
        // }
        // else
        // {
        //     return '-----';
        // }
    }



}

<?php

use yii\db\Migration;

class m170615_070804_alter_table_bookings_item_group_change_travel_partner_id_constraint extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group`  
                          DROP FOREIGN KEY `FK_booking_group_travel_partner_id`;
                        ');
        $this->execute('ALTER TABLE `booking_group`   
                          CHANGE `travel_partner_id` `travel_partner_id` INT(11) UNSIGNED NULL;
                        ');
        // $this->execute('ALTER TABLE `booking_group`  
        //                   ADD CONSTRAINT `FK_booking_group_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `destination_travel_partners`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
        //                 ');

        
        // $this->execute('ALTER TABLE `booking_group`   
        //                   CHANGE `travel_partner_id` `travel_partner_id` INT(11) UNSIGNED NULL,
        //                   DROP FOREIGN KEY `FK_booking_group_travel_partner_id`;
        //                 ');

        // $this->execute('ALTER TABLE `booking_group`  
        //                   ADD CONSTRAINT `FK_booking_group_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `destination_travel_partners`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;

        //                 ');
    }

    public function down()
    {
        echo "m170615_070804_alter_table_bookings_item_group_change_travel_partner_id_constraint cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

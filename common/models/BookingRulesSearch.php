<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BookingRules;

/**
 * BookingRulesSearch represents the model behind the search form about `common\models\BookingRules`.
 */
class BookingRulesSearch extends BookingRules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_id', 'routine_booking_confirmation', 'nearterm_booking_confirmation', 'nearterm_booking_advance', 'ebp_booking_type', 'new_booking_type', 'new_booking_status'], 'integer'],
            [['request_booking_status', 'ebp_first_night_exception_period', 'ebp_last_night_exception_period', 'same_day_booking_cutoff', 'general_booking_cancellation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BookingRules::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'routine_booking_confirmation' => $this->routine_booking_confirmation,
            'nearterm_booking_confirmation' => $this->nearterm_booking_confirmation,
            'nearterm_booking_advance' => $this->nearterm_booking_advance,
            'ebp_booking_type' => $this->ebp_booking_type,
            'ebp_first_night_exception_period' => $this->ebp_first_night_exception_period,
            'ebp_last_night_exception_period' => $this->ebp_last_night_exception_period,
            'same_day_booking_cutoff' => $this->same_day_booking_cutoff,
            'new_booking_type' => $this->new_booking_type,
            'new_booking_status' => $this->new_booking_status,
        ]);

        $query->andFilterWhere(['like', 'request_booking_status', $this->request_booking_status])
            ->andFilterWhere(['like', 'general_booking_cancellation', $this->general_booking_cancellation]);

        return $dataProvider;
    }
}

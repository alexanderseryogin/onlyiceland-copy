<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Countries;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'States/Regions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="states-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW STATE'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'states-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            //'country_id',
            [
                'attribute' => 'country_id',
                //'label' => 'Client Name',
                'value' => function($data){
                    return $data->country->name;
                },
                'filter' => Html::dropDownList(
                    'StatesSearch[country_id]', 
                    $searchModel['country_id'], 
                    ArrayHelper::map(Countries::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Country', 
                        'id'=>'StatesSearch-country', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'width:20%'],
            ],
            'name',
            [
                'attribute' => 'on_main',
                'format' => 'boolean',
                //'filter' => ,
                'contentOptions' =>['style' => 'width:20%'],
            ],
            'order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<?php
$this->registerJs("
jQuery(document).ready(function() {
    // select2 for country list
    $('#StatesSearch-country').select2({
        placeholder: \"Select Country\",
        allowClear: true
    });
    $('#states-gridview').on('pjax:end', function() {
        //$.pjax.reload({container:'#countries'});
        $('#StatesSearch-country').select2({
            placeholder: \"Select Country\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });
    
});",View::POS_END);
?>

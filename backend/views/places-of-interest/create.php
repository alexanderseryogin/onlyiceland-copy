<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PlacesOfInterest */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Places Of Interests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="places-of-interest-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CustomReports */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Custom Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-reports-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'report_title',
            'report_color',
            'report_icon',
            'description:ntext',
            'arrival_date_to',
            'arrival_date_from',
            'departure_date_to',
            'departure_date_from',
            'booking_date_to',
            'booking_date_from',
            'sort_by',
            'show_max',
        ],
    ]) ?>

</div>

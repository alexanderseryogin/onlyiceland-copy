<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;
Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
Use common\models\Destinations;
/* @var $this yii\web\View */
/* @var $model common\models\TravelPartner */
/* @var $form yii\widgets\ActiveForm */
\metronic\assets\BackendUserProfileAsset::register($this);
?>
<style>
    .hint-block{
        color:blue;
    }
</style>

<script type="text/javascript">

    // for update 
    var updated_state_id ='';
    var updated_city_id = '';
    var updated_flag = 0;
</script>

<div class="travel-partner-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                        'id' => 'travel-partner-form'
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">

                        <input type="text" name="TravelPartner[server_phone]" id="server_phone" hidden="">
                        <input type="text" name="TravelPartner[server_emergency]" id="server_emergency" hidden="">

                        <div class="row">
                            <div class="col-xs-3">
                                <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-xs-2">
                                <?= $form->field($model, 'kennitala', [
                                                                'inputOptions' => [
                                                                    'id' => 'kennitala-input',
                                                                    'class' => 'form-control'
                                                                    ]
                                                                ])->textInput(['maxlength' => true])->hint(' e.g. 123456-7890') ?>
                                <div id="kenitala_hint" style="margin-top: -20px; color:red; display: none;">Kenitala is invalid.</div>
                            </div>
                            <div class="col-xs-2">
                                <?= $form->field($model, 'phone', [
                                                                    'inputOptions' => [
                                                                        'id' => 'telephone_number',
                                                                        'class' => 'form-control',
                                                                        ],
                                                                        'template' => "{label}<br>{input}"
                                                                    ])->textInput()?>
                                <div id="phone_hint" style="margin-top: -20px; color:red; display: none;">Telephone Number is invalid.</div>
                            </div>

                            <div class="col-xs-2">
                                <?= $form->field($model, 'emergency_number', [
                                                                    'inputOptions' => [
                                                                        'id' => 'emergency_number',
                                                                        'class' => 'form-control',
                                                                        ],
                                                                        'template' => "{label}<br>{input}"
                                                                    ])->textInput()?>
                                <div id="emergency_hint" style="margin-top: -20px; color:red; display: none;">Emergency Number is invalid.</div>
                            </div>

                            <div class="col-xs-2">
                                <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-xs-6">
                                 <?= $form->field($model, 'primary_contact')->textInput(['maxlength' => true]) ?>
                            </div>

                            <div class="col-xs-6">
                                <?= $form->field($model, 'billing_contact')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <?= $form->field($model, 'booking_email')->textInput(['maxlength' => true]) ?>
                            </div>

                            <div class="col-xs-6">
                                <?= $form->field($model, 'invoice_email')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        
                        

                        <div class="row">
                        </div>

                        <div class="row">
                            <div class="col-xs-3">
                                <?= $form->field($model, 'address',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            ]
                                                        ])->textarea(['rows' => 1]) ?>
                            </div>

                            <!-- <div class="col-xs-3"> -->
                                
                                <?php
                                    if($model->isNewRecord)
                                    {
                                        $this->registerJs("$('#countries-dropdown').val(100).trigger('change');");
                                    }
                                    else
                                    {
                                        $this->registerJs("
                                            $('#countries-dropdown').val($model->country_id).trigger('change');
                                                updated_state_id = $model->state_id;
                                                updated_city_id = $model->city_id;
                                                updated_flag = 1; 
                                            ");
                                    }
                                ?>
                             <!-- </div> -->
                             <div class="col-xs-3">                       
                                <?= $form->field($model, 'country_id', [
                                'inputOptions' => [
                                    'id' => 'countries-dropdown',
                                    'class' => 'form-control',
                                    'style' => 'max-width: 500px'
                                    ]
                                ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                                    'prompt'=>'Select a Country',
                                    'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/liststates?country_id=').'"+$(this).val(), function( data ) 
                                        {
                                            $("#states-dropdown").html( data ).trigger("change");

                                            if(updated_flag==1)
                                            {
                                                updated_flag=0;
                                                $("#states-dropdown").val(updated_state_id).trigger("change");
                                            }

                                        });'
                                 ]) ?>
                            </div>
                            <div class="col-xs-3">
                                <?= $form->field($model, 'state_id', [
                                    'inputOptions' => [
                                        'id' => 'states-dropdown',
                                        'class' => 'form-control',
                                        'style' => 'max-width: 500px'
                                        ]
                                    ])->dropdownList(ArrayHelper::map(States::find()->where(['id'=>$model->state_id])->all(),'id','name'),[
                                        'prompt'=>'Select a State',
                                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/listcities?state_id=').'"+$(this).val(), function( data ) 
                                            {
                                                $("#cities-dropdown").html( data );

                                                if(updated_city_id != "")
                                                {
                                                    $("#cities-dropdown").val(updated_city_id).trigger("change");
                                                }
                                                
                                            });'
                                ]) ?> 
                            </div>
                            <div class="col-xs-3">
                                <?= $form->field($model, 'city_id', [
                                    'inputOptions' => [
                                        'id' => 'cities-dropdown',
                                        'class' => 'form-control',
                                        'style' => 'max-width: 500px'
                                        ]
                                    ])->dropdownList(ArrayHelper::map(Cities::find()->where(['id'=>$model->city_id])->all(),'id','name'),[
                                            'prompt'=>'Select a City',
                                                        
                                ]) ?>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-3"> 
                            <?= $form->field($model, 'second_address',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            ]
                                                        ])->textarea(['rows' => 1]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->field($model, 'calculation')->radioList(array(1 =>'Discount ',2=>'Commission')); ?>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-12">
                                <div class="mt-radio-inline">
                                    <label class="mt-radio">
                                        <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked> Defaut
                                        <span></span>
                                    </label>
                                    <label class="mt-radio">
                                        <input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Option 2
                                        <span></span>
                                    </label>
                                    
                                
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <?php
                                if(!$model->isNewRecord)
                                { ?>
                                    <button type="button" class="btn btn-primary" id="save-btn">Update</button>
                            <?php     }
                            ?>
                            <a class="<?= $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?>" id="submit_button" ><?=$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save') ?></a>
                            <a class="btn btn-default" href="<?= Url::to(['/travel-partner']) ?>" >Cancel</a>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#save-btn').on('click',function(){
        //alert('clicked');
        var input = $('<input>')
               .attr('type', 'hidden')
               .attr('name', 'save').val('1');
            $('#travel-partner-form').append($(input));
            $('#travel-partner-form').submit();

    });
    $('#kennitala-input').inputmask('999999-9999',{showMaskOnFocus: true,showMaskOnHover: false,'onincomplete': function(){ $('#kenitala_hint').show(); },'oncomplete': function(){ $('#kenitala_hint').hide(); }});

    $('#telephone_number').intlTelInput({
      initialCountry: 'auto',
      preferredCountries : ['is'],
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    $('#emergency_number').intlTelInput({
      initialCountry: 'auto',
      preferredCountries : ['is'],
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    // $('#telephone_number').change(function()
    // {
    //   var telInput = $('#telephone_number');
    //   if($.trim(telInput.val())) 
    //   {
    //     if (telInput.intlTelInput('isValidNumber')) 
    //     {
    //         $('#phone_hint').hide();
    //     }
    //     else 
    //     {
    //       $('#phone_hint').show();
    //     }
    //   }
    // });

    // $('#emergency_number').change(function()
    // {
    //   var telInput = $('#emergency_number');
    //   if($.trim(telInput.val())) 
    //   {
    //     if (telInput.intlTelInput('isValidNumber')) 
    //     {
    //         $('#emergency_hint').hide();
    //     }
    //     else 
    //     {
    //       $('#emergency_hint').show();
    //     }
    //   }
    // });

    $('#submit_button').click(function()
    {
        var countryData = $('#telephone_number').intlTelInput('getSelectedCountryData');

        var tel = '+'+countryData['dialCode']+' '+$('#telephone_number').val();
        $('#server_phone').val(tel);

        countryData = $('#emergency_number').intlTelInput('getSelectedCountryData');

        var emergency_number = '+'+countryData['dialCode']+' '+$('#emergency_number').val();
        $('#server_emergency').val(emergency_number);

        // *************** validate phone and emergency_number *************** //

        // if($('#telephone_number').intlTelInput('isValidNumber')==false)
        // {
        //     $('#phone_hint').show();
        //     $('#telephone_number').focus();
        //     window.location.href='#travelpartner-commission';
        // }

        // if($('#emergency_number').intlTelInput('isValidNumber')==false)
        // {
        //     $('#emergency_hint').show();
        //     $('#emergency_number').focus();
        //     window.location.href='#travelpartner-postal_code';
        // }

        // if(!$('#kennitala-input').inputmask('isComplete'))
        // {
        //     $('#kenitala_hint').show();
        //     window.location.href='#travelpartner-postal_code';
        // }

        // *************** validate phone and fax before submit

        // if($('#telephone_number').intlTelInput('isValidNumber') && $('#emergency_number').intlTelInput('isValidNumber') && $('#kennitala-input').inputmask('isComplete'))
        // {
            $('#travel-partner-form').submit();
        // }
    });

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });

});",View::POS_END);
?>

    

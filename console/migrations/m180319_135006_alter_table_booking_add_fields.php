<?php

use yii\db\Migration;

class m180319_135006_alter_table_booking_add_fields extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `bookings_items`
                          ADD COLUMN `booking_to_arrival` INT(11) NULL AFTER `paid_amount`,
                          ADD COLUMN `booking_to_cancellation` INT(11) NULL AFTER `booking_to_arrival`,
                          ADD COLUMN `cancellation_to_arrival` INT(11) NULL AFTER `booking_to_cancellation`,
                          ADD COLUMN `cancelled_at` DATE NULL AFTER `deleted_at`;
                        ');
    }

    public function safeDown()
    {
        echo "m180319_135006_alter_table_booking_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_135006_alter_table_booking_add_fields cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Destinations */

$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Destinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $provider_model->name, 'url' => ['view', 'id' => $provider_model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
// echo "<pre>";
// print_r($provider_model->active);
// exit();
?>
<div class="providers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'provider_model' => $provider_model,
        'booking_policies_model' => $booking_policies_model,
        'provider_financial_model' => $provider_financial_model,
        'booking_rules_model' => $booking_rules_model,
        'prev_destination_type' => $prev_destination_type,
        'destination_open_all_year' => $destination_open_all_year,
        'open_hours' => $open_hours,
        'meal_options' => $meal_options,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'searchModelException' => $searchModelException,
        'dataProviderException' => $dataProviderException,
        'tab_href' => $tab_href,
        'destination_travel_partners' => $travel_partners,
        'destination_add_ons' => $destination_add_ons,
        'amenities' => $amenities,
    ]) ?>

</div>

<?php

use yii\db\Migration;

class m161014_074917_create_table_travel_partner extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `travel_partner`( `id` INT(11) NOT NULL AUTO_INCREMENT, `company_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `phone` VARCHAR(256), `emergency_number` VARCHAR(256), `booking_email` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `invoice_email` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `commission` VARCHAR(256), `kennitala` VARCHAR(256), `address` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `second_address` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `country_id` INT(11), `state_id` INT(11), `city_id` INT(11), `postal_code` VARCHAR(256), PRIMARY KEY (`id`), CONSTRAINT `FK_travel_partner_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_travel_partner_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_travel_partner_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161014_074917_create_table_travel_partner cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

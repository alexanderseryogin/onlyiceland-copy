<?php
	//sidebar view
$activeController = strtolower($this->context->id);
$activeAction = strtolower($this->context->action->id);
$activeControllerAction = $activeController.'/'.$activeAction;
$activeCssClass = 'active open';  
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <?php
            	//echo $this->render('sidebar_search');
            ?>
            <li class="nav-item start <?= ($activeControllerAction == 'site/index') ? $activeCssClass:'';?>">
                <a href="<?= \yii\helpers\Url::to(['/']) ?>" class="nav-link">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <!-- <span class="arrow"></span> -->
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Management</h3>
            </li>
            <li class="nav-item <?= (($activeController == 'users') && ($activeAction == 'index' || $activeAction == 'update' || $activeAction == 'create' || $activeAction == 'view')) ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/users']) ?>" class="nav-link">
                    <i class="icon-users"></i>
                    <span class="title">Users</span>
                    <span class="selected"></span>
                    <!-- <span class="arrow"></span> -->
                </a>
            </li>

            <li class="nav-item <?= ($activeController == 'destinations' || $activeController == 'destination-types' || $activeController == 'accommodation-types' || $activeController == 'property-payment-methods') ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Destination Information</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController == 'destinations') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/destinations']) ?>" class="nav-link ">
                            <span class="title">Destination Details</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'destination-types') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/destination-types']) ?>" class="nav-link ">
                            <span class="title">Destination Types</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'accommodation-types') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/accommodation-types']) ?>" class="nav-link ">
                            <span class="title">Accommodation Types</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'property-payment-methods') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/property-payment-methods']) ?>" class="nav-link ">
                            <span class="title">Payment Methods</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item <?= ($activeController == 'booking-types' || $activeController == 'sleeping-arrangements' || $activeController == 'bed-types' || $activeController == 'beds-combinations' || $activeController == 'booking-statuses' || $activeController == 'booking-confirmation-types' || $activeController == 'bookable-items' || $activeController == 'special-requests' || $activeController == 'bookable-item-types' || $activeController == 'estimated-arrival-times' ) ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">Bookable Items</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController =='bookable-items' && $activeAction != 'unit-scheduler') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookable-items']) ?>" class="nav-link ">
                            <span class="title">Bookable Items</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='bookable-item-types') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookable-item-types']) ?>" class="nav-link ">
                            <span class="title">Bookable Item Types</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'booking-statuses') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/booking-statuses']) ?>" class="nav-link ">
                            <span class="title">Booking Statuses</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'booking-types') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/booking-types']) ?>" class="nav-link ">
                            <span class="title">Booking Flags</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='booking-confirmation-types') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/booking-confirmation-types']) ?>" class="nav-link ">
                            <span class="title">Booking Confirmation Types</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='special-requests') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/special-requests']) ?>" class="nav-link ">
                            <span class="title">Special Requests</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='sleeping-arrangements') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/sleeping-arrangements']) ?>" class="nav-link ">
                            <span class="title">Sleeping Arrangements</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='bed-types') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bed-types']) ?>" class="nav-link ">
                            <span class="title">Bed Types</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='beds-combinations') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/beds-combinations']) ?>" class="nav-link ">
                            <span class="title">Bed Combinations</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController =='estimated-arrival-times') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/estimated-arrival-times']) ?>" class="nav-link ">
                            <span class="title">Estimated Arrival Times</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction =='bookable-items/unit-scheduler') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookable-items/unit-scheduler']) ?>" class="nav-link ">
                            <span class="title">Units Availability</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- <li class="nav-item <?= ($activeController == 'rates') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/rates']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-credit-card"></i>
                    <span class="title">Rates</span>
                </a>
            </li> -->

            <li class="nav-item <?= ($activeController == 'rates' ) ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-book"></i>
                    <span class="title">Rates</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeControllerAction == 'rates/index') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/rates']) ?>" class="nav-link ">
                            <span class="title">Direct Rates</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'rates/rates-scheduler') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/rates/rates-scheduler']) ?>" class="nav-link ">
                            <span class="title">Direct Daily Rates</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item <?= ($activeController == 'bookings' || $activeController == 'booking-override-statuses' && ($activeAction =='audit-log' || $activeAction =='audit-log' || $activeAction =='audit-log' || $activeAction =='audit-log' || $activeAction =='audit-log') ) ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-book"></i>
                    <span class="title">Bookings</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeControllerAction == 'bookings/create') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookings/create']) ?>" class="nav-link ">
                            <span class="title">Create New Booking</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'bookings/index') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookings']) ?>" class="nav-link ">
                            <span class="title">View Bookings List</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'bookings/calendar') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookings/calendar']) ?>" class="nav-link ">
                            <span class="title">Booking Calendar</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'bookings/deleted-bookings') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookings/deleted-bookings']) ?>" class="nav-link ">
                            <span class="title">Deleted Bookings</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'bookings/booking-issues') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookings/booking-issues']) ?>" class="nav-link ">
                            <span class="title">Booking Scheduler</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'bookings/import-csv') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/bookings/import-csv']) ?>" class="nav-link ">
                            <span class="title">CSV Booking Import</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'booking-override-statuses/override') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/booking-override-statuses/override']) ?>" class="nav-link ">
                            <span class="title">Manage Override Statuses</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeControllerAction == 'booking-override-statuses/index') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/booking-override-statuses']) ?>" class="nav-link ">
                            <span class="title">Booking Override Statuses</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item <?= ($activeController == 'standard-reports' || $activeController == 'custom-reports') ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-files-o"></i>
                    <span class="title">Reports</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController == 'standard-reports') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/standard-reports']) ?>" class="nav-link ">
                            <span class="title">Standard Reports</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'custom-reports') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/custom-reports']) ?>" class="nav-link ">
                            <span class="title">Custom Reports</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item <?= ($activeController == 'discount-code') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/discount-code']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-line-chart"></i>
                    <span class="title">Discount / Voucher Codes</span>
                </a>
            </li>

            <li class="nav-item <?= ($activeController == 'tags') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/tags']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-tags"></i>
                    <span class="title">Tags</span>
                </a>
            </li>

             <li class="nav-item <?= ($activeController == 'upsell-items' || $activeController == 'add-on' ) ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Upsell / Add-on Items</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController == 'upsell-items') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/upsell-items']) ?>" class="nav-link nav-toggle">
                            <i class="icon-layers"></i>
                            <span class="title">Upsell Items</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'add-on') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/add-on']) ?>" class="nav-link nav-toggle">
                            <i class="fa fa-plus-square-o"></i>
                            <span class="title">Add-on Items</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- <li class="nav-item <?= ($activeController == 'upsell-items') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/upsell-items']) ?>" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Upsell Items</span>
                </a>
            </li> -->

            <li class="nav-item <?= ($activeController == 'offers') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/offers']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-gift"></i>
                    <span class="title">Offers</span>
                </a>
            </li>

            <li class="nav-item <?= ($activeController == 'travel-partner') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/travel-partner']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-bus"></i>
                    <span class="title">Travel Partners</span>
                </a>
            </li>

            <li class="nav-item <?= ($activeController == 'license-categories' || $activeController == 'license-sub-categories' ) ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Licensing Information</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController == 'license-categories') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/license-categories']) ?>" class="nav-link ">
                            <span class="title">License Categories</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'license-sub-categories') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/license-sub-categories']) ?>" class="nav-link ">
                            <span class="title">License Sub-Categories</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item <?= ($activeController == 'cities' || $activeController == 'states' || $activeController == 'state-record' || $activeController == 'countries') ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-pointer"></i>
                    <span class="title">Locations</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController == 'cities') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/cities']) ?>" class="nav-link ">
                            <span class="title">Cities</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'states') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/states']) ?>" class="nav-link ">
                            <span class="title">States</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'countries') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/countries']) ?>" class="nav-link ">
                            <span class="title">Countries</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'state-record') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/state-record']) ?>" class="nav-link ">
                            <span class="title">Region Information</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item <?= ($activeController == 'amenities') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/amenities']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-tags"></i>
                    <span class="title">Amenities</span>
                </a>
            </li>
            <!-- <li class="nav-item <?= ($activeController == 'house-keeping') ? $activeCssClass : ''; ?>">
                <a href="<?= \yii\helpers\Url::to(['/house-keeping']) ?>" class="nav-link nav-toggle">
                    <i class="fa fa-paint-brush"></i>
                    <span class="title">Housekeeping</span>
                </a>
            </li> -->

            <li class="nav-item <?= ($activeController == 'house-keeping' || $activeController == 'housekeeping-status') ? $activeCssClass : ''; ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-paint-brush"></i>
                    <span class="title">Housekeeping</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= ($activeController == 'house-keeping' && $activeAction =='details') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/house-keeping/details']) ?>" class="nav-link ">
                            <span class="title">Manage Current Status</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'house-keeping' && $activeAction =='index') ? $activeCssClass : ''; ?>">
                        <!-- <a href="<?= \yii\helpers\Url::to(['/house-keeping']) ?>" class="nav-link ">
                            <span class="title">Reports</span>
                        </a> -->
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-file-text"></i>
                            <span class="title">Reports</span>
                            <span class="selected"></span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item <?= ($activeController == 'house-keeping' && $activeAction =='index') ? $activeCssClass : ''; ?>">
                                <a href="<?= \yii\helpers\Url::to(['/house-keeping']) ?>" class="nav-link ">
                                    <span class="title">Bed Makeup Details</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item <?= ($activeController == 'housekeeping-status') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/housekeeping-status']) ?>" class="nav-link ">
                            <span class="title">Housekeeping Statuses</span>
                        </a>
                    </li>
                    
                    <!-- <li class="nav-item <?= ($activeController == 'countries') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/countries']) ?>" class="nav-link ">
                            <span class="title">Countries</span>
                        </a>
                    </li>
                    <li class="nav-item <?= ($activeController == 'state-record') ? $activeCssClass : ''; ?>">
                        <a href="<?= \yii\helpers\Url::to(['/state-record']) ?>" class="nav-link ">
                            <span class="title">Region Information</span>
                        </a>
                    </li> -->
                </ul>
            </li>        
            
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
<?php

use yii\db\Migration;

class m180606_115338_alter_table_providers_add_vat_id_column extends Migration
{

    public function up()
    {
        $this->addColumn('providers', 'vat_id', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('providers', 'vat_id');
    }
}

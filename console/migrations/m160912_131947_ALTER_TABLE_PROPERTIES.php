<?php

use yii\db\Migration;

class m160912_131947_ALTER_TABLE_PROPERTIES extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `properties` ADD COLUMN `state_id` INT(11) NULL AFTER `city_id`, ADD COLUMN `country_id` INT(11) NULL AFTER `state_id`, ADD COLUMN `business_state_id` INT(11) NULL AFTER `business_city_id`, ADD COLUMN `business_country_id` INT(11) NULL AFTER `business_state_id`, ADD CONSTRAINT `FK_properties_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_business_state_id` FOREIGN KEY (`business_state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_business_country_id` FOREIGN KEY (`business_country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL; ");
    }

    public function down()
    {
        echo "m160912_131947_ALTER_TABLE_PROPERTIES cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

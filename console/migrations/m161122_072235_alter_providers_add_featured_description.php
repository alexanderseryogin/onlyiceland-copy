<?php

use yii\db\Migration;

class m161122_072235_alter_providers_add_featured_description extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` ADD COLUMN `featured_image` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `business_postal_code`, ADD COLUMN `description` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `featured_image`;");
    }

    public function down()
    {
        echo "m161122_072235_alter_providers_add_featured_description cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

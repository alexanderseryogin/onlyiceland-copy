<?php
    use yii\helpers\Url;
    use common\models\BedTypes;
    use yii\helpers\ArrayHelper;
    use common\models\BedsCombinations;

    $SelectOption = [
        0 => '',
        1 => 'selected',
    ];

    $bedTypes = BedTypes::find()->all();
    $bedsCombinationsModel = BedsCombinations::find()->all();
?>

<!-- BEGIN ACCORDION PORTLET-->
<div class="tab-pane" id="details">

    <div class="panel-group accordion" id="accordion_details">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_details" href="#collapse_bed_type">Bed Type</a>
                </h4>
            </div>
            <div id="collapse_bed_type" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">
                    <div id="bed_type_field">
                        <div class="row">
                            <div class="col-sm-5">
                                <label class=" control-label">Bed Type</label>
                                <select name="BookableItems[bed_type][]" style="width: 100%;" id="bed_type_dropdown" class="dynamic-bed-types">
                                    <option value="">Select a Bed Type</option>
                                    <?php
                                        foreach ($bedTypes as $key => $value) 
                                        {
                                            $selected = 0;
                                            if($bed_type[0]==$value->id)
                                            {
                                                $selected = 1;
                                            }
                                            echo '<option value="'.$value->id.'" '.$SelectOption[$selected].'>'.$value->name.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <label class=" control-label">Quantity of that Bed Type</label>
                                <div class="form-group">
                                    <input id="quantity" value="<?=$quantity[0] ?>" type="text" name="BookableItems[quantity][]" />
                                </div>
                            </div>
                            <div class="col-sm-1 add-dynamic-field" style="margin-top:45px;" >
                                <a href="javascript:void(0)" ><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div id="bed_types_dynamic_div">
                            <?php
                                for ($i=1; $i < count($bed_type) ; $i++) 
                                {?>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <select name="BookableItems[bed_type][]" style="width: 100%;" class="dynamic-bed-types">
                                                <option value="">Select a Bed Type</option>
                                                <?php
                                                    foreach ($bedTypes as $key => $value) 
                                                    {
                                                        $selected = 0;
                                                        if($bed_type[$i]==$value->id)
                                                        {
                                                            $selected = 1;
                                                        }
                                                        echo '<option value="'.$value->id.'" '.$SelectOption[$selected].'>'.$value->name.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <input class="dynamic-quantity" value="<?=$quantity[$i] ?>" type="text" name="BookableItems[quantity][]" />
                                            </div>
                                        </div>
                                        <div class="col-sm-1 delete-dynamic-field" style="margin-top:17px;" >
                                            <a href="javascript:void(0)"><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>
                                        </div>
                                    </div>

                                <?php
                                }
                            ?>
                        </div>

                        <?php 
                        if(isset($dataProviderBedType) && !empty($dataProviderBedType))
                        {?>
                            <?= $this->render('bed_types_grid_view',
                                [
                                    'dataProviderBedType' => $dataProviderBedType,
                                ])?>   
                        <?php 
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_details" href="#collapse_bed_combinations">Bed Combinations</a>
                </h4>
            </div>
            <div id="collapse_bed_combinations" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">
                    <div id="bed_combinations_field">

                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-label">Beds Combinations</label>
                                <div>
                                <?php
                                    if(!empty($bedsCombinationsModel))
                                    {
                                        foreach ($bedsCombinationsModel as $key => $value) 
                                        {
                                            $checked = '';
                                            $default = '';
                                            if(!empty($model->beds_combinations) && in_array($value['id'], $model->beds_combinations) )
                                            {
                                                $checked = 'checked';
                                            }
                                            if(!empty($model->default_bed_combinations_id) && ($model->default_bed_combinations_id == $value['id'] ))
                                            {
                                                // echo "here";
                                                // exit();
                                                $default = 'checked';
                                            }

                                            $path = Yii::getAlias('@web').'/../uploads/beds-combinations/'.$value['id'].'/icons/'.$value['icon'];
                                            echo '
                                                    <label>
                                                        <input class="default-radio" type="radio" name="BookableItems[default_beds_combinations_id]" class="icheck radio-'.$value['id'].'" value="'.$value['id'].'" '.$default.'> Default <br>
                                                        <input name="BookableItems[beds_combinations][]" value="'.$value['id'].'" '.$checked.' type="checkbox" class="checkbox checkbox-'.$value['id'].'">  '.$value['combination'].' 
                                                    </label>
                                                    <label style="margin-right:20px;"><img src="'.$path.'" style="max-height:50px" ></label>

                                                ';
                                            echo '';
                                        }
                                    }
                                ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_details" href="#collapse_people_beds"> Number of Guests/Adults/Children and Extra Beds AND/OR Baby Beds/Cribs </a>
                </h4>
            </div>
            <div id="collapse_people_beds" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">

                    <div id="max_guests_adults_children_field">
                        <label class=" control-label">Desired Minimum Number of Guests and Designed Maximum Number of Guests for This Bookable Item</label>
                        <div class="form-group">
                            <input id="max_guests" value="<?=$model->max_guests ?>" type="text" name="BookableItems[max_guests]" />
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label class=" control-label">For the Total Selected Above, Select Minimum and Maximum Number of Adults</label>
                                <div class="form-group">
                                    <input id="max_adults" value="<?=$model->max_adults ?>" type="text" name="BookableItems[max_adults]" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class=" control-label">For the Total Selected Above, Select Minimum and Maximum Number of Children</label>
                                <div class="form-group">
                                    <input id="max_children" value="<?=$model->max_children ?>" type="text" name="BookableItems[max_children]" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <label class=" control-label">Select the Maximum Possible Number of Extra Beds AND/OR Baby Beds/Cribs for This Bookable Item</label>
                    <div class="form-group">
                        <input id="max_extra_baby_beds" value="<?=$model->max_extra_baby_beds ?>" type="text" name="BookableItems[max_extra_baby_beds]" />
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label class=" control-label">Maximum Number of Extra Beds for This Bookable Item</label>
                            <div class="form-group">
                                <input id="max_extra_beds" value="<?=$model->max_extra_beds ?>" type="text" name="BookableItems[max_extra_beds]" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class=" control-label">Maximum Number of Baby Beds/Cribs for This Bookable Item</label>
                            <div class="form-group">
                                <input id="max_baby_beds" value="<?=$model->max_baby_beds ?>" type="text" name="BookableItems[max_baby_beds]" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_details" href="#collapse_age"> Age Information </a>
                </h4>
            </div>
            <div id="collapse_age" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">
                    <?php 
                        if(empty($model->min_age_participation) && $model->min_age_participation!= '0')
                        {
                            $model->min_age_participation = '0';
                            $model->max_age_participation = '0';
                        }
                        $this->registerJs("
                                    $('#participation_age').ionRangeSlider({
                                        type:'double',
                                        grid:!0,
                                        min: 0,
                                        max: 100,
                                        from: $model->min_age_participation,
                                        to: $model->max_age_participation,
                                    });
                        ");
                    ?>
                    <label class=" control-label">Select the Minimum and Maximum Participation Ages</label>
                    <div class="form-group">
                        <input id="participation_age" type="text" name="BookableItems[age_participation]" />
                    </div>
                    <label class=" control-label">Maximum Free Age (age selected here will not incur any charges)</label>
                    <div class="form-group">
                        <input id="max_free_age" value="<?=$model->max_free_age ?>" type="text" name="BookableItems[max_free_age]" />
                    </div>
                    <label class=" control-label">Age Range for Children (select the age range for "Child")</label>
                    <div class="form-group">
                        <input id="age_range_children" type="text" name="BookableItems[age_range_children]" />
                    </div>
                    <label class=" control-label">Age Range for Adults (select the age range for "Adult")</label>
                    <div class="form-group">
                        <input id="min_age_adults" type="text" name="BookableItems[min_age_adults]" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-bed-type-modal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4><i class="fa fa-exclamation-triangle"></i> Confirm!</h4>
                </div>
                <div class="modal-body">
                    <h3> Are you sure you want to delete this item? </h3>
                </div>
                <div class="modal-footer">
                    <button type="button" id="yes_bed_type" class="btn blue invite_user_send">Yes</button>
                    <button type="button" class="btn default" data-dismiss="modal">No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="form-group">
         <?php
            if(!$model->isNewRecord)
            { ?>
                <button type="button" class="btn btn-primary common-save-btn" >Update</button>
        <?php     }
        ?>
        <a class="<?= $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?> common-submit-button" ><?=$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save') ?></a>
        <a class="btn btn-default" href="<?= Url::to(['/bookable-items']) ?>" >Cancel</a>
    </div>
</div>

<?php

$this->registerJs("
        $('.checkbox').click(function(){

            console.log('checked :'+$(this).is(':checked') );
            if($(this).is(':checked') == false)
            {
                $(this).prev().prev().prop('checked', false);
            }
        });
        $('.default-radio').click(function() {
            //alert($(this).val());
            console.log($(this).next().next().val());
            if($(this).next().next().is(':checked') )
            {
                console.log('is checked');
            }
            else
            {
                $(this).next().next().prop('checked', true);
                // alert('You first need to select Bed combination');
                // return false;
            }
        
    });
    ");
?>
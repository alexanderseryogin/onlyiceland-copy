<?php

use yii\db\Migration;

class m161230_113424_alter_bookable_items_add_booking_cancellation extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `general_booking_cancellation` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `description`; 
        ");
    }

    public function down()
    {
        echo "m161230_113424_alter_bookable_items_add_booking_cancellation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

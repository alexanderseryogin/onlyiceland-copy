<?php

use yii\db\Migration;

class m170216_124023_alter_bookings_add_estimated_time_arrival extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookings` ADD COLUMN `estimated_arrival_time_id` INT(11) UNSIGNED NULL AFTER `confirmation_id`, ADD CONSTRAINT `FK_bookings_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
            ALTER TABLE `bookings` DROP FOREIGN KEY `FK_bookings_offers_id`; 
            ALTER TABLE `bookings` ADD CONSTRAINT `FK_bookings_offers_id` FOREIGN KEY (`offers_id`) REFERENCES `offers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
            ALTER TABLE `bookings` DROP FOREIGN KEY `FK_bookings_travel_partner_id`; 
            ALTER TABLE `bookings` ADD CONSTRAINT `FK_bookings_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `travel_partner`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170216_124023_alter_bookings_add_estimated_time_arrival cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

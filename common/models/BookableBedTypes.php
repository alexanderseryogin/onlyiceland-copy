<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_bed_types".
 *
 * @property string $id
 * @property integer $bookable_id
 * @property string $bed_type_id
 * @property integer $quantity
 *
 * @property BedTypes $bedType
 * @property BookableItems $bookable
 */
class BookableBedTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_bed_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookable_id', 'bed_type_id', 'quantity'], 'integer'],
            [['bed_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BedTypes::className(), 'targetAttribute' => ['bed_type_id' => 'id']],
            [['bookable_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'bookable_id' => Yii::t('app', 'Bookable ID'),
            'bed_type_id' => Yii::t('app', 'Bed Type ID'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBedType()
    {
        return $this->hasOne(BedTypes::className(), ['id' => 'bed_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookable()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_id']);
    }
}

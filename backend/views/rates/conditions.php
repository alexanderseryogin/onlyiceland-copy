<?php

use common\models\BookingTypes;
use common\models\BookingStatuses;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\Rates;

?>
<div class="tab-pane" id="conditions">

    <?= $form->field($model, 'rate_allowed')->checkboxList(Rates::$week_days,[
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <div style="display:inline-block;">&nbsp; <input type="checkbox" id="rate_check_all"> Check All </div>
    <div style="display:inline-block">&nbsp; <input type="checkbox" id="rate_clear_all"> Clear All </div>
    <br><br>

    <?= $form->field($model, 'check_in_allowed')->checkboxList(Rates::$week_days,[
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <div style="display:inline-block;">&nbsp; <input type="checkbox" id="in_check_all"> Check All </div>
    <div style="display:inline-block">&nbsp; <input type="checkbox" id="in_clear_all"> Clear All </div>
    <br><br>

    <?= $form->field($model, 'check_out_allowed')->checkboxList(Rates::$week_days,[
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <div style="display:inline-block;">&nbsp; <input type="checkbox" id="out_check_all"> Check All </div>
    <div style="display:inline-block">&nbsp; <input type="checkbox" id="out_clear_all"> Clear All </div>
    <br><br>

    <?php 
        if(!empty($model->min_max_stay))
        {
            $stay_arr = explode(';', $model->min_max_stay);
            $from = $stay_arr[0];
            $to = $stay_arr[1];
        }
        else
        {
            $from = 1;
            $to = 365;
        }

        $this->registerJs("
                $('#min_max_stay').ionRangeSlider({
                    type:'double',
                    grid:!0,
                    min: 1,
                    max: 365,
                    from: $from,
                    to: $to,
                });
            ");

        if(!empty($model->general_booking_cancellation))
        {
            $this->registerJs("
                value = '$model->general_booking_cancellation';
                $('#booking_cancellation').val(value).trigger('change');
            ");
        }
        
    ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'booking_status_id', [
                'inputOptions' => [
                    'id' => 'booking-status-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%'
                    ]
                ])->dropdownList(ArrayHelper::map(BookingStatuses::find()->all(),'id','label'),['prompt'=>'Select a Status']) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'booking_flag_id', [
                'inputOptions' => [
                    'id' => 'booking-flag-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%'
                    ]
                ])->dropdownList(ArrayHelper::map(BookingTypes::find()->all(),'id','label'),['prompt'=>'Select a Flag']) ?>
        </div>
    </div>
</div>
<?php

use yii\db\Migration;

class m170525_084822_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("

            ALTER TABLE `bookings_items`   
          ADD COLUMN `item_name_id` INT(11) UNSIGNED NULL AFTER `item_id`,
          ADD CONSTRAINT `FK_bookings_items_item_name_id` FOREIGN KEY (`item_name_id`) REFERENCES `bookable_items_names`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;

           ALTER TABLE `booking_dates`   
          ADD COLUMN `item_name_id` INT(11) UNSIGNED NULL AFTER `item_id`,
          ADD CONSTRAINT `FK_booking_dates_item_name_id` FOREIGN KEY (`item_name_id`) REFERENCES `bookable_items_names`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
         
        ");
    }

    public function down()
    {
        echo "m170525_084822_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

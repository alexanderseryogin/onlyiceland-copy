<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
Use common\models\Regions;
Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
Use common\models\User;
Use common\models\PropertyTypes;
Use common\models\Properties;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\Properties */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .hint-block{
        color:blue;
    }
</style>
<div class="properties-form">

    <?php $form = ActiveForm::begin([
        'errorSummaryCssClass' => 'alert alert-danger'
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'region_id', [
    'inputOptions' => [
        'id' => 'region-dropdown',
        'class' => 'form-control',
        'style' => 'max-width: 500px'
        ]
    ])->dropdownList(ArrayHelper::map(Regions::find()->all(),'id','name'),['prompt'=>'Select a Region']) ?>

    <?= $form->field($model, 'owner_id', [
        'inputOptions' => [
            'id' => 'owner-dropdown',
            'class' => 'form-control',
            'style' => 'max-width: 500px'
            ]
        ])->dropdownList(ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_PMANAGER])->all(),'id','username'),['prompt'=>'Select an Owner']) ?>

    <?= $form->field($model, 'property_type_id', [
        'inputOptions' => [
            'id' => 'property-type-dropdown',
            'class' => 'form-control',
            'style' => 'max-width: 500px'
            ]
        ])->dropdownList(ArrayHelper::map(PropertyTypes::find()->all(),'id','name'),['prompt'=>'Select a Type']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_id_number', [
        'inputOptions' => [
            'id' => 'kennitala-input',
            'class' => 'form-control'
            ]
        ])->textInput(['maxlength' => true])->hint(' e.g. 123456-7890') ?>

    <div class="row">
        <div id="property-address" class="col-xs-6">
            <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-xs-4">
                    <?= $form->field($model, 'country_id', [
                    'inputOptions' => [
                        'id' => 'countries-dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                    ])->dropdownList(ArrayHelper::map(Countries::find()->where(['id'=>'100'])->all(),'id','name'),[
                        'prompt'=>'Select a Country',
                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('properties/liststates?country_id=').'"+$(this).val(), function( data ) {
                              $("#states-dropdown").html( data );
                            });'
                    ]) ?>      
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'state_id', [
                    'inputOptions' => [
                        'id' => 'states-dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                    ])->dropdownList(ArrayHelper::map(States::find()->where(['id'=>$model->state_id])->all(),'id','name'),[
                        'prompt'=>'Select a State',
                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('properties/listcities?state_id=').'"+$(this).val(), function( data ) {
                              $("#cities-dropdown").html( data );
                            });'
                    ]) ?>  
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'city_id', [
                    'inputOptions' => [
                        'id' => 'cities-dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                    ])->dropdownList(ArrayHelper::map(Cities::find()->where(['id'=>$model->city_id])->all(),'id','name'),[
                        'prompt'=>'Select a City',
                        
                    ]) ?> 
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php 
                        // echo Html::checkbox('Properties[same_business_address]', false, [
                        //     'label' => 'Use same address for business',
                        //     'id' => 'same-business-address'
                        // ]); 
                    ?>
                    <?= $form->field($model, 'same_address')->checkbox() ?>
                </div>
            </div>
        </div>
        <div id="property-business-address" class="col-xs-6">

            <?= $form->field($model, 'business_address')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'business_postal_code')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-xs-4">
                    <?= $form->field($model, 'business_country_id', [
                    'inputOptions' => [
                        'id' => 'business-countries-dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                    ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                        'prompt'=>'Select a Country',
                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('properties/liststates?country_id=').'"+$(this).val(), function( data ) {
                              $("#business-states-dropdown").html( data );
                            });'
                    ]) ?>      
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'business_state_id', [
                    'inputOptions' => [
                        'id' => 'business-states-dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                    ])->dropdownList(ArrayHelper::map(States::find()->where(['id'=>$model->business_state_id])->all(),'id','name'),[
                        'prompt'=>'Select a State',
                        'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('properties/listcities?state_id=').'"+$(this).val(), function( data ) {
                              $("#business-cities-dropdown").html( data );
                            });'
                    ]) ?>  
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'business_city_id', [
                    'inputOptions' => [
                        'id' => 'business-cities-dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                    ])->dropdownList(ArrayHelper::map(Cities::find()->where(['id'=>$model->business_city_id])->all(),'id','name'),[
                        'prompt'=>'Select a City',
                        
                    ]) ?> 
                </div>
            </div>
        </div>
    </div>

    <?php 
        // echo $form->field($model, 'city_id', [
        // 'inputOptions' => [
        //     'id' => 'cities-dropdown',
        //     'class' => 'form-control',
        //     'style' => 'max-width: 500px'
        //     ]
        // ])->dropdownList(ArrayHelper::map(Cities::find()->all(),'id','name'),['prompt'=>'Select a City']); 
    ?>
    <?php 
        // echo $form->field($model, 'business_city_id', [
        // 'inputOptions' => [
        //     'id' => 'business-cities-dropdown',
        //     'class' => 'form-control',
        //     'style' => 'max-width: 500px'
        //     ]
        // ])->dropdownList(ArrayHelper::map(Cities::find()->all(),'id','name'),['prompt'=>'Select a City']);
    ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reservations_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoice_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'youngest_age')->textInput() ?>

    <?= $form->field($model, 'payment_methods_for_gauarantee')->checkboxList(Properties::$methodsGuarantee, [
        //'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <?= $form->field($model, 'payment_methods_checkin')->checkboxList(Properties::$methodsCheckin,[
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#kennitala-input').inputmask('999999-9999');

    $('#properties-same_address').change(function() {
        if(this.checked) {
            $('#property-business-address').hide();
        } else {
            $('#property-business-address').show();
        }
    });
    
    $('#region-dropdown').select2({
        placeholder: \"Select a Region\",
        allowClear: true
    });

    $('#owner-dropdown').select2({
        placeholder: \"Select an Owner\",
        allowClear: true
    });

    $('#property-type-dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });

    $('#business-cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#business-states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#business-countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });
});",View::POS_END);
?>
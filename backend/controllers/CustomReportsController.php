<?php

namespace backend\controllers;

use Yii;
use common\models\CustomReports;
use common\models\CustomReportsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\BaseController;
use common\models\CustomReportColumns;
use common\models\CustomReportStatuses;
use common\models\CustomReportFlags;
use common\models\CustomReportReferers;
use common\models\CustomReportConditions;
use common\models\CustomReportDestinations;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use mPDF;
/**
 * CustomReportsController implements the CRUD actions for CustomReports model.
 */
class CustomReportsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomReports models.
     * @return mixed
     */
    public function actionPhpInfo()
    {
        echo phpinfo();
    }

    public function actionIndex()
    {
        $searchModel = new CustomReportsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // if(!empty($searchModel->destinations))
        // {
        //     echo "<pre>";
        //     print_r($searchModel->destinations);
        //     exit(); 
        // }
            

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomReports model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomReports model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomReports();

        $conditions_array = array();
        $temp = array();
        $temp['condition'] = '';
        $temp['operator'] = '';
        $temp['cond_columns'] = '';
        $temp['value'] = '';
        array_push($conditions_array, $temp);
        $model->conditions = $conditions_array;

        $columns = CustomReportColumns::getColumns();
        $defaultColumns = CustomReportColumns::getDefaultColumns();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            if(!isset($_POST['CustomReports']['columns']) || empty($_POST['CustomReports']['columns']))
            {

                Yii::$app->session->setFlash('error', 'No Columns selected');
                return $this->render('create', [
                    'model' => $model,
                    'columns' =>$columns,
                    'defaultColumns' => [],
                ]);
            }   
            // echo "<pre>";
            // print_r($_POST['CustomReports']['columns']);
            // exit();

            if(!$model->save())
            {
                echo "<pre>";
                print_r($model->errors);
                exit();
            }

            if(isset($_POST['CustomReports']['color']) && !empty($_POST['CustomReports']['color']))
            {
                $model->report_color = $_POST['CustomReports']['color'];
            }

            if(!isset($_POST['CustomReports']['arrival_checkbox']) )
            {
                $arrival_date = explode(':', $_POST['CustomReports']['arrival_date_input']);
                $model->arrival_date_from = $arrival_date[0];
                $model->arrival_date_to = $arrival_date[1];
                // $model->report_color = $_POST['CustomReports']['color'];
            }

            if(!isset($_POST['CustomReports']['departure_checkbox']) )
            {
                $departure_date = explode(':', $_POST['CustomReports']['departure_date_input']);
                $model->departure_date_from = $departure_date[0];
                $model->departure_date_to = $departure_date[1];
            }

            if(!isset($_POST['CustomReports']['booking_checkbox']) )
            {
                $booking_date = explode(':', $_POST['CustomReports']['booking_date_input']);
                $model->booking_date_from = $booking_date[0];
                $model->booking_date_to = $booking_date[1];
            }

            if(isset($_POST['CustomReports']['destinations']) && !empty($_POST['CustomReports']['destinations']))
            {
                // $model->statuses_operator = $_POST['CustomReports']['status_operator'];
                foreach ($_POST['CustomReports']['destinations'] as $value) 
                {
                    $custom_report_destination = new CustomReportDestinations();
                    $custom_report_destination->custom_report_id = $model->id;
                    $custom_report_destination->destination_id = $value;
                    if(!$custom_report_destination->save())
                    {
                        echo "<pre>";
                        print_r('destination error');
                        print_r($custom_report_destination->errors);
                        exit();
                    }
                }
            }

            if(isset($_POST['CustomReports']['statuses']) && !empty($_POST['CustomReports']['statuses']))
            {
                $model->statuses_operator = isset($_POST['CustomReports']['status_operator'])?$_POST['CustomReports']['status_operator']:0;
                foreach ($_POST['CustomReports']['statuses'] as $value) 
                {
                    $custom_report_status = new CustomReportStatuses();
                    $custom_report_status->custom_report_id = $model->id;
                    $custom_report_status->status_id = $value;
                    if(!$custom_report_status->save())
                    {
                        echo "<pre>";
                        print_r('statuses error');
                        print_r($custom_report_status->errors);
                        exit();
                    }
                }
            }

            if(isset($_POST['CustomReports']['status_not_operator']) )
            {
                $model->status_not_operator = ($_POST['CustomReports']['status_not_operator'] == 1)?1:0;
            }

            if(isset($_POST['CustomReports']['flags']) && !empty($_POST['CustomReports']['flags']))
            {
                $model->flags_operator = isset($_POST['CustomReports']['flag_operator'])?$_POST['CustomReports']['flag_operator']:0;
                foreach ($_POST['CustomReports']['flags'] as $value) 
                {
                    $custom_report_flag = new CustomReportFlags();
                    $custom_report_flag->custom_report_id = $model->id;
                    $custom_report_flag->flag_id = $value;
                    if(!$custom_report_flag->save())
                    {
                        echo "<pre>";
                        print_r('flags error');
                        print_r($custom_report_flag->errors);
                        exit();
                    }
                }
            }

            if(isset($_POST['CustomReports']['flag_not_operator']) )
            {
                $model->flag_not_operator = ($_POST['CustomReports']['flag_not_operator'] == 1)?1:0;
            }

            if(isset($_POST['CustomReports']['referers']) && !empty($_POST['CustomReports']['referers']))
            {
                $model->referers_operator = isset($_POST['CustomReports']['referer_operator'])?$_POST['CustomReports']['referer_operator']:0;
                foreach ($_POST['CustomReports']['referers'] as $value) 
                {
                    $custom_report_referer = new CustomReportReferers();
                    $custom_report_referer->custom_report_id = $model->id;
                    $custom_report_referer->travel_partner_id = $value;
                    if(!$custom_report_referer->save())
                    {
                        echo "<pre>";
                        print_r('referers error');
                        print_r($custom_report_referer->errors);
                        exit();
                    }
                }
                   
            }

            if(isset($_POST['CustomReports']['referer_not_operator']) )
            {
                $model->referer_not_operator = ($_POST['CustomReports']['referer_not_operator'] == 1)?1:0;
            }

            if(isset($_POST['CustomReports']['sort_by']) && !empty($_POST['CustomReports']['sort_by']))
            {
                $model->sort_by = $_POST['CustomReports']['sort_by'];
            }

            if(isset($_POST['CustomReports']['show_max']) && !empty($_POST['CustomReports']['show_max']))
            {
                $model->show_max = $_POST['CustomReports']['show_max'];
            }

            if(isset($_POST['CustomReports']['conditions']) && !empty($_POST['CustomReports']['conditions']))
            {
                foreach ($_POST['CustomReports']['conditions'] as $key => $value) 
                {
                    $custom_report_condition = new CustomReportConditions();
                    $custom_report_condition->custom_report_id = $model->id;
                    $custom_report_condition->condition = $value['condition'];
                    $custom_report_condition->value = $value['value'];
                    $custom_report_condition->operator = $_POST['CustomReports']['condition_operator'][$key];
                    $custom_report_condition->column = $value['cond_columns'];

                    if(!$custom_report_condition->save())
                    {
                        echo "<pre>";
                        print_r('conditions error');
                        print_r($custom_report_condition->errors);
                        exit();
                    }
                }
            }

            foreach ($_POST['CustomReports']['columns'] as $value) 
            {
                $custom_report_column = new CustomReportColumns();
                $custom_report_column->custom_report_id = $model->id;
                $custom_report_column->column = $value;

                if(!$custom_report_column->save())
                {
                    echo "<pre>";
                    print_r('columns error');
                    print_r($custom_report_column->errors);
                    exit();
                }
            }

            if(!$model->save())
            {
                echo "<pre>";
                print_r($model->errors);
                exit();
            }

            // echo "<pre>";
            // print_r($model);
            // exit();
            
            


            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'columns' =>$columns,
                'defaultColumns' => $defaultColumns
            ]);
        }
    }

    /**
     * Updates an existing CustomReports model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $columns = CustomReportColumns::getColumns();

        $selected_columns = array();
        foreach ($model->customReportColumns as $value) 
        {
            $selected_columns[$value->column] = $columns[$value->column];
        }

        $defaultColumns = $selected_columns;


        if(!empty($model->customReportDestinations))
        {
            $temp_array = array();
            foreach ($model->customReportDestinations as $value) 
            {
                array_push($temp_array, $value->destination_id);
            }

            $model->destinations_array = $temp_array;
        }

        if(!empty($model->customReportStatuses))
        {
            $temp_array = array();
            foreach ($model->customReportStatuses as $value) 
            {
                array_push($temp_array, $value->status_id);
            }

            $model->statuses_array = $temp_array;
        }

        if(!empty($model->customReportFlags))
        {
            $temp_array = array();
            foreach ($model->customReportFlags as $value) 
            {
                array_push($temp_array, $value->flag_id);
            }

            $model->flags_array = $temp_array;
        }

        if(!empty($model->customReportsReferers))
        {
            $temp_array = array();
            foreach ($model->customReportsReferers as $value) 
            {
                array_push($temp_array, $value->travel_partner_id);
            }

            $model->referers_array = $temp_array;
        }

        if(!empty($model->customReportConditions))
        {

            $conditions_array = array();
            foreach ($model->customReportConditions as $value) 
            {
                $temp = array();
                $temp['condition'] = $value->condition;
                $temp['operator'] = $value->operator;
                $temp['cond_columns'] = $value->column;
                $temp['value'] = $value->value;

                array_push($conditions_array, $temp);
            }

            // $conditions_array = array();
            // $temp = array();
            // $temp['condition'] = '';
            // $temp['operator'] = '';
            // $temp['cond_columns'] = '';
            // $temp['value'] = '';
            // array_push($conditions_array, $temp);

            $model->conditions = $conditions_array;
        }



        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
             // echo "<pre>";
             //    print_r($_POST);
             //    exit();
            if(!isset($_POST['CustomReports']['columns']) || empty($_POST['CustomReports']['columns']))
            {


                if(!empty($model->customReportDestinations))
                {
                    $temp_array = array();
                    foreach ($model->customReportDestinations as $value) 
                    {
                        array_push($temp_array, $value->destination_id);
                    }

                    $model->destinations_array = $temp_array;
                }

                if(!empty($model->customReportStatuses))
                {
                    $temp_array = array();
                    foreach ($model->customReportStatuses as $value) 
                    {
                        array_push($temp_array, $value->status_id);
                    }

                    $model->statuses_array = $temp_array;
                }

                if(!empty($model->customReportFlags))
                {
                    $temp_array = array();
                    foreach ($model->customReportFlags as $value) 
                    {
                        array_push($temp_array, $value->flag_id);
                    }

                    $model->flags_array = $temp_array;
                }

                if(!empty($model->customReportsReferers))
                {
                    $temp_array = array();
                    foreach ($model->customReportsReferers as $value) 
                    {
                        array_push($temp_array, $value->travel_partner_id);
                    }

                    $model->referers_array = $temp_array;
                }

                if(!empty($model->customReportConditions))
                {

                    $conditions_array = array();
                    foreach ($model->customReportConditions as $value) 
                    {
                        $temp = array();
                        $temp['condition'] = $value->condition;
                        $temp['operator'] = $value->operator;
                        $temp['cond_columns'] = $value->column;
                        $temp['value'] = $value->value;

                        array_push($conditions_array, $temp);
                    }

                    // $conditions_array = array();
                    // $temp = array();
                    // $temp['condition'] = '';
                    // $temp['operator'] = '';
                    // $temp['cond_columns'] = '';
                    // $temp['value'] = '';
                    // array_push($conditions_array, $temp);

                    $model->conditions = $conditions_array;
                }
                // echo "<pre>";
                // print_r($model);
                // exit();

                Yii::$app->session->setFlash('error', 'No Column selected');
                return $this->render('update', [
                    'model' => $model,
                    'columns' =>$columns,
                    'defaultColumns' => [],
                ]);
            }   

            if(!$model->save())
            {
                echo "<pre>";
                print_r($model->errors);
                exit();
            }

            // echo "<pre>";
            // print_r($_POST);
            // exit();

            if(isset($_POST['CustomReports']['color']) && !empty($_POST['CustomReports']['color']))
            {
                $model->report_color = $_POST['CustomReports']['color'];
            }

            if(!isset($_POST['CustomReports']['arrival_checkbox']) )
            {
                $arrival_date = explode(':', $_POST['CustomReports']['arrival_date_input']);
                $model->arrival_date_from = $arrival_date[0];
                $model->arrival_date_to = $arrival_date[1];
                // $model->report_color = $_POST['CustomReports']['color'];
            }
            else
            {
                $model->arrival_date_from = null;
                $model->arrival_date_to = null;
            }

            if(!isset($_POST['CustomReports']['departure_checkbox']) )
            {
                $departure_date = explode(':', $_POST['CustomReports']['departure_date_input']);
                $model->departure_date_from = $departure_date[0];
                $model->departure_date_to = $departure_date[1];
            }
            else
            {
                $model->departure_date_from = null;
                $model->departure_date_to = null;
            }

            if(!isset($_POST['CustomReports']['booking_checkbox']) )
            {
                $booking_date = explode(':', $_POST['CustomReports']['booking_date_input']);
                $model->booking_date_from = $booking_date[0];
                $model->booking_date_to = $booking_date[1];
            }
            else
            {
                $model->booking_date_from = null;
                $model->booking_date_to = null;
            }

            CustomReportDestinations::deleteAll(['custom_report_id' => $model->id]);
            if(isset($_POST['CustomReports']['destinations']) && !empty($_POST['CustomReports']['destinations']))
            {
                
                foreach ($_POST['CustomReports']['destinations'] as $value) 
                {
                    $custom_report_destination = new CustomReportDestinations();
                    $custom_report_destination->custom_report_id = $model->id;
                    $custom_report_destination->destination_id = $value;
                    if(!$custom_report_destination->save())
                    {
                        echo "<pre>";
                        print_r('destination error');
                        print_r($custom_report_destination->errors);
                        exit();
                    }
                }
            }

            CustomReportStatuses::deleteAll(['custom_report_id' => $model->id]);
            if(isset($_POST['CustomReports']['statuses']) && !empty($_POST['CustomReports']['statuses']))
            {
                
                $model->statuses_operator = isset($_POST['CustomReports']['status_operator'])?$_POST['CustomReports']['status_operator']:0;
                foreach ($_POST['CustomReports']['statuses'] as $value) 
                {
                    $custom_report_status = new CustomReportStatuses();
                    $custom_report_status->custom_report_id = $model->id;
                    $custom_report_status->status_id = $value;
                    if(!$custom_report_status->save())
                    {
                        echo "<pre>";
                        print_r('statuses error');
                        print_r($custom_report_status->errors);
                        exit();
                    }
                }
            }
            else
            {
                $model->statuses_operator = isset($_POST['CustomReports']['status_operator'])?$_POST['CustomReports']['status_operator']:0;
            }

            if(isset($_POST['CustomReports']['status_not_operator']) )
            {
                $model->status_not_operator = ($_POST['CustomReports']['status_not_operator'] == 1)?1:0;
            }


            CustomReportFlags::deleteAll(['custom_report_id' => $model->id]);
            if(isset($_POST['CustomReports']['flags']) && !empty($_POST['CustomReports']['flags']))
            {
                
                $model->flags_operator = isset($_POST['CustomReports']['flag_operator'])?$_POST['CustomReports']['flag_operator']:0;
                foreach ($_POST['CustomReports']['flags'] as $value) 
                {
                    $custom_report_flag = new CustomReportFlags();
                    $custom_report_flag->custom_report_id = $model->id;
                    $custom_report_flag->flag_id = $value;
                    if(!$custom_report_flag->save())
                    {
                        echo "<pre>";
                        print_r('flags error');
                        print_r($custom_report_flag->errors);
                        exit();
                    }
                }
            }
            else
            {
                $model->flags_operator = isset($_POST['CustomReports']['flag_operator'])?$_POST['CustomReports']['flag_operator']:0;
            }

            if(isset($_POST['CustomReports']['flag_not_operator']) )
            {
                $model->flag_not_operator = ($_POST['CustomReports']['flag_not_operator']==1)?1:0;
            }

            CustomReportReferers::deleteAll(['custom_report_id' => $model->id]);
            if(isset($_POST['CustomReports']['referers']) && !empty($_POST['CustomReports']['referers']))
            {
                
                $model->referers_operator = isset($_POST['CustomReports']['referer_operator'])?$_POST['CustomReports']['referer_operator']:0;
                foreach ($_POST['CustomReports']['referers'] as $value) 
                {
                    $custom_report_referer = new CustomReportReferers();
                    $custom_report_referer->custom_report_id = $model->id;
                    $custom_report_referer->travel_partner_id = $value;
                    if(!$custom_report_referer->save())
                    {
                        echo "<pre>";
                        print_r('referers error');
                        print_r($custom_report_referer->errors);
                        exit();
                    }
                }
                   
            }
            else
            {
                $model->referers_operator = isset($_POST['CustomReports']['referer_operator'])?$_POST['CustomReports']['referer_operator']:0;
            }

            if(isset($_POST['CustomReports']['referer_not_operator']) )
            {
                $model->referer_not_operator = ($_POST['CustomReports']['referer_not_operator'] == 1)?1:0;
            }


            if(isset($_POST['CustomReports']['sort_by']) && !empty($_POST['CustomReports']['sort_by']))
            {
                $model->sort_by = $_POST['CustomReports']['sort_by'];
            }
            else
            {
                $model->sort_by = null;
            }

            if(isset($_POST['CustomReports']['show_max']) && !empty($_POST['CustomReports']['show_max']))
            {
                $model->show_max = $_POST['CustomReports']['show_max'];
            }
            else
            {
                $model->show_max = null;
            }

            CustomReportConditions::deleteAll(['custom_report_id' => $model->id]);
            if(isset($_POST['CustomReports']['conditions']) && !empty($_POST['CustomReports']['conditions']))
            {
                
                foreach ($_POST['CustomReports']['conditions'] as $key => $value) 
                {
                    $custom_report_condition = new CustomReportConditions();
                    $custom_report_condition->custom_report_id = $model->id;
                    $custom_report_condition->condition = $value['condition'];
                    $custom_report_condition->value = $value['value'];
                    $custom_report_condition->operator = $_POST['CustomReports']['condition_operator'][$key];
                    $custom_report_condition->column = $value['cond_columns'];

                    if(!$custom_report_condition->save())
                    {
                        echo "<pre>";
                        print_r('conditions error');
                        print_r($custom_report_condition->errors);
                        exit();
                    }
                }
            }

            CustomReportColumns::deleteAll(['custom_report_id' => $model->id]);
            foreach ($_POST['CustomReports']['columns'] as $value) 
            {
                $custom_report_column = new CustomReportColumns();
                $custom_report_column->custom_report_id = $model->id;
                $custom_report_column->column = $value;

                if(!$custom_report_column->save())
                {
                    echo "<pre>";
                    print_r('columns error');
                    print_r($custom_report_column->errors);
                    exit();
                }
            }

            if(!$model->save())
            {
                echo "<pre>";
                print_r($model->errors);
                exit();
            }

            if(isset($_POST['save']) && $_POST['save'] == 1)
            {
                // $_POST['post'] = $_POST;
                return $this->redirect(['index']);
            }
            else
            {
                return $this->redirect(['update','id' => $model->id ]);
            }
            // return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'columns' =>$columns,
                'defaultColumns' => $defaultColumns
            ]);
        }
    }

    public function actionExecuteReport($id,$post = null)
    {
        // echo "<pre>";
        // print_r(unserialize($post));
        // exit();
        $this->layout = "secondary";
        $model = $this->findModel($id); 

        $select_statement_comp = $this->getSelectStatement($id);

        $select_statement = $select_statement_comp['select_statement'];
        $from_statement = $this->getFromStatement($id,$select_statement_comp['new_added_columns']);
        $where_statement = $this->getWhereStatement($id);
        $orderBy_statement = $this->getOrderByStatement($id);
        $groupBy_statement = " GROUP BY `BI`.`id` ";
        $limit_statement = $this->getLimitStatement($id);

        $query =  $select_statement.'<br>'.$from_statement.'<br>'.$where_statement.'<br>'.$groupBy_statement.'<br>'.$orderBy_statement.'<br>'.$limit_statement;

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($select_statement.' '.$from_statement. ' '.$where_statement.' '.$groupBy_statement.' '.$orderBy_statement.' '.$limit_statement);

        $result = $command->queryAll();
        
        
        // echo "<pre>";
        // print_r($result);
        // exit();

        $selected_columns = array();
        foreach ($model->customReportColumns as $column) 
        {
            array_push($selected_columns, $column->column);
        }

        return $this->render('execute', [
            'model' => $model,
            'result' => $result,
            'query' => $query,
            'org_query' => $select_statement.' '.$from_statement. ' '.$where_statement.' '.$groupBy_statement.' '.$orderBy_statement.' '.$limit_statement,
            'selected_columns' => $selected_columns,
            'post' => unserialize($post)
        ]);

    }

    public function actionShowReport()
    {
        $this->layout = "secondary";
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        // $model = $this->findModel($id); 

        $select_statement_comp = $this->getSelectStatementFromPost($_POST);

        $select_statement = $select_statement_comp['select_statement'];
        $from_statement = $this->getFromStatementFromPost($_POST,$select_statement_comp['new_added_columns']);
        $where_statement = $this->getWhereStatementFromPost($_POST);
        $groupBy_statement = " GROUP BY `BI`.`id` ";
        $orderBy_statement = $this->getOrderByStatementFromPost($_POST);
        $limit_statement = $this->getLimitStatementFromPost($_POST);

        $query =  $select_statement.'<br>'.$from_statement.'<br>'.$where_statement.'<br>'.$groupBy_statement.'<br>'.$orderBy_statement.'<br>'.$limit_statement;

        // echo "<pre>";
        // print_r($query);
        // exit();

        // $from_statement = $this->getFromStatement($id);
        // $where_statement = $this->getWhereStatement($id);
        // $orderBy_statement = $this->getOrderByStatement($id);
        // $limit_statement = $this->getLimitStatement($id);

        // echo "<pre>";
        // print_r($where_statement);
        // exit();

        // echo $orderBy_statement;
        

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($select_statement.' '.$from_statement. ' '.$where_statement.' '.$groupBy_statement.' '.$orderBy_statement.' '.$limit_statement);

        $result = $command->queryAll();
        
        // $query =  $select_statement.'<br>'.$from_statement.'<br>'.$where_statement.'<br>'.$orderBy_statement.'<br>'.$limit_statement;
        // echo "<pre>";
        // print_r($result);
        // exit();

        return $this->render('execute_from_post', [
            // 'model' => $model,
            'post' => $_POST,
            'result' => $result,
            'query' => $query,
            'org_query' => $select_statement.' '.$from_statement. ' '.$where_statement.' '.$groupBy_statement.' '.$orderBy_statement.' '.$limit_statement
        ]);

    }

    // old print function
    public function actionPrint()
    {

        // echo "<pre>";
        // print_r(unserialize($_POST['post']));
        // exit();
        $query = $_POST['query'];

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($query);

        $columns = unserialize($_POST['columns']);
        $result = $command->queryAll();
        $title = $_POST['title'];

        // $temp = array();
        // array_push($temp, $result[146]);

        $view = $this->renderPartial('report_pdf',['result' => $result,
                                                            'query' => $query,
                                                            'title' => $title,
                                                            'selected_columns' => $columns,
                                                            'sum_average' => isset($_POST['sum_average'])?$_POST['sum_average']:null,
                                                            'group_by' => isset($_POST['group_by'])?$_POST['group_by']:null
                                                            ]);
      
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'content' => $view,
            // 'content' => $this->renderPartial('report_pdf',['result' => $result,
            //                                                 'query' => $query,
            //                                                 'title' => $title,
            //                                                 'selected_columns' => $columns
            //                                                 ]), 
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // 'destination' => Pdf::DEST_BROWSER, 
            'options' => [
                'title' => $title,
                'subject' => $title
            ],
            'methods' => [
                // 'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
                // 'SetFooter' => ['|Page {PAGENO}|'],
            ],
        ]);
        // echo "not real: ".(memory_get_peak_usage(false)/1024/1024)." MiB\n";
        // echo "real: ".(memory_get_peak_usage(true)/1024/1024)." MiB\n\n";

        return $pdf->render();
        // */

        // $mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0); 
        // $view = $this->renderPartial('report_pdf',['result' => $result,
        //                                                     'query' => $query,
        //                                                     'title' => $title,
        //                                                     'selected_columns' => $columns
        //                                                     ]);
        // // echo "<pre>";
        // // print_r($view);
        // // exit();
        // $mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0); 
 
        // $mpdf->SetDisplayMode('fullpage');
         
        // $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
         
        // $mpdf->WriteHTML('hello');
                 
        // $mpdf->Output();
    }

    // new print function //

    public function actionPrintReport()
    {
        $post = unserialize($_POST['post']);

        $query = $_POST['query'];

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($query);

        // $columns = unserialize($_POST['columns']);
        $result = $command->queryAll();

        $view = $this->renderPartial('backup_report_pdf', [
                                        // 'model' => $model,
                                        'post' => $post,
                                        'result' => $result,
                                        'query' => $query,
                                        // 'org_query' => $select_statement.' '.$from_statement. ' '.$where_statement.' '.$groupBy_statement.' '.$orderBy_statement.' '.$limit_statement
                                    ]);
      
        // echo "<pre>";
        // print_r($view);
        // exit();
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'content' => $view,
            // 'content' => $this->renderPartial('report_pdf',['result' => $result,
            //                                                 'query' => $query,
            //                                                 'title' => $title,
            //                                                 'selected_columns' => $columns
            //                                                 ]), 
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // 'destination' => Pdf::DEST_BROWSER, 
            'options' => [
                'title' => $post['CustomReports']['report_title'],
                'subject' => $post['CustomReports']['report_title']
            ],
            'methods' => [
                // 'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
                // 'SetFooter' => ['|Page {PAGENO}|'],
            ],
        ]);
        // echo "not real: ".(memory_get_peak_usage(false)/1024/1024)." MiB\n";
        // echo "real: ".(memory_get_peak_usage(true)/1024/1024)." MiB\n\n";

    // it was working before (task 395)
    // return $pdf->render();
        $pdf->render();
        exit();
    }

    public function actionGetTravelPartners()
    {
        $destinations = $_POST['destinations'];
        echo json_encode([
                    'travel_partners' => $this->renderAjax('travel_partners', [
                                            'provider_id' => $destinations,
                                        ]),
                ]);
    }

    /**
     * Deletes an existing CustomReports model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function getSelectStatement($id)
    {
        $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();
        $count = 0;
        $selected = '';
        $test = 0;

        $has_charges_column = false;
        $has_payment_column = false;

        $new_added_columns = array();

        if(isset($model->customReportColumns) && !empty($model->customReportColumns))
        {
            $selected.='SELECT ';

            $index = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 1]);
            $col_check = true;
            if(!empty($index))
            {
                $selected= $selected.' DISTINCT(`BI`.`id`), ';
                $col_check = false;
            }
            foreach ($model->customReportColumns as $value) 
            {
                if($value->column == 16)
                {
                    $has_charges_column = true;
                    // $selected= $selected.'SUM(CASE When `BDF`.`type`=1 Then `'.$alias[$columns[$value->column]['table']].'`'.'.'.'`'.$columns[$value->column]['db_col'].'` Else 0 End) AS charge_total'.' ';
                }
                elseif($value->column == 19)
                {
                    $has_payment_column = true;
                }
                else
                {
                    $selected= $selected.'`'.$alias[$columns[$value->column]['table']].'`'.'.'.'`'.$columns[$value->column]['db_col'].'`'.' ';
                }
                // }
                // $selected= $selected.'`'.$alias[$columns[$value->column]['table']].'`'.'.'.'`'.$columns[$value->column]['db_col'].'`'.' ';
                $count++;
                if($count < count($model->customReportColumns) && $value->column != 16 && $value->column !=19)
                {
                    $last_letter = $selected[strlen($selected)-1];
                    if($last_letter == ',')
                    {
                        // $selected= $selected.',';
                    }
                    else
                    {
                        $selected= $selected.',';
                    }
                    
                }
            }
            if($test = 0)
            {
                $test == 1;
            }
        }

        if(!empty($model->customReportDestinations))
        {
            $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 20]);
            if(empty($check))
            {
                $last_letter = $selected[strlen($selected)-1];
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`provider_id`';
                    array_push($new_added_columns, 20);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`provider_id`';
                    array_push($new_added_columns, 20);
                }
                    
            }
        }

        if(!empty($model->customReportStatuses))
        {
            $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 22]);
            if(empty($check))
            {
                $last_letter = $selected[strlen($selected)-1];
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`status_id`';
                    array_push($new_added_columns, 22);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`status_id`';
                    array_push($new_added_columns, 22);
                }
                    
            }
        }

        if(!empty($model->customReportFlags))
        {
            $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 23]);
            if(empty($check))
            {
                $last_letter = $selected[strlen($selected)-1];
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`flag_id`';
                        array_push($new_added_columns, 23);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`flag_id`';
                    array_push($new_added_columns, 23);
                }
                
            }
        }

        if(!empty($model->customReportsReferers))
        {
            $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 21]);
            if(empty($check))
            {
                $last_letter = $selected[strlen($selected)-1];
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`travel_partner_id`';
                    array_push($new_added_columns, 21);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`travel_partner_id`';
                    array_push($new_added_columns, 21);
                }
                    
            }
        }

        if(!empty($model->customReportConditions))
        {
            foreach ($model->customReportConditions as $value) 
            {   
                $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => $value->column ]);
                if(empty($check))
                {
                    $selected = $selected.','.'`'.$alias[$columns[$value->column]['table']].'`.`'.$columns[$value->column]['db_col'].'` ';
                    array_push($new_added_columns, $value->column);
                }
            }
        }

        if($model->booking_date_from != null && $model->booking_date_to != null )
        {
            // $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 21]);
            // if(empty($check))
            // {
            $last_letter = $selected[strlen($selected)-1];
            if($last_letter == ',')
            {
                $selected = $selected.' `BD`.`date`';
                array_push($new_added_columns, 26);
            }
            else
            {
                $selected = $selected.',`BD`.`date`';
                array_push($new_added_columns, 26);
            }
            
                
            // }
        }

        // if($has_charges_column)
        // {
        //     $check_tables = $this->checkTtablesSaved($model->id,$new_added_columns);
        //     // echo $check_tables;
        //     // exit();
        //     if($check_tables)
        //     {
        //         $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS charge_total'.' ';
        //     }
        //     else
        //     {
        //         $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END) AS charge_total'.' ';
        //     }
            
        // }

        // if($has_payment_column)
        // {
        //     $check_tables = $this->checkTtablesSaved($model->id,$new_added_columns);
        //     // echo $check_tables;
        //     // exit();
        //     if($check_tables)
        //     {
        //         $selected = $selected.',SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS payment_total'.' ';
        //     }
        //     else
        //     {
        //         $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END) AS payment_total'.' ';
        //     }
            
        // }

        if($has_charges_column)
        {
            $check_tables = $this->checkTtablesSaved($model->id,$new_added_columns);

            $last_letter = $selected[strlen($selected)-1];
            if($check_tables)
            {   
                if($last_letter == ',')
                {
                    $selected= $selected.' SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS charge_total'.' ';
                }
                else
                {
                    $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS charge_total'.' ';
                }
                
            }
            else
            {
                if($last_letter == ',')
                {
                    $selected= $selected.' SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END) AS charge_total'.' ';
                }
                else
                {
                    $selected= $selected.' ,SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END) AS charge_total'.' ';
                }
                
            }
            
        }

        if($has_payment_column)
        {
            $check_tables = $this->checkTtablesSaved($model->id,$new_added_columns);

            $last_letter = $selected[strlen($selected)-1];
            if($check_tables)
            {
                if($last_letter == ',')
                {
                    $selected = $selected.' SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS payment_total'.' ';
                }
                else
                {
                    $selected = $selected.',SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS payment_total'.' ';
                }
                
            }
            else
            {
                if($last_letter == ',')
                {
                    $selected= $selected.' SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END) AS payment_total'.' ';
                }
                else
                {
                    $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END) AS payment_total'.' ';
                }
                
            }
            
        }

        if($model->group_by != null)
        {
            if($model->group_by == 2 || $model->group_by == 3)
            {
                $last_letter = $selected[strlen($selected)-1];
                if($last_letter == ',')
                {
                    $selected = $selected.' `BIG`.`booking_group_id` ';
                }
                else
                {
                    $selected = $selected.' ,`BIG`.`booking_group_id` ';
                }
            }
        }

        $selected = $selected.', `BI`.`item_id`';
        $selected = $selected.', `BI`.`provider_id`';
        // $selected= $selected.'SUM(CASE When `BDF`.`type`=1 Then `'.$alias[$columns[$value->column]['table']].'`'.'.'.'`'.$columns[$value->column]['db_col'].'` Else 0 End) AS charge_total'.' ';

        // there will alwa
        
        return [
            'select_statement' => $selected,
            'new_added_columns' => $new_added_columns,

        ];
    }

    protected function getFromStatement($id,$new_added_columns)
    {
        $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();
        $relations = CustomReportColumns::getRelations();

        $tables_arr = array();

        $from_column = '';
        foreach ($model->customReportColumns as $value) 
        {   
            $index = array_search($columns[$value->column]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value->column]['table']);               
            }
        }

        foreach ($new_added_columns as $value) 
        {
            $index = array_search($columns[$value]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value]['table']);               
            }
        }

        if(count($tables_arr) > 1)
        {
            $temp_array = array();
            $from_column = ' FROM ' ;
            $flag = true;
            foreach ($tables_arr as $value) 
            {


                foreach ($relations[$value] as $key => $relation) 
                {
                    // checking if relation table exists in original tables array
                    $index = array_search($key, $tables_arr);
                    


                    if ($index === false)
                    {
                        // array_push($tables_arr, $columns[$value->column]['table']);               
                    }
                    else
                    {
                        // checking if tables are already in relation
                        $index2 = array_search($key, $temp_array);

                        if ($index2 === false)
                        {
                            if($flag)
                            {
                                $from_column = $from_column.' '.$value.' '.$alias[$value].' INNER JOIN '.$key.' '.$alias[$key].' ON '.'`'.$alias[$value].'`.`'.$relation['my_col'].'`'.' = `'.$alias[$key].'`.`'.$relation['col'].'`' ;
                            }
                            else
                            {
                                $from_column =$from_column.' INNER JOIN '.$key.' '.$alias[$key].' ON '.'`'.$alias[$value].'`.`'.$relation['my_col'].'`'.' = `'.$alias[$key].'`.`'.$relation['col'].'`' ;
                            }
                            
                            
                            array_push($temp_array, $key);  
                            array_push($temp_array, $value);     
                            $flag = false;        
                        }
                        else
                        {
                            
                            // array_push($temp_array, $key)
                        }
                        
                    }
                }
            }
        }
        else
        {
            $from_column = ' FROM ' ;
            foreach ($tables_arr as $value) 
            {
                $from_column = $from_column.'`'.$value.'`';
            }
        }

        if($model->group_by != null)
        {
            if($model->group_by == 2 || $model->group_by == 3)
            {
                $from_column =$from_column.' INNER JOIN booking_item_group BIG ON `BIG`.`booking_item_id` = `BI`.`id`';
            }
        }
        // echo "<pre>";
        // print_r($from_column);
        // exit();

        return $from_column;
    }

    protected function getWhereStatement($id)
    {
        $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();
        $relations = CustomReportColumns::getRelations();
        $operators = CustomReports::getOperators();
        $conditions = CustomReports::getConditions();

        $test = 0;

        $tables_arr = array();

        if(!empty($model->customReportDestinations) || !empty($model->customReportStatuses) || !empty($model->customReportFlags) || !empty($model->customReportsReferers) || !empty($model->customReportConditions) || ($model->arrival_date_to != null && $model->arrival_date_from != null ) || ($model->departure_date_from != null && $model->departure_date_to != null) || ( $model->booking_date_from != null && $model->booking_date_to != null ))
        {
            $where_clause = ' WHERE ';
        }
        else
        {
            $where_clause = ' WHERE ';
        }

        if(!empty($model->customReportDestinations))
        {
            $count = 0;
            $where_clause = $where_clause.'`BI`.`provider_id` IN (';
            foreach ($model->customReportDestinations as $value) 
            {
                $where_clause = $where_clause.$value->destination_id;
                $count++;
                if($count < count($model->customReportDestinations))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';

            if($test == 0 )
            {
                $test = 1;
            }
        }

        if($model->arrival_date_to != null && $model->arrival_date_from != null )
        {
            if($test == 0)
            {
                $where_clause = $where_clause.'(`BI`.`arrival_date` BETWEEN \''.$model->arrival_date_from.'\' AND \''.$model->arrival_date_to.'\')';
                $test = 1;
            }
            else
            {
                $where_clause = $where_clause.' AND (`BI`.`arrival_date` BETWEEN \''.$model->arrival_date_from.'\' AND \''.$model->arrival_date_to.'\')';
            }
        }

        if($model->departure_date_from != null && $model->departure_date_to != null )
        {
            if($test == 0)
            {
                $where_clause = $where_clause.'(`BI`.`departure_date` BETWEEN \''.$model->departure_date_from.'\' AND \''.$model->departure_date_to.'\')';
                $test = 1;
            }
            else
            {
                $where_clause = $where_clause.' AND (`BI`.`departure_date` BETWEEN \''.$model->departure_date_from.'\' AND \''.$model->departure_date_to.'\')';
            }
        }

        if($model->booking_date_from != null && $model->booking_date_to != null )
        {
            if($test == 0)
            {
                $where_clause = $where_clause.'(`BD`.`date` BETWEEN \''.$model->booking_date_from.'\' AND \''.$model->booking_date_to.'\')';
                $test = 1;
            }
            else
            {
                $where_clause = $where_clause.' AND (`BD`.`date` BETWEEN \''.$model->booking_date_from.'\' AND \''.$model->booking_date_to.'\')';
            }
        }

        if(!empty($model->customReportStatuses))
        {
            $count = 0;

            if($test == 0)
            {
                if($model->status_not_operator == 0)
                {
                    $where_clause = $where_clause.' `BI`.`status_id` IN (';
                }
                else
                {
                    $where_clause = $where_clause.' `BI`.`status_id` NOT IN (';
                }
                $test = 1;
            }
            else
            {
                if($model->statuses_operator == null)
                {
                    $model->statuses_operator = 0;
                    $model->save();
                }
                if($model->status_not_operator == 0)
                {
                    $where_clause = $where_clause.' '.$conditions[$model->statuses_operator].' `BI`.`status_id` IN (';
                }
                else
                {
                    $where_clause = $where_clause.' '.$conditions[$model->statuses_operator].' `BI`.`status_id` NOT IN (';
                }
                
            }
            
            foreach ($model->customReportStatuses as $value) 
            {
                $where_clause = $where_clause.$value->status_id;
                $count++;
                if($count < count($model->customReportStatuses))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';
        }

        if(!empty($model->customReportFlags))
        {
            $count = 0;

            if($test == 0)
            {
                if($model->flag_not_operator == 0)
                {
                    $where_clause = $where_clause.' `BI`.`flag_id` IN (';
                }
                else
                {
                    $where_clause = $where_clause.' `BI`.`flag_id` NOT IN (';
                }
                
                $test = 1;
            }
            else
            {
                if($model->flags_operator == null)
                {
                    $model->flags_operator = 0;
                    $model->save();
                }
                if($model->flag_not_operator == 0)
                {
                    $where_clause = $where_clause.' '.$conditions[$model->flags_operator].' `BI`.`flag_id` IN (';
                }
                else
                {
                    $where_clause = $where_clause.' '.$conditions[$model->flags_operator].' `BI`.`flag_id` NOT IN (';
                }   
                
            }
            
            foreach ($model->customReportFlags as $value) 
            {
                $where_clause = $where_clause.$value->flag_id;
                $count++;
                if($count < count($model->customReportFlags))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';
        }

        if(!empty($model->customReportsReferers))
        {
            $count = 0;

            if($test == 0)
            {
                if($model->referer_not_operator == 0)
                {
                    $where_clause = $where_clause.' `BI`.`travel_partner_id` IN (';
                }
                else
                {
                    $where_clause = $where_clause.' `BI`.`travel_partner_id` NOT IN (';
                }
                
                $test = 1;
            }
            else
            {
                if($model->referers_operator == null)
                {
                    $model->referers_operator = 0;
                    $model->save();
                }
                if($model->referer_not_operator == 0)
                {
                    $where_clause = $where_clause.' '.$conditions[$model->referers_operator].'  `BI`.`travel_partner_id` IN (';
                }
                else
                {
                    $where_clause = $where_clause.' '.$conditions[$model->referers_operator].'  `BI`.`travel_partner_id` NOT IN (';
                }
                
            }
            
            foreach ($model->customReportsReferers as $value) 
            {
                $where_clause = $where_clause.$value->travel_partner_id;
                $count++;
                if($count < count($model->customReportsReferers))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';
        }

        if(!empty($model->customReportConditions))
        {
            $count = 0;

            // if($test == 0)
            // {
            //     $where_clause = $where_clause.' `BI`.`travel_partner_id` IN (';
            //     $test = 1;
            // }
            // else
            // {
            //     $where_clause = $where_clause.' '.$conditions[$model->flags_operator].'  `BI`.`travel_partner_id` IN (';
            // }
            foreach ($model->customReportConditions as $value) 
            {

                    // echo "here";
                    // exit();
                if(!empty($value->value) && $value->operator != null  )
                {
                    if(empty($value->operator))
                    {
                        $value->operator = 0;
                    }
                    if($test == 0)
                    {
                        
                        // checking if condition is like
                        if($value->condition == 4)
                        {
                            $where_clause = $where_clause.' '.'`'.$alias[$columns[$value->column]['table']].'`.`'.$columns[$value->column]['db_col'].'` '.$operators[$value->condition].' \'%'.$value->value.'%\'';
                        }
                        else
                        {
                            $where_clause = $where_clause.' '.'`'.$alias[$columns[$value->column]['table']].'`.`'.$columns[$value->column]['db_col'].'` '.$operators[$value->condition].' '.$value->value;
                        }

                        $test = 1;
                    }
                    else
                    {
                        if($value->condition == 4)
                        {
                            $where_clause = $where_clause.' '.$conditions[$value->operator].' `'.$alias[$columns[$value->column]['table']].'`.`'.$columns[$value->column]['db_col'].'` '.$operators[$value->condition].' \'%'.$value->value.'%\'';
                        }
                        else
                        {
                            $where_clause = $where_clause.' '.$conditions[$value->operator].' `'.$alias[$columns[$value->column]['table']].'`.`'.$columns[$value->column]['db_col'].'` '.$operators[$value->condition].' '.$value->value;
                        }
                        
                    }
                }
                   
            }
        }

        if($test == 0)
        {
            $where_clause = $where_clause.'`BI`.`deleted_at` = 0';
            $test = 1;
        }
        else
        {
            $where_clause = $where_clause.' AND `BI`.`deleted_at` = 0';
        }
        

        return $where_clause;
    }

    protected function getOrderByStatement($id)
    {
        $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();

        $order_by = '';
        if($model->group_by != null)
        {
            if($model->group_by == 0)
            {
                $order_by = $order_by.'ORDER BY `BI`.`provider_id` ';
            }
            if($model->group_by == 1)
            {
                $order_by = $order_by.'ORDER BY `BI`.`item_id` ';
            }
            if($model->group_by == 2 || $model->group_by == 3)
            {
                $order_by = $order_by.'ORDER BY `BIG`.`booking_group_id` ';
            }

        }
        if(!empty($model->sort_by))
        {
            if($model->group_by != null && ($model->group_by == 1 || $model->group_by == 2 || $model->group_by == 3 ))
            {
                $order_by = $order_by.', `'.$alias[$columns[$model->sort_by]['table']].'`.`'.$columns[$model->sort_by]['db_col'].'`'.' ';
            }
            else
            {
                $order_by = $order_by.'ORDER BY `'.$alias[$columns[$model->sort_by]['table']].'`.`'.$columns[$model->sort_by]['db_col'].'`'.' ';
            }

            
        }
        // echo $order_by;
        // exit();
        return $order_by;
    }

    protected function getLimitStatement($id)
    {
        $model = $this->findModel($id);
        // $columns = CustomReportColumns::getColumns();

        $limit = '';
        if(!empty($model->show_max))
        {
            $limit = ' LIMIT '.$model->show_max;
        }
        return $limit;
    }


    protected function getSelectStatementFromPost($post)
    {
        // echo "<pre>";
        // print_r($post);
        // exit();
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();
        $count = 0;
        $selected = '';
        $test = 0;

        $has_charges_column = false;
        $has_payment_column = false;

        $new_added_columns = array();

        if(isset($post['CustomReports']['columns']) && !empty($post['CustomReports']['columns']))
        {
            $selected.='SELECT ';

            $index = array_search(1, $post['CustomReports']['columns']);
            $col_check = 0;
            if($index !== false)
            {
                $selected= $selected.' DISTINCT(`BI`.`id`), ';
                $col_check = 1;
            }
            
            foreach ($post['CustomReports']['columns'] as $value) 
            {
                if($value == 16)
                {
                    $has_charges_column = true;
                    // $selected= $selected.'SUM(CASE When `BDF`.`type`=1 Then `'.$alias[$columns[$value]['table']].'`'.'.'.'`'.$columns[$value]['db_col'].'` Else 0 End) AS charge_total'.' ';
                }
                elseif($value == 19)
                {
                    $has_payment_column = true;
                }
                else
                {
                    $selected= $selected.'`'.$alias[$columns[$value]['table']].'`'.'.'.'`'.$columns[$value]['db_col'].'`'.' ';
                }
                    
                
                $count++;
                if($count < count($post['CustomReports']['columns']) && $value!= 16 && $value!=19 )
                {
                    $last_letter = $selected[strlen($selected)-1];
                    if($last_letter == ',')
                    {
                        // $selected = $selected.' `BI`.`provider_id`';     
                        // array_push($new_added_columns, 20);
                    }
                    else
                    {
                        $selected = $selected= $selected.',';  
                        // array_push($new_added_columns, 20);
                    }
                    // $selected= $selected.',';
                }
            }
            if($test = 0)
            {
                $test == 1;
            }
        }

        if(!empty($post['CustomReports']['destinations']))
        {
            // $check = CustomReportColumns::findOne(['custom_report_id' => $model->id, 'column' => 20]);
            $check = array_search(20, $post['CustomReports']['columns'] );
            $last_letter = $selected[strlen($selected)-1];
            if ($check === false)
            {   
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`provider_id`';     
                    array_push($new_added_columns, 20);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`provider_id`';     
                    array_push($new_added_columns, 20);
                }
                
            }
            // if(empty($check))
            // {
            //     $selected = $selected.',`BI`.`provider_id`';
            // }
        }

        if(!empty($post['CustomReports']['statuses']))
        {
            $check = array_search(22, $post['CustomReports']['columns']);
            $last_letter = $selected[strlen($selected)-1];
            if($check === false)
            {
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`status_id`';
                    array_push($new_added_columns, 22);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`status_id`';
                    array_push($new_added_columns, 22);
                }
                    
            }
        }

        if(!empty($post['CustomReports']['flags']))
        {
            $check = array_search(23, $post['CustomReports']['columns'] );
            $last_letter = $selected[strlen($selected)-1];
            if($check === false)
            {
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`flag_id`';
                    array_push($new_added_columns, 23);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`flag_id`';
                    array_push($new_added_columns, 23);
                }
                    
            }
        }

        if(!empty($post['CustomReports']['referers']))
        {
            $check = array_search(21, $post['CustomReports']['columns'] );
            $last_letter = $selected[strlen($selected)-1];
            if($check === false)
            {
                if($last_letter == ',')
                {
                    $selected = $selected.' `BI`.`travel_partner_id`';
                    array_push($new_added_columns, 21);
                }
                else
                {
                    $selected = $selected.' ,`BI`.`travel_partner_id`';
                    array_push($new_added_columns, 21);
                }
                    
            }
        }

        if(isset($post['CustomReports']['conditions']) && !empty($post['CustomReports']['conditions']))
        {
            foreach ($post['CustomReports']['conditions'] as  $key => $value) 
            {
                $check = array_search($value['cond_columns'], $post['CustomReports']['columns'] );
                if($check === false)
                {
                    $selected = $selected.','.'`'.$alias[$columns[$value['cond_columns']]['table']].'`.`'.$columns[$value['cond_columns']]['db_col'].'` ';
                    array_push($new_added_columns, $value['cond_columns']);
                }
            }
        }

        if(isset($post['CustomReports']['booking_date_input'])  && !empty($post['CustomReports']['booking_date_input']) && !isset($_POST['CustomReports']['booking_checkbox']))
        {
            
            $last_letter = $selected[strlen($selected)-1];
            if($last_letter == ',')
            {
                $selected = $selected.' `BD`.`date`';
                array_push($new_added_columns, 26);
            }
            else
            {
                $selected = $selected.' ,`BD`.`date`';
                array_push($new_added_columns, 26);
            }
                
            // }
        }

        if($has_charges_column)
        {
            $check_tables = $this->checkTtables($post,$new_added_columns);

            $last_letter = $selected[strlen($selected)-1];
            if($check_tables)
            {   
                if($last_letter == ',')
                {
                    $selected= $selected.' SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS charge_total'.' ';
                }
                else
                {
                    $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS charge_total'.' ';
                }
                
            }
            else
            {
                if($last_letter == ',')
                {
                    $selected= $selected.' SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END) AS charge_total'.' ';
                }
                else
                {
                    $selected= $selected.' ,SUM( CASE WHEN `BDF`.`type`=1 THEN `BDF`.`final_total` ELSE 0 END) AS charge_total'.' ';
                }
                
            }
            
        }

        if($has_payment_column)
        {
            $check_tables = $this->checkTtables($post,$new_added_columns);

            $last_letter = $selected[strlen($selected)-1];
            if($check_tables)
            {
                if($last_letter == ',')
                {
                    $selected = $selected.' SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS payment_total'.' ';
                }
                else
                {
                    $selected = $selected.',SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END)/DATEDIFF (`BI`.`departure_date`,`BI`.`arrival_date`) AS payment_total'.' ';
                }
                
            }
            else
            {
                if($last_letter == ',')
                {
                    $selected= $selected.' SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END) AS payment_total'.' ';
                }
                else
                {
                    $selected= $selected.',SUM( CASE WHEN `BDF`.`type`=2 THEN `BDF`.`final_total` ELSE 0 END) AS payment_total'.' ';
                }
                
            }
            
        }

        if(isset($post['CustomReports']['group_by']))
        {
            if($post['CustomReports']['group_by'] == 2 || $post['CustomReports']['group_by'] == 3)
            {
                $last_letter = $selected[strlen($selected)-1];
                if($last_letter == ',')
                {
                    $selected = $selected.' `BIG`.`booking_group_id` ';
                }
                else
                {
                    $selected = $selected.' ,`BIG`.`booking_group_id` ';
                }
            }
        }
        
        $selected = $selected.', `BI`.`item_id`';
        $selected = $selected.', `BI`.`provider_id`';
        return [
            'select_statement' => $selected,
            'new_added_columns' => $new_added_columns,

        ];
    }

    protected function getFromStatementFromPost($post,$new_added_columns)
    {
        // $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();
        $relations = CustomReportColumns::getRelations();

        $tables_arr = array();

        $from_column = '';
        foreach ($post['CustomReports']['columns'] as $value) 
        {   
            $index = array_search($columns[$value]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value]['table']);               
            }
        }

        foreach ($new_added_columns as $value) 
        {
            $index = array_search($columns[$value]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value]['table']);               
            }
        }
        // echo "<per>";
        // print_r($tables_arr);
        // exit();

        if(count($tables_arr) > 1)
        {
            $temp_array = array();
            $from_column = ' FROM ' ;
            $flag = true;
            foreach ($tables_arr as $value) 
            {


                foreach ($relations[$value] as $key => $relation) 
                {
                    // checking if relation table exists in original tables array
                    $index = array_search($key, $tables_arr);
                    


                    if ($index === false)
                    {
                        // array_push($tables_arr, $columns[$value->column]['table']);               
                    }
                    else
                    {
                        // checking if tables are already in relation
                        $index2 = array_search($key, $temp_array);

                        if ($index2 === false)
                        {
                            if($flag)
                            {
                                $from_column = $from_column.' '.$value.' '.$alias[$value].' INNER JOIN '.$key.' '.$alias[$key].' ON '.'`'.$alias[$value].'`.`'.$relation['my_col'].'`'.' = `'.$alias[$key].'`.`'.$relation['col'].'`' ;
                            }
                            else
                            {
                                $from_column =$from_column.' INNER JOIN '.$key.' '.$alias[$key].' ON '.'`'.$alias[$value].'`.`'.$relation['my_col'].'`'.' = `'.$alias[$key].'`.`'.$relation['col'].'`' ;
                            }
                            
                            
                            array_push($temp_array, $key);  
                            array_push($temp_array, $value);     
                            $flag = false;        
                        }
                        else
                        {
                            
                            // array_push($temp_array, $key)
                        }
                        
                    }
                }
            }
        }
        else
        {
            $from_column = ' FROM ' ;
            foreach ($tables_arr as $value) 
            {
                $from_column = $from_column.'`'.$value.'`';
            }
        }

        if(isset($post['CustomReports']['group_by']))
        {
            if($post['CustomReports']['group_by'] == 2 || $post['CustomReports']['group_by'] == 3)
            {
                $from_column =$from_column.' INNER JOIN booking_item_group BIG ON `BIG`.`booking_item_id` = `BI`.`id`';
            }
        }
        // echo "<pre>";
        // print_r($from_column);
        // exit();

        return $from_column;
    }

    protected function getWhereStatementFromPost($post)
    {
        // $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();
        $relations = CustomReportColumns::getRelations();
        $operators = CustomReports::getOperators();
        $conditions = CustomReports::getConditions();

        $test = 0;

        $tables_arr = array();

        // $arrival_date_from = ;
        // $arrival_date_to = 

        if(!empty($post['CustomReports']['destinations']) || !empty($post['CustomReports']['statuses']) || !empty($post['CustomReports']['flags']) || !empty($post['CustomReports']['referers']) || !empty($post['CustomReports']['conditions']) || !empty($post['CustomReports']['arrival_date_input']) || !empty($post['CustomReports']['departure_date_input']) || !empty($post['CustomReports']['booking_date_input']))
        {
            $where_clause = ' WHERE ';
        }

        if(!empty($post['CustomReports']['destinations']))
        {
            $count = 0;
            $where_clause = $where_clause.'`BI`.`provider_id` IN (';
            foreach ($post['CustomReports']['destinations'] as $value) 
            {
                $where_clause = $where_clause.$value;
                $count++;
                if($count < count($post['CustomReports']['destinations']))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';

            if($test == 0 )
            {
                $test = 1;
            }
        }

        if(isset($post['CustomReports']['arrival_date_input'])  && !empty($post['CustomReports']['arrival_date_input']) && !isset($_POST['CustomReports']['arrival_checkbox']))
        {
            $arrival_date_input = explode(':', $post['CustomReports']['arrival_date_input']);
            if($test == 0)
            {
                $where_clause = $where_clause.'(`BI`.`arrival_date` BETWEEN \''.$arrival_date_input[0].'\' AND \''.$arrival_date_input[1].'\')';
                $test = 1;
            }
            else
            {
                $where_clause = $where_clause.' AND (`BI`.`arrival_date` BETWEEN \''.$arrival_date_input[0].'\' AND \''.$arrival_date_input[1].'\')';
            }
        }

        if(isset($post['CustomReports']['departure_date_input'])  && !empty($post['CustomReports']['departure_date_input']) && !isset($_POST['CustomReports']['departure_checkbox']))
        {
            $departure_date_input = explode(':', $post['CustomReports']['departure_date_input']);
            if($test == 0)
            {
                $where_clause = $where_clause.'(`BI`.`departure_date` BETWEEN \"'.$departure_date_input[0].'\" AND '.$departure_date_input[1].')';
                $test = 1;
            }
            else
            {
                $where_clause = $where_clause.' AND (`BI`.`departure_date` BETWEEN \''.$departure_date_input[0].'\' AND \''.$departure_date_input[1].'\')';
            }
        }

        if(isset($post['CustomReports']['booking_date_input'])  && !empty($post['CustomReports']['booking_date_input']) && !isset($_POST['CustomReports']['booking_checkbox']))
        {
            $booking_date_input = explode(':', $post['CustomReports']['booking_date_input']);
            if($test == 0)
            {
                $where_clause = $where_clause.'(`BD`.`date` BETWEEN \''.$booking_date_input[0].'\' AND \''.$booking_date_input[1].'\')';
                $test = 1;
            }
            else
            {
                $where_clause = $where_clause.' AND (`BD`.`date` BETWEEN \''.$booking_date_input[0].'\' AND \''.$booking_date_input[1].'\')';
            }
        }

        if(isset($post['CustomReports']['statuses']) && !empty($post['CustomReports']['statuses']))
        {
            $count = 0;

            if($test == 0)
            {
                if(isset($post['CustomReports']['status_not_operator']) && $post['CustomReports']['status_not_operator'] == 1)
                {
                    $where_clause = $where_clause.' `BI`.`status_id` NOT IN (';
                }
                else
                {
                    $where_clause = $where_clause.' `BI`.`status_id` IN (';
                }
                
                $test = 1;
            }
            else
            {
                if(empty($post['CustomReports']['status_operator']))
                {
                    $post['CustomReports']['status_operator'] = 0;
                }
                if(isset($post['CustomReports']['status_not_operator']) && $post['CustomReports']['status_not_operator'] == 1)
                {
                    $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['status_operator']].' `BI`.`status_id` NOT IN (';
                }
                else
                {
                    $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['status_operator']].' `BI`.`status_id` IN (';
                }
                
            }
            
            foreach ($post['CustomReports']['statuses'] as $value) 
            {
                $where_clause = $where_clause.$value;
                $count++;
                if($count < count($post['CustomReports']['statuses']))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';
        }

        if(isset($post['CustomReports']['flags']) && !empty($post['CustomReports']['flags']))
        {
            $count = 0;

            if($test == 0)
            {
                if(isset($post['CustomReports']['flag_not_operator']) && $post['CustomReports']['flag_not_operator'] == 1)
                {
                    $where_clause = $where_clause.' `BI`.`flag_id` NOT IN (';
                }
                else
                {
                    $where_clause = $where_clause.' `BI`.`flag_id` IN (';
                }
                
                $test = 1;
            }
            else
            {
                if(empty($post['CustomReports']['flag_operator']))
                {
                    $post['CustomReports']['flag_operator'] = 0;
                }
                if(isset($post['CustomReports']['flag_not_operator']) && $post['CustomReports']['flag_not_operator'] == 1)
                {
                    $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['flag_operator']].' `BI`.`flag_id` NOT IN (';
                }
                else
                {
                    $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['flag_operator']].' `BI`.`flag_id` IN (';
                }
                
            }
            
            foreach ($post['CustomReports']['flags'] as $value) 
            {
                $where_clause = $where_clause.$value;
                $count++;
                if($count < count($post['CustomReports']['flags']))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';
        }

        if(isset($post['CustomReports']['referers']) && !empty($post['CustomReports']['referers']))
        {
            $count = 0;

            if($test == 0)
            {
                if(isset($post['CustomReports']['referer_not_operator']) && $post['CustomReports']['referer_not_operator'] == 1)
                {
                    $where_clause = $where_clause.' `BI`.`travel_partner_id` NOT IN (';
                }
                else
                {
                    $where_clause = $where_clause.' `BI`.`travel_partner_id` IN (';
                }
                
                $test = 1;
            }
            else
            {
                if(empty($post['CustomReports']['referer_operator']))
                {
                    $post['CustomReports']['referer_operator'] = 0;
                }
                if(isset($post['CustomReports']['referer_not_operator']) && $post['CustomReports']['referer_not_operator'] == 1)
                {
                    $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['referer_operator']].'  `BI`.`travel_partner_id` NOT IN (';
                }
                else
                {
                    $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['referer_operator']].'  `BI`.`travel_partner_id` IN (';
                }
                
            }
            
            foreach ($post['CustomReports']['referers'] as $value) 
            {
                $where_clause = $where_clause.$value;
                $count++;
                if($count < count($post['CustomReports']['referers']))
                {
                    $where_clause = $where_clause.',';
                }
            }
            $where_clause = $where_clause.')';
        }

        if(isset($post['CustomReports']['conditions']) && !empty($post['CustomReports']['conditions']))
        {
            $count = 0;

            // echo "<pre>";
            // print_r($post['CustomReports']['condition_operator']);
            // print_r($post['CustomReports']['conditions']);
            // // exit();
            foreach ($post['CustomReports']['conditions'] as  $key => $value) 
            {
                // print_r($value);
                // if(!empty($value['value']) )
                if(isset($value['value'])  )
                {
                    // echo "in if";
                    if(empty($post['CustomReports']['condition_operator'][$key]))
                    {
                        $post['CustomReports']['condition_operator'][$key] = 0;
                    }
                    if($test == 0)
                    {
                        
                        // checking if condition is like
                        if($value['condition'] == 4)
                        {
                            $where_clause = $where_clause.' '.'`'.$alias[$columns[$value['cond_columns']]['table']].'`.`'.$columns[$value['cond_columns']]['db_col'].'` '.$operators[$value['condition']].' \'%'.$value['value'].'%\'';
                        }
                        else
                        {

                            $where_clause = $where_clause.' '.'`'.$alias[$columns[$value['cond_columns']]['table']].'`.`'.$columns[$value['cond_columns']]['db_col'].'` '.$operators[$value['condition']].' \''.$value['value'].'\'';
                        }

                        $test = 1;
                    }
                    else
                    {
                        if($value['condition'] == 4)
                        {
                            $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['condition_operator'][$key]].' `'.$alias[$columns[$value['cond_columns']]['table']].'`.`'.$columns[$value['cond_columns']]['db_col'].'` '.$operators[$value['condition']].' \'%'.$value['value'].'%\'';
                        }
                        else
                        {
                            $where_clause = $where_clause.' '.$conditions[$post['CustomReports']['condition_operator'][$key]].' `'.$alias[$columns[$value['cond_columns']]['table']].'`.`'.$columns[$value['cond_columns']]['db_col'].'` '.$operators[$value['condition']].' \''.$value['value'].'\'';
                        }
                        
                    }
                }

            }
            // exit();
        }

        if($test == 0)
        {
            $where_clause = $where_clause.'`BI`.`deleted_at` = 0';
            $test = 1;
        }
        else
        {
            $where_clause = $where_clause.' AND `BI`.`deleted_at` = 0';
        }

        // $index = array_search(16, $post['CustomReports']['columns']);
        // if($index !== false)
        // {

        //     if($test == 0)
        //     {
        //         $where_clause = $where_clause.'`BDF`.`type` = 1';
        //         $test = 1;
        //     }
        //     else
        //     {
        //         $where_clause = $where_clause.' AND `BDF`.`type` = 1';
        //     }
        // }
        

        return $where_clause;
    }

    protected function getOrderByStatementFromPost($post)
    {
        // $model = $this->findModel($id);
        // echo "<pre>";
        // print_r($post);
        // exit();
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();

        $order_by = '';
        if(isset($post['CustomReports']['group_by']))
        {
            if($post['CustomReports']['group_by'] == 0)
            {
                 $order_by = $order_by.'ORDER BY `BI`.`provider_id` ';
            }
            if($post['CustomReports']['group_by'] == 1)
            {
                $order_by = $order_by.'ORDER BY `BI`.`item_id` ';
            }
            if($post['CustomReports']['group_by'] == 2 || $post['CustomReports']['group_by'] == 3)
            {
                $order_by = $order_by.'ORDER BY `BIG`.`booking_group_id` ';
            }

        }

        if(isset($post['CustomReports']['sort_by']) && !empty($post['CustomReports']['sort_by']))
        {
            if(isset($post['CustomReports']['group_by']) && ($post['CustomReports']['group_by'] == 1 || $post['CustomReports']['group_by'] == 2 || $post['CustomReports']['group_by'] == 0 || $post['CustomReports']['group_by'] == 3) )
            {
                $order_by = $order_by.', `'.$alias[$columns[$post['CustomReports']['sort_by']]['table']].'`.`'.$columns[$post['CustomReports']['sort_by']]['db_col'].'`'.' ';  
            }
            else
            {
                $order_by = $order_by.'ORDER BY `'.$alias[$columns[$post['CustomReports']['sort_by']]['table']].'`.`'.$columns[$post['CustomReports']['sort_by']]['db_col'].'`'.' ';    
            }
            
        }
        // echo $order_by;
        // exit();
        return $order_by;
    }

    protected function getLimitStatementFromPost($post)
    {
        // $model = $this->findModel($id);
        // $columns = CustomReportColumns::getColumns();

        $limit = '';
        if(isset($post['CustomReports']['show_max']) && !empty($post['CustomReports']['show_max']))
        {
            $limit = ' LIMIT '.$post['CustomReports']['show_max'];
        }
        return $limit;
    }

    protected function checkTtables($post,$new_added_columns)
    {
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();

        $tables_arr = array();
        foreach ($post['CustomReports']['columns'] as $value) 
        {   
            $index = array_search($columns[$value]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value]['table']);               
            }
        }

        foreach ($new_added_columns as $value) 
        {
            $index = array_search($columns[$value]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value]['table']);               
            }
        }

        if(count($tables_arr) > 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function checkTtablesSaved($id,$new_added_columns)
    {
        $model = $this->findModel($id);
        $columns = CustomReportColumns::getColumns();
        $alias = CustomReportColumns::getAlias();

        $tables_arr = array();
        foreach ($model->customReportColumns as $value) 
        {   
            $index = array_search($columns[$value->column]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value->column]['table']);               
            }
        }

        foreach ($new_added_columns as $value) 
        {
            $index = array_search($columns[$value]['table'], $tables_arr);
            if ($index === false)
            {
                array_push($tables_arr, $columns[$value]['table']);               
            }
        }

        if(count($tables_arr) > 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * Finds the CustomReports model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomReports the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomReports::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\db\Migration;

class m170322_063220_alter_discount_codes extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `discount_code` CHANGE `provider_id` `provider_id` INT(11) NULL AFTER `name`, CHANGE `enterable_by_registered_users` `enterable_by_registered_users` VARCHAR(5) CHARSET utf8 COLLATE utf8_unicode_ci NULL, CHANGE `active` `active` VARCHAR(5) CHARSET utf8 COLLATE utf8_unicode_ci NULL, ADD COLUMN `linked_to_quantity` VARCHAR(5) NULL AFTER `active`; 
        ");
    }

    public function down()
    {
        echo "m170322_063220_alter_discount_codes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161019_070214_create_table_rates extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `rates`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `provider_id` INT(11), `unit_id` INT(11) COMMENT 'unit', `rate_period` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `unit_price_first_adult` VARCHAR(256), `unit_price_additional_adult` VARCHAR(256), `unit_price_first_child` VARCHAR(256), `unit_price_additional_child` VARCHAR(256), `first_night_rate` DATE, `last_night_rate` DATE, `min_booking` INT(11), `max_booking` INT(11), `rate_allowed` TEXT, `check_in_allowed` TEXT, `check_out_allowed` TEXT, `discount_code_id` INT(11), `booking_confirmation_type_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_rate_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_rate_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `bookable_items`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_rate_discount_code_id` FOREIGN KEY (`discount_code_id`) REFERENCES `discount_code`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_rate_booking_confirmation_id` FOREIGN KEY (`booking_confirmation_type_id`) REFERENCES `booking_confirmation_types`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

            ALTER TABLE `rates` DROP COLUMN `max_booking`, DROP COLUMN `discount_code_id`, CHANGE `booking_confirmation_type_id` `booking_confirmation_type_id` INT(11) NULL AFTER `provider_id`, CHANGE `unit_price_first_adult` `item_price_first_adult` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL, CHANGE `unit_price_additional_adult` `item_price_additional_adult` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL, CHANGE `unit_price_first_child` `item_price_first_child` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL, CHANGE `unit_price_additional_child` `item_price_additional_child` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL, CHANGE `min_booking` `min_max_booking` TEXT NULL, ADD COLUMN `min_max_stay` TEXT NULL AFTER `check_out_allowed`, ADD COLUMN `booking_status_id` INT(11) NULL AFTER `min_max_stay`, ADD COLUMN `booking_flag_id` INT(11) NULL AFTER `booking_status_id`, DROP INDEX `FK_rate_discount_code_id`, DROP FOREIGN KEY `FK_rate_discount_code_id`, ADD CONSTRAINT `FK_rate_booking_status_id` FOREIGN KEY (`booking_status_id`) REFERENCES `booking_statuses`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_rate_booking_flag_id` FOREIGN KEY (`booking_flag_id`) REFERENCES `booking_types`(`id`) ON DELETE SET NULL; 

            ALTER TABLE `rates` CHANGE `first_night_rate` `first_night_rate` INT(11) NULL COMMENT 'date', CHANGE `last_night_rate` `last_night_rate` INT(11) NULL COMMENT 'date'; 

        ");
    }

    public function down()
    {
        echo "m161019_070214_create_table_rates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

$(document).ready(function() {

    collapse($('#show_regions'), $('#regions_checklist'));
    collapse($('#show_destinations'), $('.provider_types'));
    collapse($('#show_amenities'), $('#amenities_checklist .form-group'));
    collapse($('#show_accommodation_types'), $('#accommodation_types_checklist .form-group'));

    //collapse($('#show_food_types'), $('#food_types_checklist .form-group'));

    $('#accommodation_types_checklist').hide();

    /* Amenities */
    var amenities_ids = '';
    var accommodation_type_ids = '';
    var food_type_ids = '';
    $('#search').on('click', function() {
        //replace amenities value with array of all values
        $.each($('.amenities:checked'), function(key, val) {
            if(amenities_ids.length == 0) {
                amenities_ids += $(this).val();
            } else {
                amenities_ids += ',' + $(this).val();
            }
        });
        $('#amenities').val(amenities_ids);

        //replace accommodation_type_ids value with array of all values
        $.each($('.accommodation_types:checked'), function(key, val) {
            if(accommodation_type_ids.length == 0) {
                accommodation_type_ids += $(this).val();
            } else {
                accommodation_type_ids += ',' + $(this).val();
            }
        });
        $('#accommodation_types').val(accommodation_type_ids);

        //replace food_type_ids value with array of all values
        $.each($('.food_types:checked'), function(key, val) {
            if(food_type_ids.length == 0) {
                food_type_ids += $(this).val();
            } else {
                food_type_ids += ',' + $(this).val();
            }
        });
        $('#food_types').val(food_type_ids);
    });

    $('#amenities_checklist').hide();
    $('#amenities_checklist .form-group').hide();
    $('.destinations').change(function () {
        if ($(this).prop('checked') && $(this).val() == 11) {
            $('#amenities_checklist').fadeIn(1000);
        } else {
            $('#amenities_checklist').fadeOut(1000);
        }
    });

    /* Destinations */
    // uncheck other destinations when one is checked
    $('.destinations').on('change', function() {
        var checked = false;
        if($(this).prop('checked')) {
            checked = true;
        }
        $('.destinations').prop('checked', false);
        $(this).prop('checked', checked);
    });

    function collapse(button, block) {
        block.hide();
        button.on('click', function() {
            if(button.hasClass('fa-plus')) {
                block.fadeIn(1000);
                button.removeClass('fa-plus').addClass('fa-minus');
            } else {
                block.fadeOut(500);
                button.removeClass('fa-minus').addClass('fa-plus');
            }
        });
    }

    var dateFormat = "mm/dd/yy",
        from = $("#date_from")
            .datepicker({
                minDate: "+1d",
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
                to.val(getFormattedDate(getDate(this)));
            }),
        to = $("#date_to").datepicker({
                minDate: "+2d",
            });

    function getDate(element) {
        var date;
        try {
            console.log(element.value);
            date = $.datepicker.parseDate(dateFormat, element.value);
            date = new Date(date.valueOf() + 1000 * 3600 * 24);
            console.log(date);
        } catch (error) {
            date = null;
        }

        return date;
    }
});

function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}

/* floating search button */
$(window).scroll(function() {
    var sb_m = 60;
    var mb = 0;
    var st = $(window).scrollTop();
    var sb = $(".sidebar");
    var sbi = $(".sidebar #search_up");
    var sb_ot = sb.offset().top;
    var sbi_ot = sbi.offset().top;
    var sb_h = sb.height();

    if(sb_h + $(document).scrollTop() + sb_m + mb < $(document).height()) {
        if(st > sb_ot) {
            var h = Math.round(st - sb_ot) + sb_m;
            sbi.css({"position" : "fixed"});
            sbi.css({"top" : 50});
        }
        else {
            sbi.css({"top" : 0});
            sbi.css({"position" : "unset"});
        }
    }
});
/* --------------------- */
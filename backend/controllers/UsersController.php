<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use common\models\UserProfile;
use common\models\UserProfileSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\Security;
/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    // [
                    //     'actions' => ['signup'],
                    //     'allow' => true,
                    //     'roles' => ['?'],
                    // ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'force-reset' ,'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['UserSearch']) && !$this->isArrayEmpty($params['UserSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_model = new User();
        $profile_model = new UserProfile();
        //$model->scenario = User::SCENARIO_CREATE;
        $post = Yii::$app->request->post();

        if ($user_model->load($post)){

            $password = Yii::$app->security->generateRandomString(12);
            $user_model->setPassword($password);
            $user_model->generateAuthKey();

            if($user_model->save()) 
            {
                if ($profile_model->load($post))
                {
                    $profile_model->user_id = $user_model->id;

                    if($profile_model->same_address==1)
                    {

                        $profile_model->business_address = $profile_model->address;
                        $profile_model->business_city_id = $profile_model->city_id;
                        $profile_model->business_state_id = $profile_model->state_id;
                        $profile_model->business_country_id = $profile_model->country_id;
                        $profile_model->business_postal_code = $profile_model->postal_code;
                        $profile_model->telephone_number = $user_model->server_phone;
                    }
                    $profile_model->telephone_number = $_POST['Users']['server_phone'];
                    
                    $profile_model->save();
                }
                //send email here
                Yii::$app->mailer
                ->compose(
                    ['html' => 'userRegister-html', 'text' => 'userRegister-text'],
                    ['user' => $user_model, 'password' => $password]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' Troll'])
                ->setTo($user_model->email)
                ->setSubject('Account created for ' . Yii::$app->name)
                ->send();
                return $this->redirect(['index']);
            } 
        } 
        return $this->render('create', [
            'user_model' => $user_model,
            'profile_model' => $profile_model,
        ]);
    }
    //$2y$13$oGXrnJDJUG1kkbRyOLXuiewUHJkohddUQJycejQPpDgm/RPwiW6JS

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //echo '<pre>';
        //print_r($_POST);
        //exit;
        $user_model = $this->findModel($id);
        
        if(!isset($user_model->profile)){
            $model = new UserProfile;
            $model->user_id = $user_model->id;
            $model->save();
        }
        
        $profile_model = $this->findProfile($id);
        $user_model->scenario = User::SCENARIO_UPDATE;
        $post = Yii::$app->request->post();

         if ($user_model->load($post)){

            if(isset($post['User']['password']) && !empty($post['User']['password']))
                $user_model->setPassword($post['User']['password']);

            if($user_model->save()) {

                if ($profile_model->load($post))
                {
                    if($profile_model->same_address==1)
                    {
                        $profile_model->business_address = $profile_model->address;
                        $profile_model->business_city_id = $profile_model->city_id;
                        $profile_model->business_state_id = $profile_model->state_id;
                        $profile_model->business_country_id = $profile_model->country_id;
                        $profile_model->business_postal_code = $profile_model->postal_code;
                    }
                    $profile_model->telephone_number = $_POST['Users']['server_phone'];

                    $profile_model->save();
                }
                Yii::$app->session->setFlash('success', 'User Profile is updated successfully');
                return $this->redirect(['index']);
            } 
        } 
        return $this->render('update', [
            'user_model' => $user_model,
            'profile_model' => $profile_model,
        ]);
    }

    /**
     * Force a user to update his/her password on next login
     * @param integer $id
     * @return mixed
     */
    public function actionForceReset($id)
    {
        $model = $this->findModel($id);
        $model->first_login_reset_flag = 0;

         if ($model->save()){
            return $this->redirect(['index']);
        } 
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(intval($id) != 1)
            $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findProfile($id)
    {
        if (($model = UserProfile::findOne(['user_id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\db\Migration;

class m161007_122915_create_table_booking_rules extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `booking_rules`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `request_booking_status` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `routine_booking_confirmation` INT(11), `nearterm_booking_confirmation` INT(11), `nearterm_booking_advance` INT(11), `ebp_booking_type` INT(11), `ebp_first_night_exception_period` DATE, `ebp_last_night_exception_period` DATE, `same_day_booking_cutoff` TIME, `general_booking_cancellation` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `new_booking_type` INT(11), `new_booking_status` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_booking_rules_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_rules_routine_booking_confirmation` FOREIGN KEY (`routine_booking_confirmation`) REFERENCES `booking_confirmation_types`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_rules_nearterm_booking_confirmation` FOREIGN KEY (`nearterm_booking_confirmation`) REFERENCES `booking_confirmation_types`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_rules_ebp_booking_type` FOREIGN KEY (`ebp_booking_type`) REFERENCES `booking_confirmation_types`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_rules_new_booking_type` FOREIGN KEY (`new_booking_type`) REFERENCES `booking_types`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_booking_rules_new_booking_status` FOREIGN KEY (`new_booking_status`) REFERENCES `booking_statuses`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161007_122915_create_table_booking_rules cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

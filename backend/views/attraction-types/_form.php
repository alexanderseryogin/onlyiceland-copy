<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\AttractionTypeFields;
use yii\widgets\Pjax;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
    var at_id='';
</script>

<div class="attraction-types-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">
                        
    					<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?php
                            if(!$model->isNewRecord && !empty($model->logo))
                            {
                                $path = Yii::getAlias('@web').'/../uploads/attraction-types/images/logo/'.$model->logo;

                                echo '<img src="'.$path.'" style="max-height:100px" >';
                            }
                        ?>

    					<?= $form->field($model, 'image')->fileInput() ?>

                        <div class="form-group" >
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id'=>'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            <a class="btn btn-default" href="<?= Url::to(['/attraction-types']) ?>" >Cancel</a>
                        </div>

                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#add').on('click',function()
    {
        if($('#attractiontypefields-label').val()=='')
        {
            alert('Label Cannot be Empty');
        }
        else
        {
            var type='text';
            var required = 1;

            var Object = {
                            at_id : at_id,
                            required : required,
                            type: type,
                            label: $('#attractiontypefields-label').val(),
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'add-custom-field',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#field-gridview'});  //Reload GridView
                },
            });
        }
        
    });

    $('a[name=\"delete[]\"]').click(function(e) 
    {
        alert('ok');
        var Object = {
                        data_id : $(this).attr('data-id'),
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'delete-field',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#field-gridview'});  //Reload GridView
            },
        });
    });

    $('#field-gridview').on('pjax:end', function() 
    {
        $('a[name=\"delete[]\"]').click(function(e) 
        {
            alert('Are you sure you want to delete this item?');
            var Object = {
                            data_id : $(this).attr('data-id'),
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'delete-field',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#field-gridview'});  //Reload GridView
                },
            });
        });

    });

    $('#submit_button').on('click',function(){

    });
    

});",View::POS_END);
?>


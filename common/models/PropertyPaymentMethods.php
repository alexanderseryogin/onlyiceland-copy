<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "property_payment_methods".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 */
class PropertyPaymentMethods extends \yii\db\ActiveRecord
{
    const TYPE_GUARANTEE = 0;
    const TYPE_CHECKIN = 1;
    const TYPE_GUARANTEE_CHECKIN = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_methods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}

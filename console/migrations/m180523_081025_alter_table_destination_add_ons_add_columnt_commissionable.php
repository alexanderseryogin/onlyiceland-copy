<?php

use yii\db\Migration;

class m180523_081025_alter_table_destination_add_ons_add_columnt_commissionable extends Migration
{
    public function up()
    {
        $this->addColumn('destination_add_ons', 'commissionable', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('destination_add_ons', 'commissionable');
    }
}

<?php

use yii\db\Migration;

class m180227_075036_alter_table_custom_report_conditions extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `custom_report_conditions`   
                          ADD COLUMN `operator` VARCHAR(256) NULL AFTER `value`;
                        ');
    }

    public function safeDown()
    {
        echo "m180227_075036_alter_table_custom_report_conditions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180227_075036_alter_table_custom_report_conditions cannot be reverted.\n";

        return false;
    }
    */
}

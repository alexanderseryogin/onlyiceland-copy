<?php
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Destinations;
use common\models\BookableItems;

$this->title = "Rates Scheduler";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];

$session = Yii::$app->session;
$session->open();
$params = '';

if(isset($session[Yii::$app->controller->id.'-booking-schedular-values']) && !empty($session[Yii::$app->controller->id.'-booking-schedular-values']))
{
    $params = json_decode($session[Yii::$app->controller->id.'-booking-schedular-values'], true);
    unset($session[Yii::$app->controller->id.'-booking-schedular-values']);
    //print_r($params);
}

?>

<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	#calendar {
		
		margin: 0 auto;
	}

</style>
<div class="row hide">
    <div class="col-md-4">
        <button id=test class="btn btn-success">Test</button>
    </div>
</div>
<div class="row calendar-div">
    <div class="col-md-12">
        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md" style="width: 100%;">
                    <div class="row">
                		<div class="col-sm-4">
	                        <label class="control-label">Destinations</label>
	                        <div class="form-group" >
	                            <select id="provider_dropdown" style="width: 100%;" class="form-control">
	                            	<option value=""></option>
	                                <?php
	                                    $destinations = ArrayHelper::map(Destinations::find()->where(['active' => 1])->all(),'id','name');

                                        if($destination_id != null )
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if($destination_id == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }
                                        }
                                        else
                                        {
                                            foreach ($destinations as $key => $value) 
                                            {
                                                

                                                if(!empty($params) && $params['provider_id'] == $key)
                                                   echo '<option value="'.$key.'" selected>'.$value.'</option>';
                                                else
                                                   echo '<option value="'.$key.'">'.$value.'</option>'; 
                                            }    
                                        }
	                                    
	                                ?>
	                            </select>
	                        </div>
	                    </div>
                        
                	</div>
                </div>
                <ul class="nav nav-tabs bookings_tabs">
                    <li class="active">
                        <!-- <a href="#general_info" data-toggle="tab">General info</a> -->
                    </li>
                </ul>
            </div>
            
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <center><div class="loader"></div></center><br>
                        	<div id="error_div" class="alert-danger alert fade in" style="display: none">
								No item found.
							</div>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
                <br>        
            </div>
        </div>
    </div>
</div>
<div id='calendar'></div>

<div class="modal fade rates_override_modal" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg ">
    
      <!-- Modal content-->
       <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Daily Price Management</h4>
            </div>
            <div class="modal-body">
                <div id="blockui_sample_3_1_element">
                <div class="row rate_inputs" style="padding-top: 15px;">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control input-sm input-small input-inline" type="" name="" id='rate_price'>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top: 10px;">
                    <div class="col-md-1" style="margin-left: 31px;">
                        <input type="checkbox" class="make-switch rates-switch" data-on-text="Percentage" data-off-text="Normal" data-size="small">
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <div class="form-group">
                            <!-- <label>%</label> -->
                            <input class="form-control input-sm input-small input-inline" type="number" id='rate_percentage' disabled min="1">
                        </div>
                    </div>


                    <div class="col-md-4 ">
                        <!-- <label class="control-label">Housekeeping Status</label> -->
                        <div class="button-group">
                            <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">Apply to <span class="caret"></span></button>
                            <ul class="dropdown-menu bookable_items">
                              
                            </ul>
                        </div>
                        <div class="form-group" >
                            
                        </div>
                    </div>
                    
                </div>

                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <h4><b>Set Date Range</b></h4>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-4">
                        <div class="form-group" >
                            <div class="input-group input-medium date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                <input type="text" class="form-control" id="rate_from" >
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" id="rate_to">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Mo</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='rate_mo' data-order="0" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Tu</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='tu' data-order="1" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>We</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='we' data-order="2" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Th</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='th' data-order="3" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Fr</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='fr' data-order="4" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Sa</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='sat' data-order="5" checked>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Su</label>
                            <input class="rate_checkboxes" type="checkbox" name="" id='su' data-order="6" checked>
                        </div>
                    </div>
                </div>

                <hr>
              <!-- <p>Some text in the modal.</p> -->
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <button type="button" class="btn green pull-left" id="rate_save">Save</button>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                    <div class="col-md-6 pull-right">
                        <button type="button" class="btn red pull-right" id="close_bookabele_item">Close Bookable Item</button>
                    </div>
                    
                </div>
            </div>
            </div>
        </div>
      
    </div>
</div>

<?php
// echo "<pre>";
// print_r($params);
// exit();

// else
// {
// 	echo "no params";
// 	exit();
// }
$get_rates = Url::to(['/rates/get-rates-schedular']);
$edit_or_drag_booked_item = Url::to(['/bookings/edit-or-drag-booked-item-schedular']);

$get_rate_events = Url::to(['/rates/generate-schedular-events']);

$get_rates_inputs = Url::to(['/rates/get-rates-inputs']);

$override_rate = Url::to(['/rates/override-rates']);

$close_bookabele_item = Url::to(['/rates/close-bookable-item']);

$open_bookabele_item = Url::to(['/rates/open-bookable-item']);

$get_resources = Url::to(['/rates/generate-schedular-resources']);


$get_rates_inputs_with_percentage = Url::to(['/rates/get-rates-inputs-with-percentage']);

$this->registerJs("
	$(document).ready(function(){
		$('#provider_dropdown').select2({
	        placeholder: \"Select a Destination\",
	        allowClear: true,
	    });
	
		$(document).on('change','#provider_dropdown',function()
	    {
	    	onChnageDropdown();
	    	
	    });

        // $('.bookable_items').select2({
        //     multiple: true,
        //     placeholder: 'Apply To',
        //     //width:'100%',
        //     //data: z 
        // });

        $(document).on('switchChange.bootstrapSwitch', '.rates-switch', function (event, state) {
            // console.log('total change');
            console.log('in change switch');
            console.log($(this).bootstrapSwitch('state'));
            if ($(this).bootstrapSwitch('state')) {     
                console.log('in if');
                $('#rate_percentage').attr('disabled',false);
                UpdateInputsPercentage($('#rate_percentage').val());
            }else {
                console.log('in else');
                $('#rate_percentage').attr('disabled','disabled');
                UpdateInputs();
            }
        });

        $(document).on('change','#rate_percentage',function(){
            console.log('percentage change');
            UpdateInputsPercentage($(this).val());
        });


        $(document).on('hide.bs.modal','.rates_override_modal', function () {
            console.log($(this).attr('class'));
            $('#rate_percentage').val('');
            $('.rates-switch').bootstrapSwitch('state', false);
            $('.bookable_items_checkboxes').each(function(){
                $(this).attr('checked',false);
            });
            $('.rate_checkboxes').each(function(){
                $(this).prop('checked',true);
            });
        });

        $(document).on('click','.select_all_bookable_items',function(){
            $('.bookable_items_checkboxes').each(function(){
                $(this).prop('checked',true);
            });
        });
        $(document).on('click', '.bookable_items ', function (e) {
            console.log('here');
          e.stopPropagation();
        });
        $(document).on('click','.deselect_all_bookable_items',function(){
            $('.bookable_items_checkboxes').each(function(){
                $(this).attr('checked',false);
            });
        });

        $(document).on('click','#rate_save',function(){
            console.log('save pressed');
            OverrideRate();
        })

        // closing bookable item //

        $(document).on('click','#close_bookabele_item',function(){
            console.log('close bookable item pressed');
            CloseBookableItem();
        });

        $(document).on('click','#open_bookable_item',function(){
            console.log('open bookable item pressed');
            OpenBookableItem();
        });

        $('#rate_from').datepicker().on('hide', function(e) {
            e.stopPropagation();
          });
        $('#rate_to').datepicker().on('hide', function(e) {
            e.stopPropagation();
          });
        $('#test').click(function(){
            // console.log($('#calendar').fullCalendar('getView'));
            // alert($('#calendar').fullCalendar('getDate'));
             var date = $('#calendar').fullCalendar('getDate');
            // var month_int = date.getMonth();
            console.log(date);
            console.log(date.format());
        });
	    
	});

    function OverrideRate()
    {
        App.blockUI({
                target: '#blockui_sample_3_1_element',
                overlayColor: 'none',
                animate: true
            });
        // App.startPageLoading({animate: true});
        var aChecked = $('input:checkbox:checked'); // assumes your checkboxes are in a form tag
        var aValues = [];
        $('.rate_checkboxes').each(function(){
            aValues[$(this).attr('data-order')] = $(this).is(':checked');
        });
        var cpricing_values = [];
        $('.cpricing').each(function(){
            cpricing_values.push($(this).val());
        });
        console.log(aValues);

        var bookable_items_checkboxes = [];
        $('.bookable_items_checkboxes').each(function(){
            if($(this).is(':checked'))
            {
                bookable_items_checkboxes.push($(this).val());
            }
            
        });

        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'rate_id' : $('#rate').attr('data-rate'),
                        'date' : $('#rate_date').val(),
                        'rate_from' : $('#rate_from').val(),
                        'rate_to' : $('#rate_to').val(),
                        'override_type' : $('.rates-switch').bootstrapSwitch('state'),
                        'percentage' : $('#rate_percentage').val(),
                        'bookable_items' : bookable_items_checkboxes,
                        'rate_days' : aValues,
                        'cpricing' : cpricing_values
                    };
        $.ajax({
            async: false,
            url: '$override_rate',
            data: object,
            type: 'POST',
            success: function (data) {
                 App.unblockUI('#blockui_sample_3_1_element');
                data = jQuery.parseJSON( data );

                // console.log('date : '+".$date." );
                // var original_date =  '".$date."';
                original_date = data.date;
                var date_arr = original_date.split('-');

                var month = date_arr[2]-1;
                var scroll = parseInt(month)*190;

                console.log(data);
                // $('.rate_inputs').empty().html(data.rate_inputs)
                $('#myModal').modal('toggle');
                // $('#calendar').fullCalendar( 'destroy');
                initFullCalendar(data.resources,data.events,data.date,scroll,original_date,data.closed_dates);
                
                
            },
            error: function () {
                alert('could not get the data');
            },
        });
        
    }

    function CloseBookableItem()
    {
        // alert('in close');

        App.blockUI({
                target: '#blockui_sample_3_1_element',
                overlayColor: 'none',
                animate: true
            });
        // App.startPageLoading({animate: true});
        var aChecked = $('input:checkbox:checked'); // assumes your checkboxes are in a form tag
        var aValues = [];
        $('.rate_checkboxes').each(function(){
            aValues[$(this).attr('data-order')] = $(this).is(':checked');
        });
        var cpricing_values = [];
        $('.cpricing').each(function(){
            cpricing_values.push($(this).val());
        });
        console.log(aValues);

        var bookable_items_checkboxes = [];
        $('.bookable_items_checkboxes').each(function(){
            if($(this).is(':checked'))
            {
                bookable_items_checkboxes.push($(this).val());
            }
            
        });

        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'rate_id' : $('#rate').attr('data-rate'),
                        'date' : $('#rate_date').val(),
                        'rate_from' : $('#rate_from').val(),
                        'rate_to' : $('#rate_to').val(),
                        'override_type' : $('.rates-switch').bootstrapSwitch('state'),
                        'percentage' : $('#rate_percentage').val(),
                        'bookable_items' : bookable_items_checkboxes,
                        'rate_days' : aValues,
                        'cpricing' : cpricing_values
                    };
        $.ajax({
            async: false,
            url: '$close_bookabele_item',
            data: object,
            type: 'POST',
            success: function (data) {
                 App.unblockUI('#blockui_sample_3_1_element');
                data = jQuery.parseJSON( data );

                // console.log('date : '+".$date." );
                // var original_date =  '".$date."';
                original_date = data.date;
                var date_arr = original_date.split('-');

                var month = date_arr[2]-1;
                var scroll = parseInt(month)*190;

                console.log(data);
                // $('.rate_inputs').empty().html(data.rate_inputs)
                $('#myModal').modal('toggle');
                // $('#calendar').fullCalendar( 'destroy');
                initFullCalendar(data.resources,data.events,data.date,scroll,original_date,data.closed_dates);
                
                
            },
            error: function () {
                alert('could not get the data');
            },
        });
        
    }

    function OpenBookableItem()
    {
        // alert('in open');
        App.blockUI({
                target: '#blockui_sample_3_1_element',
                overlayColor: 'none',
                animate: true
            });
        // App.startPageLoading({animate: true});
        var aChecked = $('input:checkbox:checked'); // assumes your checkboxes are in a form tag
        var aValues = [];
        $('.rate_checkboxes').each(function(){
            aValues[$(this).attr('data-order')] = $(this).is(':checked');
        });
        var cpricing_values = [];
        $('.cpricing').each(function(){
            cpricing_values.push($(this).val());
        });
        console.log(aValues);

        var bookable_items_checkboxes = [];
        $('.bookable_items_checkboxes').each(function(){
            if($(this).is(':checked'))
            {
                bookable_items_checkboxes.push($(this).val());
            }
            
        });

        var object = {
                        'destination_id' : $('#provider_dropdown').val(),
                        'rate_id' : $('#rate').attr('data-rate'),
                        'date' : $('#rate_date').val(),
                        'rate_from' : $('#rate_from').val(),
                        'rate_to' : $('#rate_to').val(),
                        'override_type' : $('.rates-switch').bootstrapSwitch('state'),
                        'percentage' : $('#rate_percentage').val(),
                        'bookable_items' : bookable_items_checkboxes,
                        'rate_days' : aValues,
                        'cpricing' : cpricing_values
                    };
        $.ajax({
            async: false,
            url: '$open_bookabele_item',
            data: object,
            type: 'POST',
            success: function (data) {
                 App.unblockUI('#blockui_sample_3_1_element');
                data = jQuery.parseJSON( data );

                // console.log('date : '+".$date." );
                // var original_date =  '".$date."';
                original_date = data.date;
                var date_arr = original_date.split('-');

                var month = date_arr[2]-1;
                var scroll = parseInt(month)*190;

                console.log(data);
                // $('.rate_inputs').empty().html(data.rate_inputs)
                $('#myModal').modal('toggle');
                // $('#calendar').fullCalendar( 'destroy');
                initFullCalendar(data.resources,data.events,data.date,scroll,original_date,data.closed_dates);
                
                
            },
            error: function () {
                alert('could not get the data');
            },
        });
        
    }

	function onChnageDropdown()
    {

    	App.blockUI({target:\".loader\", animate: !0});

        var object = {
                    	'destination_id' : $('#provider_dropdown').val(),
                    };

        $.ajax(
        {
            type: 'POST',
            url: '$get_rates',
            data: object,
            success: function(data)
            {
                data = jQuery.parseJSON( data );
                //alert(data.closed_dates);
                if(data.empty)
                {
                   	$('#error_div').css('display','block');
                   	$('#calendar').hide();
                    //$('#item_dropdown').empty().html('');
                    App.unblockUI(\".loader\");
                }
                else
                {
                	console.log('date : '+".$date." );
                    var original_date =  '".$date."';
                    original_date =original_date.toString();
                    var date_arr = original_date.split('-');

                    var month = date_arr[2]-1;
                	var scroll = parseInt(month)*190;
                	console.log('scroll : '+ scroll);

                   	$('#error_div').css('display','none');
                   	$('#calendar').show();
                    $('.bookable_items').empty().html(data.bookable_items);
                    App.unblockUI(\".loader\");
                   	// console.log(jQuery.parseJSON(data.bookings_items));

                   	console.log(jQuery.parseJSON(data.resources));
                   	// console.log(jQuery.parseJSON(data.notifications));
                    // alert('here');
                    // console.log('after scroll');
                   	initFullCalendar(jQuery.parseJSON(data.resources),jQuery.parseJSON(data.events),data.date,scroll,original_date,data.closed_dates);

       //             	$('.fc-scroller').animate({
					  //   scrollLeft: '+='+scroll+'px'
					  // }, 'slow');
                }
            },
            error: function()
            {
                alert('error');
            }
        });
    }

    function initFullCalendar(resources,events,date,scroll,original_date,closed_dates)
    {
        console.log('here in init cal');
        // console.log(events);
    	$('#calendar').fullCalendar( 'destroy' );

    	$('#calendar').fullCalendar({
			schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
			height: 700,
			//now: '2017-09-27',
			editable: false,
			aspectRatio: 1.8,
			scrollTime: '00:00',
			slotWidth: 200,
			// filterResourcesWithEvents: true, 
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'timelineWeek,timelineThreeDays,timelineMonth'
			},
			defaultView: 'timelineMonth',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 14 },        
                    slotDuration: {days: 1},
				},
                timelineWeek: {
                    type: 'timeline',
                    duration: { days: 7 },        
                    slotDuration: {days: 1},
                },


			},
			groupByResource: true,
			resourceAreaWidth: '30%',
			resourceColumns: [
				{
					group: true,
					labelText: 'Bookable Item',
					field: 'item',
					width: 155,
				},
				{
					// group: true,
					labelText: 'Rates',
					field: 'title',
					width: 75,
				},
				// {
				// 	labelText: 'Occupancy',
				// 	field: 'occupancy'
				// }
			],
			resourceGroupField: 'id',
			// resources: resources,
            refetchResourcesOnNavigate : true,
            resources: function(callback) {
                App.blockUI({target:\".loader\", animate: !0});
                setTimeout(function() { // necessary hack
                    var view = $('#calendar').fullCalendar('getView'); // TODO: update to your selector
                    $.ajax({
                        url: '$get_resources',
                        type: 'POST',
                        cache: false,
                        data: {
                            'destination_id' : $('#provider_dropdown').val(),
                            start: view.start.format(),
                            end: view.end.format(),
                            timezone: view.options.timezone
                        },
                        success: function (data) {
                            data = jQuery.parseJSON( data );
                            console.log('data');
                            App.unblockUI(\".loader\");
                            callback(data.resources);
                        },
                        error: function () {
                            alert('could not get the data');
                        },
                    })
                }, 0);
            },
			events: [],
            // filterResourcesWithEvents : true,
			eventResizeStart: function (event, jsEvent, ui, view) 
            {
                before_start = event.start.format();
                before_end = event.end.format();
                //console.log('RESIZE Before!! ' + before_start + ' to '+ before_end);
            },
            eventResize: function(event, delta, revertFunc) 
            {
                console.log('RESIZE After!! ' + event.start.format() + ' to '+ event.end.format());
        
                editOrDragBookingItem(event,revertFunc);
            },
            eventDrop: function(event, delta, revertFunc) 
            {
                editOrDragBookingItem(event,revertFunc);
            },
            dayRender: function (date, cell) {

                var d = new Date(date);
                
                console.log('in day render');
                console.log()

                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();
                //console.log(typeof(notifications));
                // console.log('curr_date'+curr_date + 'curr_month' + curr_month + 'curr_year' + curr_year);
                
                // for (var i = 0; i < closed_dates.length; i++) 
                // {
                //     var new_c_date = new Date(closed_dates[i]);
                //     var new_date = new_c_date.getDate();
                //     var new_month = new_c_date.getMonth();
                //     var new_year = new_c_date.getFullYear();

                //     if(curr_date == new_date && curr_month == new_month && curr_year == new_year) 
                //     {
                //         console.log('exception_dates');
                //         cell.css('background-color','red');
                        
                        
                //         cell.prepend(`<span style='font-size:15px;margin-left:20px;'><b>Closed for new bookings</b></span>`);
                //         // cell.prepend(`<a class='add_event_label' style='position:relative;top:0 ;left:0;display: block;font-size:12px;color:#000;cursor:pointer;'>(+)Add Event</a>`);                    
                //     }
                //     // else
                //     // {
                //     //     cell.prepend(`<a class='remove_event_label' style='position:relative;top:0;left:0;display: block;font-size:12px;color:#000;cursor:pointer;'>(-)Remove Event</a>`);
                //     // }
                // }
                
            },
            eventClick: function(calEvent, jsEvent, view) {
                console.log(calEvent);
                console.log(calEvent.resourceId);
                // var resource = $('#calendar').fullCalendar( 'getEventResource', event );
                // console.log()
                // console.log(resource);
                var object = {
                                    'rate_id' : calEvent.resourceId,
                                    'date' : calEvent.start._i,
                                    'bookable_item_id' : calEvent.bookable_item_id
                                };

                $.ajax({
                    async: false,
                    url: '$get_rates_inputs',
                    data: object,
                    type: 'POST',
                    success: function (data) {
                        data = jQuery.parseJSON( data );
                        if(data.is_item_closed == 1)
                        {
                            // alert('item closed');
                            $('#close_bookabele_item').removeClass('red');
                            $('#close_bookabele_item').addClass('yellow');

                            $('#close_bookabele_item').html('Open Bookable Item');
                            $('#close_bookabele_item').attr('id','open_bookable_item');

                        }
                        else
                        {
                            $('#open_bookable_item').removeClass('yellow');
                            $('#open_bookable_item').addClass('red');

                            $('#open_bookable_item').html('Close Bookable Item');
                            $('#open_bookable_item').attr('id','close_bookabele_item');
                        }

                        console.log(calEvent.start._i); 
                        console.log(data);
                        $('.rate_inputs').empty().html(data.rate_inputs)
                        $('#myModal').modal('show'); 

                        $('#rate_from').datepicker('setDate', new Date(calEvent.start._i) );
                        $('#rate_to').datepicker('setDate', new Date(calEvent.start._i) );

                        $('#rate_from').datepicker({
                                                      minDate: new Date(calEvent.start._i)
                                                    } );
                        $('#rate_to').datepicker({
                                                      minDate: new Date(calEvent.start._i)
                                                    } );

                        $('.bookable_items_checkboxes').each(function(){
                            // console.log('resource : '+calEvent.bookable_item_id);
                            var current_input = $(this);
                            if(current_input.val() == calEvent.bookable_item_id)
                            {
                                console.log('equal');
                                console.log('bookable item : '+$(this).val() );
                                console.log('resource : '+calEvent.bookable_item_id );
                                current_input.prop('checked',true);
                            }
                            
                        });
                        
                    },
                    error: function () {
                        alert('could not get the data');
                    },
                });


                

            },
            viewRender: function (view, element) {
                

                // ajax call for gettting events //
                var date = $('#calendar').fullCalendar('getDate');
                console.log(element);
                console.log(view.intervalStart.format('YYYY-MM-DD'));
                console.log(view.intervalEnd.format('YYYY-MM-DD'));
                // console.log($('#calendar').fullCalendar('getView').visEnd);

                var object = {
                                    'destination_id' : $('#provider_dropdown').val(),
                                    'date' : date.format(),
                                    'start' : view.intervalStart.format('YYYY-MM-DD'),
                                    'end' : view.intervalEnd.format('YYYY-MM-DD'),
                                };

                    $.ajax({
                        async: false,
                        url: '$get_rate_events',
                        data: object,
                        type: 'POST',
                        success: function (data) {
                            data = jQuery.parseJSON( data );
                            console.log('data');
                            console.log(data);
                            $('#calendar').fullCalendar('removeEvents');
                            $('#calendar').fullCalendar('addEventSource',data.events);
                            // $('#calendar').fullCalendar( 'refetchResources' );
                            // $('#calendar').fullCalendar('rerenderEvents');
                                
                            // console.log(data);
                        },
                        error: function () {
                            alert('could not get the data');
                        },
                    });
            },

            eventRender: function(event, element) { 
                // element.find('.fc-title').append('<br/>' + event.title); 
                // console.log(event);
                if(event.backgroundColor !== 'undefined')
                {
                    console.log('asdfasdfasdfasdfasdf');
                    console.log(event.backgroundColor);
                }
                // element.qtip({
                //   content: event.background_color
                // });
            }, 
            resourceRender: function(resourceObj, labelTds, bodyTds) {
            	//console.log('res obj : '+resourceObj.background_color);
            	//typeof(resourceObj.background_color);
            	if (typeof(resourceObj.background_color) === 'undefined') 
            	{
				   
				}
            	else
            	{
                   // console.log(labelTds);
            		labelTds.parent().css('background', resourceObj.background_color);
            		labelTds.css('background', resourceObj.background_color);
                    labelTds.children().children().css('color', resourceObj.text_color);
                    labelTds.parent().css('color', resourceObj.text_color);
            	}
			    
			}
		});
        $('#calendar').fullCalendar('gotoDate',original_date);
		$('.fc-scroller').animate({
	    	scrollLeft: '+='+scroll+'px'
	  	}, 'slow');
    }

    function editOrDragBookingItem(event,revertFunc)
    {
    	var resource = $('#calendar').fullCalendar( 'getEventResource', event );
    	console.log(resource);
    	//console.log($('#calendar').fullCalendar( 'getEventResource', event ))
        // if(confirm('Are you sure about this change?')) 
        // {
            var object = 
                {
                    'id': event.id,
                    'bookable_item_id':resource.bookable_item_id,
                    'item_name_id': resource.id,
                    'arrival_date' : event.start.format(),
                    'departure_date' : event.end.format(),

                };

            $.ajax(
            {
                type: 'POST',
                data: object,
                url: '$edit_or_drag_booked_item',
                success: function(data)
                {

                    if(data=='false')
                    {
                        //alert('This rate is not available '+event.start.format() +' - '+event.end.format());
                        alert('The rate is not available');
                        revertFunc();
                    }
                    else if(data == 'date_exception')
                    {
                        alert('you cannot book on this day');
                        revertFunc();
                    }
                    else
                    {
                    	data = jQuery.parseJSON(data);
                    	console.log(data.notifications);
                    	
                    	console.log('in else');

                    	// console.log('scroll date : '+data.first_date);
                    	// var scroll = parseInt(data.first_date)*240;
                    	// console.log('scroll : '+scroll);

                        original_date = (data.first_date).toString();
                        var date_arr = original_date.split('-');

                        var month = date_arr[2]-1;
                        var scroll = parseInt(month)*240;
                        console.log('scroll : '+ scroll);

                    	initFullCalendar(data.bookings_items,data.resources,data.date,data.closed_dates,data.notifications,scroll,original_date);

                    	//var noTime = $.fullCalendar.moment('2017-09-27');
                    	//$('#calendar').fullCalendar('gotoDate',noTime);
                    	//$('#calendar').fullCalendar('today');
                    	
                    }
                },
                error: function()
                {
                    alert('error');
                }
            });
       // }
        // else
        // {
        //     revertFunc();
        // }
    }

    // function getCalData() {

    //     var source = [{}];
    //     var result;
    //     var date = $('#calendar').fullCalendar('getDate');
    //     console.log(date);
    //     console.log(date.format());

    //     var object = {
    //                         'destination_id' : $('#provider_dropdown').val(),
    //                         'date' : date.format(),
    //                     };

    //     $.ajax({
    //         // async: false,
    //         url: '$get_rate_events',
    //         data: object,
    //         type: 'POST',
    //         success: function (data) {
    //             data = jQuery.parseJSON( data );
    //             console.log('data');
    //             console.log(data.events);
    //             result = data.events;
                    
    //             // console.log(data);
    //         },
    //         error: function () {
    //             alert('could not get the data');
    //         },
    //     });
    //     return result;
    // }

        function UpdateInputsPercentage(percentage)
        {
            var object = {
                                    'rate_id' : $('#rate').attr('data-rate'),
                                    'date' : $('#rate_date').val(),
                                    'percentage' : percentage,
                                };
            if(percentage != '' && percentage >0 )
            {
                $.ajax({
                    async: false,
                    url: '$get_rates_inputs_with_percentage',
                    data: object,
                    type: 'POST',
                    success: function (data) {
                        data = jQuery.parseJSON( data );

                        console.log(data);
                        $('.rate_inputs').empty().html(data.rate_inputs)
                        
                        
                    },
                    error: function () {
                        alert('could not get the data');
                    },
                });
            }
            else
            {
                // alert('percentage not correct');
            }
                
        }

        function UpdateInputs()
        {
            var object = {
                                    'rate_id' : $('#rate').attr('data-rate'),
                                    'date' : $('#rate_date').val(),
                                };

            $.ajax({
                async: false,
                url: '$get_rates_inputs',
                data: object,
                type: 'POST',
                success: function (data) {
                    data = jQuery.parseJSON( data );

                    console.log(data);
                    $('.rate_inputs').empty().html(data.rate_inputs)

                },
                error: function () {
                    alert('could not get the data');
                },
            });
        }

        
");

if($destination_id != null)
{
	$this->registerJs("
	    	$(document).ready(function(){
	    	
	    		onChnageDropdown();
	    	});
	    	
	    	//$('#provider_dropdown').trigger('change');
	    	//$('#provider_dropdown').change();
	    ");
}

else
{
	if(!empty($params) && isset($params['provider_id']) )
	{
	    $this->registerJs("
	    	$(document).ready(function(){
	    		console.log('param exists'+".$params['provider_id'].");
	    		onChnageDropdown();
	    	});
	    	
	    	//$('#provider_dropdown').trigger('change');
	    	//$('#provider_dropdown').change();
	    ");
	}
}
$this->registerCss("
	#calendar {
		max-height: 1000px;
		margin: 50px auto;
	}
    .fc-timeline-event {
        position: absolute;
        border-radius: 0;
        padding: 2px 0;
        margin-bottom: 1px;
        height: 110%;
        background-color: white !important;
        border-color: #ddd !important;
        border-bottom: none !important;
        border-right: none !important;
        border-left: none !important;
        
    }
     .fc-timeline-event .fc-content  {
            margin-top: 9px;
            text-align :right;
            // cursor: context-menu;
            cursor: pointer;
     }
     // displaying rates resources in 2 lines
    //  .fc-timeline td, .fc-timeline th {
    //     white-space: normal !important;
    // }

	");


?>
<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CustomReports;

/**
 * CustomReportsSearch represents the model behind the search form about `common\models\CustomReports`.
 */
class CustomReportsSearch extends CustomReports
{
    /**
     * @inheritdoc
     */
    public $destinations;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['report_title', 'report_color', 'report_icon', 'description', 'arrival_date_to', 'arrival_date_from', 'departure_date_to', 'departure_date_from', 'booking_date_to', 'booking_date_from', 'sort_by', 'show_max','destinations'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomReports::find()->distinct();
        $query->joinwith('customReportDestinations');
        $query->joinwith('customReportDestinations.destination');

        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'arrival_date_to' => $this->arrival_date_to,
            'arrival_date_from' => $this->arrival_date_from,
            'departure_date_to' => $this->departure_date_to,
            'departure_date_from' => $this->departure_date_from,
            'booking_date_to' => $this->booking_date_to,
            'booking_date_from' => $this->booking_date_from,
        ]);

        // if(!empty($params['CustomReportsSearch']['destinations']))
        // {
        //     // echo "<pre>";
        //     // print_r($params['CustomReportsSearch']['destinations']);
        //     // exit();
        //      $query->andFilterWhere([
        //         'in', 'providers.id' ,$params['CustomReportsSearch']['destinations']
        //     ]); 
        // }

        $query->andFilterWhere(['like', 'report_title', $this->report_title])
            ->andFilterWhere(['like', 'report_color', $this->report_color])
            ->andFilterWhere(['like', 'report_icon', $this->report_icon])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'sort_by', $this->sort_by])
            ->andFilterWhere(['like', 'show_max', $this->show_max])
            ->andFilterWhere(['like', 'providers.name', $this->destinations]);


        return $dataProvider;
    }
}

<?php

use yii\db\Migration;

class m161017_125748_insert_into_discount_voucher_types extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `discount_code_types` (`name`) 
        VALUES ('Percent off Everything'),
               ('Percent off Unit Only'),
               ('Percent off All Except Unit'),
               ('Fixed Amount off Each Unit'),
               ('Fixed Amount off Everything')
        ;");
    }

    public function down()
    {
        echo "m161017_125748_insert_into_discount_voucher_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_group_special_requests".
 *
 * @property string $id
 * @property string $booking_group_id
 * @property integer $special_request_id
 *
 * @property BookingGroup $bookingGroup
 * @property SpecialRequests $specialRequest
 */
class BookingGroupSpecialRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_group_special_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_group_id', 'special_request_id'], 'integer'],
            [['booking_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingGroup::className(), 'targetAttribute' => ['booking_group_id' => 'id']],
            [['special_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpecialRequests::className(), 'targetAttribute' => ['special_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'booking_group_id' => Yii::t('app', 'Booking Group ID'),
            'special_request_id' => Yii::t('app', 'Special Request ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingGroup()
    {
        return $this->hasOne(BookingGroup::className(), ['id' => 'booking_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialRequest()
    {
        return $this->hasOne(SpecialRequests::className(), ['id' => 'special_request_id']);
    }
}

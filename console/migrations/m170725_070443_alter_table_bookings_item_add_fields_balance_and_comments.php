<?php

use yii\db\Migration;

class m170725_070443_alter_table_bookings_item_add_fields_balance_and_comments extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `comments` LONGTEXT NULL AFTER `pricing_type`,
                          ADD COLUMN `balance` DOUBLE NULL AFTER `comments`;
                        ');
    }

    public function down()
    {
        echo "m170725_070443_alter_table_bookings_item_add_fields_balance_and_comments cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

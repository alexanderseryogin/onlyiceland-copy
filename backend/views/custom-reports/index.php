<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Destinations;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CustomReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Custom Reports';
$this->params['breadcrumbs'][] = $this->title;
$update_page_url = Url::to(['/custom-reports/update']);
?>

<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>


<div class="standard-report-top" id="standard-reports-top">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Custom Reports</span>

                    </div>
                    <ul class="nav nav-tabs standard-report-top">
                        <?= Html::a('Create Custom Reports', ['create'], ['class' => 'btn btn-primary']) ?>
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body destination_portlet">
                        <div class="tab-content">
                            <div class="tab-pane active" id="">
                                <div class="tiles hide">

                                    <div class="tile bg-green" >
                                        <div class="tile-body">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Test Report </div>
                                            <div class="number"> 
                                               <a href="javascript:voic(0)"><i class="fa fa-pencil"></i> </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tile bg-red-sunglo" >
                                        <div class="tile-body">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Test Report 2 </div>
                                            <div class="number">
                                                <a href="javascript:voic(0)"><i class="fa fa-pencil"></i> </a>
                                            </div>
                                            
                                            <!-- <div class="number"> 12 </div> -->
                                        </div>
                                    </div>

                                    <div class="tile bg-blue" >
                                        <div class="tile-body">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="tile-object">
                                            <div class="name"> Test Report 3 </div>
                                            <div class="number"> 
                                                <a href="javascript:voic(0)"><i class="fa fa-pencil"></i> </a>
                                            </div>
                                            
                                            <!-- <div class="number"> 12 </div> -->
                                        </div>
                                    </div>

                                </div>

                                <?php Pjax::begin(['id' => 'custom_reports-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    <?= GridView::widget([
                                    'id' => 'custom_reports-gridview',
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    // 'pjax' => true,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'id',
                                        [
                                            'label' => 'Destinations',
                                            'attribute' => 'destinations',
                                            'value' => function($data)
                                            {
                                                $destinations = '';
                                                $count = 1;
                                                foreach ($data->customReportDestinations as $value) 
                                                {
                                                    $destinations.=$value->destination->name;
                                                    if($count != count($data->customReportDestinations))
                                                    {
                                                        $destinations.=',';
                                                    }
                                                    
                                                    $count++;
                                                }
                                                return $destinations;
                                            },
                                            // 'filter' => Html::dropDownList(
                                            // 'CustomReportsSearch[destinations]', 
                                            // $searchModel['destinations'], 
                                            // ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                                            // [
                                            //     'prompt' => 'Select Destination', 
                                            //     // 'data-placeholder' => 'Select Destination',
                                            //     'id'=>'provider_dropdown',
                                            //     'style' => 'width:100%',
                                            // ]),
                                            // 'filter' => Select2::widget([
                                            //         // 'model' => ,
                                            //         'name' => 'CustomReportsSearch[destinations]',
                                            //         'value' => $searchModel->destinations,
                                            //         'data' => ArrayHelper::map(Destinations::find()->all(),'id','name'),
                                            //         // 'theme' => Select2::THEME_BOOTSTRAP,
                                            //         'theme' => 'krajee',
                                            //         // 'hideSearch' => true,
                                            //         // 'initValueText' => $searchModel->destinations,
                                            //         'options' => [
                                            //                 'placeholder' => 'Select Destination',
                                            //                 'multiple' => true,
                                            //         ],
                                            //         'pluginOptions' => [
                                            //             'allowClear' => true,
                                            //             // 'closeOnSelect' => true
                                            //         ]
                                            // ]),
                                            // 'filterInputOptions' => ['placeholder' => 'Select Destination',]
                                            // 'contentOptions' => function($data)
                                            // {
                                            //     return ['style' => 'background:'.$data->report_color.';' ];
                                            // },
                                        ],
                                        'report_title',
                                        'description:ntext',
                                        [
                                            'attribute' => 'report_color',
                                            'value' => function($data)
                                            {
                                                return $data->report_color;
                                            },
                                            'contentOptions' => function($data)
                                            {
                                                return ['style' => 'background:'.$data->report_color.'; width:10%;' ];
                                            },
                                        ],
                                        
                                        // [
                                        //     'format' =>'raw',
                                        //     'label' => 'Execute Report',
                                        //     // 'attribute' => 'destinations',
                                        //     'value' => function($data)
                                        //     {
                                        //         // $destinations = '';
                                        //         // $count = 1;
                                        //         // foreach ($data->customReportDestinations as $value) 
                                        //         // {
                                        //         //     $destinations.=$value->destination->name;
                                        //         //     if($count != count($data->customReportDestinations))
                                        //         //     {
                                        //         //         $destinations.=',';
                                        //         //     }
                                                    
                                        //         //     $count++;
                                        //         // }
                                        //         return '<a href=javascript:void(0);><icon class="fa fa-play"></icon> Execute Report </a>';
                                        //     },
                                        //     // 'contentOptions' => function($data)
                                        //     // {
                                        //     //     return ['style' => 'background:'.$data->report_color.';' ];
                                        //     // },
                                        // ],
                                        // 'report_color',
                                        // 'report_icon',
                                        
                                        
                                        // 'arrival_date_to',
                                        // 'arrival_date_from',
                                        // 'departure_date_to',
                                        // 'departure_date_from',
                                        // 'booking_date_to',
                                        // 'booking_date_from',
                                        // 'sort_by',
                                        // 'show_max',
                                        // 'statuses_operator',
                                        // 'flags_operator',
                                        // 'referers_operator',

                                        // ['class' => 'yii\grid\ActionColumn'],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            //'headerOptions' =>['style' => 'width:70px;'],
                                            'contentOptions' =>['style' => 'width:4%;'],
                                            'template' => '{update}{delete}',

                                            'buttons' => [
                                                // 'update' => function ($url, $model) {
                                                //     return Html::a('<span class="glyphicon glyphicon-pencil" style="margin-left:5px;"></span>', Url::to(['update', 'id'=>$model->id]), 
                                                //     [
                                                //         'class'   => 'updateBookings',
                                                //         'title' => 'Update',
                                                //         'aria-label' => 'Update',
                                                //         'data-pjax' => '0',
                                                //         'data-toggle' => 'tooltip'
                                                //     ]);
                                                // },
                                                // 'delete' => function ($url, $model) {
                                                //     return Html::a('<span class="glyphicon glyphicon-trash" style="margin-left:5px;"></span>', Url::to(['delete', 'id'=>$model->id]), 
                                                //     [   
                                                //         'class'   => 'deleteBookings',
                                                //         'title' => 'Delete',
                                                //         'aria-label' => 'Delete',
                                                //         'data-pjax' => '0',
                                                //         'data-toggle' => 'tooltip'
                                                //     ]);
                                                // },
                                                'execute' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-play print" style="margin-left:5px;z-index:20;"></span>', Url::to(['execute-report', 'id'=>$model->id]), 
                                                    [
                                                        'title' => 'Execute Report',
                                                        'aria-label' => 'execute-reports',
                                                        'data-pjax' => '0',
                                                        'target' => '_blank',
                                                        'data-toggle' => 'tooltip',
                                                        'class' => 'cancel',
                                                        'data-key' => $model->id
                                                    ]);
                                                },
                                            ],
                                            /*'visibleButtons' => [
                                                // show update button for item who is master
                                                'update' => function ($model, $key, $index) {
                                                    return $model->master == 1 ? true : false;
                                                },
                                            ]*/
                                        ],
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>   
               </div>
           </div>
    </div>
</div>

<?php

$this->registerJs("
        $('[data-toggle=\'tooltip\']').tooltip();
        // $('#provider_dropdown').select2({
        //     allowClear: true,
        //     multiple :true
        // });
        jQuery(document).on('pjax:success', '#custom_reports-gridview',  function(event){
            console.log('in pjax success');
            // $('#provider_dropdown').select2({
            //     placeholder: \"Select a Destination\",
            //     allowClear: true,
            //     multiple :true
            // });
        });
        $(document).on('click','.table-striped tbody tr td',function()
        {
            // console.log($(this).parent().attr('data-key'));
            // window.location.replace('$update_page_url?id='+$(this).parent().attr('data-key'));
            // console.log();
            // return false;
            //id = 
            if($(this).hasClass('cancel'))
            {

            }
            else
            {
                var pageNo = $('.pagination .active a').html();
                if(!pageNo)
                {
                    pageNo = 1;
                }
                // console.log('page no :'+ pageNo);
                // console.log('url: '+ $(this).attr('href') +'&pageNo='+pageNo);
                // $(this).attr('href',$(this).attr('href') +'&pageNo='+pageNo);

                window.location.replace('$update_page_url?id='+$(this).parent().attr('data-key'));
            }
        });
        $('.print').parent().parent().addClass('cancel');


        ");

?>
<?php

namespace backend\components;

use Yii;
use yii\helpers\Url;

class BaseController extends \yii\web\Controller
{
    public function beforeAction($event)
    {
        ob_start();
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;

    	// check if user has reset his/her initial password
        if (!Yii::$app->user->isGuest)	
            if(Yii::$app->user->identity->first_login_reset_flag == 0)
                if($action != 'first-login-reset')
                    $this->redirect(['/site/first-login-reset']);

    	// intialize page titles
        $this->view->params['title'] = '';
        $this->view->params['subTitle'] = '';

        if(Yii::$app->controller->id == 'bookings' && (Yii::$app->controller->action->id == 'delete-bookings-gridview' || Yii::$app->controller->action->id == 'update-bookings-flags-gridview' || Yii::$app->controller->action->id == 'update-bookings-flag-late-checkin-gridview'))
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($event);
    }

    public function isArrayEmpty($array) 
    {
        foreach($array as $key => $val) 
        {
            if (!empty($val))
                return false;
        }
        return true;
    }

}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discount_code".
 *
 * @property integer $id
 * @property string $name
 * @property string $amount
 * @property integer $discount_type_id
 * @property string $enterable_by_registered_users
 * @property string $active
 * @property integer $provider_id
 * @property integer $provider_type_id
 * @property integer $state_id
 *
 * @property Providers $provider
 * @property ProviderTypes $providerType
 * @property States $state
 * @property DiscountCodeTypes $discountType
 * @property Rates[] $rates
 */
class DiscountCode extends \yii\db\ActiveRecord
{
    public static $type_in = [
                                'yes' => 'Yes',
                                'no'=> 'No',
                                ];
    public $server_amount;

    public static $pricing_type = 
    [
        0 => 'Percentage',
        1=> 'Amount',
    ];

    public static $applies_to = 
    [
        0 => 'Unit',
        1 => 'Unit + Upsell',
        2 => 'Upsell',
    ];

    public static $quantity_type = 
    [
        0 => 'Applicable once',
        1 => 'Dependant on extra persons',
        2 => 'Dependant on all persons',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'amount','pricing_type','applies_to','quantity_type','provider_id'], 'required'],
            [[ 'provider_id','server_amount','pricing_type','applies_to','quantity_type'], 'integer'],
            [['name','amount',], 'string', 'max' => 256],
            [['enterable_by_registered_users', 'active', 'linked_to_quantity'], 'string', 'max' => 5],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'amount' => Yii::t('app', 'Amount'),
            'enterable_by_registered_users' => Yii::t('app', 'Enterable By Registered Users'),
            'active' => Yii::t('app', 'Active'),
            'provider_id' => Yii::t('app', 'Destination Name'),
            'pricing_type' => Yii::t('app', 'Pricing Type'),
            'applies_to' => Yii::t('app', 'Applies To'),
            'quantity_type' => Yii::t('app', 'Qunatity Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasMany(Rates::className(), ['discount_code_id' => 'id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_discount_code".
 *
 * @property integer $id
 * @property integer $rates_id
 * @property integer $discount_code_id
 *
 * @property Rates $rates
 * @property DiscountCode $discountCode
 */
class RatesDiscountCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_discount_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rates_id', 'discount_code_id'], 'integer'],
            [['rates_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rates_id' => 'id']],
            [['discount_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountCode::className(), 'targetAttribute' => ['discount_code_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rates_id' => Yii::t('app', 'Rates ID'),
            'discount_code_id' => Yii::t('app', 'Discount Code ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rates_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCode()
    {
        return $this->hasOne(DiscountCode::className(), ['id' => 'discount_code_id']);
    }
}

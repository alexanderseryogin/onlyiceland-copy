<?php

namespace backend\assets_b;

use yii\web\AssetBundle;
use metronic\assets\BackendAsset;
/**
 * Main backend application asset bundle.
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/assets_b';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/restaurant_tags.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'metronic\assets\BackendAsset',
    ];
}

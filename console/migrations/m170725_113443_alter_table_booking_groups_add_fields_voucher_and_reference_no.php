<?php

use yii\db\Migration;

class m170725_113443_alter_table_booking_groups_add_fields_voucher_and_reference_no extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group`   
                          ADD COLUMN `voucher_no` VARCHAR(256) NULL AFTER `travel_partner_id`,
                          ADD COLUMN `reference_no` VARCHAR(256) NULL AFTER `voucher_no`;
                        ');
    }

    public function down()
    {
        echo "m170725_113443_alter_table_booking_groups_add_fields_voucher_and_reference_no cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

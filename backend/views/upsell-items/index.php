<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

use common\models\Destinations;
use common\models\UpsellItems;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UpsellItemsSearch */
/* @var $dataDestination yii\data\ActiveDataDestination */

$this->title = Yii::t('app', 'Upsell Items');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upsell-items-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW UPSELL ITEM'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'upsell-gridview','timeout' => 10000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'value' => function($data)
                {
                    return $data->name;
                },
            ],
            [
                'attribute' => 'description',
                'value' => function($data)
                {
                    return $data->description;
                },
                'contentOptions' =>['style' => 'min-width:300px; overflow: hidden; text-overflow: ellipsis;'],
                'headerOptions' =>['style' => 'min-width:300px;'],
            ],
            [
                'attribute' => 'type',
                'label' => 'Type',
                'value' => function($data)
                {
                    return UpsellItems::$types[$data->type];
                },
                'filter' => Html::dropDownList(
                    'UpsellItemsSearch[type]', 
                    $searchModel['type'], 
                    UpsellItems::$types,
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'type_dropdown', 
                        'class'=>'form-control'
                    ]),
            ],
            [
                'attribute' => 'applies_to',
                'label' => 'Applies To',
                'value' => function($data)
                {
                    return UpsellItems::$applies_to[$data->applies_to];
                },
                'filter' => Html::dropDownList(
                    'UpsellItemsSearch[applies_to]', 
                    $searchModel['applies_to'], 
                    UpsellItems::$applies_to,
                    [
                        'prompt' => 'Select an Option', 
                        'id'=>'applies_dropdown', 
                        'class'=>'form-control'
                    ]),
            ],

            [
                'attribute' => 'period',
                'label' => 'Period',
                'value' => function($data)
                {
                    return UpsellItems::$period[$data->period];
                },
                'filter' => Html::dropDownList(
                    'UpsellItemsSearch[period]', 
                    $searchModel['period'], 
                    UpsellItems::$period,
                    [
                        'prompt' => 'Select a Period', 
                        'id'=>'period_dropdown', 
                        'class'=>'form-control'
                    ]),
            ],

            [   
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>['style' => 'min-width:70px;'],
                'headerOptions' =>['style' => 'min-width:70px;'],],
            ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#applies_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#period_dropdown').select2({
        placeholder: \"Select a Period\",
        allowClear: true
    });

    $('#upsell-gridview').on('pjax:end', function() {

        $('#type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });

        $('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
        });

        $('#applies_dropdown').select2({
            placeholder: \"Select an Option\",
            allowClear: true
        });

        $('#period_dropdown').select2({
            placeholder: \"Select a Period\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

});",View::POS_END);
?>

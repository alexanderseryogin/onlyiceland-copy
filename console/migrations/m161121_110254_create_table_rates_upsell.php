<?php

use yii\db\Migration;

class m161121_110254_create_table_rates_upsell extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `rates_upsell`( `id` INT(11) NOT NULL AUTO_INCREMENT, `rates_id` INT(11), `upsell_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_rates_upsell_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_rates_upsell_upsell_id` FOREIGN KEY (`upsell_id`) REFERENCES `upsell_items`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

            
        ");
    }

    public function down()
    {
        echo "m161121_110254_create_table_rates_upsell cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

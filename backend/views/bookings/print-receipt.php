<?php
/**
 * @var \common\models\BookingsItems $booking
 */
    $balance = 0;
    $total = 0;
    $payments = 0;
    $vat_amount = 0;
    $city_tax = $booking->no_of_booking_days * 300;
?>

<table class="table">
    <tr>
        <td>
            <h1><em><?= $booking->provider->name;?></em></h1>

            <?php if($booking->itemName) { ?>
                <h3><?= $booking->itemName->item_name; ?></h3>
            <?php } ?>

            <h4>Num. <?= $booking->id;?></h4>
        </td>
        <td class="text-right">
            <p>
                Simi: <?= $booking->provider->phone;?>
            </p>
            <p>
                Fax: <?= $booking->provider->fax;?>
            </p>
            <br>
            <p>
                Kt. <?= $booking->provider->kennitala;?>
            </p>
            <p>
                VAT # <?= $booking->provider->vat_id;?>
            </p>
        </td>
    </tr>
</table>


<table class="table">
    <thead>
        <tr>
            <th>Date</th>
            <th>Description</th>
            <th>VAT</th>
            <th>Charge(s)</th>
            <th>Credit(s)</th>
            <th>Balance</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($booking->bookingDateFinancesReverse as $finance) { ?>
            <tr>
                <td>
                    <?= $finance->date;?>
                </td>
                <td>
                    <?= $finance->price_description; ?>
                    <?php if($finance->type == 2) {?>
                    Payment
                    <?php } ?>
                    <?php if($finance->quantity > 1) { ?>
                        <?= "($finance->quantity @ $finance->amount kr/each)" ;?>
                    <?php } ?>
                </td>
                <td>
                    <?= $finance->vat ? $finance->vat . '%' : ''; ?>
                </td>
                <td class="text-right">
                    <?php if($finance->type == 1) {?>
                        <?= number_format($finance->final_total, 0, ',', '.'); ?> kr
                        <?php
                            $balance += $finance->final_total;
                            $total += $finance->final_total;
                            $vat_amount += $finance->vat_amount;
                        ?>
                    <?php } ?>
                </td>
                <td class="text-right">
                    <?php if($finance->type == 2) {?>
                        <?= number_format($finance->final_total, 0, ',', '.'); ?> kr
                        <?php
                            $balance -= $finance->final_total; //if was payment
                            $payments += $finance->final_total;
                        ?>
                    <?php } ?>
                </td>
                <td class="text-right">
                    <?= number_format($total - $payments, 0, ',', '.'); ?> kr
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php
    $total_exl = $balance - $vat_amount - $city_tax;
    $total_exl = number_format($total_exl, 0, ',', '.');
?>

<table class="table table-no-bordered text-right">
    <tr>
        <td>
            <p>
                <b>Total (incl. all taxes and fees)</b>
            </p>
        </td>
        <td>
            <p>
                <?php $total = number_format($total, 0, ',', '.'); ?>
                <b><?= $total;?> kr</b>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <b>Payments and Credits</b>
            </p>
        </td>
        <td>
            <p>
                <?php $payments = number_format($payments, 0, ',', '.'); ?>
                <b><?= $payments;?> kr</b>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <b>Balance Due</b>
            </p>
        </td>
        <td>
            <p>
                <?php $balance = number_format($balance, 0, ',', '.'); ?>
                <b><?= $balance;?> kr</b>
            </p>
        </td>
    </tr>
</table>

<hr >

<table class="table table-no-bordered text-right">
    <tr>
        <td>
            <p>
                <b>City Tax (300 kr/night)</b>
            </p>
        </td>
        <td>
            <p>
                <b>VAT</b>
            </p>
        </td>
        <td>
            <p>
                <b>Total (excl. VAT and Fees)</b>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p>
                <?php $city_tax = number_format($city_tax, 0, ',', '.'); ?>
                <b><?= $city_tax;?> kr</b>
            </p>
        </td>
        <td>
            <p>
                <?php $vat_amount = number_format($vat_amount, 0, ',', '.'); ?>
                <b><?= $vat_amount;?> kr</b>
            </p>
        </td>
        <td>
            <p>
                <b><?= $total_exl;?> kr</b>
            </p>
        </td>
    </tr>
</table>

<?php

use yii\db\Migration;

class m180212_062800_alter_table_destinations_add_field_use_housekeeping extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `providers`   
                          ADD COLUMN `use_housekeeping` INT(6) NULL AFTER `active`;
                        ');
    }

    public function safeDown()
    {
        echo "m180212_062800_alter_table_destinations_add_field_use_housekeeping cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_062800_alter_table_destinations_add_field_use_housekeeping cannot be reverted.\n";

        return false;
    }
    */
}

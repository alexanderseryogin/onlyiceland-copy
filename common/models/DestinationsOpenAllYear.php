<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_open_all_year".
 *
 * @property string $id
 * @property integer $provider_id
 * @property integer $jan
 * @property integer $feb
 * @property integer $march
 * @property integer $april
 * @property integer $may
 * @property integer $june
 * @property integer $july
 * @property integer $august
 * @property integer $sep
 * @property integer $oct
 * @property integer $nov
 * @property integer $dec
 *
 * @property Providers $provider
 */
class DestinationsOpenAllYear extends \yii\db\ActiveRecord
{
    public static $months = 
    [
        '1'=>'January',
        '2'=>'February',
        '3'=>'March',
        '4'=>'April',
        '5'=>'May',
        '6'=>'June',
        '7'=>'July ',
        '8'=>'August',
        '9'=>'September',
        '10'=>'October',
        '11'=>'November',
        '12'=>'December'
    ];

    public $selected_months = '';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_open_all_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'august', 'sep', 'oct', 'nov', 'dec'], 'integer'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            //[['selected_months'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
            'jan' => Yii::t('app', 'Jan'),
            'feb' => Yii::t('app', 'Feb'),
            'march' => Yii::t('app', 'March'),
            'april' => Yii::t('app', 'April'),
            'may' => Yii::t('app', 'May'),
            'june' => Yii::t('app', 'June'),
            'july' => Yii::t('app', 'July'),
            'august' => Yii::t('app', 'August'),
            'sep' => Yii::t('app', 'Sep'),
            'oct' => Yii::t('app', 'Oct'),
            'nov' => Yii::t('app', 'Nov'),
            'dec' => Yii::t('app', 'Dec'),
            'selected_months' => Yii::t('app', 'Months'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $picture_name
 * @property integer $type
 * @property integer $poi_id
 * @property integer $p_id
 *
 * @property PlacesOfInterest $poi
 * @property Properties $p
 */
class Gallery extends \yii\db\ActiveRecord
{
    const GALLERY_TYPE_POI = 0;
    const GALLERY_TYPE_P   = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'picture_name', 'type'], 'required'],
            [['id', 'type', 'poi_id', 'p_id'], 'integer'],
            [['picture_name'], 'string', 'max' => 256],
            [['poi_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlacesOfInterest::className(), 'targetAttribute' => ['poi_id' => 'id']],
            [['p_id'], 'exist', 'skipOnError' => true, 'targetClass' => Properties::className(), 'targetAttribute' => ['p_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'picture_name' => Yii::t('app', 'Picture'),
            'type' => Yii::t('app', 'Type'),
            'poi_id' => Yii::t('app', 'Poi ID'),
            'p_id' => Yii::t('app', 'P ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoi()
    {
        return $this->hasOne(PlacesOfInterest::className(), ['id' => 'poi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getP()
    {
        return $this->hasOne(Properties::className(), ['id' => 'p_id']);
    }
}

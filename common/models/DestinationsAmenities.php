<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_amenities".
 *
 * @property string $id
 * @property integer $provider_id
 * @property string $amenities_id
 *
 * @property Amenities $amenities
 * @property Providers $provider
 */
class DestinationsAmenities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_amenities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'amenities_id','type','can_be_banner'], 'integer'],
            [['amenities_id'], 'exist', 'skipOnError' => true, 'targetClass' => Amenities::className(), 'targetAttribute' => ['amenities_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
            'amenities_id' => Yii::t('app', 'Amenities ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenities()
    {
        return $this->hasOne(Amenities::className(), ['id' => 'amenities_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }
}

<?php

?>
<footer class="site-footer">
                  <div class="container">
                      <div class="row">
                        <div class="col-md-3 col-sm-6 footer-widget widget">
                            <h3 class="widgettitle">Latest News</h3>
                              <ul>
                                  <li>
                                      <a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a>
                                      <span class="meta-data">1 March, 2014</span>
                                  </li>
                                  <li>
                                  <a href="blog-post.html">Lorem ipsum dolor sit amet elit, consectetur adipiscing.</a>
                                      <span class="meta-data">28 February, 2014</span>
                                  </li>
                                  <li>
                                  <a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a>
                                      <span class="meta-data">26 February, 2014</span>
                                  </li>
                              </ul>
                          </div>
                        <div class="col-md-3 col-sm-6 footer-widget widget">
                            <h3 class="widgettitle">Useful Links</h3>
                           <ul>
                            <li><a href="javascript:void(0)">Travel Info</a></li>
                            <li><a href="javascript:void(0)">Tour Guides</a></li>
                            <li><a href="javascript:void(0)">About Us</a></li>
                            <li><a href="javascript:void(0)">Legal</a></li>
                            <li><a href="javascript:void(0)">Contact</a></li>
                           </ul>
                       </div>
                        <div class="col-md-3 col-sm-6 footer-widget widget">
                            <h3 class="widgettitle">Our Newsletter</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel.</p>
                            <form method="post" id="newsletterform" name="newsletterform" class="newsletter-form" action="mail/newsletter.php">
                                <input type="email" name="nl-email" id="nl-email" placeholder="Enter your email" class="form-control">
                                <input type="submit" name="nl-submit" id="nl-submit" class="btn btn-primary btn-block btn-lg" value="Subscribe">
                            </form>
                            <div class="clearfix"></div>
                            <div id="nl-message"></div>
                       </div>
                   </div>
               </div>
              </footer>
<?php

use yii\db\Migration;

class m170602_064744_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items`   
          ADD COLUMN `travel_partner_id` INT(11) NULL AFTER `status_id`,
          ADD COLUMN `offers_id` INT(11) NULL AFTER `travel_partner_id`,
          ADD COLUMN `estimated_arrival_time_id` INT(11) UNSIGNED NULL AFTER `offers_id`,
          ADD CONSTRAINT `bookings_items_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `travel_partner`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          ADD CONSTRAINT `bookings_items_offers` FOREIGN KEY (`offers_id`) REFERENCES `offers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
          ADD CONSTRAINT `bookings_items_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
        ");
    }

    public function down()
    {
        echo "m170602_064744_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

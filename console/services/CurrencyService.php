<?php


namespace console\services;


use common\models\CurrencyRate;
use SimpleXMLElement;
use GuzzleHttp\Client;

class CurrencyService
{
    private $url;
    private $client;

    public function __construct()
    {
        $this->url = 'http://www.sedlabanki.is/xmltimeseries/Default.aspx?DagsFra=LATEST&GroupID=9&Type=xml';
        $this->client = new Client();
    }

    public function getXml()
    {
        $xml = $this->client->request('GET', $this->url);

        $xml = new SimpleXMLElement($xml->getBody());
        $result = $this->getRate($xml);

        $this->save($result);
    }

    private function getRate($xml)
    {
        $result = [];

        foreach ($xml as $currency) {
            if ($currency->FameName == CurrencyRate::USD
                || $currency->FameName == CurrencyRate::EUR
                || $currency->FameName == CurrencyRate::GBP
                || $currency->FameName == CurrencyRate::CAD
                || $currency->FameName == CurrencyRate::DKK
                || $currency->FameName == CurrencyRate::NOK
                || $currency->FameName == CurrencyRate::SEK
                || $currency->FameName == CurrencyRate::CHF
                || $currency->FameName == CurrencyRate::JPY
            ) {
                $res['name'] = (string)$currency->FameName;
                $res['date'] = (string)$currency->TimeSeriesData->Entry->Date;
                $res['value'] = (double)$currency->TimeSeriesData->Entry->Value;

                $result[] = $res;
            }
        }
        return $result;
    }

    private function save($data)
    {
        foreach ($data as $array) {
            /** @var CurrencyRate $currency */
            $currency = CurrencyRate::findByName($array['name']);
            $currency->setValues($array);
            $currency->save();
        }
    }

    public function getCurrentRate()
    {
        $usd = CurrencyRate::find()->where(['code' => CurrencyRate::USD])->limit(1)->one();
        $eur = CurrencyRate::find()->where(['code' => CurrencyRate::EUR])->limit(1)->one();
        $gbp = CurrencyRate::find()->where(['code' => CurrencyRate::GBP])->limit(1)->one();
        $cad = CurrencyRate::find()->where(['code' => CurrencyRate::CAD])->limit(1)->one();
        $dkk = CurrencyRate::find()->where(['code' => CurrencyRate::DKK])->limit(1)->one();
        $nok = CurrencyRate::find()->where(['code' => CurrencyRate::NOK])->limit(1)->one();
        $sek = CurrencyRate::find()->where(['code' => CurrencyRate::SEK])->limit(1)->one();
        $chf = CurrencyRate::find()->where(['code' => CurrencyRate::CHF])->limit(1)->one();
        $jpy = CurrencyRate::find()->where(['code' => CurrencyRate::JPY])->limit(1)->one();

        return [
            'usd' => $usd,
            'eur' => $eur,
            'gbp' => $gbp,
            'cad' => $cad,
            'dkk' => $dkk,
            'nok' => $nok,
            'sek' => $sek,
            'chf' => $chf,
            'jpy' => $jpy,
        ];
    }
}
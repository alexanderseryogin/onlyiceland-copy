<?php

namespace common\helpers;

/**
 * Class DestinationDateSearch
 * @package common\helpers
 */
class DestinationDateSearch
{
    /**
     * @var array
     */
    const MONTHS = [
        '01' => 'jan',
        '02' => 'feb',
        '03' => 'march',
        '04' => 'april',
        '05' => 'may',
        '06' => 'june',
        '07' => 'july',
        '08' => 'august',
        '09' => 'sep',
        '10' => 'oct',
        '11' => 'nov',
        '12' => 'dec',
    ];

    const WHOLE_WEEK = [1, 2, 3, 4, 5, 6, 7];

    /**
     * @param string $date
     * @return string
     */
    public static function getMonth($date)
    {
        $date = date("m", strtotime($date));
        return self::MONTHS[$date];
    }

    /**
     * @param $date_checkin
     * @param $date_checkout
     * @return array
     */
    public static function getWeekDays($date_checkin, $date_checkout)
    {
        $begin = date("w", strtotime($date_checkin));
        $end = date("w", strtotime($date_checkout));
        if($begin == 0) {
            $begin = 7;
        }
        if($end == 0) {
            $end = 7;
        }
        $days_count = (strtotime($date_checkout) - strtotime($date_checkin)) / (24 * 60 * 60) + 1;
        $week_array = array();

        if($begin < $end && $days_count <= 7) {
            for($i = $begin; $i < $begin + $days_count; $i++) {
                $week_array[] = $i;
            }
        }
        if($begin < $end && $days_count >= 7) {
            $week_array = self::WHOLE_WEEK;
        }
        if($begin > $end && $days_count <= 7) {
            for($i = $begin; $i <= 7; $i++) {
                $week_array[] = $i;
            }
            for($i = 1; $i <= $end; $i++) {
                $week_array[] = $i;
            }
        }

        return $week_array;
    }
}
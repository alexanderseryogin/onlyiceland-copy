<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_amenities".
 *
 * @property string $id
 * @property integer $item_id
 * @property string $amenities_id
 * @property integer $type
 *
 * @property Amenities $amenities
 * @property BookableItems $item
 */
class BookableAmenities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_amenities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'amenities_id', 'type','can_be_banner'], 'integer'],
            [['amenities_id'], 'exist', 'skipOnError' => true, 'targetClass' => Amenities::className(), 'targetAttribute' => ['amenities_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'amenities_id' => Yii::t('app', 'Amenities ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenities()
    {
        return $this->hasOne(Amenities::className(), ['id' => 'amenities_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    }
}

<?php
use common\models\BookingConfirmationTypes;
use common\models\BookingTypes;
use common\models\Destinations;
use common\models\BookingStatuses;
use yii\helpers\ArrayHelper;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\EstimatedArrivalTimes;
use common\models\BookingSpecialRequests;
use common\models\BookingGroupSpecialRequests;
use common\models\DestinationTravelPartners;

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

?>

<div class="row">
    <div class="col-sm-3">
        <label class="control-label"><strong>Booking Confirmation</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-dropdowns" name="BookingGroup[confirmation_id]" >
                <option value=""> Select Booking Confirmation </option>
                <?php
                    $bookingConfirmation =ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name');

                    if(!empty($bookingConfirmation))
                    {
                        foreach ($bookingConfirmation as $key => $value) 
                        {
                            $selected = ($key==$bookingGroupModel->confirmation_id) ? 2 : 0 ;

                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <label class="control-label"><strong>Bookings Cancellation</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-dropdowns" name="BookingGroup[cancellation_id]" >
                <option value=""> Select Booking Cancellation </option>
                <?php
                    foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                    {
                        $selected = ($key==$bookingGroupModel->cancellation_id) ? 2 : 0 ;

                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <label class="control-label"><strong>Flag</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-dropdowns" name="BookingGroup[flag_id]" >
                <option value=""> Select Flag </option>
                <?php
                    $bookingTypes = ArrayHelper::map(BookingTypes::find()->all(),'id','label');

                    if(!empty($bookingTypes))
                    {
                        foreach ($bookingTypes as $key => $value) 
                        {
                            $selected = ($key==$bookingGroupModel->flag_id) ? 2 : 0 ;

                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <label class="control-label"><strong>Status</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-dropdowns" name="BookingGroup[status_id]" >
                <option value=""> Select Status </option>
                <?php
                    $bookingStatuses = ArrayHelper::map(BookingStatuses::find()->all(),'id','label');

                    if(!empty($bookingStatuses))
                    {
                        foreach ($bookingStatuses as $key => $value) 
                        {
                            $selected = ($key==$bookingGroupModel->status_id) ? 2 : 0 ;

                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                        }
                    }
                ?>
            </select>
        </div>
    </div>
</div>

<?php
    if(empty($bookingGroupModel->travel_partner_id) && empty($bookingGroupModel->offers_id))
    {
        $this->registerJs
        ("
            $('.update-all-offers-dropdown').attr('disabled',false);
            $('.update-all-travel-partner-dropdown').attr('disabled',false);
            $('.update-all-vou_ref_div').addClass('hide');
        ");
    }
    else if(empty($bookingGroupModel->travel_partner_id) && !empty($bookingGroupModel->offers_id))
    {
        $this->registerJs
        ("
            $('.update-all-offers-dropdown').attr('disabled',false);
            $('.update-all-travel-partner-dropdown').attr('disabled','disabled');
            $('.update-all-vou_ref_div').addClass('hide');
        ");
    }
    else
    {
        $this->registerJs
        ("
            $('.update-all-offers-dropdown').attr('disabled','disabled');
            $('.update-all-travel-partner-dropdown').attr('disabled',false);
            $('.update-all-vou_ref_div').removeClass('hide');
        ");
    }
?>

<div class="row">
    <div class="col-sm-4">
        <label class="control-label"><strong>Travel Partner</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-travel-partner-dropdown" name="BookingGroup[travel_partner_id]" >
                <option value=""> Select Referrer </option>
                <?php
                    // echo 'provider :'.$provider_id;
                    // exit();
                    $travelPartners = DestinationTravelPartners::find()
                                                                ->where(['destination_id' => $provider_id])
                                                                ->all();

                    if(!empty($travelPartners))
                    {
                        foreach ($travelPartners as $key => $value) 
                        {
                            $selected = ($value->id==$bookingGroupModel->travel_partner_id) ? 2 : 0 ;

                            echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->travelPartner->company_name.' - '.$value->travelPartner->commission.'%'.'</option>';
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <label class="control-label"><strong>Offers</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-offers-dropdown" name="BookingGroup[offers_id]" >
                <option value=""> Select Offer </option>
                <?php
                    $offers = Offers::find()->where(['provider_id'=>$provider_id])->all();

                    if(!empty($offers))
                    {
                        foreach ($offers as $key => $value) 
                        {
                            $selected = ($value->id==$bookingGroupModel->offers_id) ? 2 : 0 ;

                            echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->name.' - '.$value->commission.'%'.'</option>';
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-4">
        <label class="control-label" ><strong>Estimated Arrival Time</strong></label>
        <div class="form-group" >
            <select style="width: 100%;" class="form-control update-all-dropdowns" name="BookingGroup[estimated_arrival_time_id]" >
                <option value=""></option>
                <?php
                    if(!empty($provider_id))
                    {
                        $provider = Destinations::findOne(['id' => $provider_id]);

                        $estimated_arrival_times = EstimatedArrivalTimes::find()->where(['time_group' => $provider->bookingPolicies->time_group])->all();

                        if(!empty($estimated_arrival_times))
                        {
                            foreach ($estimated_arrival_times as $key =>  $obj) 
                            {
                                $start_time = '';

                                if(empty($obj->text))
                                {
                                    $start_time = date('H:i',strtotime($obj->start_time));
                                }
                                else
                                {
                                    $start_time = $obj->text;
                                }
                                $obj->end_time = date('H:i',strtotime($obj->end_time));

                                $selected = ($obj->id==$bookingGroupModel->estimated_arrival_time_id) ? 2 : 0 ;
                                
                                echo "<option value='".$obj->id."' ".$Options[$selected].">".$start_time." - ".$obj->end_time."</option>";  
                            }
                        }  
                    }
                ?>
            </select>
        </div>
    </div>
</div>

<div class="row update-all-vou_ref_div">
    <div class="col-sm-3">
        <label class="control-label "><strong>Voucher #</strong></label>
        <div class="form-group" >
            <input class="form-control update-all-voucher_no?>" name="BookingGroup[voucher_no]" value="<?php echo $bookingGroupModel->voucher_no; ?>">
        </div>
    </div>
    <div class="col-sm-3">
        <label class="control-label"><strong>Reference #</strong></label>
        <div class="form-group" >
            <input class="form-control update-all-reference_no" name="BookingGroup[reference_no]" value="<?php echo $bookingGroupModel->reference_no; ?>">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php
            if(!empty($provider_id))
            {
                $get_special_requests = BookingGroupSpecialRequests::find()->where(['booking_group_id' => $bookingGroupModel->id])->all();

                $checked_special_requests = [];

                if(!empty($get_special_requests))
                {
                    foreach ($get_special_requests as $key => $value) 
                    {
                        $checked_special_requests[] = $value['special_request_id'];
                    }
                }

                echo $this->render('update_all_special_requests',
                [
                    'provider_id' => $provider_id,
                    'checked_special_requests' => $checked_special_requests,
                ]);
            }
        ?>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <label class="control-label"><strong>Comment</strong></label>
        <div class="form-group" >
            <textarea class="form-control update-all-comment>" name="BookingGroup[comments]" rows="3"><?php echo $bookingGroupModel->comments; ?></textarea>
        </div>
    </div>
</div>

<input type="text" name="BookingGroup[booked_items_ids]" value="<?php echo $items_ids; ?>" hidden>
<input type="text" name="BookingGroup[group_id]" value="<?php echo $bookingGroupModel->id; ?>" hidden>
<input type="text" name="BookingGroup[type]" id="setting_type" hidden>

<?php

use yii\db\Migration;

class m170110_081903_alter_providers_amenities_add_can_be_banner extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_amenities` ADD COLUMN `can_be_banner` TINYINT(4) NULL AFTER `type`; ");
    }

    public function down()
    {
        echo "m170110_081903_alter_providers_amenities_add_can_be_banner cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170721_062550_alter_table_housekeeping_status_add_primary_key extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `housekeeping_status`   
                          CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, 
                          ADD PRIMARY KEY (`id`);
                        ');
    }

    public function down()
    {
        echo "m170721_062550_alter_table_housekeeping_status_add_primary_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

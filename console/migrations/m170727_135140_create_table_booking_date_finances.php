<?php

use yii\db\Migration;

class m170727_135140_create_table_booking_date_finances extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_date_finances`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `booking_date_id` INT(11) UNSIGNED,
                          `booking_item_id` INT(11) UNSIGNED,
                          `booking_group_id` INT(11) UNSIGNED,
                          `price_description` LONGTEXT,
                          `amount` DOUBLE,
                          `vat` DOUBLE,
                          `tax` DOUBLE,
                          `quantity` INT(11),
                          `show_receipt` INT(11),
                          `added_by` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`booking_date_id`) REFERENCES `booking_dates`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`booking_group_id`) REFERENCES `booking_group`(`id`) ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170727_135140_create_table_booking_date_finances cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

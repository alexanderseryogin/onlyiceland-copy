F<?php
	
use common\models\BookingsItems;
use common\models\BookableBedsCombinations;
use common\models\RatesCapacityPricing;
use common\models\Rates;
use common\models\BookableBedTypes;
use common\models\BookingsDiscountCodes;
use common\models\BookingsUpsellItems;
use common\models\User;
use common\models\BookingConfirmationTypes;
use common\models\BookingTypes;
use common\models\Destinations;
use common\models\BookingStatuses;
use yii\helpers\ArrayHelper;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\EstimatedArrivalTimes;
use common\models\BookingSpecialRequests;
use common\models\Countries;

$model = BookingsItems::findOne(['id' => $id]);

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$bookingsItemsModel = BookingsItems::find()->where(['item_id' => $model->item_id, 'deleted_at' => 0, 'temp_flag' => 0]);
$total_booked_items = $bookingsItemsModel->sum('quantity');
$item_quantity = $model->item->item_quantity;

$quantityOptions = ($item_quantity - $total_booked_items) + $model->quantity;

if($quantityOptions>10)
    $quantityOptions = 10;

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading" style="background: <?php echo $model->item->background_color?>; color: <?php echo $model->item->text_color?>">
                <h4 class="panel-title">
                    <div class="accordion-toggle" data-parent="#accordion_item_rates">
                        <label><?php echo $model->id.' - '.$model->provider->name.' - '.date('d/m/Y',strtotime($model->arrival_date)).'<span class="fa fa-arrow-right"></span>'.date('d/m/Y',strtotime($model->departure_date)).' - '.$model->item->itemType->name; ?></label>
                        <a id="delete-item" data="<?php echo $model->id?>" href="javascript:void(0)" class="btn btn-sm btn-danger pull-right" style="margin-top: -3px;">Delete</a>
                        <a id="unlink-item" data="<?php echo $model->id?>" href="javascript:void(0)" class="btn btn-sm btn-warning pull-right" style="margin-right: 5px;margin-top: -3px;">Unlink</a>
                        
                    </div>
                </h4>
            </div>
            <div class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">       
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <strong>
                                    <th>Rate</th>
                                    
                                    <?php
                                        if($model->rates->destination->pricing == 1 && $model->rates->destination->getDestinationTypeName()=='Accommodation') 
                                        {  
                                            echo '<th>Capacity Pricing </th>';
                                        }
                                        else if($model->rates->destination->pricing == 0)
                                        {
                                            echo '<th>Adults</th><th>Children</th>';
                                        }
                                    ?>
                                
                                    <th>Available Discounts</th>
                                    <th>Upsell Items</th>
                                    <th>Total </th>
                                </strong>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td style="min-width:120px;">

                                    <input class="modal-booking-item-id?>" data="<?php echo $model->id.':'.$model->rates_id.':'.$model->id ?>" type="text" value="<?php echo $model->id?>" hidden>
                                    <input class="pricing-type" type="text" value="<?php echo $model->rates->destination->pricing?>" hidden>
                                    <input class="total-nights" type="text" value="<?php echo $model->no_of_nights?>" hidden>

                                    <?php echo $model->rates->name.'<br><br>'?>

                                    <!-- As the quantity will always be one so no need to calculate quantity of items -->

                                    <input type="hidden" data="<?php echo $model->item_id.':'.$model->rates_id.':'.$model->id?>" class="form-control item-quantity item-quantities-<?php echo $model->item_id?> item-quantity-<?php echo $model->rates_id?>-<?php echo $model->id?>" value="1">
                                    
                                    <br><br><button id="<?=$model->id?>" type="button" class="btn btn-xs btn-default view-details"><i class="fa fa-chevron-down" aria-hidden="true"></i> <span>View Details</span> </button>
                                </td>

                                <?php

                                // ************* capacity pricing ************ //

                                if($model->provider->pricing == 1 && $model->provider->getDestinationTypeName()=='Accommodation') 
                                {
                                    echo '<td>';

                                    $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $model->rates_id])->all();

                                    if(!empty($rates_capacity_pricing))
                                    {
                                        foreach ($rates_capacity_pricing as $key => $value)
                                        {
                                            if($value->person_no >=1 && $value->person_no<=4)
                                            {
                                                $word =  Rates::$numberWords[$value->person_no-1];

                                                $checked = ($value->person_no == $model->person_no)? 1 : 0;

                                                $price = $value->price;
                                                $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");

                                                ?>

                                                <label>
                                                    <input name="BookedItems[<?php echo $model->id?>][capacity_pricing]" type="checkbox" data="<?php echo $value->person_no?>" id="<?php echo $model->item_id.':'.$model->rates_id.':'.$model->id?>" class="capacity-pricing capacity-pricing-<?php echo $model->item_id?> cpricing-<?php echo $model->rates_id.'-'.$model->id?>" value="<?php echo $value->person_no?>" <?php echo $Options[$checked]?>> <?php echo $word.': '.$value->price?>
                                                </label><br>

                                                <?php
                                            }
                                        } 
                                    }
                                    echo '</td>';
                                }
                                else if($model->provider->pricing == 0)  // ************* First/Additional ************ //
                                {
                                ?>
                                    <td style="min-width:50px;">
                                        <select style="width:100%" data="<?php echo $model->item_id.':'.$model->rates_id.':'.$model->id?>" class="adults-dropdown adults-dropdown-<?php echo $model->item_id?> adult-dropdown-<?php echo $model->rates_id?>-<?php echo $model->id?>" name="BookedItems[<?php echo $model->id?>][no_of_adults]">
                                            <?php
                                                for ($i=0; $i <= 20 ; $i++) 
                                                {
                                                    $selected = $model->no_of_adults == $i ? 2 : 0;

                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                }
                                            ?>
                                        </select>
                                    </td>

                                    <td style="min-width:50px;">
                                        <select style="width:100%" data="<?php echo $model->item_id.':'.$model->rates_id.':'.$model->id?>" class="children-dropdown children-dropdown-<?php echo $model->item_id?> child-dropdown-<?php echo $model->rates_id?>-<?php echo $model->id?>" name="BookedItems[<?php echo $model->id?>][no_of_children]">
                                            <?php
                                                for ($i=0; $i <= 20 ; $i++) 
                                                { 
                                                    $selected = $selected = $model->no_of_children == $i ? 2 : 0;

                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                }
                                            ?>
                                        </select>
                                    </td>
                                <?php
                                }

                                // ******************* Available Discount Codes and Upsell Items ******************** //

                                $max_extra_beds = $model->item->max_extra_beds;
                                $bed_type_people = 0;

                                $selected_upsell_extra_bed_quantity = '';
                                $selected_discount_code_extra_bed_quantity = '';

                                if($model->provider->pricing == 1 && $model->provider->getDestinationTypeName()=='Accommodation')
                                {
                                    $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $model->item_id])->all();

                                    if(!empty($itemBedTypesModel))
                                    {
                                        foreach ($itemBedTypesModel as $key => $value) 
                                        {
                                            $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                        }
                                    }
                                }
                                
                                // *********** Discount Codes ********** //

                                $get_discount_codes = BookingsDiscountCodes::find()->where(['bookings_items_id' => $model->id])->all();
                                $checked_discount_codes = [];
                                $selected_discount_code_extra_bed_quantity = [];

                                if(!empty($get_discount_codes))
                                {
                                    foreach ($get_discount_codes as $key => $value) 
                                    {
                                        $checked_discount_codes[$value['discount_id']] = $value['price'];
                                        $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                    }
                                }

                                echo $this->render('update_group_discount_codes',
                                [
                                    'item_id'  => $model->item_id,
                                    'rates_id' => $model->rates_id,
                                    'checked_discount_codes' => $checked_discount_codes,
                                    'bed_type_people' => $bed_type_people,
                                    'max_extra_beds' => $max_extra_beds,
                                    'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                    'model_id' => $model->id,
                                ]);

                                // *********** Upsell Items ********** //

                                $get_upsell_items = BookingsUpsellItems::find()->where(['bookings_items_id' => $model->id])->all();
                                $checked_upsell_items = [];
                                $selected_upsell_extra_bed_quantity = [];

                                if(!empty($get_upsell_items))
                                {
                                    foreach ($get_upsell_items as $key => $value) 
                                    {
                                        $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                        $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                    }
                                }

                                echo $this->render('update_group_upsell_items',
                                [
                                    'rates_id' => $model->rates_id,
                                    'checked_upsell_items' => $checked_upsell_items,
                                    'bed_type_people' => $bed_type_people,
                                    'max_extra_beds' => $max_extra_beds,
                                    'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                    'model_id' => $model->id,
                                ]);

                                // ****************** Total Price and Json *********************//

                                $orgTotal = 0;
                                $icelandTotal = 0;
                                $tooltips_title = '';

                                if(!empty($model->total))
                                {
                                    $orgTotal = $model->total;
                                    $icelandTotal = Yii::$app->formatter->asDecimal($orgTotal, "ISK");
                                }

                                if(!empty($model->price_json))
                                {
                                    $arr = json_decode($model->price_json);

                                    $tooltips_title = $tooltips_title . 'Quantity = '. $arr->quantity .' , ';
                                    $tooltips_title = $tooltips_title . 'Total Nights = '. $arr->total_nights .' , ';
                                    $tooltips_title = $tooltips_title . 'Rate = '. $arr->rate .' , ';
                                    $tooltips_title = $tooltips_title . 'VAT % = '. $arr->vat .' , ';
                                    $tooltips_title = $tooltips_title . 'x = '. $arr->x .' , ';
                                    $tooltips_title = $tooltips_title . 'lodgingTax = '. $arr->lodgingTax .' , ';
                                    $tooltips_title = $tooltips_title . 'y = '. $arr->y .' , ';
                                    $tooltips_title = $tooltips_title . 'voucher_discount = '. $arr->voucher_discount .' , ';
                                    $tooltips_title = $tooltips_title . 'travel_partner_discount = '. $arr->travel_partner_discount .' , ';
                                    $tooltips_title = $tooltips_title . 'offer_discount = '. $arr->offer_discount .' , ';
                                    $tooltips_title = $tooltips_title . 'amount_of_discount = '. $arr->amount_of_discount .' , ';
                                    $tooltips_title = $tooltips_title . 'sub_total_1 = '. $arr->sub_total_1 .' , ';
                                    $tooltips_title = $tooltips_title . 'sub_total_2 = '. $arr->sub_total_2 .' , ';
                                    $tooltips_title = $tooltips_title . 'vat_amount = '. $arr->vat_amount .' , ';
                                    $tooltips_title = $tooltips_title . 'Total Price = '. $arr->icelandPrice;

                                    $price_json = str_replace('"', "'", $model->price_json);
                                }
                                else
                                {
                                    $tooltips_title = 'Item Price';
                                    $price_json = '';
                                }

                                ?>

                                <td>
                                    <input class="item-total-input item-total-input-<?php echo $model->rates_id?>-<?php echo $model->id?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                    <input type="text" value="<?php echo $price_json; ?>" name="BookedItems[<?php echo $model->id?>][price_json]" class="price-json-<?php echo $model->rates_id?>-<?php echo $model->id?>" hidden>

                                    <label>
                                        <a href="javascript:;" data-placement="left" class="item-total-<?php echo $model->rates_id?>-<?php echo $model->id?> total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                        </a>
                                        <span><?php echo $icelandTotal?></span>
                                    </label>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="panel panel-default modal-rate-details-fields modal-rate-details-fields-<?php echo $model->id?>" style="display: none; background: #F1F1F1">
                        <div class="panel-body">
                            <?php 

                                if($model->provider->getDestinationTypeName()=='Accommodation')
                                {
                                    $itemBedsCombinationsModel = BookableBedsCombinations::find()->where(['item_id' => $model->item_id])->all();

                                    if(!empty($itemBedsCombinationsModel))
                                    {
                                        echo '<label class="control-label modal-rate-details-fields" style="display: none;"><strong>Bed Preference:</strong></label>';

                                        echo '<div class="modal-rate-details-fields" style="display: block;">';

                                            foreach ($itemBedsCombinationsModel as $key => $value) 
                                            {   
                                                $checked = ($value->beds_combinations_id == $model->beds_combinations_id)? 1 : 0;

                                                $path = Yii::getAlias('@web').'/../uploads/beds-combinations/'.$value->beds_combinations_id.'/icons/'.$value->bedsCombinations->icon;
                                                ?>

                                                <label style="margin-left:10px;" class="mt-radio mt-radio-outline">
                                                    <input data="<?php echo $value->beds_combinations_id?>" class="modal-beds-combinations" name="BookedItems[<?php echo $model->id?>][beds_combinations_id]" value="<?php echo $value->beds_combinations_id?>" <?php echo $Options[$checked]?> type="radio"> <?php echo $value->bedsCombinations->combination?> <img src="<?php echo $path ?>" style="max-height:30px" >
                                                    <span></span>
                                                </label>

                                                <?php
                                            }
                                        echo '</div>';
                                    }
                                }
                            ?>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>User</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-user-dropdown" name="BookedItems[<?php echo $model->id?>][user_id]" >
                                            <option value="">Select a User</option>
                                            <?php
                                                $users = ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_USER])->all(),
                                                'id',
                                                function($model, $defaultValue) 
                                                {
                                                    if(empty($model->profile->country_id))
                                                        $country = '';
                                                    else
                                                        $country = ' - '.$model->profile->country->name;

                                                    return $model->username.' - '.$model->email.$country;
                                                });

                                                if(!empty($users))
                                                {
                                                    foreach ($users as $key => $value) 
                                                    {
                                                        $selected = ($key==$model->user_id) ? 2 : 0 ;

                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>First Name</strong></label>
                                    <div class="form-group" >
                                        <input type="text" name="BookedItems[<?php echo $model->id?>][guest_first_name]" class="form-control modal-guest-first-name" value="<?= isset($model->guest_user_first_name) ? $model->guest_user_first_name : ''?>" >
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Last Name</strong></label>
                                    <div class="form-group" >
                                        <input type="text" name="BookedItems[<?php echo $model->id?>][guest_last_name]" class="form-control modal-guest-last-name " value="<?= isset($model->guest_user_last_name)? $model->guest_user_last_name : ''?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Country</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-guest-country" name="BookedItems[<?php echo $model->id?>][guest_country_id]">
                                            <?php
                                                $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                                foreach ($countries as $key => $value) 
                                                {
                                                    if(empty($model->user_id))
                                                    {
                                                        $selected = ($key == $model->guest_user_country) ? 2 : 0 ;
                                                    }
                                                    else
                                                    {
                                                        //######## added iceland as default selected #############//
                                                        $selected = ($value == 'Iceland') ? 2 : 0 ;
                                                    }
                                                    
                                                    echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Booking Confirmation</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-confirmation" name="BookedItems[<?php echo $model->id?>][confirmation_id]" >
                                            <?php
                                                $bookingConfirmation =ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name');

                                                if(!empty($bookingConfirmation))
                                                {
                                                    foreach ($bookingConfirmation as $key => $value) 
                                                    {
                                                        $selected = ($key==$model->confirmation_id) ? 2 : 0 ;

                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Bookings Cancellation</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-cancellation" name="BookedItems[<?php echo $model->id?>][booking_cancellation]" >
                                            <?php
                                                foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                                                {
                                                    $selected = ($key==$model->booking_cancellation) ? 2 : 0 ;

                                                    echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Flag</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-flag" name="BookedItems[<?php echo $model->id?>][flag_id]" >
                                            <?php
                                                $bookingTypes = ArrayHelper::map(BookingTypes::find()->all(),'id','label');

                                                if(!empty($bookingTypes))
                                                {
                                                    foreach ($bookingTypes as $key => $value) 
                                                    {
                                                        $selected = ($key==$model->flag_id) ? 2 : 0 ;

                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label"><strong>Status</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-status" name="BookedItems[<?php echo $model->id?>][status_id]" >
                                            <?php
                                                $bookingStatuses = ArrayHelper::map(BookingStatuses::find()->all(),'id','label');

                                                if(!empty($bookingStatuses))
                                                {
                                                    foreach ($bookingStatuses as $key => $value) 
                                                    {
                                                        $selected = ($key==$model->status_id) ? 2 : 0 ;

                                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <?php
                                if(empty($model->travel_partner_id) && empty($model->offers_id))
                                {
                                    $this->registerJs
                                    ("
                                        $('.offers-dropdown-'+".$model->rates_id."+'-'+".$model->id.").attr('disabled',false);
                                        $('.travel-partner-dropdown-'+".$model->rates_id."+'-'+".$model->id.").attr('disabled',false);
                                    ");
                                }
                                else if(empty($model->travel_partner_id) && !empty($model->offers_id))
                                {
                                    $this->registerJs
                                    ("
                                        $('.offers-dropdown-'+".$model->rates_id."+'-'+".$model->id.").attr('disabled',false);
                                        $('.travel-partner-dropdown-'+".$model->rates_id."+'-'+".$model->id.").attr('disabled','disabled');
                                    ");
                                }
                                else
                                {
                                    $this->registerJs
                                    ("
                                        $('.offers-dropdown-'+".$model->rates_id."+'-'+".$model->id.").attr('disabled','disabled');
                                        $('.travel-partner-dropdown-'+".$model->rates_id."+'-'+".$model->id.").attr('disabled',false);
                                    ");
                                } 
                            ?>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="control-label"><strong>Travel Partner</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" data="<?php echo $model->item_id.':'.$model->rates_id.':'.$model->id?>" class="form-control travel-partner-dropdown travel-partner-dropdown-<?php echo $model->rates_id?>-<?php echo $model->id?>" name="BookedItems[<?php echo $model->id?>][travel_partner_id]" >
                                            <option value=""> Select Referrer </option>
                                            <?php
                                                $travelPartners = TravelPartner::find()->all();

                                                if(!empty($travelPartners))
                                                {
                                                    foreach ($travelPartners as $key => $value) 
                                                    {
                                                        $selected = ($value->id==$model->travel_partner_id) ? 2 : 0 ;

                                                        echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->company_name.' - '.$value->commission.'%'.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label"><strong>Offers</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" data="<?php echo $model->item_id.':'.$model->rates_id.':'.$model->id?>" class="form-control offers-dropdown offers-dropdown-<?php echo $model->rates_id?>-<?php echo $model->id?>" name="BookedItems[<?php echo $model->id?>][offers_id]" >
                                            <option value=""> Select Offer </option>
                                            <?php
                                                $offers = Offers::find()->where(['provider_id'=>$model->provider_id])->all();

                                                if(!empty($offers))
                                                {
                                                    foreach ($offers as $key => $value) 
                                                    {
                                                        $selected = ($value->id==$model->offers_id) ? 2 : 0 ;

                                                        echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->name.' - '.$value->commission.'%'.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label" ><strong>Estimated Arrival Time</strong></label>
                                    <div class="form-group" >
                                        <select style="width: 100%;" class="form-control modal-estimated-arrival-time-dropdown modal-rate-details-dropdowns" name="BookedItems[<?php echo $model->id?>][estimated_arrival_time_id]" >
                                            <?php
                                                if(!empty($model->provider_id))
                                                {
                                                    $estimated_arrival_times = EstimatedArrivalTimes::find()->where(['time_group' => $model->provider->bookingPolicies->time_group])->all();

                                                    if(!empty($estimated_arrival_times))
                                                    {
                                                        foreach ($estimated_arrival_times as $key =>  $obj) 
                                                        {
                                                            $start_time = '';

                                                            if(empty($obj->text))
                                                            {
                                                                $start_time = date('H:i',strtotime($obj->start_time));
                                                            }
                                                            else
                                                            {
                                                                $start_time = $obj->text;
                                                            }
                                                            $obj->end_time = date('H:i',strtotime($obj->end_time));

                                                            $selected = ($obj->id==$model->estimated_arrival_time_id) ? 2 : 0 ;
                                                            
                                                            echo "<option value='".$obj->id."' ".$Options[$selected].">".$start_time." - ".$obj->end_time."</option>";  
                                                        }
                                                    }  
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php 
                                        if(!empty($model->provider_id))
                                        {
                                            $get_special_requests = BookingSpecialRequests::find()->where(['booking_item_id' => $model->id])->all();

                                            $checked_special_requests = [];

                                            if(!empty($get_special_requests))
                                            {
                                                foreach ($get_special_requests as $key => $value) 
                                                {
                                                    $checked_special_requests[] = $value['request_id'];
                                                }
                                            }

                                            echo $this->render('update_group_special_requests',
                                            [
                                                'provider_id' => $model->provider_id,
                                                'checked_special_requests' => $checked_special_requests,
                                                'model_id' => $model->id,
                                            ]);
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CustomReports */

$this->title = 'Update Custom Reports: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Custom Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="custom-reports-update">

    <?= $this->render('update_form', [
        'model' => $model,
        'columns' =>$columns,
        'defaultColumns' => $defaultColumns
    ]) ?>

</div>

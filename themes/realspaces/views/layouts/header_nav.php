<?php
use yii\helpers\Url;
use common\models\States;
use yii\helpers\ArrayHelper;
?>

<header class="site-header">
    <div class="top-header hidden-xs">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-6">
            <ul class="horiz-nav pull-left">
              <?php if(!Yii::$app->user->isGuest): ?>
                <li class="dropdown">
                    <a data-toggle="dropdown"><i class="fa fa-user"></i>
                        &nbsp;<?php echo isset(Yii::$app->user->identity->username)? Yii::$app->user->identity->username: 'Guest'; ?>&nbsp; 
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php if(isset(Yii::$app->user->identity) && Yii::$app->user->identity->type == \common\models\User::USER_TYPE_PMANAGER): ?>
                            <li><a href="<?= \yii\helpers\Url::to(['//provider-admin']) ?>">Dashboard</a></li>
                        <?php endif; ?>
                        
                        <li><a href="<?= \yii\helpers\Url::to(['/user-profile']) ?>">My Profile</a></li>
                        <!--<li><a href="<?= \yii\helpers\Url::to(['/site/change-password']) ?>">Change Password</a></li>-->
                        <li><a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>">Logout</a></li>
                    </ul>
                </li>
              <?php else : ?>
                <li><a href="<?= \yii\helpers\Url::to(['/site/login']) ?>"><i class="fa fa-user"></i> Login </a></li>
              <?php endif; ?>
          </ul>
      </div>
      <div class="col-md-8 col-sm-6">
        <ul class="horiz-nav pull-right">
          <li><a href="//instagram.com" target="_blank"><i class="fa fa-instagram"></i></a></li>
          <li><a href="//facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="//twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
      </ul>
  </div>
</div>
</div>
</div>
<div class="middle-header">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-8 col-xs-8">
        <h1 class="logo"> 
          <a href="<?= \yii\helpers\Url::to(['/site/index']) ?>">
            <img style="max-height:50px;" src="<?= Yii::getAlias('@web')?>/frontend/assets/img/oi_logo.png" alt="Logo">
          </a> 
        </h1>
    </div>
    <div class="col-md-8 col-sm-4 col-xs-4">
      <div class="contact-info-blocks hidden-sm hidden-xs">
          <div>
              <i class="fa fa-phone"></i> Free Line For You
              <span>080 378678 90</span>
          </div>
          <div>
              <i class="fa fa-envelope"></i> Email Us
              <span>sales@realspaces.com</span>
          </div>
          <div>
              <i class="fa fa-clock-o"></i> Working Hours
              <span>09:00 to 17:00</span>
          </div>
      </div>
      <a href="#" class="visible-sm visible-xs menu-toggle"><i class="fa fa-bars"></i></a>
  </div>
</div>
</div>
</div>
<div class="main-menu-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav class="navigation">
          <ul class="sf-menu">
            <li><a href="javascript:void(0);">Regions</a>
              <ul class="dropdown">
                <?php
                    $regions = States::find()->where(['country_id' => 100, 'on_main' => 1])->orderBy('name')->all();
                    foreach ($regions as $region) { ?>
                      <li><a href="<?= Url::to(['/search/region-destinations', 'region_name' => $region->alias]) ;?>"> <?= $region->name ;?> </a></li>
                    <?php } ?>
              </ul>
            </li>
            <li><a href="<?= Url::to(['/search/items?destination_type=11']) ?>">Accommodation</a></li>
            <li><a href="<?= Url::to(['/search/items?destination_type=6']) ?>">Restaurants</a></li>
            <li><a href="<?= Url::to(['/#']) ?>">Activities</a></li>
            <li><a href="<?= Url::to(['/#']) ?>">Sights</a></li>
            <li><a href="<?= Url::to(['/#']) ?>">Tours</a></li>
            <li><a href="<?= Url::to(['/#']) ?>">Guides</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>
</header>
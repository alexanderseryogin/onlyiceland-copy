<?php

namespace backend\controllers;

use Yii;
use common\models\BookingOverrideStatuses;
use common\models\BookingOverrideStatusesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\BaseController;
use common\models\BookingsItems;
use common\models\DestinationsOpenHoursExceptions;
use common\models\Destinations;
use common\models\BookingDates;
use yii\helpers\Url;
use common\models\Notifications;
use yii\filters\AccessControl;
use common\models\BookingOverrideStatusDetails;
use common\models\BookableItemsNames;
use common\models\BookableItems;
/**
 * BookingOverrideStatusesController implements the CRUD actions for BookingOverrideStatuses model.
 */
class BookingOverrideStatusesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
        ];
    }

    /**
     * Lists all BookingOverrideStatuses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookingOverrideStatusesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BookingOverrideStatuses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BookingOverrideStatuses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BookingOverrideStatuses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BookingOverrideStatuses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BookingOverrideStatuses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionOverride($destination_id = null, $exp_date = null)
    {
        if($exp_date == NULL)
        {
            $date = date("Y-m-d");
            //$date = explode("-", $current_date);
            //$date = $date[2];
        }
        else
        {
            $date = $exp_date;
            //$date = $date[2];   
        }
        // echo "<pre>";
        // print_r($date[2]);
        // exit();
        return $this->render('override_statuses',[
            'destination_id' => $destination_id,
           // 'bookable_item_id' => $bookable_item_id,
            'date'  => $date,
            // 'date'  => 25-1,
        ]);
    }

    

    public function actionGetDestinationBookedItemsSchedular() // use for Schedular
    {
        $session = Yii::$app->session;
        $session->open();
        
        $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
                                ->all();
    
        $bookings_items_arr = [];

        // if(!empty($bookingsItemsModel))
        // {
            // $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            // $closed_dates_arr = array();
            // if(!empty($closed_dates))
            // {
            //     foreach ($closed_dates  as $date) 
            //     {
            //         array_push($closed_dates_arr, $date->date);
            //     }
            // }

            $schedular_resources = $this->generateSchedularResources(Destinations::findOne(['id' => $_POST['destination_id'] ]));

            $override_statuses = BookingOverrideStatusDetails::find()
                                ->where(['destination_id' => $_POST['destination_id']])
                                ->all();
            if(!empty($override_statuses))
            {
                foreach ($override_statuses as $key => $model) 
                {
                    $bookings_items_arr[] = $this->generateSchedularEvent($model);
                }
            }
                

            $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id'] ])
                                ->all();

            foreach ($bookingsItemsModel as $key => $model) 
            {
                $bookings_items_arr[] = $this->generateSchedularBookingEvent($model);
            }

            // $notifications_array = array();

            // $notifications = Notifications::find()->where(['notification_type' => Notifications::UNIT_OVERBOOKING ])->all();

            // if(!empty($notifications))
            // {
            //     foreach ($notifications as $noti) 
            //     {
            //         array_push($notifications_array, $noti->date);
            //     }
            // }
            

            echo json_encode([
                'bookings_items' => json_encode($bookings_items_arr),
                'bookable_items' => $this->renderAjax('bookable_items_scheduler',['provider_id' => $_POST['destination_id']]),
                'resources'     => json_encode($schedular_resources),
                'empty' => 0,
                'date' => date('Y-m-d'),
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
            $params['provider_id'] =  $_POST['destination_id'];
            $session[Yii::$app->controller->id.'-booking-schedular-values'] = json_encode($params);
                
        // }
        // else
        // {
        //     echo json_encode([
        //         'empty' => 1,
        //     ]); 
        // }
    }

    public function generateSchedularResources($destination)
    {
        $resources = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                $item_names_count = count($bookable_item->bookableItemsNames);
                $counter = 1;
                foreach ($bookable_item->bookableItemsNames as $item_name) 
                {   
                    $item_array = array();
                    $item_array['id'] = $item_name->id;
                    $item_array['item'] = $bookable_item->itemType->name;
                    $item_array['title'] = $item_name->item_name;
                    $item_array['eventColor'] = $bookable_item->background_color;
                    $item_array['bookable_item_id'] = $bookable_item->id;
                    $item_array['bookable_item_name_id'] = $item_name->id;
                    $item_array['background_color'] = $bookable_item->background_color;
                    $item_array['text_color'] = $bookable_item->text_color;

                    array_push($resources, $item_array);
                    // if($counter == $item_names_count)
                    // {
                    //     $item_array = array();
                    //     $item_array['id'] = $bookable_item->id.$destination->id;
                    //     $item_array['item'] = $bookable_item->itemType->name;
                    //     $item_array['title'] = "No Unit Assigned";
                    //     $item_array['eventColor'] = $bookable_item->background_color;
                    //     $item_array['bookable_item_id'] = $bookable_item->id;
                    //     $item_array['background_color'] = $bookable_item->background_color;
                    //     $item_array['text_color'] = $bookable_item->text_color;

                    //     array_push($resources, $item_array);
                    // }
                    $counter++;
                }
            }
            return $resources;
        }
    }

    public function generateSchedularEvent($model)
    {   
        // if(empty($model->bookingOverrideStatus))
        // {
        //     echo "<pre>";
        //     print_r($model);
        //     exit();
        // }
        $booking_item['id'] = $model->id;
        $booking_item['booking_override_status_id'] = $model->bookingOverrideStatus->id;
        $booking_item['title'] = $model->bookingOverrideStatus->label;
        $booking_item['resourceId'] = $model->bookableItemName->id;
        $booking_item['start'] = $model->date;
        $booking_item['end'] = $model->date;
        $booking_item['borderColor'] = '#000000';
        $booking_item['textColor'] = $model->bookingOverrideStatus->text_color;
        $booking_item['backgroundColor'] = $model->bookingOverrideStatus->background_color;
        $booking_item['is_override_status'] = 1;
        $booking_item['sort_value'] = 1;
        // $booking_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);

        return $booking_item;
    }

    public function generateSchedularBookingEvent($model)
    {
        $booking_item['id'] = $model->id;
        $booking_date = BookingDates::findOne(['booking_item_id' => $model->id]);
        $name = '';
        $name .= !empty($booking_date->guest_first_name)?$booking_date->guest_first_name.' ':'';
        $name .= !empty($booking_date->guest_last_name)?$booking_date->guest_last_name:'';

        if(empty($name))
            $name = 'Unknown';

        // removed following line 
        // .$model->bookingItemGroups->bookingGroup->group_name
        
        if(isset($model->itemName))
        {
            $booking_item['title'] = $model->id.' - '.$name;
            $booking_item['resourceId'] = isset($model->itemName)?$model->itemName->id:$model->item_id.$model->provider_id;
        }
        else
        {
            $booking_item['title'] = $model->id.' - NO UNIT ASSIGNED - '.$name;
            $item_name = BookableItemsNames::findOne(['bookable_item_id' => $model->item_id]);
            $booking_item['resourceId'] = $item_name->id;
        }
        
        $booking_item['start'] = $model->arrival_date;
        $booking_item['end'] = $model->departure_date;
        $booking_item['borderColor'] = '#000000';
        $booking_item['textColor'] = $model->item->text_color;
        $booking_item['is_override_status'] = 0;
        $booking_item['sort_value'] = 2;
        $booking_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'override_status_sch']);

        return $booking_item;
    }

     public function actionOverrideStatus()
    {

        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        $bookable_items =  isset($_POST['bookable_items'])?$_POST['bookable_items']:[];
        $bookable_item_name_id = isset($_POST['global_item_name_id'])?$_POST['global_item_name_id']:'';
        $rate_from = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_from'])));
        $rate_to = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_to'])));
        $rate_days = isset($_POST['rate_days'])?$_POST['rate_days']:[];
        // $override_type = isset($_POST['override_type'])?1:0;
        // $cpricing = isset($_POST['cpricing'])?$_POST['cpricing']:[];

        $date1=date_create($rate_from);
        $date2=date_create($rate_to);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");


        if(empty($_POST['override_status']))
        {
            // for ($i=0; $i <= $difference ; $i++)
            // {
                
            //     // if(strtotime($date)>= strtotime($rate_from) && (strtotime($date)<= strtotime($rate_to)) )
            //     // {
            //         $day = date('N', strtotime($date));
            //         if($rate_days[$day-1] == 'true')
            //         {

            //             $status_override = BookingOverrideStatusDetails::findOne(['date' => $date, 'bookable_item_id' => $bookable_item_name->bookableItem->id,'bookable_item_name_id' => $bookable_item_name->id]);
            //             if(empty($status_override))
            //             {
            //                 $status_override = new BookingOverrideStatusDetails;
            //             }
                        
            //             $status_override->destination_id = $destination->id;
            //             $status_override->bookable_item_id = $bookable_item_name->bookableItem->id;
            //             $status_override->bookable_item_name_id = $bookable_item_name->id;
            //             $status_override->date = $date;
            //             $status_override->booking_override_status_id = $_POST['override_status'];
 
            //             if(!$status_override->save())
            //             {
            //                 echo "<pre>";
            //                 print_r($status_override->errors);
            //                 exit();
            //             }
                    
            //         }
            //     // }
            //     $date = strtotime("+1 day", strtotime($date));
            //     $date = date("Y-m-d", $date);
            // }
            if(!empty($bookable_items))
            {
                foreach ($bookable_items as $value) 
                {
                    $bookable_item = BookableItems::findOne(['id' => $value]);
                    
                    // if(!empty($rates))
                    // {
                        foreach ($bookable_item->bookableItemsNames as $item_name) 
                        {
                            $date = $rate_from;
                            $date = date("Y-m-d", strtotime($date));
                            for ($i=0; $i <= $difference ; $i++)
                            {
                                
                                // if(strtotime($date)>= strtotime($rate_from) && (strtotime($date)<= strtotime($rate_to)) )
                                // {
                                $day = date('N', strtotime($date));
                                if($rate_days[$day-1] == 'true')
                                {

                                    $status_override = BookingOverrideStatusDetails::findOne(['date' => $date, 'bookable_item_id' => $bookable_item->id,'bookable_item_name_id' => $item_name->id]);
                                    if(!empty($status_override))
                                    {
                                        $status_override->delete();
                                    }
                                    
                                    // $status_override->destination_id = $destination->id;
                                    // $status_override->bookable_item_id = $bookable_item->id;
                                    // $status_override->bookable_item_name_id = $item_name->id;
                                    // $status_override->date = $date;
                                    // $status_override->booking_override_status_id = $_POST['override_status'];

                                    // if(!$status_override->save())
                                    // {
                                    //     echo "<pre>";
                                    //     print_r($status_override->errors);
                                    //     exit();
                                    // }
                                }
                                // }
                                $date = strtotime("+1 day", strtotime($date));
                                $date = date("Y-m-d", $date);
                            }
                        }
                    // }
                    
                }
            }
            else
            {
                $bookable_item_name = BookableItemsNames::findOne(['id' => $bookable_item_name_id]); 
                $date = $rate_from;
                $date = date("Y-m-d", strtotime($date));

                for ($i=0; $i <= $difference ; $i++)
                {
                    
                    // if(strtotime($date)>= strtotime($rate_from) && (strtotime($date)<= strtotime($rate_to)) )
                    // {
                    $day = date('N', strtotime($date));
                    if($rate_days[$day-1] == 'true')
                    {

                        $status_override = BookingOverrideStatusDetails::findOne(['date' => $date, 'bookable_item_id' => $bookable_item_name->bookableItem->id,'bookable_item_name_id' => $bookable_item_name->id]);
                        if(!empty($status_override))
                        {
                            $status_override->delete();
                        }
                        
                        // $status_override->destination_id = $destination->id;
                        // $status_override->bookable_item_id = $bookable_item_name->bookableItem->id;
                        // $status_override->bookable_item_name_id = $bookable_item_name->id;
                        // $status_override->date = $date;
                        // $status_override->booking_override_status_id = $_POST['override_status'];
 
                        // if(!$status_override->save())
                        // {
                        //     echo "<pre>";
                        //     print_r($status_override->errors);
                        //     exit();
                        // }
                    
                    }
                    // }
                    $date = strtotime("+1 day", strtotime($date));
                    $date = date("Y-m-d", $date);
                }
            }
        }
        else
        {
            if(!empty($bookable_items))
            {
                foreach ($bookable_items as $value) 
                {
                    $bookable_item = BookableItems::findOne(['id' => $value]);
                    
                    // if(!empty($rates))
                    // {
                        foreach ($bookable_item->bookableItemsNames as $item_name) 
                        {
                            $date = $rate_from;
                            $date = date("Y-m-d", strtotime($date));
                            for ($i=0; $i <= $difference ; $i++)
                            {
                                
                                if(strtotime($date)>= strtotime($rate_from) && (strtotime($date)<= strtotime($rate_to)) )
                                {
                                    $day = date('N', strtotime($date));
                                    if($rate_days[$day-1] == 'true')
                                    {

                                        $status_override = BookingOverrideStatusDetails::findOne(['date' => $date, 'bookable_item_id' => $bookable_item->id,'bookable_item_name_id' => $item_name->id]);
                                        if(empty($status_override))
                                        {
                                            $status_override = new BookingOverrideStatusDetails;
                                        }
                                        
                                        $status_override->destination_id = $destination->id;
                                        $status_override->bookable_item_id = $bookable_item->id;
                                        $status_override->bookable_item_name_id = $item_name->id;
                                        $status_override->date = $date;
                                        $status_override->booking_override_status_id = $_POST['override_status'];

                                        if(!$status_override->save())
                                        {
                                            echo "<pre>";
                                            print_r($status_override->errors);
                                            exit();
                                        }
                                    }
                                }
                                $date = strtotime("+1 day", strtotime($date));
                                $date = date("Y-m-d", $date);
                            }
                        }
                    // }
                    
                }
            }
            else
            {
                $bookable_item_name = BookableItemsNames::findOne(['id' => $bookable_item_name_id]); 
                $date = $rate_from;
                $date = date("Y-m-d", strtotime($date));

                for ($i=0; $i <= $difference ; $i++)
                {
                    
                    if(strtotime($date)>= strtotime($rate_from) && (strtotime($date)<= strtotime($rate_to)) )
                    {
                        $day = date('N', strtotime($date));
                        if($rate_days[$day-1] == 'true')
                        {

                            $status_override = BookingOverrideStatusDetails::findOne(['date' => $date, 'bookable_item_id' => $bookable_item_name->bookableItem->id,'bookable_item_name_id' => $bookable_item_name->id]);
                            if(empty($status_override))
                            {
                                $status_override = new BookingOverrideStatusDetails;
                            }
                            
                            $status_override->destination_id = $destination->id;
                            $status_override->bookable_item_id = $bookable_item_name->bookableItem->id;
                            $status_override->bookable_item_name_id = $bookable_item_name->id;
                            $status_override->date = $date;
                            $status_override->booking_override_status_id = $_POST['override_status'];
     
                            if(!$status_override->save())
                            {
                                echo "<pre>";
                                print_r($status_override->errors);
                                exit();
                            }
                        
                        }
                    }
                    $date = strtotime("+1 day", strtotime($date));
                    $date = date("Y-m-d", $date);
                }
            }
        }
            
            

        $schedular_resources = $this->generateSchedularResources($destination);

        $bookings_items_arr = [];
        $override_statuses = BookingOverrideStatusDetails::find()
                                ->where(['destination_id' => $_POST['destination_id']])
                                ->all();
        if(!empty($override_statuses))
        {
            foreach ($override_statuses as $key => $model) 
            {
                $bookings_items_arr[] = $this->generateSchedularEvent($model);
            }
        }

        $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id'] ])
                                ->all();

        foreach ($bookingsItemsModel as $key => $model) 
        {
            $bookings_items_arr[] = $this->generateSchedularBookingEvent($model);
        }
        // exit();
        // $dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        // $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        echo json_encode([
                'success' => 'true',
                'bookings_items' => $bookings_items_arr,
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                'resources'     => $schedular_resources,
                'empty' => 0,
                'date' => $rate_from,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

    /**
     * Finds the BookingOverrideStatuses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BookingOverrideStatuses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BookingOverrideStatuses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

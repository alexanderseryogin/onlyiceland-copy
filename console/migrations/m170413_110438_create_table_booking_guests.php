<?php

use yii\db\Migration;

class m170413_110438_create_table_booking_guests extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_guests`(  
                          `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                          `first_name` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci,
                          `last_name` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci,
                          `country_id` INT(11),
                          PRIMARY KEY (`id`),
                          CONSTRAINT `FK_booking_guests_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170413_110438_create_table_booking_guests cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_financial".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property string $email
 * @property string $bank_info
 * @property string $account_holder_name
 * @property string $account_number
 * @property string $owning_entity
 * @property string $address
 * @property integer $country_id
 * @property integer $city_id
 * @property integer $state_id
 * @property string $postal_code
 * @property string $vat_rate
 * @property string $license_type
 * @property string $license_expiration_date
 * @property string $license_image
 *
 * @property Cities $city
 * @property Countries $country
 * @property Providers $provider
 * @property States $state
 */
class DestinationsFinancial extends \yii\db\ActiveRecord
{
    public $pdfFile;
    public $server_lodging_tax_per_night;
    public $server_vat_rate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_financial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        
            [['license_expiration_date'], 'required'],
            [['license_expiration_date','server_lodging_tax_per_night','server_vat_rate'], 'integer'],
            [['bank_info','vat_rate', 'license_type', 'license_image'], 'string', 'max' => 256],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['pdfFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination ID'),
            'bank_info' => Yii::t('app', 'Bank Account Info'),
            'vat_rate' => Yii::t('app', 'Vat Rate'),
            'license_type' => Yii::t('app', 'License Type'),
            'license_expiration_date' => Yii::t('app', 'License Expiration Date'),
            'license_image' => Yii::t('app', 'License Image'),
            'pdfFile' => Yii::t('app', 'License Image (Destination license)'),
            'lodging_tax_per_night' => Yii::t('app', 'Lodging Tax per Night'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

}

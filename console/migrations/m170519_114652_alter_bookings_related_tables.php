<?php

use yii\db\Migration;

class m170519_114652_alter_bookings_related_tables extends Migration
{
    public function up()
    {
        $this->execute("
          ALTER TABLE `bookings_discount_codes`   
          DROP COLUMN `bookings_items_id`, 
          ADD COLUMN `booking_date_id` INT(11) UNSIGNED NULL AFTER `id`, 
          DROP INDEX `FK_bookings_discount_codes_bookings_items_id`,
          DROP FOREIGN KEY `FK_bookings_discount_codes_bookings_items_id`,
          ADD CONSTRAINT `FK_bookings_discount_codes_bdate_id` FOREIGN KEY (`booking_date_id`) REFERENCES `booking_dates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE;
          ALTER TABLE `booking_special_requests`   
          DROP COLUMN `booking_item_id`, 
          ADD COLUMN `booking_date_id` INT(11) UNSIGNED NULL AFTER `request_id`, 
          DROP INDEX `FK_bsr_booking_item_id`,
          DROP FOREIGN KEY `FK_bsr_booking_item_id`,
          ADD CONSTRAINT `FK_bsr_bdate_id` FOREIGN KEY (`booking_date_id`) REFERENCES `booking_dates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE;
          ALTER TABLE `bookings_upsell_items`   
          DROP COLUMN `bookings_items_id`, 
          ADD COLUMN `booking_date_id` INT(11) UNSIGNED NULL AFTER `id`,
          CHANGE `upsell_id` `upsell_id` INT(11) NULL  AFTER `booking_date_id`, 
          DROP INDEX `FK_bookings_upsell_items_bookings_items_id`,
          DROP FOREIGN KEY `FK_bookings_upsell_items_bookings_items_id`,
          ADD CONSTRAINT `FK_bui_bdate_id` FOREIGN KEY (`booking_date_id`) REFERENCES `booking_dates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE;

        ");
    }

    public function down()
    {
        echo "m170519_114652_alter_bookings_related_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

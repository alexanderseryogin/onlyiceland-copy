<?php

use yii\db\Migration;

class m161114_112347_alter_providers_add_tags_accomodations extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` ADD COLUMN `accommodation_id` INT(11) NULL AFTER `provider_type_id`, ADD COLUMN `tag_id` INT(11) NULL AFTER `accommodation_id`, ADD CONSTRAINT `FK_providers_accommodation_id` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation_types`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_providers_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags`(`id`) ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m161114_112347_alter_providers_add_tags_accomodations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

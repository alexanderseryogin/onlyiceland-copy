<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_capacity_pricing".
 *
 * @property string $id
 * @property integer $rates_id
 * @property integer $person_no
 * @property integer $price
 *
 * @property Rates $rates
 */
class RatesCapacityPricing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_capacity_pricing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rates_id', 'person_no', 'price'], 'integer'],
            [['rates_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rates_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rates_id' => Yii::t('app', 'Rates ID'),
            'person_no' => Yii::t('app', 'Person No'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rates_id']);
    }
}

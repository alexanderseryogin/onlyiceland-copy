<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_override".
 *
 * @property integer $id
 * @property integer $rate_id
 * @property integer $bookable_item_id
 * @property string $date
 * @property integer $override_type
 * @property double $single
 * @property double $double
 * @property double $triple
 * @property double $quad
 * @property double $item_price_first_adult
 * @property double $item_price_additional_adult
 * @property double $item_price_first_child
 * @property double $item_price_additional_child
 *
 * @property Rates $rate
 * @property BookableItems $bookableItem
 */
class RatesOverride extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_override';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate_id', 'bookable_item_id', 'override_type'], 'integer'],
            [['date','bookable_item_closed'], 'safe'],
            [['single', 'double', 'triple', 'quad', 'item_price_first_adult', 'item_price_additional_adult', 'item_price_first_child', 'item_price_additional_child'], 'number'],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
            [['bookable_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rate_id' => 'Rate ID',
            'bookable_item_id' => 'Bookable Item ID',
            'date' => 'Date',
            'override_type' => 'Override Type',
            'single' => 'Single',
            'double' => 'Double',
            'triple' => 'Triple',
            'quad' => 'Quad',
            'item_price_first_adult' => 'Item Price First Adult',
            'item_price_additional_adult' => 'Item Price Additional Adult',
            'item_price_first_child' => 'Item Price First Child',
            'item_price_additional_child' => 'Item Price Additional Child',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_item_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unit_availability".
 *
 * @property integer $id
 * @property integer $bookable_item_id
 * @property string $unit_id
 * @property string $date
 *
 * @property BookableItems $bookableItem
 * @property BookableItemsNames $unit
 */
class UnitAvailability extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit_availability';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookable_item_id', 'unit_id'], 'integer'],
            [['date'], 'safe'],
            [['bookable_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_item_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItemsNames::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bookable_item_id' => 'Bookable Item ID',
            'unit_id' => 'Unit ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(BookableItemsNames::className(), ['id' => 'unit_id']);
    }
}

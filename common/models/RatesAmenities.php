<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_amenities".
 *
 * @property string $id
 * @property integer $rates_id
 * @property string $amenities_id
 * @property integer $type
 *
 * @property Amenities $amenities
 * @property Rates $rates
 */
class RatesAmenities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_amenities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rates_id', 'amenities_id', 'type'], 'integer'],
            [['amenities_id'], 'exist', 'skipOnError' => true, 'targetClass' => Amenities::className(), 'targetAttribute' => ['amenities_id' => 'id']],
            [['rates_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rates_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rates_id' => Yii::t('app', 'Rates ID'),
            'amenities_id' => Yii::t('app', 'Amenities ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenities()
    {
        return $this->hasOne(Amenities::className(), ['id' => 'amenities_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rates_id']);
    }
}

<?php

use yii\db\Migration;

class m170614_063035_create_table_destination_travel_partners_change_field_name extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `travel_partner`   
                          CHANGE `business_contact` `billing_contact` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL;
                        ');
    }   

    public function down()
    {
        echo "m170614_063035_create_table_destination_travel_partners_change_field_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161006_114319_drop_type_payment_method extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `payment_methods` DROP COLUMN `type`;");
    }

    public function down()
    {
        echo "m161006_114319_drop_type_payment_method cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

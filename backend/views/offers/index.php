<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\Destinations;
use common\models\BookingConfirmationTypes;
use common\models\BookableItems;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OffersSearch */
/* @var $dataDestination yii\data\ActiveDataDestination */

$this->title = Yii::t('app', 'Offers');
$this->params['breadcrumbs'][] = $this->title;
$update_page_url = Url::to(['/offers/update']);
?>
<style type="text/css">
    tr:hover{
        cursor: pointer;
    }

</style>
<div class="offers-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW OFFER '), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(['id' => 'offers-gridview','timeout' => 10000, 'enablePushState' => false]); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'provider_id',
                'label' => 'Destination Name',
                'value' => function($data)
                {
                    if(isset($data->destination->name) && !empty($data->destination->name))
                        return $data->destination->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'OffersSearch[provider_id]', 
                    $searchModel['provider_id'], 
                    ArrayHelper::map(Destinations::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Destination', 
                        'id'=>'provider_dropdown', 
                        'class'=>'form-control',
                        /*'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('providers/listitems?provider_id=').'"+$(this).val(), function( data ) 
                                                {
                                                    $("#item_dropdown").html( data );
                                                });'*/
                    ]),
            ],

            [
                'attribute' => 'item_id',
                'label' => 'Item Name',
                'value' => function($data)
                {
                    if(isset($data->item->itemType->name) && !empty($data->item->itemType->name))
                        return $data->item->itemType->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'OffersSearch[item_id]', 
                    $searchModel['item_id'], 
                    ArrayHelper::map(BookableItems::find()->all(),'id','itemType.name','destination.name'), 
                    [
                        'prompt' => 'Select an Item', 
                        'id'=>'item_dropdown', 
                        'class'=>'form-control'
                    ]),
            ],
            [
                'attribute' => 'booking_confirmation_type_id',
                'label' => 'Booking Confirmation Type',
                'value' => function($data)
                {
                    if(isset($data->bookingConfirmationType->name) && !empty($data->bookingConfirmationType->name))
                        return $data->bookingConfirmationType->name;
                    else
                        return '';
                },
                'filter' => Html::dropDownList(
                    'OffersSearch[booking_confirmation_type_id]', 
                    $searchModel['booking_confirmation_type_id'], 
                    ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select a Type', 
                        'id'=>'type_dropdown', 
                        'class'=>'form-control'
                    ]),
            ],
            'min_stay',
            'commission',

            [   
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>['style' => 'min-width:70px;'],
                'headerOptions' =>['style' => 'min-width:70px;'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#item_dropdown').select2({
        placeholder: \"Select an Item\",
        allowClear: true
    });

    $('#offers-gridview').on('pjax:end', function() {

        $('#type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
            });

        $('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
            });

        $('#item_dropdown').select2({
            placeholder: \"Select an Item\",
            allowClear: true
        });
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

    /*var provider_id = $('#provider_dropdown').val();

    if(provider_id!='')
    {
        $('#provider_dropdown').val(provider_id).trigger('change');
    }*/

    $(document).on('click','.table-striped tbody tr',function()
    {
        window.location.replace('$update_page_url?id='+$(this).attr('data-key'));
    });

});",View::POS_END);
?>

<?php

use yii\db\Migration;

class m161006_073723_rename_payment_method extends Migration
{
    public function up()
    {
        $this->execute("RENAME TABLE `property_payment_methods` TO `payment_methods`; 
        ");
    }

    public function down()
    {
        echo "m161006_073723_rename_payment_method cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

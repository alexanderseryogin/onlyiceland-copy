<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Countries;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\States */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="states-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_id', [
    	'inputOptions' => [
    		'id' => 'country-dropdown',
    		'class' => 'form-control',
    		'style' => 'max-width: 500px'
    		]
    	])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name')) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'button_text')->textInput(['maxlength' => true]) ?>

    <?php
    if(!empty($model->img)) { ?>
        <img src='<?= \Yii::$app->urlManagerFrontend->createAbsoluteUrl('') . 'uploads' . $model->img; ?>' height="120" width="150">
        <br>
    <?php } ?>

    <?= $form->field($model, 'img')->fileInput() ?>

    <?php $order_count = \common\models\States::find()->where(['country_id' => 100])->count();
        $order_count = range(1, $order_count);
        $oC = [];
        for ($i = 0; $i < count($order_count); $i++) {
            $oC[$i + 1] = $order_count[$i];
        }
    ?>
    <?= $form->field($model, 'order')->dropDownList(
        $oC
    ,
    [
        'prompt' => 'Number...'
    ]) ?>

    <?= $form->field($model, 'on_main')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/states']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
jQuery(document).ready(function() {
    // select2 for country list
    $('#country-dropdown').select2({
        placeholder: \"Select Country\"
    });
});",View::POS_END);
?>
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attraction_type_fields".
 *
 * @property integer $id
 * @property string $type
 * @property integer $at_id
 * @property integer $required
 * @property string $label
 *
 * @property AttractionTypes $at
 */
class AttractionTypeFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attraction_type_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['at_id', 'required'], 'integer'],
            [['type', 'label'], 'string', 'max' => 256],
            [['at_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttractionTypes::className(), 'targetAttribute' => ['at_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'at_id' => Yii::t('app', 'At ID'),
            'required' => Yii::t('app', 'Required'),
            'label' => Yii::t('app', 'Label'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAt()
    {
        return $this->hasOne(AttractionTypes::className(), ['id' => 'at_id']);
    }

    public function getAtfd()
    {
        return $this->hasOne(AttractionTypeFieldsData::className(), ['atf_id' => 'id']);
    }
}

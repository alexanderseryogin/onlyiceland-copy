<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

use common\models\DestinationsImages;

?>

<?php Pjax::begin(['id' => 'images-gridview','timeout' => 3000000, 'enablePushState' => false]) ?>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'label' => 'Images',
            'format' =>'raw',
            'value' => function($data)
            {
                if(!empty($data))
                {
                    $path = Yii::getAlias('@web').'/../uploads/Destinations/'.$data->provider_id.'/images/'.$data->image;
                
                    return '<a name="pop[]"> <img src="'.$path.'" style="max-height:100px" > </a>';
                }
            },
        ],

        [
            'label' => 'Description',
            'value' => function($data)
            {
                return $data->description;
            },
            'contentOptions' => ['style' => 'max-width:200px; overflow: hidden; text-overflow: ellipsis;']
        ],

        [
                'label' => 'Order',
                'format' => 'raw',
                'value' => function($data)
                {
                    if($data->isFirstRecord())
                    {
                        return '<div style="display:inline-block">
                                <span class="badge badge-primary">'.$data->display_order.'</span>
                            </div>
                            <div style="display:inline-block;">
                                <a name="down[]" style="text-decoration: none;" data-id='.$data->id.'>
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                </a>
                            </div>';
                    }
                    else if($data->isLastRecord())
                    {
                       return '<div style="display:inline-block">
                                <span class="badge badge-primary">'.$data->display_order.'</span>
                            </div>
                            <div style="display:inline-block;">
                                <a name="up[]" style="text-decoration: none;" data-id='.$data->id.'>
                                    <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                </a>
                            </div>';
                    }
                    else
                    {
                        return '<div style="display:inline-block">
                                <span class="badge badge-primary">'.$data->display_order.'</span>
                            </div>
                            <div style="display:inline-block;">
                                <a name="up[]" style="text-decoration: none;" data-id='.$data->id.'>
                                    <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                </a>
                                <a name="down[]" style="text-decoration: none;" data-id='.$data->id.'>
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                </a>
                            </div>';
                    }
                },
                'contentOptions' => ['style' => 'max-width: 30px;']
        ],

          ['class' => 'yii\grid\ActionColumn',
          'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                       return '<a style="text-decoration: none;" aria-label="update" data-id='.$model->id.' title="update" name="update[]">
                                <span class="glyphicon glyphicon-pencil"></span>
                                </a>';
                  },
                  'delete' => function ($url, $model, $key) {
                       return '<a style="text-decoration: none;" aria-label="delete" data-id='.$model->id.' title="delete" name="delete[]">
                                <span class="glyphicon glyphicon-trash"></span>
                                </a>';
                  },
              ]
        ],
    ],
]); ?>

<?php Pjax::end() ?>
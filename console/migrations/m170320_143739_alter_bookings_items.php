<?php

use yii\db\Migration;

class m170320_143739_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` ADD COLUMN `beds_combinations_id` INT(11) UNSIGNED NULL AFTER `price_json`, ADD CONSTRAINT `bookings_items_beds_combinations_id` FOREIGN KEY (`beds_combinations_id`) REFERENCES `beds_combinations`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
        ");
    }

    public function down()
    {
        echo "m170320_143739_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

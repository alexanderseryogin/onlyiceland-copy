<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Destinations;
use common\models\BookableItems;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\BookingStatuses;
use yii\web\View;
use yii\helpers\Url;
use common\models\Rates;
use common\models\BookingDates;
use common\models\BookingTypes;
use kartik\date\DatePicker;
use common\models\BookableItemsNames;
use common\models\TravelPartner;
use common\models\HousekeepingStatus;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingsItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manage Housekeeping Statuses');
$this->params['breadcrumbs'][] = $this->title;
$update_page_url = Url::to(['/bookings/update']);
$unset_url = Url::to(['/house-keeping/unset-date-range-session']);

$Checkbox = [
    0 => '',
    1 => 'checked'
];

$BookingTypes = [
    0 => 'Active',
    1 => 'Deleted'
];

// ***************** show or hide filters div **************** //

if($filter_flag)
{
    $this->registerJs("
            $('#filters_div').removeClass('hide-filters');
            $('#filters_div').addClass('show-filters');");
}

// ***************** show or hide group field **************** //

if(!$group_flag)
{
    $this->registerJs("
            $('.group-col').addClass('hide');");
}
else
{
    $this->registerJs("
            $('.group-col').removeClass('hide');");
}

// ***************** set selected value of date range picker **************** //

$session = Yii::$app->session;
$session->open();

$bookingDates = BookingDates::find()->orderBy('date DESC')->one();
if(!empty($bookingDates))
{
    $last_booking_date = $bookingDates->date;
}
else
{
    $last_booking_date = date('Y-m-d');
}


if(isset($session[Yii::$app->controller->id.'-grid-start-date']) && !empty($session[Yii::$app->controller->id.'-grid-start-date']))
{
    $startDate =  date('d-m-Y',strtotime($session[Yii::$app->controller->id.'-grid-start-date']));
}
else
{
    $startDate = date('d-m-Y');
}

// $this->registerJs("
//     var start = '$startDate';
// ");

?>

<style type="text/css">
    .show-filters{ display: block; }
    .hide-filters{ display: none; }
    tr:hover{
        cursor: pointer;
    }

    .grid-view .filters input, .grid-view .filters select {
        width: 65px;
    }

    .grid-view td {
        white-space: pre-wrap;
    }

</style>

<script type="text/javascript">
    var server_group_flag = '<?php echo $group_flag ?>';
</script>

<div class="bookings-index main-div">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW BOOKING'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
        <div class="pull-right" style="margin: 8px;">
            Filters &nbsp;<input type="checkbox" id="filters_switch" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size="mini" <?php echo $Checkbox[$filter_flag]  ?>>
        </div>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <div id="booking-range-picker" class="tooltips btn btn-default" data-container="body" data-placement="bottom" data-original-title="Change Booking date range">
        <i class="icon-calendar"></i>&nbsp;
        <span class="thin uppercase hidden-xs"></span>&nbsp;
        <i class="fa fa-angle-down"></i>
    </div> -->
    <?php $form = ActiveForm::begin([
        'id' => 'booking_form',
    ]);?>

    <div class="row">
        <div class="col-md-2">
            <input name='start_date' id="booking-range-picker" class="form-control form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=$startDate?>">
        </div>
        <div class="col-md-2" style="margin-left: -90px;margin-top:1px;">
             <a href="#" id="refresh_btn" class="btn btn-default btn-square"><i class="fa fa-refresh" aria-hidden="true" style="float: right;"></i></a>
        </div>
        
       <!--  <span class="help-block"> Select date </span> -->
    </div>
   

    

        <div class="hide-filters" id="filters_div">
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-sliders"></i>
                                <span class="caption-subject bold uppercase"> Booking Filters </span>
                            </div>
                            <div class="inputs">
                                <div class="portlet-input input-inline input-medium">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <input class="btn btn-warning pull-right" type="submit" value="Update">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php
                                        if(!empty($booking_status))
                                        {
                                            echo '<h4>Booking Status</h4>';
                                            foreach ($booking_status as $key => $value) 
                                            {
                                                $checked = 0;

                                                if(!empty($selected_status) && in_array($value->id, $selected_status))
                                                {
                                                    $checked = 1;
                                                }
                                                echo '<label><input name="Bookings[booking_status][]"  value="'.$value->id.'" style="margin-left:10px;" type="checkbox" '.$Checkbox[$checked].'>&nbsp;'.$value->label.'</label>';
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <h4>Booking Group </h4>
                                    <input style="display: inline;" type="checkbox" name="Bookings[group_switch]" id="group_switch" class="make-switch pull-right" data-on-color="success" data-off-color="warning" data-on-text="On" data-off-text="Off" data-size="mini" <?php echo $Checkbox[$group_flag]  ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Portlet PORTLET-->
                </div>
            </div>
        </div>

        <input type="text" id="rangepicker_status" name="Bookings[daterangepicker]" value="0" hidden>
    <?php ActiveForm::end(); ?>



<?php Pjax::begin(['id' => 'housekeeping-status-gridview','timeout' => 1000000, 'enablePushState' => false]); ?>    

<?= GridView::widget([
        'id' => 'bookings_gridview',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view'],
        'columns' => [

            [
                'attribute' => 'id',
                'label' => 'Number',
                'value' => function($data)
                {
                    return $data->id;
                },
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
                'contentOptions' => function($data)
                {
                    return ['style' => 'text-align:right;width:3%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
                //'headerOptions' =>['style' => 'width:30px;'],
            ],
            [
                'format' =>'raw',
                'label' => 'HK',
                'value' => function($data)
                {
                    $all_status_types = HousekeepingStatus::find()->all();
                    // echo "<pre>";
                    // print_r($all_status_types);
                    // exit();

                    // foreach($all_status_types as $status_type) 
                    // {
                    //     $path = Yii::getAlias('@web').'/../uploads/housekeeping-status/'.$status_type->id.'/icons/'.$status_type->icon;
                    //     echo $path."<br>";

                    //    // $html  += '<a> <img src="'.$path.'" style="max-height:50px" > </a>';
                    // }
                    // exit();
                    $html = '';
                    if(!empty($all_status_types))
                    {
                        $housekeeping_status_objects = $data->bookingHousekeepingStatus;
                        $housekeeping_status_array = array();
                        if(!empty($housekeeping_status_objects))
                        {
                            foreach ($housekeeping_status_objects as $value) 
                            {
                                array_push($housekeeping_status_array, $value->housekeeping_status_id);
                            }
                        }
                        foreach($all_status_types as $status_type) 
                        {
                            $path = Yii::getAlias('@web').'/../uploads/housekeeping-status/'.$status_type->id.'/icons/'.$status_type->icon;
                            if(in_array($status_type->id, $housekeeping_status_array))
                            {
                                if($status_type->id == 1)
                                {
                                    $html.='<a data-toggle="tooltip" title="'.$status_type->label.'"> <img id="'.$status_type->id.':'.$data->id.'" class="status_icon hk1 exists hk1-'.$data->id.'" src="'.$path.'" style="max-height:30px;border:2px solid red" > </a>';
                                }
                                else
                                {
                                    $html.='<a data-toggle="tooltip" title="'.$status_type->label.'"> <img id="'.$status_type->id.':'.$data->id.'" class="status_icon exists hk2 hk2-'.$data->id.'" src="'.$path.'" style="max-height:30px;border:2px solid red" > </a>';
                                }
                                
                            }
                            else
                            {
                                if($status_type->id == 1)
                                {
                                    $html.='<a data-toggle="tooltip" title="'.$status_type->label.'"> <img id="'.$status_type->id.':'.$data->id.'" class="status_icon hk1 hk1-'.$data->id.'" src="'.$path.'" style="max-height:30px" > </a>';
                                }
                                else
                                {
                                    $html.='<a data-toggle="tooltip" title="'.$status_type->label.'"> <img id="'.$status_type->id.':'.$data->id.'" class="status_icon hk2 hk2-'.$data->id.'" src="'.$path.'" style="max-height:30px" > </a>';
                                }
                                
                            }                               
                            
                            //echo $html."<br>";
                        }
                    }
                    // echo "<pre>";
                    // print_r($html);
                    // exit();
                    return $html;
                },
               // 'headerOptions' =>['style' => 'width:70px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:10%;' ]; 

                },
            ],
            [
                'attribute' => 'group_name',
                //'contentOptions' => ['class' => 'hide'],
                //'headerOptions' => ['class' => 'hide'],
                //'filterOptions' => ['class' => 'hide'],
                'label' => 'Group',
                'value' => function($data)
                {
                    return $data->bookingItemGroups->bookingGroup->group_name;
                },
                'headerOptions' =>[ 'class' => 'group-col'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:5%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';', 'class' => 'group-col' ];
                },
                'filterOptions' => ['class' => 'group-col'],
                'filterInputOptions' => [
                    'class' => '',
                    'style' => 'width: 100%'
                
                ],
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Status',
                'value' => function($data)
                {
                    if(isset($data->status->label) && !empty($data->status->label))
                        return $data->status->label;
                    else
                        return '';
                },
                /*'filter' => Html::dropDownList(
                    'BookingsItemsSearch[status_id]', 
                    $searchModel['status_id'], 
                    ArrayHelper::map(BookingStatuses::find()->all(),'id','label'), 
                    [
                        'prompt' => 'Select a Status', 
                        'id'=>'status_dropdown',
                        'style' => 'width:100%',
                    ]),*/
                'contentOptions' => function($data)
                {
                    if(!empty($data->status_id))
                        return ['style' => 'width:1%; color:'.$data->status->text_color.'; background:'.$data->status->background_color.';' ];
                    else
                       return ['style' => 'width:1%;' ]; 
                },
                //'headerOptions' =>['style' => 'width:75px;'],
            ],
            [
                'attribute' => 'flag_id',
                'label' => 'Flag',
                'value' => function($data)
                {
                    if(isset($data->flag->label) && !empty($data->flag->label))
                        return $data->flag->label;
                    else
                        return '';
                },
                /*'filter' => Html::dropDownList(
                    'BookingsItemsSearch[flag_id]', 
                    $searchModel['flag_id'], 
                    ArrayHelper::map(BookingTypes::find()->all(),'id','label'), 
                    [
                        'prompt' => 'Select a Flag', 
                        'id'=>'flag_dropdown',
                        'style' => 'width:100%',
                    ]),*/
                'contentOptions' => function($data)
                {
                    if(!empty($data->flag_id))
                        return ['style' => 'width:1%; color:'.$data->flag->text_color.'; background:'.$data->flag->background_color.';' ];
                    else
                       return ['style' => 'width:1%;' ];
                },
                //'headerOptions' =>['style' => 'width:75px;'],
            ],
            [
                'attribute' => 'provider_id',
                'label' => 'Destination',
                'value' => function($data)
                {
                    return $data->provider->name;
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[provider_id]', 
                    $searchModel['provider_id'], 
                    ArrayHelper::map(Destinations::find()->where(['use_housekeeping' => 1 ])->all(),'id','name'), 
                    [
                        'prompt' => 'Select Destination', 
                        'id'=>'provider_dropdown',
                        'style' => 'width:100%',
                    ]),
                //'headerOptions' =>['style' => 'width:120px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:8%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            [
                'attribute' => 'item_id',
                'label' => 'Item',
                'value' => function($data)
                {
                    return $data->item->itemType->name;
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[item_id]', 
                    $searchModel['item_id'], 
                    ArrayHelper::map(BookableItems::find()->where(['provider_id' => $searchModel['provider_id']])->all(),'id','itemType.name'), 
                    [
                        'prompt' => 'Select an Item', 
                        'id'=>'item_dropdown',
                        'style' => 'width:100%',
                    ]),
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:7.5%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
                //'headerOptions' =>['style' => 'width:75px;'],
            ],
            [
                'attribute' => 'item_name_id',
                'label' => 'Item Name',
                'value' => function($data)
                {
                    $booking_date = BookingDates::findOne(['booking_item_id' => $data->id]);
                    if(!empty($data->item_name_id))
                        return $data->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
                    else
                        return '- - - - -';
                },
                'filter' => Html::dropDownList(
                    'BookingsItemsSearch[item_name_id]', 
                    $searchModel['item_name_id'], 
                    ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $searchModel['item_id']])->all(),'id','item_name'), 
                    [
                        'prompt' => 'Select an Item Name', 
                        'id'=>'item_name_dropdown',
                        'style' => 'width:100%',
                    ]),
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:6%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
                //'headerOptions' =>['style' => 'width:110px;'],
            ],
            [
                'attribute' => 'arrival_date',
                'label' => 'Arr',
                'format' => 'raw',
                'value' => function($data)
                {
                    $bookingDates = $data->bookingDatesSortedAndNotNull;
                    return date('d.m.Y',strtotime($bookingDates[0]->date)).'<br>'.date('l',strtotime($bookingDates[0]->date));
                },
                //'headerOptions' =>['style' => 'width:80px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:1%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            [
                'attribute' => 'departure_date',
                'label' => 'Dep',
                'format' => 'raw',
                'value' => function($data)
                {
                    $bookingDates = $data->bookingDatesSortedAndNotNull;
                    return date('d.m.Y',strtotime("+1 day", strtotime($bookingDates[count($bookingDates)-1]->date))).'<br>'.date('l',strtotime("+1 day", strtotime($bookingDates[count($bookingDates)-1]->date)));
                },
                //'headerOptions' =>['style' => 'width:80px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:1%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            [
                'label' => '#',
                'value' => function($data)
                {
                    $date1=date_create($data->departure_date);
                    $date2=date_create($data->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a");
                    return $difference;
                },
                //'headerOptions' =>['style' => 'width:30px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:1%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            [
                'label' => 'Guest',
                'value' => function($data)
                {
                    $bookingDateModel = $data->FirstBookingDate();
                    $guest_string = '';

                    if(!empty($bookingDateModel->user_id))
                    {
                        $guest_string .= $bookingDateModel->user->profile->first_name.' ';
                        $guest_string .= $bookingDateModel->user->profile->last_name;
                    }
                    else
                    {
                        $guest_string .= $bookingDateModel->guest_first_name.' ';
                        $guest_string .= $bookingDateModel->guest_last_name;
                    }
                    return $guest_string;
                },
                //'headerOptions' =>['style' => 'width:70px;'],
                'contentOptions' => function($data)
                {
                    return ['style' => 'width:5%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
                },
            ],
            // [
            //     'attribute' => 'travel_partner_id',
            //     'label' => 'Referrer',
            //     'value' => function($data)
            //     {
            //         if(isset($data->travelPartner->travelPartner->company_name) && !empty($data->travelPartner->travelPartner->company_name))
            //             return $data->travelPartner->travelPartner->company_name;
            //         else
            //             return '- - - - - - -';
            //     },
            //     'filter' => Html::dropDownList(
            //         'BookingsItemsSearch[travel_partner_id]', 
            //         $searchModel['travel_partner_id'], 
            //         ArrayHelper::map(TravelPartner::find()->all(),'id','company_name'), 
            //         [
            //             'prompt' => 'Select Referrer', 
            //             'id'=>'travel_partner_dropdown',
            //             'style' => 'width:100%',
            //         ]),
            //     //'headerOptions' =>['style' => 'width:90px;'],
            //     'contentOptions' => function($data)
            //     {
            //         return ['style' => 'width:5%; color:'.$data->item->text_color.'; background:'.$data->item->background_color.';' ];
            //     },
            // ],
            [
                'class' => 'yii\grid\ActionColumn',
                //'headerOptions' =>['style' => 'width:70px;'],
                'contentOptions' =>['style' => 'width:4%;'],
                'template' => '{late-checkin}{update}',

                'buttons' => [
                    'update-group' => function ($url, $model) {
                            return Html::a('<span class="fa fa-object-group" style="margin-left:5px;"></span>', Url::to(['update-group', 'id'=>$model->bookingItemGroups->booking_group_id]), 
                            [
                                'title' => 'Update Group',
                                'aria-label' => 'Update-Group',
                                'data-pjax' => '0',
                                'data-toggle' => 'tooltip'
                            ]);
                        },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil" style="margin-left:5px;"></span>', Url::to(['bookings/update', 'id'=>$model->id]), 
                        [
                            'title' => 'Update',
                            'aria-label' => 'Update',
                            'data-pjax' => '0',
                            'data-toggle' => 'tooltip'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash" style="margin-left:5px;"></span>', Url::to(['delete', 'id'=>$model->id]), 
                        [
                            'title' => 'Delete',
                            'aria-label' => 'Delete',
                            'data-pjax' => '0',
                            'data-toggle' => 'tooltip'
                        ]);
                    },
                    'late-checkin' => function ($url, $model) {
                        return Html::a('<span class="fa fa-print print" style="margin-left:5px;z-index:20;"></span>', Url::to(['bookings/late-checkin', 'id'=>$model->id]), 
                        [
                            'title' => 'Print Notes',
                            'aria-label' => 'print-notes',
                            'data-pjax' => '0',
                            'target' => '_blank',
                            'data-toggle' => 'tooltip'

                        ]);
                    },
                ],
                /*'visibleButtons' => [
                    // show update button for item who is master
                    'update' => function ($model, $key, $index) {
                        return $model->master == 1 ? true : false;
                    },
                ]*/
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$change_housekeeping_status = Url::to(['/house-keeping/change-status']);
$this->registerJs("
jQuery(document).ready(function() {
    //changing housekeeping staus through ajax //
        $(document).on('click','.status_icon',function(){
            //alert('clicked');
            //App.blockUI({target:'.main-div', animate: !0});
            var arr = $(this).attr('id').split(':');
            var booking_item_id = arr[1];
            var housekeeping_status_id = arr[0];

            var send_ajax = 1;

            var selected_icon = $(this)
            if($(this).hasClass('hk1'))
            {
                if($(this).hasClass('exists'))
                {
                    $(this).css('border','none');
                    $(this).removeClass('exists');
                }
                else
                {
                    $('.hk2-'+booking_item_id).css('border','none');
                    $('.hk2-'+booking_item_id).removeClass('exists');

                    $(this).css('border','2px solid red');
                    $(this).addClass('exists');
                }
                
            }
            else
            {
                if($(this).hasClass('exists'))
                {
                    $(this).css('border','none');
                    $(this).removeClass('exists');
                }
                else
                {
                    if($('.hk1-'+booking_item_id).hasClass('exists'))
                    {
                        //send_ajax = 0;
                        //alert('you have to unselect ready status first');
                        $('.hk1-'+booking_item_id).css('border','none');
                        $('.hk1-'+booking_item_id).removeClass('exists');
                        $(this).css('border','2px solid red');
                        $(this).addClass('exists');
                    }
                    else
                    {
                        $(this).css('border','2px solid red');
                        $(this).addClass('exists');
                    }

                    
                }
            }
            
            if(send_ajax == 1)
            {
                $.ajax(
                {
                    type: 'POST',
                    url: '$change_housekeeping_status',
                    data: {booking_item_id:booking_item_id,housekeeping_status_id:housekeeping_status_id},
                    success: function(data)
                    {
                        if(data)
                        {
                            App.unblockUI('.main-div');
                            //$.pjax.reload({container: '#housekeeping-status-gridview'});

                        }
                        else
                        {
                            if(selected_icon.hasClass('exists'))
                            {
                                selected_icon.css('border','2px solid red');
                                selected_icon.addClass('exists');
                            }
                            else
                            {
                                selected_icon.css('border','none');
                                selected_icon.removeClass('exists');
                                
                            }
                            alert('error');
                        }
                        // // data = jQuery.parseJSON( data );
                        // // $('#items_table').empty().html(data.report_items);
                        // App.unblockUI('.main-div');

                        // // $('#booking_item_modal').modal('show');
                        // // initJsAfterBookingViewRender();
                    },
                    error: function()
                    {
                        alert('error');
                    }
                });
            }
                

        });

    $('[data-toggle=\'tooltip\']').tooltip(); 
    $('#filters_switch').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        if(state)
        {
            $('#filters_div').removeClass('hide-filters');
            $('#filters_div').addClass('show-filters');
        }
        else
        {
            $('#filters_div').removeClass('show-filters');
            $('#filters_div').addClass('hide-filters');
        }
    });

    /*$('#provider_dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#item_dropdown').select2({
        placeholder: \"Select an Item\",
        allowClear: true
    });

    $('#item_name_dropdown').select2({
        placeholder: \"Select an Item Name\",
        allowClear: true
    });

    $('#rates_dropdown').select2({
        placeholder: \"Select a Rate\",
        allowClear: true
    });

    $('#user_dropdown').select2({
        placeholder: \"Select a User\",
        allowClear: true
    });

    $('#status_dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#flag_dropdown').select2({
        placeholder: \"Select a Flag\",
        allowClear: true
    });*/

    $('#bookings-gridview').on('pjax:end', function() 
    {
        /*$('#provider_dropdown').select2({
            placeholder: \"Select a Destination\",
            allowClear: true
        });

        $('#item_dropdown').select2({
            placeholder: \"Select an Item\",
            allowClear: true
        });

        $('#item_name_dropdown').select2({
            placeholder: \"Select an Item Name\",
            allowClear: true
        });

        $('#rates_dropdown').select2({
            placeholder: \"Select a Rate\",
            allowClear: true
        });

        $('#user_dropdown').select2({
            placeholder: \"Select a User\",
            allowClear: true
        });

        $('#status_dropdown').select2({
            placeholder: \"Select a Status\",
            allowClear: true
        });

        $('#flag_dropdown').select2({
            placeholder: \"Select a Flag\",
            allowClear: true
        });*/

        if(server_group_flag=='0')
            $('.group-col').addClass('hide');
        else
            $('.group-col').removeClass('hide');
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

    // $(document).on('click','.table-striped tbody tr td',function()
    // {
    //     // console.log();
    //     // return false;
    //     //id = 
    //     if($(this).hasClass('cancel'))
    //     {

    //     }
    //     else
    //     {
    //         window.location.replace('$update_page_url?id='+$(this).parent().attr('data-key'));
    //     }
    // });
    $('.print').parent().parent().addClass('cancel');
    $(document).on('click','#refresh_btn',function()
    {
        $.ajax(
        {
            type: 'GET',
            url: '$unset_url',
        });
    });

});",View::POS_END);

$this->registerJs('

    jQuery(document).on("pjax:success", "#bookings-gridview",  function(event){
        //alert("pjax complete");

        var gridview_id = "#bookings_gridview"; // specific gridview
        var columns = [3]; // index column that will grouping, start 1
        var group_flag = 0;

        var column_data = [];
            column_start = [];
            rowspan = [];

        for (var i = 0; i < columns.length; i++) {
            column = columns[i];
            column_data[column] = "";
            column_start[column] = null;
            rowspan[column] = 1;
        }


        var row = 1;
        var flag = 1;
        $(gridview_id+" table > tbody  > tr").each(function() {
            var row = $(this);                
            var col = 1;
            
            $(this).find("td").each(function(){
                for (var i = 0; i < columns.length; i++) {
                    if(col==columns[i]){
                        if(column_data[columns[i]] == $(this).html()){
                            $(this).remove();
                            rowspan[columns[i]]++;
                            $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                            if(flag)
                            {
                                row.prev().css("border-top","5px solid #36C6D3");
                                flag = 0;
                            }
                            row.prev().css("border-left","5px solid #36C6D3");
                            row.prev().css("border-right","5px solid #36C6D3");     
                            row.css("border-left","5px solid #36C6D3");
                            row.css("border-right","5px solid #36C6D3");
                           
                            row.css("border-bottom","5px solid #36C6D3");  
                            row.prev().css("border-bottom","");
                            
                            if(group_flag)
                            {
                                row.prev().css("border-top","5px solid #36C6D3");
                                group_flag = 0;
                            }
                        }
                        else
                        {
                            group_flag = 1;
                            column_data[columns[i]] = $(this).html();
                            rowspan[columns[i]] = 1;
                            column_start[columns[i]] = $(this);
                        }
                    }
                }
                col++;
            })
            row++;
        });
      }
    );

    var gridview_id = "#bookings_gridview"; // specific gridview
    var columns = [3]; // index column that will grouping, start 1

    var column_data = [];
        column_start = [];
        rowspan = [];

    for (var i = 0; i < columns.length; i++) {
        column = columns[i];
        column_data[column] = "";
        column_start[column] = null;
        rowspan[column] = 1;
    }

    var row = 1;
    var flag = 1;
    var group_flag = 0;
    $(gridview_id+" table > tbody  > tr").each(function() {
        var row = $(this);                
        var col = 1;
        
        $(this).find("td").each(function()
        {
            for (var i = 0; i < columns.length; i++) 
            {
                if(col==columns[i])
                {
                    if(column_data[columns[i]] == $(this).html())
                    {
                        $(this).remove();
                        rowspan[columns[i]]++;
                        $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                        if(flag)
                        {
                            row.prev().css("border-top","5px solid #36C6D3");
                            flag = 0;
                        }
                        row.prev().css("border-left","5px solid #36C6D3");
                        row.prev().css("border-right","5px solid #36C6D3");     
                        row.css("border-left","5px solid #36C6D3");
                        row.css("border-right","5px solid #36C6D3");
                        
                        row.css("border-bottom","5px solid #36C6D3");  
                        row.prev().css("border-bottom","");

                        if(group_flag)
                        {
                            row.prev().css("border-top","5px solid #36C6D3");
                            group_flag = 0;
                        }
                    }
                    else
                    {
                        group_flag = 1;
                        column_data[columns[i]] = $(this).html();
                        rowspan[columns[i]] = 1;
                        column_start[columns[i]] = $(this);
                    }
                }
            }
            col++;
        })
        row++;
    });


',View::POS_END);

$this->registerJs("

$(document).ready(function(){
     var start = '$startDate';

    $('#booking-range-picker').datepicker().val(start);

    $('#booking-range-picker').on('change', function() {
        $('#rangepicker_status').val(1);
        //alert( $('#booking-range-picker').val());
      $('#booking_form').submit();
    });
});
{
    // function cb(start, end) {
    //     $('#booking-range-picker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    // }

    // $('#booking-range-picker').daterangepicker({
    //     startDate: start,
    //     endDate: end,
    //     ranges: {
    //        'Today': [moment(), moment()],
    //        'Today to N days': [moment(), last_booking_date],
    //        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //        'This Month': [moment().startOf('month'), moment().endOf('month')],
    //        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //     }
    // }, cb);

    // cb(start, end);


}",View::POS_END);
?>
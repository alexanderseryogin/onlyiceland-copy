<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "places_of_interest".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property string $description
 * @property string $title_picture
 *
 * @property Gallery[] $galleries
 * @property Regions $region
 */
class PlacesOfInterest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'places_of_interest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string'],
            [['name', 'title_picture'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'title_picture' => Yii::t('app', 'Title Picture'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleries()
    {
        return $this->hasMany(Gallery::className(), ['poi_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findPOI($id)
    {
        return static::findOne(['id' => $id]);
    }
}

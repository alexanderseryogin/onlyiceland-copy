<?php

use yii\db\Migration;

class m170331_075936_alter_bookings_special_requests extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_special_requests` ADD COLUMN `booking_item_id` INT UNSIGNED NULL AFTER `request_id`, DROP INDEX `bookings_special_requests_booking_id`, DROP FOREIGN KEY `bookings_special_requests_booking_id`, ADD CONSTRAINT `FK_bsr_booking_item_id` FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE; 

            RENAME TABLE `bookings_special_requests` TO `booking_special_requests`;
        ");
    }

    public function down()
    {
        echo "m170331_075936_alter_bookings_special_requests cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170802_065630_create_table_bookings_items_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `bookings_items_cart`(  
                      `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                      `booking_number` INT(11),
                      `provider_id` INT(11),
                      `item_id` INT(11),
                      `item_name_id` INT(11) UNSIGNED,
                      `arrival_date` DATE,
                      `departure_date` DATE,
                      `flag_id` INT(11),
                      `status_id` INT(11),
                      `housekeeping_status_id` INT(11),
                      `travel_partner_id` INT(11) UNSIGNED,
                      `offers_id` INT(11),
                      `estimated_arrival_time_id` INT(11) UNSIGNED,
                      `rate_conditions` TINYINT(1),
                      `pricing_type` TINYINT(1),
                      `comments` LONGTEXT,
                      `balance` DOUBLE,
                      `voucher_no` VARCHAR(256),
                      `reference_no` VARCHAR(256),
                      `created_by` INT(11),
                      `created_at` INT(11),
                      `updated_at` INT(11),
                      `deleted_at` INT(11) DEFAULT 0,
                      PRIMARY KEY (`id`),
                      CONSTRAINT `bookings_items_cart_created_by` FOREIGN KEY (`created_by`) REFERENCES `user`(`id`) ON UPDATE SET NULL ON DELETE CASCADE,
                      CONSTRAINT `bookings_items_cart_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
                      CONSTRAINT `bookings_items_cart_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
                      CONSTRAINT `bookings_items_cart_housekeeping_status_id` FOREIGN KEY (`housekeeping_status_id`) REFERENCES `housekeeping_status`(`id`) ON UPDATE SET NULL ON DELETE CASCADE,
                      CONSTRAINT `bookings_items_cart_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE,
                      CONSTRAINT `bookings_items_cart_item_name_id` FOREIGN KEY (`item_name_id`) REFERENCES `bookable_items_names`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
                      CONSTRAINT `bookings_items_cart_offers` FOREIGN KEY (`offers_id`) REFERENCES `offers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
                      CONSTRAINT `bookings_items_cart_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
                      CONSTRAINT `bookings_items_cart_travel_partner_id` FOREIGN KEY (`travel_partner_id`) REFERENCES `destination_travel_partners`(`id`) ON UPDATE SET NULL ON DELETE SET NULL,
                      CONSTRAINT `bookings_items_cart_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL
                    ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                    ');
    }

    public function down()
    {
        echo "m170802_065630_create_table_bookings_items_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

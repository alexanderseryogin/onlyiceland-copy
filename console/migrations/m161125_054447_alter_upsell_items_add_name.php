<?php

use yii\db\Migration;

class m161125_054447_alter_upsell_items_add_name extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `upsell_items` ADD COLUMN `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `id`;");
    }

    public function down()
    {
        echo "m161125_054447_alter_upsell_items_add_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

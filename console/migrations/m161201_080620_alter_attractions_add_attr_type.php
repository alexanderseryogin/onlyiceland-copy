<?php

use yii\db\Migration;

class m161201_080620_alter_attractions_add_attr_type extends Migration
{
    public function up()
    {
        $this->execute(" 
            ALTER TABLE `attractions` CHANGE `at_id` `attr_type_id` INT(11) NULL, DROP FOREIGN KEY `FK_attractions_at_id`, ADD CONSTRAINT `FK_attractions_attr_type_id` FOREIGN KEY (`attr_type_id`) REFERENCES `provider_types`(`id`) ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m161201_080620_alter_attractions_add_attr_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

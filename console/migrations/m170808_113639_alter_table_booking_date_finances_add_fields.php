<?php

use yii\db\Migration;

class m170808_113639_alter_table_booking_date_finances_add_fields extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_date_finances`   
                          ADD COLUMN `item_type` INT(11) NULL AFTER `type`,
                          ADD COLUMN `entry_type` INT(11) NULL AFTER `item_type`,
                          CHANGE `vat` `vat` DOUBLE NULL  AFTER `amount`,
                          ADD COLUMN `x` DOUBLE NULL AFTER `vat`,
                          CHANGE `tax` `tax` DOUBLE NULL  AFTER `x`,
                          ADD COLUMN `y` DOUBLE NULL AFTER `tax`,
                          ADD COLUMN `discount` DOUBLE NULL AFTER `y`,
                          ADD COLUMN `discount_amount` DOUBLE NULL AFTER `discount`,
                          ADD COLUMN `sub_total1` DOUBLE NULL AFTER `discount_amount`,
                          ADD COLUMN `sub_total2` DOUBLE NULL AFTER `sub_total1`,
                          ADD COLUMN `vat_amount` DOUBLE NULL AFTER `sub_total2`,
                          ADD COLUMN `final_total` DOUBLE NULL AFTER `vat_amount`;
                        ');
    }

    public function down()
    {
        echo "m170808_113639_alter_table_booking_date_finances_add_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

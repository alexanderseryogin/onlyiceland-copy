<?php

use yii\db\Migration;

class m161007_044932_create_table_provider_financial extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_financial`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `email` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `bank_info` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `account_holder_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `account_number` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `owning_entity` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `address` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `country_id` INT(11), `city_id` INT(11), `state_id` INT(11), `postal_code` VARCHAR(256), `vat_rate` VARCHAR(256), `license_type` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `license_expiration_date` DATE, `license_image` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_financial_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_financial_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_financial_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_financial_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161007_044932_create_table_provider_financial cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

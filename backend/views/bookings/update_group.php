

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\BookingsItems;
use common\models\BookableBedsCombinations;
use common\models\RatesCapacityPricing;
use common\models\Rates;
use common\models\BookableBedTypes;
use common\models\BookingsDiscountCodes;
use common\models\BookingsUpsellItems;
use common\models\User;
use common\models\BookingConfirmationTypes;
use common\models\BookingTypes;
use common\models\Destinations;
use common\models\BookingStatuses;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\EstimatedArrivalTimes;
use common\models\BookingSpecialRequests;
use common\models\Countries;
use common\models\BookingItemGroup;

$this->title = "Update Booking Group";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $bookingGroupModel->id, 'url' => ['update-group', 'id' => $bookingGroupModel->id]];
$this->params['breadcrumbs'][] = $this->title;

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$provider_id = '';
$items_ids_arr = [];
$items_ids_str = '';

$items = BookingItemGroup::find()->joinWith('bookingItem')->where(['booking_group_id' => $group_id, 'deleted_at' => 0])->all();

//********** maintaining string to show on captaion wether user will show or group guest text will show *********//

$group_string = $bookingGroupModel->group_name;

if(!empty($bookingGroupModel->group_guest_user_id))
{
    $group_string .= !empty($bookingGroupModel->groupGuestUser->first_name)?' - '.$bookingGroupModel->groupGuestUser->first_name:'';
    $group_string .= !empty($bookingGroupModel->groupGuestUser->last_name)?' - '.$bookingGroupModel->groupGuestUser->last_name:'';
    $group_string .= !empty($bookingGroupModel->groupGuestUser->country->name)?' - '.$bookingGroupModel->groupGuestUser->country->name:'';
}
else
{
    $group_string .= !empty($bookingGroupModel->groupUser->profile->first_name)?' - '.$bookingGroupModel->groupUser->profile->first_name:'';
    $group_string .= !empty($bookingGroupModel->groupUser->profile->last_name)?' - '.$bookingGroupModel->groupUser->profile->last_name:'';
    $group_string .= !empty($bookingGroupModel->groupUser->profile->country_id)?' - '.$bookingGroupModel->groupUser->profile->country->name:'';
}

?>

<script type="text/javascript">
    var group_id = <?php echo $group_id; ?>;
</script>

<div class="bookings-form" id="booking_form">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Update Booking Item</span>
                    </div>
                    <ul class="nav nav-tabs booking_tabs">
                        <li class="active">
                            <a href="#details" data-toggle="tab">Details</a>
                        </li>
                        <li >
                            <a href="#charges" data-toggle="tab">Charges & Payments</a>
                        </li>
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->


                    </ul>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body destination_portlet">
                        <div class="tab-content">
                            <div class="tab-pane active" id="details">
                                <?= $this->render('update_group_details', [
                                    'group_id' => $group_id,
                                    'bookingGroupModel' => $bookingGroupModel,
                                    'pageNo' => $pageNo,
                                ]) ?>
                            </div>
                            <div class="tab-pane" id="charges">
                               <?= $this->render('group_payment-charges', [
                                    'group_id' => $group_id,
                                    'bookingGroupModel' => $bookingGroupModel,
                                    'pageNo' => $pageNo,
                                ]) ?>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
            </div>
           
        </div>
    </div>
</div>

<!-- ************************* User modal **************************** -->

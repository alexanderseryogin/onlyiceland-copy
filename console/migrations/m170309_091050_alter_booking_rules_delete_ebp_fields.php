<?php

use yii\db\Migration;

class m170309_091050_alter_booking_rules_delete_ebp_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_rules` DROP COLUMN `ebp_booking_type`, DROP COLUMN `ebp_first_night_exception_period`, DROP COLUMN `ebp_last_night_exception_period`, DROP INDEX `FK_booking_rules_ebp_booking_type`, DROP FOREIGN KEY `FK_booking_rules_ebp_booking_type`; 
        ");
    }

    public function down()
    {
        echo "m170309_091050_alter_booking_rules_delete_ebp_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

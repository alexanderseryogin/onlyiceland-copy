<?php

namespace common\models;
use DateTime;
use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $destination_id
 * @property string $booking_item_id
 * @property integer $notification_type
 * @property string $message
 * @property integer $status
 * @property integer $user_type
 * @property string $created_at
 *
 * @property BookingsItems $bookingItem
 * @property Providers $destination
 */
class Notifications extends \yii\db\ActiveRecord
{
    const RATE_NOT_AVAILABLE = 1;
    const DESTINATION_OVERBOOKING = 2;
    const UNIT_OVERBOOKING = 3;
    const UNIT_NOT_ASSIGNED = 4;

    const NOTIFICATION_SEEN = 1;
    const NOTIFICATION_UNSEEN = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['destination_id', 'booking_item_id', 'bookable_item_id', 'notification_type', 'status', 'user_type','rate_id','bookable_item_name_id'], 'integer'],
            [['message'], 'string'],
            [['created_at','date'], 'safe'],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItems::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
            [['destination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['destination_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'destination_id' => 'Destination ID',
            'booking_item_id' => 'Booking Item ID',
            'notification_type' => 'Notification Type',
            'message' => 'Message',
            'status' => 'Status',
            'user_type' => 'User Type',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItems::className(), ['id' => 'booking_item_id']);
    }
    
    public function getBookableItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_item_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'destination_id']);
    }

    public function getNotificationType()
    {
        switch($this->notification_type)
        {
            case 1:
                return "Rate not available";
            case 2:
                return "Destination Overbooking";
            case 3:
                return "Unit Overbooking";
            case 4:
                return "No Unit assigned";
            default:
                return "";
        }
    }

    public static function addDestinationOverbooking($orgBookingItem,$bookingDatesModel)
    {
        // $org_quantity = $orgBookingItem->item->item_quantity;
        // if(count(BookingDates::find()->where(['date' => $bookingDatesModel->date, 'item_id' => $bookingDatesModel->item_id ])->all()) > $org_quantity)
        // {
        $total_rooms = 0;
        foreach ($orgBookingItem->provider->bookableItems as  $value)
        {
            $total_rooms = $total_rooms + $value->item_quantity; 
        }
       
        if(count(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date, 'provider_id' => $orgBookingItem->provider_id,'bookings_items.deleted_at' => 0 ])->all()) > $total_rooms)
        {
            $destination_overbooking_noti_exists = Notifications::findOne(['destination_id' => $orgBookingItem->provider_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::DESTINATION_OVERBOOKING]);
            if(empty($destination_overbooking_noti_exists))
            {
                $rate_noti = new Notifications();
                $rate_noti->destination_id = $orgBookingItem->provider_id;
                $rate_noti->booking_item_id = $orgBookingItem->id;
                $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                $rate_noti->rate_id = $bookingDatesModel->rate_id;
                $rate_noti->date = $bookingDatesModel->date;
                $rate_noti->notification_type = Notifications::DESTINATION_OVERBOOKING;
                $rate_noti->message = "Overbooking on ".$bookingDatesModel->date;
                $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                $rate_noti->save();
            }
                
        }
    }

    public static function addMultipleUnitsBooking($orgBookingItem,$bookingDatesModel)
    {
        if(!empty($bookingDatesModel->item_name_id))
        {
            if(!empty(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date,'booking_dates.item_id' => $bookingDatesModel->item_id, 'booking_dates.item_name_id' => $bookingDatesModel->item_name_id,'bookings_items.deleted_at' => 0])->andWhere(['!=','booking_dates.id',$bookingDatesModel->id ])->all() ))
            {
                $multiple_unit_noti_exists = Notifications::findOne(['bookable_item_id' => $bookingDatesModel->item_id, 'date' => $bookingDatesModel->date,'bookable_item_name_id' => $bookingDatesModel->item_name_id, 'notification_type' => Notifications::UNIT_OVERBOOKING]);
                if(empty($multiple_unit_noti_exists))
                {
                    $rate_noti = new Notifications();
                    $rate_noti->destination_id = $orgBookingItem->provider_id;
                    $rate_noti->booking_item_id = $orgBookingItem->id;
                    $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                    $rate_noti->bookable_item_name_id = $bookingDatesModel->item_name_id;
                    $rate_noti->rate_id = $bookingDatesModel->rate_id;
                    $rate_noti->date = $bookingDatesModel->date;
                    $rate_noti->notification_type = Notifications::UNIT_OVERBOOKING;
                    $rate_noti->message = "Multiple booking in unit on ".$bookingDatesModel->date;
                    $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                    $rate_noti->save();
                }
            }
        }
    }

    public static function addUnitNotAssingned($orgBookingItem,$bookingDatesModel)
    {
        if($bookingDatesModel->item_name_id == NULL)
        {
            $multiple_units_noti_exists = Notifications::findOne(['booking_item_id' => $orgBookingItem->id,'destination_id' => $orgBookingItem->provider_id,'bookable_item_id' => $orgBookingItem->item_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::UNIT_NOT_ASSIGNED]);
            if(empty($multiple_units_noti_exists))
            {
                $rate_noti = new Notifications();
                $rate_noti->destination_id = $orgBookingItem->provider_id;
                $rate_noti->booking_item_id = $orgBookingItem->id;
                $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                $rate_noti->rate_id = $bookingDatesModel->rate_id;
                $rate_noti->date = $bookingDatesModel->date;
                $rate_noti->notification_type = Notifications::UNIT_NOT_ASSIGNED;
                $rate_noti->message = "No Unit assigned on ".$bookingDatesModel->date;
                $rate_noti->user_type = User::USER_TYPE_ADMIN;
                $rate_noti->save();
            }
        }
    }

    public static function addRateNotAvailable($orgBookingItem)
    {
        $all_rates = Rates::find()->where(['unit_id' => $orgBookingItem->item_id])->all();
        $notfication_rates = array();
        $notification_exists = 0;
        $largest = date('Y-m-d');
        $largest_rate = null;
        if(!empty($all_rates))
        {
            foreach ($all_rates as $value) 
            {
                if(date("Y-m-d",strtotime($value->to)) > $largest)
                {
                    $largest = date("Y-m-d",strtotime($value->to));
                    $largest_rate = $value;
                }
            } 
        }
        if($largest_rate != null)
        {
            $d1 = new DateTime($largest_rate->to);
            $d2 = new DateTime(date('Y-m-d'));

            if($d1->diff($d2)->m < 12) // int(4)
            {
                $bDate = BookingDates::findOne(['booking_item_id' => $orgBookingItem->id]);
                $rate_noti_exists = Notifications::findOne(['destination_id' => $orgBookingItem->provider_id,'bookable_item_id' => $orgBookingItem->item_id, 'rate_id' => $bDate->rate_id, 'notification_type' => Notifications::RATE_NOT_AVAILABLE]);
                if(empty($rate_noti_exists))
                {
                    $rate_noti = new Notifications();
                    $rate_noti->destination_id = $orgBookingItem->provider_id;
                    $rate_noti->booking_item_id = $orgBookingItem->id;
                    $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                    $rate_noti->rate_id = $largest_rate->id;
                    $rate_noti->notification_type = Notifications::RATE_NOT_AVAILABLE;
                    $rate_noti->message = "No rate available after ".$largest_rate->to;
                    $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                    $rate_noti->save();
                    //$message.= $value->name." not available after ".$value->to."<br>";
                }
            }
            
        }
    }

    public static function generateNoRateAvailableNotifications($bookableItem) // for cron job
    {
        if(!empty($bookableItem->rates))
        {
            $largest = date('Y-m-d');
            $largest_rate = null;

            foreach($bookableItem->rates as $key => $rate) 
            {
                if(date("Y-m-d",strtotime($rate->to)) > $largest)
                {
                    $largest = date("Y-m-d",strtotime($rate->to));
                    $largest_rate = $rate;
                }
            }

            if($largest_rate != null)
            {
                Notifications::deleteAll(['bookable_item_id' => $bookableItem->id, 'notification_type' => Notifications::RATE_NOT_AVAILABLE]);

                $d1 = new DateTime($largest_rate->to);
                $d2 = new DateTime(date('Y-m-d'));

                if($d1->diff($d2)->m < 12) // int(4)
                {
                    $rate_noti = new Notifications();
                    $rate_noti->destination_id = $largest_rate->provider_id;
                    $rate_noti->booking_item_id = Null;
                    $rate_noti->bookable_item_id = $largest_rate->unit_id;
                    $rate_noti->rate_id = $largest_rate->id;
                    $rate_noti->notification_type = Notifications::RATE_NOT_AVAILABLE;
                    $rate_noti->message = "No rate available after ".$largest_rate->to;
                    $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                    $rate_noti->save();
                }
            }
        }
    }

    public static function removeDestinationOverbooking($orgBookingItem,$bookingDatesModel)
    {
        // $org_quantity = $orgBookingItem->item->item_quantity;
        // if(count(BookingDates::find()->where(['date' => $bookingDatesModel->date, 'item_id' => $bookingDatesModel->item_id ])->all()) > $org_quantity)
        // {
        $total_rooms = 0;
        foreach ($orgBookingItem->provider->bookableItems as  $value)
        {
            $total_rooms = $total_rooms + $value->item_quantity; 
        }

        // echo "<pre>";
        // print_r(count(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date, 'provider_id' => $orgBookingItem->provider_id ])->all()));
        // exit();
       
        if(count(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date, 'provider_id' => $orgBookingItem->provider_id,'bookings_items.deleted_at' => 0 ])->all())-1 <= $total_rooms)
        {
            $destination_overbooking_noti = Notifications::findOne(['destination_id' => $orgBookingItem->provider_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::DESTINATION_OVERBOOKING]);
            if(!empty($destination_overbooking_noti))
            {
                $destination_overbooking_noti->delete();
            }
        }
    }

    public static function removeUnitOverbooking($orgBookingItem,$bookingDatesModel)
    {
        // echo "<pre>";
        // print_r('bookable_item_id : '.$bookingDatesModel->item_name_id.'  '.(count(BookingDates::find()->where(['date' => $bookingDatesModel->date, 'item_name_id' => $bookingDatesModel->item_name_id ])->all())));
        // exit();
        if((count(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date, 'booking_dates.item_name_id' => $bookingDatesModel->item_name_id,'deleted_at' => 0 ])->all()))-1 <= 1)
        {
            $unit_overbooking_noti = Notifications::find()->where(['date' => $bookingDatesModel->date,'bookable_item_name_id' => $bookingDatesModel->item_name_id, 'notification_type' => Notifications::UNIT_OVERBOOKING])->all();
            if(!empty($unit_overbooking_noti))
            {
                foreach ($unit_overbooking_noti as $notification) 
                {
                    $notification->delete();
                }
                
            }
        }
    }
    public function getNotificationUrl($date = null)
    {
        switch ($this->notification_type) 
        {
            case Notifications::RATE_NOT_AVAILABLE:
                $url = yii::$app->getUrlManager()->createUrl(['rates', 'RatesSearch[provider_id]' => $this->destination_id, 'RatesSearch[unit_id]' => $this->bookable_item_id, 'current_rates' => 1, 'future_rates' => 1]);
                return $url;
            case Notifications::DESTINATION_OVERBOOKING:
                $url = yii::$app->getUrlManager()->createUrl(['bookings/calendar', 'destination_id' => $this->destination_id, ]);
                return $url;
            case Notifications::UNIT_OVERBOOKING:
                $url = yii::$app->getUrlManager()->createUrl(['bookings/booking-issues', 'destination_id' => $this->destination_id, 'exp_date' => $date]);
                return $url;
            case Notifications::UNIT_NOT_ASSIGNED:
                $url = yii::$app->getUrlManager()->createUrl(['bookings/update', 'id' => $this->booking_item_id]);
                return $url;
            
            default:
                # code...
                break;
        }
    }

}

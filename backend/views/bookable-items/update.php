<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BookableItems */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bookable Items',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookable Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bookable-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'tab_href' => $tab_href,
        'bed_type' => $bed_type,
        'quantity' => $quantity,
        'dataProviderBedType' => $dataProviderBedType,
        'amenities' => $amenities
    ]) ?>

</div>

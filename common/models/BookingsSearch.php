<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bookings;

/**
 * BookingsSearch represents the model behind the search form about `common\models\Bookings`.
 */
class BookingsSearch extends Bookings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'provider_id', 'travel_partner_id', 'offers_id','created_at', 'updated_at'], 'integer'],
            [['arrival_date', 'departure_date'], 'safe'],
            [['group_name'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bookings::find()->where(['=', 'deleted_at', '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'provider_id' => $this->provider_id,
            'arrival_date' => $this->arrival_date,
            'departure_date' => $this->departure_date,
            'travel_partner_id' => $this->travel_partner_id,
            'offers_id' => $this->offers_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'group_name', $this->group_name]);

        return $dataProvider;
    }

    public function searchBookings($params)
    {
        if(empty($params))
        {
            $query = Bookings::find()->where(['id' => 0]);
        }
        else
        {
            if(isset($params['booking_types']) && isset($params['booking_status']))
            {
                if(count($params['booking_types'])==1)
                {
                    if($params['booking_types'][0]==0)
                        $query = Bookings::find()->where(['=', 'deleted_at', $params['booking_types'][0]]);
                    else
                        $query = Bookings::find()->where(['>', 'deleted_at', $params['booking_types'][0]]);
                }
                else if(count($params['booking_types'])==2)
                {
                    $query = Bookings::find()->where(['=', 'deleted_at', '0']);
                    $query->orWhere(['>', 'deleted_at', '1']);
                }
                
                $query->andWhere(['status_id' => $params['booking_status']]);
            }
            else
            {
                $query = Bookings::find()->where(['id' => 0]);
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    public function searchAuditLog($params)
    {
        $query = Modelhistory::find()->where(['field_id' => $params]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

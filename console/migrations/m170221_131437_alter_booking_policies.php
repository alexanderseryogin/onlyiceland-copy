<?php

use yii\db\Migration;

class m170221_131437_alter_booking_policies extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_policies` ADD COLUMN `pricing` TINYINT(4) NULL AFTER `time_group`; 
        ");
    }

    public function down()
    {
        echo "m170221_131437_alter_booking_policies cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170831_151342_alter_table_booking_group_finance_add_field extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_group_finance`   
                          ADD COLUMN `deposit_percent_age` INT(11) NULL AFTER `payment_type`;
                        ');
    }

    public function down()
    {
        echo "m170831_151342_alter_table_booking_group_finance_add_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

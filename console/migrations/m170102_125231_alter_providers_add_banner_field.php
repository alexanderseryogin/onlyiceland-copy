<?php

use yii\db\Migration;

class m170102_125231_alter_providers_add_banner_field extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers` ADD COLUMN `banner` INT(11) NULL AFTER `text_color`; ");
    }

    public function down()
    {
        echo "m170102_125231_alter_providers_add_banner_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DiscountCode;

/**
 * DiscountCodeSearch represents the model behind the search form about `common\models\DiscountCode`.
 */
class DiscountCodeSearch extends DiscountCode
{
    public $provider_type_id;
    public $state_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','pricing_type','applies_to','quantity_type', 'provider_id', ], 'integer'],
            [['provider_id','name', 'amount', 'enterable_by_registered_users', 'active','provider_type_id', 'state_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiscountCode::find()->joinWith(['destination']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => isset($params['page'])?($params['page']-1):0,
            ],
        ]);

        $dataProvider->sort->attributes['provider_type_id'] = [
        'asc' => ['providers.provider_type_id' => SORT_ASC], //tableName.attributeName
        'desc' => ['providers.provider_type_id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['state_id'] = [
        'asc' => ['providers.state_id' => SORT_ASC], //tableName.attributeName
        'desc' => ['providers.state_id' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_type_id' => $this->provider_type_id,
            'pricing_type' => $this->pricing_type,
            'applies_to' => $this->applies_to,
            'quantity_type' => $this->quantity_type,
            'state_id' => $this->state_id,
            'provider_id' => $this->provider_id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'enterable_by_registered_users', $this->enterable_by_registered_users])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}

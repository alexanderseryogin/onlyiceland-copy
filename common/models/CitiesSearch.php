<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Cities;

/**
 * CitiesSearch represents the model behind the search form of `common\models\Cities`.
 */
class CitiesSearch extends Cities
{
    public $country_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'state_id'], 'integer'],
            [['name','country_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cities::find()->joinWith('state');

        // add conditions that should always apply here

        /*if(isset($params['CitiesSearch']['country_id']) && !empty($params['CitiesSearch']['country_id']))
        {
            $query->andwhere(['=','states.country_id',$params['CitiesSearch']['country_id']]);
        }*/

        /*if(isset($params['CitiesSearch']['state_id']) && !empty($params['CitiesSearch']['state_id']))
        {
            $session = Yii::$app->session;
            $session->open();
            $session['state_id'] = $params['CitiesSearch']['state_id'];
        }*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => isset($params['page'])?($params['page']-1):0,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'country_id' => [
                    'asc' => ['country_id' => SORT_ASC],
                    'desc' => ['country_id' => SORT_DESC],
                ],
                'state_id' => [
                    'asc' => ['state_id' => SORT_ASC],
                    'desc' => ['state_id' => SORT_DESC],
                ],
                'name' => [
                    'asc' => ['cities.name' => SORT_ASC],
                    'desc' => ['cities.name' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        $this->country_id = $this->country_id ? $this->country_id : 100;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'state_id' => $this->state_id,
            'states.country_id' => $this->country_id,

        ]);

        $query->andFilterWhere(['like', 'cities.name', $this->name]);

        return $dataProvider;
    }
}

<?php

use yii\db\Migration;

class m170210_092535_create_request_upsell_discount extends Migration
{
    public function up()
    {
        $this->execute("

            CREATE TABLE `bookings_special_requests`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `request_id` INT(11), `booking_id` INT(11) UNSIGNED, PRIMARY KEY (`id`), CONSTRAINT `FK_bookings_special_requests_request_id` FOREIGN KEY (`request_id`) REFERENCES `special_requests`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `bookings_special_requests_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `bookings`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

            CREATE TABLE `bookings_discount_codes`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `discount_id` INT(11), `booking_id` INT(11) UNSIGNED, PRIMARY KEY (`id`), CONSTRAINT `FK_bookings_discount_codes_discount_id` FOREIGN KEY (`discount_id`) REFERENCES `discount_code`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_bookings_discount_codes_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `bookings`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

            CREATE TABLE `bookings_upsell_items`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `upsell_id` INT(11), `booking_id` INT(11) UNSIGNED, PRIMARY KEY (`id`), CONSTRAINT `FK_bookings_upsell_items_upsell_id` FOREIGN KEY (`upsell_id`) REFERENCES `upsell_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_bookings_upsell_items_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `bookings`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 

        ");
    }

    public function down()
    {
        echo "m170210_092535_create_request_upsell_discount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace metronic\assets;

use yii\web\AssetBundle;
use yii\web\View;

class LoginAsset extends AssetBundle
{
    public $sourcePath = '@metronic/resources';
    public $css = [
        // GLOBAL MANDATORY STYLES
        'global/plugins/font-awesome/css/font-awesome.min.css',
        'global/plugins/simple-line-icons/simple-line-icons.min.css',
        'global/plugins/bootstrap/css/bootstrap.min.css',
        'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        
        // BEGIN PAGE LEVEL PLUGINS 
        'global/plugins/select2/css/select2.min.css',
        'global/plugins/select2/css/select2-bootstrap.min.css',

        // THEME GLOBAL STYLES
        'global/css/components-md.min.css',
        'global/css/plugins-md.min.css',

        // THEME LAYOUT STYLES
        //'layouts/layout/css/layout.min.css',
        //'layouts/layout/css/themes/darkblue.min.css',
        //'layouts/layout/css/custom.min.css',
        
        //Login page
        'pages/css/login-5.min.css',
    ];
    public $cssOptions = [
        'type' => 'text/css',
        'position' => View::POS_BEGIN
    ];
    public $js = [

        // BEGIN CORE PLUGINS 
        //'global/plugins/jquery.min.js',
        'global/plugins/bootstrap/js/bootstrap.min.js',
        'global/plugins/js.cookie.min.js',
        'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'global/plugins/jquery.blockui.min.js',
        'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'global/plugins/jquery-validation/js/jquery.validate.min.js',
        
        // BEGIN PAGE LEVEL PLUGINS 
        'global/plugins/jquery-validation/js/jquery.validate.min.js',
        'global/plugins/jquery-validation/js/additional-methods.min.js',
        'global/plugins/select2/js/select2.full.min.js',
        'global/plugins/backstretch/jquery.backstretch.min.js',
        'global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',

        // BEGIN THEME GLOBAL SCRIPTS
        'global/scripts/app.min.js',
        //'pages/scripts/login-5.min.js',

    ];
    public $jsOptions = [
        'type' => 'text/javascript',
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
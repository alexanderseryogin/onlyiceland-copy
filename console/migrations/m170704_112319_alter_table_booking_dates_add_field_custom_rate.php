<?php

use yii\db\Migration;

class m170704_112319_alter_table_booking_dates_add_field_custom_rate extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `booking_dates`   
                          ADD COLUMN `custom_rate` DOUBLE NULL AFTER `rate_id`;
                        ');
    }

    public function down()
    {
        echo "m170704_112319_alter_table_booking_dates_add_field_custom_rate cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

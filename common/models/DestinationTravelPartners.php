<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "destination_travel_partners".
 *
 * @property string $id
 * @property integer $destination_id
 * @property integer $travel_partner_id
 * @property integer $comission
 * @property string $booking_email_address
 * @property string $invoice_email_address
 * @property string $primary_contact
 * @property string $billing_contact
 *
 * @property Providers $destination
 * @property TravelPartner $travelPartner
 */
class DestinationTravelPartners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'destination_travel_partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['destination_id', 'travel_partner_id'], 'integer'],
            [['comission'],'safe'],
            [['booking_email_address', 'invoice_email_address', 'primary_contact', 'billing_contact'], 'string', 'max' => 256],
            [['destination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['destination_id' => 'id']],
            [['travel_partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => TravelPartner::className(), 'targetAttribute' => ['travel_partner_id' => 'id']],
            [['notes'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'destination_id' => 'Destination ID',
            'travel_partner_id' => 'Travel Partner ID',
            'comission' => 'Comission',
            'booking_email_address' => 'Booking Email Address',
            'invoice_email_address' => 'Invoice Email Address',
            'primary_contact' => 'Primary Contact',
            'billing_contact' => 'Billing Contact',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'destination_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravelPartner()
    {
        return $this->hasOne(TravelPartner::className(), ['id' => 'travel_partner_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "offers".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $item_id
 * @property string $name
 * @property string $summary
 * @property string $additional_details
 * @property string $allow_cancellations
 * @property integer $booking_confirmation_type_id
 * @property string $min_stay
 * @property integer $show
 *
 * @property BookingConfirmationTypes $bookingConfirmationType
 * @property BookableItems $item
 * @property Providers $provider
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'item_id', 'booking_confirmation_type_id', 'show'], 'integer'],
            [['provider_id', 'item_id', 'booking_confirmation_type_id', 'show','name', 'allow_cancellations', 'min_stay','commission'], 'required'],
            [['summary', 'additional_details'], 'string'],
            [['name', 'allow_cancellations', 'min_stay'], 'string', 'max' => 256],
            [['name'],'unique'],
            [['commission'], 'integer','min' => 0],
            [['booking_confirmation_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['booking_confirmation_type_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination'),
            'item_id' => Yii::t('app', 'Item'),
            'name' => Yii::t('app', 'Name'),
            'summary' => Yii::t('app', 'Summary'),
            'additional_details' => Yii::t('app', 'Additional Details'),
            'allow_cancellations' => Yii::t('app', 'Allow Cancellations'),
            'booking_confirmation_type_id' => Yii::t('app', 'Booking Confirmation Type'),
            'min_stay' => Yii::t('app', 'Min Stay'),
            'show' => Yii::t('app', 'Show'),
            'commission' => Yii::t('app', 'Commission %'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingConfirmationType()
    {
        return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'booking_confirmation_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }
}

<?php

use yii\db\Migration;

class m161014_102808_create_table_special_requests extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `special_requests`( `id` INT(11) NOT NULL AUTO_INCREMENT, `label` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `background_color` VARCHAR(256), `text_color` VARCHAR(256), PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161014_102808_create_table_special_requests cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
    use common\models\PropertyPaymentMethods;
    use common\models\BookingTypes;
    use common\models\BookingStatuses;
    use common\models\BookingPolicies;
    Use common\models\Destinations;
    use yii\helpers\ArrayHelper;
    use yii\web\View;
    use yii\helpers\Url;
    use common\models\EstimatedArrivalTimes;

    $statusCheckbox = [
        0 => '',
        1 => 'checked'
    ];
?>                       

<div class="tab-pane" id="policies">

    <?= $form->field($provider_model, 'special_requests')->checkboxList(Destinations::$all_special_requests, [
            'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($booking_policies_model, 'booking_status_id', [
                'inputOptions' => [
                    'id' => 'booking-status-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
                ])->dropdownList(ArrayHelper::map(BookingStatuses::find()->all(),'id','label'),['prompt'=>'Select a Status']) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($booking_policies_model, 'booking_type_id', [
                'inputOptions' => [
                    'id' => 'booking-type-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
                ])->dropdownList(ArrayHelper::map(BookingTypes::find()->all(),'id','label'),['prompt'=>'Select a Flag']) ?>
        </div>
    </div>

    <label class=" control-label">Youngest Age Allowed</label>
    <div class="form-group">
        <input id="youngest-age" type="text" value="<?=$booking_policies_model->youngest_age?>" name="BookingPolicies[youngest_age]" />
    </div>

    <label class=" control-label">Oldest Free Age</label>
    <div class="form-group">
        <input id="oldest-age" type="text" value="<?=$booking_policies_model->oldest_free_age?>" name="BookingPolicies[oldest_free_age]" />
    </div>

    <?= $form->field($booking_policies_model, 'payment_methods_gauarantee')->checkboxList(BookingPolicies::$methodsGuarantee, [
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <?= $form->field($booking_policies_model, 'payment_methods_checkin')->checkboxList(BookingPolicies::$methodsCheckin,[
        'itemOptions' => ['style' => 'margin-left:10px;']
    ]) ?>

    <?php 
        if($booking_policies_model->isNewRecord && empty($booking_policies_model))
        {
            $time=date('H:i');
            $check_in_from = $time;
            $check_in_to = $time;
            $check_out_to = $time;
            $check_out_from = $time;
        }
        else
        {
            $check_in_from = $booking_policies_model->checkin_time_from;
            $check_in_to = $booking_policies_model->checkin_time_to;
            $check_out_to = $booking_policies_model->checkout_time_to;
            $check_out_from = $booking_policies_model->checkout_time_from;
        }
    ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-3">
                <label class="control-label">Check-in Time From</label>
                <div class="form-group">
                    <div>
                        <div class="input-icon">
                            <i class="fa fa-clock-o"></i>
                            <input type="text" id="check_in_from" value="<?=$check_in_from ?>" name="BookingPolicies[checkin_time_from]" class="form-control timepicker timepicker-24"> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-3">
                <label class="control-label">Check-in Time To</label>
                <div class="form-group">
                    <div>
                        <div class="input-icon">
                            <i class="fa fa-clock-o"></i>
                            <input type="text" id="check_in_to" value="<?=$check_in_to ?>" name="BookingPolicies[checkin_time_to]" class="form-control timepicker timepicker-24"> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-3">
                <label class="control-label">Checkout Time From</label>
                <div class="form-group">
                    <div>
                        <div class="input-icon">
                            <i class="fa fa-clock-o"></i>
                            <input type="text" id="check_out_from" value="<?=$check_out_from ?>" name="BookingPolicies[checkout_time_from]" class="form-control timepicker timepicker-24"> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-3">
                <label class="control-label">Checkout Time To</label>
                <div class="form-group">
                    <div>
                        <div class="input-icon">
                            <i class="fa fa-clock-o"></i>
                            <input type="text" id="check_out_to" value="<?=$check_out_to ?>" name="BookingPolicies[checkout_time_to]" class="form-control timepicker timepicker-24"> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($booking_policies_model, 'time_group', [
                    'inputOptions' => [
                                    'id' => 'time_group_dropdown',
                                    'class' => 'form-control',
                                    'style' => 'width: 100%'
                                    ]
            ])->dropdownList(EstimatedArrivalTimes::$time_groups,['prompt'=>'Select a Time Group']) ?>
        </div>
        <div class="col-xs-4">
            <label class="control-label">Printing late check-in notice updates booking flag</label>
            <div class="form-group">
                <input type="checkbox" <?= $statusCheckbox[$booking_policies_model->booking_flag_updated] ?> id="booking_flag_updated" class="make-switch" name="BookingPolicies[booking_flag_updated]" data-on-color="success" data-off-color="danger" data-on-text="Yes" data-off-text="No" >
            </div>
        </div>
    </div>

</div>
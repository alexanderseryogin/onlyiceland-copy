<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class importCsvModel extends Model
{
    public $csv_file;
    public $provider_id;
    public $update_existing_booking;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['csv_file'], 'file', 'skipOnEmpty' => true, 'extensions' => ['csv']],
            [['provider_id','update_existing_booking'],'integer'],
            ['provider_id','required'],
        ];
    }

    /** 
     * @inheritdoc 
     */ 
    public function attributeLabels() 
    {
        return [
            'csv_file' => Yii::t('app', 'Import CSV File'),
            'provider_id' => Yii::t('app', 'Select Destination'),
            'update_existing_booking' => Yii::t('app', 'Update Existing Booking'),
        ];
    } 
}

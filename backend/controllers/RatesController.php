<?php

namespace backend\controllers;

use Yii;
use common\models\Rates;
use common\models\RatesSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\DiscountCode;
use common\models\RatesDiscountCode;
use common\models\RatesDates;
use common\models\UpsellItems;
use common\models\RatesUpsell;
use yii\helpers\ArrayHelper;
use common\models\RatesAmenities;
use common\models\BookableItems;
use common\models\BookableAmenities;
use common\models\DestinationsAmenities;
use common\models\Amenities;
use common\models\Destinations;
use yii\filters\AccessControl;
use common\models\RatesMultiNightDiscount;
use common\models\BookingPolicies;
use common\models\BookableBedTypes;
use common\models\BedTypes;
use common\models\RatesCapacityPricing;
use common\models\Notifications;
use common\models\BookingsItems;
use common\models\BookingDates;
use common\models\RatesOverride;
use common\models\DestinationsOpenHoursExceptions;
/**
 * RatesController implements the CRUD actions for Rates model.
 */
class RatesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [ 
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'update-multi-night') 
        {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionDuplicateRate()
    {
        $existing_model = $this->findModel($_POST['data_id']);
        $model = new Rates();

        $model->provider_id = $existing_model->provider_id;
        $model->booking_confirmation_type_id = $existing_model->booking_confirmation_type_id ;
        $model->rate_period = $existing_model->rate_period ;
        $model->min_max_booking = $existing_model->min_max_booking ;
        $model->rate_allowed = $existing_model->rate_allowed ;
        $model->check_in_allowed = $existing_model->check_in_allowed ;
        $model->check_out_allowed = $existing_model->check_out_allowed ;
        $model->min_max_stay = $existing_model->min_max_stay ;
        $model->booking_status_id = $existing_model->booking_status_id ;
        $model->booking_flag_id = $existing_model->booking_flag_id ;
        $model->published = 0 ;
        $model->min_price = (string)$existing_model->min_price;
        $model->max_price = (string)$existing_model->max_price;
        $model->lodging_tax_per_night = $existing_model->lodging_tax_per_night;
        $model->vat_rate = $existing_model->vat_rate;

        if($existing_model->destination->pricing == 0)
        {
            $model->item_price_first_adult = (string)$existing_model->item_price_first_adult ;
            $model->item_price_additional_adult = (string)$existing_model->item_price_additional_adult ;
            $model->item_price_first_child = (string)$existing_model->item_price_first_child ;
            $model->item_price_additional_child = (string)$existing_model->item_price_additional_child ;
        }

        $date_model = RatesDates::find()->where(['rate_id' => $_POST['data_id']])->orderBy('night_rate DESC')->one();
        // $model->first_night_rate = date('Y-m-d',strtotime("+1 day", strtotime($date_model->night_rate)));

        if($_POST['action'] == 'duplicate')
        {
            $date_1 = RatesDates::find()->where(['rate_id' => $_POST['data_id']])->orderBy('night_rate ASC')->one();
            $date_2 = RatesDates::find()->where(['rate_id' => $_POST['data_id']])->orderBy('night_rate DESC')->one();

            // $model->first_night_rate = date('d/m/Y',strtotime($date_1->night_rate));
            // $model->last_night_rate = date('d/m/Y',strtotime($date_2->night_rate));

            $model->from = date('Y-m-d',strtotime("+1 days",strtotime($date_2->night_rate)) );
            $model->to = date('Y-m-d', strtotime("+30 days",strtotime($date_2->night_rate)) );

            $model->unit_id = $existing_model->unit_id;
            $model->name = $existing_model->name;

            $get_rates_discount = RatesDiscountCode::find()->where(['rates_id' => $_POST['data_id']])->all();

            if(!empty($get_rates_discount))
            {
                foreach ($get_rates_discount as $key => $value) 
                {
                    $obj = new RatesDiscountCode();
                    $obj->rates_id = $model->id;
                    $obj->discount_code_id = $value->discount_code_id;
                    $obj->save();
                }
            }
            
            $model->save();
            /*echo '<pre>';
            print_r($model->getErrors());
            exit;*/

            if($existing_model->destination->pricing)
            {
                if(!empty($existing_model->ratesCapacityPricings))
                {
                    foreach ($existing_model->ratesCapacityPricings as $key => $value) 
                    {
                        $obj = new RatesCapacityPricing();
                        $obj->rates_id = $model->id;
                        $obj->person_no = $value->person_no;
                        $obj->price = $value->price;
                        $obj->save();
                    }
                }
            }

            $date = date('Y-m-d',strtotime("+1 days",strtotime($date_2->night_rate)));

            for ($i=0; $i<30 ; $i++) 
            { 
                $rates_dates = new RatesDates();
                $rates_dates->rate_id = $model->id;

                if($i==0)
                {
                    $rates_dates->night_rate = $date;
                }
                else
                {
                    $date = strtotime("+1 day", strtotime($date));
                    $rates_dates->night_rate =  date('Y-m-d',$date);
                    $date = date("Y-m-d", $date);
                }

                $rates_dates->save();
            }
        }
        else if ($_POST['action'] == 'copy') 
        {
            $date_1 = RatesDates::find()->where(['rate_id' => $_POST['data_id']])->orderBy('night_rate ASC')->one();
            $date_2 = RatesDates::find()->where(['rate_id' => $_POST['data_id']])->orderBy('night_rate DESC')->one();

            $model->first_night_rate = date('d/m/Y',strtotime("+1 days",strtotime($date_2->night_rate)) );
            $model->last_night_rate = date('d/m/Y', strtotime("+30 days",strtotime($date_2->night_rate)) );

            // *********** Set Default Amenities of selected Bookable Item ************ //

            $bookable_amenities = [];
            $bookable_banners = [];
            $bookable_amenities_model = BookableAmenities::find()->where(['item_id' => $model->unit_id])->all();

            foreach ($bookable_amenities_model as $key => $value) 
            {
                $bookable_amenities[$value->amenities_id] = $value->amenities_id.','.$value->type;
                if($value->can_be_banner)
                    $bookable_banners[] = $value->amenities_id;
            }

            $model->amenities = $bookable_amenities;
            $model->amenities_banners = $bookable_banners;

            // *********** Set Default Pricing of selected Rate ************ //

            $get_rates_upsell = RatesUpsell::find()->where(['rates_id' => $existing_model->id])->all();

            if(!empty($get_rates_upsell))
            {
                foreach ($get_rates_upsell as $key => $value) 
                {
                    $pricing[$value['upsell_id']] = $value['price'];
                }
                $model->upsell_pricing = $pricing;
            }

            $saved_capacity_pricing = [];

            /*if($existing_model->destination->pricing)
            {
                if(!empty($existing_model->ratesCapacityPricings))
                {
                    foreach ($existing_model->ratesCapacityPricings as $key => $value) 
                    {
                        $saved_capacity_pricing[$value->person_no-1] = $value->price;
                    }
                    $model->rates_capacity_pricing = $saved_capacity_pricing;
                }
            }*/

            $session = Yii::$app->session;
            $_SESSION['copy_model'] = $model;
            
            return $this->redirect(['create']);
        }

        return;
    }

    /**
     * Lists all Rates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RatesSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['RatesSearch']) && !$this->isArrayEmpty($params['RatesSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }
        $check = 1;
        $dataProvider = $searchModel->search($params);
        //print_r($dataProvider); die;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params'       => $params
        ]);
    }

    /**
     * Displays a single Rates model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function listItems($provider_id)
    {
        $items = UpsellItems::find()->where(['provider_id' => $provider_id])->all();
        foreach ($items as $key => $value) 
        {
            Rates::$upsell_items[$value['id']] = $value['name'];
        }
    }

    public function actionCreate()
    {
       /* echo '<pre>';
            print_r($_POST);
            exit;*/
        $largest_capacity_price = 0;
        $smallest_capacity_price = 0;
        $multiNightDiscount = [
            'nights' => [
            ],
            'percent' => [
            ],
            'per_night' => [
            ],
            'once_off' => [
            ],
            'price_cap' => [
            ],
        ];

        $tab_href = '';
        $model = new Rates();
        $model->published = 0;

        $allIds = Amenities::find()->select('id')->column();
        $ids = RatesAmenities::find()->select('amenities_id')->where(['rates_id' => $model->id])->column();
        $ids = array_merge($ids, array_diff($allIds, $ids));
        $amenities = Amenities::find()
            ->orderBy(new \yii\db\Expression('FIELD (id, '.implode(',', $ids).')'))
            ->all();

        if ($model->load(Yii::$app->request->post())) 
        {
            $tab_href = $_POST['Rates']['tab_href'];

            $model->amenities = isset($_POST['Rates']['amenities_radio']) && !empty($_POST['Rates']['amenities_radio'])? $_POST['Rates']['amenities_radio']: NULL;

            $model->amenities_banners = isset($_POST['Rates']['amenities_checkbox']) && !empty($_POST['Rates']['amenities_checkbox'])? $_POST['Rates']['amenities_checkbox']: NULL;

            $model->discount_code = isset($_POST['Rates']['discount_codes']) ? $_POST['Rates']['discount_codes']:'';
            $model->upsell_pricing = isset($_POST['Rates']['upsell_pricing']) ? $_POST['Rates']['upsell_pricing']:'';
            $multiNightDiscount = isset($_POST['Rates']['multi_night']) ? $_POST['Rates']['multi_night']:'';

            if(!empty($model->upsell_pricing))
            {
                foreach ($model->upsell_pricing as $key => $value) 
                {
                    $model->upsell_pricing[$key]['price'] = $this->removeLeadingZero($value['price']);
                    $model->upsell_pricing[$key]['vat'] = str_replace('_', '', str_replace(",", ".", $value['vat']));
                    $model->upsell_pricing[$key]['tax'] = $value['tax'];
                    $model->upsell_pricing[$key]['fee'] = $value['fee'];
                    $model->upsell_pricing[$key]['show_on_frontend'] = isset($value['show_on_frontend'])?1:0;
                    $model->upsell_pricing[$key]['discountable'] = isset($value['discountable'])?1:0;
                    $model->upsell_pricing[$key]['commissionable'] = isset($value['commissionable'])?1:0;

                }
            }
            // echo "<pre>";
            // print_r($model->upsell_pricing);
            // exit();
            if(!empty($multiNightDiscount))
            {
                for ($i=0; $i < count($multiNightDiscount['nights']) ; $i++) 
                { 
                    $multiNightDiscount['percent'][$i] = !empty($multiNightDiscount['percent'][$i]) ? ltrim($multiNightDiscount['percent'][$i],'0'):'0';
                    $multiNightDiscount['per_night'][$i] = !empty($multiNightDiscount['per_night'][$i]) ? ltrim($multiNightDiscount['per_night'][$i],'0'):'0';
                    $multiNightDiscount['once_off'][$i] = !empty($multiNightDiscount['once_off'][$i]) ? ltrim($multiNightDiscount['once_off'][$i],'0'):'0';
                    $multiNightDiscount['price_cap'][$i] = !empty($multiNightDiscount['price_cap'][$i]) ? ltrim($multiNightDiscount['price_cap'][$i],'0'):'0';
                } 
            }

            switch ($model->destination->pricing) 
            {
                case 0: // First/Additional
                    
                    $model->item_price_first_adult = $this->removeLeadingZero($model->server_item_price_first_adult);
                    $model->item_price_additional_adult = $this->removeLeadingZero($model->server_item_price_additional_adult);
                    $model->item_price_first_child = $this->removeLeadingZero($model->server_item_price_first_child);
                    $model->item_price_additional_child = $this->removeLeadingZero($model->server_item_price_additional_child);

                    break;
                
                case 1: // Capacity
                    $model->rates_capacity_pricing = isset($_POST['Rates']['capacity-pricing'])?$_POST['Rates']['capacity-pricing']:'';

                    $count = 0;
                    if(!empty($model->rates_capacity_pricing))
                    {
                        foreach ($model->rates_capacity_pricing as $key => $value) 
                        {
                            $model->rates_capacity_pricing[$key] = $this->removeLeadingZero($value);
                            if($count == 0)
                            {
                                $smallest_capacity_price = $this->removeLeadingZero($value);
                            }
                            else
                            {
                                $largest_capacity_price = $this->removeLeadingZero($value);
                                
                            }
                            $count++;
                        }
                    }
                    break;
            }
            // echo "<pre>";
            // print_r($smallest_capacity_price);
            // print_r($largest_capacity_price);
            // exit();
            $model->min_price = $this->removeLeadingZero($model->server_min_price);
            $model->max_price = $this->removeLeadingZero($model->server_max_price);

            $model->lodging_tax_per_night = $this->removeLeadingZero($model->server_lodging_tax_per_night);
            $model->vat_rate = str_replace(',','.', $model->server_vat_rate);
            
            $model->published = (!isset($_POST['Rates']['published']))? 0 : 1 ;

            if($model->min_price >= $model->max_price)
            {
                Yii::$app->session->setFlash('error','Max Price must exceed Min Price');

                return $this->render('create', [
                    'model' => $model,
                    'tab_href' => $tab_href,
                    'multiNightDiscount' => $multiNightDiscount,
                ]);
            }

            if($model->destination->getDestinationTypeName()=='Accommodation' && $model->destination->pricing)
            {
                if($model->min_price > $smallest_capacity_price)
                {
                    Yii::$app->session->setFlash('error','The lowest capacity pricing value cannot be less than the Min Price value');

                    return $this->render('create', [
                        'model' => $model,
                        'tab_href' => $tab_href,
                        'multiNightDiscount' => $multiNightDiscount,
                        'amenities' => $amenities
                    ]);
                }
                if($model->max_price < $largest_capacity_price)
                {
                    Yii::$app->session->setFlash('error','The highest capacity pricing value must not exceed Max Price value');

                    return $this->render('create', [
                        'model' => $model,
                        'tab_href' => $tab_href,
                        'multiNightDiscount' => $multiNightDiscount,
                        'amenities' => $amenities
                    ]);
                }
            }

            // ********************* Validate Date Rates ********************* //

            $model->first_night_rate = strtotime(str_replace('/', '-', $model->first_night_rate));
            $model->last_night_rate = strtotime(str_replace('/', '-', $model->last_night_rate));

            $model->first_night_rate = date("Y-m-d", $model->first_night_rate);
            $model->last_night_rate = date("Y-m-d", $model->last_night_rate);

            $model->from = $model->first_night_rate;
            $model->to = $model->last_night_rate;

            $date = $model->first_night_rate;

            $date1=date_create($model->last_night_rate);
            $date2=date_create($model->first_night_rate);
            $diff=date_diff($date1,$date2);
            $difference =  $diff->format("%a")+1;  

            if($difference >=365)
            {
                Yii::$app->session->setFlash('error', 'Date limit should be less than 1 Year');

                $model->first_night_rate = date('d/m/Y',strtotime($model->first_night_rate));
                $model->last_night_rate = date('d/m/Y',strtotime($model->last_night_rate));

                return $this->render('create', [
                    'model' => $model,
                    'tab_href' => $tab_href,
                    'multiNightDiscount' => $multiNightDiscount,
                    'amenities' => $amenities
                ]);
            }

            $model->rate_allowed = implode(';', $model->rate_allowed);
            $model->check_in_allowed = implode(';', $model->check_in_allowed);
            $model->check_out_allowed = implode(';', $model->check_out_allowed);

            $model->min_max_stay = empty($model->min_max_stay)?'1;365':$model->min_max_stay; 

            if($model->save())
            {
                // *************** Save Amenities ************* //

                if(is_null($model->amenities_banners))
                        $model->amenities_banners = [];

                if(!empty($model->amenities))
                {
                    foreach ($model->amenities as $key => $value) 
                    {
                        $arr = explode(',', $value);

                        $obj = new RatesAmenities();
                        $obj->rates_id = $model->id;
                        $obj->amenities_id = $arr[0];
                        $obj->type = $arr[1];
                        $obj->can_be_banner = in_array($arr[0], $model->amenities_banners)?1:0;
                        $obj->save(); 
                    }
                }

                if(!empty($_POST['Rates']['discount_codes']))
                {
                    foreach ($_POST['Rates']['discount_codes'] as $key => $value) 
                    {
                        $obj = new RatesDiscountCode();
                        $obj->rates_id = $model->id;
                        $obj->discount_code_id = $value;
                        $obj->save();
                    }
                }

                if(!empty($model->upsell_pricing))
                {
                    foreach ($model->upsell_pricing as $key => $value) 
                    {
                        $obj = new RatesUpsell();
                        $obj->rates_id = $model->id;
                        $obj->upsell_id = $key;
                        $obj->price = $value['price'];
                        $obj->vat = $value['vat'];
                        $obj->tax = $value['tax'];
                        $obj->fee = $value['fee'];
                        $obj->show_on_frontend = $value['show_on_frontend'];
                        $obj->discountable = $value['discountable'];
                        $obj->commissionable = $value['commissionable'];
                        if(!$obj->save())
                        {
                            echo "<pre>";
                            print_r($obj->errors);
                            exit();
                        }
                    }
                }

                if(!empty($model->rates_capacity_pricing))
                {
                    foreach ($model->rates_capacity_pricing as $key => $value) 
                    {
                        $obj = new RatesCapacityPricing();
                        $obj->rates_id = $model->id;
                        $obj->person_no = $key+1;
                        $obj->price = $value;
                        $obj->save();
                    }
                }

                // *********************** Save Multi-Night Discount ************************ //

                if(!empty($multiNightDiscount))
                {
                    for ($i=0; $i < count($multiNightDiscount['nights']) ; $i++) 
                    { 
                        $obj = new RatesMultiNightDiscount();
                        $obj->rates_id = $model->id;
                        $obj->nights = $multiNightDiscount['nights'][$i];
                        $obj->percent = $multiNightDiscount['percent'][$i];
                        $obj->per_night = $multiNightDiscount['per_night'][$i];
                        $obj->once_off = $multiNightDiscount['once_off'][$i];
                        $obj->price_cap = $multiNightDiscount['price_cap'][$i];
                        $obj->save();
                    } 
                }

                for ($i=0; $i < $difference ; $i++) 
                { 
                    $rates_dates = new RatesDates();
                    $rates_dates->rate_id = $model->id;

                    if($i==0)
                    {
                        $rates_dates->night_rate = $date;
                    }
                    else
                    {
                        $date = strtotime("+1 day", strtotime($date));
                        $rates_dates->night_rate =  date('Y-m-d',$date);
                        $date = date("Y-m-d", $date);
                    }

                    $rates_dates->save();
                }

                // ********* Generate No rate available Notifications *********//

                Notifications::generateNoRateAvailableNotifications($model->unit);

                Yii::$app->session->setFlash('success','Rate was created successfully');
            }

            // echo '<pre>';
            // print_r($model->getErrors());
            // exit;

            return $this->redirect(['index']);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
                'tab_href' => $tab_href,
                'multiNightDiscount' => $multiNightDiscount,
                'amenities' => $amenities
            ]);
        }
    }

    /**
     * Updates an existing Rates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $largest_capacity_price = 0;
        $smallest_capacity_price = 0;

        $multiNightDiscount = [
            'nights' => [
            ],
            'percent' => [
            ],
            'per_night' => [
            ],
            'once_off' => [
            ],
            'price_cap' => [
            ],
        ];

        $ratesNightModel = RatesMultiNightDiscount::find()->where(['rates_id' => $id])->all();
        $savedNights = ArrayHelper::map($ratesNightModel,'id','nights');
 
        $originalMultiNightDiscount = $multiNightDiscount;

        $tab_href = '';
        $model = $this->findModel($id);

        $allIds = Amenities::find()->select('id')->column();
        $ids = RatesAmenities::find()->select('amenities_id')->where(['rates_id' => $model->id])->column();
        $ids = array_merge($ids, array_diff($allIds, $ids));
        $amenities = Amenities::find()
            ->orderBy(new \yii\db\Expression('FIELD (id, '.implode(',', $ids).')'))
            ->all();

        if(empty($model->vat_rate))
        {
            $model->vat_rate = $model->destination->destinationsFinancials->vat_rate;
            $model->server_vat_rate = (float)$model->vat_rate;
        }
        if(empty($model->lodging_tax_per_night))
        {
            $model->lodging_tax_per_night = $model->destination->destinationsFinancials->lodging_tax_per_night;
            $model->server_lodging_tax_per_night = $model->destination->destinationsFinancials->lodging_tax_per_night;
        }


        $searchModel = new RatesSearch();
        $dataProvider = $searchModel->searchMultiNight($id);

        $codes = DiscountCode::find()->all();
        foreach ($codes as $key => $value) 
        {
            Rates::$discount_codes[$value['id']] = $value['name'];
        }

        $date_1 = RatesDates::find()->where(['rate_id' => $id])->orderBy('night_rate ASC')->one();
        $date_2 = RatesDates::find()->where(['rate_id' => $id])->orderBy('night_rate DESC')->one();

        $model->first_night_rate = date('d/m/Y',strtotime($date_1->night_rate));
        $model->last_night_rate = date('d/m/Y',strtotime($date_2->night_rate));

        $get_amenities = RatesAmenities::find()->where(['rates_id' => $id])->orderBy('id')->all();

        if(!empty($get_amenities))
        {
            $saved_amenities = array();
            $saved_banners = array();

            foreach ($get_amenities as $key => $value) 
            {
                $temp = array();
                $temp[] = $value['amenities_id'];
                $temp[] = $value['type'];
                $temp = implode(',', $temp);
                $saved_amenities[$value['amenities_id']] = $temp;

                if($value['can_be_banner'])
                    $saved_banners[] = $value['amenities_id'];
            }
            $model->amenities = $saved_amenities;
            $model->amenities_banners = $saved_banners;
        }

        $get_rates_discount = RatesDiscountCode::find()->where(['rates_id' => $id])->all();

        if(!empty($get_rates_discount))
        {
            foreach ($get_rates_discount as $key => $value) 
            {
                $saved[] = $value['discount_code_id'];
            }
            $model->discount_code = $saved;
        }

        $get_rates_upsell = RatesUpsell::find()->where(['rates_id' => $id])->all();

        if(!empty($get_rates_upsell))
        {
            foreach ($get_rates_upsell as $key => $value) 
            {
                $checkbox[] = $value['upsell_id'];
                $pricing[$value['upsell_id']]['price'] = $value['price'];
                $pricing[$value['upsell_id']]['vat'] = $value['vat'];
                $pricing[$value['upsell_id']]['tax'] = $value['tax'];
                $pricing[$value['upsell_id']]['fee'] = $value['fee'];
                $pricing[$value['upsell_id']]['show_on_frontend'] = $value['show_on_frontend'];
                $pricing[$value['upsell_id']]['discountable'] = $value['discountable'];
                $pricing[$value['upsell_id']]['commissionable'] = $value['commissionable'];
            }
            $model->upsell_checkbox = $checkbox;
            $model->upsell_pricing = $pricing;
        }
        // echo "<pre>";
        // print_r($model->upsell_pricing);
        // exit();
        $get_rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $id])->all();

        $bookable_bed_types = BookableBedTypes::find()->where(['bookable_id' => $model->unit_id])->all();
        $sum = 0;
        foreach ($bookable_bed_types as $key => $value) 
        {
            $sum = $sum + ($value->bedType->max_sleeping_capacity * $value->quantity);
        }

        // echo "sum : ".$sum;
        // exit();
        // echo "<pre>";
        // print_r(count($get_rates_capacity_pricing));
        // exit;
        $rates_c_pricing_arr = array();
        
        if(!empty($get_rates_capacity_pricing))
        {
            if(count($get_rates_capacity_pricing) == $sum)
            {
                //echo "here in equal";
                
                foreach ($get_rates_capacity_pricing as $key => $value) 
                {
                    $rates_c_pricing_arr[$value['person_no']-1] = $value['price'];
                }

            }
            else if(count($get_rates_capacity_pricing) > $sum)
            {
                // echo "here in greater";
                // exit();
                $counter = 1;
                foreach ($get_rates_capacity_pricing as $key => $value) 
                {
                    if($counter > $sum)
                    {
                        break;
                    }
                    $rates_c_pricing_arr[$value['person_no']-1] = $value['price'];
                    $counter++;
                }   
            }
            else
            {
                // echo "here in smaller";
                // exit();
                $counter = 0;
                foreach ($get_rates_capacity_pricing as $key => $value) 
                {
                    $rates_c_pricing_arr[$value['person_no']-1] = $value['price'];
                    $counter++;
                }
                // echo "get_rates_capacity_pricing : ".count($get_rates_capacity_pricing); 
                // echo "counter"; 
                for ($i=$counter; $i < $sum; $i++) 
                { 
                    $rates_c_pricing_arr[$i] = 0;
                } 
            }
            
            $model->rates_capacity_pricing = $rates_c_pricing_arr;
        }

         // print_r($model->rates_capacity_pricing);
         //        exit();  

        // $bookable_bed_types = BookableBedTypes::find()->where(['bookable_id' => $item_id])->all();

        if ($model->load(Yii::$app->request->post())) 
        {
            //return $this->redirect(['index']);
            // echo '<pre>';
            // print_r($_POST);
            // exit;
            $tab_href = $_POST['Rates']['tab_href'];

            $model->amenities = isset($_POST['Rates']['amenities_radio']) && !empty($_POST['Rates']['amenities_radio'])? $_POST['Rates']['amenities_radio']: NULL;

            $model->amenities_banners = isset($_POST['Rates']['amenities_checkbox']) && !empty($_POST['Rates']['amenities_checkbox'])? $_POST['Rates']['amenities_checkbox']: NULL;

            $model->discount_code = isset($_POST['Rates']['discount_codes']) ? $_POST['Rates']['discount_codes']:'';
            $model->upsell_pricing = isset($_POST['Rates']['upsell_pricing']) ? $_POST['Rates']['upsell_pricing']:'';
            $multiNightDiscount = isset($_POST['Rates']['multi_night']) ? $_POST['Rates']['multi_night']:'';
            
            if(!empty($multiNightDiscount))
            {
                for ($i=0; $i < count($multiNightDiscount['nights']) ; $i++) 
                { 
                    $multiNightDiscount['percent'][$i] = !empty($multiNightDiscount['percent'][$i]) ? ltrim($multiNightDiscount['percent'][$i],'0'):'0';
                    $multiNightDiscount['per_night'][$i] = !empty($multiNightDiscount['per_night'][$i]) ? ltrim($multiNightDiscount['per_night'][$i],'0'):'0';
                    $multiNightDiscount['once_off'][$i] = !empty($multiNightDiscount['once_off'][$i]) ? ltrim($multiNightDiscount['once_off'][$i],'0'):'0';
                    $multiNightDiscount['price_cap'][$i] = !empty($multiNightDiscount['price_cap'][$i]) ? ltrim($multiNightDiscount['price_cap'][$i],'0'):'0';
                } 
            }

            if(!empty($model->upsell_pricing))
            {
                foreach ($model->upsell_pricing as $key => $value) 
                {
                    $model->upsell_pricing[$key]['price'] = $this->removeLeadingZero($value['price']);
                    $model->upsell_pricing[$key]['vat'] = str_replace('_', '', str_replace(",", ".", $value['vat']));
                    $model->upsell_pricing[$key]['tax'] = $value['tax'];
                    $model->upsell_pricing[$key]['fee'] = $value['fee'];
                    $model->upsell_pricing[$key]['show_on_frontend'] = isset($value['show_on_frontend'])?1:0;
                    $model->upsell_pricing[$key]['discountable'] = isset($value['discountable'])?1:0;
                    $model->upsell_pricing[$key]['commissionable'] = isset($value['commissionable'])?1:0;
                }
            }

            switch ($model->destination->pricing) 
            {
                case 0: // First/Additional
                    
                    $model->item_price_first_adult = $this->removeLeadingZero($model->server_item_price_first_adult);
                    $model->item_price_additional_adult = $this->removeLeadingZero($model->server_item_price_additional_adult);
                    $model->item_price_first_child = $this->removeLeadingZero($model->server_item_price_first_child);
                    $model->item_price_additional_child = $this->removeLeadingZero($model->server_item_price_additional_child);

                    break;
                
                case 1: // Capacity
                    $model->rates_capacity_pricing = isset($_POST['Rates']['capacity-pricing'])?$_POST['Rates']['capacity-pricing']:'';
                    $count = 0;
                    if(!empty($model->rates_capacity_pricing))
                    {
                        foreach ($model->rates_capacity_pricing as $key => $value) 
                        {
                            $model->rates_capacity_pricing[$key] = $this->removeLeadingZero($value);
                            if($count == 0)
                            {
                                $smallest_capacity_price = $this->removeLeadingZero($value);
                            }
                            else
                            {
                                $largest_capacity_price = $this->removeLeadingZero($value);
                                
                            }
                            $count++;
                        }
                    }
                    break;
            }

            $model->min_price = $this->removeLeadingZero($model->server_min_price);
            $model->max_price = $this->removeLeadingZero($model->server_max_price);

            $model->lodging_tax_per_night = $this->removeLeadingZero($model->server_lodging_tax_per_night);
            $model->vat_rate = str_replace(',','.', str_replace('_', '', $model->server_vat_rate));

            $model->published = (!isset($_POST['Rates']['published']))? 0 : 1 ;

            if($model->min_price >= $model->max_price)
            {
                Yii::$app->session->setFlash('error','Max Price must exceed Min Price');

                return $this->render('update', [
                    'model' => $model,
                    'tab_href' => $tab_href,
                    'multiNightDiscount' => $multiNightDiscount,
                    'dataProvider' => $dataProvider,
                    'savedNights' => $savedNights,
                    'amenities' => $amenities,
                ]);
            }
            // echo "<pre>";
            // print_r('smallest value :'.$smallest_capacity_price.'<br>largest :'.$largest_capacity_price);
            // exit();
            if($model->destination->getDestinationTypeName()=='Accommodation' && $model->destination->pricing)
            {
                if($model->min_price > $smallest_capacity_price)
                {
                    Yii::$app->session->setFlash('error','The lowest capacity pricing value cannot be less than the Min Price value');

                    return $this->render('update', [
                        'model' => $model,
                        'tab_href' => $tab_href,
                        'multiNightDiscount' => $multiNightDiscount,
                        'dataProvider' => $dataProvider,
                        'savedNights' => $savedNights,
                        'amenities' => $amenities,
                    ]);
                }
                if($model->max_price < $largest_capacity_price)
                {
                    Yii::$app->session->setFlash('error','The highest capacity pricing value must not exceed Max Price value');

                    return $this->render('update', [
                        'model' => $model,
                        'tab_href' => $tab_href,
                        'multiNightDiscount' => $multiNightDiscount,
                        'dataProvider' => $dataProvider,
                        'savedNights' => $savedNights,
                        'amenities' => $amenities,
                    ]);
                }
            }

            // ********************** Date Rates ********************** //

            $model->first_night_rate = strtotime(str_replace('/', '-', $model->first_night_rate));
            $model->last_night_rate = strtotime(str_replace('/', '-', $model->last_night_rate));

            $model->first_night_rate = date("Y-m-d", $model->first_night_rate);
            $model->last_night_rate = date("Y-m-d", $model->last_night_rate);

            $model->from = $model->first_night_rate;
            $model->to = $model->last_night_rate;

            $date = $model->first_night_rate;

            $date1=date_create($model->last_night_rate);
            $date2=date_create($model->first_night_rate);
            $diff=date_diff($date1,$date2);
            $difference =  $diff->format("%a")+1;  

            if($difference >=365)
            {
                Yii::$app->session->setFlash('error', 'Date limit should be less than 1 Year');

                $model->first_night_rate = date('d/m/Y',strtotime($model->first_night_rate));
                $model->last_night_rate = date('d/m/Y',strtotime($model->last_night_rate));

                return $this->render('update', [
                    'model' => $model,
                    'tab_href' => $tab_href,
                    'multiNightDiscount' => $multiNightDiscount,
                    'dataProvider' => $dataProvider,
                    'savedNights' => $savedNights,
                    'amenities' => $amenities
                ]);
            }

            $model->rate_allowed = implode(';', $model->rate_allowed);
            $model->check_in_allowed = implode(';', $model->check_in_allowed);
            $model->check_out_allowed = implode(';', $model->check_out_allowed);

            $model->min_max_stay = empty($model->min_max_stay)?'1;365':$model->min_max_stay;

            if($model->save())
            {
                RatesDiscountCode::deleteAll(['rates_id' => $model->id]);
                RatesUpsell::deleteAll(['rates_id' => $model->id]);
                RatesCapacityPricing::deleteAll(['rates_id' => $model->id]);

                // *************** Save Amenities ************* //

                if(is_null($model->amenities_banners))
                    $model->amenities_banners = [];

                //echo "<pre>"; print_r($model->amenities); exit;

                if(!empty($model->amenities))
                {
                    RatesAmenities::deleteAll(['rates_id' => $model->id]);
                    foreach ($model->amenities as $key => $value) 
                    {
                        $arr = explode(',', $value);

                        $obj = new RatesAmenities();
                        $obj->rates_id = $model->id;
                        $obj->amenities_id = $arr[0];
                        $obj->type = $arr[1];
                        $obj->can_be_banner = in_array($arr[0], $model->amenities_banners)?1:0;
                        $obj->save(); 
                    }
                }

                if(isset($_POST['Rates']['discount_codes']) && !empty($_POST['Rates']['discount_codes']))
                {
                    foreach ($_POST['Rates']['discount_codes'] as $key => $value) 
                    {
                        $obj = new RatesDiscountCode();
                        $obj->rates_id = $model->id;
                        $obj->discount_code_id = $value;
                        $obj->save();
                    }
                }

                if(!empty($model->upsell_pricing))
                {
                    foreach ($model->upsell_pricing as $key => $value) 
                    {
                        $obj = new RatesUpsell();
                        $obj->rates_id = $model->id;
                        $obj->upsell_id = $key;
                        $obj->price = $value['price'];
                        $obj->vat = $value['vat'];
                        $obj->tax = $value['tax'];
                        $obj->fee = $value['fee'];
                        $obj->show_on_frontend = $value['show_on_frontend'];
                        $obj->discountable = $value['discountable'];
                        $obj->commissionable = $value['commissionable'];
                        $obj->save();
                    }
                }

                if(!empty($model->rates_capacity_pricing))
                {
                    foreach ($model->rates_capacity_pricing as $key => $value) 
                    {
                        $obj = new RatesCapacityPricing();
                        $obj->rates_id = $model->id;
                        $obj->person_no = $key+1;
                        $obj->price = $value;
                        $obj->save();
                    }
                }

                // *********************** Save Multi-Night Discount ************************ //

                if(!empty($multiNightDiscount))
                {
                    for ($i=0; $i < count($multiNightDiscount['nights']) ; $i++) 
                    { 
                        $obj = new RatesMultiNightDiscount();
                        $obj->rates_id = $model->id;
                    
                        $obj->nights = $multiNightDiscount['nights'][$i];
                        $obj->percent = $multiNightDiscount['percent'][$i];
                        $obj->per_night = $multiNightDiscount['per_night'][$i];
                        $obj->once_off = $multiNightDiscount['once_off'][$i];
                        $obj->price_cap = $multiNightDiscount['price_cap'][$i];
                        $obj->save();
                    } 
                }

                $multiNightDiscount = $originalMultiNightDiscount;

                // ************************  Save date record into date_rates table *********************** //

                $rates_dates = RatesDates::deleteAll(['rate_id' => $model->id]);

                for ($i=0; $i < $difference ; $i++) 
                { 
                    $rates_dates = RatesDates::findOne(['rate_id' => $model->id, 'night_rate' => $date]);

                    if(empty($rates_dates))
                    {
                        $rates_dates = new RatesDates();
                        $rates_dates->rate_id = $model->id;
                        $rates_dates->night_rate =  $date;
                        $rates_dates->save();
                    }
                    
                    $date = strtotime("+1 day", strtotime($date));
                    $date = date("Y-m-d", $date);
                }

                // ********* Generate No rate available Notifications *********//

                Notifications::generateNoRateAvailableNotifications($model->unit);

                Yii::$app->session->setFlash('success','Record was updated successfully');
            }

            if(!$model->save())
            {
                echo "<pre>";
                print_r($model->errors);
                exit();
            }
            /*$model->first_night_rate = date('d/m/Y',strtotime($model->first_night_rate));
            $model->last_night_rate = date('d/m/Y',strtotime($model->last_night_rate));

            $model->rate_allowed = explode(';', $model->rate_allowed);
            $model->check_in_allowed = explode(';', $model->check_in_allowed);
            $model->check_out_allowed = explode(';', $model->check_out_allowed);
            
            return $this->render('update', [
                'model' => $model,
                'tab_href' => $tab_href,
                'multiNightDiscount' => $multiNightDiscount,
                'dataProvider' => $dataProvider,
            ]);*/
            if(isset($_POST['save']))
            {
                return $this->redirect(['update','id' => $model->id]);
            }
            else
            {
                return $this->redirect(['index']);
            }
            
        }  
        else 
        {
            $model->rate_allowed = explode(';', $model->rate_allowed);
            $model->check_in_allowed = explode(';', $model->check_in_allowed);
            $model->check_out_allowed = explode(';', $model->check_out_allowed);

            $model->server_item_price_first_adult = $model->item_price_first_adult;
            $model->server_item_price_additional_adult = $model->item_price_additional_adult;
            $model->server_item_price_first_child = $model->item_price_first_child;
            $model->server_item_price_additional_child = $model->item_price_additional_child;
            $model->server_min_price = $model->min_price;
            $model->server_max_price = $model->max_price;

            return $this->render('update', [
                'model' => $model,
                'tab_href' => $tab_href,
                'multiNightDiscount' => $multiNightDiscount,
                'dataProvider' => $dataProvider,
                'savedNights' => $savedNights,
                'amenities' => $amenities
            ]);
        }
    }

    /**
     * Deletes an existing Rates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist');
        }
    }

    public function actionListitems($id=null,$provider_id)
    {
        $destination = Destinations::findOne(['id' => $provider_id]);

        if($id == null)
        {
            $vat_rate = $destination->destinationsFinancials->vat_rate;
            $logging_tax = $destination->destinationsFinancials->lodging_tax_per_night;
        }
        else
        {
            $rate = Rates::findOne(['id' => $id]);
            $vat_rate = $rate->vat_rate;
            $logging_tax = $rate->lodging_tax_per_night;
        }

        echo json_encode([
                'bookableItems' => $this->renderAjax('bookable_items', [
                                        'provider_id' => $provider_id,
                                    ]),
                'discountCodes' => $this->renderAjax('dynamic_discount_codes', [
                                        'provider_id' => $provider_id,
                                    ]),
                'upsellItems' => $this->renderAjax('dynamic_upsell_items', [
                                        'provider_id' => $provider_id,
                                    ]),
                'pricing' => $destination->pricing,
                'vat_rate' => $vat_rate,
                'lodging_tax' => $vat_rate,
            ]);
    }

    public function actionListAmenities($item_id)
    {
        $html = '<div><span class="label label-danger"></span>
                        <span>&nbsp;  There were no amenities found </span></div>';

        if($item_id=="null" || empty($item_id))
        {
            echo json_encode([
                'amenities' => $html,
                'general_booking' => 'Not Found',
                'flag' => '',
                'status' => '',
                'confirmation' => '',
                'pricing_fields' => '',
                'pricing' => '',
                'from' => '',
            ]);
        }
        else
        {
            $bookable_item = BookableItems::findOne(['id' => $item_id]);
            $provider_id = $bookable_item->provider_id;
            $destination = Destinations::findOne(['id' => $provider_id]);

            $lastRate = Rates::find()->where(['unit_id' => $item_id])->orderBy('to DESC')->one();
            $from = '';
            $to = '';
            if(!empty($lastRate))
            {
                $from = date('d/m/Y',strtotime("+1 day", strtotime($lastRate->to)));
                $to = date('d/m/Y',strtotime("+2 day", strtotime($lastRate->to)));
            }

            if($destination->getDestinationTypeName()!='Accommodation')
            {
                if($destination->pricing == 0) // first/additioal pricies
                {
                    $pricing_fields = '';
                }
                else if($destination->pricing == 1) // capacity
                {
                    $pricing_fields = '<div class="capacity-error alert-danger alert fade in">No bed types found</div>';
                }

                echo json_encode([
                    'amenities' => $html,
                    'general_booking' => 'Not found',
                    'flag' => '',
                    'status' => '',
                    'confirmation' => '',
                    'pricing_fields' => $pricing_fields,
                    'pricing' => $destination->pricing,
                    'from' => $from,
                    'to' => $to
                ]);
            }
            else
            {
                $bookable_amenities = [];
                $bookable_banners = [];
                $bookable_amenities_model = BookableAmenities::find()->where(['item_id' => $item_id])->all();

                foreach ($bookable_amenities_model as $key => $value) 
                {
                    $bookable_amenities[$value->amenities_id] = $value->amenities_id.','.$value->type;
                    if($value->can_be_banner)
                        $bookable_banners[] = $value->amenities_id;
                }

                $sum = 0;

                if($destination->pricing == 1) // capacity
                {
                    $bookable_bed_types = BookableBedTypes::find()->where(['bookable_id' => $item_id])->all();

                    foreach ($bookable_bed_types as $key => $value) 
                    {
                        $sum = $sum + ($value->bedType->max_sleeping_capacity * $value->quantity);
                    }
                }

                echo json_encode([
                    'amenities' => $this->renderAjax('total_amenities_dynamic', [
                                            'selected_amenities' => $bookable_amenities,
                                            'selected_banners' => $bookable_banners,
                                        ]),
                    'general_booking' => $bookable_item->general_booking_cancellation,
                    'flag' => $bookable_item->booking_type_id,
                    'status' => $bookable_item->booking_status_id,
                    'confirmation' => $bookable_item->booking_confirmation_type_id,
                    'pricing_fields' => $this->renderAjax('pricing_fields', [
                                            'sum' => $sum,
                                        ]),
                    'pricing' => $destination->pricing,
                    'from' => $from,
                    'to' => $to
                ]);
            }
        }
    }

    public function actionGetMultiNightHtml()
    {
        echo json_encode([
            'multi_night_html' => $this->renderAjax('dynamic_multi_night_html', [
                                ]),
        ]);
    }

    public function actionFixRates()
    {
        $rates = Rates::find()->all();

        foreach ($rates as $rate) 
        {
            $from = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one();
            $to = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one();

            $rate->from = $from->night_rate;
            $rate->to = $to->night_rate;
            //$rate->save();
            if(!$rate->save(false))
            {
                echo "<pre>";
                print_r($rate->errors);
                exit();
            }
        }
    }

    public function actionDeleteMultiNight($id)
    {
        $obj = RatesMultiNightDiscount::findOne(['id'=>$id]);
        $obj->delete();
    }

    public function actionUpdateMultiNight()
    {
        $obj = RatesMultiNightDiscount::findOne(['id'=>$_POST['id']]);
        //$obj->nights = $_POST['nights'];
        $obj->percent = empty(ltrim($_POST['percent'],'0'))?0:ltrim($_POST['percent'],'0');
        $obj->per_night = empty(ltrim($_POST['per_night'],'0'))?0:ltrim($_POST['per_night'],'0');
        $obj->once_off = empty(ltrim($_POST['once_off'],'0'))?0:ltrim($_POST['once_off'],'0');
        $obj->price_cap = empty(ltrim($_POST['price_cap'],'0'))?0:ltrim($_POST['price_cap'],'0');
        $obj->save();
    }

    public function removeLeadingZero($value)
    {
        if($value!='' && preg_match("/^0+$/", $value))
        {
            $value = '0';
        } 
        else 
        {
            $value = ltrim($value,'0');
        }
        return $value;
    }

    public function actionRatesScheduler($destination_id = null, $exp_date = null)
    {
        if($exp_date == NULL)
        {
            $date = date("Y-m-d");
            //$date = explode("-", $current_date);
            //$date = $date[2];
        }
        else
        {
            $date = $exp_date;
            //$date = $date[2];   
        }
        // echo "<pre>";
        // print_r($date[2]);
        // exit();
        return $this->render('rates_schedular',[
            'destination_id' => $destination_id,
           // 'bookable_item_id' => $bookable_item_id,
            'date'  => $date,
            // 'date'  => 25-1,
        ]);
    }

    public function actionGetRatesSchedular() // use for Scheduler
    {
        $session = Yii::$app->session;
        $session->open();
        
        $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
                                ->all();
    
        $bookings_items_arr = [];
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);

        if(!empty($destination->bookableItems))
        {
            $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            $closed_dates_arr = array();
            if(!empty($closed_dates))
            {
                foreach ($closed_dates  as $date) 
                {
                    array_push($closed_dates_arr, $date->date);
                }
            }

            $schedular_resources = $this->generateSchedularResources(Destinations::findOne(['id' => $_POST['destination_id'] ]));

            // echo "<pre>";
            // print_r($schedular_resources);
            // exit();
            // foreach ($bookingsItemsModel as $key => $model) 
            // {
            //     $bookings_items_arr[] = $this->generateSchedularEvent($model);
            // }

            $events = $this->generateSchedularEvent(Destinations::findOne(['id' => $_POST['destination_id'] ]));

            // echo "<pre>";
            // print_r($events);
            // exit();
            // echo "<pre>";
            // print_r($schedular_resources);
            // exit();
            

            echo json_encode([
                'events' => json_encode($events),
                'bookable_items' => $this->renderAjax('bookable_items_scheduler',['provider_id' => $_POST['destination_id']]),
                'resources'     => json_encode($schedular_resources),
                'empty' => 0,
                'date' => date('Y-m-d'),
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
            $params['provider_id'] =  $_POST['destination_id'];
            $session[Yii::$app->controller->id.'-booking-schedular-values'] = json_encode($params);
                
        }
        else
        {
            echo json_encode([
                'empty' => 1,
            ]); 
        }
    }

    public function generateSchedularEvent($destination)
    {
        $month_start = date('Y-m-01',strtotime(date('Y-m-d')));
        $month_end =  date('Y-m-t',strtotime(date('Y-m-d')));

        $events = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                foreach ($bookable_item->rates as $rate) 
                {   
                    if($rate->published == 1)
                    {

                        if( (strtotime($month_start) <  strtotime($rate->from)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) <  strtotime($rate->to)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) >  strtotime($rate->to)  &&  strtotime($month_end) < strtotime($rate->from) )
                            )
                        {
                            foreach ($rate->dates as $date) 
                            {
                                if(strtotime($date->night_rate) >=  strtotime($month_start) && strtotime($date->night_rate) <=  strtotime($month_end))
                                {
                                    $rate_item['id'] = $rate->id;
                                    // $booking_date = BookingDates::findOne(['booking_item_id' => $model->id]);
                                    $name = '';
                                    // $name .= !empty($booking_date->guest_first_name)?$booking_date->guest_first_name.' ':'';
                                    // $name .= !empty($booking_date->guest_last_name)?$booking_date->guest_last_name:'';

                                    if(empty($name))
                                        $name = 'Unknown';

                                    // removed following line 
                                    // .$model->bookingItemGroups->bookingGroup->group_name
                                    // echo "<pre>";
                                    // print_r($rate->ratesCapacityPricings);
                                    // exit();
                                    if($destination->pricing == 1)
                                    {
                                        $name ='';
                                        if(isset($rate->ratesCapacityPricings[0]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            $name .= Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                        }
                                        if(isset($rate->ratesCapacityPricings[1]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            $name .= ', '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[1]->price);
                                        }
                                        if(isset($rate->ratesCapacityPricings[2]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            $name .= ', '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[2]->price);
                                        }
                                        if(isset($rate->ratesCapacityPricings[3]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            $name .= ', '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[3]->price);
                                        }

                                        if($name == '' )
                                        {
                                            $rate_item['title'] = $rate->id.' - '.$name;
                                        }
                                        else
                                        {
                                            $rate_item['title'] = $name;
                                        }
                                    }
                                    else
                                    {
                                        $rate_item['title'] = $rate->id.' - '.$name;
                                    }
                                    // $rate_item['title'] = $rate->ratesCapacityPricings[0]->price;
                                    // $rate_item['title'] = $rate->id.' - '.$name;
                                    $rate_item['resourceId'] = $rate->id;
                                    $rate_item['start'] = $date->night_rate;
                                    $rate_item['end'] = $date->night_rate;
                                    // $rate_item['borderColor'] = '#000000';
                                    $rate_item['textColor'] = 'black';
                                    $rate_item['bookable_item_id'] = $bookable_item->id;
                                    // $rate_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);

                                    array_push($events, $rate_item);
                                }
                                    
                            }
                        }
                            
                       
                    }
                }
            }
            
        }

        return $events;
    }

    public function generateSchedularResources($destination)
    {
        $month_start = date('Y-m-01',strtotime(date('Y-m-d')));
        $month_end =  date('Y-m-t',strtotime(date('Y-m-d')));

        // echo "<pre>";
        // print_r($search_form);
        // print_r($search_to);
        // exit();
        $resources = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                $rates_count = count($bookable_item->rates);
                $counter = 1;
                foreach ($bookable_item->rates as $rate) 
                {   
                    if($rate->published == 1)
                    {

                        if( (strtotime($month_start) <  strtotime($rate->from)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) <  strtotime($rate->to)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) >  strtotime($rate->to)  &&  strtotime($month_end) < strtotime($rate->from) )
                            )
                        {

                        
                            $item_array = array();
                            $item_array['id'] = $rate->id;
                            $item_array['item'] = $bookable_item->itemType->name;
                            $item_array['title'] = $rate->name;
                            // $item_array['eventColor'] = $bookable_item->background_color;
                            $item_array['bookable_item_id'] = $bookable_item->id;
                            $item_array['background_color'] = $bookable_item->background_color;
                            $item_array['text_color'] = $bookable_item->text_color;

                            array_push($resources, $item_array);
                            // if($counter == $item_names_count)
                            // {
                            //     $item_array = array();
                            //     $item_array['id'] = $bookable_item->id.$destination->id;
                            //     $item_array['item'] = $bookable_item->itemType->name;
                            //     $item_array['title'] = "No Unit Assigned";
                            //     $item_array['eventColor'] = $bookable_item->background_color;
                            //     $item_array['bookable_item_id'] = $bookable_item->id;
                            //     $item_array['background_color'] = $bookable_item->background_color;
                            //     $item_array['text_color'] = $bookable_item->text_color;

                            //     array_push($resources, $item_array);
                            // }
                            $counter++;
                        }
                    }
                }
            }
            return $resources;
        }
    }

    public function actionGetRatesInputs()
    {
        $rate = Rates::findOne(['id' => $_POST['rate_id'] ]);
        $bookable_item_id = $_POST['bookable_item_id'];
        $is_bookabele_item_closed = RatesOverride::findOne(['rate_id'=> $rate->id,'date' => $_POST['date'],'bookable_item_id' => $bookable_item_id, 'bookable_item_closed' => 1]);


        echo json_encode([
                // 'events' => $events,
                'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date']]),
                'is_item_closed' => !empty($is_bookabele_item_closed)?1:0
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

    public function actionGetRatesInputsWithPercentage()
    {
        $rate = Rates::findOne(['id' => $_POST['rate_id'] ]);
        

        echo json_encode([
                // 'events' => $events,
                'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

    public function actionOverrideRates()
    {

        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        $bookable_items =  isset($_POST['bookable_items'])?$_POST['bookable_items']:[];
        $rate_from = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_from'])));
        $rate_to = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_to'])));
        $rate_days = isset($_POST['rate_days'])?$_POST['rate_days']:[];
        $override_type = ($_POST['override_type'])?1:0;
        $cpricing = isset($_POST['cpricing'])?$_POST['cpricing']:[];

        $date1=date_create($rate_from);
        $date2=date_create($rate_to);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");


        // if($destination->pricing == 1)
        // {
            foreach ($bookable_items as $value) 
            {
                $rates = Rates::find()->where(['unit_id' => $value])->all();
                
                if(!empty($rates))
                {
                    foreach ($rates as $rate) 
                    {
                        $date = $rate_from;
                        $date = date("Y-m-d", strtotime($date));
                        for ($i=0; $i <= $difference ; $i++)
                        {
                            
                            if(strtotime($date)>= strtotime($rate->from) && (strtotime($date)<= strtotime($rate->to)) )
                            {
                                $day = date('N', strtotime($date));
                                if($rate_days[$day-1] == 'true')
                                {
                                    $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id,'bookable_item_id' => $rate->unit_id]);
                                    if(empty($rate_override))
                                    {
                                        $rate_override = new RatesOverride;
                                    }
                                    
                                    $rate_override->rate_id = $rate->id;
                                    $rate_override->bookable_item_id = $rate->unit_id;
                                    $rate_override->date = $date;
                                    $rate_override->override_type = $override_type;
                                    foreach ($cpricing as $key => $price) 
                                    {
                                        $price = str_replace(',', '.', str_replace('.', '', $price));
                                        if($key==0)
                                        {   
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->single = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_first_adult = $price;
                                            }
                                        }
                                        if($key==1)
                                        {
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->double = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_additional_adult = $price;
                                            }
                                        }
                                        if($key==2)
                                        {
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->triple = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_first_child = $price;
                                            }
                                        }
                                        if($key==3)
                                        {
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->quad = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_additional_child = $price;
                                            }
                                        }
                                    }
                                    $rate_override->save();

                                }
                            }
                            $date = strtotime("+1 day", strtotime($date));
                            $date = date("Y-m-d", $date);
                        }
                    }
                }
                
            }
        // }
        // else
        // {

        // }

        $schedular_resources = $this->generateSchedularResources($destination);

        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

        $closed_dates_arr = array();
        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                array_push($closed_dates_arr, $date->date);
            }
        }
        // exit();
        // $dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        // $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        echo json_encode([
                'success' => 'true',
                'events' => [],
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                'resources'     => $schedular_resources,
                'empty' => 0,
                'date' => $rate_from,
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }


    // closing bookable items //

    public function actionCloseBookableItem()
    {

        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        $bookable_items =  isset($_POST['bookable_items'])?$_POST['bookable_items']:[];
        $rate_from = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_from'])));
        $rate_to = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_to'])));
        $rate_days = isset($_POST['rate_days'])?$_POST['rate_days']:[];
        $override_type = ($_POST['override_type'])?1:0;
        $cpricing = isset($_POST['cpricing'])?$_POST['cpricing']:[];

        $date1=date_create($rate_from);
        $date2=date_create($rate_to);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");


        // if($destination->pricing == 1)
        // {
            foreach ($bookable_items as $value) 
            {
                $rates = Rates::find()->where(['unit_id' => $value])->all();
                
                if(!empty($rates))
                {
                    foreach ($rates as $rate) 
                    {
                        $date = $rate_from;
                        $date = date("Y-m-d", strtotime($date));
                        for ($i=0; $i <= $difference ; $i++)
                        {
                            
                            if(strtotime($date)>= strtotime($rate->from) && (strtotime($date)<= strtotime($rate->to)) )
                            {
                                $day = date('N', strtotime($date));
                                if($rate_days[$day-1] == 'true')
                                {
                                    $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id,'bookable_item_id' => $rate->unit_id]);
                                    if(empty($rate_override))
                                    {
                                        $rate_override = new RatesOverride;
                                    }
                                    
                                    $rate_override->rate_id = $rate->id;
                                    $rate_override->bookable_item_id = $rate->unit_id;
                                    $rate_override->date = $date;
                                    $rate_override->override_type = $override_type;
                                    foreach ($cpricing as $key => $price) 
                                    {
                                        $price = str_replace(',', '.', str_replace('.', '', $price));
                                        if($key==0)
                                        {   
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->single = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_first_adult = $price;
                                            }
                                        }
                                        if($key==1)
                                        {
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->double = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_additional_adult = $price;
                                            }
                                        }
                                        if($key==2)
                                        {
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->triple = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_first_child = $price;
                                            }
                                        }
                                        if($key==3)
                                        {
                                            if($destination->pricing == 1)
                                            {
                                                $rate_override->quad = $price;
                                            }
                                            else
                                            {
                                                $rate_override->item_price_additional_child = $price;
                                            }
                                        }
                                    }
                                    // closing bookable items // 
                                    $rate_override->bookable_item_closed = 1;
                                    $rate_override->save();

                                }
                            }
                            $date = strtotime("+1 day", strtotime($date));
                            $date = date("Y-m-d", $date);
                        }
                    }
                }
                
            }
        // }
        // else
        // {

        // }

        $schedular_resources = $this->generateSchedularResources($destination);

        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

        $closed_dates_arr = array();
        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                array_push($closed_dates_arr, $date->date);
            }
        }
        // exit();
        // $dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        // $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        echo json_encode([
                'success' => 'true',
                'events' => [],
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                'resources'     => $schedular_resources,
                'empty' => 0,
                'date' => $rate_from,
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

    // open bookable item //

    public function actionOpenBookableItem()
    {

        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        $bookable_items =  isset($_POST['bookable_items'])?$_POST['bookable_items']:[];
        $rate_from = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_from'])));
        $rate_to = date('Y-m-d',strtotime(str_replace('/', '-', $_POST['rate_to'])));
        $rate_days = isset($_POST['rate_days'])?$_POST['rate_days']:[];
        $override_type = ($_POST['override_type'])?1:0;
        $cpricing = isset($_POST['cpricing'])?$_POST['cpricing']:[];

        $date1=date_create($rate_from);
        $date2=date_create($rate_to);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");


        // if($destination->pricing == 1)
        // {
            foreach ($bookable_items as $value) 
            {
                $rates = Rates::find()->where(['unit_id' => $value])->all();
                
                if(!empty($rates))
                {
                    foreach ($rates as $rate) 
                    {
                        $date = $rate_from;
                        $date = date("Y-m-d", strtotime($date));
                        for ($i=0; $i <= $difference ; $i++)
                        {
                            
                            if(strtotime($date)>= strtotime($rate->from) && (strtotime($date)<= strtotime($rate->to)) )
                            {
                                $day = date('N', strtotime($date));
                                if($rate_days[$day-1] == 'true')
                                {
                                    $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id,'bookable_item_id' => $rate->unit_id, 'bookable_item_closed' => 1]);
                                    if(!empty($rate_override))
                                    {
                                        $rate_override->delete();
                                    }
                                    
                                    // $rate_override->rate_id = $rate->id;
                                    // $rate_override->bookable_item_id = $rate->unit_id;
                                    // $rate_override->date = $date;
                                    // $rate_override->override_type = $override_type;
                                    // foreach ($cpricing as $key => $price) 
                                    // {
                                    //     $price = str_replace(',', '.', str_replace('.', '', $price));
                                    //     if($key==0)
                                    //     {   
                                    //         if($destination->pricing == 1)
                                    //         {
                                    //             $rate_override->single = $price;
                                    //         }
                                    //         else
                                    //         {
                                    //             $rate_override->item_price_first_adult = $price;
                                    //         }
                                    //     }
                                    //     if($key==1)
                                    //     {
                                    //         if($destination->pricing == 1)
                                    //         {
                                    //             $rate_override->double = $price;
                                    //         }
                                    //         else
                                    //         {
                                    //             $rate_override->item_price_additional_adult = $price;
                                    //         }
                                    //     }
                                    //     if($key==2)
                                    //     {
                                    //         if($destination->pricing == 1)
                                    //         {
                                    //             $rate_override->triple = $price;
                                    //         }
                                    //         else
                                    //         {
                                    //             $rate_override->item_price_first_child = $price;
                                    //         }
                                    //     }
                                    //     if($key==3)
                                    //     {
                                    //         if($destination->pricing == 1)
                                    //         {
                                    //             $rate_override->quad = $price;
                                    //         }
                                    //         else
                                    //         {
                                    //             $rate_override->item_price_additional_child = $price;
                                    //         }
                                    //     }
                                    // }
                                    // // closing bookable items // 
                                    // $rate_override->bookable_item_closed = 1;
                                    // $rate_override->save();

                                }
                            }
                            $date = strtotime("+1 day", strtotime($date));
                            $date = date("Y-m-d", $date);
                        }
                    }
                }
                
            }
        // }
        // else
        // {

        // }

        $schedular_resources = $this->generateSchedularResources($destination);

        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

        $closed_dates_arr = array();
        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                array_push($closed_dates_arr, $date->date);
            }
        }
        // exit();
        // $dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        // $dowMap = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        echo json_encode([
                'success' => 'true',
                'events' => [],
                // 'rate_inputs' => $this->renderAjax('rate_inputs',['rate' => $rate , 'date' => $_POST['date'], 'percentage' => $_POST['percentage']]   ),
                // 'resources'     => $resources,
                'resources'     => $schedular_resources,
                'empty' => 0,
                'date' => $rate_from,
                'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }

    // generating events and resources for ajax calls //

    public function actionGenerateSchedularEvents()
    {
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        // $month_start = date('Y-m-d',strtotime($_POST['date']));
        // $month_start = date('Y-m-01',strtotime($_POST['date']));
        // $month_end =  date('Y-m-t',strtotime($_POST['date']));

        $month_start = date('Y-m-d',strtotime($_POST['start']));
        $month_end =  date('Y-m-d',strtotime($_POST['end']));
        // echo "<pre>";
        // print_r(date('Y-m-d',strtotime($_POST['start'])));
        // print_r(date('Y-m-d',strtotime($_POST['end'])));
        // exit();
        // generating events // 


        $events = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                foreach ($bookable_item->rates as $rate) 
                {   
                    if($rate->published == 1)
                    {

                        if( (strtotime($month_start) <  strtotime($rate->from)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) <  strtotime($rate->to)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) >  strtotime($rate->to)  &&  strtotime($month_end) < strtotime($rate->from) )
                            )
                        {
                            if($destination->pricing == 1)
                            {
                                foreach ($rate->dates as $date) 
                                {
                                    if(strtotime($date->night_rate) >=  strtotime($month_start) && strtotime($date->night_rate) <=  strtotime($month_end))
                                    {

                                        $rate_item['id'] = $rate->id;
                                        // $booking_date = BookingDates::findOne(['booking_item_id' => $model->id]);
                                        // $name = '';
                                        // $name .= !empty($booking_date->guest_first_name)?$booking_date->guest_first_name.' ':'';
                                        // $name .= !empty($booking_date->guest_last_name)?$booking_date->guest_last_name:'';

                                        // if(empty($name))
                                        //     $name = 'Unknown';

                                        // removed following line 
                                        // .$model->bookingItemGroups->bookingGroup->group_name
                                        // echo "<pre>";
                                        // print_r($rate->ratesCapacityPricings);
                                        // exit();
                                        // if($destination->pricing == 1)
                                        // {
                                        $rate_override = RatesOverride::findOne(['date' => $date->night_rate, 'rate_id' => $rate->id,'bookable_item_id' => $rate->unit_id]);
                                        $name ='';
                                        if(isset($rate->ratesCapacityPricings[0]))
                                        {
                                            if(empty($rate_override))
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            }
                                            else
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate_override->single);
                                            }
                                            $name .= ''.$price;
                                        }
                                        if(isset($rate->ratesCapacityPricings[1]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            if(empty($rate_override))
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[1]->price);
                                            }
                                            else
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate_override->double);
                                            }
                                            $name .= ', '.$price;
                                        }
                                        if(isset($rate->ratesCapacityPricings[2]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            if(empty($rate_override))
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[2]->price);
                                            }
                                            else
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate_override->triple);
                                            }
                                            $name .= ', '.$price;
                                        }
                                        if(isset($rate->ratesCapacityPricings[3]))
                                        {
                                            // $rate_item['title'] = 'Single : '.Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[0]->price);
                                            if(empty($rate_override))
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate->ratesCapacityPricings[3]->price);
                                            }
                                            else
                                            {
                                                $price = Yii::$app->formatter->asDecimal($rate_override->quad);
                                            }
                                            $name .= ', '.$price;
                                        }

                                        if($name == '' )
                                        {
                                            $rate_item['title'] = $rate->id.' - '.$name;
                                        }
                                        else
                                        {
                                            $rate_item['title'] = $name;
                                        }
                                        // }
                                        // else
                                        // {
                                        //     $rate_item['title'] = $rate->id.' - '.$name;
                                        // }
                                        // $rate_item['title'] = $rate->ratesCapacityPricings[0]->price;
                                        // $rate_item['title'] = $rate->id.' - '.$name;
                                        $rate_item['resourceId'] = $rate->id;
                                        $rate_item['start'] = $date->night_rate;
                                        $rate_item['end'] = $date->night_rate;
                                        // $rate_item['borderColor'] = '#000000';
                                        if(empty($rate_override))
                                        {
                                            $rate_item['textColor'] = 'black';
                                        }
                                        else
                                        {
                                            // orange if bookable item is closed and red if rate is overridden
                                            if($rate_override->bookable_item_closed == 1)
                                            {
                                                $rate_item['textColor'] = 'orange';
                                                // $rate_item['backgroundColor'] = 'black';
                                            }
                                            else
                                            {
                                                $rate_item['textColor'] = 'red';
                                            }
                                            
                                        }
                                        $rate_item['bookable_item_id'] = $bookable_item->id;
                                        // $rate_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);
                                        $closed_dates = DestinationsOpenHoursExceptions::findOne(['provider_id' => $rate->destination->id, 'state' => 0,'date' => $date->night_rate]);
                                        if(empty($closed_dates))
                                        {
                                            array_push($events, $rate_item);    
                                        }
                                        
                                    }
                                        
                                }
                            }
                            else
                            {
                                // if(strtotime($date->night_rate) >=  strtotime($month_start) && strtotime($date->night_rate) <=  strtotime($month_end))
                                // {
                                    $rate_from = date('Y-m-d',strtotime($rate->from));
                                    $rate_to = date('Y-m-d',strtotime($rate->to));

                                    $date1=date_create($rate_from);
                                    $date2=date_create($rate_to);
                                    $diff=date_diff($date1,$date2);
                                    $difference =  $diff->format("%a");

                                    $date = $rate_from;
                                    $date = date("Y-m-d", strtotime($date));

                                    for ($i=0; $i <= $difference ; $i++) 
                                    { 
                                        $rate_item['id'] = $rate->id;
                                        
                                        $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate->id,'bookable_item_id' => $rate->unit_id]);
                                        $name ='';
                                        
                                        if(empty($rate_override))
                                        {
                                            $price = Yii::$app->formatter->asDecimal($rate->item_price_first_adult).', '. Yii::$app->formatter->asDecimal($rate->item_price_additional_adult).', '.Yii::$app->formatter->asDecimal($rate->item_price_first_child).', '.Yii::$app->formatter->asDecimal($rate->item_price_additional_child);
                                        }
                                        else
                                        {
                                            $price = Yii::$app->formatter->asDecimal($rate_override->item_price_first_adult).', '. Yii::$app->formatter->asDecimal($rate_override->item_price_additional_adult).', '.Yii::$app->formatter->asDecimal($rate_override->item_price_first_child).', '.Yii::$app->formatter->asDecimal($rate_override->item_price_additional_child);
                                        }
                                        $name .= ''.$price;
                                        
        
                                        if($name == '' )
                                        {
                                            $rate_item['title'] = $rate->id.' - '.$name;
                                        }
                        
                                        // }
                                        else
                                        {
                                            $rate_item['title'] = $name;
                                        }
                                        // $rate_item['title'] = $rate->ratesCapacityPricings[0]->price;
                                        // $rate_item['title'] = $rate->id.' - '.$name;
                                        $rate_item['resourceId'] = $rate->id;
                                        $rate_item['start'] = $date;
                                        $rate_item['end'] = $date;
                                        // $rate_item['borderColor'] = '#000000';
                                        if(empty($rate_override))
                                        {
                                            $rate_item['textColor'] = 'black';
                                        }
                                        else
                                        {
                                            if($rate_override->bookable_item_closed == 1)
                                            {
                                                $rate_item['textColor'] = 'orange';
                                                // $rate_item['backgroundColor'] = 'red';
                                            }
                                            else
                                            {
                                                $rate_item['textColor'] = 'red';
                                            }
                                        }
                                        $rate_item['bookable_item_id'] = $bookable_item->id;
                                        // $rate_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);
                                        
                                        array_push($events, $rate_item);

                                        $date = strtotime("+1 day", strtotime($date));
                                        $date = date("Y-m-d", $date);
                                    }
                                // }
                                       
                            }
                        }
                            
                       
                    }
                }
            }
            
        }
        // echo "<pre>";
        // print_r($events);
        // exit();

        // generating resources //

        $resources = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                $rates_count = count($bookable_item->rates);
                $counter = 1;
                foreach ($bookable_item->rates as $rate) 
                {   
                    if($rate->published == 1)
                    {

                        if( (strtotime($month_start) <  strtotime($rate->from)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) <  strtotime($rate->to)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) >  strtotime($rate->to)  &&  strtotime($month_end) < strtotime($rate->from) )
                            )
                        {

                        
                            $item_array = array();
                            $item_array['id'] = $rate->id;
                            $item_array['item'] = $bookable_item->itemType->name;
                            $item_array['title'] = $rate->name;
                            // $item_array['eventColor'] = $bookable_item->background_color;
                            $item_array['bookable_item_id'] = $bookable_item->id;
                            $item_array['background_color'] = $bookable_item->background_color;
                            $item_array['text_color'] = $bookable_item->text_color;

                            array_push($resources, $item_array);
                            // if($counter == $item_names_count)
                            // {
                            //     $item_array = array();
                            //     $item_array['id'] = $bookable_item->id.$destination->id;
                            //     $item_array['item'] = $bookable_item->itemType->name;
                            //     $item_array['title'] = "No Unit Assigned";
                            //     $item_array['eventColor'] = $bookable_item->background_color;
                            //     $item_array['bookable_item_id'] = $bookable_item->id;
                            //     $item_array['background_color'] = $bookable_item->background_color;
                            //     $item_array['text_color'] = $bookable_item->text_color;

                            //     array_push($resources, $item_array);
                            // }
                            $counter++;
                        }
                    }
                }
            }
        }
        // echo "<pre>";
        // print_r($events);
        // exit();

        echo json_encode([
                'events' => $events,
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }


    // generating schedular resources //
    public function actionGenerateSchedularResources()
    {
        // $month_start = date('Y-m-01',strtotime(date('Y-m-d')));
        // $month_end =  date('Y-m-t',strtotime(date('Y-m-d')));
        $destination = Destinations::findOne(['id' => $_POST['destination_id'] ]);
        // $month_start = date('Y-m-d',strtotime($_POST['date']));
        // $month_start = date('Y-m-01',strtotime($_POST['date']));
        // $month_end =  date('Y-m-t',strtotime($_POST['date']));

        $month_start = date('Y-m-d',strtotime($_POST['start']));
        $month_end =  date('Y-m-d',strtotime($_POST['end']));

        // echo "<pre>";
        // print_r($search_form);
        // print_r($search_to);
        // exit();
        $resources = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                $rates_count = count($bookable_item->rates);
                $counter = 1;
                foreach ($bookable_item->rates as $rate) 
                {   
                    if($rate->published == 1)
                    {

                        if( (strtotime($month_start) <  strtotime($rate->from)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) <  strtotime($rate->to)  &&  strtotime($month_end) > strtotime($rate->from) )
                            ||  (strtotime($month_start) >  strtotime($rate->to)  &&  strtotime($month_end) < strtotime($rate->from) )
                            )
                        {

                        
                            $item_array = array();
                            $item_array['id'] = $rate->id;
                            $item_array['item'] = $bookable_item->itemType->name;
                            $item_array['title'] = $rate->name;
                            // $item_array['eventColor'] = $bookable_item->background_color;
                            $item_array['bookable_item_id'] = $bookable_item->id;
                            $item_array['background_color'] = $bookable_item->background_color;
                            $item_array['text_color'] = $bookable_item->text_color;

                            array_push($resources, $item_array);
                            // if($counter == $item_names_count)
                            // {
                            //     $item_array = array();
                            //     $item_array['id'] = $bookable_item->id.$destination->id;
                            //     $item_array['item'] = $bookable_item->itemType->name;
                            //     $item_array['title'] = "No Unit Assigned";
                            //     $item_array['eventColor'] = $bookable_item->background_color;
                            //     $item_array['bookable_item_id'] = $bookable_item->id;
                            //     $item_array['background_color'] = $bookable_item->background_color;
                            //     $item_array['text_color'] = $bookable_item->text_color;

                            //     array_push($resources, $item_array);
                            // }
                            $counter++;
                        }
                    }
                }
            }
            // return $resources;
        }

        echo json_encode([
                'resources' => $resources,
                // 'resources'     => $resources,
                // 'closed_dates' => $closed_dates_arr,
                // 'notifications' => json_encode($notifications_array)
            ]);
    }
}

<?php

use yii\db\Migration;

class m170802_080657_create_table_bookings_date_finances_cart extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `booking_date_finances_cart` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `date` date DEFAULT NULL,
                          `booking_item_id` int(11) unsigned DEFAULT NULL,
                          `booking_group_id` int(11) unsigned DEFAULT NULL,
                          `type` int(11) DEFAULT NULL,
                          `price_description` longtext COLLATE utf8_unicode_ci,
                          `amount` double DEFAULT NULL,
                          `vat` double DEFAULT NULL,
                          `tax` double DEFAULT NULL,
                          `quantity` int(11) DEFAULT NULL,
                          `show_receipt` int(11) DEFAULT NULL,
                          `added_by` int(11) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          CONSTRAINT `booking_date_finances_cart_ibfk_2` FOREIGN KEY (`booking_item_id`) REFERENCES `bookings_items_cart` (`id`) ON DELETE CASCADE,
                          CONSTRAINT `booking_date_finances_cart_ibfk_3` FOREIGN KEY (`booking_group_id`) REFERENCES `booking_group_cart` (`id`) ON DELETE CASCADE
                        ) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
                        ');
    }

    public function down()
    {
        echo "m170802_080657_create_table_bookings_date_finances_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

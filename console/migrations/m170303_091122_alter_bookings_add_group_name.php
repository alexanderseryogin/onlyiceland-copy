<?php

use yii\db\Migration;

class m170303_091122_alter_bookings_add_group_name extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` ADD COLUMN `group_name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `id`; 
        ");
    }

    public function down()
    {
        echo "m170303_091122_alter_bookings_add_group_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

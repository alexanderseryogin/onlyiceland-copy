<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attraction_tags".
 *
 * @property integer $id
 * @property string $name
 *
 * @property AttractionAndTags[] $attractionAndTags
 */
class AttractionTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attraction_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttractionAndTags()
    {
        return $this->hasMany(AttractionAndTags::className(), ['tag_id' => 'id']);
    }
}

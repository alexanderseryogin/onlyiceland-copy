<?php


namespace common\models;

use yii\db\ActiveRecord;

class CurrencyRate extends ActiveRecord
{
    const USD = 'USD.ISK.OVMI.S.D';
    const EUR = 'EUR.ISK.OVMI.S.D';
    const GBP = 'GBP.ISK.OVMI.S.D';
    const CAD = 'CAD.ISK.OVMI.S.D';
    const DKK = 'DKK.ISK.OVMI.S.D';
    const NOK = 'NOK.ISK.OVMI.S.D';
    const SEK = 'SEK.ISK.OVMI.S.D';
    const CHF = 'CHF.ISK.OVMI.S.D';
    const JPY = 'JPY.ISK.OVMI.S.D';

    public function rules()
    {
        return [
            [['date', 'code'], 'string'],
            [['date', 'code', 'rate'], 'required'],
            ['rate', 'double']
        ];
    }

    public function setValues($data)
    {
        $this->date = date('U');
        $this->code = $data['name'];
        $this->rate = $data['value'];
        return $this;
    }

    public static function findByName($name)
    {
        return self::find()->where(['code' => $name])->limit(1)->one();
    }
}
<?php

namespace backend\controllers;

use Yii;
use common\models\HousekeepingStatus;
use common\models\HousekeepingStatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\BaseController;
use common\models\UploadFile;
use yii\web\UploadedFile;

/**
 * HousekeepingStatusController implements the CRUD actions for HousekeepingStatus model.
 */
class HousekeepingStatusController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HousekeepingStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HousekeepingStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HousekeepingStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HousekeepingStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HousekeepingStatus();

        if($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->save();
            
            if(empty($model->getErrors()))
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'temp_icon')) 
                {
                    if($uploadFile->uploadFile('housekeeping-status',$model->id,'icons'))
                    {
                        $model->icon = $uploadFile->FileName;
                        $model->update();
                    }
                }
            }
            
            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['index']);
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Updates an existing HousekeepingStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->save();

            if(empty($model->getErrors()))
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'temp_icon')) 
                {
                    if($uploadFile->uploadFile('housekeeping-status',$model->id,'icons'))
                    {
                        if(!empty($model->icon))
                        {
                            $prev_image = Yii::getAlias('@app').'/../uploads/housekeeping-status/'.$model->id.'/icons/'.$model->icon;
                            unlink($prev_image);
                        }

                        $model->icon = $uploadFile->FileName;
                        $model->update();
                    }
                }
            }
            return $this->redirect(['index']);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HousekeepingStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HousekeepingStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HousekeepingStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HousekeepingStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "destination_add_ons".
 *
 * @property integer $id
 * @property integer $destination_id
 * @property integer $add_on_id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property integer $discountable
 * @property integer $commissionable
 * @property double $price
 * @property double $vat
 * @property double $tax
 * @property double $fee
 *
 * @property Destinations $destination
 * @property AddOn $addOn
 */
class DestinationAddOns extends \yii\db\ActiveRecord
{
    const TYPE_FIXED = 0;
    const TYPE_PERCENTAGE = 1;

    const DISCOUNTABLE = 1;
    const NOT_DISCOUNTABLE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'destination_add_ons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'destination_id', 'add_on_id', 'type', 'discountable', 'commissionable'], 'integer'],
            [['price', 'vat', 'tax', 'fee'], 'number'],
            [['name', 'description'], 'string', 'max' => 256],
            [['destination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['destination_id' => 'id']],
            [['add_on_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddOn::className(), 'targetAttribute' => ['add_on_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'destination_id' => 'Destination ID',
            'add_on_id' => 'Add On ID',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
            'discountable' => 'Discountable',
            'price' => 'Price',
            'vat' => 'Vat',
            'tax' => 'Tax',
            'fee' => 'Fee',
            'commissionable' => 'Commissionable'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Providers::className(), ['id' => 'destination_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddOn()
    {
        return $this->hasOne(AddOn::className(), ['id' => 'add_on_id']);
    }
}

<?php
namespace backend\controllers;

use Yii;
use backend\components\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\InitialResetPasswordForm;
use common\models\User;
use common\models\UserProfile;
use common\models\Countries;
use common\models\Destinations;
use common\models\BookingsItems;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'migrate', 'first-login-reset', 'change-password','first-row-data','second-row-data','third-row-data','fourth-row-data'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    //     return [
    //         'error' => [
    //             'class' => 'yii\web\ErrorAction',
    //         ],
    //     ];
    // }

    public function actionIndex()
    {
        $this->view->params['title'] = 'Dashboard';
        $this->view->params['subTitle'] = '  Welcome!';

        // get analytics for widgets
        $reservations = BookingsItems::find()->where(['temp_flag' => 0,'deleted_at' => 0])->count();
        $reservations24hours = BookingsItems::find()
                                            ->where(['temp_flag' => 0,'deleted_at' => 0])
                                            ->andWhere(['>=','created_at', strtotime(date('Y-m-d 00:00:00'))])
                                            ->andWhere(['<=','created_at', strtotime(date('Y-m-d 24:00:00'))])
                                            ->count();
        $users = User::find()->count();
        $providers = Destinations::find()->count();

        return $this->render('index',[
            'reservations' => $reservations,
            'reservations24hours' => $reservations24hours,
            'users' => $users,
            'providers' => $providers,
        ]);
    }

    public function actionLogin()
    {
        $this->layout = 'login';

        if (!Yii::$app->user->isGuest) {
            //logout a user if currently one is active
            // if(isset($_GET['ulg'])){
            //     Yii::$app->user->logout();
            // }
            Yii::$app->user->logout();
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

        /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'login';

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'login';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    /**
     * Reset login on first login
     * @return 
     */
    public function actionFirstLoginReset (){
        $this->layout = 'login';

        try {
            $model = new InitialResetPasswordForm(Yii::$app->user->identity->id);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);



    }

    /**
     * migrate DB
     * @return view 
     */
    public function actionMigrate()
    {
        //default console commands outputs to STDOUT so this needs to be declared for wep app
        if (!defined('STDOUT')) {
            define('STDOUT', fopen('stdout', 'w'));
        }

        //migration command begin
        $migration = new \yii\console\controllers\MigrateController('migrate', Yii::$app);
        $migration->runAction('up', ['migrationPath' => '@console/migrations/', 'interactive' => false]);
        //migration command end

        /**
         * open the STDOUT output file for reading
         * @var $message collects the resulting messages of the migrate command to be displayed in a view
         */
        $handle = fopen('stdout', 'r');
        $message = '';
        while (($buffer = fgets($handle, 4096)) !== false) {
            $message.=$buffer . "<br>";
        }
        fclose($handle);

        return $this->render('migrate', ['message' => $message]);
    }

    /**
     * Change password.
     */
    public function actionChangePassword()
    {
        //$this->layout = 'login';

        try {
            $model = new InitialResetPasswordForm(Yii::$app->user->identity->id);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }

    public function actionFirstRowData()
    {
        $current_reservations = BookingsItems::find()->where(['temp_flag' => 0,'deleted_at' => 0])
                                                     ->andWhere(['arrival_date' => date('Y-m-d')])
                                                     ->andWhere(['>','departure_date',date('Y-m-d') ])
                                                     ->count();
        $future_reservations = BookingsItems::find()->where(['temp_flag' => 0,'deleted_at' => 0])
                                                     ->andWhere(['>','arrival_date' ,date('Y-m-d')])
                                                     ->count();
        $completed_reservations = BookingsItems::find()->where(['temp_flag' => 0,'deleted_at' => 0])
                                                     ->andWhere(['<=','departure_date' , date('Y-m-d')])
                                                     ->count();
        $canceled_reservations = BookingsItems::find()->where(['temp_flag' => 0,'deleted_at' => 0])
                                                     ->andWhere(['status_id' => 3])
                                                     ->count();
        $data = array();
        $data['current_reservations'] = $current_reservations;
        $data['future_reservations'] = $future_reservations;
        $data['completed_reservations'] = $completed_reservations;
        $data['canceled_reservations'] = $canceled_reservations;
        
        echo json_encode($data);
    }

    public function actionSecondRowData()
    {
        $time_last24_hrs =  strtotime('-24 hours', time());

        $reservations_created_last_24 = BookingsItems::find()
                                            ->where(['temp_flag' => 0,'deleted_at' => 0])
                                            ->andWhere(['>=','created_at', $time_last24_hrs ])
                                            ->andWhere(['<=','created_at', time() ])
                                            ->count();
        $reservations_canceled_last_24 = BookingsItems::find()
                                            ->where(['temp_flag' => 0,'deleted_at' => 0])
                                            ->andWhere(['status_id' => 3])
                                            ->andWhere(['>=','created_at', $time_last24_hrs ])
                                            ->andWhere(['<=','created_at', time() ])
                                            ->count();
        $deleted_reservatrions = BookingsItems::find()
                                            ->where(['temp_flag' => 0])
                                            ->andWhere(['!=','deleted_at',0])
                                            ->count();
        $deleted_reservatrionsr_last_24 = BookingsItems::find()
                                                        ->where(['temp_flag' => 0])
                                                        ->andWhere(['!=','deleted_at',0])
                                                        ->andWhere(['>=','deleted_at', $time_last24_hrs ])
                                                        ->andWhere(['<=','deleted_at', time() ])
                                                        ->count();
        $data = array();
        $data['reservations_created_last_24'] = $reservations_created_last_24;
        $data['reservations_canceled_last_24'] = $reservations_canceled_last_24;
        $data['deleted_reservatrions'] = $deleted_reservatrions;
        $data['deleted_reservatrionsr_last_24'] = $deleted_reservatrionsr_last_24;
        
        echo json_encode($data);
    }

    public function actionThirdRowData()
    {
        $time_last24_hrs =  strtotime('-24 hours', time());

        $total_users = User::find()->count();

        $total_users_24 = User::find()
                                ->Where(['>=','created_at', $time_last24_hrs ])
                                ->andWhere(['<=','created_at', time() ])
                                ->count();

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT `user_profile`.`country_id`,COUNT(*) AS cnt 
            FROM `user`,`user_profile` 
            WHERE `user`.`id` = `user_profile`.`user_id` AND (`user`.`type` = 2 )
            GROUP BY `country_id`
            ");

        $result = $command->queryAll();

        $max_count_mru = null;
        $country_id = null;
        $country_mru = null;

        if(!empty($result))
        {
            foreach ($result as $value) 
        {
            if($value['cnt'] > $max_count_mru )
            {
                $max_count_mru = $value['cnt'];
                $country_id = $value['country_id'];
            }
        }

            $country = Countries::findOne(['id' => $country_id]);
            $country_mru = $country->name;

        }
        
        // echo "<pre>";
        // print_r($country);
        // exit();

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT `user_profile`.`country_id`,COUNT(*) AS cnt 
            FROM `user`,`user_profile` 
            WHERE `user`.`id` = `user_profile`.`user_id` AND (`user`.`type` = 2 ) AND (`user`.`created_at`>= ".$time_last24_hrs." AND `user`.`created_at` <= ".time().")
            GROUP BY `country_id`
            ");

        $result = $command->queryAll();

        $max_count_mru_24 = null;
        $country_id = null;
        $country_mru_24 =null;

        if(!empty($result))
        {
            foreach ($result as $value) 
            {
                if($value['cnt'] > $max_count_mru_24 )
                {
                    $max_count_mru_24 = $value['cnt'];
                    $country_id = $value['country_id'];
                }
            }

            $country = Countries::findOne(['id' => $country_id]);
            $country_mru_24 = $country->name;
        }
            

        
        $data = array();
        $data['total_users'] = $total_users;
        $data['total_users_24'] = $total_users_24;
        $data['country_mru'] = $country_mru;
        $data['max_count_mru'] = $max_count_mru;
        $data['country_mru_24'] = $country_mru_24;
        $data['max_count_mru_24'] = $max_count_mru_24;
        
        echo json_encode($data);
    }

    public function actionFourthRowData()
    {
        $total_destinations = Destinations::find()->where(['active' => 1])
                                                  ->count();
        $total_accomodations = Destinations::find()->where(['active' => 1])
                                                   ->andWhere(['provider_type_id' => 11])
                                                  ->count();
        $total_resturants = Destinations::find()->where(['active' => 1])
                                                   ->andWhere(['provider_type_id' => 6])
                                                  ->count();
        $total_sights = Destinations::find()->where(['active' => 1])
                                                   ->andWhere(['provider_type_id' => 8])
                                                  ->count();
        $data = array();
        $data['total_destinations'] = $total_destinations;
        $data['total_accomodations'] = $total_accomodations;
        $data['total_resturants'] = $total_resturants;
        $data['total_sights'] = $total_sights;
        
        echo json_encode($data);
    }
    /**
     * error
     */
    public function actionError()
    {
        if(Yii::$app->user->isGuest)
        {
            $this->layout = 'error';
        }
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

}

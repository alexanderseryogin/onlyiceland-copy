<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\UserProfile;
use yii\helpers\ArrayHelper;
Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
use frontend\components\Helpers;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Profile');
$this->params['breadcrumbs'][] = $this->title;

\realspaces\assets\UserProfileAsset::register($this);
?>

  <!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <div class="single-agent">
                          <!--<div class="counts pull-right"><strong>18</strong><span>Properties</span></div>-->
                          <h2 class="page-title">
                                <?php
                                if(isset($model->title))
                                    echo Helpers::ProfileTitleTypes($model->title)." ";

                                if(isset($model->first_name))
                                    echo $model->first_name." ";

                                if(isset($model->last_name))
                                    echo $model->last_name." ";
                                ?>
                          </h2>
                          <div class="col-md-4 col-sm-4">
                              <div class="row">
                                      <img src="<?php if(empty(Yii::$app->user->identity->profile->profile_picture)){echo Yii::getAlias('@web').'/frontend/assets/img/dummy_profile.jpg';}else{echo Yii::getAlias('@web').'/uploads/user-profile/'.Yii::$app->user->identity->profile->profile_picture;} ?>" class="img-thumbnail" alt=""/>
                              </div>
                              <div class="row">
                                  <div class="agent-contact-details">
                                          <br>
                                          <ul class="list-group">
                                          <li class="list-group-item">
                                            <h4>Company</h4>
                                            <span>
                                                <?php 
                                                if(isset($model->company_name))
                                                    echo $model->company_name;
                                                ?>
                                            </span>
                                            <h4>Address</h4>
                                            <span> 
                                                <?php
                                                if(isset($model->address))
                                                    echo $model->address.", ";

                                                if(isset($model->postal_code))
                                                    echo $model->postal_code.", ";

                                                if(isset($model->city_id))
                                                    echo $model->city->name.", ";

                                                if(isset($model->state_id))
                                                    echo $model->state->name.", ";

                                                if(isset($model->country_id))
                                                    echo $model->country->name;

                                                        // echo isset($model->address) ? $model->address.", " : "";
                                                        // echo isset($model->postal_code) ? $model->postal_code.", " : "";
                                                        // echo isset($model->city_id) ? $model->city->name.", " : "";
                                                        // echo isset($model->state_id) ? $model->state->name.", " : "";
                                                        // echo isset($model->country_id) ? $model->country->name : "";
                                                ?> 
                                            </span>
                                          </li>
                                          <li class="list-group-item">
                                            <i class="fa fa-phone"></i> Phone
                                            <span class="badge">
                                                <?php 
                                                if(isset($model->telephone_number))
                                                    echo $model->telephone_number;
                                                ?>
                                            </span>
                                          </li>
                                          <!--
                                          <li class="list-group-item">
                                              <div class="social-icons">
                                                  <a href="#"><i class="fa fa-facebook"></i></a>
                                                  <a href="#"><i class="fa fa-twitter"></i></a>
                                                  <a href="#"><i class="fa fa-google-plus"></i></a>
                                                  <a href="#"><i class="fa fa-envelope"></i></a>
                                             </div>
                                          </li>
                                          -->
                                          </ul>
                                      </div>
                               </div>
                          </div>
                          <div class="col-md-8 col-sm-8" style="padding-left: 40px;">
                               <div class="row">
                                  <div class="agent-contact-form">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4"><h4>Update Profile</h4></div>
                                        <div class="tabs col-md-8 col-sm-8">
                                            <ul class="nav nav-tabs pull-right">
                                              <li class="active"> 
                                                <a data-toggle="tab" href="#profile-info"> Profile Info </a>
                                              </li>
                                              <li>
                                                <a data-toggle="tab" href="#update-profile-picture"> Change Picture </a>
                                              </li>
                                              <li>
                                                <a data-toggle="tab" href="#update-password"> Change Password </a>
                                              </li>
                                            </ul>
                                        </div>
                                    </div>
                                        
                                            <div class="tab-content">
                                              <div id="profile-info" class="tab-pane active">
                                                <?php $form = ActiveForm::begin(); ?>

                                                <?= $form->field($model, 'title')->dropDownList([
                                                    UserProfile::TITLE_MR => Helpers::ProfileTitleTypes(UserProfile::TITLE_MR),
                                                    UserProfile::TITLE_MS => Helpers::ProfileTitleTypes(UserProfile::TITLE_MS),
                                                    UserProfile::TITLE_MRS => Helpers::ProfileTitleTypes(UserProfile::TITLE_MRS),
                                                    UserProfile::TITLE_DR => Helpers::ProfileTitleTypes(UserProfile::TITLE_DR),
                                                ]); ?>

                                                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                                                <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

                                                <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

                                                <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

                                                <?php /*echo $form->field($model, 'city_id')->textInput() ?>

                                                <?php echo $form->field($model, 'state_id')->textInput() ?>

                                                <?php echo $form->field($model, 'country_id')->textInput()*/ ?>
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <?= $form->field($model, 'country_id', [
                                                        'inputOptions' => [
                                                            'id' => 'countries-dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                                        ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                                                            'prompt'=>'Select a Country',
                                                            'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('user-profile/liststates?country_id=').'"+$(this).val(), function( data ) {
                                                                  $("#states-dropdown").html( data );
                                                                });'
                                                        ]) ?>      
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <?= $form->field($model, 'state_id', [
                                                        'inputOptions' => [
                                                            'id' => 'states-dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                                        ])->dropdownList(ArrayHelper::map(States::find()->where(['id'=>$model->state_id])->all(),'id','name'),[
                                                            'prompt'=>'Select a State',
                                                            'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('user-profile/listcities?state_id=').'"+$(this).val(), function( data ) {
                                                                  $("#cities-dropdown").html( data );
                                                                });'
                                                        ]) ?>  
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <?= $form->field($model, 'city_id', [
                                                        'inputOptions' => [
                                                            'id' => 'cities-dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                                        ])->dropdownList(ArrayHelper::map(Cities::find()->where(['id'=>$model->city_id])->all(),'id','name'),[
                                                            'prompt'=>'Select a City',
                                                            
                                                        ]) ?> 
                                                    </div>
                                                </div>


                                                <?= $form->field($model, 'telephone_number', [
                                                        'inputOptions' => [
                                                            'id' => 'telephone_number',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 800px'
                                                            ],
                                                            'template' => "{label}<br>{input}"
                                                        ])->textInput()?>

                                                <div class="margiv-top-10">
                                                    <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                                                    <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                </div>

                                                <?php ActiveForm::end(); ?>
                                              </div>
                                              <div id="update-profile-picture" class="tab-pane">

                                              <div class="submit-image" style="width: 150px; height: 150px;">
                                                  <img src="<?php if(empty(Yii::$app->user->identity->profile->profile_picture)){echo Yii::getAlias('@web').'/frontend/assets/img/dummy_profile.jpg';}else{echo Yii::getAlias('@web').'/uploads/user-profile/'.Yii::$app->user->identity->profile->profile_picture;} ?>" alt=""/>
                                              </div>
                                                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                                                <?= $form->field($image_model, 'imageFile')->fileInput() ?>
                                                <button class="btn btn-primary">Submit</button>

                                                <?php ActiveForm::end() ?>
                                              </div>
                                              <div id="update-password" class="tab-pane">
                                                <?php $form = ActiveForm::begin([
                                                    'id' => 'reset-password-form',
                                                    'fieldConfig' => [
                                                        'options' => [
                                                            'tag'=>'span'
                                                        ]
                                                    ]
                                                ]); ?>
                                                <div class="margin-top-10">
                                                <?= $form->field($password_model, 'current_password')->passwordInput([
                                                            //'required' => true,
                                                            'autocomplete' => "off",
                                                            'placeholder' => 'Current Password',
                                                            'id'=>'password-input-current',
                                                            'class' => "form-control"
                                                        ]) ?>
                                                </div>
                                                <div class="margin-top-10">
                                                <?= $form->field($password_model, 'new_password')->passwordInput([
                                                            //'required' => true,
                                                            'autocomplete' => "off",
                                                            'placeholder' => 'New Password',
                                                            'id'=>'password-input-new',
                                                            'class' => "form-control",
                                                            'style' => "margin-bottom: 10px;"
                                                        ]) ?>
                                                </div>
                                                <div class="margin-top-10">
                                                    <?= Html::submitButton('Change Password', ['class' => 'btn btn-primary', 'id' => 'change-password', 'disabled' => true]) ?>
                                                    <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                </div>
                                                <?php ActiveForm::end(); ?>
                                              </div>
                                            </div>
                                  </div>
                               </div>
                          </div>
                    </div>
                    <div class="spacer-20"></div>
                  </div>  
              </div>
          </div>
      </div>
  </div>


<?php
$this->registerJs("
jQuery(document).ready(function() {
    
    //$('#telephone_number').intlTelInput();

    $('#telephone_number').intlTelInput({
      initialCountry: 'auto',
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#countries-dropdown').select2({
        placeholder: \"Select a Country\",
        allowClear: true
    });



});",View::POS_END);
?>

<?php 
//Javascript for password strength checker
$this->registerJs("
var initialized = false;
var input = $('#password-input-new');

input.keydown(function () {
    if (initialized === false) {
        // set base options
        input.pwstrength({
            raisePower: 1.4,
            minChar: 8,
            verdicts: [\"Weak\", \"Normal\", \"Medium\", \"Strong\", \"Very Strong\"],
            scores: [17, 26, 40, 50, 60],
            common: {
                onKeyUp: function (evt, data) {
                    if(data.score >= 50) {
                        $('#change-password').prop('disabled', false);
                    }
                    else{
                        $('#change-password').prop('disabled', true);
                    }
                }
            }
        });

        // add your own rule to calculate the password strength
        input.pwstrength(\"addRule\", \"demoRule\", function (options, word, score) {
            return word.match(/[a-z].[0-9]/) && score;
        }, 10, true);

        // set as initialized 
        initialized = true;
    }
});
", \yii\web\View::POS_END);
?>
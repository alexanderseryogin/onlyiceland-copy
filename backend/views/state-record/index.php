<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\States;
use yii\helpers\ArrayHelper;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $searchModel common\models\StateRecordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'State Record');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="state-record-index">


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'state_id',
                'label' => 'State Name',
                'value' => function($data){
                    return $data->state->name;
                },
                'filter' => Html::dropDownList(
                    'StateRecordSearch[state_id]', 
                    $searchModel['state_id'], 
                    ArrayHelper::map(States::find()->where(['country_id'=>100])->orderBy('name')->all(),'id','name'), 
                    [
                        'prompt' => 'Select State', 
                        'id'=>'states', 
                        'class'=>'form-control',

                    ]),
                'contentOptions' =>['style' => 'width:20%'],
            ],
            [
                'label' => 'Image',
                'format' =>'raw',
                'value' => function($data)
                {
                    if(!empty($data->image))
                    {
                        $path = Yii::getAlias('@web').'/../uploads/states/'.$data->state_id.'/images/'.$data->image;
                                                            
                        return '<a> <img src="'.$path.'" style="max-height:100px" > </a>';
                    }
                },
            ],
            [
                'attribute' => 'description',
                'value' => function($data)
                {
                    if(strlen($data->description) > 430)
                    {
                        $data->description = substr($data->description, 0,430);
                        $data->description = $data->description.'.....';
                    }
                    return $data->description;
                },
            ],

            ['class' => 'yii\grid\ActionColumn','template' => '{update}',],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#states').select2({
        placeholder: \"Select State\",
        allowClear: true
    });
    
});",View::POS_END);
?>

<?php

use yii\db\Migration;

class m180524_140155_alter_table_booking_date_finances_add_discount_disable_column extends Migration
{
    public function up()
    {
        $this->addColumn('booking_date_finances', 'discount_disable', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('booking_date_finances', 'discount_disable');
    }
}

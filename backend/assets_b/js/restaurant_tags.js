$('#dish-dropdown').select2({
    placeholder: "Select Dishes",
    allowClear: true
});
$('#type-dropdown').select2({
    placeholder: "Select Types",
    allowClear: true
});
$('#dietary-dropdown').select2({
    placeholder: "Select Dietary Restrictions",
    allowClear: true
});
$('#meal-dropdown').select2({
    placeholder: "Select Meals",
    allowClear: true
});
$('#price-dropdown').select2({
    placeholder: "Select Prices",
    allowClear: true
});
$('#feature-dropdown').select2({
    placeholder: "Select Features",
    allowClear: true
});
$('#alcohol-dropdown').select2({
    placeholder: "Select Alcohol",
    allowClear: true
});
$('#good_for-dropdown').select2({
    placeholder: "Select Good For",
    allowClear: true
});

$(document).on('ready', function () {
    if($('#provider-type-dropdown').val() != 6) {
        $('.field-resturant_tags-dropdown').hide();
        $('.field-dish-dropdown').hide();
        $('.field-type-dropdown').hide();
        $('.field-dietary-dropdown').hide();
        $('.field-meal-dropdown').hide();
        $('.field-price-dropdown').hide();
        $('.field-feature-dropdown').hide();
        $('.field-alcohol-dropdown').hide();
        $('.field-good_for-dropdown').hide();
    }
});

<?php
namespace backend\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use common\models\User;

/**
 * Password reset form
 */
class InitialResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \common\models\User
     */
    private $_user;


    public function __construct($id = null, $config = [])
    {
        if (!isset($id) || empty($id)) {
            throw new InvalidParamException('User Id cannot be blank.');
        }
        $this->_user = User::findIdentity($id);
        if (!$this->_user) {
            throw new InvalidParamException('Invalid user ID.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //http://stackoverflow.com/questions/8141125/regex-for-password-php
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 8],
            //['password', 'match', 'pattern' => '/^\S*(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\W])(?=\S*[\d])\S*$/', 'message' => 'The password length must be greater than or equal to 8, must contain one or more uppercase characters, must contain one or more lowercase characters, must contain one or more numeric values, must contain one or more special characters.'],
            ['password', 'match', 'pattern' => '/^\S*(?=\S*[a-z])\S*$/', 
                'message' => 'The password must contain one or more lowercase characters'],

            ['password', 'match', 'pattern' => '/^\S*(?=\S*[A-Z])\S*$/', 
                'message' => 'The password length must contain one or more uppercase characters'],

            ['password', 'match', 'pattern' => '/^\S*(?=\S*[\W])\S*$/', 
                'message' => 'The password length must contain one or more special characters'],

            ['password', 'match', 'pattern' => '/^\S*(?=\S*[\d])\S*$/', 
                'message' => 'The password must contain one or more numeric values'],

        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->first_login_reset_flag = 1;

        return $user->save(false);
    }
}

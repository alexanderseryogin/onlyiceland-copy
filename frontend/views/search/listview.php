<?php
use common\models\BookableItemsImages;
use common\models\BookableItems;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>					
<div class="property-listing">
    <ul>
	  <?php
	  		foreach ($models as $destination) 
			{
				$image_path = "//placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER";
				$image_count = 1;

				$query = BookableItems::find()->where(['provider_id' => $destination->id]);

				$items = $query->all();
				$result = ArrayHelper::map($items, 'id', 'id');
				$min_price = $query->min('min_price');

				if(!empty($destination->featured_image))
				{
					$image_path = Yii::getAlias('@web').'/uploads/Destinations/'.$destination->id.'/images/'.$destination->featured_image;
				}

				$arr = explode('_', $min_price);
                $min_price =  $arr[count($arr)-1];
                if(empty($min_price))
                   $min_price = 'Not Set';

                if(strlen($destination->description) > 280)
                {
                	$destination->description = substr($destination->description, 0, 280);
                	$destination->description = $destination->description.'......';
                }

                if(strlen($destination->name) > 110)
                {
                	$destination->name = substr($destination->name, 0, 110);
                	$destination->name = $destination->name.'......';
                }
                ?>
		    	<li class="type-rent col-md-12">
                        <div class="col-md-4"> <a href="#" class="property-featured-image"> <img src='<?=$image_path;?>' alt="" style="height:170px; width:220px;"> <span class="images-count"><i class="fa fa-picture-o"></i>'<?=$image_count;?>'</span> </a>
                        </div>

                        <div class="col-md-8">
                              <div class="property-info">
                              	<div style="height: 55px;">
                              		<h4>
                                        <a href="<?= Url::to(['/search/bookable-items', 'destination_id' => $destination->id]) ;?>">
                                            <?= $destination->name ;?>
                                        </a>
                                    </h4>
                                    <div>
                                        <?php
                                        $counter = 0;
                                        foreach ($banners as $banner) {
                                            ?>
                                            <?php if ($banner->provider_id == $destination->id && $counter < 5) { ?>
                                                <img src="<?= Yii::getAlias('@web') . '/uploads/amenities/' . $banner->amenities_id . '/icons/' . $banner->amenities->icon ?>"
                                                     alt="<?= $banner->amenities->name; ?>"
                                                     data-toggle="tooltip" data-placement="top" title="<?= $banner->amenities->name;?>">
                                                <?php $counter++;
                                            } ?>
                                        <?php } ?>
                                    </div>
                              	</div>
                                
                                <div style="height: 80px;">
                                	<p><?=$destination->description;?></p>
                                </div>
                              </div>

                              <div class="property-amenities clearfix"> 
                              	<span class="area"><strong><?=count($result);?></strong>Quantity</span>
                              	<span class="min_price"><strong><?=$min_price;?></strong>Min Price</span>
                              </div>
                        </div>
                      </li>
			<?php } ?>
    </ul>
</div>
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 */
class Tags extends \yii\db\ActiveRecord
{
    public static $types = ['Descriptive','Suitable','Difficulty','Cuisine','Dish','Type','Dietary Restriction','Meal','Price','Feature','Alcohol','Good For'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type'], 'integer'],
             ['type', 'default', 'value' => 0],
            [['name'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'), 
        ];
    }
}

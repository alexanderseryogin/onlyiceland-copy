<?php

use yii\db\Migration;

class m160910_165411_ADD_TABLE_REGIONS extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `regions`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; ');
    }

    public function down()
    {
        echo "m160910_165411_ADD_TABLE_REGIONS cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

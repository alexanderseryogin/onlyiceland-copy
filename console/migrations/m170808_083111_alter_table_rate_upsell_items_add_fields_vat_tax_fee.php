<?php

use yii\db\Migration;

class m170808_083111_alter_table_rate_upsell_items_add_fields_vat_tax_fee extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `rates_upsell`   
                          ADD COLUMN `vat` DOUBLE NULL AFTER `price`,
                          ADD COLUMN `tax` DOUBLE NULL AFTER `vat`,
                          ADD COLUMN `fee` DOUBLE NULL AFTER `tax`;
                        ');
    }

    public function down()
    {
        echo "m170808_083111_alter_table_rate_upsell_items_add_fields_vat_tax_fee cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

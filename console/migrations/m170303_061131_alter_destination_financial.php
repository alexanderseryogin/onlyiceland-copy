<?php

use yii\db\Migration;

class m170303_061131_alter_destination_financial extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_financial` ADD COLUMN `lodging_tax_per_night` INT(11) NULL AFTER `license_image`; ");
    }

    public function down()
    {
        echo "m170303_061131_alter_destination_financial cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161129_090021_create_table_providers_sight extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_sight` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `provider_id` int(11) DEFAULT NULL,
          `owner_id` int(11) DEFAULT NULL,
          `region_id` int(11) DEFAULT NULL,
          `city_id` int(11) DEFAULT NULL,
          `address` text COLLATE utf8_unicode_ci NOT NULL,
          `description` text COLLATE utf8_unicode_ci NOT NULL,
          `gps_coordinates` text COLLATE utf8_unicode_ci,
          `phone` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
          `website` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
          `price_range` text COLLATE utf8_unicode_ci,
          PRIMARY KEY (`id`),
          KEY `FK_providers_sight_provider_id` (`provider_id`),
          KEY `FK_providers_sight_region_id` (`region_id`),
          KEY `FK_providers_sight_owner_id` (`owner_id`),
          KEY `FK_providers_sight_city_id` (`city_id`),
          CONSTRAINT `FK_providers_sight_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE SET NULL,
          CONSTRAINT `FK_providers_sight_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
          CONSTRAINT `FK_providers_sight_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
          CONSTRAINT `FK_providers_sight_region_id` FOREIGN KEY (`region_id`) REFERENCES `states` (`id`) ON DELETE SET NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
    }

    public function down()
    {
        echo "m161129_090021_create_table_providers_sight cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m180614_071239_update_states_table extends Migration
{
    public function up()
    {
        $this->addColumn('states', 'alias', $this->string(255)->notNull());
        $this->addColumn('states', 'description', $this->string(255));
        $this->addColumn('states', 'button_text', $this->string(255));
        $this->addColumn('states', 'img', $this->string(255));
        $this->addColumn('states', 'on_main', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('states', 'alias');
        $this->dropColumn('states', 'description');
        $this->dropColumn('states', 'button_text');
        $this->dropColumn('states', 'img');
        $this->dropColumn('states', 'on_main');
    }
}

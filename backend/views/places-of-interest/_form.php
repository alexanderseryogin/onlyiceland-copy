<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
Use common\models\Regions;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\PlacesOfInterest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="places-of-interest-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?php echo $form->field($model, 'title_picture')->fileInput(); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/places-of-interest']) ?>" >Cancel</a>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\db\Migration;

class m161201_105756_cascade_tables_with_provider extends Migration
{
    public function up()
    {
        $this->execute("
                ALTER TABLE `bookable_items` DROP FOREIGN KEY `FK_bookable_items_provider_id`; 
                ALTER TABLE `bookable_items` ADD CONSTRAINT `FK_bookable_items_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `discount_code` DROP FOREIGN KEY `FK_discount_code_provider_id`; 
                ALTER TABLE `discount_code` ADD CONSTRAINT `FK_discount_code_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `offers` DROP FOREIGN KEY `FK_offers_item_id`; 
                ALTER TABLE `offers` ADD CONSTRAINT `FK_offers_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON DELETE CASCADE; 
                ALTER TABLE `offers` DROP FOREIGN KEY `FK_offers_provider_id`; 
                ALTER TABLE `offers` ADD CONSTRAINT `FK_offers_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `rates` DROP FOREIGN KEY `FK_rate_provider_id`; 
                ALTER TABLE `rates` ADD CONSTRAINT `FK_rate_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 
                ALTER TABLE `rates` DROP FOREIGN KEY `FK_rate_unit_id`; 
                ALTER TABLE `rates` ADD CONSTRAINT `FK_rate_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `bookable_items`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `rates_discount_code` DROP FOREIGN KEY `FK_rates_discount_id`; 
                ALTER TABLE `rates_discount_code` ADD CONSTRAINT `FK_rates_discount_id` FOREIGN KEY (`discount_code_id`) REFERENCES `discount_code`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `state_image_text` DROP FOREIGN KEY `FK_state_image_text_state_id`; 
                ALTER TABLE `state_image_text` ADD CONSTRAINT `FK_state_image_text_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE CASCADE; 

                ALTER TABLE `upsell_items` DROP FOREIGN KEY `FK_upsell_items_provider_id`; 
                ALTER TABLE `upsell_items` ADD CONSTRAINT `FK_upsell_items_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE; 


            ");
    }

    public function down()
    {
        echo "m161201_105756_cascade_tables_with_provider cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

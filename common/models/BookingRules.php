<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_rules".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property string $request_booking_status
 * @property integer $routine_booking_confirmation
 * @property integer $nearterm_booking_confirmation
 * @property integer $nearterm_booking_advance
 * @property integer $ebp_booking_type
 * @property string $ebp_first_night_exception_period
 * @property string $ebp_last_night_exception_period
 * @property string $same_day_booking_cutoff
 * @property string $general_booking_cancellation
 * @property integer $new_booking_type
 * @property integer $new_booking_status
 *
 * @property BookingConfirmationTypes $ebpBookingType
 * @property BookingConfirmationTypes $neartermBookingConfirmation
 * @property BookingStatuses $newBookingStatus
 * @property BookingTypes $newBookingType
 * @property Providers $provider
 * @property BookingConfirmationTypes $routineBookingConfirmation
 */
class BookingRules extends \yii\db\ActiveRecord
{
    public $booking_cancellation_arr = ['Never','Always','1 day before check-in to 30 days before check in','1 month before check in to 12 months before check in'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'routine_booking_confirmation', 'nearterm_booking_confirmation', 'nearterm_booking_advance', 'new_booking_type', 'new_booking_status',], 'integer'],
            [['request_booking_status', 'general_booking_cancellation','same_day_booking_cutoff'], 'string', 'max' => 256],
            [['nearterm_booking_confirmation'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['nearterm_booking_confirmation' => 'id']],
            [['new_booking_status'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['new_booking_status' => 'id']],
            [['new_booking_type'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['new_booking_type' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['routine_booking_confirmation'], 'exist', 'skipOnError' => true, 'targetClass' => BookingConfirmationTypes::className(), 'targetAttribute' => ['routine_booking_confirmation' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination ID'),
            'request_booking_status' => Yii::t('app', 'Request Booking Status'),
            'routine_booking_confirmation' => Yii::t('app', 'Routine Booking Confirmation'),
            'nearterm_booking_confirmation' => Yii::t('app', 'Near-term Booking Confirmation'),
            'nearterm_booking_advance' => Yii::t('app', 'Nearterm Booking Advance'),
            'same_day_booking_cutoff' => Yii::t('app', 'Same Day Booking Cutoff Time'),
            'general_booking_cancellation' => Yii::t('app', 'General Booking Cancellation'),
            'new_booking_type' => Yii::t('app', 'Default Booking Flag for New Bookings'),
            'new_booking_status' => Yii::t('app', 'Default Booking Status for New Bookings'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNeartermBookingConfirmation()
    {
        return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'nearterm_booking_confirmation']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewBookingStatus()
    {
        return $this->hasOne(BookingStatuses::className(), ['id' => 'new_booking_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewBookingType()
    {
        return $this->hasOne(BookingTypes::className(), ['id' => 'new_booking_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutineBookingConfirmation()
    {
        return $this->hasOne(BookingConfirmationTypes::className(), ['id' => 'routine_booking_confirmation']);
    }
}

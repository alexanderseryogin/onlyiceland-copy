<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookings_upsell_items_cart".
 *
 * @property string $id
 * @property string $booking_date_id
 * @property integer $upsell_id
 * @property integer $price
 * @property integer $extra_bed_quantity
 *
 * @property UpsellItems $upsell
 * @property BookingDatesCart $bookingDate
 */
class BookingsUpsellItemsCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings_upsell_items_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_date_id', 'upsell_id', 'price', 'extra_bed_quantity'], 'integer'],
            [['upsell_id'], 'exist', 'skipOnError' => true, 'targetClass' => UpsellItems::className(), 'targetAttribute' => ['upsell_id' => 'id']],
            [['booking_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingDatesCart::className(), 'targetAttribute' => ['booking_date_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_date_id' => 'Booking Date ID',
            'upsell_id' => 'Upsell ID',
            'price' => 'Price',
            'extra_bed_quantity' => 'Extra Bed Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpsell()
    {
        return $this->hasOne(UpsellItems::className(), ['id' => 'upsell_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingDate()
    {
        return $this->hasOne(BookingDatesCart::className(), ['id' => 'booking_date_id']);
    }
}


<?php
	use common\models\BookableItems;
	
	if(!empty($provider_id))
	{
		$items = BookableItems::find()
                ->where(['provider_id' => $provider_id])
                ->all();

		if(!empty($items))
		{
			echo "<option value=''></option>";
			foreach ($items as $key =>  $item) 
		    {
		        echo "<option value='".$item->id."'>".$item->itemType->name."</option>";  
		    }
		}  
	}
	

?>


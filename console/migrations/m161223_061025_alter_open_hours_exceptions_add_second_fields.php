<?php

use yii\db\Migration;

class m161223_061025_alter_open_hours_exceptions_add_second_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `providers_open_hours_exceptions` CHANGE `date` `date_from` DATE NULL, ADD COLUMN `date_to` DATE NULL AFTER `date_from`, CHANGE `switch` `state` TINYINT(1) NULL, ADD COLUMN `opening_second` TIME NULL AFTER `state`, ADD COLUMN `closing_second` TIME NULL AFTER `opening_second`, ADD COLUMN `split_service` TINYINT(1) NULL AFTER `closing_second`; 
        ");
    }

    public function down()
    {
        echo "m161223_061025_alter_open_hours_exceptions_add_second_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

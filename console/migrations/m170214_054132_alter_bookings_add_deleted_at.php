<?php

use yii\db\Migration;

class m170214_054132_alter_bookings_add_deleted_at extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` ADD COLUMN `deleted_at` INT(11) NULL AFTER `updated_at`; ");
    }

    public function down()
    {
        echo "m170214_054132_alter_bookings_add_deleted_at cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

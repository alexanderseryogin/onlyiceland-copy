<?php

use yii\db\Migration;

class m170330_114046_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` DROP COLUMN `no_of_people`; ");
    }

    public function down()
    {
        echo "m170330_114046_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
    $SelectOption = [
        0 => '',
        1 => 'selected',
    ];
?>
<div class="tab-pane" id="discounts">

    <div class="panel-group accordion" id="accordion_discount_codes">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_discount_codes" href="#collapse_discount_codes"> Discount Codes</a>
                </h4>
            </div>
            <div id="collapse_discount_codes" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">

                    <p class="help-block"><span class="label label-sm label-primary">Info:&nbsp;</span> The discounts listed below only apply to your booking page. These discounts are <strong>not</strong> sent to any booking channels.</p> <br>

                    <div id="discount_codes">
                        <?php if(!empty($model->provider_id) || !$model->isNewRecord): ?>
                        <?=  
                            $this->render('dynamic_discount_codes',
                            [
                                'checked_discount_codes' => $model->discount_code,
                                'provider_id' => $model->provider_id,
                            ]);
                        ?>
                        <?php else: ?>
                        <span class="label label-danger"> Note! </span>
                        <span>&nbsp;  Please First select "Destination" from dropdown to add Upsell Items. </span> 
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_discount_codes" href="#collapse_discount_codes_others"> Multi-Night Discount</a>
                </h4>
            </div>
            <div id="collapse_discount_codes_others" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">

                    <p class="help-block"><span class="label label-sm label-danger">Note:&nbsp;</span> If multiple entries have same number of <strong>Nights</strong> then existing record will update. If an entry has <strong>Nights</strong> value and no values in any other field then <strong>System</strong> will skip/discard that entry; there must be a value in at least one field other than Nights field. </p> <br>

                    <div class="row">
                        <div class="col-sm-2">
                            <label class=" control-label">Nights</label>
                            <select style="width: 100%;" id="multi_night_static" class="dynamic-multi-night-dropdown nights-ddl">
                                <option value="">Select Nights</option>
                                <?php

                                    for ($i=2; $i <= 50 ; $i++) 
                                    {
                                        if(!empty($savedNights) && in_array($i, $savedNights))
                                        {
                                            echo '<option disabled="disabled" value="'.$i.'">'.$i.'</option>';
                                        }
                                        else
                                        {
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Percent</label>
                            <input type="text" class="form-control multi-night-percent static-fields" value="" >
                            <input type="text" value="" class="static-fields" id="percent_static" hidden>
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Per Night</label>
                            <input type="text" class="form-control multi-night-price static-fields" value="">
                            <input type="text" value="" class="static-fields" id="per_night_static" hidden>
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Once Off</label>
                            <input type="text" class="form-control multi-night-price static-fields" value="">
                            <input type="text" value="" class="static-fields" id="once_off_static" hidden>
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Price Cap</label>
                            <input type="text" class="form-control multi-night-price static-fields" value="">
                            <input type="text" value="" class="static-fields" id="price_cap_static" hidden>
                        </div>
                        <div class="col-sm-2" style="margin-top:30px;" >
                            <a class="add-dynamic-field" href="javascript:void(0)" ><i class="fa fa-plus fa-2x" aria-hidden="true"></i></a>
                        </div>

                    </div>
                    <div id="multi_night_dynamic_div">
                        <?php
                            if(isset($multiNightDiscount['nights']) && !empty($multiNightDiscount['nights']))
                            {
                                for ($i=0; $i < count($multiNightDiscount['nights']) ; $i++) 
                                {?>
                                    <div class="row">
                                        <div style="margin-top: 20px"> </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control nights-readonly" name="Rates[multi_night][nights][]" value="<?=$multiNightDiscount['nights'][$i]?>" readonly>
                                        </div>
                                        <div class="col-sm-2">
                                            <input readonly type="text" class="form-control multi-night-percent" value="<?=$multiNightDiscount['percent'][$i]?>" >
                                            <input type="text"  value="<?=$multiNightDiscount['percent'][$i]?>" name="Rates[multi_night][percent][]" hidden>
                                        </div>
                                        <div class="col-sm-2">
                                            <input readonly type="text" class="form-control multi-night-price" value="<?=$multiNightDiscount['per_night'][$i]?>">
                                            <input type="text" value="<?=$multiNightDiscount['per_night'][$i]?>" name="Rates[multi_night][per_night][]" hidden>
                                        </div>
                                        <div class="col-sm-2">
                                            <input readonly type="text" class="form-control multi-night-price" value="<?=$multiNightDiscount['once_off'][$i]?>">
                                            <input type="text" value="<?=$multiNightDiscount['once_off'][$i]?>" name="Rates[multi_night][once_off][]" hidden>
                                        </div>
                                        <div class="col-sm-2">
                                            <input readonly type="text" class="form-control multi-night-price" value="<?=$multiNightDiscount['price_cap'][$i]?>">
                                            <input type="text" value="<?=$multiNightDiscount['price_cap'][$i]?>" name="Rates[multi_night][price_cap][]" hidden>
                                        </div>
                                        <div class="col-sm-2" style="margin-top:07px;" >
                                            <a class="delete-dynamic-field" href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>
                                        </div>
                                    </div>

                                <?php
                                }
                            }
                        ?>
                    </div>

                    <br>

                    <?php
                    if(isset($dataProvider) && !empty($dataProvider))
                    {?>
                        <?= $this->render('multi_night_grid_view',
                            [
                                'dataProvider' => $dataProvider,
                            ])?>   
                    <?php 
                    }
                    ?>

                </div>
            </div>
        </div>

    </div>
    <br>

    <div class="modal fade" id="delete-multi-night-modal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4><i class="fa fa-exclamation-triangle"></i> Confirm!</h4>
                </div>
                <div class="modal-body">
                    <h3> Are you sure you want to delete this item? </h3>
                </div>
                <div class="modal-footer">
                    <button type="button" id="yes_multi_night" class="btn blue invite_user_send">Yes</button>
                    <button type="button" class="btn default" data-dismiss="modal">No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade bs-modal-lg" id="update-multi-night-modal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Multi-Night Discount</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class=" control-label">Percent</label>
                            <input id="ml_percent" type="text" class="form-control multi-night-percent" value="0" >
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Per Night</label>
                            <input id="ml_per_night" type="text" class="form-control multi-night-price" value="0">
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Once Off</label>
                            <input id="ml_once_off" type="text" class="form-control multi-night-price" value="0">
                        </div>
                        <div class="col-sm-2">
                            <label class=" control-label">Price Cap</label>
                            <input id="ml_price_cap" type="text" class="form-control multi-night-price" value="0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="save_changes_multi_night" class="btn green">Save changes</button>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
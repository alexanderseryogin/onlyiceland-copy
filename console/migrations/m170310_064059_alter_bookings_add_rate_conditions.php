<?php

use yii\db\Migration;

class m170310_064059_alter_bookings_add_rate_conditions extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` ADD COLUMN `rate_conditions` TINYINT(1) NULL AFTER `estimated_arrival_time_id`; ");
    }

    public function down()
    {
        echo "m170310_064059_alter_bookings_add_rate_conditions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m171127_131459_alter_destination_add_field_booking_flag_update extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE `booking_policies`   
        ADD COLUMN `booking_flag_updated` TINYINT(1) NULL AFTER `time_group`;");
    }

    public function safeDown()
    {
        echo "m171127_131459_alter_destination_add_field_booking_flag_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171127_131459_alter_destination_add_field_booking_flag_update cannot be reverted.\n";

        return false;
    }
    */
}

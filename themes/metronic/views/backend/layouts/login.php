<?php

/* @var $this \yii\web\View */
/* @var $content string */

use metronic\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="onlyiceland site description" name="description" />
        <meta content="M. Saqib Zafar" name="author" />

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <link rel="shortcut icon" href="favicon.ico" /> 
        <?php $this->head() ?>
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <?php $this->beginBody() ?>
        
        <!-- BEGIN : LOGIN PAGE 5-2 -->
        <?= $content ?>
        <!-- END : LOGIN PAGE 5-2 -->
        
        <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script> 
        <script src="../assets/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->
    
    <?php $this->endBody() ?>
    </body>
    
</html>
<?php $this->endPage() ?>
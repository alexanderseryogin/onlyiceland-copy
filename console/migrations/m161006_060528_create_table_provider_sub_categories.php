<?php

use yii\db\Migration;

class m161006_060528_create_table_provider_sub_categories extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `provider_sub_categories`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161006_060528_create_table_provider_sub_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\PropertyPaymentMethods;
use backend\components\Helpers;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PropertyPaymentMethods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-payment-methods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a class="btn btn-default" href="<?= Url::to(['/property-payment-methods']) ?>" >Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>

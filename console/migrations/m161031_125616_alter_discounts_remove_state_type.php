<?php

use yii\db\Migration;

class m161031_125616_alter_discounts_remove_state_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `discount_code` DROP COLUMN `provider_type_id`, DROP COLUMN `state_id`, DROP INDEX `FK_discount_code_provider_type_id`, DROP INDEX `FK_discount_code_state_id`, DROP FOREIGN KEY `FK_discount_code_provider_type_id`, DROP FOREIGN KEY `FK_discount_code_state_id`; 
        ");
    }

    public function down()
    {
        echo "m161031_125616_alter_discounts_remove_state_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

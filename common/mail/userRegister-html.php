<?php
use yii\helpers\Html;
use common\models\User;

/* @var $this yii\web\View */
/* @var $user common\models\User */

if ($user->type != User::USER_TYPE_ADMIN)
{
	$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['../site/login', 'ulg'=>true]);
}
else
{
	$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login', 'ulg'=>true]);
}

?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->username) ?>,</p>

    <p>Your password is <strong><?= $password ?></strong></p>

    <p>Your account has been created, follow the link below to login:</p>

    <p><?= Html::a(Html::encode($loginLink), $loginLink) ?></p>
</div>

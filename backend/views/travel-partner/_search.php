<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TravelPartnerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="travel-partner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'phone') ?>

    <?= $form->field($model, 'emergency_number') ?>

    <?= $form->field($model, 'booking_email') ?>

    <?php // echo $form->field($model, 'invoice_email') ?>

    <?php // echo $form->field($model, 'commission') ?>

    <?php // echo $form->field($model, 'kennitala') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'second_address') ?>

    <?php // echo $form->field($model, 'country_id') ?>

    <?php // echo $form->field($model, 'state_id') ?>

    <?php // echo $form->field($model, 'city_id') ?>

    <?php // echo $form->field($model, 'postal_code') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

namespace backend\controllers;

use Yii;
use common\models\AttractionTypes;
use common\models\AttractionTypesSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;
use common\models\UploadFile;
use common\models\User;
use common\models\AttractionTypeFields;

/**
 * AttractionTypesController implements the CRUD actions for AttractionTypes model.
 */
class AttractionTypesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'add-custom-field' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAddCustomField()
    {
        $field_model =  new AttractionTypeFields();

        $field_model->type = $_POST['type'];
        $field_model->required = $_POST['required'];
        $field_model->label = $_POST['label'];
        $field_model->at_id = $_POST['at_id'];

        $existing_model = AttractionTypeFields::find()
                        ->where(['at_id' => $field_model->at_id])
                        ->andWhere('label LIKE :query')
                        ->addParams([':query'=> $field_model->label])
                        ->one();

        if(empty($existing_model))
        {
            $field_model->save();
        }
        return;
    }

    /**
     * Lists all AttractionTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttractionTypesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AttractionTypes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AttractionTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttractionTypes();

        if ($model->load(Yii::$app->request->post()) && $model->Validate()) 
        {
            $uploadFile = new UploadFile();

            if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'image')) 
            {
                if($uploadFile->uploadFile('attraction-types','images','logo'))
                {
                    $model->logo = $uploadFile->FileName;
                }
            }

            $model->save();

            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing AttractionTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $searchModel = new AttractionTypesSearch();
        $dataProvider = $searchModel->searchFields($id);

        $model = $this->findModel($id);
        $field_model =  new AttractionTypeFields;

        $previous_logo='';
        $previous_logo_path ='';

        if(!empty($model->logo))
        {
            $previous_logo = $model->logo;
            $previous_logo_path = Yii::getAlias('@app').'/../uploads/attraction-types/images/logo/'.$model->logo;
        }

        if ($model->load(Yii::$app->request->post()) && $model->Validate()) 
        {
            $uploadFile = new UploadFile();

            if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'image')) 
            {
                if($uploadFile->uploadFile('attraction-types','images','logo'))
                {
                    $model->logo = $uploadFile->FileName;
                }

                if(!empty($previous_logo_path))
                {
                    unlink($previous_logo_path);
                }
            }
            else
            {
                $model->logo = $previous_logo;
            }
            
            $model->save();

            return $this->redirect(['index']);
        } 
        else {
            return $this->render('update', [
                'model' => $model,
                'field_model' => $field_model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing AttractionTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(!empty($model->logo))
        {
            $previous_logo = Yii::getAlias('@app').'/../uploads/attraction-types/images/logo/'.$model->logo;
            unlink($previous_logo);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteField()
    {
        $model = AttractionTypeFields::findOne($_POST['data_id']);
        $model->delete();

        return;
    }

    /**
     * Finds the AttractionTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttractionTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttractionTypes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\db\Migration;

class m170320_092418_create_bookable_bed_combinations extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `bookable_beds_combinations`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `item_id` INT(11), `beds_combinations_id` INT(11) UNSIGNED, PRIMARY KEY (`id`), CONSTRAINT `FK_bbc_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE, CONSTRAINT `FK_bbc_bed_combination_id` FOREIGN KEY (`beds_combinations_id`) REFERENCES `beds_combinations`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170320_092418_create_bookable_bed_combinations cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

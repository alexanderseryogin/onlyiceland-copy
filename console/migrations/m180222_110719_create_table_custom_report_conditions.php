<?php

use yii\db\Migration;

class m180222_110719_create_table_custom_report_conditions extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `custom_report_conditions`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `custom_report_id` INT(11),
                          `condition` INT(11),
                          `value` VARCHAR(256),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`custom_report_id`) REFERENCES `custom_reports`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180222_110719_create_table_custom_report_conditions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180222_110719_create_table_custom_report_conditions cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170329_122242_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` DROP COLUMN `booking_id`, ADD COLUMN `provider_id` INT(11) NULL AFTER `id`, ADD COLUMN `estimated_arrival_time_id` INT(11) UNSIGNED NULL AFTER `master`, ADD COLUMN `rate_conditions` TINYINT(1) NULL AFTER `estimated_arrival_time_id`, ADD COLUMN `arrival_date` DATE NULL AFTER `rate_conditions`, ADD COLUMN `departure_date` DATE NULL AFTER `arrival_date`, ADD COLUMN `no_of_nights` INT(11) NULL AFTER `departure_date`, ADD COLUMN `created_at` INT(11) NULL AFTER `offers_id`, ADD COLUMN `updated_at` INT(11) NULL AFTER `created_at`, ADD COLUMN `deleted_at` INT(11) DEFAULT 0 NULL AFTER `updated_at`, DROP INDEX `bookings_items_booking_id`, DROP FOREIGN KEY `bookings_items_booking_id`, ADD CONSTRAINT `bookings_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `bookings_estimated_arrival_time_id` FOREIGN KEY (`estimated_arrival_time_id`) REFERENCES `estimated_arrival_times`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170329_122242_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

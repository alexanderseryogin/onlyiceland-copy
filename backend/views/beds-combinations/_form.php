<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portlet light ">
	<div class="beds-combinations-form">

	    <?php $form = ActiveForm::begin([
	        'errorSummaryCssClass' => 'alert alert-danger',
	        'options' => ['enctype' => 'multipart/form-data'],
	    ]); ?>

	    <div class="row">
	    	<div class="col-sm-6">
	    		<?= $form->field($model, 'combination')->textInput(['maxlength' => true]) ?>
	    	</div>
	    	<div class="col-sm-2">
	    		<?= $form->field($model, 'short_code')->textInput(['maxlength' => true]) ?>
	    	</div>
	    </div>

	    <?php
	        if(!empty($model->icon))
	        {
	            echo '<img src='.Yii::getAlias('@web').'/../uploads/beds-combinations/'.$model->id.'/icons/'.$model->icon.' height="50" width="50">';
	            echo '<br>'; 
	        }
	    ?>

	    <?= $form->field($model, 'temp_icon')->fileInput() ?>

	    <span class="label label-danger"> NOTE! </span>
	    <span>&nbsp; Recommended Dimensions less or equal (50x50) </span><br><br>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	        <a class="btn btn-default" href="<?= Url::to(['/beds-combinations']) ?>" >Cancel</a>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {


});",View::POS_END);
?>


<?php

use yii\db\Migration;

class m170721_075157_alter_table_bookings_item_add_FK_constraint_for_field_housekeeping_status extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`  
                          ADD CONSTRAINT `bookings_items_housekeeping_status_id` FOREIGN KEY (`housekeeping_status_id`) REFERENCES `housekeeping_status`(`id`) ON UPDATE SET NULL ON DELETE CASCADE;
                        ');
    }

    public function down()
    {
        echo "m170721_075157_alter_table_bookings_item_add_FK_constraint_for_field_housekeeping_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles adding order to table `state`.
 */
class m180618_122742_add_order_column_to_state_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('states', 'order', $this->integer()->defaultValue('0'));
        $this->createIndex('idx-order', 'states', 'order');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx-order', 'states');
        $this->dropColumn('states', 'order');
    }
}

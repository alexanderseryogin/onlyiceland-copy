<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BookingsItems;

/**
 * BookingsItemsSearch represents the model behind the search form about `common\models\BookingsItems`.
 */
class BookingsItemsSearch extends BookingsItems
{
    public $group_name;
    public $item_names;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','provider_id','item_id','item_name_id','travel_partner_id'], 'integer'],
            [['group_name','item_names','provider_id',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $session = Yii::$app->session;
        $session->open();

        // echo "<pre>";
        //         print_r(Yii::$app->request->post('start_date'));
        //         print_r($session[Yii::$app->controller->id.'-grid-start-date']);
        //         exit();

        if(isset($session[Yii::$app->controller->id.'-grid-start-date']) && !empty($session[Yii::$app->controller->id.'-grid-start-date']))
        {
            
            $startDate = date('Y-m-d',strtotime($session[Yii::$app->controller->id.'-grid-start-date']));
            $endDate = $session[Yii::$app->controller->id.'-grid-end-date'];

            // echo '<pre>';
            // print_r($startDate);
            // exit();
            
            if($endDate == null || $endDate == '')
            {
                $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
                if(!empty($bookingDates))
                {
                    $endDate = date('Y-m-d',strtotime($bookingDates->date));
                }
                else
                {
                    $endDate = date('Y-m-d');
                }
            }
        }
        else
        {
            // echo "<pre>";
            // print_r("not in if");
            // exit();
            $startDate = date('Y-m-d');
            $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
            if(!empty($bookingDates))
            {
                $endDate = $bookingDates->date;
            }
            else
            {
                $endDate = date('Y-m-d');
            }
            
        }
        // echo "<pre>";
        // print_r($startDate);
        // exit();

        if(array_key_exists('sort', $params))
        {
            // echo "<pre>";
            // print_r("in if");
            // print_r($startDate);
            // print_r($endDate);
            // exit();
            $query = BookingsItems::find()
                                    ->distinct()
                                    ->where(['temp_flag' => 0, 'deleted_at' => 0])
                                    ->andWhere(['between', 'booking_dates.date', $startDate, $endDate])
                                    ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull','itemName']);
        }
        else
        {
            // echo "<pre>";
            // print_r("in else");
            // print_r($startDate);
            // print_r($endDate);
            // exit();
            //changed order  by arrival date  instead of group name
            $query = BookingsItems::find()
                                    ->distinct()
                                    ->where(['temp_flag' => 0, 'deleted_at' => 0])
                                    ->andWhere(['between', 'booking_dates.date', $startDate, $endDate ])
                                    ->orderBy('arrival_date ASC')
                                    ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull','itemName']);
        }

        // echo "<pre>";
        // print_r($query->all());
        // exit();
        // add conditions that should always apply here
        if(isset($params['pageNo']) && !isset($params['page']) )
        {
            $pageNo = $params['pageNo']-1;
            if(isset($params['sort']))
            {

                $sort_column;
                if (strpos($params['sort'], '-') > -1)
                {
                    $sort_column = substr($params['sort'],1);
                }
                else
                {
                    $sort_column = $params['sort'];
                }
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                        'page'   => $pageNo
                    ],
                    'sort' => [
                        'defaultOrder' => [
                            $sort_column => (strpos($params['sort'],'-')>-1)?SORT_DESC:SORT_ASC,
                        ],
                    ]
                ]);
            }
            else
            {
                $dataProvider = new ActiveDataProvider([
                'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                        'page'   => $pageNo
                    ],
                ]);
            }

            
                
        }
        else
        {
            // $pageNo = isset($params['page'])?$params['page']-1:1;
            if(isset($params['sort']))
            {
                // echo "jeere";
                // exit(); 

                $sort_column;
                if (strpos($params['sort'], '-') > -1)
                {
                    $sort_column = substr($params['sort'],1);
                }
                else
                {
                    $sort_column = $params['sort'];
                }
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                        'page'   => isset($params['page'])?$params['page']-1:1
                    ],
                    'sort' => [
                        'defaultOrder' => [
                            $sort_column => (strpos($params['sort'],'-')>-1)?SORT_DESC:SORT_ASC,
                        ],
                    ]
                ]);
            }
            else
            {
                // echo "<pre>";
                // print_r(isset($params['page'])?$params['page']-1:1);
                // exit();
                $dataProvider = new ActiveDataProvider([
                'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                        'page'   => isset($params['page'])?$params['page']-1:0
                    ],
                ]);
            }

            
        }
        
        // echo "<pre>";
        //     // print_r($query->all());
        //     print_r($dataProvider->getModels());
        //     exit();


        $dataProvider->sort->attributes['group_name'] = [
            'asc' => ['booking_group.group_name' => SORT_ASC],
            'desc' => ['booking_group.group_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['item_name_id'] = [
            'asc' => ['bookable_items_names.item_name' => SORT_ASC],
            'desc' => ['bookable_items_names.item_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) 
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bookings_items.id' => $this->id,
            'bookings_items.item_id' => $this->item_id,
            'bookings_items.item_name_id' => $this->item_name_id,
            'provider_id' => $this->provider_id,
            'bookings_items.travel_partner_id' => $this->travel_partner_id,
        ]);

        $query->andFilterWhere(['like', 'booking_group.group_name', $this->group_name]);

        return $dataProvider;
    }

    public function searchDateRangeBookings($start_date,$end_date)
    {
        // if($end_date == null || $end_date == '')
        // {
        //     $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
        //     if(!empty($bookingDates))
        //     {
        //         $end_date = date('Y-m-d',strtotime($bookingDates->date));
        //     }
        //     else
        //     {
        //         $end_date = date('Y-m-d');
        //     }
        // }
        // // echo $start_date.' '.$end_date;
        // // exit();
        $query = BookingsItems::find()
                ->distinct()
                ->where(['temp_flag' => 0, 'deleted_at' => 0, ])
                ->andWhere(['between', 'date', $start_date, $end_date ])
                ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['group_name'] = [
        'asc' => ['booking_group.group_name' => SORT_ASC],
        'desc' => ['booking_group.group_name' => SORT_DESC],
        ];
        
        return $dataProvider;
    }

    public function searchBookings($params,$start_date,$end_date)
    {
        if(isset($params['booking_status']))
        {  
            $query = BookingsItems::find()
                            ->distinct()
                            ->where(['temp_flag' => 0, 'deleted_at' => 0])
                            ->andWhere(['bookings_items.status_id' => $params['booking_status']])
                            ->andWhere(['between', 'date', $start_date, $end_date ])
                            ->orderBy('arrival_date ASC')
                            ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull']);
        }
        else
        {
            $query = BookingsItems::find()->where(['id' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        $dataProvider->sort->attributes['group_name'] = [
        'asc' => ['booking_group.group_name' => SORT_ASC],
        'desc' => ['booking_group.group_name' => SORT_DESC],
        ];

        return $dataProvider;
    }

    public function searchDeleteBookings($params)
    {
        if(array_key_exists('sort', $params))
        {
            $query = BookingsItems::find()->where(['temp_flag' => 0])->andWhere(['>','deleted_at', 0]);
        }
        else
        {
            $query = BookingsItems::find()->where(['temp_flag' => 0])->andWhere(['>','deleted_at', 0])->orderBy('arrival_date ASC');
        }

        // add conditions that should always apply here
        // $page = isset($params['page'])?$params['page']:1;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => isset($params['page'])?($params['page']-1):0,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) 
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'item_id' => $this->item_id,
            'provider_id' => $this->provider_id,
        ]);

        //$query->andFilterWhere(['like', 'bookings.group_name', $this->group_name]);

        return $dataProvider;
    }


    // for housekeeping status

    public function search_hk($params)
    {
        // echo "<pre>";
        // print_r($params);
        // exit();
        $session = Yii::$app->session;
        $session->open();

        if(isset($session[Yii::$app->controller->id.'-grid-start-date']) && !empty($session[Yii::$app->controller->id.'-grid-start-date']))
        {
            
            $startDate = date('Y-m-d',strtotime($session[Yii::$app->controller->id.'-grid-start-date']));
            $endDate = $session[Yii::$app->controller->id.'-grid-end-date'];

            
            if($endDate == null || $endDate == '')
            {
                $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
                if(!empty($bookingDates))
                {
                    $endDate = date('Y-m-d',strtotime($bookingDates->date));
                }
                else
                {
                    $endDate = date('Y-m-d');
                }
            }
        }
        else
        {
            // echo "<pre>";
            // print_r("not in if");
            // exit();
            $startDate = date('Y-m-d');
            $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
            if(!empty($bookingDates))
            {
                $endDate = $bookingDates->date;
            }
            else
            {
                $endDate = date('Y-m-d');
            }
            
        }
        // echo "<pre>";
        // print_r($startDate);
        // exit();

        if(array_key_exists('sort', $params))
        {
            // echo "<pre>";
            // print_r("in if");
            // exit();
            $query = BookingsItems::find()
                                    ->distinct()
                                    ->where(['temp_flag' => 0, 'deleted_at' => 0,])
                                    ->andWhere(['between', 'booking_dates.date', $startDate, $endDate])
                                    ->andWhere(['use_housekeeping' => 1])
                                    ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull','itemName','provider']);
        }
        else
        {
            // echo "<pre>";
            // print_r("in else");
            // print_r($startDate);
            // exit();
            //changed order  by arrival date  instead of group name
            $query = BookingsItems::find()
                                    ->distinct()
                                    ->where(['temp_flag' => 0, 'deleted_at' => 0])
                                    ->andWhere(['between', 'booking_dates.date', $startDate, $endDate ])
                                    ->andWhere(['use_housekeeping' => 1])
                                    ->orderBy('arrival_date ASC')
                                    ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull','itemName','provider']);
        }

        // add conditions that should always apply here
        if(isset($params['pageNo']) && !isset($params['page']))
        {
            $pageNo = $params['pageNo']-1;
             $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
                'page'   => $pageNo
            ],
        ]);
        }
        else
        {
             $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        }
       

        $dataProvider->sort->attributes['group_name'] = [
            'asc' => ['booking_group.group_name' => SORT_ASC],
            'desc' => ['booking_group.group_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['item_name_id'] = [
            'asc' => ['bookable_items_names.item_name' => SORT_ASC],
            'desc' => ['bookable_items_names.item_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) 
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bookings_items.id' => $this->id,
            'bookings_items.item_id' => $this->item_id,
            'bookings_items.item_name_id' => $this->item_name_id,
            'provider_id' => $this->provider_id,
            'bookings_items.travel_partner_id' => $this->travel_partner_id,
        ]);

        $query->andFilterWhere(['like', 'booking_group.group_name', $this->group_name]);

        return $dataProvider;
    }

    public function searchDateRangeBookings_hk($start_date,$end_date)
    {
        // if($end_date == null || $end_date == '')
        // {
        //     $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
        //     if(!empty($bookingDates))
        //     {
        //         $end_date = date('Y-m-d',strtotime($bookingDates->date));
        //     }
        //     else
        //     {
        //         $end_date = date('Y-m-d');
        //     }
        // }
        // // echo $start_date.' '.$end_date;
        // // exit();
        $query = BookingsItems::find()
                ->distinct()
                ->where(['temp_flag' => 0, 'deleted_at' => 0, ])
                ->andWhere(['between', 'date', $start_date, $end_date ])
                ->andWhere(['use_housekeeping' => 1])
                ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull','provider']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['group_name'] = [
        'asc' => ['booking_group.group_name' => SORT_ASC],
        'desc' => ['booking_group.group_name' => SORT_DESC],
        ];
        
        return $dataProvider;
    }

    public function searchBookings_hk($params,$start_date,$end_date)
    {
        if(isset($params['booking_status']))
        {  
            $query = BookingsItems::find()
                            ->distinct()
                            ->where(['temp_flag' => 0, 'deleted_at' => 0])
                            ->andWhere(['bookings_items.status_id' => $params['booking_status']])
                            ->andWhere(['between', 'date', $start_date, $end_date ])
                            ->andWhere(['use_housekeeping' => 1])
                            ->orderBy('arrival_date ASC')
                            ->joinwith(['bookingItemGroups','bookingItemGroups.bookingGroup','bookingDatesSortedAndNotNull','provider']);
        }
        else
        {
            $query = BookingsItems::find()->where(['id' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        $dataProvider->sort->attributes['group_name'] = [
        'asc' => ['booking_group.group_name' => SORT_ASC],
        'desc' => ['booking_group.group_name' => SORT_DESC],
        ];

        return $dataProvider;
    }
}

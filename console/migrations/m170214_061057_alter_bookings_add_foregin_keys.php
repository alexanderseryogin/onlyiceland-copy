<?php

use yii\db\Migration;

class m170214_061057_alter_bookings_add_foregin_keys extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` ADD CONSTRAINT `FK_bookings_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_bookings_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, ADD CONSTRAINT `FK_bookings_confirmation_id` FOREIGN KEY (`confirmation_id`) REFERENCES `booking_confirmation_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170214_061057_alter_bookings_add_foregin_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

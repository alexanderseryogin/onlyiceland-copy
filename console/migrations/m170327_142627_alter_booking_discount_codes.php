<?php

use yii\db\Migration;

class m170327_142627_alter_booking_discount_codes extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_discount_codes` ADD COLUMN `extra_bed_quantity` INT(11) NULL AFTER `bookings_items_id`; 
        ");
    }

    public function down()
    {
        echo "m170327_142627_alter_booking_discount_codes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161027_090507_alter_travel_partner_add_primary_contact extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `travel_partner` ADD COLUMN `primary_contact` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `postal_code`; 
        ");
    }

    public function down()
    {
        echo "m161027_090507_alter_travel_partner_add_primary_contact cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

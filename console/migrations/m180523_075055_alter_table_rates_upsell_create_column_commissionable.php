<?php

use yii\db\Migration;

class m180523_075055_alter_table_rates_upsell_create_column_commissionable extends Migration
{
    public function up()
    {
        $this->addColumn('rates_upsell', 'commissionable', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('rates_upsell', 'commissionable');
    }
}

<?php
	
	use common\models\BookingsItems;
    use common\models\BookingsItemsCart;
    use yii\helpers\Url;

    $this->registerJs("$('.sidebar-total-tooltips').tooltip();");

	$bookingsItemsModel = BookingsItemsCart::find()->where(['created_by' => Yii::$app->user->identity->id])->all();

	if(!empty($bookingsItemsModel))
	{
        $grandTotal = 0;
        ?>

        <a id="group_settings" class="btn btn-sm btn-warning" style="margin-left:170px;" >Group Settings</a><br><br>
        <ul class="feeds list-items">

        <?php
        foreach ($bookingsItemsModel as $key => $bIModel) 
        {
            $bookingDates = $bIModel->bookingDatesSortedAndNotNull;

            if(!empty($bookingDates))
            {
                ?>
                <li>
                    <div class="col1">
                        <div class="cont" style="margin-right: 80px;">
                            <div class="cont-col1">
                                <div class="label label-sm label-success">
                                    <i class="fa fa-check"></i>
                                </div>
                            </div>
                            <div class="cont-col2">
                                <div class="desc"> 
                                    <?php
                                        $item_name = !empty($bIModel->item_name_id)?$bIModel->itemName->item_name:'- - - - -';
                                        echo '- '.$bIModel->provider->name.' <br> - '.$bIModel->item->itemType->name.'<br>- '.$item_name.'<br>'; 
                                    ?>
                                    
                                    <?php
                                    foreach ($bookingDates as $key => $bDateModel) 
                                    {
                                        $grandTotal += $bDateModel->total;
                                        echo date('d/m/Y',strtotime($bDateModel->date)).' : ';

                                        $tooltips_title = '';
                                        if(!empty($bDateModel->price_json))
                                        {
                                            $arr = json_decode($bDateModel->price_json);

                                            $tooltips_title = $tooltips_title . 'Quantity = '. $arr->quantity .' , ';
                                            $tooltips_title = $tooltips_title . 'Total Nights = '. $arr->total_nights .' , ';
                                            $tooltips_title = $tooltips_title . 'Rate = '. $arr->rate .' , ';
                                            $tooltips_title = $tooltips_title . 'VAT % = '. $arr->vat .' , ';
                                            $tooltips_title = $tooltips_title . 'x = '. $arr->x .' , ';
                                            $tooltips_title = $tooltips_title . 'lodgingTax = '. $arr->lodgingTax .' , ';
                                            $tooltips_title = $tooltips_title . 'y = '. $arr->y .' , ';
                                            $tooltips_title = $tooltips_title . 'voucher_discount = '. $arr->voucher_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'travel_partner_discount = '. $arr->travel_partner_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'offer_discount = '. $arr->offer_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'amount_of_discount = '. $arr->amount_of_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'sub_total_1 = '. $arr->sub_total_1 .' , ';
                                            $tooltips_title = $tooltips_title . 'sub_total_2 = '. $arr->sub_total_2 .' , ';
                                            $tooltips_title = $tooltips_title . 'vat_amount = '. $arr->vat_amount .' , ';
                                            $tooltips_title = $tooltips_title . 'Total Price = '. $arr->icelandPrice;

                                            $price_json = str_replace('"', "'", $bDateModel->price_json);
                                        }
                                        else
                                        {
                                            $tooltips_title = 'Item Price';
                                            $price_json = '';
                                        }
                                        ?>
                                        
                                        <label>
                                            <a href="javascript:;" data-placement="bottom" class="sidebar-total-tooltips" style="text-decoration: none; color:white" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                            </a>
                                            <span><?php echo Yii::$app->formatter->asDecimal( $bDateModel->total, "ISK"); ?> </span>
                                        </label>
                                        <br>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col2" style="width: 85px; margin-left: -88px;">
                        <div class="date"> 
                            <a data="<?php echo $bIModel->id ?>" href="javascript:;" class="btn btn-icon-only blue edit-booking-item" style="color:white;">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a data="<?php echo $bIModel->id ?>" href="javascript:;" class="btn btn-icon-only red delete-booking-item" style="color:white;">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <?php
            }   
        }
        ?>
        <li>
            <div class="col1">
                <div class="cont">
                    <div class="cont-col1">
                        <div class="label label-sm label-success">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                    </div>
                    <div class="cont-col2">
                        <div class="desc"> 
                            Grand Total:
                        </div>
                    </div>
                </div>
            </div>
            <div class="col2">
                <div class="date"> 
                    <?php echo Yii::$app->formatter->asDecimal( $grandTotal, "ISK"); ?> 
                </div>
            </div>
        </li>

        <li>
            <div class="form-group" style="margin-left: 50px; ">
                <button type="submit" class="btn btn-success" id="submit_button" >Book Now<i class="fa fa-check" aria-hidden="true"></i></button>
                <a class="btn btn-default" id="cancel_btn" href="javascript:;" >Cancel</a>
            </div>
        </li>

        </ul>
        <?php
	}
    else
    {
        echo '<a id="group_settings" class="btn btn-sm btn-warning" style="margin-left:170px;" >Group Settings</a><br><br>';
        echo '<i>No item added for booking...</i>';
    }
?>
<?php

use yii\db\Migration;

class m170224_132540_alter_bookings_remove_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings` DROP COLUMN `status_id`, DROP COLUMN `flag_id`, DROP COLUMN `cancellation_id`, DROP COLUMN `confirmation_id`, DROP INDEX `FK_bookings_status_id`, DROP INDEX `FK_bookings_flag_id`, DROP INDEX `FK_bookings_confirmation_id`, DROP FOREIGN KEY `FK_bookings_confirmation_id`, DROP FOREIGN KEY `FK_bookings_flag_id`, DROP FOREIGN KEY `FK_bookings_status_id`; 
        ");
    }

    public function down()
    {
        echo "m170224_132540_alter_bookings_remove_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

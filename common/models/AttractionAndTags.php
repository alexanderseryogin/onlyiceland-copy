<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attraction_and_tags".
 *
 * @property integer $id
 * @property integer $attr_id
 * @property integer $tag_id
 *
 * @property Attractions $attr
 * @property AttractionTags $tag
 */
class AttractionAndTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attraction_and_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attr_id', 'tag_id'], 'integer'],
            [['attr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attractions::className(), 'targetAttribute' => ['attr_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttractionTags::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'attr_id' => Yii::t('app', 'Attr ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attractions::className(), ['id' => 'attr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(AttractionTags::className(), ['id' => 'tag_id']);
    }
}

<?php

use yii\db\Migration;

class m161010_060311_insert_sub_category extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `provider_sub_categories` (`name`) 
        VALUES ('No License Required/Engra leyfa krafist'),
               ('Hotel/Hótel'),
               ('Guest House/Gistiheimili'),
               ('Lodge/Gistiskáli'),
               ('Homestay/Heimagisting'),
               ('Apartment/Íbúðir'),
               ('Summer House/Sumarhús'),
               ('Restaurant/Veitingahús'),
               ('Late Night Establishment/Skemmtistaður'),
               ('Café and Catering Services/Veitingastofa og greiðasala'),
               ('Restaurant and Catering Services/Veisluþjónusta og veitingaverslun'),
               ('Coffee House/Kaffihús'),
               ('Bar/Krá'),
               ('Auditorium/Samkomusalir')
        ;");
    }

    public function down()
    {
        echo "m161010_060311_insert_sub_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

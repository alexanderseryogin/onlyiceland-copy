<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
Use common\models\Cities;
Use common\models\States;
Use common\models\Countries;
Use common\models\AttractionTypes;
Use common\models\AttractionTypeFieldsData;
use common\models\DestinationTypes;
use common\models\AttractionTags;

use yii\grid\GridView;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model common\models\Attractions */
/* @var $form yii\widgets\ActiveForm */
\metronic\assets\BackendUserProfileAsset::register($this);
?>
<style>
    .hint-block{
        color:blue;
    }
</style>

<script type="text/javascript">

    // for update 
    var updated_state_id ='';
    var updated_city_id = '';
    var updated_flag = 0;
</script>

<div class="attractions-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Attractions</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#general" data-toggle="tab">General Info</a>
                            </li>
                                      
                            <li>
                                <a href="#photos" data-toggle="tab">Photos</a>
                            </li>

                        </ul>
                    </div>

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                        'options' => ['enctype' => 'multipart/form-data'],
                        'id' => 'attraction_form',
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="general">

                                <input type="text" name="Attractions[server_phone]" id="server_phone" hidden="">

                                <div class="row">
                                    <div class="col-xs-6">
                                    <?php
                                        $url = Yii::$app->urlManager->createUrl(['attractions/list-fields' , 'model_id' => (isset($model->id)?$model->id:'')]);
                                    ?>
                                        <?= $form->field($model, 'attr_type_id', [
                                                'inputOptions' => [
                                                                'id' => 'type_dropdown',
                                                                'class' => 'form-control',
                                                                'style' => 'width: 100%'
                                                                ]
                                        ])->dropdownList(ArrayHelper::map(DestinationTypes::find()->all(),'id','name'),[
                                        'prompt'=>'Select a Type',
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'tags', [
                                                'inputOptions' => [
                                                                'id' => 'tags_dropdown',
                                                                'class' => 'form-control',
                                                                'style' => 'width: 100%',
                                                                'multiple' => 'multiple',
                                                                ]
                                        ])->dropdownList(ArrayHelper::map(AttractionTags::find()->all(),'id','name')) ?>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'phone', [
                                                                            'inputOptions' => [
                                                                                'id' => 'telephone_number',
                                                                                'class' => 'form-control',
                                                                                ],
                                                                                'template' => "{label}<br>{input}"
                                                                            ])->textInput()?>
                                        <div id="phone_hint" style="margin-top: -20px; color:red; display: none;">Phone is invalid.</div>
                                    </div>

                                    <div class="col-xs-3">
                                        <?= $form->field($model, 'price', [
                                                                        'inputOptions' => [
                                                                            'id' => 'price-input',
                                                                            'class' => 'form-control'
                                                                            ]
                                                                        ])->textInput(['maxlength' => true])->hint(' e.g xxx.xxx') ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'address',[
                                                                'inputOptions' => [
                                                                    'class' => 'form-control',
                                                                    'style' => 'resize: none;',
                                                                    ]
                                                                ])->textarea(['rows' => 4]) ?>
                                    </div>

                                    <div class="col-xs-6">
                                        
                                        <?php
                                            if(!$model->isNewRecord)
                                            {
                                                $this->registerJs("
                                                    $('#type_dropdown').val($model->attr_type_id).trigger('change');
                                                    $('#states-dropdown').val($model->state_id).trigger('change');
                                                        updated_city_id = $model->city_id;
                                                        updated_flag = 1; 
                                                    ");
                                            }
                                        ?>

                                        <?= $form->field($model, 'state_id', [
                                            'inputOptions' => [
                                                'id' => 'states-dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(States::find()->where(['country_id' => 100])->orderBy('name ASC')->all(),'id','name'),[
                                                'prompt'=>'Select a State',
                                                'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/listcities?state_id=').'"+$(this).val(), function( data ) 
                                                    {
                                                        $("#cities-dropdown").html( data );

                                                        if(updated_flag == 1)
                                                        {
                                                            $("#cities-dropdown").val(updated_city_id).trigger("change");
                                                        }
                                                        
                                                    });'
                                        ]) ?> 

                                        <?= $form->field($model, 'city_id', [
                                            'inputOptions' => [
                                                'id' => 'cities-dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(Cities::find()->where(['id'=>$model->city_id])->all(),'id','name'),[
                                                    'prompt'=>'Select a City',
                                                                
                                        ]) ?>
                                    </div>
                                </div>
                                
                                <?= $form->field($model, 'description',[
                                                                'inputOptions' => [
                                                                    'class' => 'form-control',
                                                                    'style' => 'resize: none;',
                                                                    ]
                                                                ])->textarea(['rows' => 6]) ?>

                                <?php
                                    
                                    if(!$model->isNewRecord)
                                    {
                                        $fields_data = AttractionTypeFieldsData::find()
                                                ->where(['attraction_id' => $model->id])
                                                ->all();
                                        foreach ($fields_data as $key => $field_data) 
                                        {
                                            $var = strtolower(str_replace(' ','_', $field_data->atf->label));
                                            $this->registerJs("
                                                var id = '$var';
                                                var value = '$field_data->value';
                                                $('#'+id).val(value);  
                                            ");
                                        }
                                    }
                                ?>


                                <?php ActiveForm::end(); ?>
                            </div>

                            <div class="tab-pane" id="photos">
                                <form action="<?= Url::to(['/attractions/upload-images']) ?>" class="dropzone dropzone-file-area" method="POST" enctype="multipart/form-data" id="my-dropzone" style="width: 900px; margin-top: 50px;">
                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                                    <h3 class="sbold">Drop files here or click to upload</h3>
                                </form>
                                <br>

                                <?php 

                                    if(isset($dataProvider) && !empty($dataProvider))
                                    {?>
                                        <?php Pjax::begin(['id' => 'images-gridview','timeout' => 1000000, 'enablePushState' => false]) ?>

                                            <?= GridView::widget([
                                            'dataProvider' => $dataProvider,
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],

                                                [
                                                    'label' => 'Images',
                                                    'format' =>'raw',
                                                    'value' => function($data)
                                                    {
                                                        if(!empty($data))
                                                        {
                                                            $path = Yii::getAlias('@web').'/../uploads/attractions/'.$data->attraction_id.'/images/'.$data->image;

                                                            return '<a name="pop[]"> <img src="'.$path.'" style="max-height:100px" > </a>';
                                                        }
                                                    },
                                                ],

                                                [
                                                    'label' => 'Description',
                                                    'value' => function($data)
                                                    {
                                                        return $data->description;
                                                    },
                                                    'contentOptions' => ['style' => 'max-width:200px; overflow: hidden; text-overflow: ellipsis;']
                                                ],

                                                [
                                                    'label' => 'Order',
                                                    'format' => 'raw',
                                                    'value' => function($data)
                                                    {
                                                        if($data->isFirstRecord())
                                                        {
                                                            return '<div style="display:inline-block">
                                                                    <span class="badge badge-primary">'.$data->display_order.'</span>
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <a name="down[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>';
                                                        }
                                                        else if($data->isLastRecord())
                                                        {
                                                           return '<div style="display:inline-block">
                                                                    <span class="badge badge-primary">'.$data->display_order.'</span>
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <a name="up[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>';
                                                        }
                                                        else
                                                        {
                                                            return '<div style="display:inline-block">
                                                                    <span class="badge badge-primary">'.$data->display_order.'</span>
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <a name="up[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a name="down[]" style="text-decoration: none;" data-id='.$data->id.'>
                                                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>';
                                                        }
                                                    },
                                                    'contentOptions' => ['style' => 'max-width: 30px;']
                                                ],


                                                  ['class' => 'yii\grid\ActionColumn',
                                                  'template' => '{update}{delete}',
                                                    'buttons' => [
                                                        'update' => function ($url, $model, $key) {
                                                               return '<a style="text-decoration: none;" aria-label="update" data-id='.$model->id.' title="update" name="update[]">
                                                                        <span class="glyphicon glyphicon-pencil"></span>
                                                                        </a>';
                                                          },
                                                          'delete' => function ($url, $model, $key) {
                                                               return '<a style="text-decoration: none;" aria-label="delete" data-id='.$model->id.' title="delete" name="delete[]">
                                                                        <span class="glyphicon glyphicon-trash"></span>
                                                                        </a>';
                                                          },

                                                        ]
                                                    ],
                                                ],
                                            ]); ?>

                                        <?php Pjax::end() ?>


                                <?php 
                                }
                                ?>
                            </div>

                            <div class="form-group">
                                <a class="<?= $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?>" id="submit_button" ><?=$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update') ?></a>
                                <a class="btn btn-default" href="<?= Url::to(['/attractions']) ?>" >Cancel</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4><i class="fa fa-exclamation-triangle"></i> Warning!</h4>
            </div>
            <div class="modal-body">
                <h3> Are you sure to delete image? </h3>
            </div>
            <div class="modal-footer">
                <button type="button" id="yes1" class="btn blue invite_user_send">Yes</button>
                <button type="button" id="no1" class="btn default" data-dismiss="modal">No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="update-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4> Update Image Description</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="6" style="resize: none;" id="image-description"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" id="update" class="btn blue invite_user_send">Update</button>
                <button type="button" id="no1" class="btn default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Image preview</h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" style="max-width: 570px; max-height: 600px;" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#submit_button').click(function()
    {
        var countryData = $('#telephone_number').intlTelInput('getSelectedCountryData');

        var tel = '+'+countryData['dialCode']+' '+$('#telephone_number').val();
        $('#server_phone').val(tel);

        if($('#telephone_number').intlTelInput('isValidNumber')==false)
        {
            $('#phone_hint').show();
        }

        if($('#telephone_number').intlTelInput('isValidNumber'))
        {
            $('#attraction_form').submit();
        }
    });

    $('a[name=\"pop[]\"]').on('click', function() 
    {
       $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
       $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
    });

    $('a[name=\"up[]\"]').click(function()
    {
        row_id = $(this).attr('data-id');

        var Object = {
                        row_id : row_id,
                        action : 'up',
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'arrange-data',
            data: Object,
            success: function(result)
            {
                 $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"down[]\"]').click(function()
    {
        row_id = $(this).attr('data-id');

        var Object = {
                        row_id : row_id,
                        action : 'down',
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'arrange-data',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('#images-gridview').on('pjax:end', function() {

        $('a[name=\"pop[]\"]').on('click', function() 
        {
           $('#imagepreview').attr('src', $(this).children().attr('src')); // here asign the image to the modal when the user click the enlarge link
           $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        });
    
        $('a[name=\"up[]\"]').click(function()
        {
            row_id = $(this).attr('data-id');

            var Object = {
                            row_id : row_id,
                            action : 'up',
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'arrange-data',
                data: Object,
                success: function(result)
                {
                     $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        $('a[name=\"down[]\"]').click(function()
        {
            row_id = $(this).attr('data-id');

            var Object = {
                            row_id : row_id,
                            action : 'down',
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'arrange-data',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        var image_id;

        $('a[name=\"update[]\"]').click(function(e) 
        {
            $('#update-modal').modal('show');

            image_id = $(this).attr('data-id');

            var Object = {
                            image_id : image_id,
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'get-description',
                data: Object,
                success: function(result)
                {
                    $('#image-description').val(result); 
                },
            });
        });

        $('#update').click(function(e) 
        {
            $('#update-modal').modal('hide');

            var Object = {
                            image_id : image_id,
                            description: $('#image-description').val(),
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'set-description',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });

        $('a[name=\"delete[]\"]').click(function(e) 
        {
            image_id = $(this).attr('data-id');

            $('#delete-modal').modal('show');
        });

        $('#yes1').click(function()
        {
            $('#delete-modal').modal('hide');

            var Object = {
                            image_id : image_id,
                        }
            $.ajax(
            {
                type: 'POST',
                url: 'delete-image',
                data: Object,
                success: function(result)
                {
                    $.pjax.reload({container:'#images-gridview'});  //Reload GridView
                },
            });
        });
        
    });

    var image_id;

    $('a[name=\"update[]\"]').click(function(e) 
    {
        $('#update-modal').modal('show');

        image_id = $(this).attr('data-id');

        var Object = {
                        image_id : image_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'get-description',
            data: Object,
            success: function(result)
            {
                $('#image-description').val(result); 
            },
        });
    });

    $('#update').click(function(e) 
    {
        $('#update-modal').modal('hide');

        var Object = {
                        image_id : image_id,
                        description: $('#image-description').val(),
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'set-description',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"delete[]\"]').click(function(e) 
    {
        image_id = $(this).attr('data-id');

        $('#delete-modal').modal('show');
    });

    $('#yes1').click(function()
    {
        $('#delete-modal').modal('hide');

        var Object = {
                        image_id : image_id,
                    }
        $.ajax(
        {
            type: 'POST',
            url: 'delete-image',
            data: Object,
            success: function(result)
            {
                $.pjax.reload({container:'#images-gridview'});  //Reload GridView
            },
        });
    });


    Dropzone.options.myAwesomeDropzone = {
      paramName: 'file', // The name that will be used to transfer the file
      maxFilesize: 10, // MB
    };

    $('#price-input').inputmask('999.999',{numericInput:true,rightAlign: true})

    $('#telephone_number').intlTelInput({
      initialCountry: 'auto',
      preferredCountries : ['is'],
      geoIpLookup: function(callback) {
        $.get('//ipinfo.io', function() {}, 'jsonp').always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : \"\";
          callback(countryCode);
        });
      }
    });

    $('#telephone_number').change(function()
    {
      var telInput = $('#telephone_number');
      if ($.trim(telInput.val())) 
      {
        if (telInput.intlTelInput('isValidNumber')) 
        {
            $('#phone_hint').hide();
        }
        else 
        {
          $('#phone_hint').show();
        }
      }
    });

    $('#cities-dropdown').select2({
        placeholder: \"Select a City\",
        allowClear: true
    });

    $('#states-dropdown').select2({
        placeholder: \"Select a State\",
        allowClear: true
    });

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#tags_dropdown').select2({
        placeholder: \"Select Tag\",
        allowClear: true
    });

});",View::POS_END);
?>

    

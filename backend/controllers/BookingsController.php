<?php

namespace backend\controllers;

use function PHPSTORM_META\type;
use Yii;
use common\models\BookingGroup;
use common\models\BookingGroupCart;
use common\models\BookingsSearch;
use common\models\BookingsItemsSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Destinations;
use common\models\BookingRules;
use common\models\Modelhistory;
use backend\components\AuditLog;
use common\models\DiscountCode;
use common\models\SpecialRequests;
use common\models\UpsellItems;
use common\models\BookingStatuses;
use yii\helpers\ArrayHelper;
use common\models\BookingPolicies;
use common\models\EstimatedArrivalTimes;
use common\models\User;
use common\models\UserProfile;
use common\models\BookableItems;
use yii\helpers\Url;
use common\models\BookingDates;
use common\models\BookingDatesCart;
use common\models\BookingsDiscountCodes;
use common\models\BookingsDiscountCodesCart;
use common\models\BookingSpecialRequests;
use common\models\BookingSpecialRequestsCart;
use common\models\BookingsUpsellItems;
use common\models\BookingsUpsellItemsCart;
use common\models\BookingsItems;
use common\models\BookingsItemsCart;
use common\models\Rates;
use common\models\RatesUpsell;
use common\models\RatesCapacityPricing;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\BookableBedTypes;
use common\models\BookingItemGroup;
use common\models\BookingItemGroupCart;
use common\models\BookingGuests;
use common\models\BookingGuestsCart;
use common\models\BookingGroupSpecialRequests;
use common\models\DestinationTravelPartners;
use common\models\DestinationsOpenHoursExceptions;
use common\models\BookingDateFinances;
use common\models\BookingDateFinancesCart;
use common\models\DestinationAddOns;
use common\models\BookingHousekeepingStatus;
use common\models\BookingHousekeepingStatusCart;
use common\models\BookableItemsNames;
use common\models\BookingGroupFinance;
use common\models\BookingGroupFinanceDetails;
use common\models\Notifications;
use kartik\mpdf\Pdf;
use backend\models\importCsvModel;
use yii\web\UploadedFile;
use common\models\UploadFile;
use common\models\BookableItemTypes;
use common\models\BookingTypes;
use common\models\Countries;
use common\models\RatesOverride;
use common\models\BookingsLog;
use DateTime;/**

 * BookingsController implements the CRUD actions for Bookings model.
 */
class BookingsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['POST'],
            //     ],
            // ],
        ];
    }

    public function actionAuditLog()
    {
        return $this->render('log');
        // echo "here";
        // exit();
    }
    public function actionLog($id)
    {
        // $model = $this->findModel($id);
        return $this->render('log');
    }

    /**
     * Lists all Bookings models.
     * @return mixed
     */
    public function actionIndex($filter = null,$pageNo = null)
    {
        $searchModel = new BookingsItemsSearch();
        $booking_status = BookingStatuses::find()->all();
        $booking_flags = BookingTypes::find()->all();

        if($filter == null || $filter == 'false')
        {
            $filter_flag = 0;
        }
        else
        {

            $filter_flag = 1;
        }
        
        $group_flag = 0;
        $booking_ref_flag =0;
        $selected_status = ArrayHelper::map($booking_status,'id','id');

        $session = Yii::$app->session;
        $session->open();

        if(Yii::$app->request->post())
        {
            // echo "<pre>";
            // print_r("in post");
            // exit();
            $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
            if(!empty($bookingDates))
            {
                $last_booking_date = date('Y-m-d',strtotime($bookingDates->date));
            }
            else
            {
                $last_booking_date = date('Y-m-d');
            }

            $start_booking_date = date('Y-m-d',strtotime(Yii::$app->request->post('start_date'))); 

            $params = Yii::$app->request->post();
            $params['Bookings'] = isset($params['Bookings'])?$params['Bookings']:'';

            $start_date = $start_booking_date;
            $end_date = $last_booking_date;

            if($params['Bookings']['daterangepicker'] == 1)
            {   
                // echo "<pre>";
                // print_r(Yii::$app->request->post());
                // exit();

                

                
                $dataProvider = $searchModel->searchDateRangeBookings($start_date,$end_date);
                $session[Yii::$app->controller->id.'-grid-start-date'] = Yii::$app->request->post('start_date');
            }
            else
            {
                // echo "<pre>";
                // print_r(Yii::$app->request->post());
                // exit();
                $filter_flag = 1;
                $dataProvider = $searchModel->searchBookings($params['Bookings'],$start_date,$end_date);
                $selected_status = [];

                $selected_status = !empty($params['Bookings']) && isset($params['Bookings']['booking_status'])?$params['Bookings']['booking_status']:'';

                $group_flag = isset($params['Bookings']['group_switch'])?1:0;

                $booking_ref_flag = isset($params['Bookings']['booking_ref_switch'])?1:0;

                $session[Yii::$app->controller->id.'-grid-booking-status'] = $selected_status;
                $session[Yii::$app->controller->id.'-grid-group-flag'] = $group_flag;
                $session[Yii::$app->controller->id.'-booking_ref-flag'] = $booking_ref_flag;
            }
        }
        else
        {
            // echo "<pre>";
            // print_r(Yii::$app->request->queryParams);
            // exit();
            $params = Yii::$app->request->queryParams;

            // echo "<pre>";
            // print_r($params);
            // exit();
            $session = Yii::$app->session;
            $session->open();

            // echo "<pre>";
            // print_r($params);
            // print_r(json_decode($session[Yii::$app->controller->id.'-grid'], true));
            // print_r(array_merge($params,json_decode($session[Yii::$app->controller->id.'-grid'], true)));
            // exit();
            if(!empty($params))
            {
                // echo "<pre>";
                // print_r("in if");
                // exit();
                
                if(isset($params['BookingsItemsSearch']) && !$this->isArrayEmpty($params['BookingsItemsSearch']) && !isset($params['page']))
                {
                    foreach($params['BookingsItemsSearch'] as $key=>$value)
                    {
                        if(is_null($value) || $value == '')
                            unset($params['BookingsItemsSearch'][$key]);
                    }
                    $params['page'] = 1;
                }
               
                if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']) )
                {   

                    // $session_arr = array();

                    $session_arr = json_decode($session[Yii::$app->controller->id.'-grid'], true);
                    if(isset($session_arr['BookingsItemsSearch']) && !empty($session_arr['BookingsItemsSearch'])  )
                    {
                        foreach($session_arr['BookingsItemsSearch'] as $key=>$value)
                        {
                            if(is_null($value) || $value == '')
                                unset($session_arr['BookingsItemsSearch'][$key]);
                        }
                    }
                        
                    // echo "<pre>";
                    // print_r($params);
                    // print_r($session_arr);
                    // print_r($params +$session_arr);
                    // exit();
                    $params = $params +$session_arr ;
                    
                } 

                // if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
                // {   
                //     $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);
                    
                // }
               
                $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
            }
            else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
            {
                // echo "<pre>";
                // print_r("in else if");
                // print_r(json_decode($session[Yii::$app->controller->id.'-grid'], true));
                // exit();
                $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);    
                // echo "<pre>";
                // print_r($params);
                // exit();       

            }
            else
            {
                // $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
                // echo "<pre>";
                // print_r("in else");
                // print_r($params);
                // exit();
            }
            if($pageNo != null)
            {
                $params['pageNo'] = $pageNo;
            }
            // echo "<pre>";
            // print_r($params);
            // exit();
            $dataProvider = $searchModel->search($params);
            
        }

        // ********** if status and group flag is in session then get them ************ //

        if(isset($session[Yii::$app->controller->id.'-grid-booking-status']) && !empty($session[Yii::$app->controller->id.'-grid-booking-status']))
        {
            $selected_status = $session[Yii::$app->controller->id.'-grid-booking-status'];
        }

        if(isset($session[Yii::$app->controller->id.'-grid-group-flag']))
        {
            $group_flag = $session[Yii::$app->controller->id.'-grid-group-flag'];
        }

        if(isset($session[Yii::$app->controller->id.'-booking_ref-flag']))
        {
            $booking_ref_flag = $session[Yii::$app->controller->id.'-booking_ref-flag'];
        }

        //$dataProvider->pagination->pageSize=10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'booking_status' => $booking_status,
            'filter_flag' => $filter_flag,
            'group_flag' => $group_flag,
            'booking_ref_flag' => $booking_ref_flag,
            'selected_status' => $selected_status,
            'booking_flags' => $booking_flags,
            'sort' => isset($params['sort'])?$params['sort']:null
        ]);
    }

    public function actionUnsetDateRangeSession()
    {
        $session = Yii::$app->session;
        $session->open();
        unset($session[Yii::$app->controller->id.'-grid-start-date']);
        unset($session[Yii::$app->controller->id.'-grid-end-date']);
        
        return $this->redirect(['index']);
    }

    public function actionDeletedBookings()
    {
        $searchModel = new BookingsItemsSearch();

        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            // echo "<pre>";
            // print_r($params);
            // exit();
            // $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
            if(isset($params['BookingsItemsSearch']) && !$this->isArrayEmpty($params['BookingsItemsSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->searchDeleteBookings($params);

        return $this->render('deleted_bookings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bookings model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bookings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */

    public function actionCreate()
    {
        $errors = [];
        $error_flag = 0;

        $model = new BookingsItemsCart();
        $bookingGroupModel = new BookingGroupCart();

        $query = new \yii\db\Query;
        $query->from('booking_group');
        $query->select('max(id) as max');
        $command = $query->createCommand();
        $row = $command->queryOne();
        $group_no = $row['max'] +1; 
        $group_name = "Group-".$group_no;

        if($bookingGroupModel->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) 
        {
            // echo "<pre>";
            // print_r($_POST);
            // exit();
            $bookingGroupModel->group_name = !empty($bookingGroupModel->group_name)?$bookingGroupModel->group_name:NULL;
            $bookingsItemsQuery = BookingsItemsCart::find()->where(['created_by' => Yii::$app->user->identity->id]);
            $bookingsItemsModel = $bookingsItemsQuery->all();
            
            if(!empty($bookingsItemsModel))
            {
                foreach ($bookingsItemsModel as $key => $bImodel) 
                {
                    $item_names = '';

                    $date1=date_create($bImodel->departure_date);
                    $date2=date_create($bImodel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a");

                    $bookingDates = $bImodel->bookingDatesSortedAndNotNull;
                    $grand_total = 0;

                    if(!empty($bookingDates))
                    {
                        foreach ($bookingDates as $key => $bDateModel) 
                        {
                            ///////////////////////////// Changing Flag to 0 and commenting check available item names to allow over booking ////////////////
                            $flag = 0;

                            $grand_total += $bDateModel->total;

                            if($flag)
                            {
                                $item_names = $item_names.$bDateModel->item->itemType->name.' , ';
                                $bDateModel->delete();
                            }
                        }

                        if(!empty($item_names))
                        {
                            if(empty($bImodel->bookingDates))
                            {
                                $bImodel->delete();
                            }

                            $error_flag = 1;
                            Yii::$app->session->setFlash('error', 'One or more items you attempted to book are no longer available.');
                        }
                    }

                     $bImodel->balance = $grand_total;
                }

                if($error_flag)
                {
                    return $this->render('create', [
                        'model' => $model,
                        'bookingGroupModel' => $bookingGroupModel,
                        'group_name'        => $group_name, 
                    ]);
                }

                $booking_group_flag = 0;

                // ########### checking which radio button user has seleted ##########//
                if(isset($_POST['selected_user']) && $_POST['selected_user'] ==  'guest_user')
                {
                    $bookingGuestModel = new BookingGuestsCart();
                    $bookingGuestModel->first_name = isset($_POST['guest_first_name'])?$_POST['guest_first_name']:null;
                    $bookingGuestModel->last_name = isset($_POST['guest_last_name'])?$_POST['guest_last_name']:null;
                    $bookingGuestModel->country_id = isset($_POST['guest_country'])?$_POST['guest_country']:null;
                    $bookingGuestModel->save();

                    $bookingGroupModel->group_guest_user_id = $bookingGuestModel->id;
                    $bookingGroupModel->group_user_id = null;
                    if($bookingGroupModel->save())
                    {
                        $booking_group_flag = 1;
                    }
                }
                else
                {
                    if(!empty($bookingGroupModel->group_name) || !empty($bookingGroupModel->group_user_id) || !empty(isset($bookingGroupModel->group_guest_user_id)?$bookingGroupModel->group_guest_user_id:'' ) )
                    {
                        if($bookingGroupModel->save())
                        {
                            $booking_group_flag = 1;
                        }
                    }
                }   
                // ########### checking which radio button user has seleted ##########//

                // ###### saving group comment ############/////
                $bookingGroupModel->comments = isset($_POST['group_comment'])?$_POST['group_comment']:null;
                if(!$bookingGroupModel->save())
                {
                    echo "<pre>";
                    print_r($bookingGroupModel->errors);
                    exit();
                }

                // ********************* Save Bookable Item(s) and their respective Rates ****************** //

                foreach ($bookingsItemsModel as $key => $bImodel) 
                {
                    $bImodel->balance = round($bImodel->balance);
                    $bImodel->update();

                    if($booking_group_flag)
                    {
                        $bookingItemGroupModel = new BookingItemGroupCart();
                        $bookingItemGroupModel->booking_item_id = $bImodel->id;
                        $bookingItemGroupModel->booking_group_id = $bookingGroupModel->id;
                        if(!$bookingItemGroupModel->save())
                        {
                            echo "<pre>";
                            print_r($bookingItemGroupModel->errors);
                            exit();
                        }
                        
                    }
                }
                // calculating total balance for booking group //
                if($booking_group_flag)
                {
                    $grand_group_total = 0;
                    foreach ($bookingsItemsModel as $bImodel) 
                    {
                        $grand_group_total+= $bImodel->balance;
                    }

                    $bookingGroupModel->balance = $grand_group_total;
                    if(!$bookingGroupModel->save())
                    {
                        echo "<pre>";
                        print_r($bookingGroupModel->errors);
                        exit();
                    }
                    //$bookingGroupModel->save();
                }

                /////////////////////// ADDING ENTRIES TO BOOKING DATE FINANCE CART TABLE ////////////////////////////
                foreach ($bookingsItemsModel as $key => $bImodel) 
                {
                    foreach ($bImodel->bookingDatesSortedAndNotNull as $bDate) 
                    {
                        $amount = ($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate;
                        $booking_date_finance = new BookingDateFinancesCart;
                        $booking_date_finance->date = $bDate->date;
                        $booking_date_finance->booking_item_id = $bDate->booking_item_id;
                        $booking_date_finance->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;
                        $booking_date_finance->price_description = $bDate->bookingItem->item->itemType->name;
                        $booking_date_finance->quantity = 1;

                        $booking_date_finance->amount = ($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate;
                        $booking_date_finance->vat = (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate;
                        $booking_date_finance->tax = (empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night;

                        $temp_x = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)/(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                        $booking_date_finance->x = $temp_x;

                        $temp_y = $temp_x - ((empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night);

                        $booking_date_finance->y = $temp_y;

                        $temp_discount = 0;
                        $temp_discount_percentage = 0;
                        if(isset($bDate->bookingsDiscountCodes) && !empty($bDate->bookingsDiscountCodes))
                        {
                            foreach ($bDate->bookingsDiscountCodes as  $bookingdc) 
                            {
                                $model = $bookingdc->discount;
                                if($model->applies_to == 0 or $model->applies_to == 1) 
                                {
                                    switch ($model->pricing_type) 
                                    {
                                        case 0: // percentage
                                            $discount_amount = $this->calculateDiscountAmount($bDate->person_price,$model->amount);

                                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                                            {
                                                $temp_discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                            }

                                            $temp_discount += $discount_amount;
                                            $temp_discount_percentage +=$model->amount;
                                            break;
                                        case 1: // amount
                                            $discount_amount = $model->amount;

                                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                                            {
                                                $discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                            }

                                            $temp_discount += $discount_amount;
                                            break;
                                    }

                                }

                            }
                            $booking_date_finance->discount = $temp_discount_percentage; 
                            $booking_date_finance->discount_amount = round($temp_discount,2);
                        }
                        else
                        {
                            if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) )
                            {

                                // checking if travel partner calculation type is commission //
                                if($bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_DISCOUNT)
                                {
                                    $booking_date_finance->discount = $bDate->travelPartner->comission;
                                    $temp_discount = $temp_y*($bDate->travelPartner->comission/100);
                                    $booking_date_finance->discount_amount = round($temp_discount,2);
                                }
                                if($bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                                {
                                    $booking_date_finance->discount = $bDate->travelPartner->comission;
                                    // $temp_discount = $temp_y*($bDate->travelPartner->comission/100);

                                    $temp_discount = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*($bDate->travelPartner->comission/100);
                                    $booking_date_finance->discount_amount = round($temp_discount,2);
                                }
                               

                                
                            }
                            else
                            {
                                if(isset($bDate->offers_id) && !empty($bDate->offers_id) )
                                {
                                    // $booking_date_finance_credit = new BookingDateFinancesCart;
                                    // $booking_date_finance_credit->date = $bDate->date;
                                    // $booking_date_finance_credit->booking_item_id = $bDate->booking_item_id;
                                    // $booking_date_finance_credit->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;

                                    // $booking_date_finance_credit->price_description = $bDate->offers->name.' - '.$bDate->offers->commission.' Discount';

                                    $booking_date_finance->discount = $bDate->offers->commission;
                                    $temp_discount = $temp_y*($bDate->offers->commission/100);
                                    $booking_date_finance->discount_amount = round($temp_discount,2);

                                    // $booking_date_finance_credit->final_total = round($temp_discount_credit,2);
                                    // $booking_date_finance_credit->type = BookingDateFinancesCart::TYPE_CREDIT;
                                    // $booking_date_finance_credit->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                                    // //$booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                                    // $booking_date_finance_credit->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                                    // if(!$booking_date_finance_credit->save())
                                    // {
                                    //     echo "<pre>";
                                    //     print_r($booking_date_finance_credit->errors);
                                    //     exit();
                                    // }
                                }
                            }
                        }

                        // if($temp_discount_percentage == 0)
                        // {
                        //     $booking_date_finance->discount = 0;
                        // }
                        // if($temp_discount == 0)
                        // {
                        //     $booking_date_finance->discount_amount = 0;
                        // }

                        if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) && $bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                        {
                            $temp_subtotal1 = $temp_y - $temp_discount;
                            $booking_date_finance->sub_total1 = $temp_subtotal1;

                            $temp_subtotal2 = $temp_subtotal1 + ((empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night);

                            $booking_date_finance->sub_total2 = $temp_subtotal2;

                            $temp_vat_amount = $temp_subtotal2*(( (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100);
                            // $booking_date_finance->vat_amount  = round($temp_vat_amount,2);
                            // formula for new vat amount //
                            $temp_new_vat_rate = (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate;
                            if($temp_new_vat_rate > 0)
                            {
                                $cal_1 = 100 + $temp_new_vat_rate;
                                $cal_2 = 100/$cal_1;
                                $cal_3 = round(($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate) * $cal_2;
                                $new_vat_amount = round(round(($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)  - $cal_3);

                                $booking_date_finance->vat_amount  = $new_vat_amount;
                            }
                            else
                            {
                                $booking_date_finance->vat_amount  = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                            }
                            


                            // end 
                            // $booking_date_finance->vat_amount  = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                            $booking_date_finance->final_total = round(($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate);
                        }
                        else
                        {
                            $temp_subtotal1 = $temp_y - $temp_discount;
                            $booking_date_finance->sub_total1 = $temp_subtotal1;

                            $temp_subtotal2 = $temp_subtotal1 + ((empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night);

                            $booking_date_finance->sub_total2 = $temp_subtotal2;

                            $temp_vat_amount = $temp_subtotal2*(( (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100);
                            $booking_date_finance->vat_amount  = round($temp_vat_amount,2);
                            $booking_date_finance->final_total = round($temp_subtotal2 + $temp_vat_amount);
                        }
                        
 
                        $booking_date_finance->type = BookingDateFinancesCart::TYPE_DEBIT;
                        $booking_date_finance->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                        $booking_date_finance->item_type = BookingDateFinancesCart::ITEM_TYPE_BOOKING;
                        $booking_date_finance->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                        if(!$booking_date_finance->save())
                        {
                            echo "<pre>";
                            print_r($booking_date_finance->errors);
                            exit();
                        }

                        if(isset($bDate->bookingsUpsellItems))
                        {
                            foreach ($bDate->bookingsUpsellItems as $value) 
                            {
                                $booking_date_finance_upsell = new BookingDateFinancesCart;
                                $booking_date_finance_upsell->date = $bDate->date;
                                $booking_date_finance_upsell->booking_item_id = $bDate->booking_item_id;
                                $booking_date_finance_upsell->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;
                                
                                $booking_date_finance_upsell->quantity = 1;

                                $temp_amount = 0;
                                $temp_vat = 0;
                                $temp_tax = 0;
                                if(isset($bDate->rate->ratesUpsells)  && !empty($bDate->rate->ratesUpsells))
                                {
                                    foreach ($bDate->rate->ratesUpsells as $ratesUpsell) 
                                    {
                                        if($ratesUpsell->upsell_id == $value->upsell_id)
                                        {
                                            $booking_date_finance_upsell->price_description = $ratesUpsell->upsell->name;

                                            $booking_date_finance_upsell->amount = $ratesUpsell->price;
                                            $temp_amount = $ratesUpsell->price;

                                            $booking_date_finance_upsell->vat = (empty($ratesUpsell->vat) || ($ratesUpsell->vat == NULL))?0:$ratesUpsell->vat;
                                            $temp_vat = (empty($ratesUpsell->vat) || ($ratesUpsell->vat == NULL))?0:$ratesUpsell->vat;

                                            $booking_date_finance_upsell->tax = (empty($ratesUpsell->tax) || ($ratesUpsell->tax == NULL))?0:$ratesUpsell->tax;
                                            $temp_tax = (empty($ratesUpsell->tax) || ($ratesUpsell->tax == NULL))?0:$ratesUpsell->tax;
                                        }
                                    }
                                }
                                

                                $temp_x = $temp_amount/(1+($temp_vat/100));
                                $booking_date_finance_upsell->x = $temp_x;

                                $temp_y = $temp_x - $temp_tax;
                                $booking_date_finance_upsell->y = $temp_y;

                                $temp_discount = 0;
                                $temp_discount_percentage = 0;
                                if(isset($bDate->bookingsDiscountCodes) && !empty($bDate->bookingsDiscountCodes))
                                {
                                    foreach ($bDate->bookingsDiscountCodes as  $bookingdc) 
                                    {
                                        $model = $bookingdc->discount;
                                        if($model->applies_to == 1 or $model->applies_to == 2) 
                                        {
                                            switch ($model->pricing_type) 
                                            {
                                                case 0: // percentage
                                                    $discount_amount = $this->calculateDiscountAmount($temp_amount,$model->amount);

                                                    if($model->quantity_type == 1 || $model->quantity_type == 2)
                                                    {
                                                        $temp_discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                                    }

                                                    $temp_discount += $discount_amount;
                                                    $temp_discount_percentage +=$model->amount;
                                                    break;
                                                case 1: // amount
                                                    $discount_amount = $model->amount;

                                                    if($model->quantity_type == 1 || $model->quantity_type == 2)
                                                    {
                                                        $discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                                    }

                                                    $temp_discount += $discount_amount;
                                                    break;
                                            }

                                        }

                                    }
                                    $booking_date_finance_upsell->discount = $temp_discount_percentage; 
                                    $booking_date_finance_upsell->discount_amount = $temp_discount;
                                }
                                else
                                {
                                    if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) )
                                    {
                                        // $booking_date_finance_upsell_credit = new BookingDateFinancesCart;
                                        // $booking_date_finance_upsell_credit->date = $bDate->date;
                                        // $booking_date_finance_upsell_credit->booking_item_id = $bDate->booking_item_id;
                                        // $booking_date_finance_upsell_credit->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;
                                        
                                        // $booking_date_finance_upsell_credit->quantity = 1;
                                        // $booking_date_finance_upsell_credit->price_description = $bDate->travelPartner->travelPartner->company_name.' - '.$bDate->travelPartner->comission.' Discount';

                                        if($bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                                        {
                                            $booking_date_finance_upsell->discount = $bDate->travelPartner->comission;
                                            // $temp_discount = $temp_y*($bDate->travelPartner->comission/100);

                                            $temp_discount = ($temp_amount)*($bDate->travelPartner->comission/100);
                                            $booking_date_finance_upsell->discount_amount = round($temp_discount,2);
                                        }
                                        else
                                        {
                                            $booking_date_finance_upsell->discount = $bDate->travelPartner->comission;
                                            $temp_discount = $temp_y*($bDate->travelPartner->comission/100);
                                            $booking_date_finance_upsell->discount_amount = round($temp_discount,2);
                                        }
                                            

                                        // $booking_date_finance_upsell_credit->final_total = round($temp_discount_credit,2);
                                        // $booking_date_finance_upsell_credit->type = BookingDateFinancesCart::TYPE_CREDIT;
                                        // $booking_date_finance_upsell_credit->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                                        //$booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                                        // $booking_date_finance_upsell_credit->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                                        // if(!$booking_date_finance_upsell_credit->save())
                                        // {
                                        //     echo "<pre>";
                                        //     print_r($booking_date_finance_upsell->errors);
                                        //     exit();
                                        // }
                                    }
                                    else
                                    {
                                        if(isset($bDate->offers_id) && !empty($bDate->offers_id) )
                                        {
                                            // $booking_date_finance_upsell_credit = new BookingDateFinancesCart;
                                            // $booking_date_finance_upsell_credit->date = $bDate->date;
                                            // $booking_date_finance_upsell_credit->booking_item_id = $bDate->booking_item_id;
                                            // $booking_date_finance_upsell_credit->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;

                                            // $booking_date_finance_upsell_credit->price_description = $bDate->offers->name.' - '.$bDate->offers->commission.' Discount';

                                            $booking_date_finance_upsell->discount = $bDate->offers->commission;
                                            $temp_discount = $temp_y*($bDate->offers->commission/100);
                                            $booking_date_finance_upsell->discount_amount = round($temp_discount,2);

                                            // $booking_date_finance_upsell_credit->final_total = round($temp_discount_credit,2);
                                            // $booking_date_finance_upsell_credit->type = BookingDateFinancesCart::TYPE_CREDIT;
                                            // $booking_date_finance_upsell_credit->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                                            // //$booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                                            // $booking_date_finance_upsell_credit->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                                            // if(!$booking_date_finance_upsell_credit->save())
                                            // {
                                            //     echo "<pre>";
                                            //     print_r($booking_date_finance_upsell->errors);
                                            //     exit();
                                            // }
                                        }
                                    }
                                }

                                // if($temp_discount_percentage == 0)
                                // {
                                //     $booking_date_finance->discount = 0;
                                // }
                                // if($temp_discount == 0)
                                // {
                                //     $booking_date_finance->discount_amount = 0;
                                // }
                                if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) && $bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                                {
                                    $temp_subtotal1 = $temp_y - $temp_discount;
                                    $booking_date_finance_upsell->sub_total1 = $temp_subtotal1;

                                    $temp_subtotal2 = $temp_subtotal1 + $temp_tax;

                                    $booking_date_finance_upsell->sub_total2 = $temp_subtotal2;

                                    $temp_vat_amount = $temp_subtotal2*(( (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100);
                                    // $booking_date_finance->vat_amount  = round($temp_vat_amount,2);
                                    // formula for new vat amount //
                                    $temp_new_vat_rate = (empty($temp_vat) || ($temp_vat == NULL))?0:$temp_vat;
                                    if($temp_new_vat_rate > 0)
                                    {
                                        $cal_1 = 100 + $temp_new_vat_rate;
                                        $cal_2 = 100/$cal_1;
                                        $cal_3 = round($temp_amount) * $cal_2;
                                        $new_vat_amount = round(round($temp_amount)  - $cal_3);

                                        $booking_date_finance_upsell->vat_amount  = $new_vat_amount;
                                    }
                                    // else
                                    // {
                                    //     $booking_date_finance->vat_amount  = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                                    // }
                                    


                                    // end 
                                    // $booking_date_finance->vat_amount  = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                                    $booking_date_finance_upsell->final_total = round($temp_amount);
                                }
                                else
                                {
                                    $temp_subtotal1 = $temp_y - $temp_discount;
                                    $booking_date_finance_upsell->sub_total1 = $temp_subtotal1;

                                    $temp_subtotal2 = $temp_subtotal1 + $temp_tax;
                                    $booking_date_finance_upsell->sub_total2 = $temp_subtotal2;

                                    $temp_vat_amount = $temp_subtotal2*($temp_vat/100);
                                    $booking_date_finance_upsell->vat_amount  = round($temp_vat_amount,2);
                                    $booking_date_finance_upsell->final_total = round($temp_subtotal2 + $temp_vat_amount);
                                }
                                    $booking_date_finance_upsell->type = BookingDateFinancesCart::TYPE_DEBIT;
                                    $booking_date_finance_upsell->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                                    $booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                                    $booking_date_finance_upsell->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                                
                                    
                                if(!$booking_date_finance_upsell->save())
                                {
                                    echo "<pre>";
                                    print_r($booking_date_finance_upsell->errors);
                                    exit();
                                }
                            }
                        }


                    }

                    if(!empty($bImodel->bookingDateCharges))
                    {
                        $total_debit = BookingDateFinancesCart::find()->where(['booking_item_id' => $bImodel->id])
                                          ->andWhere(['type' => BookingDateFinancesCart::TYPE_DEBIT ])
                                          ->sum('final_total');

                        // $total_credit = BookingDateFinancesCart::find()->where(['booking_item_id' => $bImodel->id])
                        //                                           ->andWhere(['type' => BookingDateFinancesCart::TYPE_CREDIT ])
                        //                                           ->sum('final_total');
                        // $total_debit = empty($total_debit)?0:$total_debit;
                        // $total_credit =  empty($total_credit)?0:$total_credit;      

                        // $balance_due = $total_debit - $total_credit; 
                        // $balance = 0;
                        // foreach ($bImodel->bookingDateCharges as $charge) 
                        // {
                        //     $balance += $charge->final_total;
                        // }
                        // echo "<pre>";
                        // print_r($total_debit);
                        // exit();
                        $bImodel->balance = $total_debit;
                        $bImodel->save();
                    }
                }


                ////////////////////// HERE WE ARE COPYING ALL DATE FROM CART TO ORIGINAL TABLES //////////////////////////////

                ////////////////////////////////////// CREATING BOOKING GROUP /////////////////////////////////
                $orgBookingGroupModel = new BookingGroup();
                $orgBookingGroupModel->group_name = $bookingGroupModel->group_name;
                $orgBookingGroupModel->comments = $bookingGroupModel->comments;
               // $orgBookingGroupModel->group_user_id = $bookingGroupModel->group_user_id;
               // $orgBookingGroupModel->group_guest_user_id = $bookingGroupModel->group_guest_user_id;
                $orgBookingGroupModel->balance = $bookingGroupModel->balance;

                if(!$orgBookingGroupModel->save(false))
                {
                    echo "<pre>";
                    print_r($orgBookingGroupModel->errors);
                    exit();
                }

                if(isset($_POST['selected_user']) && $_POST['selected_user'] ==  'guest_user')
                {
                    $bookingGuestModel = new BookingGuests();
                    $bookingGuestModel->first_name = isset($_POST['guest_first_name'])?$_POST['guest_first_name']:null;
                    $bookingGuestModel->last_name = isset($_POST['guest_last_name'])?$_POST['guest_last_name']:null;
                    $bookingGuestModel->country_id = isset($_POST['guest_country'])?$_POST['guest_country']:null;
                    $bookingGuestModel->save();

                    $orgBookingGroupModel->group_guest_user_id = $bookingGuestModel->id;
                    $orgBookingGroupModel->group_user_id = null;
                    if($orgBookingGroupModel->save())
                    {
                        $booking_group_flag = 1;
                    }
                }
                else
                {
                    $orgBookingGroupModel->group_user_id = $bookingGroupModel->group_user_id;
                    if($orgBookingGroupModel->save())
                    {
                        $booking_group_flag = 1;
                    }
                }

                foreach ($bookingsItemsModel as $key => $bImodel) 
                {
                    //////////////////////////////// SAVING BOOKING ITEM DETAILS ///////////////////////////

                    $date1=date_create($bImodel->departure_date);
                    $date2=date_create($bImodel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $no_of_days =  $diff->format("%a");

                    $orgBookingItem = new BookingsItems();
                    $orgBookingItem->save(false);
                    //$orgBookingItem->booking_number = $orgBookingItem->id;
                    $orgBookingItem->provider_id = $bImodel->provider_id;
                    $orgBookingItem->item_id = $bImodel->item_id;
                    $orgBookingItem->arrival_date = $bImodel->arrival_date;
                    $orgBookingItem->departure_date = $bImodel->departure_date;
                    $orgBookingItem->rate_conditions = $bImodel->rate_conditions;
                    $orgBookingItem->pricing_type = $bImodel->pricing_type;
                    $orgBookingItem->created_by = $bImodel->created_by;
                    $orgBookingItem->item_name_id = $bImodel->item_name_id;
                    $orgBookingItem->status_id = $bImodel->status_id;
                    //$orgBookingItem->housekeeping_status_id = $bImodel->housekeeping_status_id;
                    $orgBookingItem->flag_id = $bImodel->flag_id;
                    $orgBookingItem->travel_partner_id = $bImodel->travel_partner_id;
                    $orgBookingItem->voucher_no = $bImodel->voucher_no;
                    $orgBookingItem->reference_no = (empty($bImodel->reference_no) || $bImodel->reference_no == '')?null:$bImodel->reference_no;
                    $orgBookingItem->comments = $bImodel->comments;
                    $orgBookingItem->notes = $bImodel->notes;
                    $orgBookingItem->offers_id = $bImodel->offers_id;
                    $orgBookingItem->estimated_arrival_time_id = $bImodel->estimated_arrival_time_id;
                    $orgBookingItem->temp_flag = 0;
                    $orgBookingItem->balance = $bImodel->balance;
                    $orgBookingItem->no_of_booking_days = $no_of_days;
                    $orgBookingItem->paid_amount = 0;

                    if(!$orgBookingItem->save())
                    {
                        echo "<pre>";
                        echo "booking item error";
                        print_r($orgBookingItem->errors);
                        exit();
                    }

                    //////////////////////////////// CHCEKING BOOKING STATUS AND ADDING FIELDS /////////////
                    if($orgBookingItem->status_id == 3)
                    {

                    }
                    else
                    {
                        $date1=date_create(date('Y-m-d'));
                        $date2=date_create($bImodel->arrival_date);
                        $diff=date_diff($date1,$date2);
                        $no_of_days =  $diff->format("%a");

                        $orgBookingItem->booking_to_arrival = $no_of_days;
                        $orgBookingItem->save();
                    }



                    ////////////////////////////////SAVING HOUSEKEEPING STATUSES ///////////////////////////
                    if(isset($bImodel->bookingHousekeepingStatus))
                    {
                        //$housekeeping_status_ids = explode(',',$bookingItem['housekeeping_status_id'] );
                        //if(!empty($housekeeping_status_ids))
                        
                        foreach ($bImodel->bookingHousekeepingStatus as $status) 
                        {
                            $new_housekeeping_status = new BookingHousekeepingStatus();
                            $new_housekeeping_status->booking_item_id = $orgBookingItem->id;
                            $new_housekeeping_status->housekeeping_status_id = $status->housekeeping_status_id;
                            if(!$new_housekeeping_status->save())
                            {
                                echo "<pre>";
                                print_r($new_housekeeping_status->errors);
                                exit();
                            }
                        }
                        
                        
                    }
                    //////////////////////////////// SAVING BOOKING DATES DETAILS ///////////////////////////
                    if(!empty($bImodel->bookingDatesSortedAndNotNull))
                    {
                        foreach ($bImodel->bookingDatesSortedAndNotNull as $bDate) 
                        {
                            $bookingDatesModel = new BookingDates();

                            $bookingDatesModel->booking_item_id = $orgBookingItem->id;
                            $bookingDatesModel->date = $bDate->date;
                            $bookingDatesModel->item_id = $bDate->item_id;
                            $bookingDatesModel->item_name_id = $bDate->item_name_id;
                            $bookingDatesModel->rate_id = $bDate->rate_id;
                            $bookingDatesModel->custom_rate = $bDate->custom_rate;
                            // Adding Custom Rate Type Either throuh input or though daily rates //
                            $bookingDatesModel->rate_added_through = $bDate->rate_added_through; 
                            $bookingDatesModel->quantity = $bDate->quantity;
                            $bookingDatesModel->no_of_nights = $bDate->no_of_nights;
                            $bookingDatesModel->estimated_arrival_time_id = $bDate->estimated_arrival_time_id;
                            $bookingDatesModel->beds_combinations_id = $bDate->beds_combinations_id;
                            $bookingDatesModel->user_id = $bDate->user_id;
                            $bookingDatesModel->guest_first_name = $bDate->guest_first_name;
                            $bookingDatesModel->guest_last_name = $bDate->guest_last_name;
                            $bookingDatesModel->guest_country_id = $bDate->guest_country_id;                    
                            $bookingDatesModel->confirmation_id = $bDate->confirmation_id;
                            $bookingDatesModel->booking_cancellation = $bDate->booking_cancellation;
                            $bookingDatesModel->flag_id = $bDate->flag_id;
                            $bookingDatesModel->status_id = $bDate->status_id;
                           // $bookingDatesModel->housekeeping_status_id = $bDate->housekeeping_status_id;
                            $bookingDatesModel->no_of_adults = $bDate->no_of_adults;
                            $bookingDatesModel->no_of_children = $bDate->no_of_children;
                            $bookingDatesModel->person_no = $bDate->person_no;
                            $bookingDatesModel->person_price = $bDate->person_price;
                            $bookingDatesModel->total = $bDate->total;
                            $bookingDatesModel->price_json = $bDate->price_json;
                            $bookingDatesModel->travel_partner_id = $bDate->travel_partner_id;
                            $bookingDatesModel->voucher_no = $bDate->voucher_no;
                            $bookingDatesModel->reference_no = $bDate->reference_no;
                            $bookingDatesModel->offers_id = $bDate->offers_id;
                            
                            if(!$bookingDatesModel->save())
                            {
                                echo "<pre>";
                                echo "booking dates error";
                                print_r($bookingDatesModel->errors);
                                exit();
                            }
                            if($bookingDatesModel->save())
                            {

                                if(isset($bDate->bookingsDiscountCodes))
                                {
                                    foreach ($bDate->bookingsDiscountCodes as $value) 
                                    {

                                        $obj = new BookingsDiscountCodes();
                                        $obj->discount_id = $value->discount_id;
                                        $obj->price =  $value->price;
                                        $obj->booking_date_id = $bookingDatesModel->id;
                                        $obj->extra_bed_quantity = $value->extra_bed_quantity;


                                        if(!$obj->save())
                                        {
                                            echo "<pre>";
                                            print_r($obj->errors);
                                            exit();
                                        }
                                    }
                                }

                                if(isset($bDate->bookingsUpsellItems))
                                {
                                    foreach ($bDate->bookingsUpsellItems as $value) 
                                    {
                                        $obj = new BookingsUpsellItems();
                                        $obj->upsell_id = $value->upsell_id;
                                        $obj->price = $value->price;
                                        $obj->booking_date_id = $bookingDatesModel->id;
                                        $obj->extra_bed_quantity = $value->extra_bed_quantity;

                                        if(!$obj->save())
                                        {
                                            echo "<pre>";
                                            print_r($obj->errors);
                                            exit();
                                        }
                                    }
                                }

                                if(isset($bDate->bookingSpecialRequests))
                                {
                                    foreach ($bDate->bookingSpecialRequests as $value) 
                                    {
                                        $model = new BookingSpecialRequestsCart();
                                        $model->booking_date_id = $bookingDatesModel->id;
                                        $model->request_id = $value->request_id;
                                        if(!$model->save())
                                        {
                                            echo "<pre>";
                                            print_r($model->errors);
                                            exit();
                                        }
                                    }
                                }
                            }

                            ///////////////////// ADDING NOTIFICATION IF ANY FOR THIS BOOKING DATE /////////////////////
                            $total_rooms = 0;
                            foreach ($orgBookingItem->provider->bookableItems as  $value)
                            {
                                $total_rooms = $total_rooms + $value->item_quantity; 
                            }
                
                           
                            if(count(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date, 'provider_id' => $orgBookingItem->provider_id,'bookings_items.deleted_at' => 0 ])->all()) > $total_rooms)
                            {
                                $destination_overbooking_noti_exists = Notifications::findOne(['destination_id' => $orgBookingItem->provider_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::DESTINATION_OVERBOOKING]);
                                if(empty($destination_overbooking_noti_exists))
                                {
                                    $rate_noti = new Notifications();
                                    $rate_noti->destination_id = $orgBookingItem->provider_id;
                                    $rate_noti->booking_item_id = $orgBookingItem->id;
                                    $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                                    $rate_noti->rate_id = $bookingDatesModel->rate_id;
                                    $rate_noti->date = $bookingDatesModel->date;
                                    $rate_noti->notification_type = Notifications::DESTINATION_OVERBOOKING;
                                    $rate_noti->message = "Overbooking on ".$bookingDatesModel->date;
                                    $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                                    $rate_noti->save();
                                }
                                    
                            }
                            if(!empty($bookingDatesModel->item_name_id))
                            {
                                if(!empty(BookingDates::find()->joinWith('bookingItem')->where(['date' => $bookingDatesModel->date, 'booking_dates.item_name_id' => $bookingDatesModel->item_name_id,'bookings_items.deleted_at' => 0 ])->andWhere(['!=','booking_dates.id',$bookingDatesModel->id ])->all() ))
                                {
                                    $multiple_unit_noti_exists = Notifications::findOne(['bookable_item_id' => $bookingDatesModel->item_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::UNIT_OVERBOOKING]);
                                    if(empty($multiple_unit_noti_exists))
                                    {
                                        $rate_noti = new Notifications();
                                        $rate_noti->destination_id = $orgBookingItem->provider_id;
                                        $rate_noti->booking_item_id = $orgBookingItem->id;
                                        $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                                        $rate_noti->bookable_item_name_id = $bookingDatesModel->item_name_id;
                                        $rate_noti->rate_id = $bookingDatesModel->rate_id;
                                        $rate_noti->date = $bookingDatesModel->date;
                                        $rate_noti->notification_type = Notifications::UNIT_OVERBOOKING;
                                        $rate_noti->message = "Multiple booking in unit on ".$bookingDatesModel->date;
                                        $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                                        $rate_noti->save();
                                    }
                                }

                            }

                            if($bookingDatesModel->item_name_id == NULL)
                            {
                                // $multiple_units_noti_exists = Notifications::findOne(['destination_id' => $orgBookingItem->provider_id,'bookable_item_id' => $orgBookingItem->item_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::UNIT_NOT_ASSIGNED]);
                                // if(empty($multiple_units_noti_exists))
                                // {
                                $multiple_units_noti_exists = Notifications::findOne(['booking_item_id' => $orgBookingItem->id,'destination_id' => $orgBookingItem->provider_id,'bookable_item_id' => $orgBookingItem->item_id, 'date' => $bookingDatesModel->date, 'notification_type' => Notifications::UNIT_NOT_ASSIGNED]);
                                if(empty($multiple_units_noti_exists))
                                {
                                    $rate_noti = new Notifications();
                                    $rate_noti->destination_id = $orgBookingItem->provider_id;
                                    $rate_noti->booking_item_id = $orgBookingItem->id;
                                    $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                                    $rate_noti->rate_id = $bookingDatesModel->rate_id;
                                    $rate_noti->date = $bookingDatesModel->date;
                                    $rate_noti->notification_type = Notifications::UNIT_NOT_ASSIGNED;
                                    $rate_noti->message = "No Unit assigned on ".$bookingDatesModel->date;
                                    $rate_noti->user_type = User::USER_TYPE_ADMIN;
                                    $rate_noti->save();
                                }
                            }
                        }
                    }
                    //////////////////////////////// SAVING BOOKING GROUP AND BOOKING ITEM GROUP DETAILS ////////////


                    

                    if(isset($orgBookingGroupModel->id))
                    {
                        $bookingItemGroupModel = new BookingItemGroup();
                        $bookingItemGroupModel->booking_item_id = $orgBookingItem->id;
                        $bookingItemGroupModel->booking_group_id = $orgBookingGroupModel->id;
                        if(!$bookingItemGroupModel->save())
                        {
                            echo "<pre>";
                            echo "item group error";
                            print_r($bookingItemGroupModel->errors);
                            exit();
                        }
                        
                    }

                    //////////////////////////////// SAVING BOOKING DATE FINANCES DETAILS ///////////////////////////
                    $balance = 0;
                    $net_total = 0;
                    $discount_amount = 0;
                    if(!empty($bImodel->bookingDateFinances))
                    {
                        // echo "<pre>";
                        // print_r($bImodel->bookingDateFinances);
                        // exit();
                        
                        foreach ($bImodel->bookingDateFinances as $finance) 
                        {
                            // echo "<pre>";
                            // print_r($finance);
                            // exit();
                            $orgBookingFinance = new BookingDateFinances();

                            $orgBookingFinance->date = $finance->date;
                            $orgBookingFinance->booking_item_id = $orgBookingItem->id;
                            $orgBookingFinance->booking_group_id = $orgBookingGroupModel->id;  
                            $orgBookingFinance->type = $finance->type;
                            $orgBookingFinance->item_type = $finance->item_type;
                            $orgBookingFinance->entry_type = $finance->entry_type;                          
                            $orgBookingFinance->price_description = $finance->price_description;
                            $orgBookingFinance->amount = $finance->amount;
                            $orgBookingFinance->vat = $finance->vat;
                            $orgBookingFinance->tax = $finance->tax;
                            $orgBookingFinance->x = $finance->x;
                            $orgBookingFinance->y = $finance->y;
                            $orgBookingFinance->discount = $finance->discount;
                            $orgBookingFinance->discount_amount = $finance->discount_amount;
                            $orgBookingFinance->sub_total1 = $finance->sub_total1;
                            $orgBookingFinance->sub_total2 = $finance->sub_total2;
                            $orgBookingFinance->vat_amount = $finance->vat_amount;
                            $orgBookingFinance->final_total = $finance->final_total;
                            $orgBookingFinance->quantity = $finance->quantity;
                            $orgBookingFinance->show_receipt = $finance->show_receipt;
                            if(!$orgBookingFinance->save())
                            {
                                echo "<pre>";
                                print_r($orgBookingFinance->errors);
                                exit();
                            }
                            $balance += $finance->final_total;
                            $net_total += $finance->amount;
                            $discount_amount += $finance->discount_amount;
                        }
                    }
                    // $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $orgBookingItem->id])
                    //                                               ->andWhere(['type' => BookingDateFinancesCart::TYPE_CREDIT ])
                    //                                               ->sum('final_total');
                    // $net_total = BookingDateFinances::find()->where(['booking_item_id' => $orgBookingItem->id])
                    //                                               ->andWhere(['type' => BookingDateFinancesCart::TYPE_CREDIT ])
                    //                                               ->sum('amount');
                    // echo "<pre>";
                    // print_r($net_total);
                    // exit();
                    $orgBookingItem->balance = $balance;
                    $orgBookingItem->gross_total = $balance;
                    $orgBookingItem->averaged_total = round($balance/$orgBookingItem->no_of_booking_days);
                    // $orgBookingItem->net_total = $net_total;
                    $orgBookingItem->net_total = $balance - round($discount_amount);
                    $orgBookingItem->save();


                    /////////////////////// ADDING NOTIFICATION IF ANY FOR THIS BOOKING ITEM /////////////////////

                    $all_rates = Rates::find()->where(['unit_id' => $orgBookingItem->item_id])->all();
                    $notfication_rates = array();
                    $notification_exists = 0;
                    $largest = date('Y-m-d');
                    $largest_rate = null;
                    if(!empty($all_rates))
                    {

                        foreach ($all_rates as $value) 
                        {
                            if(date("Y-m-d",strtotime($value->to)) > $largest)
                            {
                                $largest = date("Y-m-d",strtotime($value->to));
                                $largest_rate = $value;
                            }

                            //if((time()-(60*60*24)) < strtotime($var))
                            
                            
                        } 
                    }
                    if($largest_rate != null)
                    {
                        $d1 = new DateTime($largest_rate->to);
                        $d2 = new DateTime(date('Y-m-d'));

                        if($d1->diff($d2)->m < 12) // int(4)
                        {
                            //array_push($notfication_rates, $value);
                        
                        // $message = "";
                        // foreach ($notfication_rates as $value) 
                        // {
                            $bDate = BookingDates::findOne(['booking_item_id' => $orgBookingItem->id]);
                            /*$rate_noti_exists = Notifications::findOne(['destination_id' => $orgBookingItem->provider_id,'bookable_item_id' => $orgBookingItem->item_id, 'rate_id' => $bDate->rate_id, 'notification_type' => Notifications::RATE_NOT_AVAILABLE]);
                            if(empty($rate_noti_exists))
                            {
                                $rate_noti = new Notifications();
                                $rate_noti->destination_id = $orgBookingItem->provider_id;
                                $rate_noti->booking_item_id = $orgBookingItem->id;
                                $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                                $rate_noti->rate_id = $largest_rate->id;
                                $rate_noti->notification_type = Notifications::RATE_NOT_AVAILABLE;
                                $rate_noti->message = "No rate available after ".$largest_rate->to;
                                $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                                $rate_noti->save();
                                //$message.= $value->name." not available after ".$value->to."<br>";
                            }*/
                        }
                        // }
                        // $rate_noti = new Notifications();
                        // $rate_noti->destination_id = $orgBookingItem->provider_id;
                        // $rate_noti->booking_item_id = $orgBookingItem->id;
                        // $rate_noti->bookable_item_id = $orgBookingItem->item_id;
                        // $rate_noti->notification_type = Notifications::RATE_NOT_AVAILABLE;
                        // $rate_noti->message = $message;
                        // $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                        // $rate_noti->save();
                        
                    }

                }

                ////////////////////////////// SAVING UPDATED BALANCE FOR BOOKING GROUP /////////////////
                $group_balance = 0;
                foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
                {
                    $group_balance += $group_item->bookingItem->balance;
                }
                $orgBookingGroupModel->balance = $group_balance;
                $orgBookingGroupModel->save();
                /*print_r($orgBookingGroupModel->getErrors());
                exit;*/
                ////////////////////////////// DELETING ALL CART ENTRIES ////////////////////////////////
                foreach ($bookingsItemsModel as $key => $bImodel) 
                {
                    $bImodel->delete();
                }
                BookingGuestsCart::deleteAll();
                $bookingGroupModel->delete();

            }
            return $this->redirect(['index']);
        } 
        else 
        {
            $session = Yii::$app->session;
            $session->open();

            if(isset($session[Yii::$app->controller->id.'-create-booking-values']) && !empty($session[Yii::$app->controller->id.'-create-booking-values']))
            {
                $params = json_decode($session[Yii::$app->controller->id.'-create-booking-values'], true);
                $model->provider_id = $params['provider_id'];
                $model->arrival_date = date('d/m/Y',strtotime($params['arrival_date']));
                $model->departure_date = date('d/m/Y',strtotime($params['departure_date']));
                unset($session[Yii::$app->controller->id.'-create-booking-values']);
            }
            else
            {
                $model->arrival_date = date('d/m/Y');
                $model->departure_date = strtotime("+1 day", strtotime(date('d-m-Y')));
                $model->departure_date = date('d/m/Y',$model->departure_date);
            }

            return $this->render('create', [
                'model' => $model,
                'bookingGroupModel' => $bookingGroupModel,
                'group_name'        => $group_name,  
            ]);
        }
    }

    public function actionUpdate($id,$check=null,$tab_href='#details',$flash=null,$pageNo =null)
    {
        $tab_href = $tab_href;

        $model = $this->findModel($id);
        $viewDetailsRecord = [];

        $bookingDates = $model->bookingDatesSortedAndNotNull;
        //$bookingDates = $model->bookingDates;

        $destination_items = $this->getItemtypenames($model->provider_id,$model->arrival_date,$model->departure_date,$model->rate_conditions);

        // begin a fix to an issue ( bookable item name chnage automatically)
        if(!array_key_exists($model->item_id,$destination_items))
        {
            $destination_items[$model->item_id] = $model->item->itemType->name;
        }
        // end

        if(!empty($bookingDates))
        {
            foreach ($bookingDates as $key => $bDateModel)
            {
                if($key == 0)
                {
                    $viewDetailsRecord['travel_partner_id'] = $bDateModel->travel_partner_id;
                    $viewDetailsRecord['offers_id'] = $bDateModel->offers_id;
                    $viewDetailsRecord['estimated_arrival_time_id'] = $bDateModel->estimated_arrival_time_id; 
                    $viewDetailsRecord['beds_combinations_id'] = $bDateModel->beds_combinations_id; 
                    $viewDetailsRecord['user_id'] = $bDateModel->user_id; 
                    $viewDetailsRecord['guest_first_name'] = $bDateModel->guest_first_name; 
                    $viewDetailsRecord['guest_last_name'] = $bDateModel->guest_last_name; 
                    $viewDetailsRecord['guest_country_id'] = $bDateModel->guest_country_id; 
                    $viewDetailsRecord['booking_cancellation'] = $bDateModel->booking_cancellation;
                    $viewDetailsRecord['confirmation_id'] = $bDateModel->confirmation_id;
                    $viewDetailsRecord['flag_id'] = $bDateModel->flag_id;
                    $viewDetailsRecord['status_id'] = $bDateModel->status_id;
                   // $viewDetailsRecord['housekeeping_status_id'] = $bDateModel->housekeeping_status_id;
                    $viewDetailsRecord['item_name_id'] = $model->item_name_id;
                    $viewDetailsRecord['voucher_no'] = $bDateModel->voucher_no;
                    // $viewDetailsRecord['reference_no'] = $bDateModel->reference_no;
                    $viewDetailsRecord['reference_no'] = $model->reference_no;
                    $viewDetailsRecord['notes'] = $model->notes;
                    $viewDetailsRecord['comment'] = $model->comments;

                }
            }
            if(isset($model->bookingHousekeepingStatus) && !empty($model->bookingHousekeepingStatus))
            {
                foreach ($model->bookingHousekeepingStatus as  $value) 
                {
                    $viewDetailsRecord['housekeeping_status_id'][] = $value->housekeeping_status_id;
                }
            }
            else
            {
                $viewDetailsRecord['housekeeping_status_id'][] = null;
            }
        }
        else
        {
            $viewDetailsRecord['travel_partner_id'] = $model->travel_partner_id;
            $viewDetailsRecord['offers_id'] = $model->offers_id;
            $viewDetailsRecord['estimated_arrival_time_id'] = $model->estimated_arrival_time_id;
            $viewDetailsRecord['item_name_id'] = $model->item_name_id;
            $viewDetailsRecord['beds_combinations_id'] = null; 
            $viewDetailsRecord['user_id'] = null; 
            $viewDetailsRecord['guest_first_name'] = null; 
            $viewDetailsRecord['guest_last_name'] = null; 
            $viewDetailsRecord['guest_country_id'] = null; 
            $viewDetailsRecord['booking_cancellation'] = null;
            $viewDetailsRecord['confirmation_id'] = null;
            $viewDetailsRecord['flag_id'] = $model->flag_id;
            $viewDetailsRecord['status_id'] = $model->status_id;
            //$viewDetailsRecord['housekeeping_status_id'] = $model->housekeeping_status_id;
            $viewDetailsRecord['item_name_id'] = $model->item_name_id;
            $viewDetailsRecord['voucher_no'] = $model->voucher_no;
            $viewDetailsRecord['reference_no'] = $model->reference_no;
            $viewDetailsRecord['notes'] = $model->notes;
            $viewDetailsRecord['comment'] = $model->comments;
        }

        if($flash!=null)
        {
            Yii::$app->session->setFlash('success', 'Item updated successfully.');
        }
        
        return $this->render('update', [
                'model' => $model,
                'viewDetailsRecord' => $viewDetailsRecord,
                'destination_items' => $destination_items,
                'check'             => $check,
                'tab_href' => $tab_href,
                'pageNo' => $pageNo
            ]);
    }

    public function actionUpdateBookedItem()
    {

        $bookingItems = $_POST['bookingItems'];
        $bookingItemsModel = $this->findModel($_POST['booking_item_id']);
       // Notifications::deleteAll(['booking_item_id' => $bookingItemsModel->id ]);
        // Deleting notifications which are related to rate 
        Notifications::deleteAll(['AND', 'booking_item_id = :booking_item_id', ['NOT IN', 'notification_type', [Notifications::UNIT_OVERBOOKING,Notifications::DESTINATION_OVERBOOKING]]], [':booking_item_id' => $bookingItemsModel->id]);

        //BookingDates::deleteAll(['booking_item_id' => $_POST['booking_item_id']]);

        $old_booking_dates = array();

        foreach ($bookingItemsModel->bookingDatesSortedAndNotNull as $bDate) 
        {
            array_push($old_booking_dates, $bDate->id);
        }

        $dateArr = [];
        $allDates = [];

        foreach ($_POST['allDates'] as $key => $value) 
        {
            $value = explode(',', $value);

            for ($i=0; $i <count($value) ; $i++) 
            {
                array_push($allDates,$value[$i]);
            }
        }

        sort($allDates);

        foreach ($bookingItems as $key => $bookingItem) 
        {
            $item_date = explode(',', $bookingItem['date']);
            //$grand_total = 0;
            if($key==0)
            {

                $this->isAttributeUpdated('Arrival Date',$bookingItemsModel->arrival_date,$allDates[0],$bookingItemsModel->id);
                $bookingItemsModel->arrival_date = $allDates[0];
                $this->isAttributeUpdated('Departure Date',$bookingItemsModel->departure_date,date('Y-m-d',strtotime("+1 day", strtotime($allDates[count($allDates)-1]))),$bookingItemsModel->id);
                $bookingItemsModel->departure_date = date('Y-m-d',strtotime("+1 day", strtotime($allDates[count($allDates)-1])));
                $bookingItemsModel->rate_conditions = $bookingItem['remove_rate_conditions'];

                $new_bookableItem = BookableItems::findOne(['id' => $bookingItem['item_id']]);
                $this->isAttributeUpdated('Bookable Item',$bookingItemsModel->item->itemType->name,BookableItemTypes::findOne(['id' => $new_bookableItem->item_type_id])->name,$bookingItemsModel->id);
                $bookingItemsModel->item_id = $bookingItem['item_id'];

                $old_item_name = !empty($bookingItemsModel->item_name_id)?$bookingItemsModel->itemName->item_name:'';
                $new_item_name = empty(BookableItemsNames::findOne(['id' =>  $bookingItem['item_name_id'] ]))?'':BookableItemsNames::findOne(['id' =>  $bookingItem['item_name_id'] ])->item_name;
                $this->isAttributeUpdated('Unit',$old_item_name ,$new_item_name,$bookingItemsModel->id);
                $bookingItemsModel->item_name_id = $bookingItem['item_name_id'];

                $new_status = BookingStatuses::findOne(['id'=>$bookingItem['status_id'] ])->label;
                $this->isAttributeUpdated('Status',$bookingItemsModel->status->label,$new_status,$bookingItemsModel->id);
                $bookingItemsModel->status_id = $bookingItem['status_id'];
                //$bookingItemsModel->housekeeping_status_id = $bookingItem['housekeeping_status_id'];
                $new_flag = BookingTypes::findOne(['id' => $bookingItem['flag_id']])->label;
                $this->isAttributeUpdated('Flag',$bookingItemsModel->flag->label,$new_flag,$bookingItemsModel->id);
                $bookingItemsModel->flag_id = $bookingItem['flag_id'];

                $old_travel_partner = !empty($bookingItemsModel->travelPartner->travelPartner->company_name)?$bookingItemsModel->travelPartner->travelPartner->company_name:'';
                $new_travel_partner = DestinationTravelPartners::findOne(['id' => $bookingItem['travel_partner_id']]);
                if(!empty($new_travel_partner))
                {
                    $new_travel_partner = $new_travel_partner->travelPartner->company_name;
                }
                else
                {
                    $new_travel_partner = '';
                }
                $this->isAttributeUpdated('Travel Partner',$old_travel_partner,$new_travel_partner,$bookingItemsModel->id);
                $bookingItemsModel->travel_partner_id = $bookingItem['travel_partner_id'];
                $this->isAttributeUpdated('Voucher No',$bookingItemsModel->voucher_no,$bookingItem['voucher_no'],$bookingItemsModel->id);
                $bookingItemsModel->voucher_no = $bookingItem['voucher_no'];
                $this->isAttributeUpdated('Reference No',$bookingItemsModel->reference_no,$bookingItem['reference_no'],$bookingItemsModel->id);
                $bookingItemsModel->reference_no = (empty($bookingItem['reference_no']) || $bookingItem['reference_no'] == '')?null:$bookingItem['reference_no'];
                $this->isAttributeUpdated('Comments',$bookingItemsModel->comments,$bookingItem['comment'],$bookingItemsModel->id);
                $bookingItemsModel->comments = $bookingItem['comment'];
                $this->isAttributeUpdated('Notes',$bookingItemsModel->notes,$bookingItem['notes'],$bookingItemsModel->id);
                $bookingItemsModel->notes = $bookingItem['notes'];

                $old_offer = !empty($bookingItemsModel->offers->name)?$bookingItemsModel->offers->name:'';
                $new_offer = Offers::findOne(['id' => $bookingItem['offer_id']]);
                if(!empty($new_offer))
                {
                    $new_offer = $new_offer->name;
                }
                else
                {
                    $new_offer = '';
                }
                $this->isAttributeUpdated('Offer',$old_offer,$new_offer,$bookingItemsModel->id);
                $bookingItemsModel->offers_id = $bookingItem['offer_id'];
                $bookingItemsModel->update();

                $date1=date_create($bookingItemsModel->departure_date);
                $date2=date_create($bookingItemsModel->arrival_date);
                $diff=date_diff($date1,$date2);
                $no_of_days =  $diff->format("%a");
                // isAttributeUpdated('No of Days',$bookingItemsModel->no_of_booking_days,$no_of_days,$bookingItemsModel->id);
                $bookingItemsModel->no_of_booking_days = $no_of_days;
                $bookingItemsModel->update();

                // echo "<pre>";
                // print_r($bookingItemsModel->getOldAttributes());
                // exit();
                if($bookingItemsModel->status_id == 3)
                {
                    $bookingItemsModel->cancelled_at = date('Y-m-d');
                    $bookingItemsModel->save();

                    $date1=date_create(date('Y-m-d',$bookingItemsModel->created_at));
                    $date2=date_create($bookingItemsModel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $no_of_days =  $diff->format("%a");

                    $bookingItemsModel->booking_to_arrival = $no_of_days;
                    $bookingItemsModel->save();

                    $date1=date_create(date('Y-m-d',$bookingItemsModel->created_at));
                    $date2=date_create(date('Y-m-d'));
                    $diff=date_diff($date1,$date2);
                    $no_of_days =  $diff->format("%a");

                    $bookingItemsModel->booking_to_cancellation = $no_of_days;
                    $bookingItemsModel->save();

                    $date1=date_create(date('Y-m-d'));
                    $date2=date_create($bookingItemsModel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $no_of_days =  $diff->format("%a");

                    $bookingItemsModel->cancellation_to_arrival = $no_of_days;
                    $bookingItemsModel->save();

                }
                else
                {
                    $date1=date_create(date('Y-m-d',$bookingItemsModel->created_at));
                    $date2=date_create($bookingItemsModel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $no_of_days =  $diff->format("%a");

                    $bookingItemsModel->booking_to_arrival = $no_of_days;
                    $bookingItemsModel->cancelled_at = null;
                    $bookingItemsModel->booking_to_cancellation = null;
                    $bookingItemsModel->cancellation_to_arrival = null;
                    $bookingItemsModel->save();
                }

                // $this->isArrayAttributeUpdated('House Keeping Status',empty(BookingHousekeepingStatus::find()->where(['booking_item_id' => $bookingItemsModel->id])->asArray()->all())?[]:BookingHousekeepingStatus::find()->where(['booking_item_id' => $bookingItemsModel->id])->asArray()->all(),(isset($bookingItem['housekeeping_status_id']) && !empty($bookingItem['housekeeping_status_id']))?$bookingItem['housekeeping_status_id']:[], $bookingItemsModel->id);
                BookingHousekeepingStatus::deleteAll(['booking_item_id' => $bookingItemsModel->id]);
                if(isset($bookingItem['housekeeping_status_id']) && !empty($bookingItem['housekeeping_status_id']))
                {
                    //$housekeeping_status_ids = explode(',',$bookingItem['housekeeping_status_id'] );
                    //if(!empty($housekeeping_status_ids))
                    
                    foreach ($bookingItem['housekeeping_status_id'] as $id) 
                    {
                        $new_housekeeping_status = new BookingHousekeepingStatus();
                        $new_housekeeping_status->booking_item_id = $bookingItemsModel->id;
                        $new_housekeeping_status->housekeeping_status_id = $id;
                        $new_housekeeping_status->save();
                    }
                }
            }

            $temp_date_for_custom_rate = date("Y-m-d", strtotime($item_date[0]));
            // getting old dates //
            $old_dates = array();
            $old_adults = array();
            $old_children = array();
            $old_no_of_nights = $bookingItemsModel->bookingDatesSortedAndNotNull[0]->no_of_nights;
            $old_beds_combination = ($bookingItemsModel->bookingDatesSortedAndNotNull[0]->beds_combinations_id != null)?$bookingItemsModel->bookingDatesSortedAndNotNull[0]->bedsCombinations->combination:'';
            $old_first_name = $bookingItemsModel->bookingDatesSortedAndNotNull[0]->guest_first_name;
            $old_last_name = $bookingItemsModel->bookingDatesSortedAndNotNull[0]->guest_last_name;
            $old_country = Countries::findOne([$bookingItemsModel->bookingDatesSortedAndNotNull[0]->guest_country_id])->name;
            $old_cancellation = $bookingItemsModel->bookingDatesSortedAndNotNull[0]->booking_cancellation;
            $old_confirmation_id = $bookingItemsModel->bookingDatesSortedAndNotNull[0]->confirmation_id;
            $old_estimated_arrival_time = $bookingItemsModel->bookingDatesSortedAndNotNull[0]->estimated_arrival_time_id;
            $old_person_no = array();
            $old_person_price = array();
            $old_balance = $bookingItemsModel->balance;

            foreach ($bookingItemsModel->bookingDatesSortedAndNotNull as  $value) 
            {
                array_push($old_dates, (string)$value->date);
                array_push($old_adults, (string)$value->no_of_adults);
                array_push($old_children, (string)$value->no_of_children);
                array_push($old_person_no, (string)$value->person_no);
                array_push($old_person_price, (string)$value->person_price);

            }

            for ($i=0; $i <count($item_date) ; $i++) 
            {
                $has_record = 0;
                $error_str = 0;

                $bookingDatesModel = new BookingDates();

                if($bookingItem['pricing_type']) // ************* Capacity Pricing ************ //
                {
                    $bookingDatesModel->no_of_adults = $bookingItem['no_of_adults'];
                    $bookingDatesModel->no_of_children = $bookingItem['no_of_children'];
                    
                    if(isset($bookingItem['person_no']) && $bookingItem['quantity'] > 0)
                    {
                        $bookingDatesModel->person_no = $bookingItem['person_no'];

                        $custom_rate = isset($bookingItem['custom_rate'])?$bookingItem['custom_rate']:NULL;
                        $bookingDatesModel->person_no = $bookingItem['person_no'];

                        $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $bookingItem['rate_id'], 'person_no' => $bookingItem['person_no']]);
                        if($custom_rate == $capacityPricingModel->price)
                        {
                            $bookingDatesModel->person_price = $capacityPricingModel->price;
                            $bookingDatesModel->custom_rate = null;
                        }
                        else
                        {
                            // chceking if its a custom rate is added through input or using daily rates //
                            if($bookingItem['person_no']==1)
                            {
                                $rate_override = RatesOverride::findOne(['rate_id' => $bookingItem['rate_id'], 'date' => $temp_date_for_custom_rate ]);
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                }
                            }
                            if($bookingItem['person_no']==2)
                            {
                                $rate_override = RatesOverride::findOne(['rate_id' => $bookingItem['rate_id'], 'date' => $temp_date_for_custom_rate]);
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                }
                            }
                            if($bookingItem['person_no']==3)
                            {
                                $rate_override = RatesOverride::findOne(['rate_id' => $bookingItem['rate_id'], 'date' => $temp_date_for_custom_rate]);
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                }
                            }
                            if($bookingItem['person_no']==4)
                            {
                                $rate_override = RatesOverride::findOne(['rate_id' => $bookingItem['rate_id'], 'date' => $temp_date_for_custom_rate]);
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                }
                            }
                            $bookingDatesModel->person_price = $capacityPricingModel->price;
                            $bookingDatesModel->custom_rate = $custom_rate;
                        }

                        $has_record = 1;
                    }
                }
                else // ************* First/Additional ************ //
                {
                    if(isset($bookingItem['no_of_adults']) && isset($bookingItem['no_of_children']) && 
                        ($bookingItem['no_of_adults']!=0 || $bookingItem['no_of_children']!=0) && $bookingItem['quantity'] > 0)
                    {
                        $bookingDatesModel->no_of_adults = $bookingItem['no_of_adults'];
                        $bookingDatesModel->no_of_children = $bookingItem['no_of_children'];
                        $has_record = 1;
                    }
                }

                if($has_record)
                {
                    $date = $bookingItemsModel->arrival_date;

                    $date1=date_create($bookingItemsModel->departure_date);
                    $date2=date_create($bookingItemsModel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a");

                    ///////////////////////////// Changing Flag to 0 and commenting check available item names to allow over booking ////////////////
                    $flag = 0;

                    // $flag = 1;

                    // $availableItemNames = BookingsItems::checkAvailableItemNamesIncludedSaved($bookingItem['item_id'],$bookingItemsModel->arrival_date,$difference,$bookingItemsModel->id);

                    // if(!empty($bookingItem['item_name_id']))
                    // {
                    //     if(!empty($availableItemNames) && in_array($bookingItem['item_name_id'], $availableItemNames))
                    //     {
                    //         $flag = 0;
                    //     }
                    // }
                    // else
                    // {
                    //     $flag = 0;
                    // }

                    if($flag)
                    {
                        BookingsItems::deleteAll(['item_id' => $bookingItem['item_id'],'created_by' => Yii::$app->user->identity->id,'temp_flag' => 1]);
                        $error_str = 1;

                        echo json_encode([
                            'bookings_items' => $this->renderAjax('side_bar_cart_view'),
                            'error' => $error_str,
                        ]);

                        return;
                    }
                    else
                    {
                        $bookingDatesModel->booking_item_id = $bookingItemsModel->id;
                        $bookingDatesModel->user_id = $bookingItem['user_id'];

                        if(empty($bookingDatesModel->user_id))
                        {   
                            $bookingDatesModel->guest_first_name = $bookingItem['guest_first_name'];
                            $bookingDatesModel->guest_last_name = $bookingItem['guest_last_name'];
                            $bookingDatesModel->guest_country_id = $bookingItem['guest_country'];
                        } 
                        
                        array_push($dateArr,$item_date[$i]);
                        $bookingDatesModel->date = $item_date[$i];
                        $bookingDatesModel->item_id = $bookingItem['item_id'];
                        $bookingDatesModel->item_name_id = $bookingItem['item_name_id'];
                        $bookingDatesModel->rate_id = $bookingItem['rate_id'];
                        $bookingDatesModel->beds_combinations_id = !empty($bookingItem['beds_combinations_id'])? $bookingItem['beds_combinations_id']:NULL;
                        $bookingDatesModel->quantity = $bookingItem['quantity'];
                        $bookingDatesModel->travel_partner_id = $bookingItem['travel_partner_id'];
                        $bookingDatesModel->voucher_no = $bookingItem['voucher_no'];
                        $bookingDatesModel->reference_no = $bookingItem['reference_no'];
                        $bookingDatesModel->offers_id = $bookingItem['offer_id'];
                        $bookingDatesModel->booking_cancellation = $bookingItem['booking_cancellation'];
                        $bookingDatesModel->flag_id = $bookingItem['flag_id'];
                        $bookingDatesModel->status_id = $bookingItem['status_id'];
                        //$bookingDatesModel->housekeeping_status_id = $bookingItem['housekeeping_status_id'];
                        $bookingDatesModel->confirmation_id = $bookingItem['confirmation_id'];
                        $bookingDatesModel->total = $bookingItem['total'];
                        $bookingDatesModel->price_json = str_replace("'", '"', $bookingItem['price_json']);
                        //$bookingDatesModel->no_of_nights = $bookingItem['total_nights'];
                        $bookingDatesModel->no_of_nights = $difference;
                        $bookingDatesModel->estimated_arrival_time_id = $bookingItem['estimated_arrival_time_id'];
                        
                        

                        if($bookingDatesModel->save())
                        {
                            

                            $bookingDatesModel->discount_codes = isset($bookingItem['discount_ids'])?$bookingItem['discount_ids']:'';
                            $bookingDatesModel->upsell_items = isset($bookingItem['upsell_ids'])?$bookingItem['upsell_ids']:'';
                            $bookingDatesModel->special_requests = isset($bookingItem['special_requests_ids'])?$bookingItem['special_requests_ids']:'';

                            if(!empty($bookingDatesModel->discount_codes))
                            {
                                foreach ($bookingDatesModel->discount_codes as $key => $value) 
                                {
                                    $arr = explode(':', $value);
                                    $discount_id = $arr[0];
                                    $discount_code_extra_bed_quantity = $arr[1];

                                    $discount_code_model = DiscountCode::findOne(['id' => $discount_id]);

                                    $obj = new BookingsDiscountCodes();
                                    $obj->discount_id = $discount_id;
                                    $obj->price = $discount_code_model->amount;
                                    $obj->booking_date_id = $bookingDatesModel->id;

                                    if($discount_code_extra_bed_quantity != 'undefined' && ($discount_code_model->quantity_type == 1 || $discount_code_model->quantity_type == 2))
                                    {
                                        $obj->extra_bed_quantity = $discount_code_extra_bed_quantity;
                                    }

                                    $obj->save();
                                }
                            }

                            if(!empty($bookingDatesModel->upsell_items))
                            {
                                foreach ($bookingDatesModel->upsell_items as $key => $value) 
                                {
                                    $arr = explode(':', $value);
                                    $upsell_id = $arr[0];
                                    $upsell_extra_bed_quantity = $arr[1];

                                    $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $bookingItem['rate_id'], 'upsell_id' => $upsell_id]);

                                    $obj = new BookingsUpsellItems();
                                    $obj->upsell_id = $upsell_id;
                                    $obj->price = $ratesUpsellModel->price;
                                    $obj->booking_date_id = $bookingDatesModel->id;

                                    if($ratesUpsellModel->upsell->type == 7 && $upsell_extra_bed_quantity != 'undefined')
                                    {
                                        $obj->extra_bed_quantity = $upsell_extra_bed_quantity;
                                    }

                                    $obj->save();
                                }
                            }

                            if(!empty($bookingDatesModel->special_requests))
                            {
                                foreach ($bookingDatesModel->special_requests as $key => $value) 
                                {
                                    $model = new BookingSpecialRequests();
                                    $model->booking_date_id = $bookingDatesModel->id;
                                    $model->request_id = $value;
                                    $model->save();
                                }
                            }
                        }
                    }
                }

                $temp_date_for_custom_rate = strtotime("+1 day", strtotime($temp_date_for_custom_rate));
                $temp_date_for_custom_rate = date("Y-m-d", $temp_date_for_custom_rate);
            }
        }

        $new_booked_dates = BookingDates::find()->where(['booking_item_id' => $bookingItemsModel->id])
                                                ->andWhere(['not in','id',$old_booking_dates])
                                                ->all();
        $new_dates = array();
        $new_adults = array();
        $new_children = array();
        $new_no_of_nights = $new_booked_dates[0]->no_of_nights;
        $new_beds_combination = ($new_booked_dates[0]->beds_combinations_id != null)?$new_booked_dates[0]->bedsCombinations->combination:'';
        $new_first_name = $new_booked_dates[0]->guest_first_name;
        $new_last_name = $new_booked_dates[0]->guest_last_name;
        $new_country = Countries::findOne([$new_booked_dates[0]->guest_country_id])->name;
        $new_cancellation = $new_booked_dates[0]->booking_cancellation;
        $new_confirmation_id = $new_booked_dates[0]->confirmation_id;
        $new_estimated_arrival_time = $new_booked_dates[0]->estimated_arrival_time_id;
        $new_person_no = array();
        $new_person_price = array();

        if(!empty($new_booked_dates))
        {
            foreach ($old_booking_dates as $oBdate) 
            {
                $oBdateModel = BookingDates::findOne(['id' => $oBdate]);
                /////////////// Removing Notification for overbooking of destination and unit (currently commented )///////////////
                Notifications::removeDestinationOverbooking($bookingItemsModel,$oBdateModel);
                Notifications::removeUnitOverbooking($bookingItemsModel,$oBdateModel);
                $oBdateModel->delete();
            }
            foreach ($new_booked_dates as $bookingDatesModel) 
            {
                array_push($new_dates, (string)$bookingDatesModel->date);
                array_push($new_adults, (string)$bookingDatesModel->no_of_adults);
                array_push($new_children, (string)$bookingDatesModel->no_of_children);
                array_push($new_person_no, (string)$bookingDatesModel->person_no);
                array_push($new_person_price, (string)$bookingDatesModel->person_price);

                Notifications::addDestinationOverbooking($bookingItemsModel,$bookingDatesModel);
                Notifications::addMultipleUnitsBooking($bookingItemsModel,$bookingDatesModel);
                Notifications::addUnitNotAssingned($bookingItemsModel,$bookingDatesModel);  
            }
            //Notifications::addRateNotAvailable($bookingItemsModel);
                
        }

        // echo "<pre>";
        // print_r($new_booked_dates);
        // exit();
        $this->isAttributeUpdated('No of Nights',$old_no_of_nights,$new_no_of_nights,$bookingItemsModel->id);
        $this->isAttributeUpdated('Beds Combination',$old_beds_combination,$new_beds_combination,$bookingItemsModel->id);
        $this->isAttributeUpdated('First Name',$old_first_name,$new_first_name,$bookingItemsModel->id);
        $this->isAttributeUpdated('Last Name',$old_last_name,$new_last_name,$bookingItemsModel->id);
        $this->isAttributeUpdated('Country',$old_country,$new_country,$bookingItemsModel->id);
        $this->isAttributeUpdated('Booking Cancellation',$old_cancellation,$new_cancellation,$bookingItemsModel->id);
        $this->isAttributeUpdated('Booking Confirmation',$old_confirmation_id,$new_confirmation_id,$bookingItemsModel->id);
        $this->isAttributeUpdated('Estimated Arrival Time',$old_estimated_arrival_time,$new_estimated_arrival_time,$bookingItemsModel->id);

        $this->isArrayAttributeUpdated('Booking Dates',$old_dates,$new_dates,$bookingItemsModel->id);
        $this->isArrayAttributeUpdated('No of Adults',$old_adults,$new_adults,$bookingItemsModel->id);
        $this->isArrayAttributeUpdated('No of Children',$old_children,$new_children,$bookingItemsModel->id);
        $this->isArrayAttributeUpdated('No of Persons',$old_person_no,$new_person_no,$bookingItemsModel->id);
        $this->isArrayAttributeUpdated('Person Price',$old_person_price,$new_person_price,$bookingItemsModel->id);



        /////////////////////// ADDING ENTRIES TO BOOKING DATE FINANCE CART TABLE ////////////////////////////
        $bookingsItemsModel = $this->findModel($_POST['booking_item_id']);
        $old_sysetem_charges = array();
        $new_sysetem_charges = array();
        $booking_system_charges = BookingDateFinances::find()->where(['booking_item_id' => $bookingsItemsModel->id,'entry_type' => BookingDateFinances::ENTRY_TYPE_SYSTEM])->all();
        foreach ($booking_system_charges as $value) 
        {
            $key = $value->date;
            $old_sysetem_charges[$key] = $value->price_description.' ('.$value->date.'): '.$value->quantity.'x'.$value->amount;
        }
        // echo "<pre>";
        // print_r($old_sysetem_charges);
        // exit();
        // die();
        BookingDateFinances::deleteAll(['booking_item_id' => $bookingsItemsModel->id,'entry_type' => BookingDateFinances::ENTRY_TYPE_SYSTEM,
                                        // 'type' => BookingDateFinances::TYPE_DEBIT
                                        ]);

        if(!empty($bookingsItemsModel))
        {
            // echo "<pre>";
            // print_r($bookingsItemsModel->bookingDatesSortedAndNotNull);
            // exit();
            foreach ($bookingsItemsModel->bookingDatesSortedAndNotNull as $bDate) 
            {
                // echo "<pre>";
                // print_r($bDate->bookingsUpsellItems);
                // exit();
                //$amount = $bDate->person_price;
                $booking_date_finance = new BookingDateFinances;
                $booking_date_finance->date = $bDate->date;
                $booking_date_finance->booking_item_id = $bDate->booking_item_id;
                $booking_date_finance->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;
                $booking_date_finance->price_description = $bDate->bookingItem->item->itemType->name;
                $booking_date_finance->quantity = 1;

                $booking_date_finance->amount = ($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate;
                $booking_date_finance->vat = (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate;
                $booking_date_finance->tax = (empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night;

                $temp_x = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)/(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                $booking_date_finance->x = $temp_x;

                $temp_y = $temp_x - ((empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night);

                $booking_date_finance->y = $temp_y;

                $temp_discount = 0;
                $temp_discount_percentage = 0;
                if(isset($bDate->bookingsDiscountCodes) && !empty($bDate->bookingsDiscountCodes))
                {
                    foreach ($bDate->bookingsDiscountCodes as  $bookingdc) 
                    {
                        $model = $bookingdc->discount;
                        if($model->applies_to == 0 or $model->applies_to == 1) 
                        {
                            switch ($model->pricing_type) 
                            {
                                case 0: // percentage
                                    $discount_amount = $this->calculateDiscountAmount($bDate->person_price,$model->amount);

                                    if($model->quantity_type == 1 || $model->quantity_type == 2)
                                    {
                                        $temp_discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                    }

                                    $temp_discount += $discount_amount;
                                    $temp_discount_percentage +=$model->amount;
                                    break;
                                case 1: // amount
                                    $discount_amount = $model->amount;

                                    if($model->quantity_type == 1 || $model->quantity_type == 2)
                                    {
                                        $discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                    }

                                    $temp_discount += $discount_amount;
                                    break;
                            }

                        }

                    }
                    $booking_date_finance->discount = $temp_discount_percentage; 
                    $booking_date_finance->discount_amount = round($temp_discount,2);
                }
                else
                {
                    if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) )
                    {
                        if($bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_DISCOUNT)
                        {
                            $booking_date_finance->discount = $bDate->travelPartner->comission;
                            $temp_discount = $temp_y*($bDate->travelPartner->comission/100);
                            $booking_date_finance->discount_amount = round($temp_discount,2);
                        }
                        if($bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                        {
                            $booking_date_finance->discount = $bDate->travelPartner->comission;

                            $temp_discount = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*($bDate->travelPartner->comission/100);
                            $booking_date_finance->discount_amount = round($temp_discount,2);
                        }
                    }
                    else
                    {
                        if(isset($bDate->offers_id) && !empty($bDate->offers_id) )
                        {
                            $booking_date_finance->discount = $bDate->offers->commission;
                            $temp_discount = $temp_y*($bDate->offers->commission/100);
                            $booking_date_finance->discount_amount = round($temp_discount,2);
                        }
                    }
                }

                if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) && $bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                {
                    $temp_subtotal1 = $temp_y;
                    $booking_date_finance->sub_total1 = $temp_subtotal1;

                    $temp_subtotal2 = $temp_subtotal1 + ((empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night);

                    $booking_date_finance->sub_total2 = $temp_subtotal2;

                    $temp_vat_amount = $temp_subtotal2*(( (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100);

                    $temp_new_vat_rate = (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate;
                    if($temp_new_vat_rate > 0)
                    {
                        $cal_1 = 100 + $temp_new_vat_rate;
                        $cal_2 = 100/$cal_1;
                        $cal_3 = round(($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate) * $cal_2;
                        $new_vat_amount = round(round(($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)  - $cal_3);

                        $booking_date_finance->vat_amount  = $new_vat_amount;
                    }
                    else
                    {
                        $booking_date_finance->vat_amount  = round($temp_vat_amount,2);
                    }
                    $booking_date_finance->final_total = round(($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate);
                }
                else
                {
                    $temp_subtotal1 = $temp_y - $temp_discount;
                    $booking_date_finance->sub_total1 = $temp_subtotal1;

                    $temp_subtotal2 = $temp_subtotal1 + ((empty($bDate->rate->lodging_tax_per_night) || ($bDate->rate->lodging_tax_per_night == NULL))?0:$bDate->rate->lodging_tax_per_night);

                    $booking_date_finance->sub_total2 = $temp_subtotal2;

                    $temp_vat_amount = $temp_subtotal2*(( (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100);
                    $booking_date_finance->vat_amount  = round($temp_vat_amount,2);
                    $booking_date_finance->final_total = round($temp_subtotal2 + $temp_vat_amount);
                }

                

                $booking_date_finance->type = BookingDateFinancesCart::TYPE_DEBIT;
                $booking_date_finance->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                $booking_date_finance->item_type = BookingDateFinancesCart::ITEM_TYPE_BOOKING;
                $booking_date_finance->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                if(!$booking_date_finance->save())
                {
                    echo "<pre>";
                    echo "booking date finance";
                    print_r($booking_date_finance->errors);
                    exit();
                }

                if(isset($bDate->bookingsUpsellItems))
                {
                    echo('$bDate->bookingsUpsellItems <br /> <pre>');
                    //print_r($bDate);
                    echo('</pre>');
                    foreach ($bDate->bookingsUpsellItems as $value) 
                    {
                        $booking_date_finance_upsell = new BookingDateFinances;
                        $booking_date_finance_upsell->date = $bDate->date;
                        $booking_date_finance_upsell->booking_item_id = $bDate->booking_item_id;
                        $booking_date_finance_upsell->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;
                        
                        $booking_date_finance_upsell->quantity = 1;

                        $temp_amount = 0;
                        $temp_vat = 0;
                        $temp_tax = 0;
                        if(isset($bDate->rate->ratesUpsells)  && !empty($bDate->rate->ratesUpsells))
                        {
                            foreach ($bDate->rate->ratesUpsells as $ratesUpsell) 
                            {
                                if($ratesUpsell->upsell_id == $value->upsell_id)
                                {
                                    $booking_date_finance_upsell->price_description = $ratesUpsell->upsell->name;

                                    $booking_date_finance_upsell->amount = $ratesUpsell->price;
                                    $temp_amount = $ratesUpsell->price;

                                    $booking_date_finance_upsell->vat = (empty($ratesUpsell->vat) || ($ratesUpsell->vat == NULL))?0:$ratesUpsell->vat;
                                    $temp_vat = (empty($ratesUpsell->vat) || ($ratesUpsell->vat == NULL))?0:$ratesUpsell->vat;

                                    $booking_date_finance_upsell->tax = (empty($ratesUpsell->tax) || ($ratesUpsell->tax == NULL))?0:$ratesUpsell->tax;
                                    $temp_tax = (empty($ratesUpsell->tax) || ($ratesUpsell->tax == NULL))?0:$ratesUpsell->tax;
                                }
                            }
                        }
                        

                        $temp_x = $temp_amount/(1+($temp_vat/100));
                        $booking_date_finance_upsell->x = $temp_x;

                        $temp_y = $temp_x - $temp_tax;
                        $booking_date_finance_upsell->y = $temp_y;

                        $temp_discount = 0;
                        $temp_discount_percentage = 0;
                        if(isset($bDate->bookingsDiscountCodes) && !empty($bDate->bookingsDiscountCodes))
                        {
                            foreach ($bDate->bookingsDiscountCodes as  $bookingdc) 
                            {
                                $model = $bookingdc->discount;
                                if($model->applies_to == 1 or $model->applies_to == 2) 
                                {
                                    switch ($model->pricing_type) 
                                    {
                                        case 0: // percentage
                                            $discount_amount = $this->calculateDiscountAmount($temp_amount,$model->amount);

                                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                                            {
                                                $temp_discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                            }

                                            $temp_discount += $discount_amount;
                                            $temp_discount_percentage +=$model->amount;
                                            break;
                                        case 1: // amount
                                            $discount_amount = $model->amount;

                                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                                            {
                                                $discount_amount = $discount_amount * ($bookingdc->extra_bed_quantity);
                                            }

                                            $temp_discount += $discount_amount;
                                            break;
                                    }

                                }

                            }
                            $booking_date_finance_upsell->discount = $temp_discount_percentage; 
                            $booking_date_finance_upsell->discount_amount = $temp_discount;
                        }
                        else
                        {
                            if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) )
                            {
                                // $booking_date_finance_upsell_credit = new BookingDateFinances;
                                // $booking_date_finance_upsell_credit->date = $bDate->date;
                                // $booking_date_finance_upsell_credit->booking_item_id = $bDate->booking_item_id;
                                // $booking_date_finance_upsell_credit->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;
                                
                                // $booking_date_finance_upsell_credit->quantity = 1;
                                // $booking_date_finance_upsell_credit->price_description = $bDate->travelPartner->travelPartner->company_name.' - '.$bDate->travelPartner->comission.' Discount';
                                if($bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                                {
                                    $booking_date_finance_upsell->discount = $bDate->travelPartner->comission;
                                    // $temp_discount = $temp_y*($bDate->travelPartner->comission/100);

                                    $temp_discount = ($temp_amount)*($bDate->travelPartner->comission/100);
                                    $booking_date_finance_upsell->discount_amount = round($temp_discount,2);
                                }
                                else
                                {
                                    $booking_date_finance_upsell->discount = $bDate->travelPartner->comission;
                                    $temp_discount = $temp_y*($bDate->travelPartner->comission/100);
                                    $booking_date_finance_upsell->discount_amount = round($temp_discount,2);
                                }
                                    

                                // $booking_date_finance_upsell_credit->final_total = round($temp_discount_credit,2);
                                // $booking_date_finance_upsell_credit->type = BookingDateFinances::TYPE_CREDIT;
                                // $booking_date_finance_upsell_credit->show_receipt = BookingDateFinances::SHOW_RECEIPT;
                                // //$booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                                // $booking_date_finance_upsell_credit->entry_type = BookingDateFinances::ENTRY_TYPE_SYSTEM;
                                // if(!$booking_date_finance_upsell_credit->save())
                                // {
                                //     echo "<pre>";
                                //     print_r($booking_date_finance_upsell_credit->errors);
                                //     exit();
                                // }
                            }
                            else
                            {
                                if(isset($bDate->offers_id) && !empty($bDate->offers_id) )
                                {
                                    // $booking_date_finance_upsell_credit = new BookingDateFinances;
                                    // $booking_date_finance_upsell_credit->date = $bDate->date;
                                    // $booking_date_finance_upsell_credit->booking_item_id = $bDate->booking_item_id;
                                    // $booking_date_finance_upsell_credit->booking_group_id =$bDate->bookingItem->bookingItemGroups->bookingGroup->id;

                                    // $booking_date_finance_upsell_credit->price_description = $bDate->offers->name.' - '.$bDate->offers->commission.' Discount';

                                    $booking_date_finance_upsell->discount = $bDate->offers->commission;
                                    $temp_discount = $temp_y*($bDate->offers->commission/100);
                                    $booking_date_finance_upsell->discount_amount = round($temp_discount,2);

                                    // $booking_date_finance_upsell_credit->final_total = round($temp_discount_credit,2);
                                    // $booking_date_finance_upsell_credit->type = BookingDateFinances::TYPE_CREDIT;
                                    // $booking_date_finance_upsell_credit->show_receipt = BookingDateFinances::SHOW_RECEIPT;
                                    // //$booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                                    // $booking_date_finance_upsell_credit->entry_type = BookingDateFinances::ENTRY_TYPE_SYSTEM;
                                    // if(!$booking_date_finance_upsell_credit->save())
                                    // {
                                    //     echo "<pre>";
                                    //     print_r($booking_date_finance_upsell_credit->errors);
                                    //     exit();
                                    // }
                                }
                            }
                        }

                        // if($temp_discount_percentage == 0)
                        // {
                        //     $booking_date_finance->discount = 0;
                        // }
                        // if($temp_discount == 0)
                        // {
                        //     $booking_date_finance->discount_amount = 0;
                        // }
                        if(isset($bDate->travel_partner_id) && !empty($bDate->travel_partner_id) && $bDate->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                        {
                            $temp_subtotal1 = $temp_y - $temp_discount;
                            $booking_date_finance_upsell->sub_total1 = $temp_subtotal1;

                            $temp_subtotal2 = $temp_subtotal1 + $temp_tax;

                            $booking_date_finance_upsell->sub_total2 = $temp_subtotal2;

                            $temp_vat_amount = $temp_subtotal2*(( (empty($bDate->rate->vat_rate) || ($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100);
                            // $booking_date_finance->vat_amount  = round($temp_vat_amount,2);
                            // formula for new vat amount //
                            $temp_new_vat_rate = (empty($temp_vat) || ($temp_vat == NULL))?0:$temp_vat;
                            if($temp_new_vat_rate > 0)
                            {
                                $cal_1 = 100 + $temp_new_vat_rate;
                                $cal_2 = 100/$cal_1;
                                $cal_3 = round($temp_amount) * $cal_2;
                                $new_vat_amount = round(round($temp_amount)  - $cal_3);

                                $booking_date_finance_upsell->vat_amount  = $new_vat_amount;
                            }
                            // else
                            // {
                            //     $booking_date_finance->vat_amount  = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                            // }
                            


                            // end 
                            // $booking_date_finance->vat_amount  = (($bDate->custom_rate == NULL)?$bDate->person_price:$bDate->custom_rate)*(1 + ((empty($bDate->rate->vat_rate) || (($bDate->rate->vat_rate == NULL))?0:$bDate->rate->vat_rate)/100));
                            $booking_date_finance_upsell->final_total = round($temp_amount);
                        }
                        else
                        {
                            $temp_subtotal1 = $temp_y - $temp_discount;
                            $booking_date_finance_upsell->sub_total1 = $temp_subtotal1;

                            $temp_subtotal2 = $temp_subtotal1 + $temp_tax;
                            $booking_date_finance_upsell->sub_total2 = $temp_subtotal2;

                            $temp_vat_amount = $temp_subtotal2*($temp_vat/100);
                            $booking_date_finance_upsell->vat_amount  = round($temp_vat_amount,2);
                            $booking_date_finance_upsell->final_total = round($temp_subtotal2 + $temp_vat_amount);
                        }
                            
 
                        $booking_date_finance_upsell->type = BookingDateFinancesCart::TYPE_DEBIT;
                        $booking_date_finance_upsell->show_receipt = BookingDateFinancesCart::SHOW_RECEIPT;
                        $booking_date_finance_upsell->item_type = BookingDateFinancesCart::ITEM_TYPE_UPSELL;
                        $booking_date_finance_upsell->entry_type = BookingDateFinancesCart::ENTRY_TYPE_SYSTEM;
                        if(!$booking_date_finance_upsell->save())
                        {
                            echo "<pre>";
                             echo "booking date finance upsell";
                            print_r($booking_date_finance_upsell->errors);
                            exit();
                        }
                    }
                }
            }

            if(!empty($bookingsItemsModel->bookingDateCharges))
            {
                $balance = 0;
                foreach ($bookingsItemsModel->bookingDateCharges as $charge) 
                {
                    // $key = $charge->date;
                    // $new_sysetem_charges[$key] = $charge->price_description.' ('.$charge->date.'): '.$charge->quantity.'x'.$charge->amount;
                    $balance += $charge->final_total;
                }
                $bookingsItemsModel->balance = $balance;
                $bookingsItemsModel->save();
            }

            $booking_system_charges = BookingDateFinances::find()->where(['booking_item_id' => $bookingsItemsModel->id,'entry_type' => BookingDateFinances::ENTRY_TYPE_SYSTEM])->all();
            foreach ($booking_system_charges as $value) 
            {
                $key = $value->date;
                $new_sysetem_charges[$key] = $value->price_description.' ('.$value->date.'): '.$value->quantity.'x'.$value->amount;
            }
            

            $this->isChargesAttributeUpdated('charge invoice',$old_sysetem_charges,$new_sysetem_charges,$bookingsItemsModel->id);
            // echo "<pre>";
            // print_r($old_sysetem_charges);
            // echo "<br>";
            // print_r($new_sysetem_charges);
            // exit();
        // die();
            // echo "<pre>";
            // print_r($bookingsItemsModel->bookingDateCharges);
            // exit();
        }
        else
        {
            echo "<pre>";
            print_r("no booking item");
            exit();
        }

        // $grand_total = 0;
        // $booking_date_for_total = BookingDates::find()->where(['booking_item_id' => $bookingItemsModel->id])->all();

        // foreach ($booking_date_for_total as  $value) 
        // {
        //    $grand_total += $value->total;
        // }

        // $bookingItemsModel->balance = round($grand_total);
        // $bookingItemsModel->save();

        $bookingGroupModel = $bookingItemsModel->bookingItemGroups->bookingGroup;

        $group_grand_total = 0;
        foreach ($bookingGroupModel->bookingItemGroups as $bookingGroupItem) 
        {
            if(!isset($bookingGroupItem->bookingItem))
            {
                echo "<pre>";
                print_r($bookingGroupItem);
                exit();
            }
            $group_grand_total += $bookingGroupItem->bookingItem->balance;
        }
        $bookingGroupModel->balance = $group_grand_total;
        $bookingGroupModel->save();

        
        /////////////////// Managing Charges and Payment ///////////////////////////

        // BookingDateFinances::deleteAll(['booking_item_id' => $_POST['booking_item_id']]);
        $bookingItem = $this->findModel($_POST['booking_item_id']);
        // if(isset($_POST['charges_data']))
        // {
        //     echo "<pre>";
        //     print_r($_POST['charges_data']);
        //     exit();
        // }
        // else
        // {
        //     echo "charges_data dont";
        //     exit();
        // }

        ///////////////// UPDTATE THIS THING IS NOW BEING DONE BY AJAX ////////////////////
        ///////////////// GETTING CHARGES AND PAYMENTS DATA ///////////////////////////////
        ///////////////// COMMENTED WHILE WE FINALIZE HOW WE UPDATE  //////////////////////
        if(isset($_POST['charges_data']))
        {
            // echo "here";

            $charges = $_POST['charges_data'];
            // echo "<pre>";
            // print_r($charges);
            // exit();
            foreach ($charges as $charge) 
            {
                $temp = isset($charge['price'])?str_replace('.', '', str_replace("_", "", $charge['price'])):0;

                if(isset($charge['price']) && $temp !=0)
                {
                    if($charge['id'] == 0)
                    {
                        $new_debit = new BookingDateFinances;
                        $old_debit_log = '';
                    }
                    else
                    {
                        $new_debit = BookingDateFinances::findOne(['id' => $charge['id']]);
                        $old_debit_log = $new_debit->price_description.' ('.$new_debit->date.'): '.$new_debit->quantity.'x'.$new_debit->amount;
                    }

                    //$new_debit = new BookingDateFinances;
                    $new_debit->booking_item_id = $bookingItem->id;
                    $new_debit->booking_group_id = $bookingItem->bookingItemGroups->bookingGroup->id;
                    $new_debit->type = BookingDateFinances::TYPE_DEBIT;
                    $new_debit->entry_type = BookingDateFinances::ENTRY_TYPE_USER;
                    $new_debit->item_type = isset($charge['item'])?$charge['item']: BookingDateFinances::ITEM_TYPE_CUSTOM;
                    $new_debit->show_receipt = ($charge['receipt']=="true")?1:0;
                    $new_debit->date = isset($charge['date'])?date('Y-m-d',strtotime($charge['date'])):"";
                    $new_debit->price_description = isset($charge['description'])?$charge['description']:"";
                    $new_debit->quantity = isset($charge['quantity'])?$charge['quantity']:"1";

                    $new_debit->amount = isset($charge['price'])?str_replace('.', '', str_replace("_", "", $charge['price'])):0;

                    $new_debit->vat = isset($charge['vat'])?str_replace('_', '', str_replace(",", ".", $charge['vat'])) :0;

                    $new_debit->tax = isset($charge['tax'])?str_replace('.', '', str_replace("_", "", $charge['tax'])):0;
                    $new_debit->tax = (double)$new_debit->tax;
                    $new_debit->vat_amount = (double)(isset($charge['vat_amount'])?str_replace(",", ".", $charge['vat_amount']):0);

                    $new_debit->discount_amount = isset($charge['discount_amount'])?str_replace(",",".",str_replace('.', '', str_replace("_", "", $charge['discount_amount']))):0;

                    $new_debit->final_total = isset($charge['final_total'])?str_replace('.', '', str_replace("_", "", $charge['final_total'])):0;
                    $new_debit->final_total = (double)$new_debit->final_total;

                    $new_debit->discount_disable = (integer) $charge['discount_disable'];
                    if(!$new_debit->save())
                    {
                        echo "<pre>";
                        print_r($new_debit->errors);
                        exit();
                    }
                    $new_debit_log = $new_debit->price_description.' ('.$new_debit->date.'): '.$new_debit->quantity.'x'.$new_debit->amount;
                    $this->isAttributeUpdated('charge invoice',$old_debit_log,$new_debit_log,$bookingItem->id);
                }            
            }
        }

        if(isset($_POST['payments_data']))
        {
            $payments = $_POST['payments_data'];
            foreach ($payments as $payment) 
            {
                $temp = isset($payment['total'])?$payment['total']:0;
                //$description=?$payment['total']:0;
                if(isset($payment['total']) && $temp !=0 && (isset($payment['description'])&& !empty($payment['description'])) )
                {
                    if($payment['id'] == 0)
                    {
                        $new_credit = new BookingDateFinances;
                        $old_credit_log = '';
                    }
                    else
                    {
                        $new_credit = BookingDateFinances::findOne(['id' => $payment['id']]);
                        $old_credit_log = $new_credit->price_description.' ('.$new_credit->date.'): '.$new_credit->final_total;
                    }
                    //$new_credit = new BookingDateFinances;
                    $new_credit->booking_item_id = $bookingItem->id;
                    $new_credit->booking_group_id = $bookingItem->bookingItemGroups->bookingGroup->id;
                    $new_credit->type = BookingDateFinances::TYPE_CREDIT;
                    $new_credit->show_receipt = ($payment['receipt']=="true")?1:0;
                    $new_credit->date = isset($payment['date'])?date('Y-m-d',strtotime($payment['date'])):"";
                    $new_credit->price_description = isset($payment['description'])?$payment['description']:"";
                    // $new_credit->quantity = isset($charge['quantity'])?$charge['quantity']:"1";
                    // $new_credit->amount = isset($charge['price'])?$charge['price']:"";
                    // $new_credit->vat = isset($charge['vat'])?$charge['vat']:"";
                    if(isset($payment['payment_type']) && ($payment['payment_type'] == 'true') )
                    {
                        $new_credit->payment_type = BookingDateFinances::PAYMETN_TYPE_DEPOSIT;
                        $new_credit->deposit_percent_age = $payment['deposit_percentage'];
                    }

                    $new_credit->amount = isset($payment['total'])?$payment['total']:0;
                    $new_credit->final_total = isset($payment['total'])?str_replace('.', '',$payment['total']):0;

                    if(!$new_credit->save())
                    {
                        echo "<pre>";
                        print_r($new_credit->errors);
                        exit();
                    }
                    $new_credit_log = $new_credit->price_description.' ('.$new_credit->date.'): '.$new_credit->final_total;
                    $this->isAttributeUpdated('payment invoice',$old_credit_log,$new_credit_log,$bookingItem->id);
                }
            }
        }

        //////////////////// UPDATING BALANCE OF BOOKING ITEM /////////////
        if(!empty($bookingItem->bookingDateCharges))
        {
            $balance = 0;
            foreach ($bookingItem->bookingDateCharges as $charge) 
            {
                $balance += $charge->final_total;
            }
            $bookingItem->balance = $balance;
            $bookingItem->save();
        }


            $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

            $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                      ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                      ->sum('final_total');
            $net_total = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                      ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                      ->sum('amount');
            $discount_amount = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                      ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                      ->sum('discount_amount');

            $total_debit = empty($total_debit)?0:$total_debit;
            $total_credit =  empty($total_credit)?0:$total_credit;  
            $discount_amount = empty($discount_amount)?0:$discount_amount;

            $balance_due = $total_debit - $total_credit;
            $this->isAttributeUpdated('Balance',$old_balance,$balance_due,$bookingItemsModel->id);
            // echo "<pre>";
            // print_r($balance_due);
            // exit();
            $bookingItem->balance = $balance_due;
            $bookingItem->paid_amount = $total_credit;

            $bookingItem->gross_total = $total_debit;
            $bookingItem->averaged_total = round($total_debit/$bookingItem->no_of_booking_days);
            $bookingItem->net_total = $total_debit - round($discount_amount);

            $bookingItem->save();


            $orgBookingGroupModel = $bookingItem->bookingItemGroups->bookingGroup;
            $group_balance = 0;
            foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
            {
                $group_balance += $group_item->bookingItem->balance;
            }
            $orgBookingGroupModel->balance = $group_balance;
            $orgBookingGroupModel->save();

            if($balance_due == 0 || $balance_due < 0 )
            {
                $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                if(empty($booking_status_paid))
                {
                    $booking_status_paid = new BookingStatuses();
                    $booking_status_paid->label = 'Paid';
                    $booking_status_paid->background_color = '#3c1efa';
                    $booking_status_paid->text_color = '#ffffff';
                    $booking_status_paid->save();
                }
                if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                {
                    if($bookingItem->status->label == 'Paid')
                    {
                        if($balance_due != 0)
                        {
                            $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                            if(empty($booking_status_exception))
                            {
                                $booking_status_exception = new BookingStatuses();
                                $booking_status_exception->label = 'Exception';
                                $booking_status_exception->background_color = '#ff6161';
                                $booking_status_exception->text_color = '#ffffff';
                                $booking_status_exception->save();
                            }
                            $bookingItem->status_id = $booking_status_exception->id;
                            $bookingItem->save();
                        }
                        
                    }
                    else
                    {
                        if($bookingItem->status->label == 'Exception')
                        {
                            if($balance_due == 0)
                            {
                                $bookingItem->status_id = $booking_status_paid->id;
                                $bookingItem->save();
                            }
                        }
                        else
                        {
                            $bookingItem->status_id = $booking_status_paid->id;
                            $bookingItem->save();
                        }
                        
                    }
                }
                else
                {
                    $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                    if(empty($booking_status_paid))
                    {
                        $booking_status_paid = new BookingStatuses();
                        $booking_status_paid->label = 'Paid';
                        $booking_status_paid->background_color = '#3c1efa';
                        $booking_status_paid->text_color = '#ffffff';
                        $booking_status_paid->save();
                    }
                    $bookingItem->status_id = $booking_status_paid->id;
                    $bookingItem->save();
                }
            }
            if($balance_due > 0 )
            {
                if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                {
                    if($bookingItem->status->label == 'Paid')
                    {
                        $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                        if(empty($booking_status_exception))
                        {
                            $booking_status_exception = new BookingStatuses();
                            $booking_status_exception->label = 'Exception';
                            $booking_status_exception->background_color = '#ff6161';
                            $booking_status_exception->text_color = '#ffffff';
                            $booking_status_exception->save();
                        }
                        $bookingItem->status_id = $booking_status_exception->id;
                        $bookingItem->save();
                    }
                }
            }
            $bookingItem->save(); 

            foreach ($bookingItem->bookingDatesSortedAndNotNull as $bDate) 
            {
                $bDate->status_id = $bookingItem->status_id;
                $bDate->save();
            } 

        // $first_booking = BookingDates::findOne(['id' => $_POST['booking_item_id']]);
        // // echo "<pre>";
        // // print_r($first_booking);
        // // exit();
        // $first_booking_date = $first_booking->date;
        // echo $first_booking_date;
        Yii::$app->session->setFlash('success', 'Item updated successfully.');
        
    }

    public function actionCopy()
    {
        $bookingItemsModel = $this->findModel($_POST['booking_item_id']);

        $new_booking_item = new BookingsItems();

        $new_booking_item->provider_id = $bookingItemsModel->provider_id;
        //$new_booking_item->booking_number = $last_booking_number+1;
        $new_booking_item->temp_flag = $bookingItemsModel->temp_flag;
        $new_booking_item->item_id = $_POST['item_id'];
        if($bookingItemsModel->item_id == $_POST['item_id'])
        {
            $new_booking_item->item_name_id = $bookingItemsModel->item_name_id;
        }
        else
        {
            $arrival_date = $new_booking_item->arrival_date;
            $departure_date = $new_booking_item->departure_date;

            $arrival_date = str_replace('/', '-', $arrival_date);
            $arrival_date = date('Y-m-d',strtotime($arrival_date));

            $departure_date = str_replace('/', '-', $departure_date);
            $departure_date = date('Y-m-d',strtotime($departure_date));

            $arrival_day = date('N',strtotime($arrival_date));
            $departure_day = date('N',strtotime($departure_date));

            $date1=date_create($departure_date);
            $date2=date_create($arrival_date);
            $diff=date_diff($date1,$date2);
            $difference =  $diff->format("%a");

            $available_item_names = BookingsItems::checkAvailableItemNamesIncludedSaved($_POST['item_id'],$new_booking_item->arrival_date, $difference, $new_booking_item->id);
            if(empty($available_item_names))
            {
                $new_booking_item->item_name_id = null;
            }
            else
            {
                $new_booking_item->item_name_id = $available_item_names[0];
            }
        }
        // $new_booking_item->item_name_id = null;
        $new_booking_item->arrival_date = $bookingItemsModel->arrival_date;
        $new_booking_item->departure_date = $bookingItemsModel->departure_date;
        $new_booking_item->flag_id = $bookingItemsModel->flag_id;
        $new_booking_item->status_id = $bookingItemsModel->status_id;
        $new_booking_item->travel_partner_id = $bookingItemsModel->travel_partner_id;
        $new_booking_item->offers_id = $bookingItemsModel->offers_id;
        $new_booking_item->estimated_arrival_time_id = $bookingItemsModel->estimated_arrival_time_id;
        $new_booking_item->rate_conditions = $bookingItemsModel->rate_conditions;
        $new_booking_item->pricing_type = $bookingItemsModel->pricing_type;
        $new_booking_item->created_by = $bookingItemsModel->created_by;
        $new_booking_item->deleted_at = $bookingItemsModel->deleted_at;
        $new_booking_item->balance = $bookingItemsModel->balance;

        if(!$new_booking_item->save())
        {
            echo "<pre>";
           // print_r($new_booking_item->errors);
        }
        
        $new_booking_item_group = new BookingItemGroup();
        $new_booking_item_group->booking_group_id = $bookingItemsModel->bookingItemGroups->booking_group_id;
        $new_booking_item_group->booking_item_id = $new_booking_item->id;

        // $new_booking_item_group->save();
        if(!$new_booking_item_group->save())
        {
            echo "<pre>";
            print_r($new_booking_item_group->errors);
        }
        //updating group balance //
        $orgBookingGroupModel = $bookingItemsModel->bookingItemGroups->bookingGroup;

        $group_balance = 0;
        foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
        {
            $group_balance += $group_item->bookingItem->balance;
        }
        $orgBookingGroupModel->balance = $group_balance;
        $orgBookingGroupModel->save();
        // if($bookingItemsModel->item_id == $_POST['item_id'] )
        // {
            // echo "<pre>";
            // print_r($bookingItemsModel->bookingDatesSortedAndNotNull->BookingsUpsellItems);
            // exit();
        foreach ($bookingItemsModel->bookingDatesSortedAndNotNull as $value) 
        {
           
            $new_date = new BookingDates();


            $new_date->attributes = $value->attributes;
            $new_date->booking_item_id = $new_booking_item->id;
            $new_date->item_id =  $new_booking_item->item_id;
            $new_date->item_name_id = $new_booking_item->item_name_id;
            // $new_date->save();
            // echo "<pre>";
            // print_r($new_date);
            // exit();

            if(!$new_date->save())
            {
                echo "<pre>";
                print_r($new_date->errors);
            }

            if(!empty($value->bookingsDiscountCodes))
            {
                foreach ($value->bookingsDiscountCodes as $value1) 
                {
                    // $arr = explode(':', $value);
                    // $discount_id = $arr[0];
                    // $discount_code_extra_bed_quantity = $arr[1];

                    // $discount_code_model = DiscountCode::findOne(['id' => $discount_id]);

                    $obj = new BookingsDiscountCodes();
                    $obj->discount_id = $value1->discount_id;
                    $obj->price = $value1->price;
                    $obj->booking_date_id = $new_date->id;
                    $obj->extra_bed_quantity = $value1->extra_bed_quantity;

                    if(!$obj->save())
                    {
                        echo "<pre>";
                        print_r($obj->errors);
                        exit();
                    }
                }
            }

            // echo "<pre>";
            // print_r($value->bookingsUpsellItems);
            // exit();
            if(!empty($value->bookingsUpsellItems))
            {

                foreach ($value->bookingsUpsellItems as $value2) 
                {
                    // $arr = explode(':', $value);
                    // $upsell_id = $arr[0];
                    // $upsell_extra_bed_quantity = $arr[1];

                    // $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $bookingItem['rate_id'], 'upsell_id' => $upsell_id]);

                    $obj = new BookingsUpsellItems();
                    $obj->upsell_id = $value2->upsell_id;
                    $obj->price = $value2->price;
                    $obj->booking_date_id = $new_date->id;
                    $obj->extra_bed_quantity = $value2->extra_bed_quantity;

                    if(!$obj->save())
                    {
                        echo "<pre>";
                        print_r($obj->errors);
                        exit();
                    }
                }
            }

            if(!empty($value->bookingSpecialRequests))
            {
                foreach ($value->bookingSpecialRequests as  $value3) 
                {
                    $model = new BookingSpecialRequests();
                    $model->booking_date_id = $new_date->id;
                    $model->request_id = $value3->request_id;
                    if(!$model->save())
                    {
                        echo "<pre>";
                        print_r($model->errors);
                        exit();
                    }
                }
            }

        }

        $finances = BookingDateFinances::find()->where(['booking_item_id' => $bookingItemsModel->id])
                                              ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT])
                                              ->andWhere(['entry_type' => BookingDateFinances::ENTRY_TYPE_SYSTEM])
                                              ->all();

        if(!empty($finances))
        {
            foreach ($finances as $finance) 
            {
                $new_finance = new BookingDateFinances;
                if(!$new_finance->save(false))
                {
                    echo "<pre>";
                    print_r($new_finance->errors);
                    exit();
                }
                $id = $new_finance->id;
                $new_finance->attributes = $finance->attributes;
                $new_finance->id = $id;
                $new_finance->booking_item_id = $new_booking_item->id;
                if(!$new_finance->save())
                {
                    echo "<pre>";
                    print_r($new_finance->errors);
                    exit();
                } 
            }
        }
        // }
        /*else
        {
            $arrival_date = $new_booking_item->arrival_date;
            $departure_date = $new_booking_item->departure_date;

            $arrival_date = str_replace('/', '-', $arrival_date);
            $arrival_date = date('Y-m-d',strtotime($arrival_date));

            $departure_date = str_replace('/', '-', $departure_date);
            $departure_date = date('Y-m-d',strtotime($departure_date));

            $arrival_day = date('N',strtotime($arrival_date));
            $departure_day = date('N',strtotime($departure_date));

            $date1=date_create($departure_date);
            $date2=date_create($arrival_date);
            $diff=date_diff($date1,$date2);
            $difference =  $diff->format("%a");

            $available_rates = BookingsItems::checkAvailableRates($new_booking_item->item_id, $new_booking_item->arrival_date, $difference, $new_booking_item->rate_conditions );
            // echo "<pre>";
            // print_r($available_rates['availableRatesIds']);
            // exit();
            if(!empty($available_rates['availableRatesIds']))
            {
                

                $selected_rate = null;
                foreach ($available_rates['availableRatesIds'] as $key => $rate) 
                {
                    $selected_rate = $rate;
                    break;
                }
                $exploded_rates = explode(',', $selected_rate);
               

                foreach ($exploded_rates as $key => $rate) 
                {
                    $selected_rate = $rate;
                    break;
                }

                foreach ($bookingItemsModel->bookingDatesSortedAndNotNull as $value) 
                {
                    $new_date = new BookingDates();

                    $new_date->booking_item_id = $new_booking_item->id;
                    $new_date->date = $value->date;
                    $new_date->item_id = $new_booking_item->item_id;
                    $new_date->item_name_id = $new_booking_item->item_name_id;
                    $new_date->rate_id = $selected_rate;
                    $new_date->quantity = $value->quantity;
                    $new_date->no_of_nights = $value->no_of_nights;
                    $new_date->estimated_arrival_time_id = $new_booking_item->estimated_arrival_time_id;
                    $new_date->beds_combinations_id = $value->beds_combinations_id;
                    $new_date->user_id = $value->user_id;
                    $new_date->guest_first_name = $value->guest_first_name;
                    $new_date->guest_last_name = $value->guest_last_name;
                    $new_date->guest_country_id = $value->guest_country_id;
                    $new_date->confirmation_id = $value->confirmation_id;
                    $new_date->booking_cancellation = $value->booking_cancellation;
                    $new_date->flag_id = $value->flag_id;
                    $new_date->status_id = $value->status_id;
                    $new_date->no_of_children = null;
                    $new_date->no_of_adults= null;
                    $new_date->person_no = null;
                    $new_date->person_price = null;
                    $new_date->total = 0;
                    $new_date->travel_partner_id = $value->travel_partner_id;
                    $new_date->offers_id =$value->offers_id;
                    // $new_date->save();

                    if(!$new_date->save())
                    {
                        echo "<pre>";
                        print_r($new_date->errors);
                    }
                }
           }
                
        }*/

        $return_url = Url::to(['/bookings/update','id' => $new_booking_item->id]);
        echo $return_url;

    }

    public function actionUpdateGroup($id,$pageNo =null)
    {
        $bookingGroupModel = BookingGroup::findOne(['id' => $id]);

        if($bookingGroupModel->load(Yii::$app->request->post())) 
        {
            // echo "<pre>";
            // print_r($_POST);
            // exit();

            // ########### checking which radio button user has seleted ##########//
            if(isset($_POST['selected_user']) && $_POST['selected_user'] ==  'guest_user')
            {
                $bookingGuestModel = ($bookingGroupModel->groupGuestUser == NULL)?new BookingGuests():$bookingGroupModel->groupGuestUser;
                
                $bookingGuestModel->first_name = isset($_POST['guest_first_name'])?$_POST['guest_first_name']:NULL;
                $bookingGuestModel->last_name = isset($_POST['guest_last_name'])?$_POST['guest_last_name']:NULL;
                $bookingGuestModel->country_id = isset($_POST['guest_country'])?$_POST['guest_country']:NULL;
                $bookingGuestModel->save();

                $bookingGroupModel->group_guest_user_id = $bookingGuestModel->id;
                $bookingGroupModel->group_user_id = NULL;
                $bookingGroupModel->update();
            }
            else
            {
                if(!empty($bookingGroupModel->group_user_id))
                {
                    if(!empty($bookingGroupModel->group_guest_user_id))
                    {
                        $bookingGroupModel->groupGuestUser->delete();
                    }
                    $bookingGroupModel->group_guest_user_id = NULL;
                    $bookingGroupModel->save();
                }
            }

            return $this->redirect(['update-group',
                    'id' => $id,
                    'pageNo' => $pageNo,
                ]);
        }
        else 
        {
            return $this->render('update_group', [
                    'group_id' => $id,
                    'bookingGroupModel' => $bookingGroupModel,
                    'pageNo' => $pageNo,
                ]);
        }
    }

    public function actionUpdateIndividualItems()
    {
        $bookedItems = $_POST['BookedItems'];
        $group_id = '';

        // echo "<pre>";
        //     print_r($bookedItems);
        //     exit;
        $grand_group_total = 0;
        foreach ($bookedItems as $key => $value) 
        {

            $bookingItemsModel = BookingsItems::findOne(['id' => $key]);
            $bookingGroupModel = BookingItemGroup::findOne(['booking_item_id' => $key]);
            $group_id = $bookingGroupModel->booking_group_id;

            $bookedDates = $bookingItemsModel->bookingDatesSortedAndNotNull;

            $bookingItemsModel->status_id = $value['status_id'];
            $bookingItemsModel->housekeeping_status_id = $value['housekeeping_status_id'];
            $bookingItemsModel->estimated_arrival_time_id = $value['estimated_arrival_time_id'];
           // $bookingItemsModel->beds_combinations_id = !empty($value['beds_combinations_id'])? $value['beds_combinations_id']:NULL;
            $bookingItemsModel->travel_partner_id = isset($value['travel_partner_id'])?$value['travel_partner_id']:NULL;
            $bookingItemsModel->offers_id = isset($value['offers_id'])?$value['offers_id']:NULL;
            //$bookingItemsModel->booking_cancellation = $value['booking_cancellation'];
            $bookingItemsModel->flag_id = $value['flag_id'];
            $bookingItemsModel->voucher_no = $value['voucher_no'];
            $bookingItemsModel->reference_no = $value['reference_no'];
            $bookingItemsModel->comments = $value['comment'];

            //$bookingItemsModel->status_id = $value['status_id'];
            //$bookingItemsModel->status_id = $value['housekeeping_status_id'];
            //$bookingItemsModel->confirmation_id = $value['confirmation_id'];

            $bookingItemsModel->save();
            // echo "<pre>";
            // print_r($bookedItems);
            // exit;
            $grand_total = 0;
            foreach ($bookedDates as $date) 
            {
                // echo '<pre>';
                // print_r($value[$date->id]);
                // exit;
                if($bookingItemsModel->provider->pricing) // ************* Capacity Pricing ************ //
                {
                    $date->no_of_adults = $value[$date->id]['no_of_adults'];
                    $date->no_of_children = $value[$date->id]['no_of_children'];
                    if(isset($value[$date->id]['capacity_pricing']))
                    {
                        $temp_arr =  explode(':',$value[$date->id]['checkbox']);
                        $selected = $temp_arr[0];
                        $date->person_no = $selected;

                        $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $date->rate_id, 'person_no' => $selected]);

                        $custom_rate = str_replace('.', '', $value[$date->id]['capacity_pricing'][$selected-1]);
                        if($custom_rate == $capacityPricingModel->price)
                        {
                            $date->person_price = $capacityPricingModel->price;
                            $date->custom_rate = null;
                        }
                        else
                        {
                            $date->person_price = $capacityPricingModel->price;
                            $date->custom_rate = $custom_rate;
                        }

                        $date->person_price = $capacityPricingModel->price;
                        $has_record = 1;
                    }
                }
                else // ************* First/Additional ************ //
                {
                    if(isset($value[$date->id]['no_of_adults']) && isset($value[$date->id]['no_of_children']) && 
                        ($value[$date->id]['no_of_adults']!=0 || $value[$date->id]['no_of_children']!=0))
                    {
                        $date->no_of_adults = $value[$date->id]['no_of_adults'];
                        $date->no_of_children = $value[$date->id]['no_of_children'];
                        $has_record = 1;
                    }
                }

                if($has_record)
                {
                    $date->user_id = $value['user_id'];

                    if(empty($bookingItemsModel->user_id))
                    {   
                        //########### saving guest data ###########//

                        $date->guest_first_name = $value['guest_first_name'];
                        $date->guest_last_name = $value['guest_last_name'];
                        $date->guest_country_id = $value['guest_country_id'];

                        //########################################//
                    }
                    else
                    {
                        $date->guest_first_name = NULL;
                        $date->guest_last_name = NULL;
                        $date->guest_country_id = $value['guest_country_id'];
                    }

                    $date->estimated_arrival_time_id = $value['estimated_arrival_time_id'];
                    $date->beds_combinations_id = !empty($value['beds_combinations_id'])? $value['beds_combinations_id']:NULL;
                    $date->travel_partner_id = isset($value['travel_partner_id'])?$value['travel_partner_id']:NULL;
                    $date->voucher_no = $value['voucher_no'];
                    $date->reference_no = $value['reference_no'];
                    $date->offers_id = isset($value['offers_id'])?$value['offers_id']:NULL;
                    $date->booking_cancellation = $value['booking_cancellation'];
                    $date->flag_id = $value['flag_id'];
                    $date->status_id = $value['status_id'];
                    $date->housekeeping_status_id = $value['housekeeping_status_id'];
                    $date->confirmation_id = $value['confirmation_id'];
                    $date->price_json = str_replace("'", '"', $value[$date->id]['price_json']);

                    $arr = json_decode($date->price_json);
                    $date->total = $arr->orgPrice;

                    $grand_total = $grand_total + $arr->orgPrice;
                    if($date->save())
                    {
                        $date->discount_codes = isset($value[$date->id]['discount_codes'])?$value[$date->id]['discount_codes']:'';
                        $date->upsell_items = isset($value[$date->id]['upsell_items'])?$value[$date->id]['upsell_items']:'';
                        $date->special_requests = isset($value[$date->id]['special_requests'])?$value[$date->id]['special_requests']:'';

                        BookingsDiscountCodes::deleteAll(['booking_date_id' => $date->id]);
                        BookingsUpsellItems::deleteAll(['booking_date_id' => $date->id]);
                        BookingSpecialRequests::deleteAll(['booking_date_id' => $date->id]);
                        /////////////////// OLD CODE //////////////////////////////////////
                        // if(!empty($bookingItemsModel->discount_codes))
                        // {
                        //     foreach ($bookingItemsModel->discount_codes as $key => $value) 
                        //     {
                        //         if(isset($value['price']))
                        //         {
                        //             $discount_code_model = DiscountCode::findOne(['id' => $key]);

                        //             $obj = new BookingsDiscountCodes();
                        //             $obj->discount_id = $key;
                        //             $obj->price = $discount_code_model->amount;
                        //             $obj->bookings_items_id = $bookingItemsModel->id;

                        //             if($discount_code_model->quantity_type == 1 || $discount_code_model->quantity_type == 2)
                        //             {
                        //                 $obj->extra_bed_quantity = $value['bed_quantity'];
                        //             }

                        //             $obj->save();
                        //         }
                        //     }
                        // }
                        ////////////////////////////// NEW CODE ///////////////////////////////////
                        // echo "<pre>";
                        // print_r($date->discount_codes);
                        // exit();
                        if(!empty($date->discount_codes))
                        {
                            foreach ($date->discount_codes as $key => $value1) 
                            {
                                if(isset($value1['price']))
                                {
                                    $discount_code_model = DiscountCode::findOne(['id' => $key]);

                                    $obj = new BookingsDiscountCodes();
                                    $obj->discount_id = $key;
                                    $obj->price = $discount_code_model->amount;
                                    $obj->booking_date_id = $date->id;

                                    if($discount_code_model->quantity_type == 1 || $discount_code_model->quantity_type == 2)
                                    {
                                        $obj->extra_bed_quantity = isset($value1['bed_quantity'])?$value1['bed_quantity']:1;
                                    }

                                    $obj->save();
                                }
                            }
                        }

                        if(!empty($date->upsell_items))
                        {
                            foreach ($date->upsell_items as $key => $value2) 
                            {
                                if(isset($value2['price']))
                                {
                                    $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $date->rate_id, 'upsell_id' => $key]);

                                    $obj = new BookingsUpsellItems();
                                    $obj->upsell_id = $key;
                                    $obj->price = $ratesUpsellModel->price;
                                    $obj->booking_date_id = $date->id;

                                    if($ratesUpsellModel->upsell->type == 7)
                                    {
                                        $obj->extra_bed_quantity = isset($value2['bed_quantity'])?$value2['bed_quantity']:1;
                                    }

                                    $obj->save();
                                }
                            }
                        }

                        // if(!empty($date->special_requests))
                        // {
                        //     foreach ($date->special_requests as $key => $value) 
                        //     {
                        //         $model = new BookingSpecialRequests();
                        //         $model->booking_item_id = $bookingItemsModel->id;
                        //         $model->request_id = $value;
                        //         $model->save();
                        //     }
                        // }
                    }  
                }



            }

            $bookingItemsModel->balance = round($grand_total);
            $bookingItemsModel->save();
           // $bookingGroupModel->balance = $bookingGroupModel->balance
            $grand_group_total = $grand_group_total + round($grand_total);
        }

        $bookinGroup_total = BookingGroup::findOne(['id' => $group_id]);

        $grand_group_total = 0;
        foreach ($bookedItems as $key =>$bImodel) 
        {
            $bookingItemsModel = BookingsItems::findOne(['id' => $key]);
            $grand_group_total+= $bookingItemsModel->balance;
        }

        $bookinGroup_total->balance = $grand_group_total;
        $bookinGroup_total->save();

        // $bookinGroup_total->balance = $grand_group_total;
        // $bookinGroup_total->save();

        Yii::$app->session->setFlash('success', 'Items updated successfully.');

        return $this->redirect(['update-group',
                    'id' => $group_id,
                ]);
    }

    public function actionUpdateAll()
    {
        $groupSetting = $_POST['BookingGroup'];

        //  echo '<pre>';
        // print_r($_POST);
        // exit;
        
        $bookingGroupModel = BookingGroup::findOne(['id' => $groupSetting['group_id']]);

        // ########### checking which radio button user has seleted ##########//
        if(isset($groupSetting['selected_user']) && $groupSetting['selected_user'] ==  'guest_user')
        {
            $bookingGuestModel = ($bookingGroupModel->groupGuestUser == NULL)?new BookingGuests():$bookingGroupModel->groupGuestUser;
            
            $bookingGuestModel->first_name = isset($groupSetting['guest_first_name'])?$groupSetting['guest_first_name']:NULL;
            $bookingGuestModel->last_name = isset($groupSetting['guest_last_name'])?$groupSetting['guest_last_name']:NULL;
            $bookingGuestModel->country_id = isset($groupSetting['guest_country'])?$groupSetting['guest_country']:NULL;
            $bookingGuestModel->save();

            $bookingGroupModel->group_guest_user_id = $bookingGuestModel->id;
            $bookingGroupModel->group_user_id = NULL;
            $bookingGroupModel->update();
        }
        else
        {
            if(!empty($groupSetting['group_user_id']))
            {
                $bookingGroupModel->group_user_id = $groupSetting['group_user_id'];
                if(!empty($bookingGroupModel->group_guest_user_id))
                {
                    $bookingGroupModel->groupGuestUser->delete();
                }
                $bookingGroupModel->group_guest_user_id = NULL;
                $bookingGroupModel->save();
            }
        }
        
        $bookingGroupModel->group_name = $groupSetting['group_name'];
        $bookingGroupModel->confirmation_id = $groupSetting['confirmation_id'];
        $bookingGroupModel->cancellation_id = $groupSetting['cancellation_id'];
        $bookingGroupModel->flag_id = $groupSetting['flag_id'];
        $bookingGroupModel->status_id = $groupSetting['status_id'];
        $bookingGroupModel->travel_partner_id = isset($groupSetting['travel_partner_id'])?$groupSetting['travel_partner_id']:NULL;
        $bookingGroupModel->offers_id = isset($groupSetting['offers_id'])?$groupSetting['offers_id']:NULL;
        $bookingGroupModel->estimated_arrival_time_id = $groupSetting['estimated_arrival_time_id'];
        $bookingGroupModel->voucher_no = $groupSetting['voucher_no'];
        $bookingGroupModel->reference_no = $groupSetting['reference_no'];
        $bookingGroupModel->comments = $groupSetting['comments'];

        if(!$bookingGroupModel->save())
        {
            echo "<pre>";
            print_r($bookingGroupModel->errors);
            exit();
        }
        if($bookingGroupModel->save())
        {
            BookingGroupSpecialRequests::deleteAll(['booking_group_id' => $groupSetting['group_id']]);

            if(isset($groupSetting['special_requests']) && !empty($groupSetting['special_requests']))
            {
                foreach ($groupSetting['special_requests'] as $key => $value) 
                {
                    $model = new BookingGroupSpecialRequests();
                    $model->booking_group_id = $bookingGroupModel->id;
                    $model->special_request_id = $value;
                    $model->save();
                }
            }
        }

        if($groupSetting['type'] == 'update-group')
        {
            Yii::$app->session->setFlash('success', 'Group settings updated successfully.');
        }
        else
        {
            $bookedItemsIds = explode(',', $groupSetting['booked_items_ids']);
            
            foreach ($bookedItemsIds as $key => $value) 
            {
                $bookingItemsModel = BookingsItems::findOne(['id' => $value]);

                $bookingItemsModel->voucher_no = $groupSetting['voucher_no'];
                $bookingItemsModel->reference_no = $groupSetting['reference_no'];
                $bookingItemsModel->comments = $groupSetting['comments'];
                $bookingItemsModel->flag_id = $groupSetting['flag_id'];
                $bookingItemsModel->status_id = $groupSetting['status_id'];
                $bookingItemsModel->travel_partner_id = isset($groupSetting['travel_partner_id'])?$groupSetting['travel_partner_id']:NULL;
                $bookingItemsModel->offers_id = isset($groupSetting['offers_id'])?$groupSetting['offers_id']:NULL;
                $bookingItemsModel->save();
                $bookedDates = $bookingItemsModel->bookingDatesSortedAndNotNull;


            //     [flag_id] => 8
            // [status_id] => 4
            // [travel_partner_id] => 
            // [offers_id] => 
                // echo "<pre>";
                // print_r($bookedDates);
                // exit;

                foreach ($bookedDates as $date) 
                {
                    if(isset($groupSetting['selected_user']) && $groupSetting['selected_user'] ==  'guest_user')
                    {
                        $date->guest_first_name = $groupSetting['guest_first_name'];
                        $date->guest_last_name = $groupSetting['guest_last_name'];
                        $date->guest_country_id = $groupSetting['guest_country'];
                        $date->user_id = NULL;
                    }
                    else
                    {
                        $date->user_id = $groupSetting['group_user_id'];
                        $date->guest_first_name = NULL;
                        $date->guest_last_name = NULL;
                        $date->guest_country_id = $groupSetting['guest_country'];
                    }
                    $date->confirmation_id = $groupSetting['confirmation_id'];
                    $date->booking_cancellation = $groupSetting['cancellation_id'];
                    $date->flag_id = $groupSetting['flag_id'];
                    $date->status_id = $groupSetting['status_id'];
                    $date->travel_partner_id = isset($groupSetting['travel_partner_id'])?$groupSetting['travel_partner_id']:NULL;
                    $date->offers_id = isset($groupSetting['offers_id'])?$groupSetting['offers_id']:NULL;
                    $date->estimated_arrival_time_id = $groupSetting['estimated_arrival_time_id'];
                    $date->voucher_no = $groupSetting['voucher_no'];
                    $date->reference_no = $groupSetting['reference_no'];

                    if($date->save())
                    {
                        // ************** update prices on change discount type ************* //

                        if(!empty($date->travel_partner_id) || !empty($date->offers_id))
                        {
                            BookingsDiscountCodes::deleteAll(['booking_date_id' => $date->id]);
                        }

                        $this->updatePricesOnChangeDiscountType($date,$date->travel_partner_id,$date->offers_id);

                        BookingSpecialRequests::deleteAll(['booking_date_id' => $date->id]);

                        if(isset($groupSetting['special_requests']) && !empty($groupSetting['special_requests']))
                        {
                            $date->special_requests = $groupSetting['special_requests'];

                            foreach ($date->special_requests as $key => $value) 
                            {
                                $model = new BookingSpecialRequests();
                                $model->booking_date_id = $date->id;
                                $model->request_id = $value;
                                $model->save();
                            }
                        }
                    }
                }

                    
            }

            Yii::$app->session->setFlash('success', 'Group settings and Items settings updated successfully.');
        }

        return $this->redirect(['update-group',
                    'id' => $groupSetting['group_id'],
                ]);
    }

    /**
     * Deletes an existing Bookings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param null $filter_flag
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id,$filter_flag=null)
    {
        // echo "<pre>";
        // print_r(Yii::$app->request->queryParams);
        // exit();

        $model = $this->findModel($id);
        // Deleting Notifications //
        Notifications::deleteAll(['AND', 'booking_item_id = :booking_item_id', ['NOT IN', 'notification_type', [Notifications::UNIT_OVERBOOKING,Notifications::DESTINATION_OVERBOOKING]]], [':booking_item_id' => $model->id]);

        foreach ($model->bookingDatesSortedAndNotNull as $booking_date_obj) 
        {
            Notifications::removeDestinationOverbooking($model,$booking_date_obj);
            Notifications::removeUnitOverbooking($model,$booking_date_obj);
        }
        if(isset($model->bookingItemGroups))
        {
            $this->UnlinkItemFromGroup($model->id);
        }
        $model->deleted_at = strtotime(date('Y-m-d H:i:s'));
        $model->update();
        if(isset(Yii::$app->request->queryParams['pageNo']))
        {
            $pageNo = Yii::$app->request->queryParams['pageNo'];
        }
        else
        {
            $pageNo = 1;
        }

        return $this->redirect(['index','filter' => $filter_flag,'pageNo' => $pageNo]);
    }

    public function actionDeleteBookingItemFromGroup($id)
    {
        $model = $this->findModel($id);
        $model->deleted_at = strtotime(date('Y-m-d H:i:s'));
        $model->update();
        // foreach ($model->bookingDatesSortedAndNotNull as $booking_date_obj) 
        // {
        //     Notifications::removeDestinationOverbooking($model,$booking_date_obj);
        //     Notifications::removeUnitOverbooking($model,$booking_date_obj);
        // }

    }

    public function actionRestore($id)
    {
        $model = $this->findModel($id);
        $model->deleted_at = 0;
        $model->update();
        
        foreach ($model->bookingDatesSortedAndNotNull as $booking_date_obj) 
        {
            Notifications::addDestinationOverbooking($model,$booking_date_obj);
            Notifications::addMultipleUnitsBooking($model,$booking_date_obj);
            Notifications::addUnitNotAssingned($model,$booking_date_obj);  
        }
        //Notifications::addRateNotAvailable($model);

        return $this->redirect(['deleted-bookings']);
    }

    /**
     * Finds the Bookings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return BookingsItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BookingsItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListitems($provider_id)
    {
        $special_requests_html = '<span class="label label-danger"> Whoops! </span>
        <span>&nbsp;  No Special Requests found </span>' ; 

        $status_id = BookingStatuses::findOne(['label' => 'New'])->id;

        if($provider_id=="null" || empty($provider_id))
        {
            echo json_encode([
                'estimated_arrival_times' => '',
                'general_booking' => '',
                'flag' => '',
                'status' => $status_id,
                'confirmation' => '',
                'offers' => '',
                'travel_partners' => '',
                'special_requests' => $special_requests_html,
                'pricing_type' => '',
            ]);
        }
        else
        {
            $destination = Destinations::findOne(['id' => $provider_id]);
            if($destination->getDestinationTypeName()!='Accommodation')
            {
                echo json_encode([
                    'general_booking' => '',
                    'estimated_arrival_times' => '',
                    'flag' => '',
                    'status' => $status_id,
                    'confirmation' => '',
                    'offers' => $this->renderAjax('offers', [
                                            'provider_id' => $provider_id,
                                        ]),
                    'travel_partners' => $this->renderAjax('travel_partners', [
                                            'provider_id' => $provider_id,
                                        ]),
                    'special_requests' => $special_requests_html,
                    'pricing_type' => $destination->pricing,
                ]);
            }
            else
            {
                // *********** Get Booking default fields *************//
                $booking_rules_model = BookingRules::findOne(['provider_id' => $provider_id]);
                $booking_polices_model = BookingPolicies::findOne(['provider_id' => $provider_id]);

                echo json_encode([
                    'estimated_arrival_times' => $this->renderAjax('estimated_arrival_times', [
                                            'time_group' => $booking_polices_model->time_group,
                                        ]),
                    'general_booking' => $booking_rules_model->general_booking_cancellation,
                    'flag' => $booking_rules_model->new_booking_type,
                    'status' => $booking_rules_model->new_booking_status,
                    'confirmation' => $booking_rules_model->routine_booking_confirmation,
                    'offers' => $this->renderAjax('offers', [
                                            'provider_id' => $provider_id,
                                        ]),
                    'travel_partners' => $this->renderAjax('travel_partners', [
                                            'provider_id' => $provider_id,
                                        ]),
                    'special_requests' => $this->renderAjax('special_requests', [
                                            'provider_id' => $provider_id,
                                        ]),
                    'pricing_type' => $destination->pricing,
                ]);
            }
        }
    }

    public function actionListRates($item_id)
    {
        echo json_encode([
                'rates' => $this->renderAjax('rates', [
                                        'item_id' => $item_id,
                                    ]),
            ]);
    }

    public function actionListDiscountCodes($rates_id)
    {
        $discount_html = '<span class="label label-danger"> Whoops! </span>
        <span>&nbsp;  No Discount Codes found </span>' ; 

        $upsell_html = '<span class="label label-danger"> Whoops! </span>
        <span>&nbsp;  No Upsell Items found </span>' ; 

        if(!empty($rates_id))
        {
            echo json_encode([
                'discount_codes' => $this->renderAjax('discount_codes', [
                                        'rates_id' => $rates_id,
                                    ]),
                'upsell_items' => $this->renderAjax('upsell_items', [
                                        'rates_id' => $rates_id,
                                    ]),
            ]);
        }
        else
        {
            echo json_encode([
                'discount_codes' => $discount_html,
                'upsell_items' => $upsell_html,
            ]);
        }
    }

    public function actionAddUser()
    {
        $model = new User();
        $model->username = $_POST['username'];
        $model->email = $_POST['email'];
        $model->status = User::STATUS_ACTIVE;
        $model->type = User::USER_TYPE_USER;
        $password = Yii::$app->security->generateRandomString(12);
        $model->setPassword($password);
        $model->generateAuthKey();

        if($model->save())
        {
            $profile_model = new UserProfile();
            $profile_model->user_id = $model->id;
            $profile_model->first_name = $_POST['fname'];
            $profile_model->last_name = $_POST['lname'];
            $profile_model->country_id = $_POST['country_id'];
            $profile_model->state_id = $_POST['state_id'];
            $profile_model->city_id = $_POST['city_id'];
            $profile_model->save();

            /*//send email here
            Yii::$app->mailer
            ->compose(
                ['html' => 'userRegister-html', 'text' => 'userRegister-text'],
                ['user' => $model, 'password' => $password]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' Troll'])
            ->setTo($model->email)
            ->setSubject('Account created for ' . Yii::$app->name)
            ->send();*/

            $users = User::find()->where(['type'=> User::USER_TYPE_USER])->all();

            $html =  "<option value=''></option>";
    
            if(!empty($users))
            {
                foreach ($users as $key =>  $user) 
                {
                    if(empty($user->profile->country_id))
                            $country = '';
                        else
                            $country = ' - '.$user->profile->country->name;

                    $html = $html."<option value='".$user->id."'>".$user->username.' - '.$user->email.$country."</option>";  
                }
            }

            echo json_encode([
                    'error' => 0,
                    'users' => $html,
                    'user_id' => $model->id,
                ]);  
        }
        else
        {
            $error_str = '<p>Please fix the following errors:</p>';
            $error_arr = $model->getErrors();

            if(!empty($error_arr))
            {
                $error_str .= '<ul>';

                if(array_key_exists('username', $error_arr))
                    $error_str = $error_str.'<li>'.$error_arr['username'][0].'</li>';

                if(array_key_exists('email', $error_arr))
                    $error_str = $error_str.'<li>'.$error_arr['email'][0].'</li>';

                $error_str .= '</ul>';

                echo json_encode([
                    'error' => 1,
                    'error_str' => $error_str,
                ]);
            }
        }
    }

    public function actionListItemsRates()
    {
        // ob_start();
        $error_html = '<div class="capacity-error alert-danger alert fade in">No Bookable Item(s) found.</div>' ;

        $items = BookableItems::find()->where(['provider_id' => $_POST['provider_id']])->all();

        $arrival_date = str_replace('/', '-', $_POST['arrival_date']);
        $arrival_date = date('Y-m-d',strtotime($arrival_date));

        $departure_date = str_replace('/', '-', $_POST['departure_date']);
        $departure_date = date('Y-m-d',strtotime($departure_date));

        $date1=date_create($departure_date);
        $date2=date_create($arrival_date);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");

        $date_exception_flag = 0;
        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['provider_id'], 'state' => 0])->all();

        $dates_arr = array();
        $date = $_POST['arrival_date'];
        $date = str_replace('/', '-', $date);

        //         echo "<pre>";
        // print_r($date);
        // exit();
       
        // $date = DateTime::createFromFormat('d/m/Y', strtotime($date));
        $date = date("Y-m-d", strtotime($date));
        // echo "<pre>";
        // print_r($date);
        // exit();
        for ($i=0; $i < $difference ; $i++)
        {
            array_push($dates_arr, $date);

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);

        }

        // checking if bookable item is close on one of the date //

        $items_to_delete = array();
        foreach ($items as $key => $value) 
        {   
            $is_item_closed = RatesOverride::find()->where(['bookable_item_id' => $value->id])->andWhere(['date' => $dates_arr])->andWhere(['bookable_item_closed' => 1])->all();

            if(!empty($is_item_closed))
            {
                array_push($items_to_delete, $key);
            }
        }
        // echo "<pre>";
        // print_r($items_to_delete);
        // exit();
        // die();
        if(!empty($items_to_delete))
        {
            foreach ($items_to_delete as $value) 
            {
                unset($items[$value]);
            }
        }

        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                if(in_array($date->date, $dates_arr))
                {
                    $date_exception_flag = 1;
                    break;
                }
            }
        }

        // echo "<pre>";
        // print_r($date_exception_flag);
        // exit();


        if(empty($items))
        {
            echo json_encode([
                'items_rates' => $error_html,
            ]);
        }
        else
        {
            echo json_encode([
                'items_rates' => $this->renderAjax('items_rates', [
                                        'items' => $items,
                                        'arrival_date' => $_POST['arrival_date'],
                                        'departure_date' => $_POST['departure_date'],
                                        'remove_rate_conditions' => $_POST['remove_rate_conditions'],
                                    ]),
                'total_nights' => $difference,
                'date_exception_flag' => $date_exception_flag,
            ]);
        }
    }

    public function actionListItemRatesForUpdate()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $model = $this->findModel($_POST['booking_item_id']);
        $bookingDates = $model->bookingDatesSortedAndNotNull;
        if($_POST['item_id'] == $model->item->id)
        {
            $item = $model->item;
        }
        else
        {
            $item = BookableItems::findOne(['id' => $_POST['item_id']]);
        }
        

        $savedDates = [];
        $mergeDates = [];

        foreach ($bookingDates as $key => $value) 
        {
            $savedDates[] = $value->date;
        }

        $arrival_date = str_replace('/', '-', $_POST['arrival_date']);
        $arrival_date = date('Y-m-d',strtotime($arrival_date));        

        $departure_date = str_replace('/', '-', $_POST['departure_date']);
        $departure_date = date('Y-m-d',strtotime($departure_date));

        $mergeDates[] = $savedDates[0];
        $mergeDates[] = $savedDates[count($savedDates)-1];
        $mergeDates[] = $arrival_date;
        $mergeDates[] = $departure_date;

        sort($mergeDates);

        $first_date = $mergeDates[0];
        $last_date = $mergeDates[count($mergeDates)-1];

        $date1=date_create($last_date);
        $date2=date_create($first_date);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");

        $date_exception_flag = 0;
        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $model->provider_id, 'state' => 0])->all();

        $dates_arr = array();
        $date = $_POST['arrival_date'];
        $date = str_replace('/', '-', $date);

        //         echo "<pre>";
        // print_r($date);
        // exit();
       
        // $date = DateTime::createFromFormat('d/m/Y', strtotime($date));
        $date = date("Y-m-d", strtotime($date));
        // echo "<pre>";
        // print_r($date);
        // exit();
        for ($i=0; $i < $difference ; $i++)
        {
            array_push($dates_arr, $date);

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);

        }

        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date) 
            {
                if(in_array($date->date, $dates_arr))
                {
                    $date_exception_flag = 1;
                    break;
                }
            }
        }

        $availableItemNames = BookingsItems::checkAvailableItemNamesIncludedSaved($item->id,$first_date,$difference,$model->id);

        echo json_encode([
            'item_rates' => $this->renderAjax('get_item_rates', [
                                    'model' => $model,
                                    'item' => $item,
                                    'arrival_date' => $_POST['arrival_date'],
                                    'departure_date' => $_POST['departure_date'],
                                    'remove_rate_conditions' => $_POST['remove_rate_conditions'],
                                    'savedDates' => $savedDates,
                                ]),
            'item_names' => $this->renderAjax('get_item_names', [
                                    'availableItemNames' => $availableItemNames,
                                    'b_item' => $model
                                ]),
            'date_exception_flag' => $date_exception_flag,
        ]);
    }

    public function actionItemTypeList()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();

        $model = $this->findModel($_POST['booking_item_id']);
        $bookingDates = $model->bookingDatesSortedAndNotNull;
        $item = $_POST['item_id'];

        $savedDates = [];
        $mergeDates = [];

        foreach ($bookingDates as $key => $value) 
        {
            $savedDates[] = $value->date;
        }

        $arrival_date = str_replace('/', '-', $_POST['arrival_date']);
        $arrival_date = date('Y-m-d',strtotime($arrival_date));        

        $departure_date = str_replace('/', '-', $_POST['departure_date']);
        $departure_date = date('Y-m-d',strtotime($departure_date));

        $mergeDates[] = $savedDates[0];
        $mergeDates[] = $savedDates[count($savedDates)-1];
        $mergeDates[] = $arrival_date;
        $mergeDates[] = $departure_date;

        sort($mergeDates);

        $first_date = $mergeDates[0];
        $last_date = $mergeDates[count($mergeDates)-1];

        $date1=date_create($last_date);
        $date2=date_create($first_date);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");

            $Org_availableItemNames = BookingsItems::checkAvailableItemNamesIncludedSaved($item,$first_date,$difference,$model->id);
            $availableItemNames = array();
           $item_names =  BookableItemsNames::find()->where(['bookable_item_id' => $item])->all();
           foreach ($item_names as $key => $value) 
           {
               array_push($availableItemNames, $value->id);
           }
        // echo "<pre>";
        // print_r($availableItemNames);
        // exit();

        echo json_encode([
            'item_names' => $this->renderAjax('get_item_names_new', [
                                    'availableItemNames' => $availableItemNames,
                                    'Org_availableItemNames' => $Org_availableItemNames,
                                    'b_item' => $model
                                ]),
        ]);
    }

    public function actionNumberAsDecimal($price)
    {
        if($price=='NaN')
            return '0';
        else
            return Yii::$app->formatter->asDecimal( $price, "ISK");
    }

    public function actionCalculateItemPrice()
    {
        $dates = isset($_POST['dates'])?explode(',', $_POST['dates']):NULL;
        $rateModel = Rates::findOne(['id' => $_POST['rate_id']]);
        
        $totalPrice = 0;
        $adults_total = 0;
        $children_total = 0;

        // ***************** Get Travel Partner Discount ***************** //
        
        $travel_partner_discount = 0;
        $travel_partner_id = 0;
        if(!empty($_POST['travel_partner_id']))
        {
            $travel_partner_model = DestinationTravelPartners::findOne(['id' => $_POST['travel_partner_id'] ]);
            $travel_partner_id = $_POST['travel_partner_id'];
            $travel_partner_discount = $travel_partner_model->comission;
        }

        // ***************** Get Offer Discount ***************** //
        
        $offer_discount = 0;

        if(!empty($_POST['offer_id']))
        {
            $offer_model = Offers::findOne(['id' => $_POST['offer_id']]);
            $offer_discount = $offer_model->commission;
        }

        // ***************** Calculate Upsell Price ***************** //

        $upsell_total = 0;
        $upsell_ids = $_POST['upsell_ids'];

        if(!empty($upsell_ids))
        {
            foreach ($upsell_ids as $key => $value) 
            {
                $arr = explode(':', $value);
                $upsell_id = $arr[0];
                $upsell_extra_bed_quantity = $arr[1];

                $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $_POST['rate_id'], 'upsell_id' => $upsell_id]);

                if($ratesUpsellModel->upsell->type == 7)
                {
                    $upsell_total = $upsell_total + ($ratesUpsellModel->price * $upsell_extra_bed_quantity);
                }
                else
                {
                    $upsell_total = $upsell_total + $ratesUpsellModel->price;
                }
            }
        }

        if(isset($_POST['no_of_adults']) && isset($_POST['no_of_children']))
        {
            $no_of_adults = $_POST['no_of_adults'];
            $no_of_children = $_POST['no_of_children'];

            // ***************** Calculate Adult Price **************** //
            $overridden_rate = RatesOverride::findOne(['date' => $dates[0], 'rate_id' => $rateModel->id]);
            if(empty($overridden_rate))
            {
                $item_price_first_adult = $rateModel->item_price_first_adult;
                $item_price_additional_adult = $rateModel->item_price_additional_adult;

                $item_price_first_child = $rateModel->item_price_first_child;
                $item_price_additional_child = $rateModel->item_price_additional_child;
            }
            else
            {
                $item_price_first_adult = $overridden_rate->item_price_first_adult;
                $item_price_additional_adult = $overridden_rate->item_price_additional_adult;

                $item_price_first_child = $overridden_rate->item_price_first_child;
                $item_price_additional_child = $overridden_rate->item_price_additional_child;
            }

            if($no_of_adults>1)
            {
                $adults_total = $item_price_first_adult;
                $no_of_adults = $no_of_adults - 1;
                $adults_total = $adults_total + ($item_price_additional_adult * $no_of_adults);
            }
            else if($no_of_adults == 1)
            {
                $adults_total = $item_price_first_adult;
            }

            // ***************** Calculate Children Price ***************** //

            if($no_of_children>1)
            {
                $children_total = $item_price_first_child;
                $no_of_children = $no_of_children - 1;
                $children_total = $children_total + ($item_price_additional_child * $no_of_children);
            }
            else if($no_of_children == 1)
            {
                $children_total = $item_price_first_child;
            }

            $totalPrice = $adults_total + $children_total;
        }
        else if(isset($_POST['person_no']))
        {

            $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $_POST['rate_id'], 'person_no' => $_POST['person_no']]);
            if(!empty($capacityPricingModel))
            {
                if(!empty($capacityPricingModel->price))
                {
                    if($_POST['custom_rate'] == '')
                    {
                        $totalPrice = $capacityPricingModel->price;
                    }
                    else
                    {
                        $totalPrice = $_POST['custom_rate'];
                    }
                    
                }
            }
        }
        // old vat rate which is stored in destination //
       // $vat_rate = $rateModel->unit->destination->destinationsFinancials->vat_rate;
        //new vat rate which is stored in rates //
        $vat_rate = $rateModel->vat_rate;
        //$vat_rate = str_replace(',', '', $vat_rate);
        
        // old lodging tax which is stored in destination //
       // $lodgingTax = $rateModel->unit->destination->destinationsFinancials->lodging_tax_per_night;
        //new lodging tax which is stored in rates //
        $lodgingTax = $rateModel->lodging_tax_per_night;
        $lodgingTax = $lodgingTax * ((isset($_POST['quantity'])?$_POST['quantity']:1) * $_POST['total_nights']);

        $discount_code_flag = 0;
        // echo "<pre>";
        // print_r($vat_rate);
        // exit();
        // ***************** Calculate Discount Price ***************** //

        $discount_arr = [
            'discounted_amount' => '',
            'discounted_total' => '',
        ];

        $discount_ids = $_POST['discount_ids'];

        if(!empty($discount_ids))
        {
            $discount_code_flag = 1;
            $discount_arr = $this->applyDicountCode($totalPrice,$upsell_total,$discount_ids);
            $totalPrice = $discount_arr['discounted_total'];
        }

        // before overridden rate //
        // echo $this->calculatePriceByIcelandFormula($totalPrice,$upsell_total,$vat_rate,$lodgingTax,$travel_partner_discount,$discount_code_flag,$offer_discount,(isset($_POST['quantity'])?$_POST['quantity']:1),$_POST['total_nights'],$discount_arr['discounted_amount'],$dates);

        // after overridden rate //
        echo $this->calculatePriceByIcelandFormula($totalPrice,$upsell_total,$vat_rate,$lodgingTax,$travel_partner_discount,$discount_code_flag,$offer_discount,(isset($_POST['quantity'])?$_POST['quantity']:1),$_POST['total_nights'],$discount_arr['discounted_amount'],$dates,$rateModel->id,isset($_POST['person_no'])?$_POST['person_no']:null,$travel_partner_id);

    }

    public function applyDicountCode($unit,$upsell,$discount_ids)
    {
        $discounted_total = 0;

        $discount_arr = [
            'unit' => 0,
            'upsell' => 0
        ];

        foreach ($discount_ids as $key => $value) 
        {
            $arr = explode(':', $value);
            $discount_id = $arr[0];
            $discount_code_extra_bed_quantity = $arr[1];

            $discount_amount = 0;

            $model = DiscountCode::findOne(['id' => $discount_id]);

            switch ($model->applies_to) 
            {
                case 0: // unit
                    switch ($model->pricing_type) 
                    {
                        case 0: // percentage
                            $discount_amount = $this->calculateDiscountAmount($unit,$model->amount);

                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                            {
                                $discount_amount = $discount_amount * $discount_code_extra_bed_quantity;
                            }

                            $discount_arr['unit'] += $discount_amount;
                            break;
                        case 1: // amount
                            $discount_amount = $model->amount;

                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                            {
                                $discount_amount = $discount_amount * $discount_code_extra_bed_quantity;
                            }

                            $discount_arr['unit'] += $discount_amount;
                            break;
                    }
                    break;

                case 1: // unit + upsell
                    switch ($model->pricing_type) 
                    {
                        case 0: // percentage
                            $discount_amount = $this->calculateDiscountAmount($unit,$model->amount);

                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                            {
                                $discount_amount = $discount_amount * $discount_code_extra_bed_quantity;
                            }

                            $discount_arr['unit'] += $discount_amount;
                            $discount_arr['upsell'] += $discount_amount;
                            break;
                        case 1: // amount
                            $discount_amount = $model->amount;

                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                            {
                                $discount_amount = $discount_amount * $discount_code_extra_bed_quantity;
                            }

                            $discount_arr['unit'] += $discount_amount;
                            $discount_arr['upsell'] += $discount_amount;
                            break;
                    }
                    break;

                case 2: // upsell
                    switch ($model->pricing_type) 
                    {
                        case 0: // percentage
                            $discount_amount = $this->calculateDiscountAmount($unit,$model->amount);

                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                            {
                                $discount_amount = $discount_amount * $discount_code_extra_bed_quantity;
                            }

                            $discount_arr['upsell'] += $discount_amount;
                            break;
                        case 1: // amount
                            $discount_amount = $model->amount;

                            if($model->quantity_type == 1 || $model->quantity_type == 2)
                            {
                                $discount_amount = $discount_amount * $discount_code_extra_bed_quantity;
                            }

                            $discount_arr['upsell'] += $discount_amount;
                            break;
                    }
                    break;
            }
        }

        if(!empty($upsell))
            $discounted_total = ($unit - $discount_arr['unit']) + ($upsell - $discount_arr['upsell']);
        else
            $discounted_total = $unit - $discount_arr['unit'];

        return [
            'discounted_total' => $discounted_total,
            'discounted_amount' => $discount_arr['unit'] + $discount_arr['upsell'],
        ];
    }

    public function calculateDiscountAmount($value,$discount)
    {
        return $value * ($discount / 100);
    }

    public function calculatePriceByIcelandFormula($rate,$upsell,$vat,$lodgingTax,$travel_partner_discount='',$discount_code_flag='',$offer_discount='',$quantity='',$total_nights='',$discount_amount='',$dates = '',$rate_id = '',$person_no = '', $travel_partner_id)
    {
        $travel_partner_commission_flag = false;

        if($travel_partner_id !=0)
        {
            $t_partner = DestinationTravelPartners::findOne(['id' => $travel_partner_id]);
            if($t_partner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
            {
                $travel_partner_commission_flag = true;
            }
        }
        $amount_of_discount = 0;

        $original_rate = $rate;

        // for overridden rates
        $new_total_price = 0;
        $new_org_rate_price = 0;
        $test_flag = false;

        $count = 0;
        if(!empty($dates))
        {
            foreach ($dates as $date) 
            {
                $rate_override = RatesOverride::findOne(['date' => $date, 'rate_id' => $rate_id]);
                if(empty($rate_override))
                {
                    $rate = $original_rate;
                }
                else
                {
                    if($person_no==1)
                    {
                        $rate = $rate_override->single;
                    }
                    if($person_no==2)
                    {
                        $rate = $rate_override->double;
                    }
                    if($person_no==3)
                    {
                        $rate = $rate_override->triple;
                    }
                    if($person_no==4)
                    {
                        $rate = $rate_override->quad;
                    }
                }
                if(!$discount_code_flag)
                {
                    $rate = $rate + $upsell;
                    $x = $rate/(1+($vat/100));
                    $y = $x - $lodgingTax;

                    if(!empty($travel_partner_discount))
                    {
                        if($travel_partner_commission_flag)
                        {
                            $amount_of_discount = 0;
                        }
                        else
                        {
                            $amount_of_discount = $y * ($travel_partner_discount/100);
                        }
                        
                    }
                    else if(!empty($offer_discount))
                    {
                        $amount_of_discount = $y * ($offer_discount/100);
                    }
                }
                else
                {
                    $x = $rate/(1+($vat/100));
                    $y = $x - $lodgingTax;
                }
                
                $sub_total_1 = $y - $amount_of_discount;
                $sub_total_2 = $sub_total_1 + $lodgingTax;
                $vat_amount = $sub_total_2 * ($vat / 100);

                if($travel_partner_commission_flag)
                {
                    $final_total = round($rate);
                    $new_total_price += $rate;
                    $final_total = ($rate *  $quantity);
                }
                else
                {
                    $final_total = round($vat_amount + $sub_total_2);
                    $new_total_price += $final_total;
                    $final_total = ($final_total *  $quantity);
                }
                
                if(empty($rate_override) && $count == 0)
                {
                    $new_org_rate_price = $final_total;
                    $count++;
                }
            }
        }
        else
        {
            // before overridden rate // 
            if(!$discount_code_flag)
            {
                $rate = $rate + $upsell;
                $x = $rate/(1+($vat/100));
                $y = $x - $lodgingTax;

                if(!empty($travel_partner_discount))
                {
                    if($travel_partner_commission_flag)
                    {
                        $amount_of_discount = 0;
                    }
                    else
                    {
                        $amount_of_discount = $y * ($travel_partner_discount/100);
                    }
                }
                else if(!empty($offer_discount))
                {
                    $amount_of_discount = $y * ($offer_discount/100);
                }
            }
            else
            {
                $x = $rate/(1+($vat/100));
                $y = $x - $lodgingTax;
            }
            
            $sub_total_1 = $y - $amount_of_discount;
            $sub_total_2 = $sub_total_1 + $lodgingTax;
            $vat_amount = $sub_total_2 * ($vat / 100);

            if($travel_partner_commission_flag)
            {
                $final_total = round($rate);
                $final_total = ($rate) * $total_nights;
            }
            else
            {
                $final_total = round($vat_amount + $sub_total_2);
                $final_total = ($final_total *  $quantity) * $total_nights;
            }
            
            $item_total_nights_price = 0;
            
            $new_org_rate_price = $final_total;
            if(!empty($dates))
            {

                $new_total_price = $final_total * count($dates);

            }
        }
            
        if($new_org_rate_price == 0)
        {
            $new_org_rate_price = $original_rate;
        }
        if(count($dates) == 1)
        {
            $new_org_rate_price = $new_total_price;
        }

        
            


        return json_encode([
                'quantity' => $quantity,
                'total_nights' => $total_nights,
                'rate' => Yii::$app->formatter->asDecimal( $rate, 2),
                'vat' => str_replace('.', ',', $vat),
                'x' => Yii::$app->formatter->asDecimal( $x, 2),
                'lodgingTax' => $lodgingTax,
                'y' => Yii::$app->formatter->asDecimal( $y, 2),
                'voucher_discount' => Yii::$app->formatter->asDecimal( $discount_amount, 2),
                'travel_partner_discount' => $travel_partner_discount,
                'offer_discount' => $offer_discount,
                'amount_of_discount' => Yii::$app->formatter->asDecimal( $amount_of_discount, 2),
                'sub_total_1' => Yii::$app->formatter->asDecimal( $sub_total_1, 2),
                'sub_total_2' => Yii::$app->formatter->asDecimal( $sub_total_2, 2),
                'vat_amount' => substr(str_replace('.', ',', $vat_amount),0,5),
                'orgPrice' => $new_org_rate_price,
                'icelandPrice' => Yii::$app->formatter->asDecimal( $new_org_rate_price, 0),
                // 'item_total_nights_price' => Yii::$app->formatter->asDecimal( $item_total_nights_price, 0),
                'item_total_nights_price' => Yii::$app->formatter->asDecimal( $new_total_price, 0),
            ]);
    }

    // new caluclulate price for updating cart items //

    public function calculatePriceByIcelandFormulaNew($rate,$upsell,$vat,$lodgingTax,$travel_partner_discount='',$discount_code_flag='',$offer_discount='',$quantity='',$total_nights='',$discount_amount='', $travel_partner_id = '',$dates = '',$rate_id = '',$person_no = '')
    {

        $travel_partner_commission_flag = false;

        if($travel_partner_id !='')
        {
            $t_partner = DestinationTravelPartners::findOne(['id' => $travel_partner_id]);
            if($t_partner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
            {
                $travel_partner_commission_flag = true;
            }
        }

        $amount_of_discount = 0;


        // $original_rate = $rate;

        // for overridden rates
        $new_total_price = 0;
        
        $amount_of_discount = 0;

        if(!$discount_code_flag)
        {
            $rate = $rate + $upsell;
            $x = $rate/(1+($vat/100));
            $y = $x - $lodgingTax;

            if(!empty($travel_partner_discount))
            {
                $amount_of_discount = $y * ($travel_partner_discount/100);
            }
            else if(!empty($offer_discount))
            {
                $amount_of_discount = $y * ($offer_discount/100);
            }
        }
        else
        {
            $x = $rate/(1+($vat/100));
            $y = $x - $lodgingTax;
        }
        
        $sub_total_1 = $y - $amount_of_discount;
        $sub_total_2 = $sub_total_1 + $lodgingTax;
        $vat_amount = $sub_total_2 * ($vat / 100);

        if($travel_partner_commission_flag)
        {
            $final_total = round($rate);
            // $new_total_price += $rate;
            // $final_total = ($rate *  $quantity);
        }
        else
        {
            $final_total = round($vat_amount + $sub_total_2);
        }
        
        // $final_total = ($final_total *  $quantity) * $total_nights;
        $item_total_nights_price = 0;

        if(!empty($dates))
        {
            $item_total_nights_price = $final_total * count($dates);
        }
        
        // echo "<pre>";
        // print_r($final_total);
        // exit();
        return json_encode([
                'quantity' => $quantity,
                'total_nights' => $total_nights,
                'rate' => Yii::$app->formatter->asDecimal( $rate, 2),
                'vat' => str_replace('.', ',', $vat),
                'x' => Yii::$app->formatter->asDecimal( $x, 2),
                'lodgingTax' => $lodgingTax,
                'y' => Yii::$app->formatter->asDecimal( $y, 2),
                'voucher_discount' => Yii::$app->formatter->asDecimal( $discount_amount, 2),
                'travel_partner_discount' => $travel_partner_discount,
                'offer_discount' => $offer_discount,
                'amount_of_discount' => Yii::$app->formatter->asDecimal( $amount_of_discount, 2),
                'sub_total_1' => Yii::$app->formatter->asDecimal( $sub_total_1, 2),
                'sub_total_2' => Yii::$app->formatter->asDecimal( $sub_total_2, 2),
                'vat_amount' => substr(str_replace('.', ',', $vat_amount),0,5),
                'orgPrice' => $final_total,
                'icelandPrice' => Yii::$app->formatter->asDecimal( $final_total, 0),
                'item_total_nights_price' => Yii::$app->formatter->asDecimal( $final_total, 0),
            ]);
    }

    public function actionAddToBookingCart()
    {
        // print_r($_POST);
        // exit;
        $bookingItems = $_POST['bookingItems'];
        $bookingItemsModel = new BookingsItemsCart();
        $dateArr = [];
        $allDates = [];

        foreach ($_POST['allDates'] as $key => $value) 
        {
            $value = explode(',', $value);

            for ($i=0; $i <count($value) ; $i++) 
            {
                array_push($allDates,$value[$i]);
            }
        }

        foreach ($bookingItems as $key => $bookingItem) 
        {
            $item_date = explode(',', $bookingItem['date']);

            if($key==0)
            {
                $bookingItemsModel->provider_id = $bookingItem['provider_id'];
                $bookingItemsModel->item_id = $bookingItem['item_id'];
                // echo "<pre>";
                // print_r($bookingItem['arrival_date']);
                // print_r($bookingItem['departure_date']);
                // exit();
                // if dates are not in order itwont work //
                // $bookingItemsModel->arrival_date = $allDates[0];
                // $bookingItemsModel->departure_date = date('Y-m-d',strtotime("+1 day", strtotime($allDates[count($allDates)-1])));
                // new approach // 
                $arrival_date = str_replace('/', '-', $bookingItem['arrival_date']);
                $departure_date = str_replace('/', '-', $bookingItem['departure_date']);
                $bookingItemsModel->arrival_date = date('Y-m-d',strtotime($arrival_date));
                $bookingItemsModel->departure_date = date('Y-m-d',strtotime($departure_date));

                $bookingItemsModel->rate_conditions = $bookingItem['remove_rate_conditions'];
                $bookingItemsModel->pricing_type = $bookingItem['pricing_type'];
                $bookingItemsModel->created_by = Yii::$app->user->identity->id;
                $bookingItemsModel->item_name_id = $bookingItem['item_name_id'];
                $bookingItemsModel->status_id = $bookingItem['status_id'];
                //$bookingItemsModel->housekeeping_status_id = $bookingItem['housekeeping_status_id'];
                $bookingItemsModel->flag_id = $bookingItem['flag_id'];
                $bookingItemsModel->travel_partner_id = $bookingItem['travel_partner_id'];
                $bookingItemsModel->voucher_no = $bookingItem['voucher_no'];
                $bookingItemsModel->reference_no = $bookingItem['reference_no'];
                $bookingItemsModel->comments = $bookingItem['comment'];
                $bookingItemsModel->notes = $bookingItem['notes'];
                $bookingItemsModel->offers_id = $bookingItem['offer_id'];
                $bookingItemsModel->estimated_arrival_time_id = $bookingItem['estimated_arrival_time_id'];
                $bookingItemsModel->save();

                if(isset($bookingItem['housekeeping_status_id']) && !empty($bookingItem['housekeeping_status_id']))
                {
                    //$housekeeping_status_ids = explode(',',$bookingItem['housekeeping_status_id'] );
                    //if(!empty($housekeeping_status_ids))
                    
                    foreach ($bookingItem['housekeeping_status_id'] as $id) 
                    {
                        $new_housekeeping_status = new BookingHousekeepingStatusCart();
                        $new_housekeeping_status->booking_item_id = $bookingItemsModel->id;
                        $new_housekeeping_status->housekeeping_status_id = $id;
                        $new_housekeeping_status->save();
                    }
                    
                    
                }
            }
            

            $temp_date_for_custom_rate = date("Y-m-d", strtotime($item_date[0]));
            for ($i=0; $i <count($item_date) ; $i++) 
            {
                $flag_for_overridden_rate = false;
                $has_record = 0;
                $error_str = 0;

                $bookingDatesModel = new BookingDatesCart();

                if($bookingItem['pricing_type']) // ************* Capacity Pricing ************ //
                {
                    if(isset($bookingItem['person_no']) && $bookingItem['quantity'] > 0)
                    {
                        $bookingDatesModel->no_of_adults = $bookingItem['no_of_adults'];
                        $bookingDatesModel->no_of_children = $bookingItem['no_of_children'];

                        $custom_rate = isset($bookingItem['custom_rate'])?$bookingItem['custom_rate']:NULL;
                        $bookingDatesModel->person_no = $bookingItem['person_no'];

                        $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $bookingItem['rate_id'], 'person_no' => $bookingItem['person_no']]);
                        $rate_override = RatesOverride::findOne(['rate_id' => $bookingItem['rate_id'], 'date' => $temp_date_for_custom_rate]);
                        if($custom_rate == $capacityPricingModel->price && empty($rate_override))
                        {
                            $bookingDatesModel->person_price = $capacityPricingModel->price;
                            $bookingDatesModel->custom_rate = null;
                        }
                        else
                        {
                            $flag_for_overridden_rate = true;
                            // chceking if its a custom rate is added through input or using daily rates //
                            if($bookingItem['person_no']==1)
                            {
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                    $custom_rate = $rate_override->single;
                                }
                            }
                            if($bookingItem['person_no']==2)
                            {
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                    $custom_rate = $rate_override->double;
                                }
                            }
                            if($bookingItem['person_no']==3)
                            {
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                    $custom_rate = $rate_override->triple;
                                }
                            }
                            if($bookingItem['person_no']==4)
                            {
                                if(empty($rate_override))
                                {
                                    $bookingDatesModel->rate_added_through = 0;
                                }
                                else
                                {
                                    $bookingDatesModel->rate_added_through = 1;
                                    $custom_rate = $rate_override->quad;
                                }
                            }

                            // Old Code //
                            // $bookingDatesModel->person_price = $capacityPricingModel->price;
                            $bookingDatesModel->person_price = $custom_rate;
                            $bookingDatesModel->custom_rate = $custom_rate;
                        }
                        
                        $has_record = 1;
                    }
                }
                else // ************* First/Additional ************ //
                {
                    if(isset($bookingItem['no_of_adults']) && isset($bookingItem['no_of_children']) && 
                        ($bookingItem['no_of_adults']!=0 || $bookingItem['no_of_children']!=0) && $bookingItem['quantity'] > 0)
                    {
                        $bookingDatesModel->no_of_adults = $bookingItem['no_of_adults'];
                        $bookingDatesModel->no_of_children = $bookingItem['no_of_children'];
                        $has_record = 1;
                    }
                }

                if($has_record)
                {
                    $date = $bookingItemsModel->arrival_date;

                    $date1=date_create($bookingItemsModel->departure_date);
                    $date2=date_create($bookingItemsModel->arrival_date);
                    $diff=date_diff($date1,$date2);
                    $difference =  $diff->format("%a");

                    ///////////////////////////// Changing Flag to 0 and commenting check available item names to allow over booking ////////////////
                    $flag = 0;
                    // $flag = 1;

                    // $availableItemNames = BookingsItemsCart::checkAvailableItemNames($bookingItem['item_id'],$bookingItemsModel->arrival_date,$difference);

                    // if(!empty($bookingItem['item_name_id']))
                    // {
                    //     if(!empty($availableItemNames) && in_array($bookingItem['item_name_id'], $availableItemNames))
                    //     {
                    //         $flag = 0;
                    //     }
                    // }
                    // else
                    // {
                    //     $flag = 0;
                    // }

                    if($flag)
                    {
                        BookingsItemsCart::deleteAll(['item_id' => $bookingItem['item_id'],'created_by' => Yii::$app->user->identity->id ]);
                        $error_str = 1;

                        echo json_encode([
                            'bookings_items' => $this->renderAjax('side_bar_cart_view'),
                            'error' => $error_str,
                        ]);

                        return;
                    }
                    else
                    {
                        $bookingDatesModel->booking_item_id = $bookingItemsModel->id;
                        $bookingDatesModel->user_id = $bookingItem['user_id'];

                        if(empty($bookingDatesModel->user_id))
                        {   
                            $bookingDatesModel->guest_first_name = $bookingItem['guest_first_name'];
                            $bookingDatesModel->guest_last_name = $bookingItem['guest_last_name'];
                            $bookingDatesModel->guest_country_id = $bookingItem['guest_country'];
                        } 
                        
                        array_push($dateArr,$item_date[$i]);
                        $bookingDatesModel->date = $item_date[$i];
                        $bookingDatesModel->item_id = $bookingItem['item_id'];
                        $bookingDatesModel->item_name_id = $bookingItem['item_name_id'];
                        $bookingDatesModel->rate_id = $bookingItem['rate_id'];
                        $bookingDatesModel->beds_combinations_id = !empty($bookingItem['beds_combinations_id'])? $bookingItem['beds_combinations_id']:NULL;
                        $bookingDatesModel->quantity = $bookingItem['quantity'];
                        $bookingDatesModel->travel_partner_id = $bookingItem['travel_partner_id'];
                        $bookingDatesModel->voucher_no = $bookingItem['voucher_no'];
                        $bookingDatesModel->reference_no = $bookingItem['reference_no'];
                        $bookingDatesModel->offers_id = $bookingItem['offer_id'];
                        $bookingDatesModel->booking_cancellation = $bookingItem['booking_cancellation'];
                        $bookingDatesModel->flag_id = $bookingItem['flag_id'];
                        $bookingDatesModel->status_id = $bookingItem['status_id'];
                        //$bookingDatesModel->housekeeping_status_id = $bookingItem['housekeeping_status_id'];
                        $bookingDatesModel->confirmation_id = $bookingItem['confirmation_id'];
                        if($flag_for_overridden_rate == true && ($bookingItem['offer_id'] !=null || $bookingItem['travel_partner_id'] !=null))
                        {
                            $amount_of_discount = 0;
                            $vat = $rate_override->rate->vat_rate;
                        
                            $lodgingTax = $rate_override->rate->lodging_tax_per_night;
                            $lodgingTax = $lodgingTax;
                            if($bookingItem['person_no']==1)
                            {
                                $rate = $rate_override->single;
                            }
                            if($bookingItem['person_no'] ==2)
                            {
                                $rate = $rate_override->double;
                            }
                            if($bookingItem['person_no']==3)
                            {
                                $rate = $rate_override->triple;
                            }
                            if($bookingItem['person_no']==4)
                            {
                                $rate = $rate_override->quad;
                            }
                            
                            $travel_partner_discount = 0;

                            if(!empty($bookingItem['travel_partner_id']))
                            {
                                $travel_partner_model = DestinationTravelPartners::findOne(['id' => $bookingItem['travel_partner_id'] ]);

                                $travel_partner_discount = $travel_partner_model->comission;
                            }

                            // ***************** Get Offer Discount ***************** //
                            
                            $offer_discount = 0;

                            if(!empty($bookingItem['offer_id']))
                            {
                                $offer_model = Offers::findOne(['id' => $bookingItem['offer_id']]);
                                $offer_discount = $offer_model->commission;
                            }
                            // if(!$discount_code_flag)
                            // {
                                // $rate = $rate + $upsell;
                            $x = $rate/(1+($vat/100));
                            $y = $x - $lodgingTax;

                            if(!empty($travel_partner_discount))
                            {
                                $amount_of_discount = $y * ($travel_partner_discount/100);
                            }
                            else if(!empty($offer_discount))
                            {
                                $amount_of_discount = $y * ($offer_discount/100);
                            }
                            // }
                            // else
                            // {
                            //     $x = $rate/(1+($vat/100));
                            //     $y = $x - $lodgingTax;
                            // }
                            
                            $sub_total_1 = $y - $amount_of_discount;
                            $sub_total_2 = $sub_total_1 + $lodgingTax;
                            $vat_amount = $sub_total_2 * ($vat / 100);
                            $final_total = round($vat_amount + $sub_total_2);
                            // $new_total_price += $final_total;
                            $final_total = ($final_total );
                            // if(empty($rate_override) && $count == 0)
                            // {
                            //     $new_org_rate_price = $final_total;
                            //     $count++;
                            // }
                            $bookingDatesModel->total = $final_total;
                        }
                        else
                        {
                            $bookingDatesModel->total = ($flag_for_overridden_rate == false)?$bookingItem['total']:$custom_rate;
                        }
                        $bookingDatesModel->price_json = str_replace("'", '"', $bookingItem['price_json']);
                        //$bookingDatesModel->no_of_nights = $bookingItem['total_nights'];
                        $bookingDatesModel->no_of_nights = count($item_date);
                        $bookingDatesModel->estimated_arrival_time_id = $bookingItem['estimated_arrival_time_id'];

                        if($bookingDatesModel->save())
                        {
                            $bookingDatesModel->discount_codes = isset($bookingItem['discount_ids'])?$bookingItem['discount_ids']:'';
                            $bookingDatesModel->upsell_items = isset($bookingItem['upsell_ids'])?$bookingItem['upsell_ids']:'';
                            $bookingDatesModel->special_requests = isset($bookingItem['special_requests_ids'])?$bookingItem['special_requests_ids']:'';

                            if(!empty($bookingDatesModel->discount_codes))
                            {
                                foreach ($bookingDatesModel->discount_codes as $key => $value) 
                                {
                                    $arr = explode(':', $value);
                                    $discount_id = $arr[0];
                                    $discount_code_extra_bed_quantity = $arr[1];

                                    $discount_code_model = DiscountCode::findOne(['id' => $discount_id]);

                                    $obj = new BookingsDiscountCodesCart();
                                    $obj->discount_id = $discount_id;
                                    $obj->price = $discount_code_model->amount;
                                    $obj->booking_date_id = $bookingDatesModel->id;

                                    if($discount_code_extra_bed_quantity != 'undefined' && ($discount_code_model->quantity_type == 1 || $discount_code_model->quantity_type == 2))
                                    {
                                        $obj->extra_bed_quantity = $discount_code_extra_bed_quantity;
                                    }

                                    $obj->save();
                                }
                            }

                            if(!empty($bookingDatesModel->upsell_items))
                            {
                                foreach ($bookingDatesModel->upsell_items as $key => $value) 
                                {
                                    $arr = explode(':', $value);
                                    $upsell_id = $arr[0];
                                    $upsell_extra_bed_quantity = $arr[1];

                                    $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $bookingItem['rate_id'], 'upsell_id' => $upsell_id]);

                                    $obj = new BookingsUpsellItemsCart();
                                    $obj->upsell_id = $upsell_id;
                                    $obj->price = $ratesUpsellModel->price;
                                    $obj->booking_date_id = $bookingDatesModel->id;

                                    if($ratesUpsellModel->upsell->type == 7 && $upsell_extra_bed_quantity != 'undefined')
                                    {
                                        $obj->extra_bed_quantity = $upsell_extra_bed_quantity;
                                    }

                                    $obj->save();
                                }
                            }

                            if(!empty($bookingDatesModel->special_requests))
                            {
                                foreach ($bookingDatesModel->special_requests as $key => $value) 
                                {
                                    $model = new BookingSpecialRequestsCart();
                                    $model->booking_date_id = $bookingDatesModel->id;
                                    $model->request_id = $value;
                                    $model->save();
                                }
                            }
                        }
                        if(!$bookingDatesModel->save())
                        {
                            echo "<pre>";
                            print_r($bookingDatesModel->errors);
                            exit();
                        }  
                    }
                }

                $temp_date_for_custom_rate = strtotime("+1 day", strtotime($temp_date_for_custom_rate));
                $temp_date_for_custom_rate = date("Y-m-d", $temp_date_for_custom_rate);
            }
        }

        foreach ($allDates as $key => $value) 
        {
            if(!in_array($value, $dateArr))
            {
                $bookingDatesModel = new BookingDates();
                $bookingDatesModel->booking_item_id = $bookingItemsModel->id;
                $bookingDatesModel->date = $value;
                $bookingDatesModel->item_id = $bookingItemsModel->item_id;
                $bookingDatesModel->item_name_id = NULL;
                $bookingDatesModel->quantity = 1;
                $bookingDatesModel->total = 0;
                $bookingDatesModel->price_json = '';
                $bookingDatesModel->no_of_nights = 1;
                $bookingDatesModel->save();
                if(!$bookingDatesModel->save())
                {
                    echo "<pre>";
                    print_r($bookingDatesModel->errors);
                    exit();
                }  
            }
        }

        echo json_encode([
                'bookings_items' => $this->renderAjax('side_bar_cart_view'),
                'error' => $error_str,
            ]);
    }

    public function actionUpdateBookingCartItem()
    {
        $bookingItems = $_POST['bookingItems'];

        foreach ($bookingItems as $key => $bookingDate) 
        {
            $has_record = 0;
            $bookingDatesModel = BookingDatesCart::findOne(['id' => $bookingDate['id']]);
            $itemModel = BookableItems::findOne(['id' => $bookingDatesModel->item_id]);

            if($itemModel->destination->pricing) // ************* Capacity Pricing ************ //
            {
                $bookingDatesModel->no_of_adults = $bookingDate['no_of_adults'];
                $bookingDatesModel->no_of_children = $bookingDate['no_of_children'];
                if(isset($bookingDate['person_no']) && $bookingDate['quantity'] > 0)
                {
                    $custom_rate = isset($bookingDate['custom_rate'])?$bookingDate['custom_rate']:NULL;   
                    $bookingDatesModel->person_no = $bookingDate['person_no'];

                    $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $bookingDatesModel->rate_id, 'person_no' => $bookingDate['person_no']]);
                    if($custom_rate == $capacityPricingModel->price)
                    {
                        $bookingDatesModel->person_price = $capacityPricingModel->price;
                        $bookingDatesModel->custom_rate = null;
                    }
                    else
                    {
                        $bookingDatesModel->person_price = $capacityPricingModel->price;
                        $bookingDatesModel->custom_rate = $custom_rate;
                    }
                    //$bookingDatesModel->person_price = $capacityPricingModel->price;
                    $has_record = 1;
                }
            }
            else // ************* First/Additional ************ //
            {
                if(isset($bookingDate['no_of_adults']) && isset($bookingDate['no_of_children']) && 
                    ($bookingDate['no_of_adults']!=0 || $bookingDate['no_of_children']!=0) && $bookingDate['quantity'] > 0)
                {
                    $bookingDatesModel->no_of_adults = $bookingDate['no_of_adults'];
                    $bookingDatesModel->no_of_children = $bookingDate['no_of_children'];
                    $has_record = 1;
                }
            }

            if($has_record)
            {
                $bookingDatesModel->user_id = $bookingDate['user_id'];

                if(empty($bookingDatesModel->user_id))
                {   
                    //########### saving guest data ###########//

                    $bookingDatesModel->guest_first_name = $bookingDate['guest_first_name'];
                    $bookingDatesModel->guest_last_name = $bookingDate['guest_last_name'];
                    $bookingDatesModel->guest_country_id = $bookingDate['guest_country'];

                    //########################################//
                }
                else
                {
                    $bookingDatesModel->guest_first_name = NULL;
                    $bookingDatesModel->guest_last_name = NULL;
                    $bookingDatesModel->guest_country_id = $bookingDate['guest_country'];
                }

                $bookingDatesModel->estimated_arrival_time_id = $bookingDate['estimated_arrival_time_id'];
                $bookingDatesModel->travel_partner_id = $bookingDate['travel_partner_id'];
                $bookingDatesModel->offers_id = $bookingDate['offer_id'];
                $bookingDatesModel->beds_combinations_id = !empty($bookingDate['beds_combinations_id'])? $bookingDate['beds_combinations_id']:NULL;
                $bookingDatesModel->quantity = $bookingDate['quantity'];
                $bookingDatesModel->booking_cancellation = $bookingDate['booking_cancellation'];
                $bookingDatesModel->flag_id = $bookingDate['flag_id'];
                $bookingDatesModel->status_id = $bookingDate['status_id'];
                $bookingDatesModel->confirmation_id = $bookingDate['confirmation_id'];
                $bookingDatesModel->total = $bookingDate['total'];
                $bookingDatesModel->price_json = str_replace("'", '"', $bookingDate['price_json']);

                if($bookingDatesModel->save())
                {
                    $bookingDatesModel->discount_codes = isset($bookingDate['discount_ids'])?$bookingDate['discount_ids']:'';
                    $bookingDatesModel->upsell_items = isset($bookingDate['upsell_ids'])?$bookingDate['upsell_ids']:'';
                    $bookingDatesModel->special_requests = isset($bookingDate['special_requests_ids'])?$bookingDate['special_requests_ids']:'';
                    
                    BookingsDiscountCodes::deleteAll(['booking_date_id' => $bookingDate['id']]);
                    BookingsUpsellItems::deleteAll(['booking_date_id' => $bookingDate['id']]);
                    BookingSpecialRequests::deleteAll(['booking_date_id' => $bookingDate['id']]);

                    if(!empty($bookingDatesModel->discount_codes))
                    {
                        foreach ($bookingDatesModel->discount_codes as $key => $value) 
                        {
                            $arr = explode(':', $value);
                            $discount_id = $arr[0];
                            $discount_code_extra_bed_quantity = $arr[1];

                            $discount_code_model = DiscountCode::findOne(['id' => $discount_id]);

                            $obj = new BookingsDiscountCodes();
                            $obj->discount_id = $discount_id;
                            $obj->price = $discount_code_model->amount;
                            $obj->booking_date_id = $bookingDatesModel->id;

                            if($discount_code_extra_bed_quantity != 'undefined' && ($discount_code_model->quantity_type == 1 || $discount_code_model->quantity_type == 2))
                            {
                                $obj->extra_bed_quantity = $discount_code_extra_bed_quantity;
                            }

                            $obj->save();
                        }
                    }

                    if(!empty($bookingDatesModel->upsell_items))
                    {
                        foreach ($bookingDatesModel->upsell_items as $key => $value) 
                        {
                            $arr = explode(':', $value);
                            $upsell_id = $arr[0];
                            $upsell_extra_bed_quantity = $arr[1];

                            $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $bookingDatesModel->rate_id, 'upsell_id' => $upsell_id]);

                            $obj = new BookingsUpsellItems();
                            $obj->upsell_id = $upsell_id;
                            $obj->price = $ratesUpsellModel->price;
                            $obj->booking_date_id = $bookingDatesModel->id;

                            if($ratesUpsellModel->upsell->type == 7 && $upsell_extra_bed_quantity != 'undefined')
                            {
                                $obj->extra_bed_quantity = $upsell_extra_bed_quantity;
                            }

                            $obj->save();
                        }
                    }

                    if(!empty($bookingDatesModel->special_requests))
                    {
                        foreach ($bookingDatesModel->special_requests as $key => $value) 
                        {
                            $model = new BookingSpecialRequests();
                            $model->booking_date_id = $bookingDatesModel->id;
                            $model->request_id = $value;
                            $model->save();
                        }
                    }
                }  
            }
        }
    
        echo json_encode([
            'bookings_items' => $this->renderAjax('side_bar_cart_view'),
            'errors' => $bookingDatesModel->getErrors(),
        ]);

        Yii::$app->session->setFlash('success', 'Item updated successfully.');
    }

    public function actionDeleteBookingItem($id)
    {
        $model = BookingsItemsCart::findOne(['id' => $id]);

        if(!empty($model))
            $model->delete();

        echo json_encode([
            'bookings_items' => $this->renderAjax('side_bar_cart_view'),
        ]);
    }

    public function actionShowBookingItem($id)
    {
        echo json_encode([
            'booking_item_view' => $this->renderAjax('booking_item_view',[
                    'id' => $id,
                ]),
        ]);
    }

    public function actionDeleteCartItems()
    {
        BookingsItemsCart::deleteAll(['created_by' => Yii::$app->user->identity->id]);

        echo json_encode([
            'bookings_items' => $this->renderAjax('side_bar_cart_view'),
        ]);
    }

    public function actionUpdateBookingCartItemsPrices($travel_partner_id='',$offers_id='')
    {
        $bookingsItemsModel = BookingsItemsCart::find()->where(['created_by' => Yii::$app->user->identity->id])->all();

        $empty = 0;

        if(!empty($bookingsItemsModel))
        {
            foreach ($bookingsItemsModel as $key => $bImodel) 
            {
                $bookingDates = $bImodel->bookingDates;

                if(!empty($bookingDates))
                {
                    foreach ($bookingDates as $key => $bDateModel) 
                    {
                        if(!empty($travel_partner_id) || !empty($offers_id))
                        {
                            BookingsDiscountCodesCart::deleteAll(['booking_date_id' => $bDateModel->id]);
                        }

                        $this->updatePricesOnChangeDiscountType($bDateModel,$travel_partner_id,$offers_id);
                    }
                }
            }
        }
        else
        {
            $empty = 1;
        }   

        echo json_encode([
            'bookings_items' => $this->renderAjax('side_bar_cart_view'),
            'empty' => $empty,
        ]);
    }

    public function updatePricesOnChangeDiscountType($model='',$travel_partner_id='',$offers_id='')
    {
        $rateModel = Rates::findOne(['id' => $model->rate_id]);
        
        $totalPrice = 0;
        $adults_total = 0;
        $children_total = 0;
        $travel_partner_discount = 0;
        $offer_discount = 0;

        // ***************** Calculate Upsell Price ***************** //

        $upsell_total = 0;
        $upsell_ids = [];
        $upsellModel = BookingsUpsellItems::find()->where(['booking_date_id' => $model->id])->all();

        foreach ($upsellModel as $key => $obj) 
        {
            $upsell_ids[] = $obj->upsell_id.':'.$obj->extra_bed_quantity;   
        }

        if(!empty($upsell_ids))
        {
            foreach ($upsell_ids as $key => $value) 
            {
                $arr = explode(':', $value);
                $upsell_id = $arr[0];
                $upsell_extra_bed_quantity = $arr[1];

                $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $model->rate_id, 'upsell_id' => $upsell_id]);

                if($ratesUpsellModel->upsell->type == 7)
                {
                    $upsell_total = $upsell_total + ($ratesUpsellModel->price * $upsell_extra_bed_quantity);
                }
                else
                {
                    $upsell_total = $upsell_total + $ratesUpsellModel->price;
                }
            }
        }

        if(!empty($model->person_no))
        {
            $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $model->rate_id, 'person_no' => $model->person_no]);
            if(!empty($capacityPricingModel))
            {
                if(!empty($capacityPricingModel->price))
                {
                    $rate_override = RatesOverride::findOne(['date' => $model->date, 'rate_id' => $model->rate_id]);
                    if(empty($rate_override))
                    {
                        $totalPrice = $capacityPricingModel->price;
                    }
                    else
                    {
                        if($model->person_no==1)
                        {
                            $totalPrice = $rate_override->single;
                        }
                        if($model->person_no==2)
                        {
                            $totalPrice = $rate_override->double;
                        }
                        if($model->person_no==3)
                        {
                            $totalPrice = $rate_override->triple;
                        }
                        if($model->person_no==4)
                        {
                            $totalPrice = $rate_override->quad;
                        }
                    }
                    
                }
            }
        }

        else if(!empty($model->no_of_adults) && !empty($model->no_of_children))
        {
            $no_of_adults = $model->no_of_adults;
            $no_of_children = $model->no_of_children;

            // ***************** Calculate Adult Price **************** //

            if($no_of_adults>1)
            {
                $adults_total = $rateModel->item_price_first_adult;
                $no_of_adults = $no_of_adults - 1;
                $adults_total = $adults_total + ($rateModel->item_price_additional_adult * $no_of_adults);
            }
            else if($no_of_adults == 1)
            {
                $adults_total = $rateModel->item_price_first_adult;
            }

            // ***************** Calculate Children Price ***************** //

            if($no_of_children>1)
            {
                $children_total = $rateModel->item_price_first_child;
                $no_of_children = $no_of_children - 1;
                $children_total = $children_total + ($rateModel->item_price_additional_child * $no_of_children);
            }
            else if($no_of_children == 1)
            {
                $children_total = $rateModel->item_price_first_child;
            }

            $totalPrice = $adults_total + $children_total;
        }

        $vat_rate = $rateModel->vat_rate;
        //$vat_rate = str_replace(',', '', $vat_rate);
        
        $lodgingTax = $rateModel->lodging_tax_per_night;
        $lodgingTax = $lodgingTax * ($model->quantity * $model->no_of_nights);

        $discount_code_flag = 0;
        $discount_arr = [
            'discounted_amount' => '',
            'discounted_total' => '',
        ];

        // ***************** Apply Discount Price ***************** //

        if(!empty($travel_partner_id))
        {
            $travel_partner_model = DestinationTravelPartners::findOne(['id' => $travel_partner_id ]);
            $travel_partner_discount = $travel_partner_model->comission;
            $model->travel_partner_id = $travel_partner_id;
            $model->offers_id = NULL;
        }
        else if(!empty($offers_id))
        {
            $offer_model = Offers::findOne(['id' => $offers_id]);
            $offer_discount = $offer_model->commission;
            $model->travel_partner_id = NULL;
            $model->offers_id = $offers_id;
        }
        else
        {
            $get_discount_codes = BookingsDiscountCodes::find()->where(['booking_date_id' => $model->id])->all();
            $discount_ids = [];

            if(!empty($get_discount_codes))
            {
                foreach ($get_discount_codes as $key => $value) 
                {
                    $discount_ids[] = $value['discount_id'].':'.$value['extra_bed_quantity'];
                }

                $discount_code_flag = 1;
                $discount_arr = $this->applyDicountCode($totalPrice,$upsell_total,$discount_ids);
                $totalPrice = $discount_arr['discounted_total'];
            }
        }

        $price_json =  $this->calculatePriceByIcelandFormulaNew($totalPrice,$upsell_total,$vat_rate,$lodgingTax,$travel_partner_discount,$discount_code_flag,$offer_discount,$model->quantity,$model->no_of_nights,$discount_arr['discounted_amount'],$travel_partner_id);

        $arr = json_decode($price_json);


        $model->total = $arr->orgPrice;
        $model->price_json = $price_json;
        $model->update();
    }

    public function updatePricesForBookingItem($model='')
    {
        $rateModel = Rates::findOne(['id' => $model->rates_id]);
        
        $totalPrice = 0;
        $adults_total = 0;
        $children_total = 0;
        $travel_partner_discount = 0;
        $offer_discount = 0;

        // ***************** Calculate Upsell Price ***************** //

        $upsell_total = 0;
        $upsell_ids = [];
        $upsellModel = BookingsUpsellItems::find()->where(['bookings_items_id' => $model->id])->all();

        foreach ($upsellModel as $key => $obj) 
        {
            $upsell_ids[] = $obj->upsell_id.':'.$obj->extra_bed_quantity;   
        }

        if(!empty($upsell_ids))
        {
            foreach ($upsell_ids as $key => $value) 
            {
                $arr = explode(':', $value);
                $upsell_id = $arr[0];
                $upsell_extra_bed_quantity = $arr[1];

                $ratesUpsellModel = RatesUpsell::findOne(['rates_id' => $model->rates_id, 'upsell_id' => $upsell_id]);

                if($ratesUpsellModel->upsell->type == 7)
                {
                    $upsell_total = $upsell_total + ($ratesUpsellModel->price * $upsell_extra_bed_quantity);
                }
                else
                {
                    $upsell_total = $upsell_total + $ratesUpsellModel->price;
                }
            }
        }

        if(!empty($model->no_of_adults) && !empty($model->no_of_children))
        {
            $no_of_adults = $model->no_of_adults;
            $no_of_children = $model->no_of_children;

            // ***************** Calculate Adult Price **************** //

            if($no_of_adults>1)
            {
                $adults_total = $rateModel->item_price_first_adult;
                $no_of_adults = $no_of_adults - 1;
                $adults_total = $adults_total + ($rateModel->item_price_additional_adult * $no_of_adults);
            }
            else if($no_of_adults == 1)
            {
                $adults_total = $rateModel->item_price_first_adult;
            }

            // ***************** Calculate Children Price ***************** //

            if($no_of_children>1)
            {
                $children_total = $rateModel->item_price_first_child;
                $no_of_children = $no_of_children - 1;
                $children_total = $children_total + ($rateModel->item_price_additional_child * $no_of_children);
            }
            else if($no_of_children == 1)
            {
                $children_total = $rateModel->item_price_first_child;
            }

            $totalPrice = $adults_total + $children_total;
        }
        else if(!empty($model->person_no))
        {
            $capacityPricingModel = RatesCapacityPricing::findOne(['rates_id' => $model->rates_id, 'person_no' => $model->person_no]);
            if(!empty($capacityPricingModel))
            {
                if(!empty($capacityPricingModel->price))
                {
                    $totalPrice = $capacityPricingModel->price;
                }
            }
        }

        $vat_rate = $rateModel->unit->destination->destinationsFinancials->vat_rate;
        $vat_rate = str_replace(',', '', $vat_rate);
        
        $lodgingTax = $rateModel->unit->destination->destinationsFinancials->lodging_tax_per_night;
        $lodgingTax = $lodgingTax * ($model->quantity * $model->no_of_nights);

        $discount_code_flag = 0;
        $discount_arr = [
            'discounted_amount' => '',
            'discounted_total' => '',
        ];

        // ***************** Apply Discount Price ***************** //

        if(!empty($model->travel_partner_id))
        {
            $travel_partner_model = DestinationTravelPartners::findOne(['id' => $model->travel_partner_id]);
            $travel_partner_discount = $travel_partner_model->comission;
        }

        if(!empty($model->offers_id))
        {
            $offer_model = Offers::findOne(['id' => $model->offers_id]);
            $offer_discount = $offer_model->commission;
        }
        
        $get_discount_codes = BookingsDiscountCodes::find()->where(['bookings_items_id' => $model->id])->all();
        $discount_ids = [];

        if(!empty($get_discount_codes))
        {
            foreach ($get_discount_codes as $key => $value) 
            {
                $discount_ids[] = $value['discount_id'].':'.$value['extra_bed_quantity'];
            }

            $discount_code_flag = 1;
            $discount_arr = $this->applyDicountCode($totalPrice,$upsell_total,$discount_ids);
            $totalPrice = $discount_arr['discounted_total'];
        }

        $price_json =  $this->calculatePriceByIcelandFormula($totalPrice,$upsell_total,$vat_rate,$lodgingTax,$travel_partner_discount,$discount_code_flag,$offer_discount,$model->quantity,$model->no_of_nights,$discount_arr['discounted_amount']);

        $arr = json_decode($price_json);

        $model->total = $arr->orgPrice;
        $model->price_json = $price_json;
        $model->update();
    }

    public function actionSearchBookingItem()
    {
        if(Yii::$app->request->isAjax && isset($_GET['q']))
        {
            $result = [
                'items' => []
            ];

            $search = $_GET['q'];
            $group_ids = [];

            $query1 = BookingItemGroup::find()
                                        ->where(['!=','booking_group_id' ,$_GET['group_id']])
                                        ->distinct('booking_group_id')
                                        ->all();
            
            foreach ($query1 as $key => $value) 
            {
                $count = BookingItemGroup::find()
                                        ->where(['booking_group_id' => $value->booking_group_id])
                                        ->count();
                if($count == 1)
                    $group_ids[] = $value->booking_group_id;
            }

            $query = BookingItemGroup::find()
                            ->joinWith('bookingItem')
                            ->where("bookings_items.id LIKE :query")
                            ->addParams([':query'=> $search.'%'])
                            ->andWhere(['booking_group_id' => $group_ids])
                            ->andWhere(['deleted_at' => 0])
                            ->andWhere(['provider_id' => $_GET['provider_id']])
                            ->distinct('booking_item_id')
                            ->all();

            foreach ($query as $item)
            {
                $result['items'][] =
                [
                    'id' => $item->booking_item_id,
                    'text' => $item->bookingItem->id.'-'.$item->bookingItem->provider->name.' - '.$item->bookingItem->item->itemType->name,
                ];
            }

            echo json_encode($result);
        }
    }

    public function actionLinkItemToGroup()
    {
        $bookingGroupModel = BookingGroup::findOne(['id' => $_POST['group_id']]);
        $bookingItemsModel = BookingsItems::findOne(['id' => $_POST['id']]);

        // $bookingItemsModel->confirmation_id = !empty($bookingGroupModel->confirmation_id)?$bookingGroupModel->confirmation_id:$bookingItemsModel->confirmation_id;
        // $bookingItemsModel->booking_cancellation = !empty($bookingGroupModel->cancellation_id)?$bookingGroupModel->cancellation_id:$bookingItemsModel->booking_cancellation;
        $bookingItemsModel->flag_id = !empty($bookingGroupModel->flag_id)?$bookingGroupModel->flag_id:$bookingItemsModel->flag_id;
        $bookingItemsModel->status_id = !empty($bookingGroupModel->status_id)?$bookingGroupModel->status_id:$bookingItemsModel->status_id;
        $bookingItemsModel->travel_partner_id = !empty($bookingGroupModel->travel_partner_id)?$bookingGroupModel->travel_partner_id:$bookingItemsModel->travel_partner_id;
        $bookingItemsModel->offers_id = !empty($bookingGroupModel->offers_id)?$bookingGroupModel->offers_id:$bookingItemsModel->offers_id;
        $bookingItemsModel->estimated_arrival_time_id = !empty($bookingGroupModel->estimated_arrival_time_id)?$bookingGroupModel->estimated_arrival_time_id:$bookingItemsModel->estimated_arrival_time_id;

        $previous_group_id = $bookingItemsModel->bookingItemGroups->booking_group_id;

        $bookingItemGroupModel = $bookingItemsModel->bookingItemGroups;
        $bookingItemGroupModel->booking_group_id = $_POST['group_id'];
        $bookingItemGroupModel->update();

        $previousGroupModel = BookingGroup::findOne(['id' => $previous_group_id]);
        if(empty($previousGroupModel->bookingItemGroups))
        {
            $previousGroupModel->delete();
        }

        if($bookingItemsModel->save())
        {
            // ************** update prices on change discount type ************* //

            foreach ($bookingItemsModel->bookingDatesSortedAndNotNull as $bDate) 
            {   
                $bookingItemsModel->flag_id = $bDate->flag_id;
                $bookingItemsModel->status_id = $bDate->status_id;
                $bookingItemsModel->travel_partner_id = $bDate->travel_partner_id;
                $bookingItemsModel->offers_id = $bDate->offers_id;
                $bookingItemsModel->estimated_arrival_time_id = $bDate->estimated_arrival_time_id;
                $bDate->save();
                if(!empty($bDate->travel_partner_id) || !empty($bDate->offers_id))
                {
                    BookingsDiscountCodes::deleteAll(['booking_date_id' => $bDate->id]);
                }

                $this->updatePricesOnChangeDiscountType($bDate,$bDate->travel_partner_id,$bDate->offers_id);

                if(!empty($bookingGroupModel->bookingGroupSpecialRequests))
                {
                    BookingSpecialRequests::deleteAll(['booking_date_id' => $bDate->id]);
                    $bDate->special_requests = $bookingGroupModel->bookingGroupSpecialRequests;

                    foreach ($bDate->special_requests as $key => $value) 
                    {
                        $model = new BookingSpecialRequests();
                        $model->booking_date_id = $bDate->id;
                        $model->request_id = $value->special_request_id;
                        $model->save();
                    }
                }
            }
            
        }

        $group_balance = 0;
        foreach ($bookingGroupModel->bookingItemGroups as $group_item) 
        {
            $group_balance += $group_item->bookingItem->balance;
        }
        $bookingGroupModel->balance = $group_balance;
        $bookingGroupModel->save();
    }

    public function actionUnlinkItemFromGroup($id)
    {
        $bookingItemsModel = BookingsItems::findOne(['id' => $id]);
        $previous_group_id = $bookingItemsModel->bookingItemGroups->booking_group_id;
        $orgBookingGroupModel = $bookingItemsModel->bookingItemGroups->bookingGroup;

        $total_items = BookingItemGroup::find()->where(['booking_group_id' => $previous_group_id])->count();

        if($total_items > 1)
        {
            /*$bookingItemsModel->confirmation_id = $bookingItemsModel->item->booking_confirmation_type_id;
            $bookingItemsModel->booking_cancellation = $bookingItemsModel->item->general_booking_cancellation;
            $bookingItemsModel->flag_id = !empty($bookingItemsModel->item->booking_type_id)?$bookingItemsModel->item->booking_type_id:$bookingItemsModel->flag_id;
            $bookingItemsModel->status_id = !empty($bookingItemsModel->item->booking_status_id)?$bookingItemsModel->item->booking_status_id:$bookingItemsModel->status_id;*/

            $bookingGroupModel = new BookingGroup();
            $bookingGroupModel->balance = $bookingItemsModel->balance;
            $bookingGroupModel->save();

            $bookingGroupModel->group_name = 'Group-'.$bookingGroupModel->id;
            $bookingGroupModel->update();

            $bookingItemGroupModel = $bookingItemsModel->bookingItemGroups;
            $bookingItemGroupModel->booking_group_id = $bookingGroupModel->id;
            $bookingItemGroupModel->update();

            $group_balance = 0;
            foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
            {
                $group_balance += $group_item->bookingItem->balance;
            }
            $orgBookingGroupModel->balance = $group_balance;
            $orgBookingGroupModel->save();
            //$bookingItemsModel->save();

            return $this->redirect(['update-group',
                        'id' => $previous_group_id,
                    ]);
        }
        else
        {
            return 'Last item cannot be unlink from group.';
        }
    }

    public function actionCalendar($destination_id = null, $bookable_item_id = null)
    {
        return $this->render('booking_calendar', [
            'destination_id' => $destination_id,
            'bookable_item_id' => $bookable_item_id
        ]);
    }

    public function actionBookingIssues($destination_id = null, $exp_date = null)
    {
        if($exp_date == NULL)
        {
            $date = date("Y-m-d");
            //$date = explode("-", $current_date);
            //$date = $date[2];
        }
        else
        {
            $date = $exp_date;
            //$date = $date[2];   
        }
        // echo "<pre>";
        // print_r($date[2]);
        // exit();
        return $this->render('booking_issues',[
            'destination_id' => $destination_id,
           // 'bookable_item_id' => $bookable_item_id,
            'date'  => $date,
            // 'date'  => 25-1,
        ]);
    }

    public function actionGetDestinationBookedItems() // use for calendar
    {
        $session = Yii::$app->session;
        $session->open();
        
        if(isset($_POST['item_id']) && $_POST['item_id']!='show_all')
        {
            $bookingsItemsModel = BookingsItems::find()
                            ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id'],'item_id' => $_POST['item_id']])
                            ->orderBy('arrival_date ASC')
                            ->all();
        }
        else
        {
            $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
                                ->orderBy('arrival_date ASC')
                                ->all();
        }
    
        $bookings_items_arr = [];

        if(!empty($bookingsItemsModel))
        {
            $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            $closed_dates_arr = array();
            if(!empty($closed_dates))
            {
                foreach ($closed_dates  as $date) 
                {
                    array_push($closed_dates_arr, $date->date);
                }
            }

            foreach ($bookingsItemsModel as $key => $model) 
            {
                $bookings_items_arr[] = $this->generateCalendarEvent($model);
            }

            if(isset($_POST['item_id']))
            {
                echo json_encode([
                    'bookings_items' => json_encode($bookings_items_arr),
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                ]);

                $params['provider_id'] =  $_POST['destination_id'];
                $params['item_id'] = $_POST['item_id'];
                $session[Yii::$app->controller->id.'-booking-calendar-values'] = json_encode($params);
            }
            else
            {
                echo json_encode([
                    'bookings_items' => json_encode($bookings_items_arr),
                    'bookable_items' => $this->renderAjax('bookable_items',['provider_id' => $_POST['destination_id']]),
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                ]);

                $params['provider_id'] =  $_POST['destination_id'];
                $params['item_id'] = 'show_all';
                $session[Yii::$app->controller->id.'-booking-calendar-values'] = json_encode($params);
            }
        }
        else
        {
            echo json_encode([
                'empty' => 1,
            ]); 
        }
    }

    public function actionGetDestinationBookedItemsSchedular() // use for Schedular
    {
        $session = Yii::$app->session;
        $session->open();
        
        $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
                                ->all();
    
        $bookings_items_arr = [];

        if(!empty($bookingsItemsModel))
        {
            $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            $closed_dates_arr = array();
            if(!empty($closed_dates))
            {
                foreach ($closed_dates  as $date) 
                {
                    array_push($closed_dates_arr, $date->date);
                }
            }

            $schedular_resources = $this->generateSchedularResources(Destinations::findOne(['id' => $_POST['destination_id'] ]));

            foreach ($bookingsItemsModel as $key => $model) 
            {
                $bookings_items_arr[] = $this->generateSchedularEvent($model);
            }

            $notifications_array = array();

            $notifications = Notifications::find()->where(['notification_type' => Notifications::UNIT_OVERBOOKING ])->all();

            if(!empty($notifications))
            {
                foreach ($notifications as $noti) 
                {
                    array_push($notifications_array, $noti->date);
                }
            }
            

            echo json_encode([
                'bookings_items' => json_encode($bookings_items_arr),
                //'bookable_items' => $this->renderAjax('bookable_items',['provider_id' => $_POST['destination_id']]),
                'resources'     => json_encode($schedular_resources),
                'empty' => 0,
                'date' => date('Y-m-d'),
                'closed_dates' => $closed_dates_arr,
                'notifications' => json_encode($notifications_array)
            ]);
            $params['provider_id'] =  $_POST['destination_id'];
            $session[Yii::$app->controller->id.'-booking-schedular-values'] = json_encode($params);
                
        }
        else
        {
            echo json_encode([
                'empty' => 1,
            ]); 
        }
    }

    public function generateCalendarEvent($model)
    {
        $booking_item['id'] = $model->id;
        $booking_item['group_id'] = isset($model->bookingItemGroups)?$model->bookingItemGroups->bookingGroup->id:null;
        $name = '';
        $name .= !empty($model->guest_user_first_name)?$model->guest_user_first_name.' ':'';
        $name .= !empty($model->guest_user_last_name)?$model->guest_user_last_name:'';

        if(empty($name))
            $name = 'Unknown';

        // removed following line 
        // .$model->bookingItemGroups->bookingGroup->group_name
        $booking_item['title'] = $name.' - '.$model->item->itemType->name.' - ';
        $booking_item['start'] = $model->arrival_date;
        $booking_item['end'] = $model->departure_date;
        $booking_item['backgroundColor'] = $model->item->background_color;
        $booking_item['textColor'] = $model->item->text_color;
        $booking_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'cal']);

        return $booking_item;
    }

    public function generateSchedularEvent($model)
    {
        $booking_item['id'] = $model->id;
        $booking_date = BookingDates::findOne(['booking_item_id' => $model->id]);
        $name = '';
        $name .= !empty($booking_date->guest_first_name)?$booking_date->guest_first_name.' ':'';
        $name .= !empty($booking_date->guest_last_name)?$booking_date->guest_last_name:'';

        if(empty($name))
            $name = 'Unknown';

        // removed following line 
        // .$model->bookingItemGroups->bookingGroup->group_name
        $booking_item['title'] = $model->id.' - '.$name;
        $booking_item['resourceId'] = isset($model->itemName)?$model->itemName->id:$model->item_id.$model->provider_id;
        $booking_item['start'] = $model->arrival_date;
        $booking_item['end'] = $model->departure_date;
        $booking_item['borderColor'] = '#000000';
        $booking_item['textColor'] = $model->item->text_color;
        $booking_item['url'] = Url::to(['/bookings/update','id' => $model->id,'check' =>'sch']);

        return $booking_item;
    }

    public function generateSchedularResources($destination)
    {
        $resources = array();
        if(!empty($destination))
        {
            foreach ($destination->bookableItems as $bookable_item) 
            {
                $item_names_count = count($bookable_item->bookableItemsNames);
                $counter = 1;
                foreach ($bookable_item->bookableItemsNames as $item_name) 
                {   
                    $item_array = array();
                    $item_array['id'] = $item_name->id;
                    $item_array['item'] = $bookable_item->itemType->name;
                    $item_array['title'] = $item_name->item_name;
                    $item_array['eventColor'] = $bookable_item->background_color;
                    $item_array['bookable_item_id'] = $bookable_item->id;
                    $item_array['background_color'] = $bookable_item->background_color;
                    $item_array['text_color'] = $bookable_item->text_color;

                    array_push($resources, $item_array);
                    if($counter == $item_names_count)
                    {
                        $item_array = array();
                        $item_array['id'] = $bookable_item->id.$destination->id;
                        $item_array['item'] = $bookable_item->itemType->name;
                        $item_array['title'] = "No Unit Assigned";
                        $item_array['eventColor'] = $bookable_item->background_color;
                        $item_array['bookable_item_id'] = $bookable_item->id;
                        $item_array['background_color'] = $bookable_item->background_color;
                        $item_array['text_color'] = $bookable_item->text_color;

                        array_push($resources, $item_array);
                    }
                    $counter++;
                }
            }
            return $resources;
        }
    }
    
    public function actionEditOrDragBookedItem()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $model = BookingsItems::findOne(['id' => $_POST['id']]);
        //Notifications::deleteAll(['booking_item_id' => $model->id ]);
        Notifications::deleteAll(['AND', 'booking_item_id = :booking_item_id', ['NOT IN', 'notification_type', [Notifications::UNIT_OVERBOOKING,Notifications::DESTINATION_OVERBOOKING]]], [':booking_item_id' => $model->id]);

        $date1=date_create($_POST['departure_date']);
        $date2=date_create($_POST['arrival_date']);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");
        $date = $_POST['arrival_date'];

        $date_exception_flag = 0;
        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $model->provider_id, 'state' => 0])->all();

        $dates_arr = array();
        $date1 = $_POST['arrival_date'];
        $date1 = str_replace('/', '-', $date1);

        // echo "<pre>";
        // print_r($difference);
        // exit();
       
        // $date = DateTime::createFromFormat('d/m/Y', strtotime($date));
        $date1 = date("Y-m-d", strtotime($date));
        // echo "<pre>";
        // print_r($date);
        // exit();
        for ($i=0; $i < $difference ; $i++)
        {
            array_push($dates_arr, $date1);

            $date1 = strtotime("+1 day", strtotime($date1));
            $date1 = date("Y-m-d", $date1);

        }

        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date2) 
            {
                if(in_array($date2->date, $dates_arr))
                {
                    $date_exception_flag = 1;
                    break;
                }
            }
        }

       // $item_availability = BookingsItems::itemsAvailabilityForCalendar($difference,$_POST['arrival_date'],$model->item_id,$model->id);
        if($date_exception_flag == 1)
        {
            return 'date_exception';
        }
        else
        {
            $old_dates = array();
            $old_adults = array();
            $old_children = array();
            $old_no_of_nights = $model->bookingDatesSortedAndNotNull[0]->no_of_nights;
            $old_beds_combination = $model->bookingDatesSortedAndNotNull[0]->bedsCombinations->combination;
            $old_first_name = $model->bookingDatesSortedAndNotNull[0]->guest_first_name;
            $old_last_name = $model->bookingDatesSortedAndNotNull[0]->guest_last_name;
            $old_country = Countries::findOne([$model->bookingDatesSortedAndNotNull[0]->guest_country_id])->name;
            $old_cancellation = $model->bookingDatesSortedAndNotNull[0]->booking_cancellation;
            $old_confirmation_id = $model->bookingDatesSortedAndNotNull[0]->confirmation_id;
            $old_estimated_arrival_time = $model->bookingDatesSortedAndNotNull[0]->estimated_arrival_time_id;
            $old_person_no = array();
            $old_person_price = array();

            foreach ($model->bookingDatesSortedAndNotNull as  $value) 
            {
                array_push($old_dates, (string)$value->date);
                array_push($old_adults, (string)$value->no_of_adults);
                array_push($old_children, (string)$value->no_of_children);
                array_push($old_person_no, (string)$value->person_no);
                array_push($old_person_price, (string)$value->person_price);

            }

            $booking_date = BookingDates::findOne(['booking_item_id' => $model->id]);

            //$ratesArr = BookingsItems::checkAvailableRates($model->item->id,$_POST['arrival_date'],$difference);
            $ratesArr = BookingsItems::checkAvailableRates($booking_date->rate->unit_id,$_POST['arrival_date'],$difference);
            $availableRates = $ratesArr['availableRates'];
            $availableRatesIds = $ratesArr['availableRatesIds'];

            $bookedDates = $model->bookingDatesSortedAndNotNull;

            $bookedRatesRecord = array();

            $rate_flag = 0; 
            $item_names_flag = 0;
            foreach ($bookedDates as $value) 
            {
                if(!in_array($value->rate_id, $bookedRatesRecord))
                {
                    array_push($bookedRatesRecord, $value->rate_id);
                }
            }
            
            $new_booking_rates = array();
            
            foreach ($bookedRatesRecord as  $booked_rate) 
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    if(!empty($value))
                    {
                        $day_rates = explode(',', $value);
                        if(in_array($booked_rate, $day_rates))
                        {
                            if(empty($new_booking_rates[$key]))
                            {
                                $new_booking_rates[$key] = $day_rates[array_search($booked_rate,$day_rates)];
                            }
                            
                        }
                    }
                    else
                    {
                        $rate_flag = 1;
                    }
                    
                }
            }

            $rates_unavailable =array();
            foreach ($availableRatesIds as $key => $value) 
            {
                if(!array_key_exists($key, $new_booking_rates))
                {
                    array_push($rates_unavailable,$key);
                    $rate_flag = 1;
                }
            } 

            $available_item_names = BookingsItems::checkAvailableItemNamesIncludedSaved($model->item->id,$_POST['arrival_date'],$difference,$model->id);
           // $availableItemNames = BookingsItems::checkAvailableItemNamesIncludedSaved($item->id,$arrival_date,$difference,$model->id);

            if(!in_array($model->item_name_id, $available_item_names))
            {
                $item_names_flag = 1;
            }
           //  echo "<pre>";
           // // print_r($bookedRatesRecord);
           //  print_r($availableRatesIds);
            
            
           //  print_r($new_booking_rates);
           //  // print_r($rate_flag);
           //  // print_r($item_names_flag);
           //  print_r(BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $_POST['arrival_date']]));
           //  exit();

            if($rate_flag == 0 )
            {
                //$org_booking = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $model->arrival_date]);
                
                // $org_booking_upsell = $org_booking->bookingsUpsellItems;
                // $org_booking_discount_codes = $org_booking->bookingsDiscountCodes;
                // $org_booking_requests = $org_booking->bookingSpecialRequests;
                //  echo "<pre>";
                // print_r($org_booking->attributes);
                // exit();
                foreach ($new_booking_rates as $key => $value) 
                {
                    $booking_date_obj = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $key]);


                    if(empty($booking_date_obj))
                    {
                        
                        $org_booking = BookingDates::findOne(['booking_item_id' => $model->id,'rate_id' => $value]);
                        $new_booking_date_obj = new BookingDates();
                        $new_booking_date_obj->booking_item_id = $org_booking->booking_item_id;
                        $new_booking_date_obj->date = $key;
                        $new_booking_date_obj->item_id = $org_booking->item_id;
                        if($item_names_flag == 1 )
                        {
                            $new_booking_date_obj->item_name_id = null;
                            
                        }
                        else
                        {
                            $new_booking_date_obj->item_name_id = $org_booking->item_name_id;
                        }
                        $new_booking_date_obj->rate_id = $org_booking->rate_id;

                        $new_booking_date_obj->beds_combinations_id = $org_booking->beds_combinations_id;
                        $new_booking_date_obj->quantity = $org_booking->quantity;
                        $new_booking_date_obj->travel_partner_id = $org_booking->travel_partner_id;
                        $new_booking_date_obj->offers_id = $org_booking->offers_id; 
                        $new_booking_date_obj->booking_cancellation = $org_booking->booking_cancellation;
                        $new_booking_date_obj->flag_id = $org_booking->flag_id;
                        $new_booking_date_obj->status_id = $org_booking->status_id;
                        $new_booking_date_obj->confirmation_id = $org_booking->confirmation_id;
                        $new_booking_date_obj->total = $org_booking->total;
                        $new_booking_date_obj->price_json = $org_booking->price_json;
                        $new_booking_date_obj->no_of_nights = $org_booking->no_of_nights;
                        $new_booking_date_obj->estimated_arrival_time_id = $org_booking->estimated_arrival_time_id;
                        $new_booking_date_obj->no_of_children = $org_booking->no_of_children;
                        $new_booking_date_obj->no_of_adults = $org_booking->no_of_adults;
                        $new_booking_date_obj->person_no = $org_booking->person_no;
                        $new_booking_date_obj->guest_first_name = $org_booking->guest_first_name;
                        $new_booking_date_obj->guest_last_name = $org_booking->guest_last_name;
                        $new_booking_date_obj->guest_country_id = $org_booking->guest_country_id;
                        // $new_booking_date_obj->attributes = $org_booking->attributes;
                        // $new_booking_date_obj->date = $key;
                        $new_booking_date_obj->save();

                        foreach ($org_booking->bookingsUpsellItems as $value1) 
                        {
                            $model_upsell = new BookingsUpsellItems();
                            $model_upsell->booking_date_id = $new_booking_date_obj->id;
                            $model_upsell->upsell_id = $value1->upsell_id;
                            $model_upsell->price = $value1->price;
                            $model_upsell->extra_bed_quantity = $value1->extra_bed_quantity;
                            $model_upsell->save();
                        }
                        foreach ($org_booking->bookingsDiscountCodes as $key => $value2) 
                        {
                            $model_discount = new BookingsDiscountCodes();
                            $model_discount->booking_date_id = $new_booking_date_obj->id;
                            $model_discount->discount_id = $value2->discount_id;
                            $model_discount->price = $value2->price;
                            $model_discount->extra_bed_quantity = $value2->extra_bed_quantity;
                            $model_discount->save();
                        }
                        foreach ($org_booking->bookingSpecialRequests as $value3) 
                        {
                            $model_request = new BookingSpecialRequests();
                            $model_request->booking_date_id = $new_booking_date_obj->id;
                            $model_request->request_id = $value3->request_id;
                            $model_request->save();
                        }
                        // Notifications::addDestinationOverbooking($model,$new_booking_date_obj);
                        // Notifications::addMultipleUnitsBooking($model,$new_booking_date_obj);
                        // Notifications::addUnitNotAssingned($model,$new_booking_date_obj);

                    }
                    else
                    {
                        // Notifications::addDestinationOverbooking($model,$booking_date_obj);
                        // Notifications::addMultipleUnitsBooking($model,$booking_date_obj);
                        // Notifications::addUnitNotAssingned($model,$booking_date_obj);
                    }
                }
                $to_be_deleted_bookings = array();
                foreach ($bookedDates as $bookedDate) 
                {
                    if(!array_key_exists($bookedDate->date, $new_booking_rates))
                    {
                        array_push($to_be_deleted_bookings, $bookedDate->id);
                        // $prev_booking_date = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $_POST['arrival_date']]);
                    }
                }

                foreach ($to_be_deleted_bookings as $value) 
                {
                    $booking_date_obj = BookingDates::findOne(['id' => $value]);
                    Notifications::removeDestinationOverbooking($model,$booking_date_obj);
                    Notifications::removeUnitOverbooking($model,$booking_date_obj);
                    $booking_date_obj->delete();
                }

                $new_booked_dates = BookingDates::find()->where(['booking_item_id' => $model->id])->all();

                // $new_booked_dates = BookingDates::find()->where(['booking_item_id' => $bookingItemsModel->id])
                //                                 ->andWhere(['not in','id',$old_booking_dates])
                //                                 ->all();
                $new_dates = array();
                $new_adults = array();
                $new_children = array();
                $new_no_of_nights = $new_booked_dates[0]->no_of_nights;
                $new_beds_combination = $new_booked_dates[0]->bedsCombinations->combination;
                $new_first_name = $new_booked_dates[0]->guest_first_name;
                $new_last_name = $new_booked_dates[0]->guest_last_name;
                $new_country = Countries::findOne([$new_booked_dates[0]->guest_country_id])->name;
                $new_cancellation = $new_booked_dates[0]->booking_cancellation;
                $new_confirmation_id = $new_booked_dates[0]->confirmation_id;
                $new_estimated_arrival_time = $new_booked_dates[0]->estimated_arrival_time_id;
                $new_person_no = array();
                $new_person_price = array();

                foreach ($new_booked_dates as $bookingDatesModel) 
                {
                    array_push($new_dates, (string)$bookingDatesModel->date);
                    array_push($new_adults, (string)$bookingDatesModel->no_of_adults);
                    array_push($new_children, (string)$bookingDatesModel->no_of_children);
                    array_push($new_person_no, (string)$bookingDatesModel->person_no);
                    array_push($new_person_price, (string)$bookingDatesModel->person_price);

                    Notifications::addDestinationOverbooking($model,$bookingDatesModel);
                    Notifications::addMultipleUnitsBooking($model,$bookingDatesModel);
                    Notifications::addUnitNotAssingned($model,$bookingDatesModel);  
                }
                //Notifications::addRateNotAvailable($model);

                $this->isAttributeUpdated('Arrival Date',$model->arrival_date,$_POST['arrival_date'],$model->id);
                $model->arrival_date = $_POST['arrival_date'];
                $this->isAttributeUpdated('Departure Date',$model->departure_date,$_POST['departure_date'],$model->id);
                $model->departure_date = $_POST['departure_date'];

                $old_item_name = !empty($model->item_name_id)?$model->itemName->item_name:'';
                
                
                if($item_names_flag == 1 )
                {
                    $model->item_name_id = null;
                }
                $model->save();
                $new_item_name = empty(BookableItemsNames::findOne(['id' =>  $model->item_name_id ]))?'':BookableItemsNames::findOne(['id' =>  $model->item_name_id ])->item_name;
                $this->isAttributeUpdated('Unit',$old_item_name ,$new_item_name,$model->id);
                //Notifications::addRateNotAvailable($model);
                $this->isAttributeUpdated('No of Nights',$old_no_of_nights,$new_no_of_nights,$model->id);
                $this->isAttributeUpdated('Beds Combination',$old_beds_combination,$new_beds_combination,$model->id);
                $this->isAttributeUpdated('First Name',$old_first_name,$new_first_name,$model->id);
                $this->isAttributeUpdated('Last Name',$old_last_name,$new_last_name,$model->id);
                $this->isAttributeUpdated('Country',$old_country,$new_country,$model->id);
                $this->isAttributeUpdated('Booking Cancellation',$old_cancellation,$new_cancellation,$model->id);
                $this->isAttributeUpdated('Booking Confirmation',$old_confirmation_id,$new_confirmation_id,$model->id);
                $this->isAttributeUpdated('Estinamted Arrival Time',$old_estimated_arrival_time,$new_estimated_arrival_time,$model->id);

                $this->isArrayAttributeUpdated('Booking Dates',$old_dates,$new_dates,$model->id);
                $this->isArrayAttributeUpdated('No of Adults',$old_adults,$new_adults,$model->id);
                $this->isArrayAttributeUpdated('No of Children',$old_children,$new_children,$model->id);
                $this->isArrayAttributeUpdated('No of Persons',$old_person_no,$new_person_no,$model->id);
                $this->isArrayAttributeUpdated('Person Price',$old_person_price,$new_person_price,$model->id);
                return 'true';
            }
            else
            {
                return 'false';
            }
        }
            

    }

    public function actionEditOrDragBookedItemSchedular()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $model = BookingsItems::findOne(['id' => $_POST['id']]);
        $copy_model = $model;
        // ['AND', 'booking_item_id = :booking_item_id', ['NOT IN', 'notification_type', [Notifications::UNIT_OVERBOOKING,Notifications::DESTINATION_OVERBOOKING]]], [':booking_item_id' => $model->id]
        //Notifications::deleteAll(['booking_item_id' => $model->id, ]);
        Notifications::deleteAll(['AND', 'booking_item_id = :booking_item_id', ['NOT IN', 'notification_type', [Notifications::UNIT_OVERBOOKING,Notifications::DESTINATION_OVERBOOKING]]], [':booking_item_id' => $model->id]);

        $bookable_item = BookableItems::findOne(['id' => $_POST['bookable_item_id']]);
        $item_name_model = BookableItemsNames::findOne(['id' => $_POST['item_name_id'] ]);

        $item_name_id = NULL;
        if(!empty($item_name_model))
        {
            $item_name_id = $item_name_model->id;
        }

        $date1=date_create($_POST['departure_date']);
        $date2=date_create($_POST['arrival_date']);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");
        $date = $_POST['arrival_date'];

        $date_exception_flag = 0;
        $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $model->provider_id, 'state' => 0])->all();

        $dates_arr = array();
        $date1 = $_POST['arrival_date'];
        $date1 = str_replace('/', '-', $date1);

        // echo "<pre>";
        // print_r($difference);
        // exit();
       
        // $date = DateTime::createFromFormat('d/m/Y', strtotime($date));
        $date1 = date("Y-m-d", strtotime($date));
        // echo "<pre>";
        // print_r($date);
        // exit();
        for ($i=0; $i < $difference ; $i++)
        {
            array_push($dates_arr, $date1);

            $date1 = strtotime("+1 day", strtotime($date1));
            $date1 = date("Y-m-d", $date1);

        }

        if(!empty($closed_dates))
        {
            foreach ($closed_dates  as $date2) 
            {
                if(in_array($date2->date, $dates_arr))
                {
                    $date_exception_flag = 1;
                    break;
                }
            }
        }

       // $item_availability = BookingsItems::itemsAvailabilityForCalendar($difference,$_POST['arrival_date'],$model->item_id,$model->id);
        if($date_exception_flag == 1)
        {
            return 'date_exception';
        }
        else
        {
            $old_dates = array();
            $old_adults = array();
            $old_children = array();
            $old_no_of_nights = $model->bookingDatesSortedAndNotNull[0]->no_of_nights;
            $old_beds_combination = $model->bookingDatesSortedAndNotNull[0]->bedsCombinations->combination;
            $old_first_name = $model->bookingDatesSortedAndNotNull[0]->guest_first_name;
            $old_last_name = $model->bookingDatesSortedAndNotNull[0]->guest_last_name;
            $old_country = Countries::findOne([$model->bookingDatesSortedAndNotNull[0]->guest_country_id])->name;
            $old_cancellation = $model->bookingDatesSortedAndNotNull[0]->booking_cancellation;
            $old_confirmation_id = $model->bookingDatesSortedAndNotNull[0]->confirmation_id;
            $old_estimated_arrival_time = $model->bookingDatesSortedAndNotNull[0]->estimated_arrival_time_id;
            $old_person_no = array();
            $old_person_price = array();

            foreach ($model->bookingDatesSortedAndNotNull as  $value) 
            {
                array_push($old_dates, (string)$value->date);
                array_push($old_adults, (string)$value->no_of_adults);
                array_push($old_children, (string)$value->no_of_children);
                array_push($old_person_no, (string)$value->person_no);
                array_push($old_person_price, (string)$value->person_price);

            }
            //$ratesArr = BookingsItems::checkAvailableRates($model->item->id,$_POST['arrival_date'],$difference);
            $booking_date = BookingDates::findOne(['booking_item_id' => $model->id]);

            //$ratesArr = BookingsItems::checkAvailableRates($model->item->id,$_POST['arrival_date'],$difference);
            $ratesArr = BookingsItems::checkAvailableRates($booking_date->rate->unit_id,$_POST['arrival_date'],$difference);
            $availableRates = $ratesArr['availableRates'];
            $availableRatesIds = $ratesArr['availableRatesIds'];



            $bookedDates = $model->bookingDatesSortedAndNotNull;

            $bookedRatesRecord = array();

            $rate_flag = 0; 
            $item_names_flag = 0;
            foreach ($bookedDates as $value) 
            {
                if(!in_array($value->rate_id, $bookedRatesRecord))
                {
                    array_push($bookedRatesRecord, $value->rate_id);
                }
            }
            
            $new_booking_rates = array();
            
            foreach ($bookedRatesRecord as  $booked_rate) 
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    if(!empty($value))
                    {
                        $day_rates = explode(',', $value);
                        if(in_array($booked_rate, $day_rates))
                        {
                            if(empty($new_booking_rates[$key]))
                            {
                                $new_booking_rates[$key] = $day_rates[array_search($booked_rate,$day_rates)];
                            }
                            
                        }
                    }
                    else
                    {
                        $rate_flag = 1;
                    }
                    
                }
            }

            $rates_unavailable =array();
            foreach ($availableRatesIds as $key => $value) 
            {
                if(!array_key_exists($key, $new_booking_rates))
                {
                    array_push($rates_unavailable,$key);
                    $rate_flag = 1;
                }
            } 

            
            ////////////////////// Removing check of available item name as we are assigning item names by our selves ////////////////////////
            // $available_item_names = BookingsItems::checkAvailableItemNamesIncludedSaved($model->item->id,$_POST['arrival_date'],$difference,$model->id);

            // if(!in_array($item_name_id, $available_item_names))
            // {
            //     $item_names_flag = 1;
            // }


            if($rate_flag == 0 )
            {
               
                foreach ($new_booking_rates as $key => $value) 
                {
                    $booking_date_obj = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $key]);


                    if(empty($booking_date_obj))
                    {
                        
                        $org_booking = BookingDates::findOne(['booking_item_id' => $model->id,'rate_id' => $value]);
                        $new_booking_date_obj = new BookingDates();
                        $new_booking_date_obj->booking_item_id = $org_booking->booking_item_id;
                        $new_booking_date_obj->date = $key;
                        $new_booking_date_obj->item_id = $bookable_item->id;
                        
                        $new_booking_date_obj->item_name_id = $item_name_id;
                        $new_booking_date_obj->rate_id = $org_booking->rate_id;

                        $new_booking_date_obj->beds_combinations_id = $org_booking->beds_combinations_id;
                        $new_booking_date_obj->quantity = $org_booking->quantity;
                        $new_booking_date_obj->travel_partner_id = $org_booking->travel_partner_id;
                        $new_booking_date_obj->offers_id = $org_booking->offers_id; 
                        $new_booking_date_obj->booking_cancellation = $org_booking->booking_cancellation;
                        $new_booking_date_obj->flag_id = $org_booking->flag_id;
                        $new_booking_date_obj->status_id = $org_booking->status_id;
                        $new_booking_date_obj->confirmation_id = $org_booking->confirmation_id;
                        $new_booking_date_obj->total = $org_booking->total;
                        $new_booking_date_obj->price_json = $org_booking->price_json;
                        $new_booking_date_obj->no_of_nights = $org_booking->no_of_nights;
                        $new_booking_date_obj->estimated_arrival_time_id = $org_booking->estimated_arrival_time_id;
                        $new_booking_date_obj->no_of_children = $org_booking->no_of_children;
                        $new_booking_date_obj->no_of_adults = $org_booking->no_of_adults;
                        $new_booking_date_obj->person_no = $org_booking->person_no;
                        $new_booking_date_obj->guest_first_name = $org_booking->guest_first_name;
                        $new_booking_date_obj->guest_last_name = $org_booking->guest_last_name;
                        $new_booking_date_obj->guest_country_id = $org_booking->guest_country_id;
                        // $new_booking_date_obj->attributes = $org_booking->attributes;
                        // $new_booking_date_obj->date = $key;
                        $new_booking_date_obj->save();

                        foreach ($org_booking->bookingsUpsellItems as $value1) 
                        {
                            $model_upsell = new BookingsUpsellItems();
                            $model_upsell->booking_date_id = $new_booking_date_obj->id;
                            $model_upsell->upsell_id = $value1->upsell_id;
                            $model_upsell->price = $value1->price;
                            $model_upsell->extra_bed_quantity = $value1->extra_bed_quantity;
                            $model_upsell->save();
                        }
                        foreach ($org_booking->bookingsDiscountCodes as $key => $value2) 
                        {
                            $model_discount = new BookingsDiscountCodes();
                            $model_discount->booking_date_id = $new_booking_date_obj->id;
                            $model_discount->discount_id = $value2->discount_id;
                            $model_discount->price = $value2->price;
                            $model_discount->extra_bed_quantity = $value2->extra_bed_quantity;
                            $model_discount->save();
                        }
                        foreach ($org_booking->bookingSpecialRequests as $value3) 
                        {
                            $model_request = new BookingSpecialRequests();
                            $model_request->booking_date_id = $new_booking_date_obj->id;
                            $model_request->request_id = $value3->request_id;
                            $model_request->save();
                        }
                        // Notifications::addDestinationOverbooking($model,$new_booking_date_obj);
                        // Notifications::addMultipleUnitsBooking($model,$new_booking_date_obj);
                        // Notifications::addUnitNotAssingned($model,$new_booking_date_obj);

                    }
                    else
                    {                        Notifications::removeUnitOverbooking($model,$booking_date_obj);
                        $booking_date_obj->item_id = $bookable_item->id;
                        if($item_names_flag == 1 )
                        {
                            $booking_date_obj->item_name_id = null;
                            
                        }
                        else
                        {
                            $booking_date_obj->item_name_id = $item_name_id;
                        }

                        $booking_date_obj->save();
                        // Notifications::removeUnitOverbooking($model,$old_booking_date_obj);
                        // Notifications::addDestinationOverbooking($model,$booking_date_obj);
                        // Notifications::addMultipleUnitsBooking($model,$booking_date_obj);
                        // Notifications::addUnitNotAssingned($model,$booking_date_obj);
                    }
                }

                $to_be_deleted_bookings = array();
                foreach ($bookedDates as $bookedDate) 
                {
                    if(!array_key_exists($bookedDate->date, $new_booking_rates))
                    {
                        array_push($to_be_deleted_bookings, $bookedDate->id);
                        // $prev_booking_date = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $_POST['arrival_date']]);
                    }
                }
                
                foreach ($to_be_deleted_bookings as $value) 
                {
                    $booking_date_obj = BookingDates::findOne(['id' => $value]);
                    Notifications::removeDestinationOverbooking($model,$booking_date_obj);
                    Notifications::removeUnitOverbooking($model,$booking_date_obj);
                    $booking_date_obj->delete();
                }

                $new_booked_dates = BookingDates::find()->where(['booking_item_id' => $model->id])->all();



                $new_bookableItem = BookableItems::findOne(['id' => $bookable_item->id]);
                $this->isAttributeUpdated('Bookable Item',$model->item->itemType->name,BookableItemTypes::findOne(['id' => $new_bookableItem->item_type_id])->name,$model->id);
                $model->item_id = $bookable_item->id;
                $this->isAttributeUpdated('Arrival Date',$model->arrival_date,$_POST['arrival_date'],$model->id);
                $model->arrival_date = $_POST['arrival_date'];
                $this->isAttributeUpdated('Departure Date',$model->departure_date,$_POST['departure_date'],$model->id);
                $model->departure_date = $_POST['departure_date'];
                

                $old_item_name = !empty($model->item_name_id)?$model->itemName->item_name:'';
                $new_item_name = empty(BookableItemsNames::findOne(['id' =>  $item_name_id ]))?'':BookableItemsNames::findOne(['id' =>  $item_name_id ])->item_name;
                $this->isAttributeUpdated('Unit',$old_item_name ,$new_item_name,$model->id);

                $model->item_name_id = $item_name_id;
                $model->save();

                $new_dates = array();
                $new_adults = array();
                $new_children = array();
                $new_no_of_nights = $new_booked_dates[0]->no_of_nights;
                $new_beds_combination = $new_booked_dates[0]->bedsCombinations->combination;
                $new_first_name = $new_booked_dates[0]->guest_first_name;
                $new_last_name = $new_booked_dates[0]->guest_last_name;
                $new_country = Countries::findOne([$new_booked_dates[0]->guest_country_id])->name;
                $new_cancellation = $new_booked_dates[0]->booking_cancellation;
                $new_confirmation_id = $new_booked_dates[0]->confirmation_id;
                $new_estimated_arrival_time = $new_booked_dates[0]->estimated_arrival_time_id;
                $new_person_no = array();
                $new_person_price = array();

                foreach ($new_booked_dates as $bookingDatesModel) 
                {
                    array_push($new_dates, (string)$bookingDatesModel->date);
                    array_push($new_adults, (string)$bookingDatesModel->no_of_adults);
                    array_push($new_children, (string)$bookingDatesModel->no_of_children);
                    array_push($new_person_no, (string)$bookingDatesModel->person_no);
                    array_push($new_person_price, (string)$bookingDatesModel->person_price);

                    Notifications::addDestinationOverbooking($model,$bookingDatesModel);
                    Notifications::addMultipleUnitsBooking($model,$bookingDatesModel);
                    Notifications::addUnitNotAssingned($model,$bookingDatesModel);  
                }
                //Notifications::addRateNotAvailable($model);

                $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $model->provider_id, 'state' => 0])->all();

                $closed_dates_arr = array();
                if(!empty($closed_dates))
                {
                    foreach ($closed_dates  as $date) 
                    {
                        array_push($closed_dates_arr, $date->date);
                    }
                }

                $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $model->provider_id ])
                                ->all();

                $schedular_resources = $this->generateSchedularResources(Destinations::findOne(['id' => $model->provider_id ]));

                foreach ($bookingsItemsModel as $key => $model) 
                {
                    $bookings_items_arr[] = $this->generateSchedularEvent($model);
                }

                $notifications_array = array();

                $notifications = Notifications::find()->where(['notification_type' => Notifications::UNIT_OVERBOOKING ])->all();

                if(!empty($notifications))
                {
                    foreach ($notifications as $noti) 
                    {
                        array_push($notifications_array, $noti->date);
                    }
                }
                
                // checking if something has changed // 

                //Notifications::addRateNotAvailable($model);
                $this->isAttributeUpdated('No of Nights',$old_no_of_nights,$new_no_of_nights,$model->id);
                $this->isAttributeUpdated('Beds Combination',$old_beds_combination,$new_beds_combination,$model->id);
                $this->isAttributeUpdated('First Name',$old_first_name,$new_first_name,$model->id);
                $this->isAttributeUpdated('Last Name',$old_last_name,$new_last_name,$model->id);
                $this->isAttributeUpdated('Country',$old_country,$new_country,$model->id);
                $this->isAttributeUpdated('Booking Cancellation',$old_cancellation,$new_cancellation,$model->id);
                $this->isAttributeUpdated('Booking Confirmation',$old_confirmation_id,$new_confirmation_id,$model->id);
                $this->isAttributeUpdated('Estinamted Arrival Time',$old_estimated_arrival_time,$new_estimated_arrival_time,$model->id);

                $this->isArrayAttributeUpdated('Booking Dates',$old_dates,$new_dates,$model->id);
                $this->isArrayAttributeUpdated('No of Adults',$old_adults,$new_adults,$model->id);
                $this->isArrayAttributeUpdated('No of Children',$old_children,$new_children,$model->id);
                $this->isArrayAttributeUpdated('No of Persons',$old_person_no,$new_person_no,$model->id);
                $this->isArrayAttributeUpdated('Person Price',$old_person_price,$new_person_price,$model->id);
                ////////// Sending the date to move schedular scrollbar to /////////

                $first_booking = BookingDates::findOne(['booking_item_id' => $copy_model->id]);
                // $first_booking_date = explode("-", $first_booking->date);
                // $first_booking_date = $first_booking_date[2];

                echo json_encode([
                    'bookings_items' => $bookings_items_arr,
                    //'bookable_items' => $this->renderAjax('bookable_items',['provider_id' => $_POST['destination_id']]),
                    'resources'     => $schedular_resources,
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                    'notifications' => $notifications_array,
                    'first_date' => $first_booking->date,
                ]);
                //Notifications::addRateNotAvailable($model);
                //return 'true';
            }
            else
            {
                return 'false';
            }
        }
            

    }

    public function actionAddEventException()
    {
        $check_booking = 0;
        $bookingItems = BookingsItems::find()->where(['provider_id' =>  $_POST['destination_id'], 'deleted_at' => '0'])->all();
        // if(!empty($bookingItems))
        // {
        //     foreach ($bookingItems as  $item) 
        //     {
        //         foreach ($item->bookingDates as $date) 
        //         {
        //             if($date->date == $_POST['date'])
        //             {
        //                 $check_booking = 1;
        //                 break;
        //             }
        //         }
        //     }
        // }
        $exception = new DestinationsOpenHoursExceptions();
        if(!empty($exception) && $check_booking == 0)
        {
            $exception->date = $_POST['date'];
            $exception->provider_id = $_POST['destination_id'];
            $exception->state = 0;
            $exception->hours_24 = 0;
            $exception->save();

            // $org_quantity = $orgBookingItem->item->item_quantity;
            //                 if(count(BookingDates::find()->where(['date' => $bookingDatesModel->date, 'item_id' => $bookingDatesModel->item_id ])->all()) > $org_quantity)
            // {
            $destination_overbooking_noti_exists = Notifications::findOne(['destination_id' => $_POST['destination_id'], 'date' => $_POST['date'], 'notification_type' => Notifications::DESTINATION_OVERBOOKING]);
            if(empty($destination_overbooking_noti_exists))
            {
                $rate_noti = new Notifications();
                $rate_noti->destination_id = $_POST['destination_id'];
                $rate_noti->booking_item_id = NULL;
                $rate_noti->bookable_item_id = NULL;
                $rate_noti->rate_id = NULL;
                $rate_noti->date = $_POST['date'];
                $rate_noti->notification_type = Notifications::DESTINATION_OVERBOOKING;
                $rate_noti->message = "Overbooking on ".$_POST['date'];
                $rate_noti->user_type = User::USER_TYPE_ADMIN; 
                $rate_noti->save();
            }
                    
            // }
        }
        if(isset($_POST['item_id']) && $_POST['item_id']!='show_all')
        {
            $bookingsItemsModel = BookingsItems::find()
                            ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id'],'item_id' => $_POST['item_id']])
                            ->all();
        }
        else
        {
            $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
                                ->all();
        }
    
        $bookings_items_arr = [];

        if(!empty($bookingsItemsModel))
        {
            $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            $closed_dates_arr = array();
            if(!empty($closed_dates))
            {
                foreach ($closed_dates  as $date) 
                {
                    array_push($closed_dates_arr, $date->date);
                }
            }

            foreach ($bookingsItemsModel as $key => $model) 
            {
                $bookings_items_arr[] = $this->generateCalendarEvent($model);
            }

            if(isset($_POST['item_id']))
            {
                echo json_encode([
                    'bookings_items' => json_encode($bookings_items_arr),
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                    'check_booking' => $check_booking
                ]);

                $params['provider_id'] =  $_POST['destination_id'];
                $params['item_id'] = $_POST['item_id'];
                $session[Yii::$app->controller->id.'-booking-calendar-values'] = json_encode($params);
            }
            else
            {
                echo json_encode([
                    'bookings_items' => json_encode($bookings_items_arr),
                    'bookable_items' => $this->renderAjax('bookable_items',['provider_id' => $_POST['destination_id']]),
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                    'check_booking' => $check_booking
                ]);

                $params['provider_id'] =  $_POST['destination_id'];
                $params['item_id'] = 'show_all';
                $session[Yii::$app->controller->id.'-booking-calendar-values'] = json_encode($params);
            }
        }
        else
        {
            echo json_encode([
                'empty' => 1,
            ]); 
        }

    }

    public function actionRemoveEventException()
    {
        $exception = DestinationsOpenHoursExceptions::findOne(['provider_id' => $_POST['destination_id'],'date' => $_POST['date']]);
        if(!empty($exception))
        {
            $exception->delete();
        }
                if(isset($_POST['item_id']) && $_POST['item_id']!='show_all')
        {
            $bookingsItemsModel = BookingsItems::find()
                            ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id'],'item_id' => $_POST['item_id']])
                            ->all();
        }
        else
        {
            $bookingsItemsModel = BookingsItems::find()
                                ->where(['temp_flag' => 0, 'deleted_at' => 0, 'provider_id' => $_POST['destination_id']])
                                ->all();
        }
    
        $bookings_items_arr = [];

        if(!empty($bookingsItemsModel))
        {
            $closed_dates = DestinationsOpenHoursExceptions::find()->where(['provider_id' => $_POST['destination_id'], 'state' => 0])->all();

            $closed_dates_arr = array();
            if(!empty($closed_dates))
            {
                foreach ($closed_dates  as $date) 
                {
                    array_push($closed_dates_arr, $date->date);
                }
            }

            foreach ($bookingsItemsModel as $key => $model) 
            {
                $bookings_items_arr[] = $this->generateCalendarEvent($model);
            }

            if(isset($_POST['item_id']))
            {
                echo json_encode([
                    'bookings_items' => json_encode($bookings_items_arr),
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                ]);

                $params['provider_id'] =  $_POST['destination_id'];
                $params['item_id'] = $_POST['item_id'];
                $session[Yii::$app->controller->id.'-booking-calendar-values'] = json_encode($params);
            }
            else
            {
                echo json_encode([
                    'bookings_items' => json_encode($bookings_items_arr),
                    'bookable_items' => $this->renderAjax('bookable_items',['provider_id' => $_POST['destination_id']]),
                    'empty' => 0,
                    'date' => date('Y-m-d'),
                    'closed_dates' => $closed_dates_arr,
                ]);

                $params['provider_id'] =  $_POST['destination_id'];
                $params['item_id'] = 'show_all';
                $session[Yii::$app->controller->id.'-booking-calendar-values'] = json_encode($params);
            }
        }
        else
        {
            echo json_encode([
                'empty' => 1,
            ]); 
        }

    }

    public function actionLateCheckin($id)
    {
        $booking = $this->findModel($id);
        $booking_date = BookingDates::findOne(['booking_item_id' => $id ]);
        $first_name = '';
        $last_name = '';
        if(isset($booking_date->guest_first_name) && isset($booking_date->guest_last_name) && !empty($booking_date->guest_first_name) && !empty($booking_date->guest_last_name))
        {
            $first_name = $booking_date->guest_first_name;
            $last_name = $booking_date->guest_last_name;
        }
        else if(isset($booking_date->user_id))
        {
            $first_name = isset($booking_date->user->profile->first_name)?$booking_date->user->profile->first_name:'________';
            $last_name = isset($booking_date->user->profile->last_name)?$booking_date->user->profile->last_name:'________';
        }
        else
        {
            $first_name = '________';
            $last_name = '________';
        }
        if(isset($booking->travel_partner_id) && !empty($booking->travel_partner_id))
        {
            $travel_partner = $booking->travelPartner->travelPartner->company_name;
            $content =  trim($booking->travelPartner->notes.$booking->item->bookable_item_notes);
        }
        else
        {
            $content =  trim($booking->item->bookable_item_default_notes.$booking->item->bookable_item_notes);
            $travel_partner = '';
        }

        $content = str_replace('font-weight: 700', 'font-weight: bold', $content);
        $content = str_replace('font-weight:700', 'font-weight: bold', $content);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'content' => $this->renderPartial('test_print',['content' => $content,'first_name' => $first_name, 'last_name' => $last_name, 'travel_partner' => $travel_partner]),
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'options' => [
                'title' => 'Late Check-in Notice',
                'subject' => 'Late Check-in Notice'
            ],
            'methods' => [
                // 'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
                // 'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        // return $pdf->render();
        $pdf->render();
        exit();
    }

    public function actionPrintReceipt($id)
    {
        $booking = BookingsItems::findOne($id);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'content' => $this->renderPartial('print-receipt',[
                    'booking' => $booking
                ]),
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'options' => [
                'title' => "Receipt for $id",
                'subject' => "Receipt for $id"
            ],
        ]);
        $pdf->render();
        exit();
    }

    public function actionPrintReceiptGroup($id)
    {
        $booking_group = BookingGroup::findOne($id);
        $bookings_ids = array();

        foreach ($booking_group->bookingItemGroups as $bookingItemGroup) {
            $bookings_ids[] = $bookingItemGroup->booking_item_id;
        }

        $bookings = BookingsItems::findAll($bookings_ids);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 15,
            'marginRight' => 15,
            'marginTop' => 16,
            'marginBottom' => 16,
            'content' => $this->renderPartial('print-receipt-group',[
                    'booking_group' => $booking_group,
                    'bookings' => $bookings
                ]),
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'options' => [
                'title' => "Receipt for group $id",
                'subject' => "Receipt for group $id"
            ],
        ]);
        $pdf->render();
        exit();
    }

    public function actionCreateBookingPageRedirect()
    {
        $departure_date = strtotime("+1 day", strtotime($_POST['arrival_date']));
        $departure_date = date("Y-m-d", $departure_date);

        $session = Yii::$app->session;
        $session->open();

        $params['provider_id'] =  $_POST['provider_id'];
        $params['arrival_date'] = $_POST['arrival_date'];
        $params['departure_date'] = $departure_date;

        $session[Yii::$app->controller->id.'-create-booking-values'] = json_encode($params);

        return $this->redirect(['create']);
    }

    public function getItemtypenames($provider_id,$arrival_date, $departure_date,$remove_rate_conditions)
    {

        $items = BookableItems::find()->where(['provider_id' => $provider_id])->all();

        // $arrival_date = str_replace('/', '-', $arrival_dt);
        // $arrival_date = date('Y-m-d',strtotime($arrival_date));

        // $departure_date = str_replace('/', '-', $departure_dt);
        // $departure_date = date('Y-m-d',strtotime($departure_date));

        $record_flag = 0;
        $rate_conditions_flag = 0;
        $item_quantity = 0;

        $arrival_day = date('N',strtotime($arrival_date));
        $departure_day = date('N',strtotime($departure_date));

        $date1=date_create($departure_date);
        $date2=date_create($arrival_date);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a");

        $destination_items = array();
        foreach ($items as $key => $item) 
        {
            $price_json = '';
            $show_item_flag = 0;
            $mergeDatesArr = [];
            $combine_dates = '';
            $combine_dates_arr = '';

            $ratesArr = BookingsItems::checkAvailableRates($item->id,$arrival_date,$difference,$remove_rate_conditions);
            $availableRates = $ratesArr['availableRates'];
            $availableRatesIds = $ratesArr['availableRatesIds'];

            $rate_conditions_flag = BookingsItems::checkRateConditions($item->id,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions);
            $availableItemNames = BookingsItems::checkAvailableItemNames($item->id,$arrival_date,$difference);

            if(!empty($availableRatesIds))
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    if(isset($mergeDatesArr[$value]))
                    {
                        $mergeDatesArr[$value][] = $key;
                    }
                    else
                    {
                        $mergeDatesArr[$value][] = $key;
                    }
                }
            }

           /* echo '<pre>';
            print_r($availableItemNames);
            echo '</pre>';*/

            if(isset($remove_rate_conditions) && $remove_rate_conditions == 0 && !empty($availableRates)  && !empty($availableItemNames) && count($availableRates)==$difference && $rate_conditions_flag==1)
            {
                $show_item_flag = 1;
            }

            if(isset($remove_rate_conditions) && $remove_rate_conditions == 1 && !empty($availableRates))
            {
                $show_item_flag = 1;
            }

           // $remaining_quantity = 1;

            if($show_item_flag)
            {
                $destination_items[$item->id] = $item->itemType->name;
            }
 
        }

        
        return $destination_items;
    }

    public function actionGetAddOnDetails()
    {
        $id = $_POST['id'];
        // echo "<pre>";
        // print_r($id);
        // exit();
        if($_POST['type'] == 3)
        {
            $destination_add_on = DestinationAddOns::findOne(['id' => $id]);
        }
        if($_POST['type'] == 2)
        {
            $destination_add_on = RatesUpsell::findOne(['id' => $id]);
        }

        if(!empty($destination_add_on))
        {
            $temp = explode('.', $destination_add_on->vat);
            if(isset($temp[1]))
            {
                $temp[1].='00000';
                $temp[1] = substr($temp[1], 0, 2);
                $temp[0].=$temp[1];
                $destination_add_on->vat = $temp[0];
            }
            else
            {
                $destination_add_on->vat.='.00';
            }
            
            echo json_encode([
                    'price' => $destination_add_on->price,
                    'vat'   => $destination_add_on->vat,
                    'tax'   => $destination_add_on->tax,
                ]);
        }
        else
        {
            echo json_encode([
                    'price' => 0,
                    'vat'   => 0,
                    'tax'   => 0,
                ]);
        }
        
    }

    public function actionCalculatePrice()
    {
        $vat = 0;
        $price = 0;
        $tax = 0;
        $discount_amount = 0;
        if(isset($_POST['price']) && !empty($_POST['price']))
        {
            $price = str_replace('.', '', str_replace("_", "", $_POST['price']));
        }
        if(isset($_POST['vat']) && !empty($_POST['vat']))
        {
            $vat = str_replace(',', '.', str_replace("_", "", $_POST['vat']));
        }
        if(isset($_POST['tax']) && !empty($_POST['tax']))
        {
            $tax = str_replace('.', '', str_replace("_", "", $_POST['tax']));
        }
        if(isset($_POST['discount_amount']) && !empty($_POST['discount_amount']))
        {
            $discount_amount = str_replace('.', '', str_replace("_", "", $_POST['discount_amount']));
        }

        // echo "<pre>";
        // echo "price : ".$price;
        // echo "vat : ".$vat;
        // echo "tax : ".$tax;
        // echo "discount_amount : ".$discount_amount;
        // exit();

        $x = $price/(1+($vat/100));
        $y = $x - $tax;
        $sub_total1 = $y-$discount_amount;
        $sub_total2 = $sub_total1 + $tax;
        $vat_amount = round($sub_total2*($vat/100),2);
        $final_total = round($vat_amount + $sub_total2);
        if(isset($_POST['qty']) && !empty($_POST['qty']))
        {
            if($_POST['qty'] > 1)
            {
                $final_total = $final_total*$_POST['qty'];
            }
        }
        return json_encode([
                'vat_amount' => substr(str_replace('.', ',', $vat_amount),0,5),
                'final_total' => Yii::$app->formatter->asDecimal($final_total,0),
            ]);
    }

    public function actionAddFinanceEntry()
    {
        $finance = $_POST['debit'];

        $bookingItem = BookingsItems::findOne(['id' => $finance['booking_item_id'] ]);

        $temp = isset($finance['price'])?str_replace('.', '', str_replace("_", "", $finance['price'])):0;

        if(isset($finance['price']) && $temp !=0)
        {
            if($finance['id'] == 0)
            {
                $new_finance = new BookingDateFinances;
                $old_debit_log = '';
            }
            else
            {
                $new_finance = BookingDateFinances::findOne(['id' => $finance['id']]);
                $old_debit_log = $new_finance->price_description.' ('.$new_finance->date.'): '.$new_finance->quantity.'x'.$new_finance->amount;
            }
                
            $new_finance->booking_item_id = $bookingItem->id;
            $new_finance->booking_group_id = $bookingItem->bookingItemGroups->bookingGroup->id;
            $new_finance->type = $finance['type'];
            $new_finance->entry_type = BookingDateFinances::ENTRY_TYPE_USER;
            $new_finance->item_type = $finance['item'];
            $new_finance->show_receipt = ($finance['receipt']=="true")?1:0;
            $new_finance->date = isset($finance['date'])?date('Y-m-d',strtotime($finance['date'])):"";
            $new_finance->price_description = isset($finance['description'])?$finance['description']:"";
            $new_finance->quantity = isset($finance['quantity'])?$finance['quantity']:"1";

            $new_finance->amount = isset($finance['price'])?str_replace('.', '', str_replace("_", "", $finance['price'])):0;

            $new_finance->vat = isset($finance['vat'])?str_replace('_', '', str_replace(",", ".", $finance['vat'])) :0;

            $new_finance->tax = isset($finance['tax'])?str_replace('.', '', str_replace("_", "", $finance['tax'])):0;

            $new_finance->vat_amount = isset($finance['vat_amount'])?str_replace(",", ".", $finance['vat_amount']):0;

            $new_finance->discount_amount = isset($finance['discount_amount'])?str_replace(",",".",str_replace('.', '', str_replace("_", "", $finance['discount_amount']))):0;

            $new_finance->final_total = isset($finance['final_total'])?str_replace(",",".",str_replace('.', '', str_replace("_", "", $finance['final_total']))):0;

            if(!$new_finance->save())
            {
                echo "<pre>";
                print_r($new_finance->errors);
                exit();
            }
            $new_debit_log = $new_finance->price_description.' ('.$new_finance->date.'): '.$new_finance->quantity.'x'.$new_finance->amount;
            $this->isAttributeUpdated('charge invoice',$old_debit_log,$new_debit_log,$bookingItem->id);
            //////////////////// UPDATING BALANCE OF BOOKING ITEM /////////////
            if(!empty($bookingItem->bookingDateCharges))
            {
                $balance = 0;
                $old_balance = $bookingItem->balance;
                foreach ($bookingItem->bookingDateCharges as $charge) 
                {
                    $balance += $charge->final_total;
                }
                $bookingItem->balance = $balance;
                $bookingItem->save();
                $this->isAttributeUpdated('Balance',$old_balance,$bookingItem->balance,$bookingItem->id);
            }

            

            $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

            $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                      ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                      ->sum('final_total');
            $net_total = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');
            $discount = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('discount_amount');

            $total_debit = empty($total_debit)?0:$total_debit;
            $total_credit =  empty($total_credit)?0:$total_credit;   
            $discount =  empty($discount)?0:$discount;   

            $balance_due = $total_debit - $total_credit;
            $bookingItem->balance = $balance_due;
            if($balance_due == 0 || $balance_due < 0 )
            {
                $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                if(empty($booking_status_paid))
                {
                    $booking_status_paid = new BookingStatuses();
                    $booking_status_paid->label = 'Paid';
                    $booking_status_paid->background_color = '#3c1efa';
                    $booking_status_paid->text_color = '#ffffff';
                    $booking_status_paid->save();
                }
                if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                {
                    if($bookingItem->status->label == 'Paid')
                    {
                        if($balance_due != 0)
                        {
                            $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                            if(empty($booking_status_exception))
                            {
                                $booking_status_exception = new BookingStatuses();
                                $booking_status_exception->label = 'Exception';
                                $booking_status_exception->background_color = '#ff6161';
                                $booking_status_exception->text_color = '#ffffff';
                                $booking_status_exception->save();
                            }
                            $bookingItem->status_id = $booking_status_exception->id;
                            $bookingItem->save();
                        }
                        
                    }
                    else
                    {
                        if($bookingItem->status->label == 'Exception')
                        {
                            if($balance_due == 0)
                            {
                                $bookingItem->status_id = $booking_status_paid->id;
                                $bookingItem->save();
                            }
                        }
                        else
                        {
                            $bookingItem->status_id = $booking_status_paid->id;
                            $bookingItem->save();
                        }
                        
                    }
                }
                else
                {
                    $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                    if(empty($booking_status_paid))
                    {
                        $booking_status_paid = new BookingStatuses();
                        $booking_status_paid->label = 'Paid';
                        $booking_status_paid->background_color = '#3c1efa';
                        $booking_status_paid->text_color = '#ffffff';
                        $booking_status_paid->save();
                    }
                    $bookingItem->status_id = $booking_status_paid->id;
                    $bookingItem->save();
                }
            }
            if($balance_due > 0 )
            {
                if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                {
                    if($bookingItem->status->label == 'Paid')
                    {
                        $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                        if(empty($booking_status_exception))
                        {
                            $booking_status_exception = new BookingStatuses();
                            $booking_status_exception->label = 'Exception';
                            $booking_status_exception->background_color = '#ff6161';
                            $booking_status_exception->text_color = '#ffffff';
                            $booking_status_exception->save();
                        }
                        $bookingItem->status_id = $booking_status_exception->id;
                        $bookingItem->save();
                    }
                }
            }

            $bookingItem->gross_total = $total_debit;
            $bookingItem->averaged_total = round($total_debit/$bookingItem->no_of_booking_days);
            $bookingItem->net_total = $total_debit - round($discount);

            $bookingItem->save(); 

            foreach ($bookingItem->bookingDatesSortedAndNotNull as $bDate) 
            {
                $bDate->status_id = $bookingItem->status_id;
                $bDate->save();
            }

            $orgBookingGroupModel = $bookingItem->bookingItemGroups->bookingGroup;
            $group_balance = 0;
            foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
            {
                $group_balance += $group_item->bookingItem->balance;
            }
            $orgBookingGroupModel->balance = $group_balance;
            $orgBookingGroupModel->save();

           // sleep(5);
            echo json_encode([

                'id' => $new_finance->id,
                'total_debit' => Yii::$app->formatter->asDecimal($total_debit,0),
                'total_credit' =>Yii::$app->formatter->asDecimal($total_credit,0) ,
                'balance_due' => Yii::$app->formatter->asDecimal($balance_due,0)
                
                ]);
        }
        else
        {
            echo json_encode([
                'id' => 0
                ]);
        }    
    }

    public function actionDeleteFinanceEntry()
    {
        $id = $_POST['id'];

        $bookingItem = BookingsItems::findOne(['id' => $_POST['booking_item_id'] ]);
        $old_balance = $bookingItem->balance;
        if(isset($id) && !empty($id))
        {
            $charge =BookingDateFinances::findOne(['id' => $id]);
            $old_debit_log = $charge->price_description.' ('.$charge->date.'): '.$charge->quantity.'x'.$charge->amount;
            $new_debit_log = '';
            if(!empty($charge))
            {
                $charge->delete();
                $this->isAttributeUpdated('charge invoice',$old_debit_log,$new_debit_log,$bookingItem->id);
                $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

                $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                          ->sum('final_total');
                $net_total = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('amount');
                $discount = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('discount_amount');
                                                                    
                $total_debit = empty($total_debit)?0:$total_debit;
                $total_credit =  empty($total_credit)?0:$total_credit; 
                $discount =  empty($discount)?0:$discount;     

                $balance_due = $total_debit - $total_credit; 
                $bookingItem->balance = $balance_due;

                $bookingItem->gross_total = $total_debit;
                $bookingItem->averaged_total = round($total_debit/$bookingItem->no_of_booking_days);
                // $bookingItem->net_total = $net_total;
                $bookingItem->net_total = $total_debit - round($discount);

                if($balance_due == 0 || $balance_due < 0 )
                {
                    $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                    if(empty($booking_status_paid))
                    {
                        $booking_status_paid = new BookingStatuses();
                        $booking_status_paid->label = 'Paid';
                        $booking_status_paid->background_color = '#3c1efa';
                        $booking_status_paid->text_color = '#ffffff';
                        $booking_status_paid->save();
                    }
                    if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                    {
                        if($bookingItem->status->label == 'Paid')
                        {
                            if($balance_due != 0)
                            {
                                $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                                if(empty($booking_status_exception))
                                {
                                    $booking_status_exception = new BookingStatuses();
                                    $booking_status_exception->label = 'Exception';
                                    $booking_status_exception->background_color = '#ff6161';
                                    $booking_status_exception->text_color = '#ffffff';
                                    $booking_status_exception->save();
                                }
                                $bookingItem->status_id = $booking_status_exception->id;
                                $bookingItem->save();
                            }
                            
                        }
                        else
                        {
                            if($bookingItem->status->label == 'Exception')
                            {
                                if($balance_due == 0)
                                {
                                    $bookingItem->status_id = $booking_status_paid->id;
                                    $bookingItem->save();
                                }
                            }
                            else
                            {
                                $bookingItem->status_id = $booking_status_paid->id;
                                $bookingItem->save();
                            }
                            
                        }
                    }
                    else
                    {
                        $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                        if(empty($booking_status_paid))
                        {
                            $booking_status_paid = new BookingStatuses();
                            $booking_status_paid->label = 'Paid';
                            $booking_status_paid->background_color = '#3c1efa';
                            $booking_status_paid->text_color = '#ffffff';
                            $booking_status_paid->save();
                        }
                        $bookingItem->status_id = $booking_status_paid->id;
                        $bookingItem->save();
                    }
                }
                if($balance_due > 0 )
                {
                    if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                    {
                        if($bookingItem->status->label == 'Paid')
                        {
                            $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                            if(empty($booking_status_exception))
                            {
                                $booking_status_exception = new BookingStatuses();
                                $booking_status_exception->label = 'Exception';
                                $booking_status_exception->background_color = '#ff6161';
                                $booking_status_exception->text_color = '#ffffff';
                                $booking_status_exception->save();
                            }
                            $bookingItem->status_id = $booking_status_exception->id;
                            $bookingItem->save();
                        }
                    }
                }
                $bookingItem->save(); 
                $this->isAttributeUpdated('Balance',$old_balance,$bookingItem->balance,$bookingItem->id);
                $orgBookingGroupModel = $bookingItem->bookingItemGroups->bookingGroup;
                $group_balance = 0;
                foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
                {
                    $group_balance += $group_item->bookingItem->balance;
                }
                $orgBookingGroupModel->balance = $group_balance;
                $orgBookingGroupModel->save();

                foreach ($bookingItem->bookingDatesSortedAndNotNull as $bDate) 
                {
                    $bDate->status_id = $bookingItem->status_id;
                    $bDate->save();
                }
                echo json_encode([

                    'total_debit' => Yii::$app->formatter->asDecimal($total_debit,0),
                    'total_credit' =>Yii::$app->formatter->asDecimal($total_credit,0) ,
                    'balance_due' => Yii::$app->formatter->asDecimal($balance_due,0)
                    
                    ]);
            }
            else
            {
                echo json_encode([
                    'id' => 0
                ]);
            }
        }
    }

    public function actionAddPaymentEntry()
    {
        $finance = $_POST['credit'];

        $bookingItem = BookingsItems::findOne(['id' => $finance['booking_item_id'] ]);
        $old_balance = $bookingItem->balance;
        $temp = isset($finance['total'])?$finance['total']:0;

        if(isset($finance['total']) && $temp !=0)
        {
            $deposit_percentage;
           if($finance['id'] == 0)
            {
                $new_credit = new BookingDateFinances;
                $old_credit_log = '';
            }
            else
            {
                $new_credit = BookingDateFinances::findOne(['id' => $finance['id']]);
                $old_credit_log = $new_credit->price_description.' ('.$new_credit->date.'): '.$new_credit->final_total;
            }

            $new_credit->booking_item_id = $bookingItem->id;
            $new_credit->booking_group_id = $bookingItem->bookingItemGroups->bookingGroup->id;
            $new_credit->type = BookingDateFinances::TYPE_CREDIT;
            $new_credit->show_receipt = ($finance['receipt']=="true")?1:0;
            $new_credit->date = isset($finance['date'])?date('Y-m-d',strtotime($finance['date'])):"";
            $new_credit->price_description = isset($finance['description'])?$finance['description']:"";

            if($finance['payment_type'] == 'true')
            {
                //$deposit_percentage_val = str_replace(',', '.', $finance['deposit_percentage'])/100;
                $new_credit->amount = str_replace(',', '.', (str_replace('.', '', $finance['total'])));
                $new_credit->final_total = str_replace(',', '.', (str_replace('.', '', $finance['total'])));
                $new_credit->payment_type = BookingDateFinances::PAYMETN_TYPE_DEPOSIT;
                $deposit_percentage = ((str_replace(',', '.', (str_replace('.', '', $finance['total']))))/(BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                  ->sum('final_total')))*100;
                $new_credit->deposit_percent_age = round($deposit_percentage,2);
                //$new_credit->deposit_percent_age = str_replace(',', '.', $finance['deposit_percentage']);;
            }
            else
            {
                $new_credit->amount = isset($finance['total'])?str_replace('.', '',$finance['total']):0;
                $new_credit->final_total = isset($finance['total'])?str_replace('.', '',$finance['total']):0;
            }
            

            if(!$new_credit->save())
            {
                echo "<pre>";
                print_r($new_credit->errors);
                exit();
            }
            else
            {
                $new_credit_log = $new_credit->price_description.' ('.$new_credit->date.'): '.$new_credit->final_total;
                $this->isAttributeUpdated('payment invoice',$old_credit_log,$new_credit_log,$bookingItem->id);
                $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

                $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                          ->sum('final_total');
                $total_debit = empty($total_debit)?0:$total_debit;
                $total_credit =  empty($total_credit)?0:$total_credit;      

                $balance_due = $total_debit - $total_credit;
                $bookingItem->balance = $balance_due;
                if($balance_due == 0 || $balance_due < 0 )
                {
                    $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                    if(empty($booking_status_paid))
                    {
                        $booking_status_paid = new BookingStatuses();
                        $booking_status_paid->label = 'Paid';
                        $booking_status_paid->background_color = '#3c1efa';
                        $booking_status_paid->text_color = '#ffffff';
                        $booking_status_paid->save();
                    }
                    if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                    {
                        if($bookingItem->status->label == 'Paid')
                        {
                            if($balance_due != 0)
                            {
                                $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                                if(empty($booking_status_exception))
                                {
                                    $booking_status_exception = new BookingStatuses();
                                    $booking_status_exception->label = 'Exception';
                                    $booking_status_exception->background_color = '#ff6161';
                                    $booking_status_exception->text_color = '#ffffff';
                                    $booking_status_exception->save();
                                }
                                $bookingItem->status_id = $booking_status_exception->id;
                                $bookingItem->save();
                            }
                            
                        }
                        else
                        {
                            if($bookingItem->status->label == 'Exception')
                            {
                                if($balance_due == 0)
                                {
                                    $bookingItem->status_id = $booking_status_paid->id;
                                    $bookingItem->save();
                                }
                            }
                            else
                            {
                                $bookingItem->status_id = $booking_status_paid->id;
                                $bookingItem->save();
                            }
                            
                        }
                    }
                    else
                    {
                        $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                        if(empty($booking_status_paid))
                        {
                            $booking_status_paid = new BookingStatuses();
                            $booking_status_paid->label = 'Paid';
                            $booking_status_paid->background_color = '#3c1efa';
                            $booking_status_paid->text_color = '#ffffff';
                            $booking_status_paid->save();
                        }
                        $bookingItem->status_id = $booking_status_paid->id;
                        $bookingItem->save();
                    }
                }
                if($balance_due > 0 )
                {
                    if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
                    {
                        if($bookingItem->status->label == 'Paid')
                        {
                            $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                            if(empty($booking_status_exception))
                            {
                                $booking_status_exception = new BookingStatuses();
                                $booking_status_exception->label = 'Exception';
                                $booking_status_exception->background_color = '#ff6161';
                                $booking_status_exception->text_color = '#ffffff';
                                $booking_status_exception->save();
                            }
                            $bookingItem->status_id = $booking_status_exception->id;
                            $bookingItem->save();
                        }
                    }
                }
                $bookingItem->paid_amount = $total_credit;
                $bookingItem->save();
                $this->isAttributeUpdated('Balance',$old_balance,$bookingItem->balance,$bookingItem->id);
                
                $orgBookingGroupModel = $bookingItem->bookingItemGroups->bookingGroup;
                $group_balance = 0;
                foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
                {
                    $group_balance += $group_item->bookingItem->balance;
                }
                $orgBookingGroupModel->balance = $group_balance;
                $orgBookingGroupModel->save();

                foreach ($bookingItem->bookingDatesSortedAndNotNull as $bDate) 
                {
                    $bDate->status_id = $bookingItem->status_id;
                    $bDate->save();
                }

                echo json_encode([

                'id' => $new_credit->id,
                'total_debit' => Yii::$app->formatter->asDecimal($total_debit,0),
                'total_credit' =>Yii::$app->formatter->asDecimal($total_credit,0) ,
                'balance_due' => Yii::$app->formatter->asDecimal($balance_due,0),
                'percentage' =>   (isset($finance['deposit_percentage']) && isset($deposit_percentage))?yii::$app->formatter->asDecimal($deposit_percentage,2):'',
                
                ]);
            }
        }
        else
        {
            echo json_encode([
                'id' => 0
                ]);
        }
    }

    public function actionDeletePaymentEntry()
    {
        $id = $_POST['id'];

        $bookingItem = BookingsItems::findOne(['id' => $_POST['booking_item_id'] ]);
        $old_balance = $bookingItem->balance;
        if(isset($id) && !empty($id))
        {
            $charge =BookingDateFinances::findOne(['id' => $id]);
            $old_credit_log = $charge->price_description.' ('.$charge->date.'): '.$charge->final_total;
            if(!empty($charge))
            {
                //$charge->delete();
                if($charge->added_from_group == BookingDateFinances::ADDED_FROM_GROUP)
                {
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $booking_group_finance = $charge->bookingGroupFinanceDetail->bookingGroupFinance;
                        if(!$charge->delete())
                        {
                            throw Exception('Unable to delete record.');
                        }
                        else
                        {
                            $this->isAttributeUpdated('payment invoice',$old_credit_log,'',$bookingItem->id);
                        }

                        if(empty($booking_group_finance->bookingGroupFinanceChildren))
                        {
                            if(!$booking_group_finance->delete())
                            {
                                throw Exception('Unable to delete record.');
                            }
                        }
                        else
                        {
                            $total = 0;
                            foreach ($booking_group_finance->bookingGroupFinanceChildren as $value) 
                            {
                                $total += $value->bookingDateFinance->final_total;
                            }
                            $booking_group_finance->amount = $total;
                            if(!$booking_group_finance->save())
                            {
                                throw Exception('Unable to delete record.');
                            }
                        }
                            

                        $transaction->commit();

                    } catch(Exception $e) {
                        $transaction->rollback();
                        echo $e->getMessage();
                        exit();
                    }
                }
                else
                {
                    $charge->delete();
                    $this->isAttributeUpdated('payment invoice',$old_credit_log,'',$bookingItem->id);
                }
                    




                $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

                $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                          ->sum('final_total');
                $total_debit = empty($total_debit)?0:$total_debit;
                $total_credit =  empty($total_credit)?0:$total_credit;      

                $balance_due = $total_debit - $total_credit; 
                $bookingItem->balance = $balance_due;


                $this->UpdateBookingBalance($bookingItem);
                $this->isAttributeUpdated('Balance',$old_balance,$bookingItem->balance,$bookingItem->id);


                echo json_encode([

                    'total_debit' => Yii::$app->formatter->asDecimal($total_debit,0),
                    'total_credit' =>Yii::$app->formatter->asDecimal($total_credit,0) ,
                    'balance_due' => Yii::$app->formatter->asDecimal($balance_due,0)
                    
                    ]);
            }
            else
            {
                echo json_encode([
                    'id' => 0
                ]);
            }
        }
    }

    public function actionAddGroupPaymentEntry()
    {
        $finance = $_POST['credit'];
        // echo "<pre>";
        // print_r($finance['payment_type']);
        // exit();
        $bookingGroup = BookingGroup::findOne(['id' => $finance['booking_group_id'] ]);

        $temp = isset($finance['total'])?$finance['total']:0;

        $deposit_percentage;

        if(isset($finance['total']) && $temp !=0)
        {
           if($finance['id'] == 0)
            {
                $new_credit = new BookingGroupFinance;
            }
            else
            {
                $new_credit = BookingGroupFinance::findOne(['id' => $finance['id']]);
            }

            $new_credit->booking_group_id = $bookingGroup->id;
            $new_credit->type = BookingDateFinances::TYPE_CREDIT;
            $new_credit->show_receipt = ($finance['receipt']=="true")?1:0;
            $new_credit->date = isset($finance['date'])?date('Y-m-d',strtotime($finance['date'])):"";
            $new_credit->price_description = isset($finance['description'])?$finance['description']:"";
            $new_credit->amount = isset($finance['total'])?str_replace('.', '',$finance['total']):0;
            if($finance['payment_type'] == 'true')
            {
                $new_credit->payment_type = BookingDateFinances::PAYMETN_TYPE_DEPOSIT;
                $deposit_percentage = $this->CalculateDepositPercentage($bookingGroup,isset($finance['total'])?str_replace('.', '',$finance['total']):0);
                $deposit_percentage = round($deposit_percentage,2);
                $new_credit->deposit_percent_age = $deposit_percentage;
                //$new_credit->deposit_percent_age = str_replace(',', '.', $finance['deposit_percentage']);
            }
            if(!$new_credit->save())
            {
                echo "<pre>";
                print_r($new_credit->errors);
                exit();
            }
            else
            {
                if($finance['payment_type'] == 'true')
                {
                    $new_entries = array();
                    //$deposit_percentage_val = str_replace(',', '.', $finance['deposit_percentage'])/100;
                    $deposit_percentage_val = $deposit_percentage/100;
                    $total_group_items = count($bookingGroup->bookingItemGroups);
                    $counter = 1;
                    $calculated_amount = 0;
                    foreach ($bookingGroup->bookingItemGroups as $bookingItemGroup) 
                    {
                        $bookingItem = $bookingItemGroup->bookingItem;

                        if($bookingItemGroup->bookingItem->balance > 0 )
                        {
                            $new_item_credit = new BookingDateFinances;

                            $new_item_credit->type = BookingDateFinances::TYPE_CREDIT;
                            $new_item_credit->booking_item_id = $bookingItem->id;
                            $new_item_credit->booking_group_id = $bookingGroup->id;
                            $new_item_credit->show_receipt = ($finance['receipt']=="true")?1:0;
                            $new_item_credit->date = isset($finance['date'])?date('Y-m-d',strtotime($finance['date'])):"";
                            $new_item_credit->price_description = isset($finance['description'])?$finance['description']:"";
                            $new_item_credit->added_from_group = BookingDateFinances::ADDED_FROM_GROUP;

                            if($counter == $total_group_items)
                            {
                                $new_item_credit->amount = $new_credit->amount - $calculated_amount;
                                $new_item_credit->final_total = $new_credit->amount - $calculated_amount;
                            }
                            else
                            {
                                //calculating amout from remaining balance
                                // $new_item_credit->amount = round($deposit_percentage_val*$bookingItem->balance);
                                // $new_item_credit->final_total = round($deposit_percentage_val*$bookingItem->balance);

                                //calculating amout from original balance
                                $new_item_credit->amount = round($deposit_percentage_val*BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                  ->sum('final_total'));
                                $new_item_credit->final_total = round($deposit_percentage_val*BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                  ->sum('final_total'));
                            }
                            $new_item_credit->payment_type = BookingDateFinances::PAYMETN_TYPE_DEPOSIT;
                            $new_item_credit->deposit_percent_age = $deposit_percentage;
                            //$new_item_credit->deposit_percent_age = str_replace(',', '.', $finance['deposit_percentage']);
                            if(!$new_item_credit->save())
                            {
                                if(!empty($new_entries))
                                {
                                    foreach ($new_entries as $value) 
                                    {
                                        $value->delete();
                                    }
                                    $new_credit->delete();
                                    $this->UpdateBookingBalance($bookingItem);
                                }
                                echo "<pre>";
                                print_r($new_item_credit->errors);
                                exit();
                            }

                            $booking_group_finacne_child =new BookingGroupFinanceDetails;
                            $booking_group_finacne_child->booking_group_finance_id = $new_credit->id;
                            $booking_group_finacne_child->booking_date_finance_id = $new_item_credit->id;

                            if(!$booking_group_finacne_child->save())
                            {
                                echo "<pre>";
                                print_r($booking_group_finacne_child->errors);
                                exit();
                            }

                            // $remaining_balance  = $remaining_balance - $bookingItem->balance;
                            $calculated_amount += round($deposit_percentage_val*BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                                                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                                                                          ->sum('final_total'));
                            $this->UpdateBookingBalance($bookingItem);
                            array_push($new_entries, $new_item_credit);
                        }
                        
                        $counter ++;
                                   
                    }

                }
                else
                {       
                    $new_entries = array();
                    $remaining_balance = str_replace('.', '',$finance['total']);
                    $booking_item_count = count($bookingGroup->bookingItemGroups);
                    $i=1;
                    foreach ($bookingGroup->bookingItemGroups as $bookingItemGroup) 
                    {
                        $bookingItem = $bookingItemGroup->bookingItem;
                        if($remaining_balance > 0)
                        {
                            $new_item_credit = new BookingDateFinances;
                            if($remaining_balance > $bookingItem->balance)
                            {
                                if($bookingItem->balance != 0 || $booking_item_count == $i)
                                {
                                    $new_item_credit->type = BookingDateFinances::TYPE_CREDIT;
                                    $new_item_credit->booking_item_id = $bookingItem->id;
                                    $new_item_credit->booking_group_id = $bookingGroup->id;
                                    $new_item_credit->show_receipt = ($finance['receipt']=="true")?1:0;
                                    $new_item_credit->date = isset($finance['date'])?date('Y-m-d',strtotime($finance['date'])):"";
                                    $new_item_credit->price_description = isset($finance['description'])?$finance['description']:"";
                                    $new_item_credit->added_from_group = BookingDateFinances::ADDED_FROM_GROUP;

                                    if($booking_item_count == $i)
                                    {
                                        $new_item_credit->amount = $remaining_balance;
                                        $new_item_credit->final_total = $remaining_balance;
                                    }
                                    else
                                    {
                                        $new_item_credit->amount = $bookingItem->balance;
                                        $new_item_credit->final_total = $bookingItem->balance;
                                    }
                                        
                                    if(!$new_item_credit->save())
                                    {
                                        if(!empty($new_entries))
                                        {
                                            foreach ($new_entries as $value) 
                                            {
                                                $value->delete();
                                            }
                                            $new_credit->delete();
                                            $this->UpdateBookingBalance($bookingItem);
                                        }
                                        echo "<pre>";
                                        print_r($new_item_credit->errors);
                                        exit();
                                    }

                                    $booking_group_finacne_child =new BookingGroupFinanceDetails;
                                    $booking_group_finacne_child->booking_group_finance_id = $new_credit->id;
                                    $booking_group_finacne_child->booking_date_finance_id = $new_item_credit->id;

                                    if(!$booking_group_finacne_child->save())
                                    {
                                        echo "<pre>";
                                        print_r($booking_group_finacne_child->errors);
                                        exit();
                                    }

                                    $remaining_balance  = $remaining_balance - $bookingItem->balance;
                                    $this->UpdateBookingBalance($bookingItem);
                                    array_push($new_entries, $new_item_credit);
                                }
                            }
                            else
                            {
                                $new_item_credit->type = BookingDateFinances::TYPE_CREDIT;
                                $new_item_credit->booking_item_id = $bookingItem->id;
                                $new_item_credit->booking_group_id = $bookingGroup->id;
                                $new_item_credit->show_receipt = ($finance['receipt']=="true")?1:0;
                                $new_item_credit->date = isset($finance['date'])?date('Y-m-d',strtotime($finance['date'])):"";
                                $new_item_credit->price_description = isset($finance['description'])?$finance['description']:"";
                                $new_item_credit->amount = $remaining_balance;
                                $new_item_credit->final_total = $remaining_balance;
                                $new_item_credit->added_from_group = BookingDateFinances::ADDED_FROM_GROUP;
                                if(!$new_item_credit->save())
                                {
                                    if(!empty($new_entries))
                                    {
                                        foreach ($new_entries as $value) 
                                        {
                                            $value->delete();
                                        }
                                        $new_credit->delete();
                                        $this->UpdateBookingBalance($bookingItem);
                                    }
                                    echo "<pre>";
                                    print_r($new_item_credit->errors);
                                    exit();
                                }
                                $booking_group_finacne_child =new BookingGroupFinanceDetails;
                                $booking_group_finacne_child->booking_group_finance_id = $new_credit->id;
                                $booking_group_finacne_child->booking_date_finance_id = $new_item_credit->id;

                                if(!$booking_group_finacne_child->save())
                                {
                                    echo "<pre>";
                                    print_r($booking_group_finacne_child->errors);
                                    exit();
                                }

                                $remaining_balance  = $remaining_balance - $bookingItem->balance;
                                $this->UpdateBookingBalance($bookingItem);
                                array_push($new_entries, $new_item_credit);
                                break;
                            }

                        }
                        else
                        {
                            break;
                        }
                        $i++;
                    }
                }
                    

                $total_debit = 0;
                foreach ($bookingGroup->bookingItemGroups as $bookingGroupItem) 
                {
                    $total_debit += $bookingGroupItem->bookingItem->balance;
                }

                $total_credit = BookingGroupFinance::find()->where(['booking_group_id' => $bookingGroup->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                          ->sum('amount');

                $total_debit = empty($total_debit)?0:$total_debit;
                $total_credit =  empty($total_credit)?0:$total_credit;      

                $balance_due = $total_debit - $total_credit;
               
                echo json_encode([

                'id' => $new_credit->id,
                'total_debit' => Yii::$app->formatter->asDecimal($total_debit,0),
                'total_credit' =>Yii::$app->formatter->asDecimal($total_credit,0) ,
                'balance_due' => Yii::$app->formatter->asDecimal($total_debit,0),
                'percentage' =>   isset($finance['deposit_percentage'])?$deposit_percentage:'',
                'charges'     => $this->renderAjax('group_charges_table',['bookingGroupModel' => $bookingGroup])
                ]);
            }
        }
        else
        {
            echo json_encode([
                'id' => 0
                ]);
        }
    }

    public function actionDeleteGroupPaymentEntry()
    {
        $id = $_POST['id'];

        $bookingGroup = BookingGroup::findOne(['id' => $_POST['booking_group_id'] ]);
        if(isset($id) && !empty($id))
        {

            $charge =BookingGroupFinance::findOne(['id' => $id]);
            if(!empty($charge))
            {
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                        foreach ($charge->bookingGroupFinanceChildren as  $value) 
                        {
                            if(!$value->bookingDateFinance->delete())
                            {
                                throw Exception('Unable to delete record.');
                            }
                        }
                        foreach ($bookingGroup->bookingItemGroups as $bookingItemGroup ) 
                        {
                            $bookingItem = $bookingItemGroup->bookingItem;
                            $this->UpdateBookingBalance($bookingItem);
                        }   
                        
                        if(!$charge->delete())
                        {
                            throw Exception('Unable to delete record.');
                        }

                        $transaction->commit();

                } catch(Exception $e) {
                    $transaction->rollback();
                    echo $e->getMessage();
                    exit();
                }
                
                
                $total_debit = 0;
                foreach ($bookingGroup->bookingItemGroups as $bookingGroupItem) 
                {
                    $total_debit += $bookingGroupItem->bookingItem->balance;
                }

                $total_credit = BookingGroupFinance::find()->where(['booking_group_id' => $bookingGroup->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                          ->sum('amount');

                $total_debit = empty($total_debit)?0:$total_debit;
                $total_credit =  empty($total_credit)?0:$total_credit;      

                $balance_due = $total_debit - $total_credit;

                echo json_encode([

                    'total_debit' => Yii::$app->formatter->asDecimal($total_debit,0),
                    'total_credit' =>Yii::$app->formatter->asDecimal($total_credit,0) ,
                    'balance_due' => Yii::$app->formatter->asDecimal($total_debit,0),
                    'charges'     => $this->renderAjax('group_charges_table',['bookingGroupModel' => $bookingGroup])
                    ]);
            }
            else
            {
                echo json_encode([
                    'id' => 0
                ]);
            }
        }
    }

    public function actionCalculateDeposit()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $bookingGroup = BookingGroup::findOne(['id' => $_POST['booking_group_id'] ]);

        

        echo json_encode([
                    'deposit_amount' => yii::$app->formatter->asDecimal($this->CalculateDepositAmount($bookingGroup),0)
                ]);
    }

    public function actionCalculatePercentage()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $bookingGroup = BookingGroup::findOne(['id' => $_POST['booking_group_id'] ]);

        

        echo json_encode([
                    'deposit_percentage' => yii::$app->formatter->asDecimal($this->CalculateDepositPercentage($bookingGroup),2)
                ]);
    }

    public function actionCalculateItemDeposit()
    {

        $bookingItem = $this->findModel($_POST['booking_item_id']);

        $deposit_amount = 0;
        $percentage = $_POST['percentage']/100;
       
        $deposit_amount = (BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                  ->sum('final_total')) * $percentage;


        echo json_encode([
                    'deposit_amount' => yii::$app->formatter->asDecimal($deposit_amount,0)
                ]);
    }

    public function actionCalculateItemDepositPercentage()
    {

        $bookingItem = $this->findModel($_POST['booking_item_id']);
       
        $deposit_percentage = ((str_replace(',', '.', (str_replace('.', '', $_POST['amount']))))/(BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                  ->sum('final_total')))*100;

        echo json_encode([
                    'deposit_percentage' => yii::$app->formatter->asDecimal($deposit_percentage,2)
                ]);
    }

    public function CalculateDepositAmount($bookingGroup)
    {
        $deposit_amount = 0;
        $percentage = $_POST['percentage']/100;
        foreach ($bookingGroup->bookingItemGroups as $bookingGroupItem ) 
        {
            // calculation deposit amount according to remaining balance
            //$deposit_amount = $deposit_amount + ($bookingGroupItem->bookingItem->balance * $percentage);

            // calculation deposit amount according to original balance
            $deposit_amount = $deposit_amount + (BookingDateFinances::find()->where(['booking_item_id' => $bookingGroupItem->bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                                  ->sum('final_total') * $percentage);

        }

        return $deposit_amount;
    }

    public function CalculateDepositPercentage($bookingGroup,$amount = null)
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $org_amount;
        if($amount == null)
        {
            $org_amount = $_POST['amount'];
        }
        else
        {
            $org_amount = $amount;
        }
        $total_due = 0;
        foreach ($bookingGroup->bookingItemGroups as $bookingGroupItem ) 
        {
            // cacuting percentage according to remaining balance 
            // $total_due = $total_due + ($bookingGroupItem->bookingItem->balance);

            // calculation percentage according to original balance 
            $total_due = $total_due + BookingDateFinances::find()->where(['booking_item_id' => $bookingGroupItem->bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

        }

        $toatal_items = count($bookingGroup->bookingItemGroups);
        $deposit_percentage = ((str_replace(',', '.', (str_replace('.', '', $org_amount))))/$total_due)*100;

        return $deposit_percentage;
    }

    public function UpdateBookingBalance($bookingItem)
    {
        $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

        $total_credit = BookingDateFinances::find()->where(['booking_item_id' => $bookingItem->id])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                  ->sum('final_total');
        $total_debit = empty($total_debit)?0:$total_debit;
        $total_credit =  empty($total_credit)?0:$total_credit;      

        $balance_due = $total_debit - $total_credit;
        $bookingItem->balance = $balance_due;
        if($balance_due == 0 || $balance_due < 0 )
        {
            $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
            if(empty($booking_status_paid))
            {
                $booking_status_paid = new BookingStatuses();
                $booking_status_paid->label = 'Paid';
                $booking_status_paid->background_color = '#3c1efa';
                $booking_status_paid->text_color = '#ffffff';
                $booking_status_paid->save();
            }
            if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
            {
                if($bookingItem->status->label == 'Paid')
                {
                    if($balance_due != 0)
                    {
                        $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                        if(empty($booking_status_exception))
                        {
                            $booking_status_exception = new BookingStatuses();
                            $booking_status_exception->label = 'Exception';
                            $booking_status_exception->background_color = '#ff6161';
                            $booking_status_exception->text_color = '#ffffff';
                            $booking_status_exception->save();
                        }
                        $bookingItem->status_id = $booking_status_exception->id;
                        $bookingItem->save();
                    }
                    
                }
                else
                {
                    if($bookingItem->status->label == 'Exception')
                    {
                        if($balance_due == 0)
                        {
                            $bookingItem->status_id = $booking_status_paid->id;
                            $bookingItem->save();
                        }
                    }
                    else
                    {
                        $bookingItem->status_id = $booking_status_paid->id;
                        $bookingItem->save();
                    }
                    
                }
            }
            else
            {
                $booking_status_paid = BookingStatuses::findOne(['label' => 'Paid']);
                if(empty($booking_status_paid))
                {
                    $booking_status_paid = new BookingStatuses();
                    $booking_status_paid->label = 'Paid';
                    $booking_status_paid->background_color = '#3c1efa';
                    $booking_status_paid->text_color = '#ffffff';
                    $booking_status_paid->save();
                }
                $bookingItem->status_id = $booking_status_paid->id;
                $bookingItem->save();
            }
        }
        if($balance_due > 0 )
        {
            if(isset($bookingItem->status_id) && !empty($bookingItem->status_id))
            {
                if($bookingItem->status->label == 'Paid')
                {
                    $booking_status_exception = BookingStatuses::findOne(['label' => 'Exception']);
                    if(empty($booking_status_exception))
                    {
                        $booking_status_exception = new BookingStatuses();
                        $booking_status_exception->label = 'Exception';
                        $booking_status_exception->background_color = '#ff6161';
                        $booking_status_exception->text_color = '#ffffff';
                        $booking_status_exception->save();
                    }
                    $bookingItem->status_id = $booking_status_exception->id;
                    $bookingItem->save();
                }
            }
        }
        $bookingItem->paid_amount = $total_credit;
        $bookingItem->save();

        $orgBookingGroupModel = $bookingItem->bookingItemGroups->bookingGroup;
        $group_balance = 0;
        foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
        {
            $group_balance += $group_item->bookingItem->balance;
        }
        $orgBookingGroupModel->balance = $group_balance;
        $orgBookingGroupModel->save();

        foreach ($bookingItem->bookingDatesSortedAndNotNull as $bDate) 
        {
            $bDate->status_id = $bookingItem->status_id;
            $bDate->save();
        }
    }

    public function UnlinkItemFromGroup($id)
    {
        $bookingItemsModel = BookingsItems::findOne(['id' => $id]);
        $previous_group_id = $bookingItemsModel->bookingItemGroups->booking_group_id;
        $orgBookingGroupModel = $bookingItemsModel->bookingItemGroups->bookingGroup;

        $total_items = BookingItemGroup::find()->where(['booking_group_id' => $previous_group_id])->count();

        if($total_items > 1)
        {
            /*$bookingItemsModel->confirmation_id = $bookingItemsModel->item->booking_confirmation_type_id;
            $bookingItemsModel->booking_cancellation = $bookingItemsModel->item->general_booking_cancellation;
            $bookingItemsModel->flag_id = !empty($bookingItemsModel->item->booking_type_id)?$bookingItemsModel->item->booking_type_id:$bookingItemsModel->flag_id;
            $bookingItemsModel->status_id = !empty($bookingItemsModel->item->booking_status_id)?$bookingItemsModel->item->booking_status_id:$bookingItemsModel->status_id;*/

            $bookingGroupModel = new BookingGroup();
            $bookingGroupModel->balance = $bookingItemsModel->balance;
            $bookingGroupModel->save();

            $bookingGroupModel->group_name = 'Group-'.$bookingGroupModel->id;
            $bookingGroupModel->update();

            $bookingItemGroupModel = $bookingItemsModel->bookingItemGroups;
            $bookingItemGroupModel->booking_group_id = $bookingGroupModel->id;
            $bookingItemGroupModel->update();


            //$bookingItemsModel->save();
            $group_balance = 0;
            foreach ($orgBookingGroupModel->bookingItemGroups as $group_item) 
            {
                $group_balance += $group_item->bookingItem->balance;
            }
            $orgBookingGroupModel->balance = $group_balance;
            $orgBookingGroupModel->save();
        }
    }

    public function actionImportCsv()
    {
        $model = new importCsvModel();
        $dataArray = '';
        $resultArr = '';

        if($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            /*echo '<pre>';
            print_r(Yii::$app->request->post());
            exit;*/
            $path = Yii::getAlias('@app').'/../uploads/temp/';
            if(is_dir($path))
            {
                $this->delete_directory($path);
            }

            $uploadFile = new UploadFile();

            if($uploadFile->FileToUpload = UploadedFile::getInstance($model,'csv_file')) 
            {
                if($uploadFile->uploadFileObject('temp',Yii::$app->user->identity->id,'csv'))
                {
                    $filename = Yii::getAlias('@app').'/../uploads/temp/'.Yii::$app->user->identity->id.'/csv/'.$uploadFile->FileName;
                    $dataArray = $this->readCsvFileAndMakeArray($filename);
                    $resultArr = $this->createBookingFromCsvData($dataArray,$model->provider_id,$_POST['importCsvModel']['update_existing_booking']);

                    // echo "<pre>";
                    // print_r($resultArr);
                    // exit();
                }
            }
            else
            {
                Yii::$app->session->setFlash('error', 'No CSV file found');
            }
        }

        return $this->render('import_csv',['model'=>$model,'dataArray'=>$dataArray,'resultArr' => $resultArr]);
    }

    private function readCsvFileAndMakeArray($filePath) 
    {
        $prev_booking_ref = '';
        $dataArray = [];
        // open csv file
        $file = fopen($filePath,"r");
        // csv data export
        while(! feof($file)) 
        {    
            $row = fgetcsv($file);

            if($row[0] != 'Booking Ref')
            {
                $arr = [];

                // booking ref no
                if(!empty($row[0]))
                {
                    $arr['type'] = 'booking';
                    $arr['booking_ref_no'] = $row[0];
                    $prev_booking_ref = $row[0];
                }

                // set booking ref for charges if no booking

                if(!empty($row[25]) && empty($row[0]))
                {
                    $arr['type'] = 'charges';
                    $arr['booking_ref_no'] = $prev_booking_ref;
                }

                // set booking ref for payment if no booking

                if(!empty($row[31]) && empty($row[0]))
                {
                    $arr['type'] = 'payment';
                    $arr['booking_ref_no'] = $prev_booking_ref;
                }

                // booking date
                if(!empty($row[1]))
                {
                    $arr['created_at'] = strtotime($row[1]);
                }

                // modified date
                if(!empty($row[2]))
                {
                    $arr['updated_at'] = strtotime($row[2]);
                }

                // first night
                if(!empty($row[3]))
                {
                    $arr['arrival_date'] = date('Y-m-d',strtotime($row[3]));
                }

                // checkout or departure date
                if(!empty($row[5]))
                {
                    $arr['departure_date'] = date('Y-m-d',strtotime($row[5]));
                }

                // adults
                if($row[7]!='' && $row[7]>=0)
                {
                    $arr['no_of_adults'] = $row[7];
                }

                // children
                if($row[8]!='' && $row[8]>=0)
                {
                    $arr['no_of_children'] = $row[8];
                }

                // status
                if(!empty($row[11]))
                {
                    $arr['status'] = trim($row[11]);
                }

                // flag
                if(!empty($row[12]))
                {
                    $arr['flag'] = trim($row[12]);
                }

                // first name
                if(!empty($row[14]))
                {
                    $arr['guest_first_name'] = $row[14];
                }

                // last name
                if(!empty($row[15]))
                {
                    $arr['guest_last_name'] = $row[15];
                }

                // guest country
                if(!empty($row[16]))
                {
                    $arr['guest_country'] = trim($row[16]);
                }

                // guest comments
                if(!empty($row[17]))
                {
                    $arr['guest_comments'] = $row[17];
                }

                // guest comments
                if(!empty($row[18]))
                {
                    $arr['guest_notes'] = $row[18];
                }

                // room
                if(!empty($row[19]))
                {
                    $arr['room'] = trim($row[19]);
                }

                // unit
                if(!empty($row[20]))
                {
                    $arr['unit'] = trim($row[20]);
                }

                // charge item date
                if(!empty($row[25]))
                {
                    $arr['charge_item_date'] = date('Y-m-d',strtotime($row[25]));
                }

                // charge item description
                if(!empty($row[26]))
                {
                    $arr['charge_item_description'] = $row[26];
                }

                // charge item price
                // if(!empty($row[27]) || $row[27] == 0)
                if($row[27]!='')    
                {
                    $arr['charge_item_price'] = $row[27];
                }

                // charge item vat %
                // if(!empty($row[28]) || $row[28] == 0)
                if($row[28]!='')    
                {
                    $arr['charge_item_vat_percent'] = $row[28];
                }

                // charge item vat
                // if(!empty($row[29]) || $row[29] == 0)
                if($row[29]!='')    
                {
                    // replacing ',' with '.'
                    $arr['charge_item_vat'] = str_replace(',', '.', $row[29]);
                }

                // charge item net
                if(!empty($row[30]))
                {
                    $arr['charge_item_net'] = $row[30];
                }

                // payment item date
                if(!empty($row[31]))
                {
                    $arr['payment_item_date'] = date('Y-m-d',strtotime($row[31]));
                }

                // payment item description
                if(!empty($row[32]))
                {
                    $arr['payment_item_description'] = $row[32];
                }

                // payment item amount
                if(!empty($row[33]))
                {
                    $arr['payment_item_amount'] = $row[33];
                }

                // charge item quantity
                if(!empty($row[40]))
                {
                    $arr['charge_item_qty'] = $row[40];
                }

                // offer
                if(!empty($row[43]))
                {
                    $arr['offer'] = trim($row[43]);
                }

                // referrer
                if(!empty($row[46]))
                {
                    $arr['referrer'] = trim($row[46]);
                }

                // tax

                if(!empty($row[47]))
                {
                    $arr['tax'] = $row[47];
                }

                // old booking group no

                if(!empty($row[48]))
                {
                    $arr['old_group_no'] = $row[48];
                }

                if(!empty($arr))
                    $dataArray[] = $arr;
            }
        }
        fclose($file);
        return $dataArray;
    }

    private function createBookingFromCsvData($dataArray,$provider_id,$update_existing_booking)
    {
        $prev_booking_id = '';
        $prev_group_id = '';
        $remaining_balance = 0.0;
        $resultArr = [];
        $custom_rate = 0;
        $new_bookings = array();

        foreach ($dataArray as $key => $data)
        {
            if(!isset($data['type']))
            {
                echo "<pre>";
                print_r($data);
                exit();
            }
            switch($data['type'])
            {
                case 'booking':

                    $existingBooking = BookingsItems::findOne(['reference_no' => $data['booking_ref_no']]);
                    $flag = false;

                    if(empty($existingBooking))
                    {
                        $model = new BookingsItems();
                        $flag = true;
                    }
                    else if(!empty($existingBooking) && $update_existing_booking==1)
                    {
                        $flag = true;
                        $model = $existingBooking;
                    }
                    else
                    {
                        $prev_booking_id = '';
                        $prev_group_id = '';
                        $remaining_balance = 0.0;
                        $resultArr['error'][$data['booking_ref_no']] = 'Booking Already Exists';
                        $custom_rate = 0;
                        $flag = false;
                    }

                    if($flag)
                    {
                        // if(!in_array($model->id, $new_bookings))
                        // {
                        //     array_push($new_bookings, $model->id);
                        // }
                        // ************* Create or Update Booking *************** //

                        $model->provider_id = $provider_id;

                        $bookableItemType = BookableItemTypes::findOne(['name' => $data['room']]);
                        if(!empty($bookableItemType))
                        {
                            $bookableItem = BookableItems::findOne(['provider_id' => $provider_id, 'item_type_id' => $bookableItemType->id]);
                            if(!empty($bookableItem))
                            {
                                $model->item_id = $bookableItem->id;
                                $model->arrival_date = $data['arrival_date'];
                                $model->departure_date = $data['departure_date'];
                                $model->rate_conditions = 0;
                                $model->pricing_type = 1;
                                $model->created_by = Yii::$app->user->identity->id;

                                $bookableItemName = BookableItemsNames::findOne(['bookable_item_id' => $bookableItem->id, 'item_name' => $data['unit']]);
                                $model->item_name_id = !empty($bookableItemName)?$bookableItemName->id:NULL;;

                                if(isset($data['status']))
                                {
                                    $bookingStatus = BookingStatuses::findOne(['label' => $data['status']]);
                                    $model->status_id = !empty($bookingStatus)?$bookingStatus->id:NULL;
                                }
                                else
                                {
                                    $model->status_id = NULL;
                                }
                                    
                                if(isset($data['flag']))
                                {
                                    $bookingType = BookingTypes::findOne(['label' => $data['flag']]);
                                    $model->flag_id = !empty($bookingType)?$bookingType->id:NULL;
                                }
                                else
                                {
                                    $model->flag_id = NULL;
                                }
                                    
                                if(isset($data['referrer']))
                                {
                                    $travelPartner = TravelPartner::findOne(['company_name' => $data['referrer']]);
                                    if(!empty($travelPartner))
                                    {
                                        $destTravelPartner = DestinationTravelPartners::findOne(['destination_id' => $provider_id, 'travel_partner_id' => $travelPartner->id]);
                                        $model->travel_partner_id = !empty($destTravelPartner)?$destTravelPartner->id:NULL;
                                    }
                                    else
                                    {
                                        $model->travel_partner_id = NULL;
                                    } 
                                }
                                else
                                {
                                    $model->travel_partner_id = NULL;
                                }

                                $model->reference_no = $data['booking_ref_no'];
                                $model->comments = isset($data['guest_comments'])?$data['guest_comments']:NULL;
                                $model->notes = isset($data['guest_notes'])?$data['guest_notes']:NULL;


                                if(isset($data['offers']))
                                {
                                    $offers = Offers::findOne(['name' => $data['offer']]);
                                    $model->offers_id = !empty($offers)?$offers->id:NULL;
                                }
                                else
                                {
                                    $model->offers_id = NULL;
                                }
                                    
                                $model->temp_flag = 0;

                                if($model->save())
                                {
                                    if(!in_array($model->id, $new_bookings))
                                    {
                                        array_push($new_bookings, $model->id);
                                    }
                                    if(!empty($existingBooking)) 
                                        $resultArr['update'][$data['booking_ref_no']] = 'Updated successfully';
                                    else
                                        $resultArr['success'][$data['booking_ref_no']] = 'Created successfully';

                                    if(!empty($prev_booking_id))
                                    {
                                        $booking = BookingsItems::findOne(['id' => $prev_booking_id]);
                                        $booking->balance = ($remaining_balance>0.0)?$remaining_balance:0.0;
                                        $booking->update();
                                        $remaining_balance = 0.0;
                                        $custom_rate = 0;
                                    }
                                    $prev_booking_id = $model->id;
                                    $model->created_at = $data['created_at'];
                                    $model->updated_at = $data['updated_at'];
                                    $model->update();

                                    // ************* Create or Update Booking Group *************** //

                                    // $query = new \yii\db\Query;
                                    // $query->from('booking_group');
                                    // $query->select('max(id) as max');
                                    // $command = $query->createCommand();
                                    // $row = $command->queryOne();
                                    // $group_no = $row['max'] +1; 
                                    // $group_name = "Group-".$group_no;

                                    // ************* Delete Previous Booking Group *************** //

                                    if(!empty($existingBooking))
                                    {
                                        $bookingItemGroupModel = $existingBooking->bookingItemGroups;
                                        $bookingGroupModel = $bookingItemGroupModel->bookingGroup;
                                        if(!empty($bookingGroupModel->group_guest_user_id))
                                        {
                                            $bookingGuestModel = BookingGuests::findOne(['id' => $bookingGroupModel->group_guest_user_id]);
                                            $bookingGuestModel->delete();
                                        }
                                        $bookingItemGroupModel->delete();
                                        if(!empty($bookingGroupModel))
                                        {
                                            $bookingGroupModel->delete();
                                        }
                                        
                                    }


                                    $bookingGroupModel = BookingGroup::findOne(['old_group_no' => $data['old_group_no']]);
                                    if(empty($bookingGroupModel))
                                    {
                                        $query = new \yii\db\Query;
                                        $query->from('booking_group');
                                        $query->select('max(id) as max');
                                        $command = $query->createCommand();
                                        $row = $command->queryOne();
                                        $group_no = $row['max'] +1; 
                                        $group_name = "Group-".$group_no;
                                    
                                        $bookingGroupModel = new BookingGroup();
                                        $bookingGroupModel->group_name = $group_name;
                                    }
                                    $bookingGroupModel->old_group_no = $data['old_group_no'];
                                    

                                    // ************* Create Booking Guest *************** //

                                    if(isset($data['guest_first_name']))
                                    {
                                        $bookingGuestModel = new BookingGuests();
                                        $bookingGuestModel->first_name = isset($data['guest_first_name'])?$data['guest_first_name']:NULL;
                                        $bookingGuestModel->last_name = isset($data['guest_last_name'])?$data['guest_last_name']:NULL;

                                        if(isset($data['guest_country']))
                                        {
                                            $country = Countries::findOne(['name' => $data['guest_country']]);
                                            $bookingGuestModel->country_id = !empty($country)?$country->id:NULL;
                                        }
                                        
                                        $bookingGuestModel->save();
                                        $bookingGroupModel->group_guest_user_id = $bookingGuestModel->id;
                                    }

                                    $bookingGroupModel->flag_id = $model->flag_id;
                                    $bookingGroupModel->status_id = $model->status_id;
                                    $bookingGroupModel->travel_partner_id = $model->travel_partner_id;
                                    $bookingGroupModel->offers_id = $model->offers_id;
                                    $bookingGroupModel->comments = $model->comments;
                                    $bookingGroupModel->save();

                                    if(isset($bookingGroupModel->id))
                                    {
                                        $bookingItemGroupModel = new BookingItemGroup();
                                        $bookingItemGroupModel->booking_item_id = $model->id;
                                        $bookingItemGroupModel->booking_group_id = $bookingGroupModel->id;
                                        $bookingItemGroupModel->save();
                                        $prev_group_id = $bookingGroupModel->id;
                                    }

                                    // ************* Delete Previous Booking Finances *************** //

                                    if(!empty($existingBooking))
                                    {
                                        BookingDateFinances::deleteAll(['booking_item_id' => $existingBooking->id ]);
                                    }

                                    // ************* Create Booking Charges *************** //

                                    if(isset($data['charge_item_date']))
                                    {
                                        $date1=date_create($data['departure_date']);
                                        $date2=date_create($data['arrival_date']);
                                        $diff=date_diff($date1,$date2);
                                        $difference =  $diff->format("%a");

                                        $model->no_of_booking_days = $difference;
                                        $model->save(); 

                                        $tax = 0;
                                        if(isset($data['tax']))
                                        {
                                            $tax = round($data['tax']/$difference);
                                        }
                                       
                                        // if(!isset($data['charge_item_price']) || empty($data['charge_item_price']))
                                        // {
                                        //     echo "<pre>";
                                        //     print_r($data['booking_ref_no']);
                                        //     print_r($model->id);
                                        //     exit();
                                        // }
                                        $charge_amount = round($data['charge_item_price']/$difference);
                                        for($i=0; $i < $difference ; $i++)
                                        {
                                            $bookingDateFinances = new BookingDateFinances();
                                            $bookingDateFinances->date = $data['charge_item_date'];
                                            $bookingDateFinances->booking_item_id = $model->id;
                                            $bookingDateFinances->booking_group_id = $bookingGroupModel->id;  
                                            $bookingDateFinances->type = BookingDateFinances::TYPE_DEBIT;
                                            $bookingDateFinances->item_type = BookingDateFinances::ITEM_TYPE_CUSTOM;
                                            $bookingDateFinances->entry_type = BookingDateFinances::ENTRY_TYPE_SYSTEM;
                                            $bookingDateFinances->price_description = $data['charge_item_description'];
                                            $bookingDateFinances->amount = $charge_amount;
                                            $bookingDateFinances->vat = $data['charge_item_vat_percent'];
                                            $bookingDateFinances->vat_amount = $data['charge_item_vat'];
                                            $bookingDateFinances->tax = $tax;
                                            $bookingDateFinances->final_total = $charge_amount;
                                            $bookingDateFinances->quantity = 1;
                                            $bookingDateFinances->show_receipt = BookingDateFinances::SHOW_RECEIPT;
                                            $bookingDateFinances->save();

                                            $remaining_balance = floatval($remaining_balance) + floatval($bookingDateFinances->amount);
                                            $custom_rate = $charge_amount;
                                        }
                                    }

                                    // ************* Create Booking Payments *************** //

                                    if(isset($data['payment_item_date']))
                                    {
                                        $bookingDateFinances = new BookingDateFinances();
                                        $bookingDateFinances->date = $data['payment_item_date'];
                                        $bookingDateFinances->booking_item_id = $model->id;
                                        $bookingDateFinances->booking_group_id = $bookingGroupModel->id;  
                                        $bookingDateFinances->type = BookingDateFinances::TYPE_CREDIT;
                                        $bookingDateFinances->price_description = $data['payment_item_description'];
                                        $bookingDateFinances->amount = $data['payment_item_amount'];
                                        $bookingDateFinances->final_total = $data['payment_item_amount'];
                                        $bookingDateFinances->quantity = 1;
                                        $bookingDateFinances->show_receipt = BookingDateFinances::SHOW_RECEIPT;
                                        $bookingDateFinances->payment_type = BookingDateFinances::PAYMETN_TYPE_NORMAL;
                                        $bookingDateFinances->save();

                                        $remaining_balance = floatval($remaining_balance) - floatval($bookingDateFinances->amount);
                                        $custom_rate = empty($custom_rate)?$data['payment_item_amount']:$custom_rate;
                                    }

                                    // ************* Delete Previous Booking Date *************** //

                                    if(!empty($existingBooking))
                                    {
                                        BookingDates::deleteAll(['booking_item_id' => $existingBooking->id ]);
                                    }

                                    // ************* Create Booking Dates *************** //

                                    $date1=date_create($data['departure_date']);
                                    $date2=date_create($data['arrival_date']);
                                    $diff=date_diff($date1,$date2);
                                    $difference =  $diff->format("%a");

                                    $date = $data['arrival_date'];
                                    for($i=0; $i < $difference ; $i++)
                                    {
                                        $bookingDatesModel = new BookingDates();
                                        $bookingDatesModel->booking_item_id = $model->id;
                                        $bookingDatesModel->date = $date;
                                        $bookingDatesModel->item_id = $model->item_id;
                                        $bookingDatesModel->item_name_id = $model->item_name_id;
                                        // $globalRate = Rates::findOne(['name' => 'Global Rate For Import CSV']);
                                        $globalRate = Rates::findOne(['name' => 'Imported From CSV']);
                                        $bookingDatesModel->rate_id = !empty($globalRate)?$globalRate->id:NULL;
                                        $bookingDatesModel->custom_rate = $custom_rate;
                                        $bookingDatesModel->quantity = 1;
                                        $bookingDatesModel->no_of_nights = 1;
                                        $bookingDatesModel->user_id = NULL;
                                        $bookingDatesModel->guest_first_name = isset($data['guest_first_name'])?$data['guest_first_name']:NULL;
                                        $bookingDatesModel->guest_last_name = isset($data['guest_last_name'])?$data['guest_last_name']:NULL;

                                        if(isset($data['guest_country']))
                                        {
                                            $country = Countries::findOne(['name' => $data['guest_country']]);
                                            $bookingDatesModel->guest_country_id = !empty($country)?$country->id:NULL;
                                        } 
                                        
                                        $bookingDatesModel->flag_id = $model->flag_id;
                                        $bookingDatesModel->status_id = $model->status_id;
                                        $bookingDatesModel->no_of_adults = isset($data['no_of_adults'])?$data['no_of_adults']:NULL;
                                        $bookingDatesModel->no_of_children = isset($data['no_of_children'])?$data['no_of_children']:NULL;

                                        $total_person = intval($bookingDatesModel->no_of_adults) + intval($bookingDatesModel->no_of_children);

                                        $bookingDatesModel->person_no = ($total_person>4)?4:$total_person;
                                        $bookingDatesModel->total = $custom_rate;
                                        $bookingDatesModel->price_json = json_encode([
                                            'quantity' => 1,
                                            'total_nights' => 1,
                                            'rate' => Yii::$app->formatter->asDecimal($custom_rate, 2),
                                            'vat' => 11,
                                            'x' => 0,
                                            'lodgingTax' => 0,
                                            'y' => 0,
                                            'voucher_discount' => 0,
                                            'travel_partner_discount' => $data['referrer'],
                                            'offer_discount' => 0,
                                            'amount_of_discount' => 0,
                                            'sub_total_1' => 0,
                                            'sub_total_2' => 0,
                                            'vat_amount' => 0,
                                            'orgPrice' => $custom_rate,
                                            'icelandPrice' => Yii::$app->formatter->asDecimal($custom_rate, 0)
                                        ]);

                                        $bookingDatesModel->travel_partner_id = $model->travel_partner_id;
                                        $bookingDatesModel->offers_id = $model->offers_id;
                                        $bookingDatesModel->save();

                                        $date = strtotime("+1 day", strtotime($date));
                                        $date = date("Y-m-d", $date);
                                    }
                                }
                                else
                                {
                                    $prev_booking_id = '';
                                    $prev_group_id = '';
                                    $remaining_balance = 0.0;
                                    //$resultArr['error'][$data['booking_ref_no']] = $model->getErrors();
                                    $resultArr['error'][$data['booking_ref_no']] = 'Whoops something went wrong to create or update booking.';
                                    $custom_rate = 0;
                                }
                            }
                            else
                            {
                                $prev_booking_id = '';
                                $prev_group_id = '';
                                $remaining_balance = 0.0;
                                $resultArr['error'][$data['booking_ref_no']] = 'No Bookalbe Item found.';
                                $custom_rate = 0;
                            }
                        }
                        else
                        {
                            $prev_booking_id = '';
                            $prev_group_id = '';
                            $remaining_balance = 0.0;
                            $resultArr['error'][$data['booking_ref_no']] = 'No room found.';
                            $custom_rate = 0;
                        }
                    }

                    break;

                case 'charges':

                    if(!empty($prev_booking_id))
                    {
                        if(!isset($data['charge_item_vat']) || empty($data['charge_item_vat']))
                        {
                            echo "<pre>";
                            print_r($data['booking_ref_no']);
                            print_r($prev_booking_id);
                            exit();
                        }
                        $temp_quantity = isset($data['charge_item_qty'])?$data['charge_item_qty']:1;
                        $bookingDateFinances = new BookingDateFinances();
                        $bookingDateFinances->date = $data['charge_item_date'];
                        $bookingDateFinances->booking_item_id = $prev_booking_id;
                        $bookingDateFinances->booking_group_id = $prev_group_id;  
                        $bookingDateFinances->type = BookingDateFinances::TYPE_DEBIT;
                        $bookingDateFinances->item_type = BookingDateFinances::ITEM_TYPE_CUSTOM;
                        $bookingDateFinances->entry_type = BookingDateFinances::ENTRY_TYPE_USER;
                        $bookingDateFinances->price_description = $data['charge_item_description'];
                        $bookingDateFinances->amount = $data['charge_item_price'];
                        $bookingDateFinances->vat = $data['charge_item_vat_percent'];
                        $bookingDateFinances->vat_amount = $data['charge_item_vat'];
                        $bookingDateFinances->final_total = $data['charge_item_price'] * $temp_quantity;
                        $bookingDateFinances->quantity = isset($data['charge_item_qty'])?$data['charge_item_qty']:1;
                        $bookingDateFinances->show_receipt = BookingDateFinances::SHOW_RECEIPT;
                        $bookingDateFinances->save();

                        $remaining_balance = floatval($remaining_balance) + floatval($bookingDateFinances->amount);
                    }
                    break;

                case 'payment':

                    if(!empty($prev_booking_id))
                    {
                        $bookingDateFinances = new BookingDateFinances();
                        $bookingDateFinances->date = $data['payment_item_date'];
                        $bookingDateFinances->booking_item_id =$prev_booking_id;
                        $bookingDateFinances->booking_group_id = $prev_group_id; 
                        $bookingDateFinances->type = BookingDateFinances::TYPE_CREDIT;
                        $bookingDateFinances->price_description = $data['payment_item_description'];
                        $bookingDateFinances->amount = $data['payment_item_amount'];
                        $bookingDateFinances->final_total = $data['payment_item_amount'];
                        $bookingDateFinances->quantity = 1;
                        $bookingDateFinances->show_receipt = BookingDateFinances::SHOW_RECEIPT;
                        $bookingDateFinances->payment_type = BookingDateFinances::PAYMETN_TYPE_NORMAL;
                        $bookingDateFinances->save();

                        $remaining_balance = floatval($remaining_balance) - floatval($bookingDateFinances->amount);
                    }
                    break;
            }
        }

        if(!empty($prev_booking_id))
        {
            $booking = BookingsItems::findOne(['id' => $prev_booking_id]);
            $booking->balance = ($remaining_balance>0.0)?$remaining_balance:0.0;
            $booking->update();
        }

        $this->AddImportedBookingDetails($new_bookings);
        return $resultArr;
    }

    public function AddImportedBookingDetails($bookings)
    {
        // echo "<pre>";
        // print_r($bookings);
        // echo "<br>";
        // echo count($bookings);
        // exit();
        if(!empty($bookings))
        {
            foreach ($bookings as $booking) 
            {
                $orgBookingItem = BookingsItems::findOne(['id' => $booking]);

                // if(!empty($orgBookingItem->bookingItemGroups->bookingGroup->bookingItemGroups))
                // {
                //     $group_balance = 0;
                //     $booking_group = $orgBookingItem->bookingItemGroups->bookingGroup;
                //     foreach ($orgBookingItem->bookingItemGroups->bookingGroup->bookingItemGroups as $value) 
                //     {
                //         $group_balance += $value->bookingItem->balance;
                //     }
                //     $booking_group->balance = $group_balance;
                //     $booking_group->save();
                // }
                if(empty($orgBookingItem))
                {
                    echo "<pre>";
                    print_r($booking);
                    exit();
                }
                $booking_charges = BookingDateFinances::find()->where(['booking_item_id' => $orgBookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->all();
                if(!empty($booking_charges))
                {
                    foreach ($booking_charges as $booking_charge) 
                    {
                        $temp_discount = 0;
                        $temp_x = ($booking_charge->amount )/(1 + ($booking_charge->vat)/100);

                        $temp_y = $temp_x - ($booking_charge->tax);

                        // if(isset($booking_charge->discount_amount) && ($booking_charge->discount_amount != null ) )
                        // {

                        //         $temp_discount = ($booking_charge->amount)*($booking_charge->discount/100);
                        //         $booking_charge->discount_amount = round($temp_discount,2);

                        //         $booking_charge->save();
                            
                        // }

                        if(isset($orgBookingItem->travel_partner_id) && !empty($orgBookingItem->travel_partner_id) )
                        {

                            // checking if travel partner calculation type is commission //
                            $has_commission = false;
                            if($orgBookingItem->travelPartner->travelPartner->calculation == TravelPartner::CALC_DISCOUNT)
                            {
                               
                                $booking_charge->discount = $orgBookingItem->travelPartner->comission;
                                $temp_discount = $temp_y*($orgBookingItem->travelPartner->comission/100);
                                $booking_charge->discount_amount = round($temp_discount,2);

                                
                            }
                            if($orgBookingItem->travelPartner->travelPartner->calculation == TravelPartner::CALC_COMMISSION)
                            { 
                                $has_commission = true;
                                $booking_charge->discount = $orgBookingItem->travelPartner->comission;
                                // $temp_discount = $temp_y*($bDate->travelPartner->comission/100);

                                $temp_discount = ($booking_charge->amount)*($orgBookingItem->travelPartner->comission/100);
                                $booking_charge->discount_amount = round($temp_discount,2);

                                $temp_new_vat_rate = $booking_charge->vat;
                                if($temp_new_vat_rate > 0)
                                {
                                    $cal_1 = 100 + $temp_new_vat_rate;
                                    $cal_2 = 100/$cal_1;
                                    $cal_3 = $booking_charge->final_total * $cal_2;
                                    $new_vat_amount = round($booking_charge->final_total  - $cal_3);

                                    $booking_charge->vat_amount  = $new_vat_amount;
                                }
                                else
                                {
                                    $has_commission = false;
                                }
                            }
                           

                            
                        }

                        $temp_tax = 0;
                        $temp_subtotal1 = $temp_y - $temp_discount;
                         
                        $temp_subtotal2 = $temp_subtotal1 + $temp_tax;

                        $temp_vat_amount = $temp_subtotal2*($booking_charge->vat/100);
                        if(! $has_commission)
                        {
                            $booking_charge->vat_amount  = round($temp_vat_amount,2);
                        }
                        

                        $booking_charge->save();
                    }
                }

                $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $orgBookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

                $orgBookingItem->gross_total = $total_debit;

                $orgBookingItem->averaged_total = round($total_debit/$orgBookingItem->no_of_booking_days);
                // $orgBookingItem->net_total = $net_total;
                $total_discount = BookingDateFinances::find()->where(['booking_item_id' => $orgBookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('discount_amount');

                $orgBookingItem->net_total = $total_debit - round($total_discount);

                $total_paid = BookingDateFinances::find()->where(['booking_item_id' => $orgBookingItem->id])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                          ->sum('final_total');
                $orgBookingItem->paid_amount = round($total_paid);
                $orgBookingItem->balance = round($total_debit - $total_paid);

                $orgBookingItem->save();

                if(!empty($orgBookingItem->bookingItemGroups->bookingGroup->bookingItemGroups))
                {
                    $group_balance = 0;
                    $booking_group = $orgBookingItem->bookingItemGroups->bookingGroup;
                    foreach ($orgBookingItem->bookingItemGroups->bookingGroup->bookingItemGroups as $value) 
                    {
                        $group_balance += $value->bookingItem->balance;
                    }
                    $booking_group->balance = $group_balance;
                    $booking_group->save();
                }
                if(empty($orgBookingItem))
                {
                    echo "<pre>";
                    print_r($booking);
                    exit();
                }
            }
        }
    }

    public function delete_directory($dirname) {
        if (is_dir($dirname))
           $dir_handle = opendir($dirname);
         if (!$dir_handle)
              return false;
         while($file = readdir($dir_handle)) {
               if ($file != "." && $file != "..") {
                    if (!is_dir($dirname."/".$file))
                         unlink($dirname."/".$file);
                    else
                        $this->delete_directory($dirname.'/'.$file);
               }
         }
         closedir($dir_handle);
         rmdir($dirname);
         return true;
    }

    public function actionDeleteBookingsGridview()
    {
        foreach ($_POST['keys'] as $id) 
        {
            $model = $this->findModel($id);
            // Deleting Notifications //
            Notifications::deleteAll(['AND', 'booking_item_id = :booking_item_id', ['NOT IN', 'notification_type', [Notifications::UNIT_OVERBOOKING,Notifications::DESTINATION_OVERBOOKING]]], [':booking_item_id' => $model->id]);

            foreach ($model->bookingDatesSortedAndNotNull as $booking_date_obj) 
            {
                Notifications::removeDestinationOverbooking($model,$booking_date_obj);
                Notifications::removeUnitOverbooking($model,$booking_date_obj);
            }
            if(isset($model->bookingItemGroups))
            {
                $this->UnlinkItemFromGroup($model->id);
            }
            // $this->UnlinkItemFromGroup($model->id);
            $model->deleted_at = strtotime(date('Y-m-d H:i:s'));
            $model->update();
        }
    }

    public function actionUpdateBookingsFlagsGridview()
    {
        foreach ($_POST['keys'] as $id)
        {
            $model = $this->findModel($id);
            $model->flag_id = $_POST['flag_id'];
            $model->update();

            foreach ($model->bookingDatesSortedAndNotNull as $key => $bookingDate) 
            {
                $bookingDate->flag_id = $_POST['flag_id'];
                $bookingDate->save();
            }
        }
    }

    public function actionUpdateBookingsFlagLateCheckinGridview()
    {
        $bookingType = BookingTypes::findOne(['label' => 'Late Arrival']);

        $model = $this->findModel($_POST['id']);
        $model->flag_id = $bookingType->id;
        $model->update();

        foreach ($model->bookingDatesSortedAndNotNull as $key => $bookingDate) 
        {
            $bookingDate->flag_id = $bookingType->id;
            $bookingDate->save();
        }
    }

    public function actionHasHousekeeping()
    {
        $destination_id = $_POST['id'];
        
        $destination = Destinations::findOne(['id' => $destination_id]);
        if($destination->use_housekeeping == 1)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function isAttributeUpdated($attribute,$old_value,$new_value,$booking_id)
    {
        // if($attribute == 'payment invoice')
        // {
        //     echo "<pre>";
        //     print_r($old_value);
        //     echo "<br>";
        //     print_r($new_value);
        //     exit();
        //     die();
        // }
        if($old_value != $new_value)
        {
            $booking_log = new BookingsLog;
            $booking_log->user_id = Yii::$app->user->id;
            $booking_log->booking_id = $booking_id;
            $booking_log->setting = $attribute;
            $booking_log->old_value = (string)$old_value;
            $booking_log->new_value = (string) $new_value;
            if(!$booking_log->save())
            {
                echo "<pre>";
                print_r($booking_log->errors);
                echo "<br>";
                print_r($attribute);
                exit();
            }
            // $booking_log->save();
        }
    }

    public function isArrayAttributeUpdated($attribute,$old_value,$new_value,$booking_id)
    {
        // if($attribute == 'No of Adults')
        // {
        //     echo "<pre>";
        //     print_r($old_value);
        //     echo "<br>";
        //     print_r($new_value);
        //     echo "<br>";
        //     print_r(array_diff($new_value,$old_value));
        //     exit();
        // }
        if(!empty(array_diff($old_value,$new_value) ) ||  !empty(array_diff($new_value,$old_value) ) )
        {
            $booking_log = new BookingsLog;
            $booking_log->user_id = Yii::$app->user->id;
            $booking_log->booking_id = $booking_id;
            $booking_log->setting = $attribute;
            $booking_log->old_value = !empty($old_value)?implode(',', $old_value):'';
            $booking_log->new_value = !empty($new_value)?implode(',', $new_value):'';
            $booking_log->save();
        }
    }

    public function isChargesAttributeUpdated($attribute,$old_value,$new_value,$booking_id)
    {
        foreach ($old_value as $key => $value) 
        {
            if(array_key_exists($key, $new_value))
            {
                if($old_value != $new_value)
                {
                    $booking_log = new BookingsLog;
                    $booking_log->user_id = Yii::$app->user->id;
                    $booking_log->booking_id = $booking_id;
                    $booking_log->setting = $attribute;
                    $booking_log->old_value = $value;
                    $booking_log->new_value = $new_value[$key];
                    $booking_log->save();
                }
                    
            }
            else
            {
                // $booking_log = new BookingsLog;
                // $booking_log->user_id = Yii::$app->user->id;
                // $booking_log->booking_id = $booking_id;
                // $booking_log->setting = $attribute;
                // $booking_log->old_value = $value;
                // $booking_log->new_value = $new_value[$key];
                // $booking_log->save();
            }
        }

        foreach ($new_value as $key => $value) 
        {
            if(array_key_exists($key, $old_value))
            {
                    
            }
            else
            {
                $booking_log = new BookingsLog;
                $booking_log->user_id = Yii::$app->user->id;
                $booking_log->booking_id = $booking_id;
                $booking_log->setting = $attribute;
                $booking_log->old_value = '';
                $booking_log->new_value = $value;
                $booking_log->save();
            }
        }
    }
}

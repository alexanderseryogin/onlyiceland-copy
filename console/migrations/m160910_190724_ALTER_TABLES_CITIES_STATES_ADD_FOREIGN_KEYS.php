<?php

use yii\db\Migration;

class m160910_190724_ALTER_TABLES_CITIES_STATES_ADD_FOREIGN_KEYS extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `cities` ADD CONSTRAINT `FK_city_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE CASCADE;");

        $this->execute("ALTER TABLE `states` ADD CONSTRAINT `FK_states_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE CASCADE;");
    }

    public function down()
    {
        echo "m160910_190724_ALTER_TABLES_CITIES_STATES_ADD_FOREIGN_KEYS cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

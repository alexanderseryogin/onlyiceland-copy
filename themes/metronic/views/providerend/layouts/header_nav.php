<?php
    $path = \Yii::$app->request->BaseUrl . '/themes/metronic';
?>
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?= \yii\helpers\Url::to(['/site/index']) ?>">
                <img style="margin-top: 7px;max-height: 60px;" src="<?= Yii::getAlias('@web')?>/frontend/assets/img/oi_logo.png" alt="logo" class="logo-default" /> </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        <?php 
            //echo $this->render('header_nav_actions');
        ?>
        <!-- END PAGE ACTIONS -->

        <!-- BEGIN PAGE TOP -->
        <div class="page-top">

            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <?php 
                //echo $this->render('header_nav_search');
            ?>
            <!-- END HEADER SEARCH BOX -->

            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <?php 
                        //echo $this->render('header_nav_notifications');
                    ?>
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile"> <?php if(!Yii::$app->user->isGuest){echo Yii::$app->user->identity->username; }?> </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img src="<?php if(empty(Yii::$app->user->identity->profile->profile_picture)){echo Yii::getAlias('@web').'/frontend/assets/img/dummy_profile.jpg';}else{echo Yii::getAlias('@web').'/uploads/user-profile/'.Yii::$app->user->identity->profile->profile_picture;} ?>" class="img-circle" alt=""/> </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/provider-admin/user-profile']) ?>">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <!--
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/provider-admin/default/change-password']) ?>">
                                    <i class="icon-lock"></i> Change Password </a>
                            </li>
                            -->
                            <li class="divider"> </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!--
                    <li class="dropdown dropdown-extended quick-sidebar-toggler">
                        <span class="sr-only">Toggle Quick Sidebar</span>
                        <i class="icon-logout"></i>
                    </li>
                    -->
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
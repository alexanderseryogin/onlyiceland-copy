<?php

use yii\db\Migration;

class m161018_070047_create_table_discount_code extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `discount_code`( `id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `amount` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `discount_type_id` INT(11), `enterable_by_registered_users` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `active` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_discount_code_type_id` FOREIGN KEY (`discount_type_id`) REFERENCES `discount_code_types`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161018_070047_create_table_discount_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170413_111128_alter_booking_items extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items`   
                          ADD COLUMN `guest_id` INT(11) UNSIGNED NULL AFTER `user_id`,
                          ADD CONSTRAINT `FK_bookings_items_guest_id` FOREIGN KEY (`guest_id`) REFERENCES `booking_guests`(`id`) ON UPDATE SET NULL ON DELETE SET NULL;
                        ');
    }

    public function down()
    {
        echo "m170413_111128_alter_booking_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php
use common\models\Rates;
use common\models\RatesDates;
use common\models\RatesCapacityPricing;
use common\models\Destinations;
use yii\helpers\ArrayHelper;
use common\models\BookingTypes;
use common\models\BookingStatuses;
use common\models\BookingConfirmationTypes;
use common\models\User;
use common\models\BookingsItems;
use common\models\BookableBedTypes;
use common\models\BedTypes;
use common\models\BookableBedsCombinations;
use common\models\BookingDates;
use common\models\Countries;
use common\models\BookableItemsNames;
use common\models\BookingsDiscountCodes;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\EstimatedArrivalTimes;
use common\models\BookingSpecialRequests;
use common\models\BookingsUpsellItems;
use common\models\RatesOverride;
use yii\helpers\Url;
$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$arrival_date = str_replace('/', '-', $arrival_date);
$arrival_date = date('Y-m-d',strtotime($arrival_date));

$departure_date = str_replace('/', '-', $departure_date);
$departure_date = date('Y-m-d',strtotime($departure_date));

$arrival_day = date('N',strtotime($arrival_date));
$departure_day = date('N',strtotime($departure_date));

$date1=date_create($departure_date);
$date2=date_create($arrival_date);
$diff=date_diff($date1,$date2);
$difference =  $diff->format("%a");

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

$record_flag = 0;
$rate_conditions_flag = 0;

$price_json = '';
$show_item_flag = 0;
$mergeDatesArr = [];
$combine_dates = '';
$combine_dates_arr = '';

$ratesArr = BookingsItems::checkAvailableRates($item->id,$arrival_date,$difference);
$availableRates = $ratesArr['availableRates'];
$availableRatesIds = $ratesArr['availableRatesIds'];

$bookedDates = $model->bookingDatesSortedAndNotNull;

// echo '<pre>';
// print_r($availableRates);
// echo '</pre>';
// exit;


$bookedRates =array();

foreach ($bookedDates as $value) 
{
    if(!in_array($value->rate_id, $bookedRates))
    {
        array_push($bookedRates, $value->rate_id);
    }
}

$bookedRatesRecord = array();
foreach ($bookedDates as $value) 
{
    if(!array_key_exists($value->rate_id, $bookedRatesRecord))
    {
        $bookedRatesRecord[$value->rate_id] = $value;
    }
}

$testBookedRatesRecord = array();
foreach ($bookedDates as $value) 
{
    // if(!array_key_exists($value->rate_id, $testBookedRatesRecord))
    // {
        $testBookedRatesRecord[$value->rate_id][$value->date] = $value;
    // }
}

// echo '<pre>';
// print_r($testBookedRatesRecord);
// echo '</pre>';
// exit;


$rate_conditions_flag = BookingsItems::checkRateConditions($item->id,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions);
//$availableItemNames = BookingsItems::checkAvailableItemNamesIncludedSaved($item->id,$arrival_date,$difference,$model->id);



if(isset($remove_rate_conditions) && $remove_rate_conditions == 0 && !empty($availableRates) && count($availableRates)==$difference && $rate_conditions_flag==1)
{
    $show_item_flag = 1;
}

if(isset($remove_rate_conditions) && $remove_rate_conditions == 1 && !empty($availableRates))
{
    $show_item_flag = 1;
}

$newAvailableRates = [];

foreach ($availableRates as $dateKey => $rates) 
{
    foreach ($rates as $rate) 
    {   //echo "rate id ". $rate->id;
        if(in_array($rate->id, $bookedRates))
        {
            $newAvailableRates[$dateKey][] = $rate;
        }
    }

}

$test_count = 0;
if($show_item_flag)
{
    foreach ($availableRates as $dateKey => $rates) 
    {
        $dateFlag = 1;

        if($dateFlag)
        {
            $flag = 1;
            $discount_upsell_flag = 1;
            $json_price_flag = 1;

            $has_rate_flag = 0;

            $rates_arr = $availableRatesIds[$dateKey];
            $rates_arr_exploded = explode(',', $rates_arr);

            // echo "<pre>";
            // print_r($bookedRatesRecord);
            // exit();

            foreach ($rates_arr_exploded as $value) 
            {
                //echo $value."<br>";
                if(array_key_exists($value, $bookedRatesRecord))
                {
                    $has_rate_flag = 1;
                    break;
                }
            }

            // echo "<pre>";
            // print_r($dropdown_flag);
            // exit();
        ?>
            <table class="table table-bordered table-<?php echo $item->id?>" date="<?php echo $dateKey;?>">
                <thead>
                    <tr>
                        <strong>
                            <th width="13%">Date</th>
                            <th width="87%">Available Rates</th>
                        </strong>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <?php
                            $item_date = '';
                           
                            echo date('d/m/Y',strtotime($dateKey)).'<br>';
                            $item_date = $dateKey;
                        ?>
                        <br>

                        <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $model->item->id?>" class="form-control item-quantity item-quantity-<?php echo $model->item->id?> item-quantities-<?php echo $model->item->id.'-'.$dateKey?>" value="1">

                        <input type="hidden" date="<?php echo $dateKey; ?>" data="<?php echo $model->item->id?>" class="form-control item-date-<?php echo $model->item->id.'-'.$dateKey?>" value="<?php echo $item_date ?>">

                    </td>
                    <td>
                    <?php
                    foreach ($rates as $key => $rate)
                    {
                        $org_rate_exists = 0;
                        // ***************** add condition for checkin ******************* //
                        $rate_row = $key;
                        
                    ?>
                        <?php
                                            
                            $db_booking_date = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $dateKey]);
                            if(!empty($db_booking_date) && ($rate->id != $db_booking_date->rate_id))
                            { 
                                //$rate = $db_booking_date->rate;
                        ?>      
                                <div class="table-scrollable">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <strong>
                                                    <th width="20%">Rate</th>
                                                    
                                                    <?php
                                                        if($db_booking_date->rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                        {  
                                                            echo '<th width="15%">Capacity Pricing </th>';
                                                        }
                                                        else if($db_booking_date->rate->destination->pricing == 0)
                                                        {
                                                            echo '<th width="10%">Adults</th><th width="10%">Children</th>';
                                                        }
                                                    ?>
                                                    <?php if($db_booking_date->rate->destination->pricing == 1 && $db_booking_date->rate->destination->getDestinationTypeName()=='Accommodation') 
                                                            { ?>
                                                            <th width="10%">Adults/Children</th>
                                                            <?php } ?>
                                                    <th width="22%">Available Discounts</th>
                                                    <th width="28%">Upsell Items</th>
                                                    <th width="10%">Total </th>
                                                </strong>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td  style="min-width:120px;">
                                                <input type="text" value="<?php echo  $rate->name?>" hidden>
                                                <?php echo  $db_booking_date->rate->name.'<br><br>'?>

                                                <input type="text" value="<?php echo $db_booking_date->item_id.':'.$db_booking_date->rate->id.':'.$dateKey?>" class="helper-field helper-field-<?php echo $db_booking_date->item_id?> helper-field-<?php echo $db_booking_date->item_id.'-'.$dateKey?>" hidden>
                                            </td>

                                            <?php

                                            // ************* capacity pricing ************ //

                                            if($db_booking_date->rate->destination->pricing == 1 && $db_booking_date->rate->destination->getDestinationTypeName()=='Accommodation') 
                                            {
                                                $org_rate_exists = 0;
                                            ?>
                                                <td date="<?php echo $dateKey; ?>" class="capacity-pricing-box capacity-pricing-box-<?php echo $item->id.'-'.$dateKey?>">
                                                <!-- Adding new code to display original rate  -->
                                                <?php
                                                    /*
                                                    $db_booking_date = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $dateKey]);
                                                    if(!empty($db_booking_date) && ($rate->id != $db_booking_date->rate_id))
                                                    {    
                                                        $rates_capacity_pricing_org = RatesCapacityPricing::findOne(['rates_id' => $db_booking_date->rate_id, 'person_no' => $db_booking_date->person_no ]);

                                                        $price = $rates_capacity_pricing_org->price;
                                                        $custom_rate = '';

                                                        if($db_booking_date->custom_rate != null)
                                                        {
                                                            $custom_rate = Yii::$app->formatter->asDecimal( $db_booking_date->custom_rate, "ISK");
                                                        }
                                                        else
                                                        {
                                                            
                                                            $price = Yii::$app->formatter->asDecimal( $rates_capacity_pricing_org->price, "ISK");
                                                        }

                                                        $icon_user = '';

                                                        for ($i=1; $i <= $db_booking_date->person_no ; $i++) 
                                                        { 
                                                            $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                        }
                                                        $org_rate_exists = 1;
                                                ?>
                                                <label>
                                                    <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $db_booking_date->person_no?>" id="<?php echo $item->id.':'.$rate->id?>" class="old capacity-pricing capacity-pricing-<?php echo $item->id?> capacity-pricing-<?php echo $item->id.'-'.$dateKey?> cpricing-<?php echo $rate->id?> cpricing-<?php echo $rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $item->id.'-'.$dateKey.'-'.$rate_row?>" value="<?php echo $db_booking_date->person_no.' : '.$price?>" <?php echo 'checked'?>> <?php echo $icon_user ?>
                                                    <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$item->id.':'.$rate->id.':'.$db_booking_date->person_no?>" value="<?=($db_booking_date->custom_rate != null )?$custom_rate:$price?>" date="<?php echo $dateKey?>" style="text-align:right;width: 50%;float: right;<?=($db_booking_date->custom_rate != null )?'color:red;':''?>" data="<?php echo $db_booking_date->person_no?>" >
                                                </label>
                                                <hr>
                                                <?php } */?>
                                                <!-- end new code -->
                                            <?php

                                                $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $db_booking_date->rate->id])->all();

                                                $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $rate->id]);
                                                // if($dateKey == '2018-02-01')
                                                // {
                                                //     echo "<pre>";
                                                //     echo "1";
                                                //     echo $dateKey;
                                                //     print_r($rate_override);
                                                //     exit();
                                                // }
                                                if(!empty($rates_capacity_pricing))
                                                {
                                                    foreach ($rates_capacity_pricing as $key => $value)
                                                    {
                                                        if($value->person_no >=1 && $value->person_no<=4)
                                                        {
                                                            $checked = 0;
                                                            
                                                            if(array_key_exists($rate->id, $bookedRatesRecord))
                                                            {
                                                                $flag = 0;
                                                                $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                $checked = ($value->person_no == $bookedSingleRateRecord->person_no)? 1 : 0;
                                                            }
                                                            else
                                                            {
                                                                if($has_rate_flag == 0)
                                                                {
                                                                    $bookedSingleRateRecord =null;
                                                                    foreach ($bookedRatesRecord as  $value1) 
                                                                    {
                                                                        $bookedSingleRateRecord = $value1;
                                                                        break;
                                                                    }
                                                                    // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];

                                                                    $checked = ($value->person_no == $bookedSingleRateRecord->person_no)? 1 : 0;
                                                                    if($checked == 1)
                                                                    {
                                                                        $flag = 0;
                                                                    }
                                                                }
                                                            }
                                                            

                                                            $price = $value->price;
                                                            $custom_rate = '';

                                                            if($checked == 1 )
                                                            {
                                                                if($bookedSingleRateRecord->custom_rate != null)
                                                                {
                                                                    $custom_rate = Yii::$app->formatter->asDecimal( $bookedSingleRateRecord->custom_rate, "ISK");
                                                                }
                                                                else
                                                                {
                                                                    $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if(!empty($rate_override))
                                                                {
                                                                    if($key==0)
                                                                    {
                                                                        $value->price = $rate_override->single;
                                                                    }
                                                                    if($key==1)
                                                                    {
                                                                        $value->price = $rate_override->double;
                                                                    }
                                                                    if($key==2)
                                                                    {
                                                                        $value->price = $rate_override->triple;
                                                                    }
                                                                    if($key==3)
                                                                    {
                                                                        $value->price = $rate_override->quad;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                                }
                                                                
                                                            }

                                                            if($org_rate_exists == 1)
                                                            {
                                                                $checked = 0;
                                                            }
                                                            $icon_user = '';

                                                            for ($i=1; $i <= $value->person_no ; $i++) 
                                                            { 
                                                                $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                            }
                                                            ?>
                                                            <label>
                                                                <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $value->person_no?>" id="<?php echo $model->item->id.':'.$db_booking_date->rate->id?>" class="capacity-pricing capacity-pricing-<?php echo $model->item->id?> capacity-pricing-<?php echo $model->item->id.'-'.$dateKey?> cpricing-<?php echo $db_booking_date->rate->id?> cpricing-<?php echo $db_booking_date->rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $model->item->id.'-'.$dateKey.'-'.$rate_row?>" value="<?php echo $value->person_no.' : '.$price?>" <?php echo $Options[$checked]?>> <?php echo $icon_user ?>
                                                                <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$model->item->id.':'.$db_booking_date->rate->id.':'.$value->person_no?>" value="<?=($checked == 1 && $bookedSingleRateRecord->custom_rate != null )?$custom_rate:$value->price?>" date="<?php echo $dateKey?>" style="text-align:right;width: 50%;float: right;<?=($checked == 1 && $bookedSingleRateRecord->custom_rate != null && $bookedSingleRateRecord->rate_added_through == 0)?'color:red;':''?> <?=($bookedSingleRateRecord->custom_rate != null && $bookedSingleRateRecord->rate_added_through == 1 )?'color:orange;':''?> <?=(!empty($rate_override))?'color:orange;':''?>" data="<?php echo $value->person_no?>" >
                                                            </label><br>
                                                            <?php
                                                        }
                                                    } 
                                                }
                                               echo '</td>';?>
                                                    <td style="min-width:20px;">
                                                        <label>AD</label>
                                                        <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$db_booking_date->rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $db_booking_date->rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $db_booking_date->rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                            <?php

                                                                if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                                                {
                                                                    //$flag = 0;
                                                                    $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                                                    //$selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                }
                                                                else
                                                                {
                                                                    $bookedSingleRateRecord =null;
                                                                    foreach ($bookedRatesRecord as  $value1) 
                                                                    {
                                                                        
                                                                        $bookedSingleRateRecord = $value1;
                                                                        break;
                                                                    }
                                                                }
                                                                $adult_count = isset($bookedSingleRateRecord->item->max_adults)?$bookedSingleRateRecord->item->max_adults:10;
                                                                for ($i=0; $i <= $adult_count ; $i++) 
                                                                {
                                                                    $selected = 0;
                                                                
                                                                    if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                                                    {
                                                                        $flag = 0;
                                                                        $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                                                        $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if($has_rate_flag == 0)
                                                                        {
                                                                           
                                                                            $bookedSingleRateRecord =null;
                                                                            foreach ($bookedRatesRecord as  $value1) 
                                                                            {
                                                                                
                                                                                $bookedSingleRateRecord = $value1;
                                                                                break;
                                                                            }
                                                                            // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                            $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                            if($selected == 2)
                                                                            {
                                                                                $flag = 0;
                                                                            }
                                                                        }
                                                                    }

                                                                    $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                        <br>
                                                        <label>CH</label>
                                                        <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$db_booking_date->rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $db_booking_date->rate->id.'-'.$dateKey?> child-dropdown-<?php echo $db_booking_date->rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                            <?php
                                                                if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                                                {
                                                                    //$flag = 0;
                                                                    $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                                                    //$selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                }
                                                                else
                                                                {
                                                                    $bookedSingleRateRecord =null;
                                                                    foreach ($bookedRatesRecord as  $value1) 
                                                                    {
                                                                        
                                                                        $bookedSingleRateRecord = $value1;
                                                                        break;
                                                                    }
                                                                }
                                                                $children_count = isset($bookedSingleRateRecord->item->max_children)?$bookedSingleRateRecord->item->max_children:10;
                                                                for ($i=0; $i <= $children_count ; $i++) 
                                                                {
                                                                    $selected = 0;
                                                                
                                                                    if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                                                    {
                                                                        $flag = 0;
                                                                        $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                                                        $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                    }
                                                                    else
                                                                    {
                                                                        if($has_rate_flag == 0)
                                                                        {
                                                                           
                                                                            $bookedSingleRateRecord =null;
                                                                            foreach ($bookedRatesRecord as  $value1) 
                                                                            {
                                                                                
                                                                                $bookedSingleRateRecord = $value1;
                                                                                break;
                                                                            }
                                                                            // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                            $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                            if($selected == 2)
                                                                            {
                                                                                $flag = 0;
                                                                            }
                                                                        }
                                                                    }
                                                                    $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                                    echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </td>
                                            <?php }
                                            else if($db_booking_date->rate->destination->pricing == 0)  // ************* First/Additional ************ //
                                            {
                                            ?>
                                                <td style="min-width:50px;">
                                                    <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$db_booking_date->rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $db_booking_date->rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $db_booking_date->rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                        <?php
                                                            for ($i=0; $i <= 20 ; $i++) 
                                                            { 
                                                                $selected = 0;
                                                                
                                                                if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                                                {
                                                                    $flag = 0;
                                                                    $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                                                    $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                }
                                                                else
                                                                {
                                                                    if($has_rate_flag == 0)
                                                                    {
                                                                       
                                                                        $bookedSingleRateRecord =null;
                                                                        foreach ($bookedRatesRecord as  $value1) 
                                                                        {
                                                                            
                                                                            $bookedSingleRateRecord = $value1;
                                                                            break;
                                                                        }
                                                                        // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                        $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                        if($selected == 2)
                                                                        {
                                                                            $flag = 0;
                                                                        }
                                                                    }
                                                                    // if($flag == 1)
                                                                    // {
                                                                       
                                                                    //     $bookedSingleRateRecord =null;
                                                                    //     foreach ($bookedRatesRecord as  $value1) 
                                                                    //     {
                                                                            
                                                                    //         $bookedSingleRateRecord = $value1;
                                                                    //         break;
                                                                    //     }
                                                                    //     // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                    //     $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                    //     if($selected == 2)
                                                                    //     {
                                                                    //         $flag = 0;
                                                                    //     }
                                                                    // }
                                                                }
                                                                
                                                                echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </td>

                                                <td style="min-width:50px;">
                                                    <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$db_booking_date->rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $db_booking_date->rate->id.'-'.$dateKey?> child-dropdown-<?php echo $db_booking_date->rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                        <?php
                                                            for ($i=0; $i <= 20 ; $i++) 
                                                            {
                                                                $selected = 0;

                                                                if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                                                {
                                                                    $flag = 0;
                                                                    $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                                                    $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                                }
                                                                else
                                                                {
                                                                    if($has_rate_flag == 0)
                                                                    {
                                                                       
                                                                        $bookedSingleRateRecord =null;
                                                                        foreach ($bookedRatesRecord as  $value1) 
                                                                        {
                                                                            
                                                                            $bookedSingleRateRecord = $value1;
                                                                            break;
                                                                        }
                                                                        // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                        $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                                        if($selected == 2)
                                                                        {
                                                                            $flag = 0;
                                                                        }
                                                                    }
                                                                    // if($flag == 1)
                                                                    // {
                                                                       
                                                                    //     $bookedSingleRateRecord =null;
                                                                    //     foreach ($bookedRatesRecord as  $value1) 
                                                                    //     {
                                                                            
                                                                    //         $bookedSingleRateRecord = $value1;
                                                                    //         break;
                                                                    //     }
                                                                    //     // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                    //     $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                                    //     if($selected == 2)
                                                                    //     {
                                                                    //         $flag = 0;
                                                                    //     }
                                                                    // }
                                                                }
                                                                 
                                                                echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </td>
                                            <?php
                                            }

                                        $max_extra_beds = $item->max_extra_beds;
                                        $bed_type_people = 0;

                                        $selected_upsell_extra_bed_quantity = [];
                                        $selected_discount_code_extra_bed_quantity = [];

                                        if($db_booking_date->rate->destination->pricing == 1 && $db_booking_date->rate->destination->getDestinationTypeName()=='Accommodation')
                                        {
                                            $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $item->id])->all();

                                            if(!empty($itemBedTypesModel))
                                            {
                                                foreach ($itemBedTypesModel as $key => $value) 
                                                {
                                                    $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                                }
                                            }
                                        }

                                        // *********** Discount Codes ********** //

                                        $checked_discount_codes = [];
                                        $checked_upsell_items = [];

                                        if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                        {   
                                            $discount_upsell_flag = 0;
                                            $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                            $get_discount_codes = BookingsDiscountCodes::find()->where(['booking_date_id' => $bookedSingleRateRecord->id])->all();

                                            if(!empty($get_discount_codes))
                                            {
                                                foreach ($get_discount_codes as $key => $value) 
                                                {
                                                    $checked_discount_codes[$value['discount_id']] = $value['price'];
                                                    $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                                }
                                            }


                                            $get_upsell_items = BookingsUpsellItems::find()->where(['booking_date_id' => $db_booking_date->id])->all();

                                            if(!empty($get_upsell_items))
                                            {
                                                foreach ($get_upsell_items as $key => $value) 
                                                {
                                                    $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                                    $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                                }
                                            }

                                        }
                                        else
                                        {
                                            if($has_rate_flag == 0)
                                            {
                                               
                                                $bookedSingleRateRecord =null;
                                                foreach ($bookedRatesRecord as  $value1) 
                                                {
                                                    
                                                    $bookedSingleRateRecord = $value1;
                                                    break;
                                                }

                                                $get_discount_codes = BookingsDiscountCodes::find()->where(['booking_date_id' => $db_booking_date->id])->all();

                                                if(!empty($get_discount_codes))
                                                {
                                                    foreach ($get_discount_codes as $key => $value) 
                                                    {
                                                        $checked_discount_codes[$value['discount_id']] = $value['price'];
                                                        $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                                    }
                                                }

                                                $get_upsell_items = BookingsUpsellItems::find()->where(['booking_date_id' => $db_booking_date->id])->all();

                                                if(!empty($get_upsell_items))
                                                {
                                                    foreach ($get_upsell_items as $key => $value) 
                                                    {
                                                        $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                                        $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                                    }
                                                }
                                                if(!empty($get_upsell_items) || !empty($get_discount_codes))
                                                {
                                                    $discount_upsell_flag = 0;
                                                }
                                            }
                                        }

                                        $org_booked_item = null;
                                        foreach ($bookedRatesRecord as  $value2) 
                                        {
                                            
                                            $org_booked_item = $value2;
                                            break;
                                        }

                                        echo $this->render('discount_codes',
                                        [
                                            'dateKey' => $dateKey,
                                            'rates_id' => $db_booking_date->rate->id,
                                            'checked_discount_codes' => $checked_discount_codes,
                                            'bed_type_people' => $bed_type_people,
                                            'max_extra_beds' => $max_extra_beds,
                                            'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                            'org_rate_exists' => $org_rate_exists,
                                        ]);

                                        // *********** Upsell Items ********** //

                                        echo $this->render('upsell_items',
                                        [
                                            'dateKey' => $dateKey,
                                            'rates_id' => $db_booking_date->rate->id,
                                            'org_rate_id' => $org_booked_item->rate_id,
                                            'checked_upsell_items' => $checked_upsell_items,
                                            'max_extra_beds' => $max_extra_beds,
                                            'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                            'org_rate_exists' => $org_rate_exists,
                                        ]);

                                            $price_json = '';
                                            $orgTotal = 0;
                                            $icelandTotal = 0;
                                            $tooltips_title = 'Item Price';
                                            if(array_key_exists($db_booking_date->rate->id, $bookedRatesRecord))
                                            {
                                                $json_price_flag = 0;
                                                $bookedSingleRateRecord = $bookedRatesRecord[$db_booking_date->rate->id];
                                            }
                                            else
                                            {
                                                $bookedSingleRateRecord =null;
                                                if ($json_price_flag == 1) 
                                                {
                                                   
                                                    foreach ($bookedRatesRecord as  $value1) 
                                                    {
                                                        
                                                        $bookedSingleRateRecord = $value1;
                                                        break;
                                                    }
                                                    if(!empty($bookedSingleRateRecord->total))
                                                    {
                                                        $json_price_flag = 0;
                                                    }
                                                }
                                            }   
                                            if(!empty($bookedSingleRateRecord))
                                            {
                                                if(!empty($bookedSingleRateRecord->total))
                                                {
                                                    $orgTotal = $bookedSingleRateRecord->total;
                                                    $icelandTotal = Yii::$app->formatter->asDecimal($orgTotal, "ISK");
                                                }

                                                if(!empty($bookedSingleRateRecord->price_json))
                                                {
                                                    $arr = json_decode($bookedSingleRateRecord->price_json);

                                                    $tooltips_title = $tooltips_title . 'Quantity = '. $arr->quantity .' , ';
                                                    $tooltips_title = $tooltips_title . 'Total Nights = '. $arr->total_nights .' , ';
                                                    $tooltips_title = $tooltips_title . 'Rate = '. $arr->rate .' , ';
                                                    $tooltips_title = $tooltips_title . 'VAT % = '. $arr->vat .' , ';
                                                    $tooltips_title = $tooltips_title . 'x = '. $arr->x .' , ';
                                                    $tooltips_title = $tooltips_title . 'lodgingTax = '. $arr->lodgingTax .' , ';
                                                    $tooltips_title = $tooltips_title . 'y = '. $arr->y .' , ';
                                                    $tooltips_title = $tooltips_title . 'voucher_discount = '. $arr->voucher_discount .' , ';
                                                    $tooltips_title = $tooltips_title . 'travel_partner_discount = '. $arr->travel_partner_discount .' , ';
                                                    $tooltips_title = $tooltips_title . 'offer_discount = '. $arr->offer_discount .' , ';
                                                    $tooltips_title = $tooltips_title . 'amount_of_discount = '. $arr->amount_of_discount .' , ';
                                                    $tooltips_title = $tooltips_title . 'sub_total_1 = '. $arr->sub_total_1 .' , ';
                                                    $tooltips_title = $tooltips_title . 'sub_total_2 = '. $arr->sub_total_2 .' , ';
                                                    $tooltips_title = $tooltips_title . 'vat_amount = '. $arr->vat_amount .' , ';
                                                    $tooltips_title = $tooltips_title . 'Total Price = '. $arr->icelandPrice;

                                                    $price_json = str_replace('"', "'", $bookedSingleRateRecord->price_json);
                                                }
                                                else
                                                {
                                                    $tooltips_title = 'Item Price';
                                                    $price_json = '';
                                                }

                                            } 
                                            

                                            
                                            
                                            

                                        ?>

                                        <td>
                                            <input class="item-total-input item-total-input-<?php echo $db_booking_date->rate->id.'-'.$dateKey?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                            <input type="text" value="<?php echo $price_json; ?>" class="price-json-<?php echo $db_booking_date->rate->id.'-'.$dateKey?>" hidden>

                                            <label>
                                                <a href="javascript:;" data-placement="left" class="item-total-<?php echo $db_booking_date->rate->id.'-'.$dateKey?> total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                                <span><?php echo $icelandTotal?></span>
                                            </label>
                                            <lable class="item-total-nights-price-<?php echo $db_booking_date->rate->id.'-'.$dateKey?>"></label>
                                        </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                        <?php        
                                $org_rate_exists = 1;
                            }
                            else
                            {
                                $rate_row = $key;
                            }
                        ?>
                            
                        <div class="table-scrollable">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <strong>
                                            <th width="20%">Rate</th>
                                            
                                            <?php
                                                if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                {  
                                                    echo '<th width="15%">Capacity Pricing </th>';
                                                }
                                                else if($rate->destination->pricing == 0)
                                                {
                                                    echo '<th width="10%">Adults</th><th width="10%">Children</th>';
                                                }
                                            ?>
                                            <?php if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                                    { ?>
                                                    <th width="10%">Adults/Children</th>
                                                    <?php } ?>
                                            <th width="22%">Available Discounts</th>
                                            <th width="28%">Upsell Items</th>
                                            <th width="10%">Total </th>
                                        </strong>
                                    </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td  style="min-width:120px;">
                                        <input type="text" value="<?php echo  $rate->name?>" hidden>
                                        <?php echo  $rate->name.'<br><br>'?>

                                        <input type="text" value="<?php echo $item->id.':'.$rate->id.':'.$dateKey?>" class="helper-field helper-field-<?php echo $model->item->id?> helper-field-<?php echo $model->item->id.'-'.$dateKey?>" hidden>
                                    </td>

                                    <?php

                                    // ************* capacity pricing ************ //

                                    if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation') 
                                    {
                                        //$org_rate_exists = 0;
                                    ?>
                                        <td date="<?php echo $dateKey; ?>" class="capacity-pricing-box capacity-pricing-box-<?php echo $item->id.'-'.$dateKey?>">
                                        <!-- Adding new code to display original rate  -->
                                        <?php
                                        /*
                                            
                                            $db_booking_date = BookingDates::findOne(['booking_item_id' => $model->id, 'date' => $dateKey]);
                                            if(!empty($db_booking_date) && ($rate->id != $db_booking_date->rate_id))
                                            {    
                                                $rates_capacity_pricing_org = RatesCapacityPricing::findOne(['rates_id' => $db_booking_date->rate_id, 'person_no' => $db_booking_date->person_no ]);

                                                $price = $rates_capacity_pricing_org->price;
                                                $custom_rate = '';

                                                if($db_booking_date->custom_rate != null)
                                                {
                                                    $custom_rate = Yii::$app->formatter->asDecimal( $db_booking_date->custom_rate, "ISK");
                                                }
                                                else
                                                {
                                                    
                                                    $price = Yii::$app->formatter->asDecimal( $rates_capacity_pricing_org->price, "ISK");
                                                }

                                                $icon_user = '';

                                                for ($i=1; $i <= $db_booking_date->person_no ; $i++) 
                                                { 
                                                    $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                }
                                                $org_rate_exists = 1;
                                        ?>
                                        <label>
                                            <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $db_booking_date->person_no?>" id="<?php echo $item->id.':'.$rate->id?>" class="old capacity-pricing capacity-pricing-<?php echo $item->id?> capacity-pricing-<?php echo $item->id.'-'.$dateKey?> cpricing-<?php echo $rate->id?> cpricing-<?php echo $rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $item->id.'-'.$dateKey.'-'.$rate_row?>" value="<?php echo $db_booking_date->person_no.' : '.$price?>" <?php echo 'checked'?>> <?php echo $icon_user ?>
                                            <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$item->id.':'.$rate->id.':'.$db_booking_date->person_no?>" value="<?=($db_booking_date->custom_rate != null )?$custom_rate:$price?>" date="<?php echo $dateKey?>" style="text-align:right;width: 50%;float: right;<?=($db_booking_date->custom_rate != null )?'color:red;':''?>" data="<?php echo $db_booking_date->person_no?>" >
                                        </label>
                                        <hr>
                                        <?php } */ ?>
                                        <!-- end new code -->
                                    <?php

                                        $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $rate->id])->all();
                                        // if($dateKey == '2018-01-24')
                                        // {
                                        //     $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $rate->id]);
                                        //     echo "<pre>";
                                        //     print_r($has_rate_flag);
                                        //     exit();
                                        // }

                                        $rate_override = RatesOverride::findOne(['date' => $dateKey, 'rate_id' => $rate->id]);
                                        // if($dateKey == '2018-02-01')
                                        // {
                                        //     echo "<pre>";
                                        //     echo "2";
                                        //     echo $dateKey;
                                        //     print_r($rate_override);
                                        //     exit();
                                        // }
                                        if(!empty($rates_capacity_pricing))
                                        {
                                            foreach ($rates_capacity_pricing as $key => $value)
                                            {
                                                if($value->person_no >=1 && $value->person_no<=4)
                                                {
                                                    $checked = 0;
                                                    
                                                    if(array_key_exists($rate->id, $testBookedRatesRecord) and isset( $testBookedRatesRecord[$rate->id][$dateKey]))
                                                    {
                                                        // if($dateKey == '2018-01-26')
                                                        // {      
                                                        //     echo "key exists";
                                                        //     exit();
                                                        // }
                                                        $flag = 0;
                                                        $bookedSingleRateRecord = $testBookedRatesRecord[$rate->id][$dateKey];
                                                        $checked = ($value->person_no == $bookedSingleRateRecord->person_no)? 1 : 0;
                                                    }
                                                    else
                                                    {
                                                        // if($dateKey == '2018-01-26')
                                                        // {      
                                                        //     echo "key does not exists";
                                                        //     exit();
                                                        // }
                                                        if($has_rate_flag == 0)
                                                        {
                                                            $bookedSingleRateRecord =null;
                                                            foreach ($bookedRatesRecord as  $value1) 
                                                            {
                                                                $bookedSingleRateRecord = $value1;
                                                                break;
                                                            }
                                                            // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];

                                                            $checked = ($value->person_no == $bookedSingleRateRecord->person_no)? 1 : 0;
                                                            if($checked == 1)
                                                            {
                                                                $flag = 0;
                                                            }
                                                        }
                                                    }

                                                    // if($dateKey == '2018-01-26')
                                                    // {

                                                    //     echo "<pre>";
                                                    //     echo $dateKey;
                                                    //     print_r($bookedSingleRateRecord);
                                                    //     exit();
                                                    // }
                                                    

                                                    $price = $value->price;
                                                    $custom_rate = '';

                                                    if($checked == 1 )
                                                    {
                                                        if($bookedSingleRateRecord->custom_rate != null)
                                                        {
                                                            $custom_rate = Yii::$app->formatter->asDecimal( $bookedSingleRateRecord->custom_rate, "ISK");
                                                        }
                                                        else
                                                        {
                                                            
                                                            $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if(!empty($rate_override))
                                                        {
                                                            if($key==0)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->single, "ISK");
                                                            }
                                                            if($key==1)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->double, "ISK");
                                                            }
                                                            if($key==2)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->triple, "ISK");
                                                            }
                                                            if($key==3)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->quad, "ISK");
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                        }
                                                        // $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                    }

                                                    if($org_rate_exists == 1)
                                                    {
                                                        $checked = 0;
                                                    }
                                                    $icon_user = '';

                                                    for ($i=1; $i <= $value->person_no ; $i++) 
                                                    { 
                                                        $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                    }
                                                    ?>
                                                    <label>
                                                        <input type="checkbox" date="<?php echo $dateKey?>" data="<?php echo $value->person_no?>" id="<?php echo $item->id.':'.$rate->id?>" class="capacity-pricing capacity-pricing-<?php echo $model->item->id?> capacity-pricing-<?php echo $model->item->id.'-'.$dateKey?> cpricing-<?php echo $rate->id?> cpricing-<?php echo $rate->id.'-'.$dateKey?> capacity-pricing-<?php echo $model->item->id.'-'.$dateKey.'-'.$rate_row?>" value="<?php echo $value->person_no.' : '.$price?>" <?php echo $Options[$checked]?>> <?php echo $icon_user ?>
                                                        <input class="custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$model->item->id.':'.$rate->id.':'.$value->person_no?>" value="<?=($checked == 1 && $bookedSingleRateRecord->custom_rate != null )?$custom_rate:$value->price?>" date="<?php echo $dateKey?>" style="text-align:right;width: 50%;float: right;<?=($checked == 1 && $bookedSingleRateRecord->custom_rate != null && $bookedSingleRateRecord->rate_added_through == 0)?'color:red;':''?><?=($bookedSingleRateRecord->custom_rate != null && $bookedSingleRateRecord->rate_added_through == 1)?'color:orange;':''?> <?=(!empty($rate_override))?'color:orange;':''?>" data="<?php echo $value->person_no?>" >
                                                    </label><br>
                                                    <?php
                                                }
                                            } 
                                        }
                                       echo '</td>';?>
                                            <td style="min-width:20px;">
                                                <label>AD</label>
                                                <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                    <?php

                                                        if(array_key_exists($rate->id, $bookedRatesRecord))
                                                        {
                                                            //$flag = 0;
                                                            $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                            //$selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                        }
                                                        else
                                                        {
                                                            $bookedSingleRateRecord =null;
                                                            foreach ($bookedRatesRecord as  $value1) 
                                                            {
                                                                
                                                                $bookedSingleRateRecord = $value1;
                                                                break;
                                                            }
                                                        }
                                                        $adult_count = isset($bookedSingleRateRecord->item->max_adults)?$bookedSingleRateRecord->item->max_adults:10;
                                                        for ($i=0; $i <= $adult_count ; $i++) 
                                                        {
                                                            $selected = 0;
                                                        
                                                            if(array_key_exists($rate->id, $bookedRatesRecord))
                                                            {
                                                                $flag = 0;
                                                                $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                            }
                                                            else
                                                            {
                                                                if($has_rate_flag == 0)
                                                                {
                                                                   
                                                                    $bookedSingleRateRecord =null;
                                                                    foreach ($bookedRatesRecord as  $value1) 
                                                                    {
                                                                        
                                                                        $bookedSingleRateRecord = $value1;
                                                                        break;
                                                                    }
                                                                    // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                    $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                    if($selected == 2)
                                                                    {
                                                                        $flag = 0;
                                                                    }
                                                                }
                                                            }

                                                            $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                            if($org_rate_exists == 1)
                                                            {
                                                                $selected = 0;
                                                            }
                                                            echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                        }
                                                    ?>
                                                </select>
                                                <br>
                                                <label>CH</label>
                                                <select style="width:40%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                    <?php
                                                        if(array_key_exists($rate->id, $bookedRatesRecord))
                                                        {
                                                            //$flag = 0;
                                                            $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                            //$selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                        }
                                                        else
                                                        {
                                                            $bookedSingleRateRecord =null;
                                                            foreach ($bookedRatesRecord as  $value1) 
                                                            {
                                                                
                                                                $bookedSingleRateRecord = $value1;
                                                                break;
                                                            }
                                                        }
                                                        $children_count = isset($bookedSingleRateRecord->item->max_children)?$bookedSingleRateRecord->item->max_children:10;
                                                        for ($i=0; $i <= $children_count ; $i++) 
                                                        {
                                                            $selected = 0;
                                                        
                                                            if(array_key_exists($rate->id, $bookedRatesRecord))
                                                            {
                                                                $flag = 0;
                                                                $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                            }
                                                            else
                                                            {
                                                                if($has_rate_flag == 0)
                                                                {
                                                                   
                                                                    $bookedSingleRateRecord =null;
                                                                    foreach ($bookedRatesRecord as  $value1) 
                                                                    {
                                                                        
                                                                        $bookedSingleRateRecord = $value1;
                                                                        break;
                                                                    }
                                                                    // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                    $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                    if($selected == 2)
                                                                    {
                                                                        $flag = 0;
                                                                    }
                                                                }
                                                            }
                                                            $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                            if($org_rate_exists == 1)
                                                            {
                                                                $selected = 0;
                                                            }
                                                            echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                        }
                                                    ?>
                                                </select>
                                            </td>
                                    <?php }
                                    else if($rate->destination->pricing == 0)  // ************* First/Additional ************ //
                                    {
                                    ?>
                                        <td style="min-width:50px;">
                                            <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="adults-dropdown adults-dropdown-<?php echo $item->id?> adults-dropdown-<?php echo $item->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id.'-'.$dateKey?> adult-dropdown-<?php echo $rate->id?> adults-dropdown-<?php echo $item->id.'-'.$key?>">
                                                <?php
                                                    for ($i=0; $i <= 20 ; $i++) 
                                                    { 
                                                        $selected = 0;
                                                        
                                                        if(array_key_exists($rate->id, $bookedRatesRecord))
                                                        {
                                                            $flag = 0;
                                                            $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                            $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                        }
                                                        else
                                                        {
                                                            if($has_rate_flag == 0)
                                                            {
                                                               
                                                                $bookedSingleRateRecord =null;
                                                                foreach ($bookedRatesRecord as  $value1) 
                                                                {
                                                                    
                                                                    $bookedSingleRateRecord = $value1;
                                                                    break;
                                                                }
                                                                // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                                if($selected == 2)
                                                                {
                                                                    $flag = 0;
                                                                }
                                                            }
                                                            // if($flag == 1)
                                                            // {
                                                               
                                                            //     $bookedSingleRateRecord =null;
                                                            //     foreach ($bookedRatesRecord as  $value1) 
                                                            //     {
                                                                    
                                                            //         $bookedSingleRateRecord = $value1;
                                                            //         break;
                                                            //     }
                                                            //     // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                            //     $selected = ($i == $bookedSingleRateRecord->no_of_adults)? 2 : 0;
                                                            //     if($selected == 2)
                                                            //     {
                                                            //         $flag = 0;
                                                            //     }
                                                            // }
                                                        }
                                                        
                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                    }
                                                ?>
                                            </select>
                                        </td>

                                        <td style="min-width:50px;">
                                            <select style="width:100%" date="<?php echo $dateKey; ?>" data="<?php echo $item->id.':'.$rate->id?>" class="children-dropdown children-dropdown-<?php echo $item->id?> children-dropdown-<?php echo $item->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id.'-'.$dateKey?> child-dropdown-<?php echo $rate->id?> children-dropdown-<?php echo $item->id.'-'.$key?>">
                                                <?php
                                                    for ($i=0; $i <= 20 ; $i++) 
                                                    {
                                                        $selected = 0;

                                                        if(array_key_exists($rate->id, $bookedRatesRecord))
                                                        {
                                                            $flag = 0;
                                                            $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                            $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                        }
                                                        else
                                                        {
                                                            if($has_rate_flag == 0)
                                                            {
                                                               
                                                                $bookedSingleRateRecord =null;
                                                                foreach ($bookedRatesRecord as  $value1) 
                                                                {
                                                                    
                                                                    $bookedSingleRateRecord = $value1;
                                                                    break;
                                                                }
                                                                // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                                $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                                if($selected == 2)
                                                                {
                                                                    $flag = 0;
                                                                }
                                                            }
                                                            // if($flag == 1)
                                                            // {
                                                               
                                                            //     $bookedSingleRateRecord =null;
                                                            //     foreach ($bookedRatesRecord as  $value1) 
                                                            //     {
                                                                    
                                                            //         $bookedSingleRateRecord = $value1;
                                                            //         break;
                                                            //     }
                                                            //     // $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                                            //     $selected = ($i == $bookedSingleRateRecord->no_of_children)? 2 : 0;
                                                            //     if($selected == 2)
                                                            //     {
                                                            //         $flag = 0;
                                                            //     }
                                                            // }
                                                        }
                                                         
                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    <?php
                                    }

                                $max_extra_beds = $item->max_extra_beds;
                                $bed_type_people = 0;

                                $selected_upsell_extra_bed_quantity = [];
                                $selected_discount_code_extra_bed_quantity = [];

                                if($rate->destination->pricing == 1 && $rate->destination->getDestinationTypeName()=='Accommodation')
                                {
                                    $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $item->id])->all();

                                    if(!empty($itemBedTypesModel))
                                    {
                                        foreach ($itemBedTypesModel as $key => $value) 
                                        {
                                            $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                        }
                                    }
                                }

                                // *********** Discount Codes ********** //

                                $checked_discount_codes = [];
                                $checked_upsell_items = [];

                                if(array_key_exists($rate->id, $bookedRatesRecord))
                                {   
                                    $discount_upsell_flag = 0;
                                    $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                    $get_discount_codes = BookingsDiscountCodes::find()->where(['booking_date_id' => $bookedSingleRateRecord->id])->all();

                                    if(!empty($get_discount_codes))
                                    {
                                        foreach ($get_discount_codes as $key => $value) 
                                        {
                                            $checked_discount_codes[$value['discount_id']] = $value['price'];
                                            $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                        }
                                    }


                                    $get_upsell_items = BookingsUpsellItems::find()->where(['booking_date_id' => $bookedSingleRateRecord->id])->all();

                                    if(!empty($get_upsell_items))
                                    {
                                        foreach ($get_upsell_items as $key => $value) 
                                        {
                                            $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                            $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                        }
                                    }

                                }
                                else
                                {
                                    if($has_rate_flag == 0)
                                    {
                                       
                                        $bookedSingleRateRecord =null;
                                        foreach ($bookedRatesRecord as  $value1) 
                                        {
                                            
                                            $bookedSingleRateRecord = $value1;
                                            break;
                                        }

                                        $get_discount_codes = BookingsDiscountCodes::find()->where(['booking_date_id' => $bookedSingleRateRecord->id])->all();

                                        if(!empty($get_discount_codes))
                                        {
                                            foreach ($get_discount_codes as $key => $value) 
                                            {
                                                $checked_discount_codes[$value['discount_id']] = $value['price'];
                                                $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                            }
                                        }

                                        $get_upsell_items = BookingsUpsellItems::find()->where(['booking_date_id' => $bookedSingleRateRecord->id])->all();

                                        if(!empty($get_upsell_items))
                                        {
                                            foreach ($get_upsell_items as $key => $value) 
                                            {
                                                $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                                $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                            }
                                        }
                                        if(!empty($get_upsell_items) || !empty($get_discount_codes))
                                        {
                                            $discount_upsell_flag = 0;
                                        }
                                    }
                                }

                                $org_booked_item = null;
                                foreach ($bookedRatesRecord as  $value2) 
                                {
                                    
                                    $org_booked_item = $value2;
                                    break;
                                }

                                echo $this->render('discount_codes',
                                [
                                    'dateKey' => $dateKey,
                                    'rates_id' => $rate->id,
                                    'checked_discount_codes' => $checked_discount_codes,
                                    'bed_type_people' => $bed_type_people,
                                    'max_extra_beds' => $max_extra_beds,
                                    'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                    'org_rate_exists' => $org_rate_exists,
                                ]);

                                // *********** Upsell Items ********** //

                                echo $this->render('upsell_items',
                                [
                                    'dateKey' => $dateKey,
                                    'rates_id' => $rate->id,
                                    'org_rate_id' => $org_booked_item->rate_id,
                                    'checked_upsell_items' => $checked_upsell_items,
                                    'max_extra_beds' => $max_extra_beds,
                                    'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                    'org_rate_exists' => $org_rate_exists,
                                ]);

                                    $price_json = '';
                                    $orgTotal = 0;
                                    $icelandTotal = 0;
                                    $tooltips_title = 'Item Price';
                                    if(array_key_exists($rate->id, $bookedRatesRecord))
                                    {
                                        $json_price_flag = 0;
                                        $bookedSingleRateRecord = $bookedRatesRecord[$rate->id];
                                    }
                                    else
                                    {
                                        $bookedSingleRateRecord =null;
                                        if ($json_price_flag == 1) 
                                        {
                                           
                                            foreach ($bookedRatesRecord as  $value1) 
                                            {
                                                
                                                $bookedSingleRateRecord = $value1;
                                                break;
                                            }
                                            if(!empty($bookedSingleRateRecord->total))
                                            {
                                                $json_price_flag = 0;
                                            }
                                        }
                                    }   
                                    if(!empty($bookedSingleRateRecord))
                                    {
                                        if(!empty($bookedSingleRateRecord->total))
                                        {
                                            $orgTotal = $bookedSingleRateRecord->total;
                                            $icelandTotal = Yii::$app->formatter->asDecimal($orgTotal, "ISK");
                                        }

                                        if(!empty($bookedSingleRateRecord->price_json))
                                        {
                                            $arr = json_decode($bookedSingleRateRecord->price_json);

                                            $tooltips_title = $tooltips_title . 'Quantity = '. $arr->quantity .' , ';
                                            $tooltips_title = $tooltips_title . 'Total Nights = '. $arr->total_nights .' , ';
                                            $tooltips_title = $tooltips_title . 'Rate = '. $arr->rate .' , ';
                                            $tooltips_title = $tooltips_title . 'VAT % = '. $arr->vat .' , ';
                                            $tooltips_title = $tooltips_title . 'x = '. $arr->x .' , ';
                                            $tooltips_title = $tooltips_title . 'lodgingTax = '. $arr->lodgingTax .' , ';
                                            $tooltips_title = $tooltips_title . 'y = '. $arr->y .' , ';
                                            $tooltips_title = $tooltips_title . 'voucher_discount = '. $arr->voucher_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'travel_partner_discount = '. $arr->travel_partner_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'offer_discount = '. $arr->offer_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'amount_of_discount = '. $arr->amount_of_discount .' , ';
                                            $tooltips_title = $tooltips_title . 'sub_total_1 = '. $arr->sub_total_1 .' , ';
                                            $tooltips_title = $tooltips_title . 'sub_total_2 = '. $arr->sub_total_2 .' , ';
                                            $tooltips_title = $tooltips_title . 'vat_amount = '. $arr->vat_amount .' , ';
                                            $tooltips_title = $tooltips_title . 'Total Price = '. $arr->icelandPrice;

                                            $price_json = str_replace('"', "'", $bookedSingleRateRecord->price_json);
                                        }
                                        else
                                        {
                                            $tooltips_title = 'Item Price';
                                            $price_json = '';
                                        }

                                    } 
                                    

                                    
                                    
                                    

                                ?>

                                <td>
                                    <input class="item-total-input item-total-input-<?php echo $rate->id.'-'.$dateKey?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                    <input type="text" value="<?php echo $price_json; ?>" class="price-json-<?php echo $rate->id.'-'.$dateKey?>" hidden>

                                    <label>
                                        <a href="javascript:;" data-placement="left" class="item-total-<?php echo $rate->id.'-'.$dateKey?> total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                        </a>
                                        <span><?php echo $icelandTotal?></span>
                                    </label>
                                    <lable class="item-total-nights-price-<?php echo $rate->id.'-'.$dateKey?>"></label>
                                </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    <?php
                    }
                    ?>
                    </td>
                </tr>
                </tbody>
            </table>
        <?php
        }
    }
}
else
{
    echo '<div class="alert-danger alert fade in">No Rate found.</div>';
}
?>



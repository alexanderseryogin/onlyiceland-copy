<div class="tiles">
	<!--Daily Occupancy Report -->
			<div class="tile bg-red-intense" id="gross_revenue_summary" data-toggle="tooltip" title="Gross Revenue Summary (Based on dates)" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Gross Revenue Summary </div>
			        <div class="number"></div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile bg-green" id="gross_revenue_summary-1" data-toggle="tooltip" title="Gross Revenue Summary (Based on departure dates)" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Gross Revenue Summary</div>
			        <div class="number"> <i  class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile  bg-blue-hoki" id="gross_revenue_summary-2" data-toggle="tooltip" title="Gross Revenue Summary (Based on arrival dates)" data-placement="top" >
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Gross Revenue Summary </div>
			        <div class="number"> <i class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile bg-blue" id="gross_credit_summary-1" data-toggle="tooltip" title="Gross Credit Summary (Based on departure dates)" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Gross Credit Summary</div>
			        <div class="number"> <i  class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile bg-purple-studio" id="gross_credit_summary-2" data-toggle="tooltip" title="Gross Credit Summary (Based on arrival dates)" data-placement="top" >
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Gross Credit Summary </div>
			        <div class="number"> <i class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile bg-green-meadow" id="gross_credit_summary" data-toggle="tooltip" title="Gross Credit Summary (Based on dates)" data-placement="top" >
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Gross Credit Summary </div>
			        <div class="number"> <i class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile bg-yellow-saffron" id="net_revenue_summary-1" data-toggle="tooltip" title="Net Revenue Summary (Based on departure dates)" data-placement="top" >
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Net Revenue Summary </div>
			        <div class="number"> <i class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!--Daily Occupancy Report -->
			<div class="tile bg-green-meadow" id="net_revenue_summary-2" data-toggle="tooltip" title="Net Revenue Summary (Based on arrival dates)" data-placement="top" >
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Net Revenue Summary </div>
			        <div class="number"> <i class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
			<div class="tile bg-red" id="net_revenue_summary-3" data-toggle="tooltip" title="Net Revenue Summary (Based on dates)" data-placement="top" >
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Net Revenue Summary </div>
			        <div class="number"> <i class="fa fa-info-circle" aria-hidden="true"></i> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
</div>
<?php

use yii\widgets\LinkPager;
use common\models\BookableItemsImages;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationTypes;

$this->title = $town_name.' Destinations';
$path = '//placehold.it/1200x260&amp;text=IMAGE+PLACEHOLDER';
?>

<!-- <div class="site-showcase">
    <div class="parallax page-header" style="background-image:url(<?=$path?>); height: 450px;">
    </div>
</div> -->

  <!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">

              	<!-- ************ Show region  ************ -->

                <div class="col-sm-10 col-sm-offset-1">

                    <div class="spacer-20"></div>

                    <!-- Start Related Properties -->
                    <div class="block-heading">
                        <h4><span class="heading-icon"><i class="fa fa-th-list"></i></span>Destinations listed in <?=$town_name?></h4>
                        <div class="toggle-view pull-right">
                         <?php
                          // check call for list view or grid view

                          if(isset($gridview) && $gridview==1)
                          {
                            echo '<a class="active" id="grid_view"><i class="fa fa-th-large"></i></a>
                                    <a id="list_view"><i class="fa fa-th-list"></i></a>';
                          }
                          else
                          {
                            echo '<a id="grid_view"><i class="fa fa-th-large"></i></a>
                                    <a id="list_view" class="active"><i class="fa fa-th-list"></i></a>';
                          }
                         ?>
                        </div>
                    </div>

                    <?php 
                      if(empty($models))
                      { 
                        echo'<h4>No Record Found..... </h4>';
                      }
                    ?>

                    <!-- ************ Grid view  ************ -->

                    <?php if(isset($gridview) && $gridview==1): ?>

                      <?=
                        $this->render('gridview',[
                             'models' => $models, // model of provider
                          ]);
                      ?>

                    <?php else: ?>
                            
                        <!-- ************ List view  ************ -->    
                      <?=
                        $this->render('listview',[
                            'models' => $models,
                        ]);
                      ?>

                    <?php endif ?>

                      <?= // display pagination
                        LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                      ?>

                  </div>

            </div>
        </div>
    </div>
</div>

<?php

$url =  Url::to(['/Vesturland/'],true);
$updateSessionUrl =  Url::to(['/search/update-session'],true);

$this->registerJs("
jQuery(document).ready(function() 
{
  $('#list_view').click(function()
  {
    var Object = {
                    view: 0,
                }
    $.ajax(
    {
        type: 'POST',
        url: '$updateSessionUrl',
        data: Object,
        success: function(result)
        {
          location.replace('$url/$town_name');
        },
    });
  });

  $('#grid_view').click(function()
  {
    var Object = {
                    view: 1,
                }
    $.ajax(
    {
        type: 'POST',
        url: '$updateSessionUrl',
        data: Object,
        success: function(result)
        {
          location.replace('$url/$town_name');
        },
    });

  });

});",View::POS_END);
?>
<?php

use yii\db\Migration;

class m161101_103620_create_table_rates_dates extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `rates_dates`( `id` INT(11) NOT NULL AUTO_INCREMENT, `rate_id` INT(11), `night_rate` DATE NOT NULL, `over_ride` TINYINT(1) DEFAULT 0, PRIMARY KEY (`id`), CONSTRAINT `FK_rates_dates_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rates`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161101_103620_create_table_rates_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

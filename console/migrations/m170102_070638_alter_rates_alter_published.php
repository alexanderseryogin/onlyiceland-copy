<?php

use yii\db\Migration;

class m170102_070638_alter_rates_alter_published extends Migration
{
    public function up()
    {
        $this->execute(" ALTER TABLE `rates` CHANGE `published` `published` TINYINT(4) NULL; ");
    }

    public function down()
    {
        echo "m170102_070638_alter_rates_alter_published cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

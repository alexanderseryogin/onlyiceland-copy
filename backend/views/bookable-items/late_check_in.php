<?php
use yii\helpers\Url;
use common\models\BedTypes;
use yii\helpers\ArrayHelper;
use common\models\BedsCombinations;
use yii\widgets\ActiveForm;
    $SelectOption = [
        0 => '',
        1 => 'selected',
    ];

    // $bedTypes = BedTypes::find()->all();
    // $bedsCombinationsModel = BedsCombinations::find()->all();
?>

<div class="tab-pane" id="late-check-in">
<?= $form->field($model, 'bookable_item_default_notes',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            'id' => 'default_notes',
                                                            ]
                                                        ])->textarea(['rows' => 10]) ?>
<?= $form->field($model, 'bookable_item_notes',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            'id' => 'notes',
                                                            ]
                                                        ])->textarea(['rows' => 10]) ?>
    <!-- <div class="panel-group accordion" id="accordion_details">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_details" href="#collapse_bed_type">Bed Type</a>
                </h4>
            </div>
            <div id="collapse_bed_type" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">
                </div>
            </div>
        </div>
        <br>

    </div> -->

    <div class="form-group">
         <?php
            if(!$model->isNewRecord)
            { ?>
                <button type="button" class="btn btn-primary common-save-btn" >Update</button>
        <?php     }
        ?>
        <a class="<?= $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'?> common-submit-button" ><?=$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save') ?></a>
        <a class="btn btn-default" href="<?= Url::to(['/bookable-items']) ?>" >Cancel</a>
    </div>
</div>
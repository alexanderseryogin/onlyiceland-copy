<?php

use yii\db\Migration;

class m161010_170238_alter_booking_rules_change_datetime extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `booking_rules` CHANGE `ebp_first_night_exception_period` `ebp_first_night_exception_period` INT(11) NULL, CHANGE `ebp_last_night_exception_period` `ebp_last_night_exception_period` INT(11) NULL, CHANGE `same_day_booking_cutoff` `same_day_booking_cutoff` VARCHAR(256) NULL; 
            ");
    }

    public function down()
    {
        echo "m161010_170238_alter_booking_rules_change_datetime cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

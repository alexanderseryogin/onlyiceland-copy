<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
Use common\models\Destinations;
use common\models\BookingConfirmationTypes;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\BookableItems;
/* @var $this yii\web\View */
/* @var $model common\models\Offers */
/* @var $form yii\widgets\ActiveForm */

if(!empty($tab_href))
{
    $this->registerJs("
        $('.offers_tabs li').each(function()
        {
            var href_val = $(this).find('a').attr('href');
            var tab_activate = '$tab_href';
            $('#tab_href').val(tab_activate);

            if(href_val==tab_activate)
            {
                $(this).addClass('active');
                $(this).find('a').attr('aria-expanded','true');
                $(tab_activate).addClass('active');
            }
            else
            {
                $(this).removeClass('active');
                $(this).find('a').attr('aria-expanded','false');
                $(href_val).removeClass('active');
            }
        });
    ");
}
?>

<div class="offers-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Offers</span>
                        </div>
                        <ul class="nav nav-tabs offers_tabs">
                            <li class="active">
                                <a href="#info" data-toggle="tab">Offer Information</a>
                            </li>
                                      
                            <li>
                                <a href="#rules" data-toggle="tab">Rules</a>
                            </li>

                        </ul>
                    </div>

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                        'options' => ['enctype' => 'multipart/form-data'],
                    ]); ?>

                    <?= $form->errorSummary($model); ?>
                    <input type="text" name="Offers[tab_href]" id="tab_href" hidden="">

                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="info">
                                
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'provider_id', [
                                            'inputOptions' => [
                                                'id' => 'provider-dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                        ])->dropdownList(ArrayHelper::map(Destinations::find()->all(),'id','name'),['prompt'=>'Select a Destination',
                                            'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/listitems?provider_id=').'"+$(this).val(), function( data ) 
                                                {
                                                    $("#item_dropdown").html( data );
                                                });'
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'item_id', [
                                            'inputOptions' => [
                                                'id' => 'item_dropdown',
                                                'class' => 'form-control',
                                                'style' => 'max-width: 500px'
                                                ]
                                        ])->dropdownList(ArrayHelper::map(BookableItems::find()->where(['id'=>$model->item_id])->all(),'id','itemType.name'),[]) ?>
                                    </div>
                                </div>

                                <?= $form->field($model, 'summary',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            'id' => 'summary',
                                                            ]
                                                        ])->textarea(['rows' => 10]) ?>

                                <?= $form->field($model, 'additional_details',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            'id' => 'details',
                                                            ]
                                                        ])->textarea(['rows' => 10]) ?>
                            </div>

                            <?php
                                if(!empty($model->allow_cancellations))
                                {
                                    $this->registerJs("
                                        $('#allow_cancellations').val('$model->allow_cancellations').change();
                                    ");
                                }
                                if(!empty($model->show))
                                {
                                    $this->registerJs("
                                        $('#show').val('$model->show').change();
                                    ");
                                }
                            ?>

                            <div class="tab-pane" id="rules">
                                <div class="row">

                                    <div class="col-xs-6">
                                        <label class="control-label" >General Booking Cancellation</label>
                                        <div class="form-group" >
                                            <select style="width: 100%;" class="form-control" id="allow_cancellations" name="Offers[allow_cancellations]" >
                                                <option value="always">Always</option>
                                                <option value="1">1 day before check in</option>
                                                <?php
                                                    for ($i=2; $i <= 29 ; $i++) 
                                                    { 
                                                        echo '<option value="'.$i.'">'.$i.' days before check in</option>';
                                                    }
                                                    foreach (Destinations::$generalBookingCancellation as $key => $value) 
                                                    {
                                                        echo '<option value="'.$value.'">'.$value.'</option>';
                                                    }
                                                ?>
                                                <option value="never">Never</option>
                                            </select>
                                        </div>
                                        
                                    </div>
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'booking_confirmation_type_id', [
                                            'inputOptions' => [
                                                'id' => 'booking_confirmation_type_dropdown',
                                                'class' => 'form-control',
                                                'style' => 'width: 470px'
                                                ]
                                            ])->dropdownList(ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name'),['prompt'=>'Select a Confirmation Type']) ?>
                                    </div>
                                
                                </div>

                                <label class=" control-label">Minimum Stay</label>
                                <div class="form-group">
                                    <input id="min_stay" value="<?=$model->min_stay ?>" type="text" name="Offers[min_stay]" />
                                </div>

                                <div class="row">

                                    <div class="col-xs-6">
                                        <label class="control-label" >Show</label>
                                        <div class="form-group" >
                                            <select style="width: 470px;" value="<?=$model->show ?>" class="form-control" id="show" name="Offers[show]" >

                                                <option id="0_options" value="0">Never</option>
                                                <option id="1_options" value="1">Always</option>
                                                <option id="2_options" value="2">Internal Only</option>
                                                <option id="3_options" value="3">Only if Available</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'commission')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>
                            
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                <a class="btn btn-default" href="<?= Url::to(['/offers']) ?>" >Cancel</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#min_stay').ionRangeSlider({
        grid:!0,
        min: 0,
        max: 60,
    });

    $('#summary').summernote({height:300});

    $('#details').summernote({height:300});

    $('#provider-dropdown').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });

    $('#item_dropdown').select2({
        placeholder: \"Select an Item\",
        allowClear: true
    });

    $('#booking_confirmation_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#allow_cancellations').select2({
        allowClear: true
    });

    $('#show').select2({
        allowClear: true
    });

    $('.offers_tabs li').click(function()
    {
        $('#tab_href').val($(this).find('a:first').attr('href'));
    });

});",View::POS_END);
?>

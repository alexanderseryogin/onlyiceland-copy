
<?php
	use common\models\Rates;
	
	if(!empty($item_id))
	{
		$rates = Rates::find()
                ->where(['unit_id' => $item_id])
                ->all();

        echo "<option value=''></option>";
        
		if(!empty($rates))
		{
			foreach ($rates as $key =>  $rate) 
		    {
		        echo "<option value='".$rate->id."'>".$rate->name."</option>";  
		    }
		}  
	}

?>


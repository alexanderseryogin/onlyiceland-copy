<?php

use yii\db\Migration;

class m170221_135430_alter_bookable_items_add_bathroom extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `bathroom` TINYINT(1) NULL AFTER `booking_type_id`; 
        ");
    }

    public function down()
    {
        echo "m170221_135430_alter_bookable_items_add_bathroom cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

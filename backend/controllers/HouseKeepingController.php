<?php

namespace backend\controllers;
use Yii;
use backend\components\BaseController;
use common\models\BookingsItems;
use common\models\BookingsItemsSearch;
use common\models\BookingStatuses;
use common\models\BookingDates;
use yii\helpers\ArrayHelper;
use common\models\BookingHousekeepingStatus;

class HouseKeepingController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetItems()
    {
    	$date = $_POST['date'];
        $provider_id = $_POST['dest_id'];
    	$date = date('Y-m-d',strtotime($date));
    	// echo "<pre>";
    	// print_r();
    	// exit();

    	$items = BookingsItems::find()->where(['arrival_date' => $date , 'provider_id' => $provider_id,'temp_flag' => 0, 'deleted_at' => 0])->all();
        $older_items = BookingsItems::find()->where(['provider_id' => $provider_id])
                                            ->andwhere(['<','arrival_date' , $date ])
                                            ->all();
        $older_items_arr = array();
        foreach ($older_items as $older_item) 
        {
            foreach ($older_item->bookingDatesSortedAndNotNull as $bdate) 
            {
                if($bdate->date == $date)
                {
                    array_push($older_items_arr, $older_item);
                }
            }
        }

    	echo json_encode([
                    'report_items' => $this->renderAjax('get_report_items', [
                                            'items' => $items,
                                            'older_items' => $older_items_arr
                                        ]),

                ]);
    }
    public function actionDetails()
    {
        $searchModel = new BookingsItemsSearch();
        $booking_status = BookingStatuses::find()->all();

        $filter_flag = 0;
        $group_flag = 0;
        $selected_status = ArrayHelper::map($booking_status,'id','id');

        $session = Yii::$app->session;
        $session->open();

        if(Yii::$app->request->post())
        {
            // echo "<pre>";
            // print_r("in post");
            // exit();
            $bookingDates = BookingDates::find()->orderBy('date DESC')->one();
            if(!empty($bookingDates))
            {
                $last_booking_date = date('Y-m-d',strtotime($bookingDates->date));
            }
            else
            {
                $last_booking_date = date('Y-m-d');
            }

            $start_booking_date = date('Y-m-d',strtotime(Yii::$app->request->post('start_date'))); 

            $params = Yii::$app->request->post();
            $params['Bookings'] = isset($params['Bookings'])?$params['Bookings']:'';

            $start_date = $start_booking_date;
            $end_date = $last_booking_date;

            if($params['Bookings']['daterangepicker'] == 1)
            {   
                // echo "<pre>";
                // print_r(Yii::$app->request->post());
                // exit();

                

                
                $dataProvider = $searchModel->searchDateRangeBookings_hk($start_date,$end_date);
                $session[Yii::$app->controller->id.'-grid-start-date'] = Yii::$app->request->post('start_date');
            }
            else
            {
                // echo "<pre>";
                // print_r(Yii::$app->request->post());
                // exit();
                $filter_flag = 1;
                $dataProvider = $searchModel->searchBookings_hk($params['Bookings'],$start_date,$end_date);
                $selected_status = [];

                $selected_status = !empty($params['Bookings']) && isset($params['Bookings']['booking_status'])?$params['Bookings']['booking_status']:'';

                $group_flag = isset($params['Bookings']['group_switch'])?1:0;

                $session[Yii::$app->controller->id.'-grid-booking-status'] = $selected_status;
                $session[Yii::$app->controller->id.'-grid-group-flag'] = $group_flag;
            }
        }
        else
        {
            // echo "<pre>";
            // print_r("not in post");
            // exit();
            $params = Yii::$app->request->queryParams;

            $session = Yii::$app->session;
            $session->open();

            if(!empty($params))
            {
                // echo "<pre>";
                // print_r("in if");
                // exit();
                if(isset($params['BookingsItemsSearch']) && !$this->isArrayEmpty($params['BookingsItemsSearch']) && !isset($params['page']))
                {
                    $params['page'] = 1;
                }
                $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
            }
            else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
            {
                // echo "<pre>";
                // print_r("in else if");
                // exit();
                $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
            }
            else
            {
                // echo "<pre>";
                // print_r($params);
                // exit();
            }

            $dataProvider = $searchModel->search_hk($params);
        }

        // ********** if status and group flag is in session then get them ************ //

        if(isset($session[Yii::$app->controller->id.'-grid-booking-status']) && !empty($session[Yii::$app->controller->id.'-grid-booking-status']))
        {
            $selected_status = $session[Yii::$app->controller->id.'-grid-booking-status'];
        }

        if(isset($session[Yii::$app->controller->id.'-grid-group-flag']))
        {
            $group_flag = $session[Yii::$app->controller->id.'-grid-group-flag'];
        }

        //$dataProvider->pagination->pageSize=10;

        return $this->render('details', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'booking_status' => $booking_status,
            'filter_flag' => $filter_flag,
            'group_flag' => $group_flag,
            'selected_status' => $selected_status,
        ]);
    }

    public function actionChangeStatus()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $booking_house_keping_status = BookingHousekeepingStatus::findOne(['booking_item_id' => $_POST['booking_item_id'], 'housekeeping_status_id' => $_POST['housekeeping_status_id']]);
        if(empty($booking_house_keping_status))
        {
            if($_POST['housekeeping_status_id'] == 1)
            {
                BookingHousekeepingStatus::deleteAll(['booking_item_id' => $_POST['booking_item_id']]);
                $new_housekeeping_status = new BookingHousekeepingStatus();
                $new_housekeeping_status->booking_item_id = $_POST['booking_item_id'];
                $new_housekeeping_status->housekeeping_status_id = $_POST['housekeeping_status_id'];

                if(!$new_housekeeping_status->save())
                {
                    echo "<pre>";
                    print_r($new_housekeeping_status->errors);
                    exit();
                }
            }
            else
            {
                $booking_house_keping_status2 = BookingHousekeepingStatus::findOne(['booking_item_id' => $_POST['booking_item_id'], 'housekeeping_status_id' => 1]);
                if(empty($booking_house_keping_status2))
                {
                    $new_housekeeping_status = new BookingHousekeepingStatus();
                    $new_housekeeping_status = new BookingHousekeepingStatus();
                    $new_housekeeping_status->booking_item_id = $_POST['booking_item_id'];
                    $new_housekeeping_status->housekeeping_status_id = $_POST['housekeeping_status_id'];

                    if(!$new_housekeeping_status->save())
                    {
                        echo "<pre>";
                        print_r($new_housekeeping_status->errors);
                        exit();
                    }
                }
                else
                {
                    $booking_house_keping_status2->delete();
                    
                    $new_housekeeping_status = new BookingHousekeepingStatus();
                    $new_housekeeping_status = new BookingHousekeepingStatus();
                    $new_housekeeping_status->booking_item_id = $_POST['booking_item_id'];
                    $new_housekeeping_status->housekeeping_status_id = $_POST['housekeeping_status_id'];

                    if(!$new_housekeeping_status->save())
                    {
                        echo "<pre>";
                        print_r($new_housekeeping_status->errors);
                        exit();
                    }
                }
            }

            
        }
        else
        {
            $booking_house_keping_status->delete();
        }
        // $booking_item = BookingsItems::findOne(['id' => $_POST['booking_item_id'] ]);
        // $booking_item->housekeeping_status_id = $_POST['housekeeping_status_id'];
        // $booking_item->save(false);

        // foreach ($booking_item->bookingDatesSortedAndNotNull as $date) 
        // {
        //     $date->housekeeping_status_id = $_POST['housekeeping_status_id'];
        // }
        return true;
    }

    public function actionUnsetDateRangeSession()
    {
        $session = Yii::$app->session;
        $session->open();
        unset($session[Yii::$app->controller->id.'-grid-start-date']);
        unset($session[Yii::$app->controller->id.'-grid-end-date']);
        
        return $this->redirect(['details']);
    }

}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_multi_night_discount".
 *
 * @property string $id
 * @property integer $rates_id
 * @property integer $nights
 * @property integer $percent
 * @property integer $per_night
 * @property integer $once_off
 * @property integer $price_cap
 *
 * @property Rates $rates
 */
class RatesMultiNightDiscount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_multi_night_discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rates_id', 'nights', 'percent', 'per_night', 'once_off', 'price_cap'], 'integer'],
            [['rates_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rates_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rates_id' => Yii::t('app', 'Rates ID'),
            'nights' => Yii::t('app', 'Nights'),
            'percent' => Yii::t('app', 'Percent'),
            'per_night' => Yii::t('app', 'Per Night'),
            'once_off' => Yii::t('app', 'Once Off'),
            'price_cap' => Yii::t('app', 'Price Cap'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rates_id']);
    }
}

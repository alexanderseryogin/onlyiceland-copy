<?php
	use common\models\DestinationTravelPartners;
	
	$travel_partners = DestinationTravelPartners::find()
            ->where(['destination_id' => $provider_id])
            ->all();
    
	if(!empty($travel_partners))
	{
		echo "<option value=''></option>";
		foreach ($travel_partners as $key =>  $travel_partner) 
	    {
	        echo "<option value='".$travel_partner->id."'>".$travel_partner->travelPartner->company_name." - ".$travel_partner->comission."%</option>";  
	    }
	}
	else
		echo '';  

?>


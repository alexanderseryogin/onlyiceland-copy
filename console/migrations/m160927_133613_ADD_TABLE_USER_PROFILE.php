<?php

use yii\db\Migration;

class m160927_133613_ADD_TABLE_USER_PROFILE extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `user_profile`( `id` INT NOT NULL AUTO_INCREMENT, `user_id` INT(11) NOT NULL, `title` TINYINT DEFAULT 0, `first_name` VARCHAR(256), `last_name` VARCHAR(256), `company_name` VARCHAR(256), `address` TEXT, `postal_code` VARCHAR(128), `city_id` INT(11), `state_id` INT(11), `country_id` INT(11), `telephone_number` VARCHAR(128), PRIMARY KEY (`id`), CONSTRAINT `FK_user_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_user_profile_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_user_profile_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_user_profile_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;");
    }

    public function down()
    {
        echo "m160927_133613_ADD_TABLE_USER_PROFILE cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

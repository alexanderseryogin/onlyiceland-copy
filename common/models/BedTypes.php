<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bed_types".
 *
 * @property string $id
 * @property string $name
 * @property string $max_sleeping_capacity
 * @property string $intended_sleeping_capacity
 *
 * @property BookableItems[] $bookableItems
 */
class BedTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bed_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['max_sleeping_capacity', 'intended_sleeping_capacity'], 'integer', 'min' => 0],
            [['name','max_sleeping_capacity', 'intended_sleeping_capacity'], 'required'],
            [['name'], 'string', 'max' => 256],
            ['name','unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'max_sleeping_capacity' => Yii::t('app', 'Maximum Sleeping Capacity'),
            'intended_sleeping_capacity' => Yii::t('app', 'Intended Sleeping Capacity'),
            'combineable' => Yii::t('app', 'Combinable'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItems()
    {
        return $this->hasMany(BookableItems::className(), ['sleeping_arrangement_id' => 'id']);
    }
}

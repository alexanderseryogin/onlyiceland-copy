
<?php
    use yii\web\View;
    use yii\helpers\Url;
?>

<style type="text/css">
    .align-center {
        text-align: center;
    }
    table tr th {
        text-align: center;
    }
    .free{background: #DFF0D8;}
    .extra{background: #FCF8E3}
    .not_available{background: #FFEB89}
    .not_display{background: #F2DEDE}
    .table-scrollable {border: none !important; overflow-x: hidden !important;}
    .dataTables_scrollBody {overflow-x: hidden !important;}
    table.dataTable thead .sorting_desc {
        background-image: none;
    }
    table.dataTable thead .sorting_asc {
        background-image: none;
    }
</style>

<!-- BEGIN ACCORDION PORTLET-->
<div class="tab-pane" id="upsell">

	<div id="upsell_items">
        <?php if(!empty($model->upsell_pricing) || !empty($model->provider_id) || !$model->isNewRecord): ?>
        <?=  
            $this->render('dynamic_upsell_items',
            [
                'model'         => $model,
                'upsell_pricing' => $model->upsell_pricing,
                'provider_id' => $model->provider_id,
            ]);
        ?>
        <?php else: ?>
        <span class="label label-danger"> Note! </span>
        <span>&nbsp;  Please First select "Destination" from dropdown to add Upsell Items. </span> 
        <?php endif ?>
    </div>
    <br>
</div>

<?php
$this->registerJs("

function initDataTableUpsellItems(tableSelector)
{
    return $(tableSelector).DataTable({
            language: {
                aria: {
                    sortAscending: \": activate to sort column ascending\",
                    sortDescending: \": activate to sort column descending\"
                },
                emptyTable: \"No data available in table\",
                info: \"Showing _TOTAL_ upsell items\",
                infoEmpty: \"No upsell items found\",
                infoFiltered: \"(filtered 1 from _MAX_ total upsell items)\",
                lengthMenu: \"_MENU_ upsell items\",
                search: \"Search:\",
                zeroRecords: \"No matching records found\"
            },
            scrollY: 300,
            scrollCollapse: true,
            //scroller: !0,
            statesave : false,
            columns: [
                { 'width': '5%' },
                { 'width': '30%' },
                { 'width': '30%' },
                { 'width': '30%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
                { 'width': '10%' },
            ],
            order: [
                [0, \"asc\"]
            ],
            \"columnDefs\": [
                { \"orderable\": false, \"targets\": [2] }
            ],
            lengthMenu: [
                [10, 15, 20, -1],
                [10, 15, 20, \"All\"]
            ],
            paging: false,
            retrieve: true
        }); 
}

function redrawDataTableUpsellItems(tableSelector)
{
    var table = initDataTableUpsellItems(tableSelector);
    table.draw();
}

function redrawUpsellItemsDataTable()
{
    redrawDataTableUpsellItems('#rates_upsell_items');
}

jQuery(document).ready(function() 
{
    initDataTableUpsellItems('#rates_upsell_items');
    setTimeout(function()
    { 
        redrawUpsellItemsDataTable(); 
    }, 500);
    
});",View::POS_END);
?>
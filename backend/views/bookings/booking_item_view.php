<?php
	
use common\models\BookingsItems;
use common\models\BookingsItemsCart;
use common\models\BookableBedsCombinations;
use common\models\RatesCapacityPricing;
use common\models\Rates;
use common\models\BookableBedTypes;
use common\models\BookingsDiscountCodes;
use common\models\BookingsDiscountCodesCart;
use common\models\BookingsUpsellItems;
use common\models\BookingsUpsellItemsCart;
use common\models\User;
use common\models\BookingConfirmationTypes;
use common\models\BookingTypes;
use common\models\Destinations;
use common\models\BookingStatuses;
use yii\helpers\ArrayHelper;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\EstimatedArrivalTimes;
use common\models\BookingSpecialRequests;
use common\models\Countries;
use common\models\DestinationTravelPartners;
use common\models\RatesOverride;

$bookingItemsModel = BookingsItemsCart::findOne(['id' => $id]);
$bookingDates = $bookingItemsModel->bookingDatesSortedAndNotNull;

$date1=date_create($bookingItemsModel->departure_date);
$date2=date_create($bookingItemsModel->arrival_date);
$diff=date_diff($date1,$date2);
$difference =  $diff->format("%a");

$availableItemNames = BookingsItemsCart::checkAvailableItemNames($bookingItemsModel->item_id,$bookingItemsModel->arrival_date,$difference);

$item_name = !empty($bookingItemsModel->item_name_id)?$bookingItemsModel->itemName->item_name:'- - - - -';

$Options = [
    0 => '',
    1 => 'checked',
    2 => 'selected',
    3 => 'disabled',
];

?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading" style="background: <?php echo $bookingItemsModel->item->background_color?>; color: <?php echo $bookingItemsModel->item->text_color?>">
                <h4 class="panel-title">
                    <div class="accordion-toggle" data-parent="#accordion_item_rates">
                        <label><?php  echo $bookingItemsModel->provider->name.' - '.date('d/m/Y',strtotime($bookingItemsModel->arrival_date)).'<span class="fa fa-arrow-right"></span>'.date('d/m/Y',strtotime($bookingItemsModel->departure_date)).' - '.$bookingItemsModel->item->itemType->name.' - '.$item_name; ?></label>
                    </div>
                </h4>
            </div>
            <div class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">       
                    <?php
                        foreach ($bookingDates as $key => $model) 
                        {
                        ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <strong>
                                            <th width="13%">Date</th>
                                            <th width="20%">Rate</th>
                                            
                                            <?php
                                                if($model->rate->destination->pricing == 1 && $model->rate->destination->getDestinationTypeName()=='Accommodation') 
                                                {  
                                                    echo '<th width="15%">Capacity Pricing </th>';
                                                }
                                                else if($model->rate->destination->pricing == 0)
                                                {
                                                    echo '<th width="10%">Adults</th><th width="10%">Children</th>';
                                                }
                                            ?>
                                            <?php if($model->rate->destination->pricing == 1 && $model->rate->destination->getDestinationTypeName()=='Accommodation') 
                                                    { ?>
                                                    <th width="22%">Adults/Children</th>
                                                    <?php } ?>
                                            <th width="22%">Available Discounts</th>
                                            <th width="30%">Upsell Items</th>
                                            <th width="8%">Total </th>
                                        </strong>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> 
                                        <?php echo date('d/m/Y',strtotime($model->date)); ?>
                                        <br><br>

                                        <input type="hidden" date="<?php echo $model->date; ?>" data="<?php echo $model->rate_id?>" class="form-control modal-item-quantity modal-item-quantity-<?php echo $model->rate_id.'-'.$model->date?>" value="1">

                                    </td>
                                    <td>
                                        <input class="modal-pricing-type" data="<?php echo $model->item_id;?>" type="text" value="<?php echo $model->rate->destination->pricing?>" hidden>

                                        <?php echo $model->rate->name.'<br><br>'?>
                                        
                                        <button date="<?php echo $model->date;?>" data="<?php echo $model->rate_id?>" type="button" class="btn btn-xs btn-default modal-view-details"><i class="fa fa-chevron-down" aria-hidden="true"></i> <span>View Details</span> </button>

                                        <input type="text" value="<?php echo $model->item_id.':'.$model->rate_id.':'.$model->date.':'.$model->id?>" class="modal-helper-field" hidden>
                                    </td>

                                    <?php

                                    // ************* capacity pricing ************ //

                                    if($model->bookingItem->provider->pricing == 1 && $model->bookingItem->provider->getDestinationTypeName()=='Accommodation') 
                                    {
                                        echo '<td>';

                                        $rates_capacity_pricing = RatesCapacityPricing::find()->where(['rates_id' => $model->rate_id])->all();
                                        $rate_override = RatesOverride::findOne(['date' => $model->date, 'rate_id' => $model->rate_id]);
                                        if(!empty($rates_capacity_pricing))
                                        {
                                            foreach ($rates_capacity_pricing as $key => $value)
                                            {
                                                if($value->person_no >=1 && $value->person_no<=4)
                                                {
                                                    $word =  Rates::$numberWords[$value->person_no-1];

                                                    $checked = ($value->person_no == $model->person_no)? 1 : 0;

                                                    $price = $value->price;
                                                    $custom_rate = '';

                                                    if($checked == 1 )
                                                    {
                                                        if($model->custom_rate != null)
                                                        {
                                                            $custom_rate = Yii::$app->formatter->asDecimal( $model->custom_rate, "ISK");
                                                        }
                                                        else
                                                        {
                                                            $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if(!empty($rate_override))
                                                        {
                                                            if($key==0)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->single, "ISK");
                                                            }
                                                            if($key==1)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->double, "ISK");
                                                            }
                                                            if($key==2)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->triple, "ISK");
                                                            }
                                                            if($key==3)
                                                            {
                                                                $value->price = Yii::$app->formatter->asDecimal( $rate_override->quad, "ISK");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                        }
                                                        // $value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");
                                                    }
                                                    //$value->price = Yii::$app->formatter->asDecimal( $value->price, "ISK");

                                                    $icon_user = '';

                                                    for ($i=1; $i <= $value->person_no ; $i++) 
                                                    { 
                                                        $icon_user = $icon_user . '<i class="fa fa-user" aria-hidden="true"></i>';
                                                    }
                                                    ?>

                                                    <label>
                                                        <input type="checkbox" date="<?php echo $model->date?>" data="<?php echo $value->person_no?>" id="<?php echo $model->item_id.':'.$model->rate_id.':'.$value->person_no?>" class="modal-capacity-pricing modal-capacity-pricing-<?php echo $model->rate_id.'-'.$model->date?>" <?php echo $Options[$checked]?>> <?php echo $icon_user?><br>
                                                        <input class="modal-custom_rate" type="text" name="custom_rate" id="<?php echo 'custom-rate:'.$model->item_id.':'.$model->rate_id.':'.$value->person_no?>" value="<?=($checked == 1 && $model->custom_rate != null )?$custom_rate:$value->price?>" date="<?php echo $model->date?>" style="text-align:right;width: 50%;margin-left: 10px;<?=($checked == 1 && $model->custom_rate != null && $model->rate_added_through == 0)?'color:red;':''?> <?=($model->custom_rate != null && $model->rate_added_through == 1 )?'color:orange;':''?>" data="<?php echo $value->person_no?>">
                                                    </label><br>

                                                    <?php
                                                }
                                            } 
                                        }
                                        echo '</td>';?>
                                        <td style="min-width:20px;">
                                            <label>No of Adults</label>
                                            <select style="width:25%" class="modal-adults-dropdown modal-adults-dropdown-<?php echo $model->rate_id.'-'.$model->date?>" date="<?php echo $model->date; ?>" data="<?php echo $model->item_id.':'.$model->rate_id?>">
                                                <?php
                                                    $adult_count = isset($model->item->max_adults)?$model->item->max_adults:10;
                                                    for ($i=0; $i <= $adult_count ; $i++) 
                                                    {
                                                        $selected = $model->no_of_adults == $i ? 2 : 0;

                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                    }
                                                ?>
                                            </select>
                                            <label>No of Adults</label>
                                            <select style="width:25%;margin-top: 20px" class="modal-children-dropdown modal-children-dropdown-<?php echo $model->rate_id.'-'.$model->date?>" date="<?php echo $model->date; ?>" data="<?php echo $model->item_id.':'.$model->rate_id?>">
                                                <?php
                                                    $children_count = isset($model->item->max_children)?$model->item->max_children:10;
                                                    for ($i=0; $i <= $children_count ; $i++) 
                                                    { 
                                                        $selected = $selected = $model->no_of_children == $i ? 2 : 0;

                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                    }
                                                ?>
                                            </select>
                                        </td>

                                    <?php }
                                    else if($model->bookingItem->provider->pricing == 0)  // ************* First/Additional ************ //
                                    {
                                    ?>
                                        <td style="min-width:50px;">
                                            <select style="width:100%" class="modal-adults-dropdown modal-adults-dropdown-<?php echo $model->rate_id.'-'.$model->date?>" date="<?php echo $model->date; ?>" data="<?php echo $model->item_id.':'.$model->rate_id?>">
                                                <?php
                                                    for ($i=0; $i <= 20 ; $i++) 
                                                    {
                                                        $selected = $model->no_of_adults == $i ? 2 : 0;

                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                    }
                                                ?>
                                            </select>
                                        </td>

                                        <td style="min-width:50px;">
                                            <select style="width:100%" class="modal-children-dropdown modal-children-dropdown-<?php echo $model->rate_id.'-'.$model->date?>" date="<?php echo $model->date; ?>" data="<?php echo $model->item_id.':'.$model->rate_id?>">
                                                <?php
                                                    for ($i=0; $i <= 20 ; $i++) 
                                                    { 
                                                        $selected = $selected = $model->no_of_children == $i ? 2 : 0;

                                                        echo '<option value="'.$i.'" '.$Options[$selected].'> '.$i.' </option>';
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                    <?php
                                    }

                                    // ******************* Available Discount Codes and Upsell Items ******************** //

                                    $max_extra_beds = $model->item->max_extra_beds;
                                    $bed_type_people = 0;

                                    $selected_upsell_extra_bed_quantity = '';
                                    $selected_discount_code_extra_bed_quantity = '';

                                    if($model->bookingItem->provider->pricing == 1 && $model->bookingItem->provider->getDestinationTypeName()=='Accommodation')
                                    {
                                        $itemBedTypesModel = BookableBedTypes::find()->where(['bookable_id' => $model->item_id])->all();

                                        if(!empty($itemBedTypesModel))
                                        {
                                            foreach ($itemBedTypesModel as $key => $value) 
                                            {
                                                $bed_type_people = $bed_type_people + ($value->quantity * $value->bedType->max_sleeping_capacity);
                                            }
                                        }
                                    }
                                    
                                    // *********** Discount Codes ********** //

                                    $get_discount_codes = BookingsDiscountCodesCart::find()->where(['booking_date_id' => $model->id])->all();
                                    $checked_discount_codes = [];
                                    $selected_discount_code_extra_bed_quantity = [];

                                    if(!empty($get_discount_codes))
                                    {
                                        foreach ($get_discount_codes as $key => $value) 
                                        {
                                            $checked_discount_codes[$value['discount_id']] = $value['price'];
                                            $selected_discount_code_extra_bed_quantity[$value['discount_id']] = $value['extra_bed_quantity'];
                                        }
                                    }

                                    echo $this->render('modal_discount_codes',
                                    [
                                        'dateKey' => $model->date,
                                        'item_id' => $model->item_id,
                                        'rates_id' => $model->rate_id,
                                        'checked_discount_codes' => $checked_discount_codes,
                                        'bed_type_people' => $bed_type_people,
                                        'max_extra_beds' => $max_extra_beds,
                                        'selected_discount_code_extra_bed_quantity' => $selected_discount_code_extra_bed_quantity,
                                    ]);

                                    // *********** Upsell Items ********** //

                                    $get_upsell_items = BookingsUpsellItemsCart::find()->where(['booking_date_id' => $model->id])->all();
                                    $checked_upsell_items = [];
                                    $selected_upsell_extra_bed_quantity = [];

                                    if(!empty($get_upsell_items))
                                    {
                                        foreach ($get_upsell_items as $key => $value) 
                                        {
                                            $checked_upsell_items[$value['upsell_id']] = $value['price'];
                                            $selected_upsell_extra_bed_quantity[$value['upsell_id']] = $value['extra_bed_quantity'];
                                        }
                                    }

                                    echo $this->render('modal_upsell_items',
                                    [
                                        'dateKey' => $model->date,
                                        'rates_id' => $model->rate_id,
                                        'checked_upsell_items' => $checked_upsell_items,
                                        'bed_type_people' => $bed_type_people,
                                        'max_extra_beds' => $max_extra_beds,
                                        'selected_upsell_extra_bed_quantity' => $selected_upsell_extra_bed_quantity,
                                    ]);

                                    // ****************** Total Price and Json *********************//

                                    $orgTotal = 0;
                                    $icelandTotal = 0;
                                    $tooltips_title = '';

                                    if(!empty($model->total))
                                    {
                                        $orgTotal = $model->total;
                                        $icelandTotal = Yii::$app->formatter->asDecimal($orgTotal, 2);
                                    }

                                    if(!empty($model->price_json))
                                    {
                                        $arr = json_decode($model->price_json);

                                        $tooltips_title = $tooltips_title . 'Quantity = '. $arr->quantity .' , ';
                                        $tooltips_title = $tooltips_title . 'Total Nights = '. $arr->total_nights .' , ';
                                        $tooltips_title = $tooltips_title . 'Rate = '. $arr->rate .' , ';
                                        $tooltips_title = $tooltips_title . 'VAT % = '. $arr->vat .' , ';
                                        $tooltips_title = $tooltips_title . 'x = '. $arr->x .' , ';
                                        $tooltips_title = $tooltips_title . 'lodgingTax = '. $arr->lodgingTax .' , ';
                                        $tooltips_title = $tooltips_title . 'y = '. $arr->y .' , ';
                                        $tooltips_title = $tooltips_title . 'voucher_discount = '. $arr->voucher_discount .' , ';
                                        $tooltips_title = $tooltips_title . 'travel_partner_discount = '. $arr->travel_partner_discount .' , ';
                                        $tooltips_title = $tooltips_title . 'offer_discount = '. $arr->offer_discount .' , ';
                                        $tooltips_title = $tooltips_title . 'amount_of_discount = '. $arr->amount_of_discount .' , ';
                                        $tooltips_title = $tooltips_title . 'sub_total_1 = '. $arr->sub_total_1 .' , ';
                                        $tooltips_title = $tooltips_title . 'sub_total_2 = '. $arr->sub_total_2 .' , ';
                                        $tooltips_title = $tooltips_title . 'vat_amount = '. $arr->vat_amount .' , ';
                                        $tooltips_title = $tooltips_title . 'Total Price = '. $arr->icelandPrice;

                                        $price_json = str_replace('"', "'", $model->price_json);
                                    }
                                    else
                                    {
                                        $tooltips_title = 'Item Price';
                                        $price_json = '';
                                    }

                                    ?>

                                    <td>
                                        <input class="modal-item-total-input-<?php echo $model->rate_id.'-'.$model->date?>" type="text" value="<?php echo $orgTotal?>" hidden>

                                        <input type="text" value="<?php echo $price_json; ?>" class="modal-price-json-<?php echo $model->rate_id.'-'.$model->date?>" hidden>

                                        <label>
                                            <a href="javascript:;" data-placement="left" class="modal-item-total-<?php echo $model->rate_id.'-'.$model->date?> modal-total-tooltips" style="text-decoration: none; color:black" data-original-title="<?php echo $tooltips_title ?>"><i class="fa fa-info-circle" aria-hidden="true"></i>
                                            </a>
                                            <span><?php echo $icelandTotal?></span>
                                        </label>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="panel panel-default modal-rate-details-fields-<?php echo $model->rate_id.'-'.$model->date?>" style="display: none; background: #F1F1F1">
                                <div class="panel-body">
                                    <?php 

                                        if($model->bookingItem->provider->getDestinationTypeName()=='Accommodation')
                                        {
                                            $itemBedsCombinationsModel = BookableBedsCombinations::find()->where(['item_id' => $model->item_id])->all();

                                            if(!empty($itemBedsCombinationsModel))
                                            {
                                                echo '<label class="control-label modal-rate-details-fields"><strong>Bed Preference:</strong></label>';

                                                echo '<div class="modal-rate-details-fields" style="display: block;">';

                                                    foreach ($itemBedsCombinationsModel as $key => $value) 
                                                    {   
                                                        $checked = ($value->beds_combinations_id == $model->beds_combinations_id)?1:0;

                                                        $path = Yii::getAlias('@web').'/../uploads/beds-combinations/'.$value->beds_combinations_id.'/icons/'.$value->bedsCombinations->icon;
                                                        ?>

                                                        <label style="margin-left:10px;" class="mt-radio mt-radio-outline">
                                                            <input data="<?php echo $value->beds_combinations_id?>" class="modal-beds-combinations modal-beds-combinations-<?php echo $model->rate_id.'-'.$model->date; ?>" name="beds-combinations-<?php echo $model->rate_id.'-'.$model->date?>" value="<?php echo $value->beds_combinations_id?>" <?php echo $Options[$checked]?> type="radio"> <?php echo $value->bedsCombinations->combination?> <img src="<?php echo $path ?>" style="max-height:30px" >
                                                            <span></span>
                                                        </label>

                                                        <?php
                                                    }
                                                echo '</div>';
                                            }
                                        }
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>User</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-user-dropdown modal-user-dropdown-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <option value="">Select a User</option>
                                                    <?php
                                                        $users = ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_USER])->all(),
                                                        'id',
                                                        function($model, $defaultValue) 
                                                        {
                                                            if(empty($model->profile->country_id))
                                                                $country = '';
                                                            else
                                                                $country = ' - '.$model->profile->country->name;

                                                            return $model->username.' - '.$model->email.$country;
                                                        });

                                                        if(!empty($users))
                                                        {
                                                            foreach ($users as $key => $value) 
                                                            {
                                                                $selected = ($key==$model->user_id) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>First Name</strong></label>
                                            <div class="form-group" >
                                                <input type="text" class="form-control modal-guest-first-name modal-guest-first-name-<?php echo $model->rate_id.'-'.$model->date?>" value="<?= isset($model->guest_first_name) ? $model->guest_first_name : ''?>" >
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Last Name</strong></label>
                                            <div class="form-group" >
                                                <input type="text" class="form-control modal-guest-last-name modal-guest-last-name-<?php echo $model->rate_id.'-'.$model->date?>" value="<?= isset($model->guest_last_name)? $model->guest_last_name : ''?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Country</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-guest-country modal-guest-country-<?php echo $model->rate_id.'-'.$model->date?>" >
                                                    <?php
                                                        $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                                        foreach ($countries as $key => $value) 
                                                        {
                                                            if(!empty($model->guest_country_id))
                                                            {
                                                                $selected = ($key == $model->guest_country_id) ? 2 : 0 ;
                                                            }
                                                            else
                                                            {
                                                                //######## added iceland as default selected #############//
                                                                $selected = ($value == 'Iceland') ? 2 : 0 ;
                                                            }
                                                            
                                                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Booking Confirmation</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-confirmation modal-booking-confirmation-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <?php
                                                        $bookingConfirmation =ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name');

                                                        if(!empty($bookingConfirmation))
                                                        {
                                                            foreach ($bookingConfirmation as $key => $value) 
                                                            {
                                                                $selected = ($key==$model->confirmation_id) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Bookings Cancellation</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-cancellation modal-booking-cancellation-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <?php
                                                        foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                                                        {
                                                            $selected = ($key==$model->booking_cancellation) ? 2 : 0 ;

                                                            echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Flag</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-flag modal-booking-flag-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <?php
                                                        $bookingTypes = ArrayHelper::map(BookingTypes::find()->all(),'id','label');

                                                        if(!empty($bookingTypes))
                                                        {
                                                            foreach ($bookingTypes as $key => $value) 
                                                            {
                                                                $selected = ($key==$model->flag_id) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label"><strong>Status</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-rate-details-dropdowns modal-booking-status modal-booking-status-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <?php
                                                        $bookingStatuses = ArrayHelper::map(BookingStatuses::find()->all(),'id','label');

                                                        if(!empty($bookingStatuses))
                                                        {
                                                            foreach ($bookingStatuses as $key => $value) 
                                                            {
                                                                $selected = ($key==$model->status_id) ? 2 : 0 ;

                                                                echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        if(!empty($model->travel_partner_id) && empty($model->offers_id))
                                        {
                                            $this->registerJs
                                            ("
                                                var rate_id = $model->rate_id;
                                                var date = '$model->date';

                                                $('.modal-offers-dropdown-'+rate_id+'-'+date).attr('disabled','disabled');
                                                $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).attr('disabled',false);
                                            ");
                                        }
                                        else if(empty($model->travel_partner_id) && !empty($model->offers_id))
                                        {
                                            $this->registerJs
                                            ("
                                                var rate_id = $model->rate_id;
                                                var date = '$model->date';

                                                $('.modal-offers-dropdown-'+rate_id+'-'+date).attr('disabled',false);
                                                $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).attr('disabled','disabled');
                                            ");
                                        }
                                        else
                                        {
                                            $this->registerJs
                                            ("
                                                var rate_id = $model->rate_id;
                                                var date = '$model->date';

                                                $('.modal-offers-dropdown-'+rate_id+'-'+date).attr('disabled',false);
                                                $('.modal-travel-partner-dropdown-'+rate_id+'-'+date).attr('disabled',false);
                                            ");
                                        } 
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="control-label"><strong>Travel Partner</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" date="<?php echo $model->date; ?>" data="<?php echo $model->item_id.':'.$model->rate_id?>" class="form-control modal-travel-partner-dropdown modal-travel-partner-dropdown-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <option value=""> Select Referrer </option>
                                                    <?php
                                                        $travelPartners = DestinationTravelPartners::find()
                                                                                                    ->where(['destination_id' => $model->bookingItem->provider_id])
                                                                                                    ->all();
    

                                                        if(!empty($travelPartners))
                                                        {
                                                            foreach ($travelPartners as $key => $value) 
                                                            {
                                                                $selected = ($value->id==$model->travel_partner_id) ? 2 : 0 ;

                                                                echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->travelPartner->company_name.' - '.$value->comission.'%'.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label"><strong>Offers</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" date="<?php echo $model->date; ?>" data="<?php echo $model->item_id.':'.$model->rate_id?>" class="form-control modal-offers-dropdown modal-offers-dropdown-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <option value=""> Select Offer </option>
                                                    <?php
                                                        $offers = Offers::find()->where(['provider_id'=>$model->bookingItem->provider_id])->all();

                                                        if(!empty($offers))
                                                        {
                                                            foreach ($offers as $key => $value) 
                                                            {
                                                                $selected = ($value->id==$model->offers_id) ? 2 : 0 ;

                                                                echo '<option value="'.$value->id.'" '.$Options[$selected].'>'.$value->name.' - '.$value->commission.'%'.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label" ><strong>Estimated Arrival Time</strong></label>
                                            <div class="form-group" >
                                                <select style="width: 100%;" class="form-control modal-estimated-arrival-time-dropdown modal-rate-details-dropdowns modal-estimated-arrival-time-dropdown-<?php echo $model->rate_id.'-'.$model->date?>">
                                                    <?php
                                                        if(!empty($model->bookingItem->provider_id))
                                                        {
                                                            $estimated_arrival_times = EstimatedArrivalTimes::find()->where(['time_group' => $model->bookingItem->provider->bookingPolicies->time_group])->all();

                                                            if(!empty($estimated_arrival_times))
                                                            {
                                                                foreach ($estimated_arrival_times as $key =>  $obj) 
                                                                {
                                                                    $start_time = '';

                                                                    if(empty($obj->text))
                                                                    {
                                                                        $start_time = date('H:i',strtotime($obj->start_time));
                                                                    }
                                                                    else
                                                                    {
                                                                        $start_time = $obj->text;
                                                                    }
                                                                    $obj->end_time = date('H:i',strtotime($obj->end_time));

                                                                    $selected = ($obj->id==$model->estimated_arrival_time_id) ? 2 : 0 ;
                                                                    
                                                                    echo "<option value='".$obj->id."' ".$Options[$selected].">".$start_time." - ".$obj->end_time."</option>";  
                                                                }
                                                            }  
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?php 
                                                if(!empty($model->bookingItem->provider_id))
                                                {
                                                    $get_special_requests = BookingSpecialRequests::find()->where(['booking_date_id' => $model->id])->all();

                                                    $checked_special_requests = [];

                                                    if(!empty($get_special_requests))
                                                    {
                                                        foreach ($get_special_requests as $key => $value) 
                                                        {
                                                            $checked_special_requests[] = $value['request_id'];
                                                        }
                                                    }

                                                    echo $this->render('modal_special_requests',
                                                    [
                                                        'dateKey' => $model->date,
                                                        'provider_id' => $model->bookingItem->provider_id,
                                                        'checked_special_requests' => $checked_special_requests,
                                                    ]);
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
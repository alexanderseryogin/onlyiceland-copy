<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Destinations;

$session = Yii::$app->session;
$session->open();
if(isset($session['standard-report-destination']) && !empty($session['standard-report-destination']))
{
    $selected_destination =  $session['standard-report-destination'];
    // echo "<pre>";
    // print_r($selected_destination);
    // exit();
}
else
{
    $selected_destination = null;
}

if(isset($session['standard-report-report_to']) && !empty($session['standard-report-report_to']))
{
    $report_to =  date('d-m-Y',strtotime($session['standard-report-report_to']));
}
else
{
    $report_to =  date('d-m-Y');
}

if(isset($session['standard-report-report_from']) && !empty($session['standard-report-report_from']))
{
    $report_from =  date('d-m-Y',strtotime($session['standard-report-report_from']));
}
else
{
    $report_from =  date('d-m-Y');
}

if(isset($session['standard-report-canceled_booking']) && !empty($session['standard-report-canceled_booking']))
{
    $canceled_booking =  ['standard-report-canceled_booking'];
}
else
{
    $canceled_booking = 0;
}

?>
<form method="POST" action="<?=yii::$app->getUrlManager()->createUrl(['standard-reports/gen-report'])?>" onSubmit="(success) ? event.preventDefault() : ''">
	<input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label"><b>Destination</b></label>
				<?= Html::dropDownList('destination_id', null,ArrayHelper::map(Destinations::find()->all(),'id','name'), ['prompt' => 'Select Destination','id' => 'destination_id','options'=> [ $selected_destination =>['Selected'=>true]]]) ?>
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label"><b>Report From</b></label>
				 <input name='report_from' id="report_from" class="form-control form-control-inline input-medium date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=$report_from?>">
			</div>
	           

		</div>


		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label"><b>Report To</b></label>
	            <input name='report_to' id="report_to" class="form-control form-control-inline input-medium date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="<?=$report_to?>">
	        </div>
		</div>
		<div class="col-sm-1 hide-fields" style="margin-top:30px; margin-left: -20px">
            <button id="today" type="button" class="btn btn-xs btn-primary">TODAY</button>
        </div>

		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label" ><b>Include Cancelled Bookings</b></label>
				<?= Html::dropDownList('c_bookings', null,[1 => 'Yes',0 => 'No'], ['prompt' => 'Select an Option','id' => 'c_bookings','options'=> [ $canceled_booking =>['Selected'=>true]]]) ?>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	var success = true;
</script>
<?php

$this->registerJs("
	$('#destination_id').select2({
        placeholder: \"Select a Destination\",
        allowClear: true
    });
    $('#c_bookings').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });
    $('#arrival').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		if($('#destination_id').val() == '' || $('#report_from').val() == '' || $('#report_to').val() == '' || $('#c_bookings').val() == '')
		{
			console.log('in if');
			alert('All fields are not filled');
			return false;
		}
		else
		{
			console.log('in else');
			$('<input>').attr({
			    type: 'hidden',
			    id: 'foo',
			    name: 'type',
			    value: 'arrival'
			}).appendTo('form');
			success = false;
			$('form').submit();
			success = true;
		}
			
	});
	$('#departure').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		if($('#destination_id').val() == '' || $('#report_from').val() == '' || $('#report_to').val() == '' || $('#c_bookings').val() == '')
		{
			console.log('in if');
			alert('All fields are not filled');
			return false;
		}
		else
		{
			console.log('in else');
			$('<input>').attr({
			    type: 'hidden',
			    id: 'foo',
			    name: 'type',
			    value: 'departure'
			}).appendTo('form');
			success = false;
			$('form').submit();
			success = true;
		}
			
	});

	$('#current_guests').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		if($('#destination_id').val() == '' || $('#report_from').val() == '' || $('#report_to').val() == '' || $('#c_bookings').val() == '')
		{
			console.log('in if');
			alert('All fields are not filled');
			return false;
		}
		else
		{
			console.log('in else');
			$('<input>').attr({
			    type: 'hidden',
			    id: 'foo',
			    name: 'type',
			    value: 'current_guests'
			}).appendTo('form');
			success = false;
			$('form').attr('action','".yii::$app->getUrlManager()->createUrl(['standard-reports/current-guests-report'])."');
			$('form').submit();
			success = true;
		}
			
	});

	$('#daily_unit_activity').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/daily-guest-activity-report'])."';
		SubmitReport(url);
			
	});

	$('#daily_unit_activity_org').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/daily-unit-activity-report'])."';
		SubmitReport(url);
			
	});
	
	$('#daily_cleaning_breif').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/daily-cleaning-brief-report'])."';
		SubmitReport(url);
			
	});

	$('#daily_occupancy').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/occupancy-report'])."';
		SubmitReport(url);
			
	});

	$('#room_occupancy').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/room-occupancy-report'])."';
		SubmitReport(url);
			
	});

	$('#monthly_occupancy').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/monthly-occupancy-report'])."';
		SubmitReport(url);
			
	});

	$('#country_occupancy').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/country-occupancy-report'])."';
		SubmitReport(url);
			
	});

	$('#gross_revenue_summary').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/gross-revenue-report','type' => '306'])."';
		SubmitReport(url);
			
	});

	$('#gross_revenue_summary-1').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/gross-revenue-report','type' => '305'])."';
		SubmitReport(url);
			
	});

	$('#gross_revenue_summary-2').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/gross-revenue-report','type' => '307'])."';
		SubmitReport(url);
			
	});

	$('#gross_credit_summary-1').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/gross-credit-report','type' => '308'])."';
		SubmitReport(url);
			
	});

	$('#gross_credit_summary-2').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/gross-credit-report','type' => '309'])."';
		SubmitReport(url);
			
	});

	$('#gross_credit_summary').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/gross-credit-report','type' => '310'])."';
		SubmitReport(url);
			
	});

	$('#net_revenue_summary-1').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/net-revenue-report','type' => '311'])."';
		SubmitReport(url);
			
	});

	$('#net_revenue_summary-2').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/net-revenue-report','type' => '312'])."';
		SubmitReport(url);
			
	});

	$('#net_revenue_summary-3').click(function(e){
		//console.log('form clicked');
		e.preventDefault();
		var url = '".yii::$app->getUrlManager()->createUrl(['standard-reports/net-revenue-report','type' => '323'])."';
		SubmitReport(url);
			
	});


	function SubmitReport(url)
	{
		if($('#destination_id').val() == '' || $('#report_from').val() == '' || $('#report_to').val() == '' || $('#c_bookings').val() == '')
		{
			console.log('in if');
			alert('All fields are not filled');
			return false;
		}
		else
		{
			console.log('in else');
			$('<input>').attr({
			    type: 'hidden',
			    id: 'foo',
			    name: 'type',
			    value: 'current_guests'
			}).appendTo('form');
			success = false;
			$('form').attr('action',url);
			$('form').submit();
			success = true;
		}
	}

	// $(document).ready(function(){
	// 	$('#report_from').trigger('change');
	// });
	
	$('#report_from').on('change',function()
    {   
        var new_date = moment($(this).val(),'D/M/YYYY').add('days', 1);
        //$('#report_to').datepicker('setDate',new_date.format('DD-MM-YYYY'));

        var startDt=document.getElementById('report_to').value;
    	var report_to = startDt.split('-');
    	var report_to_date = report_to[1]+'-'+report_to[0]+'-'+report_to[2]


		var endDt=document.getElementById('report_from').value;
		var report_from = endDt.split('-');
    	var report_from_date = report_from[1]+'-'+report_from[0]+'-'+report_from[2]

		console.log('report_to_date '+ new Date(report_to_date));
		console.log('report_from '+ new Date(report_from_date));

		if(new Date(report_from_date) > new Date(report_to_date) )
		{
		    $('#report_to').val($(this).val());
		}
		else if( new Date(report_from_date) <  new Date(report_to_date))
		{
		    console.log('smaller');
		}
		else
		{
		    console.log('same');
		}


    });

    $('#today').click(function()
    {
        var today = new Date();
        var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();

		$('#report_to').datepicker('setDate',date);
        $('#report_from').datepicker('setDate',date);
    });

    $('#report_to').on('change',function()
    {
    	var startDt=document.getElementById('report_to').value;
    	var report_to = startDt.split('-');
    	var report_to_date = report_to[1]+'-'+report_to[0]+'-'+report_to[2]


		var endDt=document.getElementById('report_from').value;
		var report_from = endDt.split('-');
    	var report_from_date = report_from[1]+'-'+report_from[0]+'-'+report_from[2]

		console.log('report_to_date '+ new Date(report_to_date));
		console.log('report_from '+ new Date(report_from_date));

		if(new Date(report_to_date) < new Date(report_from_date) )
		{
		    $('#report_from').val($(this).val());
		}
		else if( new Date(report_to_date) >  new Date(report_from_date))
		{
		    console.log('greater');
		}
		else
		{
		    console.log('same');
		}

    });
");
?>
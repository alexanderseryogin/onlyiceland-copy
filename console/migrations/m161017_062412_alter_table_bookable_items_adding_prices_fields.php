<?php

use yii\db\Migration;

class m161017_062412_alter_table_bookable_items_adding_prices_fields extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `min_price` VARCHAR(256) NULL AFTER `longest_period`, ADD COLUMN `max_price` VARCHAR(256) NULL AFTER `min_price`; 
            ");
    }

    public function down()
    {
        echo "m161017_062412_alter_table_bookable_items_adding_prices_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

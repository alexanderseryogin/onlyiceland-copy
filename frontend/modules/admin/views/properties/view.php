<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Properties */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properties-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'region_id',
            'owner_id',
            'property_type_id',
            'name',
            'address:ntext',
            'city_id',
            'postal_code',
            'latitude',
            'longitude',
            'reservations_email:email',
            'business_name',
            'business_id_number',
            'business_address:ntext',
            'business_city_id',
            'business_postal_code',
            'invoice_email:email',
            'youngest_age',
            'payment_methods_for_gauarantee:ntext',
            'payment_methods_checkin:ntext',
        ],
    ]) ?>

</div>

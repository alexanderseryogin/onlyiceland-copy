<?php

use yii\db\Migration;

class m170120_054041_alter_upsell_remove_provider_id extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `upsell_items` DROP COLUMN `provider_id`, DROP COLUMN `amount`, DROP INDEX `FK_upsell_items_provider_id`, DROP FOREIGN KEY `FK_upsell_items_provider_id`; 
        ");
    }

    public function down()
    {
        echo "m170120_054041_alter_upsell_remove_provider_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookings_discount_codes".
 *
 * @property string $id
 * @property string $booking_date_id
 * @property integer $discount_id
 * @property integer $price
 * @property integer $extra_bed_quantity
 *
 * @property BookingDates $bookingDate
 * @property DiscountCode $discount
 */
class BookingsDiscountCodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings_discount_codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_date_id', 'discount_id', 'price', 'extra_bed_quantity'], 'integer'],
            [['booking_date_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingDates::className(), 'targetAttribute' => ['booking_date_id' => 'id']],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountCode::className(), 'targetAttribute' => ['discount_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'booking_date_id' => Yii::t('app', 'Booking Date ID'),
            'discount_id' => Yii::t('app', 'Discount ID'),
            'price' => Yii::t('app', 'Price'),
            'extra_bed_quantity' => Yii::t('app', 'Extra Bed Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingDate()
    {
        return $this->hasOne(BookingDates::className(), ['id' => 'booking_date_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(DiscountCode::className(), ['id' => 'discount_id']);
    }
}

<?php

use yii\db\Migration;

class m161018_142440_alter_table_user_profile_alter_title extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user_profile` CHANGE `title` `title` VARCHAR(256) NULL; ");
    }

    public function down()
    {
        echo "m161018_142440_alter_table_user_profile_alter_title cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161026_120738_create_table_offers extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `offers`( `id` INT(11) NOT NULL AUTO_INCREMENT, `provider_id` INT(11), `item_id` INT(11), `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `summary` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `additional_details` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `allow_cancellations` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `booking_confirmation_type_id` INT(11), `min_stay` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `show` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_offers_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_offers_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_offers_booking_confirmation_type_id` FOREIGN KEY (`booking_confirmation_type_id`) REFERENCES `booking_confirmation_types`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161026_120738_create_table_offers cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

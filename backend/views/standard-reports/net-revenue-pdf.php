<?php
use common\models\BookingDates;
use yii\grid\GridView;
use yii\helpers\Html;
$bookableItems = $destination->bookableItems;
?>
<h3><b><?=$destination->name?> <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></b> <h3>
<table class="table table-striped table-bordered table2excel">
    <!-- <thead>
      <tr>
        <th><?=$destination->name?></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </thead> -->
    <tbody>
     <?php
        $total_arr = array();
        $total_arr['total_revenue'] = 0;
        $total_arr['total_credit'] = 0;
        $total_arr['total_net'] = 0;
        // $total_arr['booked_count'] = 0;
        // $total_arr['avail_count'] = 0;
        // $total_arr['occupancy_count'] = 0;

        for ($i=0; $i < count($destination->bookableItems)+3 ; $i++) 
        { 
            if($i ==  0)
            {
                echo "<tr>";
                echo "<td></td>";
                foreach ($report as $key => $value) 
                {
                    echo "<td colspan=3 style='text-align:center;'>".$key."</td>";
                    // echo "<td style='border-right: 0px;'>".$key."</td>";
                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                    // echo "<td style='border-left: 0px;'></td>";
                    
                }
                echo "<td colspan=3 style='border-right: 0px;text-align:center;'>Total</td>";
                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                // echo "<td style='border-left: 0px;'></td>";
                echo "</tr>";
            }
            elseif($i == 1)
            {
                echo "<tr>";
                echo "<td><b>".$destination->name."</b></td>";
                foreach ($report as $key => $value) 
                {
                    echo "<td>Gross Revenue</td>";
                    echo "<td>Taxes/Fees</td>";
                    echo "<td>Net Revenue</td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    // echo "<td></td>";
                    
                }
                echo "<td>Gross Revenue</td>";
                echo "<td>Taxes/Fees</td>";
                echo "<td>Net Revenue</td>";
                // echo "<td colspan='4' style='border-right: none !important;'>total</td>";
                echo "</tr>";
            }   
            else
            {
                if($i < count($destination->bookableItems)+2)
                {
                    $b_item = $bookableItems[$i-2];
                    echo "<tr>";
                    echo "<td>".$b_item->itemType->name."</td>";
                    // echo "<pre>";
                    //     print_r($report);
                    //     exit();
                    // $booked_count = 0;
                    // $avail_count = 0;
                    $total_revenue = 0;
                    $total_credit = 0;
                    $total_net = 0;
                    foreach ($report as $key => $value) 
                    {
                        $item = $value[$b_item->itemType->name];
                        $net_amount = $item['debit']-round($item['credit']);
                        // echo "<pre>";
                        // print_r($net_amount);
                        // exit();
                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($item['debit'])."</td>";
                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal(round($item['credit']))."</td>";

                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($net_amount)."</td>";
                        // echo "<td style='text-align:right'>".$item['booked']."</td>";
                        // echo "<td style='text-align:right'>".$item['avail']."</td>";
                        // echo "<td style='text-align:right'>".round($item['occupancy'],2).' %' ."</td>";
                        $total_revenue += $item['debit'];
                        $total_credit += round($item['credit']);
                        $total_net += $net_amount;
                    }
                    $total_arr['total_revenue'] += $total_revenue;
                    $total_arr['total_credit'] += $total_credit;
                    $total_arr['total_net'] += $total_net;
                    // $total_arr['booked_count'] += $booked_count;
                    // $total_arr['avail_count'] += $avail_count;
                    //$total_arr['occupancy_count'] += round($guests_occupancy/count($report),2);

                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_revenue)."</td>";
                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_credit)."</td>";
                    // echo "<td style='text-align:right'>".$total_net."</td>";
                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_net)."</td>";
                    echo "</tr>";

                }
                else
                {
                    // echo "<pre>";
                    // print_r($total_arr);
                    echo "<tr>";
                    echo "<td>Total</td>";
                    // echo "<pre>";
                    // print_r($report);
                    // exit();
                    
                    foreach ($report as $key => $value) 
                    {
                        // $guests_count = 0;
                        $total_revenue = 0;
                        $total_credit = 0;
                        $total_net = 0;
                        foreach ($destination->bookableItems as $item) 
                        {
                            $total_revenue += $value[$item->itemType->name]['debit'];
                            $total_credit += round($value[$item->itemType->name]['credit']);
                            $total_net += ($value[$item->itemType->name]['debit']-round($value[$item->itemType->name]['credit']));
                            // $guests_occupancy += round($value[$item->itemType->name]['occupancy'],2);
                        }
                        
                        // $occupancy = round(($total_occupancy/count($destination->bookableItems)),2);
                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_revenue)."</td>";
                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_credit)."</td>";
                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_net)."</td>";

                    }
                    // echo "<td style='text-align:right'>".$total_arr['guests_count']."</td>";
                    // echo "<td style='text-align:right'>".$total_arr['booked_count']."</td>";
                    // echo "<td style='text-align:right'>".$total_arr['avail_count']."</td>";
                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_revenue'])."</td>";
                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_credit'])."</td>";
                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_net'])."</td>";
                    // */
                    echo "</tr>";

                }
                                                                    
            }
            
    
        }
    ?>
    </tbody>
  </table>
<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
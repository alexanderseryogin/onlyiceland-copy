<?php

use yii\db\Migration;

class m180413_115114_alter_table_booking_group_add_field_old_booking_group_no extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `booking_group`   
                          ADD COLUMN `old_group_no` VARCHAR(256) NULL AFTER `comments`;
                        ');
    }

    public function safeDown()
    {
        echo "m180413_115114_alter_table_booking_group_add_field_old_booking_group_no cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180413_115114_alter_table_booking_group_add_field_old_booking_group_no cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\User;
use backend\components\Helpers;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

$update_page_url = Url::to(['/users/update']);
?>
<style type="text/css">
    .show-filters{ display: block; }
    .hide-filters{ display: none; }
    tr:hover{
        cursor: pointer;
    }

    .grid-view .filters input, .grid-view .filters select {
        width: 100%;
    }

    .grid-view td {
        white-space: pre-wrap;
    }

</style>
<div class="users-index">

    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW USER'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'user-gridview','timeout' => 10000, 'enablePushState' => false]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            //'status',
            [
                'attribute' => 'type',
                'value' => function($data) {
                    return Helpers::userLables($data->type); 
                },
                'format' => 'raw',
                'filter' => Html::dropDownList(
                    'UserSearch[type]', 
                    $searchModel['type'], 
                    [
                        ""=>"",
                        User::USER_TYPE_ADMIN => Helpers::userLables(User::USER_TYPE_ADMIN, true), 
                        User::USER_TYPE_PMANAGER => Helpers::userLables(User::USER_TYPE_PMANAGER, true), 
                        User::USER_TYPE_USER => Helpers::userLables(User::USER_TYPE_USER, true),
                        User::USER_TYPE_OWNER => Helpers::userLables(User::USER_TYPE_OWNER, true),
                    ],
                    ['class'=>'form-control',
                    'id' => 'type_dropdown']),
                'contentOptions' =>['style' => 'width:15%'],
            ],
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return Helpers::statusLables($data->status); 
                },
                'format' => 'raw',
                'filter' => Html::dropDownList(
                    'UserSearch[status]', 
                    $searchModel['status'], 
                    [
                        ""=>"",
                        User::STATUS_ACTIVE => Helpers::statusLables(User::STATUS_ACTIVE, true), 
                        User::STATUS_DELETED => Helpers::statusLables(User::STATUS_DELETED, true), 
                    ],
                    ['class'=>'form-control',
                    'id' => 'status_dropdown']),
                'contentOptions' =>['style' => 'width:15%'],
            ],
            [
                'attribute' => 'first_login_reset_flag',
                'value' => function($data) {
                    return Helpers::presetLables($data->first_login_reset_flag); 
                },
                'format' => 'raw',
                'filter' => Html::dropDownList(
                    'UserSearch[first_login_reset_flag]', 
                    $searchModel['first_login_reset_flag'], 
                    [
                        ""=>"",
                        User::INITIAL_PASSWORD_RESET_PENDING => Helpers::presetLables(User::INITIAL_PASSWORD_RESET_PENDING, true), 
                        User::INITIAL_PASSWORD_RESET_COMPLETE => Helpers::presetLables(User::INITIAL_PASSWORD_RESET_COMPLETE, true), 
                    ],
                    ['class'=>'form-control',
                    'id' => 'reset_dropdown']),
                'contentOptions' =>['style' => 'width:15%'],
            ],
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' =>['style' => 'width:10%'],
                'template' => '{force_reset} {view} {update} {delete}',
                'buttons' => [
                    'force_reset' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-off"></span>', Url::to(['force-reset', 'id'=>$model->id]), 
                        [
                            'title' => 'Reset Password',
                        ]);
                    }
                ],
                'visibleButtons' => [
                    // hide delete button for administrator who is logged in
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->id == $model->id ? false : true;
                    },
                    'force_reset' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->id == $model->id ? false : true;
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#status_dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#reset_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#user-gridview').on('pjax:end', function() {

        $('#status_dropdown').select2({
            placeholder: \"Select a Status\",
            allowClear: true
        });

        $('#reset_dropdown').select2({
            placeholder: \"Select an Option\",
            allowClear: true
        });

        $('#type_dropdown').select2({
            placeholder: \"Select a Type\",
            allowClear: true
        });
        
    });

    $(document).on('pjax:complete', function() {
      $(document).find('.select2-container').hide();
    });

    $(document).on('click','.table-striped tbody tr',function()
    {
        window.location.replace('$update_page_url?id='+$(this).attr('data-key'));
    });

});",View::POS_END);
?>


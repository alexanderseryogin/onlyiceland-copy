<?php
use yii\helpers\Html;
use common\models\BookingDates;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use moonland\phpexcel\Excel;
$this->title = 'Net Revenue Report';
$bookableItems = $destination->bookableItems;
// echo "<pre>";
// print_r($report);
// exit;
?>
<div class="arrival-report" id="arrival-report">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase"><?=$destination->name?> - <?=$title?> <?=date('d.m.Y',strtotime($report_from))?> - <?=date('d.m.Y',strtotime($report_to))?></span>
                    </div>
                    <ul class="nav nav-tabs arrival-report">
                       <!--  <li class="active">
                            <a href="#guests-housekeeping" data-toggle="tab">Guests & Housekeeping</a>
                        </li>
                        <li >
                            <a href="#occupancy" data-toggle="tab">Occupancy</a>
                        </li>
                        <li >
                            <a href="#financial" data-toggle="tab">Financial</a>
                        </li> -->
                        <!-- <li>
                            <a href="#details" data-toggle="tab">Details</a>
                        </li> -->

                    </ul>
                </div>

                <div class="portlet-body">
                	<div class="portlet-body arrival-portley">
                        <div class="tab-content">
                            <div class="btn-group pull-left" style="margin: 5px">
                                <div class="btn-group">
                                <button id="w2" class="btn btn-success dropdown-toggle" title="Export data in selected format" data-toggle="dropdown"><i class="glyphicon glyphicon-export"></i> Export Report <span class="caret">
                                    
                                </span></button>

                                    <ul id="w3" class="dropdown-menu">
                                        <li title="Comma Separated Values"><a id="csv-btn" class="export-full-csv" href="#" data-format="CSV" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                        <li title="Microsoft Excel 2007+ (xlsx)"><a id="excel-btn"  href="#" data-format="Excel2007" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> EXCEL</a></li>
                                        <li>
                                            <a href="<?=yii::$app->getUrlManager()->createUrl(['standard-reports/net-revenue-pdf','destination_id' => $destination_id,
                                                'report_to' => $report_to,
                                                'report_from' => $report_from,
                                                'cancelled_bookings' => $cancelled_bookings,
                                                'report_type' => $report_type,
                                                'type' => $type ])?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            PDF</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <br><br><br>
	                    	<div class="tab-pane active " id="occupancy-report" style="overflow-y: auto;">
                                <table class="table table-striped table-bordered ">
                                    <!-- <thead>
                                      <tr>
                                        <th><?=$destination->name?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                      </tr>
                                    </thead> -->
                                    <tbody>
                                     <?php
                                        $total_arr = array();
                                        $total_arr['total_revenue'] = 0;
                                        $total_arr['total_credit'] = 0;
                                        $total_arr['total_discount'] = 0;
                                        $total_arr['total_net'] = 0;
                                        // $total_arr['booked_count'] = 0;
                                        // $total_arr['avail_count'] = 0;
                                        // $total_arr['occupancy_count'] = 0;

                                        for ($i=0; $i < count($destination->bookableItems)+3 ; $i++) 
                                        { 
                                            if($i ==  0)
                                            {
                                                echo "<tr>";
                                                echo "<td></td>";
                                                foreach ($report as $key => $value) 
                                                {
                                                    echo "<td colspan=4 style='text-align:center;'>".$key."</td>";
                                                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                    // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                    // echo "<td style='border-left: 0px;'></td>";
                                                    
                                                }
                                                echo "<td colspan=4>Total</td>";
                                                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                // echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                // echo "<td style='border-left: 0px;'></td>";
                                                echo "</tr>";
                                            }
                                            elseif($i == 1)
                                            {
                                                echo "<tr>";
                                                echo "<td><b>".$destination->name."</b></td>";
                                                foreach ($report as $key => $value) 
                                                {
                                                    echo "<td>Gross Revenue</td>";
                                                    echo "<td>Taxes/Fees</td>";
                                                    echo "<td>Discount</td>";
                                                    echo "<td>Net Revenue</td>";
                                                    // echo "<td></td>";
                                                    // echo "<td></td>";
                                                    // echo "<td></td>";
                                                    
                                                }
                                                echo "<td>Gross Revenue</td>";
                                                echo "<td>Taxes/Fees</td>";
                                                echo "<td>Discount</td>";
                                                echo "<td>Net Revenue</td>";
                                                // echo "<td colspan='4' style='border-right: none !important;'>total</td>";
                                                echo "</tr>";
                                            }   
                                            else
                                            {
                                                if($i < count($destination->bookableItems)+2)
                                                {
                                                    $b_item = $bookableItems[$i-2];
                                                    echo "<tr>";
                                                    echo "<td>".$b_item->itemType->name."</td>";
                                                    // echo "<pre>";
                                                    //     print_r($report);
                                                    //     exit();
                                                    // $booked_count = 0;
                                                    // $avail_count = 0;
                                                    $total_revenue = 0;
                                                    $total_credit = 0;
                                                    $total_discount = 0;
                                                    $total_net = 0;
                                                    foreach ($report as $key => $value) 
                                                    {
                                                        $item = $value[$b_item->itemType->name];
                                                        $net_amount = $item['debit']-round($item['credit']);
                                                        // echo "<pre>";
                                                        // print_r($net_amount);
                                                        // exit();
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($item['debit'])."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal(round($item['credit']))."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal(round($item['discount']))."</td>";

                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($net_amount)."</td>";
                                                        // echo "<td style='text-align:right'>".$item['booked']."</td>";
                                                        // echo "<td style='text-align:right'>".$item['avail']."</td>";
                                                        // echo "<td style='text-align:right'>".round($item['occupancy'],2).' %' ."</td>";
                                                        $total_revenue += $item['debit'];
                                                        $total_credit += round($item['credit']);
                                                        $total_discount += round($item['discount']);
                                                        $total_net += $net_amount;
                                                    }
                                                    $total_arr['total_revenue'] += $total_revenue;
                                                    $total_arr['total_credit'] += $total_credit;
                                                    $total_arr['total_discount'] += $total_discount;
                                                    $total_arr['total_net'] += $total_net;
                                                    // $total_arr['booked_count'] += $booked_count;
                                                    // $total_arr['avail_count'] += $avail_count;
                                                    //$total_arr['occupancy_count'] += round($guests_occupancy/count($report),2);

                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_revenue)."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_credit)."</td>";
                                                     echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_discount)."</td>";
                                                    // echo "<td style='text-align:right'>".$total_net."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_net)."</td>";
                                                    echo "</tr>";

                                                }
                                                else
                                                {
                                                    // echo "<pre>";
                                                    // print_r($total_arr);
                                                    echo "<tr>";
                                                    echo "<td>Total</td>";
                                                    // echo "<pre>";
                                                    // print_r($report);
                                                    // exit();
                                                    
                                                    foreach ($report as $key => $value) 
                                                    {
                                                        // $guests_count = 0;
                                                        $total_revenue = 0;
                                                        $total_credit = 0;
                                                        $total_discount = 0;
                                                        $total_net = 0;
                                                        foreach ($destination->bookableItems as $item) 
                                                        {
                                                            $total_revenue += $value[$item->itemType->name]['debit'];
                                                            $total_credit += round($value[$item->itemType->name]['credit']);
                                                            $total_discount += round($value[$item->itemType->name]['discount']);

                                                            $total_net += ($value[$item->itemType->name]['debit']-round($value[$item->itemType->name]['credit']));
                                                            // $guests_occupancy += round($value[$item->itemType->name]['occupancy'],2);
                                                        }
                                                        
                                                        // $occupancy = round(($total_occupancy/count($destination->bookableItems)),2);
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_revenue)."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_credit)."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_discount)."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_net)."</td>";

                                                    }
                                                    // echo "<td style='text-align:right'>".$total_arr['guests_count']."</td>";
                                                    // echo "<td style='text-align:right'>".$total_arr['booked_count']."</td>";
                                                    // echo "<td style='text-align:right'>".$total_arr['avail_count']."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_revenue'])."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_credit'])."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_discount'])."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_net'])."</td>";
                                                    // */
                                                    echo "</tr>";

                                                }
                                                                                                    
                                            }
                                            
                                    
                                        }
                                    ?>
                                    </tbody>
                                  </table>
								<p style="font-size: 12px">Generated <?=date('d.m.Y H:i:s')?></p>
							</div>
						</div>
					</div>
				</div>   
               </div>
           </div>
    </div>
</div>
<table class="table table-striped table-bordered table2excel" style="display: none;">
                                    <!-- <thead>
                                      <tr>
                                        <th><?=$destination->name?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                      </tr>
                                    </thead> -->
                                    <tbody>
                                     <?php
                                        $total_arr = array();
                                        $total_arr['total_revenue'] = 0;
                                        $total_arr['total_credit'] = 0;
                                        $total_arr['total_net'] = 0;
                                        // $total_arr['booked_count'] = 0;
                                        // $total_arr['avail_count'] = 0;
                                        // $total_arr['occupancy_count'] = 0;

                                        for ($i=0; $i < count($destination->bookableItems)+3 ; $i++) 
                                        { 
                                            if($i ==  0)
                                            {
                                                echo "<tr>";
                                                echo "<td></td>";
                                                foreach ($report as $key => $value) 
                                                {
                                                    echo "<td style='border-right: 0px;'>".$key."</td>";
                                                    echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                    echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                    // echo "<td style='border-left: 0px;'></td>";
                                                    
                                                }
                                                echo "<td style='border-right: 0px;'>Total</td>";
                                                echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                echo "<td style='border-right: 0px;border-left: 0px;'></td>";
                                                // echo "<td style='border-left: 0px;'></td>";
                                                echo "</tr>";
                                            }
                                            elseif($i == 1)
                                            {
                                                echo "<tr>";
                                                echo "<td><b>".$destination->name."</b></td>";
                                                foreach ($report as $key => $value) 
                                                {
                                                    echo "<td>Revenue</td>";
                                                    echo "<td>Credit</td>";
                                                    echo "<td>Net</td>";
                                                    // echo "<td></td>";
                                                    // echo "<td></td>";
                                                    // echo "<td></td>";
                                                    
                                                }
                                                echo "<td>Revenue</td>";
                                                echo "<td>Credit</td>";
                                                echo "<td>Net</td>";
                                                // echo "<td colspan='4' style='border-right: none !important;'>total</td>";
                                                echo "</tr>";
                                            }   
                                            else
                                            {
                                                if($i < count($destination->bookableItems)+2)
                                                {
                                                    $b_item = $bookableItems[$i-2];
                                                    echo "<tr>";
                                                    echo "<td>".$b_item->itemType->name."</td>";
                                                    // echo "<pre>";
                                                    //     print_r($report);
                                                    //     exit();
                                                    // $booked_count = 0;
                                                    // $avail_count = 0;
                                                    $total_revenue = 0;
                                                    $total_credit = 0;
                                                    $total_net = 0;
                                                    foreach ($report as $key => $value) 
                                                    {
                                                        $item = $value[$b_item->itemType->name];
                                                        $net_amount = $item['debit']-round($item['credit']);
                                                        // echo "<pre>";
                                                        // print_r($net_amount);
                                                        // exit();
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($item['debit'])."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal(round($item['credit']))."</td>";

                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($net_amount)."</td>";
                                                        // echo "<td style='text-align:right'>".$item['booked']."</td>";
                                                        // echo "<td style='text-align:right'>".$item['avail']."</td>";
                                                        // echo "<td style='text-align:right'>".round($item['occupancy'],2).' %' ."</td>";
                                                        $total_revenue += $item['debit'];
                                                        $total_credit += round($item['credit']);
                                                        $total_net += $net_amount;
                                                    }
                                                    $total_arr['total_revenue'] += $total_revenue;
                                                    $total_arr['total_credit'] += $total_credit;
                                                    $total_arr['total_net'] += $total_net;
                                                    // $total_arr['booked_count'] += $booked_count;
                                                    // $total_arr['avail_count'] += $avail_count;
                                                    //$total_arr['occupancy_count'] += round($guests_occupancy/count($report),2);

                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_revenue)."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_credit)."</td>";
                                                    // echo "<td style='text-align:right'>".$total_net."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_net)."</td>";
                                                    echo "</tr>";

                                                }
                                                else
                                                {
                                                    // echo "<pre>";
                                                    // print_r($total_arr);
                                                    echo "<tr>";
                                                    echo "<td>Total</td>";
                                                    // echo "<pre>";
                                                    // print_r($report);
                                                    // exit();
                                                    
                                                    foreach ($report as $key => $value) 
                                                    {
                                                        // $guests_count = 0;
                                                        $total_revenue = 0;
                                                        $total_credit = 0;
                                                        $total_net = 0;
                                                        foreach ($destination->bookableItems as $item) 
                                                        {
                                                            $total_revenue += $value[$item->itemType->name]['debit'];
                                                            $total_credit += round($value[$item->itemType->name]['credit']);
                                                            $total_net += ($value[$item->itemType->name]['debit']-round($value[$item->itemType->name]['credit']));
                                                            // $guests_occupancy += round($value[$item->itemType->name]['occupancy'],2);
                                                        }
                                                        
                                                        // $occupancy = round(($total_occupancy/count($destination->bookableItems)),2);
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_revenue)."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_credit)."</td>";
                                                        echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_net)."</td>";

                                                    }
                                                    // echo "<td style='text-align:right'>".$total_arr['guests_count']."</td>";
                                                    // echo "<td style='text-align:right'>".$total_arr['booked_count']."</td>";
                                                    // echo "<td style='text-align:right'>".$total_arr['avail_count']."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_revenue'])."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_credit'])."</td>";
                                                    echo "<td style='text-align:right'>".Yii::$app->formatter->asDecimal($total_arr['total_net'])."</td>";
                                                    // */
                                                    echo "</tr>";

                                                }
                                                                                                    
                                            }
                                            
                                    
                                        }
                                    ?>
                                    </tbody>
                                  </table>
<?php
$this->registerJs('
    $("#excel-btn").click(function(){
        console.log("clicked");
      $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Gross Credit Report",
            filename: "net-revenue-report",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
    $("#csv-btn").click(function(){
        console.log("clicked");
        
          $(".table2excel").tableToCSV({
            name: "Net Revenue Report",
            filename: "gross-revenue-report",
          });
    }); 
');
$this->registerCss("
    table {
         display: block;
        // overflow-x: auto;
        white-space: nowrap;
        //table-layout: fixed;
    }
    ");

?>
<?php

use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<?php Pjax::begin(['id' => 'multi_night-gridview','timeout' => 1000000, 'enablePushState' => false]) ?>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        //['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'nights',
            'value' => function($data)
            {
                return $data->nights;
            },
        ],
        [
            'attribute' => 'percent',
            'format' => ['decimal'],
            'value' => function($data)
            {
                return $data->percent;
            },
        ],
        [
            'attribute' => 'per_night',
            'format' => ['decimal'],
            'value' => function($data)
            {
                return $data->per_night;
            },
        ],
        [
            'attribute' => 'once_off',
            'format' => ['decimal'],
            'value' => function($data)
            {
                return $data->once_off;
            },
        ],
        [
            'attribute' => 'price_cap',
            'format' => ['decimal'],
            'value' => function($data)
            {
                return $data->price_cap;
            },
        ],

        ['class' => 'yii\grid\ActionColumn',
        'template' => '{update}{delete}',
        'buttons' => [
              'delete' => function ($url, $model, $key) {
                   return '<a style="text-decoration: none;" aria-label="delete" data-id='.$model->id.' title="delete" name="multi_night_delete[]">
                            <span class="glyphicon glyphicon-trash"></span>
                            </a>';
              },
              'update' => function ($url, $model, $key) {
                   return '<a style="text-decoration: none;" aria-label="update" data-id='.$model->id.' title="update" name="multi_night_update[]">
                            <span class="glyphicon glyphicon-pencil"></span>
                            </a>';
              },
          ]
        ],
    ],
]); ?>

<?php Pjax::end() ?>
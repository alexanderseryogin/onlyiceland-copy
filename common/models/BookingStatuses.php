<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_statuses".
 *
 * @property integer $id
 * @property string $label
 * @property string $background_color
 * @property string $text_color
 */
class BookingStatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'background_color', 'text_color'], 'string', 'max' => 256],
            ['label','required'],
            [['label'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'background_color' => Yii::t('app', 'Background Color'),
            'text_color' => Yii::t('app', 'Text Color'),
        ];
    }
}

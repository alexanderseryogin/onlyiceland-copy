<?php

namespace backend\controllers;

use Yii;
use common\models\EstimatedArrivalTimes;
use common\models\EstimatedArrivalTimesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\BaseController;

/**
 * EstimatedArrivalTimesController implements the CRUD actions for EstimatedArrivalTimes model.
 */
class EstimatedArrivalTimesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EstimatedArrivalTimes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EstimatedArrivalTimesSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['EstimatedArrivalTimesSearch']) && !$this->isArrayEmpty($params['EstimatedArrivalTimesSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);

        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EstimatedArrivalTimes model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EstimatedArrivalTimes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EstimatedArrivalTimes();

        $model->start_time = '00:00';
        $model->end_time = '00:00';
        $model->text = 'before';
        $model->text_switch = 0;

        if ($model->load(Yii::$app->request->post()) ) 
        {
            $flag = 0;

            $start_time = $model->start_time;
            $end_time = $model->end_time;

            if(isset($_POST['EstimatedArrivalTimes']['text_switch']))
            {
                $model->text_switch = 1;
                $existing_model = EstimatedArrivalTimes::findOne(['time_group' => $model->time_group,'text' => $model->text, 'end_time' => $end_time.':00']);
                    
                if(!empty($existing_model))
                {
                    Yii::$app->session->setFlash('error', 'Entry with this time interval already exist.');
                    $flag = 1;
                }

                if($flag)
                {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                $model->start_time = NULL;
            }
            else
            {
                if(strtotime($end_time) < strtotime($start_time))
                {
                    Yii::$app->session->setFlash('error', 'End Time must be later than the Start Time.');
                    $flag = 1;
                }
                else
                {
                    $existing_model = EstimatedArrivalTimes::findOne(['time_group' => $model->time_group,'start_time' => $start_time.':00', 'end_time' => $end_time.':00']);

                    if(!empty($existing_model))
                    {
                        Yii::$app->session->setFlash('error', 'Entry with this time interval already exist.');
                        $flag = 1;
                    }
                }

                if($flag)
                {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                $model->text = NULL;
            }

            $model->save();
            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EstimatedArrivalTimes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) 
        {
            $flag = 0;

            $start_time = $model->start_time;
            $end_time = $model->end_time;

            if(isset($_POST['EstimatedArrivalTimes']['text_switch']))
            {
                $model->text_switch = 1;
                $existing_model = EstimatedArrivalTimes::findOne(['time_group' => $model->time_group,'text' => $model->text, 'end_time' => $end_time.':00']);
                    
                if(!empty($existing_model))
                {
                    Yii::$app->session->setFlash('error', 'Entry with this time interval already exist.');
                    $flag = 1;
                }

                if($flag)
                {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }

                $model->start_time = NULL;
            }
            else
            {
                $model->text_switch = 0;
                if(strtotime($end_time) < strtotime($start_time))
                {
                    Yii::$app->session->setFlash('error', 'End Time must be later than the Start Time.');
                    $flag = 1;
                }
                else
                {
                    $existing_model = EstimatedArrivalTimes::findOne(['time_group' => $model->time_group,'start_time' => $start_time.':00', 'end_time' => $end_time.':00']);

                    if(!empty($existing_model))
                    {
                        Yii::$app->session->setFlash('error', 'Entry with this time interval already exist.');
                        $flag = 1;
                    }
                }

                if($flag)
                {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }

                $model->text = NULL;
            }

            $model->save();
            return $this->redirect(['index']);
        } 
        else 
        {
            if(empty($model->text))
            {
                $model->text_switch = 0;
                $model->start_time = date('H:i',strtotime($model->start_time));
            }
            else
            {
                $model->text_switch = 1;
                $model->start_time = '00:00';
            }

            $model->end_time = date('H:i',strtotime($model->end_time));

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EstimatedArrivalTimes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EstimatedArrivalTimes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EstimatedArrivalTimes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EstimatedArrivalTimes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

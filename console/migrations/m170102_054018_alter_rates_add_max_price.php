<?php

use yii\db\Migration;

class m170102_054018_alter_rates_add_max_price extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates` ADD COLUMN `max_price` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `min_price`; 
        ");
    }

    public function down()
    {
        echo "m170102_054018_alter_rates_add_max_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

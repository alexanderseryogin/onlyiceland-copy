<?php

use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationsOpenHoursDays;

$statusCheckbox = [
    0 => '',
    1 => 'checked'
];

$timeDisabled = [
    0 => 'disabled',
    1 => ''
];

$provider_id = '';

?>
<script type="text/javascript">
    var id = '';
</script>

<!--<div class="tab-pane" id="open_hours">-->

    <!-- BEGIN ACCORDION PORTLET-->

    <!--<div class="panel-group accordion" id="accordion3">-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"> Weekly Open Hours </a>
                </h4>
            </div>
            <div id="collapse_3_1" class="panel-collapse in">
                <div class="panel-body">
<!--                     <span class="label label-danger"> NOTE! </span>
                    <span>&nbsp; if you are open 24 hours, you can choose opening time of 00:00 and closing time of 00:00. </span>
                    <br><br> -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info">
                                <strong>Info!</strong>
                                Leave Closing/End Time at 00:00 to list Opening/Start Time only.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Days</div><div class="col-sm-3">Opening/Start Time</div><div class="col-sm-3">Closing/End Time</div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-3 pull-right">
                            <div class="col-sm-6">Open</div>
                            <div class="col-sm-6">Open All Day</div>
                        </div>
                    </div>
                    <br>   
                        <?php
                            foreach ($open_hours['hours'] as $key => $value)
                            {
                                if(is_array($value))
                                {
                        ?>          <div class="row">
                                        <div class="col-sm-2"><?= ucfirst($key) ?></div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="input-icon">
                                                    <i class="fa fa-clock-o"></i>
                                                    <input type="text" id="<?= substr($key, 0,3) ?>_opening" value="<?= $value['opening'][0] ?>" name="DestinationsOpenHoursDays[<?= $key ?>][opening][]" class="form-control" <?= $timeDisabled[$value['state']] ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="input-icon">
                                                    <i class="fa fa-clock-o"></i>
                                                    <input type="text" id="<?= substr($key, 0,3) ?>_closing" value="<?= $value['closing'][0] ?>" name="DestinationsOpenHoursDays[<?= $key ?>][closing][]" class="form-control" <?= $timeDisabled[$value['state']] ?>> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-1" style="margin-top:7px;" id="<?= substr($key, 0,3) ?>_add_dynamic_field">
                                            <a href="javascript:void(0)" ><i class="fa fa-plus fa-2x add-dynamic-field" data-id="<?= substr($key, 0,3) ?>" aria-hidden="true"></i></a>
                                        </div>

                                        <div class="col-sm-3 pull-right" style="margin-top:5px;">
                                            <div class="col-sm-6">
                                                <input type="checkbox" data-id="<?= substr($key, 0,3) ?>" id="<?= substr($key, 0,3) ?>_open_switch" class="make-switch open-switch" name="DestinationsOpenHoursDays[<?= $key ?>][state]" <?= $statusCheckbox[$value['state']] ?>  data-on-color="success" data-off-color="warning" data-on-text="Yes" data-off-text="No" data-size="mini">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="checkbox" data-id="<?= substr($key, 0,3) ?>" id="<?= substr($key, 0,3) ?>_hours_24_switch" class="make-switch hours-24" name="DestinationsOpenHoursDays[<?= $key ?>][hours_24]" <?= $statusCheckbox[$value['hours_24']] ?> data-on-color="success" data-off-color="warning" data-on-text="Yes" data-off-text="No" data-size="mini">
                                            </div>
                                        </div>

                                        <?php
                                            $k = substr($key, 0,3);

                                            if($value['state']==0)
                                            {
                                                $this->registerJs("
                                                    $('#'+'$k'+'_hours_24_switch').bootstrapSwitch('disabled',true);
                                                    $('#'+'$k'+'_add_dynamic_field').hide();

                                                    $('#'+'$k'+'_opening').attr('disabled',true);
                                                    $('#'+'$k'+'_closing').attr('disabled',true);

                                                    $('#'+'$k'+'_opening').val('00:00');
                                                    $('#'+'$k'+'_closing').val('00:00');
                                                ");
                                            }

                                            if($value['hours_24']==1)
                                            {
                                                $this->registerJs("
                                                    $('#'+'$k'+'_open_switch').bootstrapSwitch('disabled',true);
                                                    $('#'+'$k'+'_add_dynamic_field').hide();

                                                    $('#'+'$k'+'_opening').attr('disabled',true);
                                                    $('#'+'$k'+'_closing').attr('disabled',true);

                                                    $('#'+'$k'+'_opening').val('00:00');
                                                    $('#'+'$k'+'_closing').val('00:00');
                                                ");
                                            }

                                        ?>
                                           
                                    </div>
                                    <div class="row" id="<?= substr($key, 0,3) ?>_split_div">
                                        <?php
                                            for ($i=1; $i < count($value['opening']) ; $i++) 
                                            {
                                        ?>      
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-clock-o"></i>
                                                                    <input type="text" value="<?= $value['opening'][$i] ?>" name="DestinationsOpenHoursDays[<?= $key ?>][opening][]" class="form-control server-clock">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <div class="input-icon">
                                                                    <i class="fa fa-clock-o"></i>
                                                                    <input type="text"  value="<?= $value['closing'][$i] ?>" name="DestinationsOpenHoursDays[<?= $key ?>][closing][]" class="form-control server-clock" > 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">
                                                            <a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php
                                            }
                                        ?>
                                    
                                    </div> 
                        <?php     
                                }
                            }
                        ?>
                </div>
            </div>
        </div>
        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2"> Link to where your hours are displayed</a>
                </h4>
            </div>
            <div id="collapse_3_2" class="panel-collapse in">
                <div class="panel-body" style=" overflow-y:auto;">
                    <?= $form->field($provider_model, 'url') ?>
                </div>
            </div>
        </div>
    <!--</div>

</div>-->

<?php
$this->registerJs("
$(document).ready(function() 
{
    var days = ['mon','tue','wed','thu','fri','sat','sun'];

    // *************** add clock face on time fields

    for (var i=0; i<days.length ; i++) 
    { 
        var temp = days[i];
        $('#'+temp+'_opening').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });   
     
        $('#'+temp+'_opening').click(function(e){   
            $(this).clockface('toggle');
        });

        $('#'+temp+'_closing').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });   
     
        $('#'+temp+'_closing').click(function(e){   
            $(this).clockface('toggle');
        });  
    }

    $('.open-switch').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        var key = '#'+$(this).attr('data-id');

        if(state)
        {
            $(key+'_opening').attr('disabled',false);
            $(key+'_closing').attr('disabled',false);

            $(key+'_opening').val('00:00');
            $(key+'_closing').val('00:00');

            $(key+'_split_div').show();
            $(key+'_add_dynamic_field').show();

            $(key+'_hours_24_switch').bootstrapSwitch('disabled',false);
        }
        else
        {
            $(key+'_opening').attr('disabled',true);
            $(key+'_closing').attr('disabled',true);
            
            $(key+'_opening').val('00:00');
            $(key+'_closing').val('00:00');

            $(key+'_split_div').hide();
            $(key+'_add_dynamic_field').hide();

            $(key+'_hours_24_switch').bootstrapSwitch('state',false);
            $(key+'_hours_24_switch').bootstrapSwitch('disabled',true);
        }
    });

    $('.hours-24').on('switchChange.bootstrapSwitch', function(event, state) 
    {
        var key = '#'+$(this).attr('data-id');

        if(state)
        {
            $(key+'_opening').attr('disabled',true);
            $(key+'_closing').attr('disabled',true);

            $(key+'_opening').val('00:00');
            $(key+'_closing').val('00:00');

            $(key+'_open_switch').bootstrapSwitch('state',true);
            $(key+'_open_switch').bootstrapSwitch('disabled',true);

            $(key+'_split_div').hide();
            $(key+'_add_dynamic_field').hide();
        }
        else
        {
            $(key+'_opening').attr('disabled',false);
            $(key+'_closing').attr('disabled',false);

            $(key+'_opening').val('00:00');
            $(key+'_closing').val('00:00');

            $(key+'_open_switch').bootstrapSwitch('disabled',false);

            $(key+'_split_div').show();
            $(key+'_add_dynamic_field').show();
        }
    });

    $('.add-dynamic-field').on('click',function()
    {
        var key = $(this).attr('data-id');
        var htmlCode = '';

        switch (key) 
        {
            case 'mon':
               htmlCode = '".DestinationsOpenHoursDays::getMondayHtml()."';
               break;

            case 'tue':
               htmlCode = '".DestinationsOpenHoursDays::getTuesdayHtml()."';
               break;

            case 'wed':
               htmlCode = '".DestinationsOpenHoursDays::getWednesdayHtml()."';
               break;

            case 'thu':
               htmlCode = '".DestinationsOpenHoursDays::getThursdayHtml()."';
               break;

            case 'fri':
               htmlCode = '".DestinationsOpenHoursDays::getFridayHtml()."';
               break;

            case 'sat':
               htmlCode = '".DestinationsOpenHoursDays::getSaturdayHtml()."';
               break;

            case 'sun':
               htmlCode = '".DestinationsOpenHoursDays::getSundayHtml()."';
               break;

        }

        $('#'+key+'_split_div').append(htmlCode);

        $('.dynamic-clock').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });   
     
        $('.dynamic-clock').click(function(e){   
            $(this).clockface('toggle');
        });

        $('.dynamic-clock').addClass(key+'-dynamic-clock');
        $('.dynamic-clock').removeClass('dynamic-clock');
    });

    $('.row').on('click','.delete-dynamic-field',function(event)
    {
        $('.clockface').hide(); 
        $(this).closest('.row').remove();
    });

    // ********** Apply clock on run time created fields row

    $('.server-clock').clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });   
 
    $('.server-clock').click(function(e){   
        $(this).clockface('toggle');
    });

    $('.server-clock').removeClass('.server-clock');
 
});
", View::POS_END);
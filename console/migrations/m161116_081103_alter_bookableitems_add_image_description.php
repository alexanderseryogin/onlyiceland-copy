<?php

use yii\db\Migration;

class m161116_081103_alter_bookableitems_add_image_description extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `featured_image` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `include_in_groups`, ADD COLUMN `description` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `featured_image`; 
        ");
    }

    public function down()
    {
        echo "m161116_081103_alter_bookableitems_add_image_description cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m160911_145714_ADD_TABLE_PROPERTIES extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `properties`( `id` INT(11) NOT NULL AUTO_INCREMENT, `region_id` INT(11) NOT NULL, `owner_id` INT(11) NOT NULL, `property_type_id` INT(11) NOT NULL, `name` VARCHAR(1024) NOT NULL, `address` TEXT NOT NULL, `city_id` INT(11) NOT NULL, `postal_code` VARCHAR(256) NOT NULL, `latitude` VARCHAR(256) NOT NULL, `longitude` VARCHAR(256) NOT NULL, `reservations_email` VARCHAR(512) NOT NULL, `business_name` VARCHAR(512), `business_id_number` VARCHAR(256) NOT NULL, `business_address` TEXT NOT NULL, `business_city_id` INT(11) NOT NULL, `business_postal_code` VARCHAR(256) NOT NULL, `invoice_email` VARCHAR(512) NOT NULL, `youngest_age` INT(11), `payment_methods_for_gauarantee` TEXT, `payment_methods_checkin` TEXT, PRIMARY KEY (`id`) ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;");
        
        //all fks here should be null 
        $this->execute("ALTER TABLE `properties` CHANGE `region_id` `region_id` INT(11) NULL, CHANGE `owner_id` `owner_id` INT(11) NULL, CHANGE `property_type_id` `property_type_id` INT(11) NULL, CHANGE `city_id` `city_id` INT(11) NULL, CHANGE `business_city_id` `business_city_id` INT(11) NULL; ");
        
        $this->execute("ALTER TABLE `properties` ADD CONSTRAINT `FK_properties_region_id` FOREIGN KEY (`region_id`) REFERENCES `regions`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_user_id` FOREIGN KEY (`owner_id`) REFERENCES `user`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_property_type_id` FOREIGN KEY (`property_type_id`) REFERENCES `property_types`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL, ADD CONSTRAINT `FK_properties_business_city_id` FOREIGN KEY (`business_city_id`) REFERENCES `cities`(`id`) ON DELETE SET NULL; ");
        
    }

    public function down()
    {
        echo "m160911_145714_ADD_TABLE_PROPERTIES cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

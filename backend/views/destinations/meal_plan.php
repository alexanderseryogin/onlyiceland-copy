<?php

use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationsMealPlan;
?>
<style type="text/css">
    .less-space
    {
        margin-top: -30px;
    }
</style>

<div class="tab-pane" id="meal_plan">

    <!-- BEGIN ACCORDION PORTLET-->

    <div class="panel-group accordion" id="accordion_meal_plan">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_meal_plan" href="#collapse_pfo">Primary Food Options</a>
                </h4>
            </div>
            <div id="collapse_pfo" class="panel-collapse collapse in">
                <div class="panel-body" style=" overflow-y:auto;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="mt-radio-inline">
                                <div style="font-size: 15px;">Breakfast</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['pfo_breakfast'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[pfo_breakfast]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[pfo_breakfast]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="mt-radio-inline less-space">
                                <div style="font-size: 15px;">Lunch</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['pfo_lunch'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[pfo_lunch]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[pfo_lunch]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="mt-radio-inline less-space">
                                <div style="font-size: 15px;">Dinner</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['pfo_dinner'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[pfo_dinner]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[pfo_dinner]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_meal_plan" href="#collapse_sfo">Supplementary Food Options</a>
                </h4>
            </div>
            <div id="collapse_sfo" class="panel-collapse collapse in">
                <div class="panel-body" style=" overflow-y:auto;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="mt-radio-inline">
                                    <div style="font-size: 15px;">Morning Snack</div>
                                    <?php
                                        foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                        {
                                            if($key==$meal_options['sfo_morning_snack'])
                                            {
                                                echo '<label class="mt-radio mt-radio-outline">
                                                    <input type="radio" checked name="DestinationsMealPlan[sfo_morning_snack]" value="'.$key.'" > '.$value.'
                                                    <span></span>
                                                </label>';
                                            }
                                            else
                                            {
                                                echo '<label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="DestinationsMealPlan[sfo_morning_snack]" value="'.$key.'" > '.$value.'
                                                    <span></span>
                                                </label>';
                                            }
                                        }
                                    ?>
                                </div>

                                <div class="mt-radio-inline less-space">
                                    <div style="font-size: 15px;">Afternoon Snack</div>
                                    <?php
                                        foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                        {
                                            if($key==$meal_options['sfo_afternoon_snack'])
                                            {
                                                echo '<label class="mt-radio mt-radio-outline">
                                                    <input type="radio" checked name="DestinationsMealPlan[sfo_afternoon_snack]" value="'.$key.'" > '.$value.'
                                                    <span></span>
                                                </label>';
                                            }
                                            else
                                            {
                                                echo '<label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="DestinationsMealPlan[sfo_afternoon_snack]" value="'.$key.'" > '.$value.'
                                                    <span></span>
                                                </label>';
                                            }
                                        }
                                    ?>
                                </div>

                                <div class="mt-radio-inline less-space">
                                    <div style="font-size: 15px;">Evening Snack</div>
                                    <?php
                                        foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                        {
                                            if($key==$meal_options['sfo_evening_snack'])
                                            {
                                                echo '<label class="mt-radio mt-radio-outline">
                                                    <input type="radio" checked name="DestinationsMealPlan[sfo_evening_snack]" value="'.$key.'" > '.$value.'
                                                    <span></span>
                                                </label>';
                                            }
                                            else
                                            {
                                                echo '<label class="mt-radio mt-radio-outline">
                                                    <input type="radio" name="DestinationsMealPlan[sfo_evening_snack]" value="'.$key.'" > '.$value.'
                                                    <span></span>
                                                </label>';
                                            }
                                        }
                                    ?>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
        </div>

        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_meal_plan" href="#collapse_abo">All Beverage Options </a>
                </h4>
            </div>
            <div id="collapse_abo" class="panel-collapse collapse in">
                <div class="panel-body" style=" overflow-y:auto;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="mt-radio-inline">
                                <div style="font-size: 15px;">Coffee/Tea</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['abo_tea'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[abo_tea]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[abo_tea]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="mt-radio-inline less-space">
                                <div style="font-size: 15px;">Fountain/Bottled Soft Drinks</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['abo_fountain'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[abo_fountain]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[abo_fountain]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="mt-radio-inline less-space">
                                <div style="font-size: 15px;">Beer/Wine</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['abo_beer'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[abo_beer]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[abo_beer]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="mt-radio-inline less-space">
                                <div style="font-size: 15px;">Liquor</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['abo_liquor'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[abo_liquor]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[abo_liquor]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                            <div class="mt-radio-inline less-space">
                                <div style="font-size: 15px;">In-room Minibar</div>
                                <?php
                                    foreach (DestinationsMealPlan::$meal_options_arr as $key => $value) 
                                    {
                                        if($key==$meal_options['abo_minibar'])
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" checked name="DestinationsMealPlan[abo_minibar]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                        else
                                        {
                                            echo '<label class="mt-radio mt-radio-outline">
                                                <input type="radio" name="DestinationsMealPlan[abo_minibar]" value="'.$key.'" > '.$value.'
                                                <span></span>
                                            </label>';
                                        }
                                    }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</div>
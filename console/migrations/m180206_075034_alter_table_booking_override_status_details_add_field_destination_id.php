<?php

use yii\db\Migration;

class m180206_075034_alter_table_booking_override_status_details_add_field_destination_id extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `booking_override_status_details`   
                          ADD COLUMN `destination_id` INT(11) NULL AFTER `date`;
                        ');
    }

    public function safeDown()
    {
        echo "m180206_075034_alter_table_booking_override_status_details_add_field_destination_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180206_075034_alter_table_booking_override_status_details_add_field_destination_id cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m161213_090256_alter_rates_add_cancellation extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `rates` ADD COLUMN `general_booking_cancellation` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `min_price`; 
        ");
    }

    public function down()
    {
        echo "m161213_090256_alter_rates_add_cancellation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

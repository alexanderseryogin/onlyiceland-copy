<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Rates */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rates-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'provider_id',
            'booking_confirmation_type_id',
            'unit_id',
            'rate_period',
            'item_price_first_adult',
            'item_price_additional_adult',
            'item_price_first_child',
            'item_price_additional_child',
            'first_night_rate',
            'last_night_rate',
            'min_max_booking:ntext',
            'rate_allowed:ntext',
            'check_in_allowed:ntext',
            'check_out_allowed:ntext',
            'min_max_stay:ntext',
            'booking_status_id',
            'booking_flag_id',
        ],
    ]) ?>

</div>

<?php
use common\models\BookingDates;

?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
          <h3><b>Guests Scheduled to Arrive Today</b></h3>
    </div>
  
</div>
<br>
<table class="table table-hover">
    <thead>
      <tr>
        <th style="width: 20%">Unit Name</th>
        <th style="width: 16%">Adults</th>
        <th style="width: 16%">Children</th>
        <th style="width: 16%">Arrival</th>
        <th style="width: 16%">Departure</th>
        <th style="width: 16%">Nights</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        if(!empty($items)){
        foreach ($items as $item) {
          $booking_date = BookingDates::findOne(['booking_item_id' => $item->id,'date' => $item->arrival_date]);
      ?>
        <tr>
          <!-- <td>
            <?php echo $item->id; ?>
          </td> -->
          <td>
             <?php
              
              if(!empty($item->item_name_id)){
                    echo $item->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
                }
              else
                  echo '- - - - -';
             ?>
            
          </td>
          <td>
            <?php
              if(!empty($booking_date->no_of_adults))
                  echo $booking_date->no_of_adults;
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
              if(!empty($booking_date->no_of_children))
                  echo $booking_date->no_of_children;
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
              if(!empty($item->arrival_date))
                  echo date('d-m-Y',strtotime($item->arrival_date));
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
              if(!empty($item->departure_date))
                  echo date('d-m-Y',strtotime($item->departure_date));
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
                $date1=date_create($item->departure_date);
                $date2=date_create($item->arrival_date);
                $diff=date_diff($date1,$date2);
                $difference =  $diff->format("%a");
                echo $difference;
            ?>
          </td>
        </tr>
        <?php if(!empty($item->comments) && $item->comments!=NULL) { ?>
        <tr style="border-top: none !important;">
          <td colspan="6" style="border-top: none !important;"><?php echo '<i>"'.$item->comments.'"</i>';?></td>
        </tr>
        <?php } ?>
      <?php } }
        else {
      ?>
      <tr>
        <td colspan="3">No guests are scheduled to arrive today</td>
      </tr>
      <?php
        }
      ?>
    </tbody>
</table>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
          <h3><b>Guests Scheduled to Stay Over</b></h3>
    </div>
  
</div>
<br>
<table class="table table-hover">
    <thead>
      <tr>
        <th style="width: 20%">Unit Name</th>
        <th style="width: 16%">Adults</th>
        <th style="width: 16%">Children</th>
        <th style="width: 16%">Arrival</th>
        <th style="width: 16%">Departure</th>
        <th style="width: 16%">Nights</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        if(!empty($older_items)){
        foreach ($older_items as $item) {
          //$booking_date = BookingDates::findOne(['booking_item_id' => $item->id,'date' => $item->arrival_date]);
      ?>

       <tr >
          <!-- <td>
            <?php echo $item->id; ?>
          </td> -->
          <td>
             <?php
              
              if(!empty($item->item_name_id)){
                    echo $item->itemName->item_name.(isset($booking_date->bedsCombinations->short_code)?': '.$booking_date->bedsCombinations->short_code:'');
                }
              else
                  echo '- - - - -';
             ?>
            
          </td>
          <td>
            <?php
              if(!empty($booking_date->no_of_adults))
                  echo $booking_date->no_of_adults;
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
              if(!empty($booking_date->no_of_children))
                  echo $booking_date->no_of_children;
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
              if(!empty($item->arrival_date))
                  echo date('d-m-Y',strtotime($item->arrival_date));
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
              if(!empty($item->departure_date))
                  echo date('d-m-Y',strtotime($item->departure_date));
              else
                  echo '0';
            ?>
          </td>
          <td>
            <?php
                $date1=date_create($item->departure_date);
                $date2=date_create($item->arrival_date);
                $diff=date_diff($date1,$date2);
                $difference =  $diff->format("%a");
                echo $difference;
            ?>
          </td>
        </tr>
        <?php if(!empty($item->comments) && $item->comments!=NULL) { ?>
        <tr style="border-top: none !important; ">
          <td colspan="6" style="border-top: none !important;><?php echo '<i>"'.$item->comments.'"</i>'?></td>
        </tr>
        <?php } ?>
      <?php } }
        else {
      ?>
      <tr>
        <td colspan="3">No guests are scheduled to stay over</td>
      </tr>
      <?php
        }
      ?>
    </tbody>
</table>
<?php

use yii\db\Migration;

class m170810_083226_create_table_bookable_item_housekeeping_status extends Migration
{
    public function up()
    {
        $this->execute('CREATE TABLE `bookable_items_housekeeping_status`(
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `bookable_item_id` INT(11),
                          `housekeeping_status_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`bookable_item_id`) REFERENCES `bookable_items`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`housekeeping_status_id`) REFERENCES `housekeeping_status`(`id`) ON DELETE SET NULL
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170810_083226_create_table_bookable_item_housekeeping_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170728_140018_create_table_destination_add_ons extends Migration
{
    public function up()
    {   
        $this->execute('CREATE TABLE `destination_add_ons`(  
                          `id` INT(11) NOT NULL,
                          `destination_id` INT(11),
                          `add_on_id` INT(11),
                          `name` VARCHAR(256),
                          `description` VARCHAR(256),
                          `type` INT(11),
                          `discountable` INT(11),
                          `price` DOUBLE,
                          `vat` DOUBLE,
                          `tax` DOUBLE,
                          `fee` DOUBLE,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`destination_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE,
                          FOREIGN KEY (`add_on_id`) REFERENCES `add_on`(`id`) ON UPDATE SET NULL ON DELETE SET NULL
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function down()
    {
        echo "m170728_140018_create_table_destination_add_ons cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

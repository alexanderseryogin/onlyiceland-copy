<?php
	use common\models\Rates;

	if($sum > 0)
	{
		echo '<div class="row">';
		for ($i=1; $i <= $sum ; $i++) 
		{
			if($i>= 1 && $i<=4)
			{
				$word =  Rates::$numberWords[$i-1];
			}
			else
				$word = $i.' Persons';

			echo '<div class="col-xs-2">
	                <label class=" control-label">'.$word.'</label>
	                <input type="text" class="form-control first-additional-prices" >
	                <input type="text" name="Rates[capacity-pricing][]" hidden>
	            </div>';
		}	
		echo '</div>';
	}
	else
	{
		echo '<div class="capacity-error alert-danger alert fade in">No Bed Types Found</div>';
	}
		
?>
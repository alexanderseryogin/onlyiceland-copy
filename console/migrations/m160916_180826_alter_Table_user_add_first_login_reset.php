<?php

use yii\db\Migration;

class m160916_180826_alter_Table_user_add_first_login_reset extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `user` ADD COLUMN `first_login_reset_flag` TINYINT(1) DEFAULT 0 NULL AFTER `status`;");
    }

    public function down()
    {
        echo "m160916_180826_alter_Table_user_add_first_login_reset cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

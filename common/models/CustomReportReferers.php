<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "custom_reports_referers".
 *
 * @property integer $id
 * @property integer $travel_partner_id
 * @property integer $custom_report_id
 *
 * @property CustomReports $customReport
 */
class CustomReportReferers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_reports_referers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['travel_partner_id', 'custom_report_id'], 'integer'],
            [['custom_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomReports::className(), 'targetAttribute' => ['custom_report_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'travel_partner_id' => 'Travel Partner ID',
            'custom_report_id' => 'Custom Report ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomReport()
    {
        return $this->hasOne(CustomReports::className(), ['id' => 'custom_report_id']);
    }

    public function getDestinationTravelPartner()
    {
        return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    }
}

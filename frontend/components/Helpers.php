<?php
namespace frontend\components;

use common\models\User;
use common\models\UserProfile;

class Helpers
{

    /**
    * status html or lables
    **/
    public static function ProfileTitleTypes($type){
        
        $html = '';

        if(isset($type)){
            switch($type){
                case UserProfile::TITLE_MR:
                    $html = 'Mr.';
                break;
                case UserProfile::TITLE_MS:
                    $html = 'Ms.';
                break;
                case UserProfile::TITLE_MRS:
                    $html = 'Mrs.';
                break; 
                case UserProfile::TITLE_DR:
                    $html = 'Dr.';
                break;  
            }
        }
    
        return $html;
    }
}
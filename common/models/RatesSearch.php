<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rates;

/**
 * RatesSearch represents the model behind the search form about `common\models\Rates`.
 */
class RatesSearch extends Rates
{
    public $from_to_period;
    public $from;
    public $to;
    public $expired_rates;
    public $current_rates;
    public $future_rates;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_id', 'booking_confirmation_type_id', 'unit_id', 'booking_status_id', 'booking_flag_id'], 'integer'],
            [['name', 'rate_period', 'item_price_first_adult', 'item_price_additional_adult', 'item_price_first_child', 'item_price_additional_child', 'min_max_booking', 'rate_allowed', 'check_in_allowed', 'check_out_allowed', 'min_max_stay','min_price','expired_rates','current_rates','future_rates'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // echo "<pre>";
        // print_r($params);
        // echo substr($params['sort'],1);
        // exit();
        $query = Rates::find()->orderBy('provider_id ASC');
        // add conditions that should always apply here
        $sort = '';
        
        if(isset($params['sort']))
        {

            $sort_column;
            if (strpos($params['sort'], '-') > -1)
            {
                $sort_column = substr($params['sort'],1);
            }
            else
            {
                $sort_column = $params['sort'];
            }
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        $sort_column => (strpos($params['sort'],'-')>-1)?SORT_DESC:SORT_ASC,
                    ],
                ]
            ]);
        }
        else
        {
            $dataProvider = new ActiveDataProvider([
            'query' => $query,
                // 'sort' => ['defaultOrder'=>'name asc']
                // 'pagination' => [
                //     'page' => isset($params['page'])?($params['page']-1):0,
                // ],
            ]);    
        }
        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($params['RatesSearch']['from_to_period']))
        {
            $arr = explode('-', $params['RatesSearch']['from_to_period']);
            $from = strtotime(str_replace('/', '-', $arr[0]));
            $to = strtotime(str_replace('/', '-', $arr[1]));
        }
        else
        {
            $from ='';
            $to='';
        }

        $current_date = date('Y-m-d',time());
        // echo "<pre>";
        // print_r($current_date);
        // exit(); 

        // grid filtering conditions
        // echo "<pre>";
        // print_r($query->all());
        // exit();
        if(!isset($params['expired_rates']) &&  !isset($params['future_rates']) && !isset($params['current_rates']) )
        {
            $query->andFilterWhere(['id' => 0]);
        }
        else
        {
            if(isset($params['expired_rates']))
            {
                $query->orFilterWhere(['<','to',$current_date]);
            }
            if(isset($params['current_rates']))
            {
                // $query->orFilterWhere(['between', 'to', 'from', $current_date]);
                $query->orFilterWhere(['and',
                                        ['>','to',$current_date],
                                        ['<','from',$current_date]
                                    ]); 
                // $query->orFilterWhere(['<=','to',$current_date]);
                // $query->andFilterWhere(['>=','from',$current_date]); 
            }
            if(isset($params['future_rates']))
            {
                // echo "here";
                // exit();
                $query->orFilterWhere(['>=','from',$current_date]);
            }    
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'booking_confirmation_type_id' => $this->booking_confirmation_type_id,
            'unit_id' => $this->unit_id,
            'booking_status_id' => $this->booking_status_id,
            'booking_flag_id' => $this->booking_flag_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'rate_period', $this->rate_period])
            ->andFilterWhere(['like', 'item_price_first_adult', $this->item_price_first_adult])
            ->andFilterWhere(['like', 'item_price_additional_adult', $this->item_price_additional_adult])
            ->andFilterWhere(['like', 'item_price_first_child', $this->item_price_first_child])
            ->andFilterWhere(['like', 'item_price_additional_child', $this->item_price_additional_child])
            ->andFilterWhere(['like', 'min_price', $this->item_price_first_adult])
            ->andFilterWhere(['like', 'min_max_booking', $this->min_max_booking])
            ->andFilterWhere(['like', 'rate_allowed', $this->rate_allowed])
            ->andFilterWhere(['like', 'check_in_allowed', $this->check_in_allowed])
            ->andFilterWhere(['like', 'check_out_allowed', $this->check_out_allowed])
            ->andFilterWhere(['like', 'min_max_stay', $this->min_max_stay]);

        $query->andFilterWhere(['not like', 'name', 'Imported From CSV']);
        
        return $dataProvider;
    }

    public function searchMultiNight($params)
    {
        $query = RatesMultiNightDiscount::find()->where(['rates_id' => $params])->orderBy('nights ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

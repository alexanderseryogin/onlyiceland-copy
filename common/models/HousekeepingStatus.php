<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "housekeeping_status".
 *
 * @property integer $id
 * @property string $label
 * @property string $abbreviation
 * @property string $background_color
 * @property string $text-color
 */
class HousekeepingStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $temp_icon='';

    public static function tableName()
    {
        return 'housekeeping_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['label', 'abbreviation', 'background_color', 'text_color'], 'string', 'max' => 256],
            ['temp_icon', 'image','skipOnEmpty' => true, 'extensions' => 'png, jpg',
                'maxWidth' => 50,
                'maxHeight' => 50,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'abbreviation' => 'Abbreviation',
            'background_color' => 'Background Color',
            'text_color' => 'Text Color',
            'icon' => Yii::t('app', 'Icon'),
            'temp_icon' => Yii::t('app', 'Upload an Icon'),
        ];
    }
}

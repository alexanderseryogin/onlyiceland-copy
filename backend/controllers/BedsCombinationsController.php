<?php

namespace backend\controllers;

use Yii;
use common\models\BedsCombinations;
use common\models\BedsCombinationsSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\UploadFile;
use yii\web\UploadedFile;
/**
 * BedsCombinationsController implements the CRUD actions for BedsCombinations model.
 */
class BedsCombinationsController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Amenities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BedsCombinationsSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['BedsCombinationsSearch']) && !$this->isArrayEmpty($params['BedsCombinationsSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BedsCombinations model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BedsCombinations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BedsCombinations();

        if($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->save();
            
            if(empty($model->getErrors()))
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'temp_icon')) 
                {
                    if($uploadFile->uploadFile('beds-combinations',$model->id,'icons'))
                    {
                        $model->icon = $uploadFile->FileName;
                        $model->update();
                    }
                }
            }
            
            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BedsCombinations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->save();

            if(empty($model->getErrors()))
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'temp_icon')) 
                {
                    if($uploadFile->uploadFile('beds-combinations',$model->id,'icons'))
                    {
                        if(!empty($model->icon))
                        {
                            $prev_image = Yii::getAlias('@app').'/../uploads/beds-combinations/'.$model->id.'/icons/'.$model->icon;
                            unlink($prev_image);
                        }

                        $model->icon = $uploadFile->FileName;
                        $model->update();
                    }
                }
            }
            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BedsCombinations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BedsCombinations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return BedsCombinations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BedsCombinations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

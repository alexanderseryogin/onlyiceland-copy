<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Attractions;
use common\models\AttractionsImages;
/**
 * AttractionsSearch represents the model behind the search form about `common\models\Attractions`.
 */
class AttractionsSearch extends Attractions
{
    public $city_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'attr_type_id', 'state_id', 'city_id'], 'integer'],
            [['name', 'description', 'address', 'phone', 'price','city_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attractions::find()->joinWith('city');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['city_name'] = [
        'asc' => ['cities.name' => SORT_ASC], //tableName.attributeName
        'desc' => ['cities.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'attr_type_id' => $this->attr_type_id,
            'state_id' => $this->state_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'cities.name', $this->city_name]);

        return $dataProvider;
    }

    public function searchImages($params)
    {
        $query = AttractionsImages::find()->where(['attraction_id' => $params])->orderBy('display_order ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //$query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}

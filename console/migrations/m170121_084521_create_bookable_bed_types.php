<?php

use yii\db\Migration;

class m170121_084521_create_bookable_bed_types extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `bookable_bed_types`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `bookable_id` INT(11), `bed_type_id` INT(11) UNSIGNED, `quantity` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_bookable_bed_types_bed_type_id` FOREIGN KEY (`bed_type_id`) REFERENCES `bed_types`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `FK_bookable_bed_types_bookable_id` FOREIGN KEY (`bookable_id`) REFERENCES `bookable_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m170121_084521_create_bookable_bed_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

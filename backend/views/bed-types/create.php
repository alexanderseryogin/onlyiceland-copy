<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BedTypes */

$this->title = Yii::t('app', 'Bed Types');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bed Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bed-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

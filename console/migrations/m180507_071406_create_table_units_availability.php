<?php

use yii\db\Migration;

class m180507_071406_create_table_units_availability extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `unit_availability`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `bookable_item_id` INT(11),
                          `unit_id` INT(11) UNSIGNED,
                          `date` DATE,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`bookable_item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
                          FOREIGN KEY (`unit_id`) REFERENCES `bookable_items_names`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180507_071406_create_table_units_availability cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180507_071406_create_table_units_availability cannot be reverted.\n";

        return false;
    }
    */
}

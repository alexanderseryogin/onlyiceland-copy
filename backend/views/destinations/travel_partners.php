<?php
    use common\models\TravelPartner;
    use yii\web\View;
    use yii\helpers\Url;

    $Checkbox = [
        0 => '',
        1 => 'checked'
    ];
    $isChecked = 0;

?>

<?php

            // echo "<pre>";
            // print_r($destination_travel_partners);
            // exit();
            
    ?>

<style type="text/css">
.checkbox, .form-horizontal .checkbox {
    padding: 20px;
}
    .align-center {
        text-align: center;
    }
    table tr th {
        text-align: center;
    }
    .free{background: #DFF0D8;}
    .extra{background: #FCF8E3}
    .not_available{background: #FFEB89}
    .not_display{background: #F2DEDE}
    .table-scrollable {border: none !important; overflow-x: hidden !important;}
    .dataTables_scrollBody {overflow-x: hidden !important;}
    table.dataTable thead .sorting_desc {
        background-image: none;
    }
    table.dataTable thead .sorting_asc {
        background-image: none;
    }
</style>

<!-- BEGIN ACCORDION PORTLET-->

<div class="tab-pane" id="travel_partners">
    <table class="table table-striped table-bordered table-hover order-column" id="destination_travel_partners">
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Comission %</th>
                <th>Booking Email</th>
                <th>Invoice Email</th>
                <th>Primary Contact</th>
                <th>Billing Contact</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $travel_partners = TravelPartner::find()->all();
                $i=0;
                $comission = 0;
                $primary_contact = '';
                $billing_contact = '';
                $booking_email = '';
                $invoice_email = '';
                $notes = '';
                if(!empty($travel_partners))
                {
                    foreach ($travel_partners as $key => $value) 
                    {
                        if($provider_model->isNewRecord )
                        {
                            $primary_contact = $value->primary_contact;
                            $billing_contact = $value->billing_contact;
                            $booking_email   = $value->booking_email;
                            $invoice_email   = $value->invoice_email;
                        }
                        else
                        {
                            if(isset($destination_travel_partners) && isset($destination_travel_partners[$value->id] ) )
                            {
                                // echo "here";
                                // exit();
                                $comission = $destination_travel_partners[$value->id]['comission'];
                                $primary_contact = $destination_travel_partners[$value->id]['primary_contact'];
                                $billing_contact = $destination_travel_partners[$value->id]['billing_contact'];
                                $booking_email   = $destination_travel_partners[$value->id]['booking_email_address'];
                                $invoice_email   = $destination_travel_partners[$value->id]['invoice_email_address'];
                                $notes = $destination_travel_partners[$value->id]['notes'];
                                $isChecked = 1;
                            }
                            else
                            {
                                // echo "<pre>";
                                // print_r($destination_travel_partners);
                                // exit();
                                $primary_contact = $value->primary_contact;
                                $billing_contact = $value->billing_contact;
                                $booking_email   = $value->booking_email;
                                $invoice_email   = $value->invoice_email;
                                $notes = '';
                                $isChecked = 0;
                                $comission = 0;
                            }
                        }

                        echo '<tr>';
                        echo '<td class="align-center"><label><input class="checkbox-'.$value->id.'" name="Destinations[travel_partners_checkbox][]" '.$Checkbox[$isChecked].' value="'.$value->id.'" type="checkbox"></label></td>';

                        echo '<td>'.$value['company_name'].'</td>';
                        echo '<td>
                        <input type="text" class="form-control travel_partners_comission " value="'.$comission.'">
                        <input type="text" class="travel_partners_comission-'.$value->id.'" name="Destinations[travel_partners_details]['.$value->id.'][comission]" value="'.$comission.'" hidden>
                        </td>

                        <td>
                        <input type="text" class="form-control booking_email-'.$value->id.'" name="Destinations[travel_partners_details]['.$value->id.'][booking_email]" value="'.$booking_email.'" >
                        </td><td>

                        <input type="text" class="form-control invoice_email-'.$value->id.'" name="Destinations[travel_partners_details]['.$value->id.'][invoice_email]" value="'.$invoice_email.'" >
                        </td><td>
                        <input type="text" class="form-control primary_contact-'.$value->id.'" name="Destinations[travel_partners_details]['.$value->id.'][primary_contact]" value="'.$primary_contact.'" >
                       
                        
                        </td>
                        <td>
                        <input type="text" class="form-control billing_contact-'.$value->id.'" name="Destinations[travel_partners_details]['.$value->id.'][billing_contact]" value="'.$billing_contact.'" >
                       
                        
                        </td>
                        <td><a href="javascript:void(0)" class="show_notes"><i class="fa fa-file-text" aria-hidden="true"></i></a>
                            <textarea id="'.$value->id.'" name="Destinations[travel_partners_details]['.$value->id.'][notes]" style="display:none;" >'.$notes.'</textarea>
                        </td>';

                        
                       
                        echo '</tr>';
                        $i++;
                    }
                }
                
            ?>
        </tbody>
    </table>
</div>

<div id="group-modal" class="modal fade bs-modal-lg" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> -->
                <h4 class="modal-title">Add Notes</h4>
            </div>
            <div class="modal-body">
                    <div class="alert alert-info">Note: You may edit notes of multiple selected travel partners at same time, update/save destination to save all permanently</div>
                <!--  -->
                <textarea id="notes" rows=4></textarea>
                <!-- #######  #######-->
            </div>
            <div class="modal-footer">
               <!--  <button id="save-notes" type="button" data-dismiss="modal" class="btn btn-primary">Update & Close</button> -->
                <button id="save-notes" type="button" class="btn btn-primary">Update & Close</button>
            </div>
        </div>
    </div>
</div>



<?php
$add_destination_travel_partner = Url::to(['/destinations/add-travel-partner']);
$destination_id = (isset($provider_model->id)) ? $provider_model->id : 0;
// echo "here";
// exit();
$this->registerJs("


    // alert('here');

    $('#notes').summernote({height:300,dialogsInBody: true,toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize',['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]});
    $('.dropdown-toggle').dropdown()
    $(document).on('click','.show_notes',function()
    {   
        console.log('clicked')
        $('#notes').summernote('reset');
        $('.dropdown-toggle').dropdown()
        var notes = $(this).next().text();
        console.log($(this).next());
        console.log('notes: '+notes);

        var id = $(this).next().attr('id');
        console.log('id: '+id);
        $('#save-notes').attr('data',id);

        // $('#notes').val('');
        // $('#notes').append(notes);
        $('#notes').summernote('code', notes);
        console.log($('#notes').summernote('code'));
        $('#group-modal').modal('show');
    });
    $(document).on('click','#save-notes',function()
    {
        var id = $(this).attr('data');
        console.log('travel_partners id: '+id);
        console.log($('#notes').summernote('code'));
        console.log($('#'+id));
        $('#'+id).text($('#notes').summernote('code'));
        console.log('get:'+$('#'+id).text());
        console.log('checkbox :'+$('.checkbox-'+id).is(':checked'));
        if($('.checkbox-'+id).is(':checked'))
        {
            $.ajax(
            {
                type: 'POST',
                data: {
                    destination_id :".$destination_id.",
                    travel_partner_id : id,
                    comission: $('.travel_partners_comission-'+id).val(),
                    booking_email: $('.booking_email-'+id).val(),
                    invoice_email: $('.invoice_email-'+id).val(),
                    primary_contact: $('.primary_contact-'+id).val(),
                    billing_contact: $('.billing_contact-'+id).val(),
                    notes :$('#notes').summernote('code')
                },
                url: '$add_destination_travel_partner',
                async: true,
                success: function(data)
                {
                   $('#group-modal').modal('toggle');
                },
                error: function()
                {
                    alert('error');
                }
            });
        }
        else
        {
            $('#group-modal').modal('toggle');
        } 
    });

    // $('.travel_partners_comission').inputmask('999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
    $('.travel_partners_comission').inputmask('99,99',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('.travel_partners_comission').on('change',function()
        {
            $(this).siblings('input').val($(this).inputmask('unmaskedvalue'));
        });

    function initDataTableTravelPartner(tableSelector)
    {

        return $(tableSelector).DataTable({
                language: {
                    aria: {
                        sortAscending: \": activate to sort column ascending\",
                        sortDescending: \": activate to sort column descending\"
                    },
                    emptyTable: \"No data available in table\",
                    info: \"Showing _TOTAL_ amenities\",
                    infoEmpty: \"No amenities found\",
                    infoFiltered: \"(filtered 1 from _MAX_ total amenities)\",
                    lengthMenu: \"_MENU_ amenities\",
                    search: \"Search:\",
                    zeroRecords: \"No matching records found\"
                },
                info: false,
                scrollY: 300,
                scrollCollapse: true,
                //scroller: !0,
                statesave : false,
                columns: [
                    { 'width': '5%' },
                    { 'width': '10%' },
                    { 'width': '10%' },
                    { 'width': '20%' },
                    { 'width': '20%' },
                    { 'width': '20%' },
                    { 'width': '20%' },
                    { 'width': '5%' },
                    
                ],
                order: [
                    [1, \"asc\"]
                ],
                \"columnDefs\": [
                    { \"orderable\": false, \"targets\": [0,2,3,4,5,6,7] }
                ],
                lengthMenu: [
                    [10, 15, 20, -1],
                    [10, 15, 20, \"All\"]
                ],
                paging: false,
                retrieve: true
            }); 
    }

    function redrawDataTableTravelPartners(tableSelector)
    {
        var table = initDataTableTravelPartner(tableSelector);
        table.draw();
    }

    function redrawDataTableTravelPartner()
    {
        redrawDataTableTravelPartners('#destination_travel_partners');
    }

    jQuery(document).ready(function() 
    {
        // alert('here');
        initDataTableTravelPartner('#destination_travel_partners');
        setTimeout(function()
        { 
            redrawDataTableTravelPartner(); 
        }, 500);
        
});",View::POS_END);

?>

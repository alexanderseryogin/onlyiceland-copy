<?php

use yii\db\Migration;

class m170309_082420_alter_bookings_items_add_price_json extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` ADD COLUMN `price_json` TEXT NULL AFTER `total`;");
    }

    public function down()
    {
        echo "m170309_082420_alter_bookings_items_add_price_json cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

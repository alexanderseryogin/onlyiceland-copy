<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_open_hours".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $day
 * @property string $opening
 * @property string $closing
 * @property integer $hours_24
 * @property integer $open
 * @property string $url
 *
 * @property Providers $provider
 */
class DestinationsOpenHoursDays extends \yii\db\ActiveRecord
{
    const Monday = 1;
    const Tuesday = 2;
    const Wednesday = 3;
    const Thursday = 4;
    const Friday = 5;
    const Saturday = 6;
    const Sunday = 7;

    public static $days_arr = 
    [
        1=>'Monday',
        2=>'Tuesday',
        3=>'Wednesday',
        4=>'Thursday',
        5=>'Friday',
        6=>'Saturday',
        7=>'Sunday'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_open_hours';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'day', 'hours_24', 'open'], 'integer'],
            [['opening', 'closing'], 'safe'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
            'day' => Yii::t('app', 'Day'),
            'opening' => Yii::t('app', 'Opening'),
            'closing' => Yii::t('app', 'Closing'),
            'hours_24' => Yii::t('app', 'Hours 24'),
            'open' => Yii::t('app', 'Open'),
            'url' => Yii::t('app', 'Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    public static function getMondayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[monday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[monday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

    public static function getTuesdayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[tuesday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[tuesday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

    public static function getWednesdayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[wednesday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[wednesday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

    public static function getThursdayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[thursday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[thursday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

    public static function getFridayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[friday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[friday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

    public static function getSaturdayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[saturday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[saturday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

    public static function getSundayHtml()
    {
        $html = "";
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-12">';
        $html .= '<div class="col-sm-2">';
        $html .= '</div>'; 
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[sunday][opening][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-3">';
        $html .= '<div class="form-group">';
        $html .= '<div>';
        $html .= '<div class="input-icon">';
        $html .= '<i class="fa fa-clock-o"></i>';
        $html .= '<input type="text" value="00:00" name="DestinationsOpenHoursDays[sunday][closing][]" class="form-control dynamic-clock">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-sm-1 delete-dynamic-field" style="margin-top:7px;">';
        $html .= '<a href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';    

        return $html;
    }

}

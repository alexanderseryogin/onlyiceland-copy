<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_dates_cart".
 *
 * @property string $id
 * @property string $booking_item_id
 * @property string $date
 * @property integer $item_id
 * @property string $item_name_id
 * @property integer $rate_id
 * @property double $custom_rate
 * @property integer $quantity
 * @property integer $no_of_nights
 * @property string $estimated_arrival_time_id
 * @property string $beds_combinations_id
 * @property integer $user_id
 * @property string $guest_first_name
 * @property string $guest_last_name
 * @property integer $guest_country_id
 * @property integer $confirmation_id
 * @property string $booking_cancellation
 * @property integer $flag_id
 * @property integer $status_id
 * @property integer $housekeeping_status_id
 * @property integer $no_of_children
 * @property integer $no_of_adults
 * @property integer $person_no
 * @property integer $person_price
 * @property string $total
 * @property string $price_json
 * @property string $travel_partner_id
 * @property string $voucher_no
 * @property string $reference_no
 * @property integer $offers_id
 *
 * @property BedsCombinations $bedsCombinations
 * @property Countries $guestCountry
 * @property EstimatedArrivalTimes $estimatedArrivalTime
 * @property BookingTypes $flag
 * @property HousekeepingStatus $housekeepingStatus
 * @property BookableItems $item
 * @property Offers $offers
 * @property Rates $rate
 * @property BookingStatuses $status
 * @property DestinationTravelPartners $travelPartner
 * @property User $user
 * @property BookingsItemsCart $bookingItem
 * @property BookableItemsNames $itemName
 */
class BookingDatesCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $discount_codes;
    public $upsell_items;
    public $special_requests;
    
    public static function tableName()
    {
        return 'booking_dates_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_item_id', 'item_id', 'item_name_id', 'rate_id', 'quantity', 'no_of_nights', 'estimated_arrival_time_id', 'beds_combinations_id', 'user_id', 'guest_country_id', 'confirmation_id', 'flag_id', 'status_id', 'housekeeping_status_id', 'no_of_children', 'no_of_adults', 'person_no', 'person_price', 'travel_partner_id', 'offers_id','rate_added_through'], 'integer'],
            [['date'], 'safe'],
            [['custom_rate', 'total'], 'number'],
            [['price_json'], 'string'],
            [['guest_first_name', 'guest_last_name', 'booking_cancellation', 'voucher_no', 'reference_no'], 'string', 'max' => 256],
            [['beds_combinations_id'], 'exist', 'skipOnError' => true, 'targetClass' => BedsCombinations::className(), 'targetAttribute' => ['beds_combinations_id' => 'id']],
            [['guest_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['guest_country_id' => 'id']],
            [['estimated_arrival_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => EstimatedArrivalTimes::className(), 'targetAttribute' => ['estimated_arrival_time_id' => 'id']],
            [['flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['flag_id' => 'id']],
            [['housekeeping_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HousekeepingStatus::className(), 'targetAttribute' => ['housekeeping_status_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['offers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offers::className(), 'targetAttribute' => ['offers_id' => 'id']],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['travel_partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTravelPartners::className(), 'targetAttribute' => ['travel_partner_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['booking_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingsItemsCart::className(), 'targetAttribute' => ['booking_item_id' => 'id']],
            [['item_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItemsNames::className(), 'targetAttribute' => ['item_name_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_item_id' => 'Booking Item ID',
            'date' => 'Date',
            'item_id' => 'Item ID',
            'item_name_id' => 'Item Name ID',
            'rate_id' => 'Rate ID',
            'custom_rate' => 'Custom Rate',
            'quantity' => 'Quantity',
            'no_of_nights' => 'No Of Nights',
            'estimated_arrival_time_id' => 'Estimated Arrival Time ID',
            'beds_combinations_id' => 'Beds Combinations ID',
            'user_id' => 'User ID',
            'guest_first_name' => 'Guest First Name',
            'guest_last_name' => 'Guest Last Name',
            'guest_country_id' => 'Guest Country ID',
            'confirmation_id' => 'Confirmation ID',
            'booking_cancellation' => 'Booking Cancellation',
            'flag_id' => 'Flag ID',
            'status_id' => 'Status ID',
            'housekeeping_status_id' => 'Housekeeping Status ID',
            'no_of_children' => 'No Of Children',
            'no_of_adults' => 'No Of Adults',
            'person_no' => 'Person No',
            'person_price' => 'Person Price',
            'total' => 'Total',
            'price_json' => 'Price Json',
            'travel_partner_id' => 'Travel Partner ID',
            'voucher_no' => 'Voucher No',
            'reference_no' => 'Reference No',
            'offers_id' => 'Offers ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBedsCombinations()
    {
        return $this->hasOne(BedsCombinations::className(), ['id' => 'beds_combinations_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuestCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'guest_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimatedArrivalTime()
    {
        return $this->hasOne(EstimatedArrivalTimes::className(), ['id' => 'estimated_arrival_time_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlag()
    {
        return $this->hasOne(BookingTypes::className(), ['id' => 'flag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHousekeepingStatus()
    {
        return $this->hasOne(HousekeepingStatus::className(), ['id' => 'housekeeping_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasOne(Offers::className(), ['id' => 'offers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(BookingStatuses::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravelPartner()
    {
        return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingItem()
    {
        return $this->hasOne(BookingsItemsCart::className(), ['id' => 'booking_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemName()
    {
        return $this->hasOne(BookableItemsNames::className(), ['id' => 'item_name_id']);
    }

        public function getBookingSpecialRequests()
    {
        return $this->hasMany(BookingSpecialRequestsCart::className(), ['booking_date_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingsDiscountCodes()
    {
        return $this->hasMany(BookingsDiscountCodesCart::className(), ['booking_date_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingsUpsellItems()
    {
        return $this->hasMany(BookingsUpsellItemsCart::className(), ['booking_date_id' => 'id']);
    }

}

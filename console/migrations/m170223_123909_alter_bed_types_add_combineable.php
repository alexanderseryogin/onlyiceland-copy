<?php

use yii\db\Migration;

class m170223_123909_alter_bed_types_add_combineable extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bed_types` ADD COLUMN `combineable` TINYINT(1) NULL AFTER `intended_sleeping_capacity`; 
        ");
    }

    public function down()
    {
        echo "m170223_123909_alter_bed_types_add_combineable cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

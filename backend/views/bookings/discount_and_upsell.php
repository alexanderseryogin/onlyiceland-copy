<div class="tab-pane" id="discounts">
	<div class="panel-group accordion" id="accordion_discount_codes">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_discount_codes" href="#collapse_discount_codes"> Discount Codes</a>
                </h4>
            </div>
            <div id="collapse_discount_codes" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">

                    <div id="discount_codes">
                        <?php if(!empty($model->rates_id) || !$model->isNewRecord): ?>
                        <?=  
                            $this->render('discount_codes',
                            [
                                'checked_discount_codes' => $model->discount_codes,
                                'rates_id' => $model->rates_id,
                            ]);
                        ?>
                        <?php else: ?>
                        <span class="label label-danger"> Note! </span>
                        <span>&nbsp;  Please First select "Rates" from dropdown to add Discount Codes. </span> 
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_discount_codes" href="#collapse_upsell_items"> Upsell Items</a>
                </h4>
            </div>
            <div id="collapse_upsell_items" class="panel-collapse collapse in">
                <div class="panel-body" style="overflow-y:auto;">

                    <div id="upsell_items">
                        <?php if(!empty($model->rates_id) || !$model->isNewRecord): ?>
                        <?=  
                            $this->render('upsell_items',
                            [
                                'checked_upsell_items' => $model->upsell_items,
                                'rates_id' => $model->rates_id,
                            ]);
                        ?>
                        <?php else: ?>
                        <span class="label label-danger"> Note! </span>
                        <span>&nbsp;  Please First select "Rates" from dropdown to add Upsell Items. </span> 
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
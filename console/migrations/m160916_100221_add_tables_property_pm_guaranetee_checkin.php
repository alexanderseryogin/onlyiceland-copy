<?php

use yii\db\Migration;

class m160916_100221_add_tables_property_pm_guaranetee_checkin extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `property_pm_guarantee`( `id` INT(11) NOT NULL AUTO_INCREMENT, `property_id` INT(11) NOT NULL, `property_pm_id` INT(11) NOT NULL, PRIMARY KEY (`id`), CONSTRAINT `FK_property_pm_guarantee_property_id` FOREIGN KEY (`property_id`) REFERENCES `properties`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_property_pm_guarantee_property_pm_id` FOREIGN KEY (`property_pm_id`) REFERENCES `property_payment_methods`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; ");

        $this->execute("CREATE TABLE `property_pm_checkin`( `id` INT(11) NOT NULL AUTO_INCREMENT, `property_id` INT(11) NOT NULL, `property_pm_id` INT(11) NOT NULL, PRIMARY KEY (`id`), CONSTRAINT `FK_property_pm_checkin_property_id` FOREIGN KEY (`property_id`) REFERENCES `properties`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_property_pm_checkin_property_pm_id` FOREIGN KEY (`property_pm_id`) REFERENCES `property_payment_methods`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; ");
    }

    public function down()
    {
        echo "m160916_100221_add_tables_property_pm_guaranetee_checkin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

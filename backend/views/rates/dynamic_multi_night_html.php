
<div class="row">
    <div style="margin-top: 20px"> </div>
    <div class="col-sm-2">
        <select name="Rates[multi_night][nights][]" style="width: 100%;" class="dynamic-multi-night-dropdown">
            <option value="">Select Nights</option>
            <?php
                for ($i=2; $i <= 50 ; $i++) 
                { 
                    echo '<option value="'.$i.'">'.$i.'</option>';
                }
            ?>
        </select>
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control multi-night-percent" value="0" >
        <input type="text" value="0" name="Rates[multi_night][percent][]" hidden>
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control multi-night-price" value="0">
        <input type="text" value="0" name="Rates[multi_night][per_night][]" hidden>
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control multi-night-price" value="0">
        <input type="text" value="0" name="Rates[multi_night][once_off][]" hidden>
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control multi-night-price" value="0">
        <input type="text" value="0" name="Rates[multi_night][price_cap][]" hidden>
    </div>
    <div class="col-sm-2" style="margin-top:07px;" >
        <a class="delete-dynamic-field" href="javascript:void(0)" ><i style="color:red;" class="fa fa-times fa-2x" aria-hidden="true"></i></a>
    </div>
    <br>
</div>

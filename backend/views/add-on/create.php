<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AddOn */

$this->title = 'Create Add-On';
$this->params['breadcrumbs'][] = ['label' => 'Add-Ons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-on-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<?php
Use common\models\User;
Use common\models\Destinations;
use common\models\BookingConfirmationTypes;
use yii\helpers\ArrayHelper;
use yii\web\View;
use common\models\BookableItems;
use common\models\Rates;
use common\models\RatesDates;
// echo "<pre>";
// print_r($model->rates_capacity_pricing);
// exit();
?>

<div class="tab-pane active" id="details">

        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?php if($model->isNewRecord)
                {?>
                    <?= $form->field($model, 'provider_id', [
                        'inputOptions' => [
                            'id' => 'provider-dropdown',
                            'class' => 'form-control',
                            'style' => 'width: 100%'
                            ]
                    ])->dropdownList(ArrayHelper::map(Destinations::find()->all(),'id','name'),['prompt'=>'Select a Destination',
                        'onchange' => 'App.blockUI({target:".rates-form", animate: !0}); $.post("'.Yii::$app->urlManager->createUrl('rates/listitems?provider_id=').'"+$(this).val(), function( data ) 
                            {
                                data = jQuery.parseJSON( data );

                                $("#unit_dropdown").empty().html(data.bookableItems);

                                if(isNewRecord==1)
                                {
                                    $("#upsell_items").empty().html(data.upsellItems);
                                    $("#discount_codes").empty().html(data.discountCodes);

                                    if(data.pricing == "0" ) // Additional Pricing
                                    {
                                        $(".first-additional-prices-div").empty().html(firstAdditionalPricesHtml());
                                        initFirstAdditionalPrices();
                                        $("#submit_button").attr("disabled", false);
                                    }
                                    else if(data.pricing == "1")
                                    {
                                        $(".first-additional-prices-div").empty();
                                        $("#submit_button").attr("disabled", "disabled");
                                    }
                                    console.log(data);
                                    $("#vat_rate").val(data.vat_rate);
                                    $("#lodging_tax_per_night").val(data.lodging_tax);
                                }
                                
                            }).done(function() 
                            {
                                App.unblockUI(".rates-form");
                            })
                            .fail(function() 
                            {
                                App.unblockUI(".rates-form");
                                alert( "error" );
                            });'
                    ]) ?>
                <?php 
                }else{
                ?>
                    <?= $form->field($model, 'provider_id', [
                        'inputOptions' => [
                            'id' => 'provider-dropdown',
                            'class' => 'form-control',
                            'style' => 'width: 100%'
                            ]
                    ])->dropdownList(ArrayHelper::map(Destinations::find()->all(),'id','name'),['prompt'=>'Select a Destination',
                        'onchange' => 'App.blockUI({target:".rates-form", animate: !0}); $.post("'.Yii::$app->urlManager->createUrl('rates/listitems?id='.$model->id.'&provider_id=').'"+$(this).val(), function( data ) 
                            {
                                data = jQuery.parseJSON( data );

                                $("#unit_dropdown").empty().html(data.bookableItems);

                                if(isNewRecord==1)
                                {
                                    $("#upsell_items").empty().html(data.upsellItems);
                                    $("#discount_codes").empty().html(data.discountCodes);

                                    if(data.pricing == "0" ) // Additional Pricing
                                    {
                                        $(".first-additional-prices-div").empty().html(firstAdditionalPricesHtml());
                                        initFirstAdditionalPrices();
                                        $("#submit_button").attr("disabled", false);
                                    }
                                    else if(data.pricing == "1")
                                    {
                                        $(".first-additional-prices-div").empty();
                                        $("#submit_button").attr("disabled", "disabled");
                                    }
                                }
                                console.log(data);
                                $("#vat_rate").val(data.vat_rate);
                                $("#lodging_tax_per_night").val(data.lodging_tax);

                            }).done(function() 
                            {
                                App.unblockUI(".rates-form");
                            })
                            .fail(function() 
                            {
                                App.unblockUI(".rates-form");
                                alert( "error" );
                            });'
                    ]) ?>
                <?php } ?>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-4">
                <?= $form->field($model, 'unit_id', [
                    'inputOptions' => [
                        'id' => 'unit_dropdown',
                        'class' => 'form-control',
                        'style' => 'max-width: 500px'
                        ]
                ])->dropdownList(ArrayHelper::map(BookableItems::find()->where(['provider_id'=>$model->provider_id])->all(),'id','itemType.name'),['prompt'=>'Select an Item',
                    'onchange' => 'App.blockUI({target:".rates-form", animate: !0}); $.post("'.Yii::$app->urlManager->createUrl('rates/list-amenities?item_id=').'"+$(this).val(), function( data ) 
                        {
                            data = JSON.parse(data);
                            if(isNewRecord==1)
                            {
                                $("#amenities_div").html(data.amenities);
                                $("#booking-flag-dropdown").val(data.flag).trigger("change");
                                $("#booking-status-dropdown").val(data.status).trigger("change");
                                $("#booking_confirmation_type_dropdown").val(data.confirmation).trigger("change");

                                if(data.pricing_fields != "" && data.pricing == 1)
                                {
                                    $(".first-additional-prices-div").empty().html(data.pricing_fields);
                                    initFirstAdditionalPrices();

                                    if(typeof $(document).find(".capacity-error").val() != "undefined")
                                        $("#submit_button").attr("disabled", "disabled");
                                    else
                                        $("#submit_button").attr("disabled", false);
                                }

                                if(data.general_booking!="Not Found")
                                {
                                    $("#booking_cancellation").val(data.general_booking).trigger("change");
                                }
                                else
                                {
                                    $("#booking_cancellation").val("always").trigger("change");
                                }

                                if(data.from!="")
                                {
                                    $("#from").val(data.from);
                                }

                                if(data.to!="")
                                {
                                    $("#to").val(data.to);
                                }
                            }
                        }).done(function() {
                            App.unblockUI(".rates-form");
                          })
                          .fail(function() {
                            alert( "error" );
                          });'
                ]) ?>
            </div>

            <div class="col-xs-3">
                <?= $form->field($model, 'booking_confirmation_type_id', [
                    'inputOptions' => [
                        'id' => 'booking_confirmation_type_dropdown',
                        'class' => 'form-control',
                        'style' => 'width: 100%'
                        ]
                    ])->dropdownList(ArrayHelper::map(BookingConfirmationTypes::find()->all(),'id','name'),['prompt'=>'Select a Confirmation Type']) ?>
            </div>
            <div class="col-sm-3">
                <label class="control-label" >General Booking Cancellation</label>
                <div class="form-group" >
                    <select style="width: 100%;" class="form-control" id="booking_cancellation" name="Rates[general_booking_cancellation]" >
                        <?php
                            foreach (Destinations::getBookingCancellationArray() as $key => $value) 
                            {
                                echo '<option value="'.$key.'">'.$value.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="panel-group accordion" id="accordion_prices">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion_prices" href="#collapse_prices"> Prices </a>
                    </h4>
                </div>
                <div id="collapse_prices" class="panel-collapse collapse in">
                    <div class="panel-body" style="overflow-y:auto;">

                        <div class="first-additional-prices-div">
                            <?php 
                                if(!empty($model->provider_id) && $model->destination->pricing == 0)
                                {
                                ?>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label class=" control-label">Item Price First Adult</label>
                                            <input type="text" class="form-control first-additional-prices" value="<?=$model->item_price_first_adult?>">
                                            <input type="text" name="Rates[server_item_price_first_adult]" value="<?=$model->item_price_first_adult?>" hidden>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class=" control-label">Item Price Additional Adult</label>
                                            <input type="text" class="form-control first-additional-prices" value="<?=$model->item_price_additional_adult?>">
                                            <input type="text" name="Rates[server_item_price_additional_adult]" value="<?=$model->item_price_additional_adult?>" hidden>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class=" control-label">Item Price First Child</label>
                                            <input type="text" class="form-control first-additional-prices" value="<?=$model->item_price_first_child?>">
                                            <input type="text" name="Rates[server_item_price_first_child]" value="<?=$model->item_price_first_child?>" hidden>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class=" control-label">Item Price Additional Child</label>
                                            <input type="text" class="form-control first-additional-prices" value="<?=$model->item_price_additional_child?>">
                                            <input type="text" name="Rates[server_item_price_additional_child]" value="<?=$model->item_price_additional_child?>" hidden>
                                        </div>
                                    </div>
                                <?php
                                }
                                else if(!empty($model->provider_id) && !empty($model->unit_id) && $model->destination->getDestinationTypeName()=='Accommodation' &&  $model->destination->pricing == 1)
                                {
                                   // echo "here";
                                   // exit();
                                    if(count($model->rates_capacity_pricing) > 0)
                                    {
                                        // echo "<pre>";
                                        // print_r($model->rates_capacity_pricing);
                                        // exit();
                                        echo '<div class="row">';
                                        foreach ($model->rates_capacity_pricing as $key => $value)
                                        {
                                            if($key>= 0 && $key<=3)
                                                $word =  Rates::$numberWords[$key];
                                            else
                                                $word = ($key+1).' Persons';

                                            echo '<div class="col-xs-2">
                                                    <label class=" control-label">'.$word.'</label>
                                                    <input type="text" class="form-control first-additional-prices" value="'.$value.'">
                                                    <input type="text" name="Rates[capacity-pricing][]" value="'.$value.'" hidden>
                                                </div>';
                                        }   
                                        echo '</div>';
                                    }
                                }
                                else if($model->isNewRecord)
                                {
                                    // echo "new record";
                                    // exit();
                                    $this->registerJs('
                                        $("#submit_button").attr("disabled", "disabled");
                                    ');
                                }
                            ?>
                        </div>        

                    </div>
                </div>
            </div>
            <br>
        </div>

        <?php 
                if($model->isNewRecord && empty($model->first_night_rate))
                {
                    $date_model = RatesDates::find()->orderBy('night_rate DESC')->one();

                    if(!empty($date_model))
                    {
                        $today = date('d/m/Y',strtotime("+1 day", strtotime($date_model->night_rate))); 
                        $tomorrow = date('d/m/Y',strtotime("+2 day", strtotime($date_model->night_rate)));
                    }
                    else
                    {
                        $today = date('d/m/Y');
                        $tomorrow = $today;
                    }
                }
                else
                {
                    $today = $model->first_night_rate;
                    $tomorrow = $model->last_night_rate;
                }

                $this->registerJs("
                            
                            option = $model->published;
                            if(option==0)
                            {
                                $('#published_switch').bootstrapSwitch('state',false);
                            }
                            else
                            {
                                $('#published_switch').bootstrapSwitch('state',true);
                            }
                        ");
        ?>

        <div class="row">
            <div class="col-sm-1">
                <label class="control-label" >Published</label>
                <div class="form-group" >
                    <input type="checkbox" id="published_switch" class="make-switch" name="Rates[published]" data-on-color="success" data-off-color="danger" data-on-text="Yes" data-off-text="No" >
                </div>
            </div>
            <div class="col-xs-2" style="width:20%;">
                <label class=" control-label">First and Last Night of Rate Period</label>

                <div class="input-group input-medium date-picker input-daterange" data-date-format="dd/mm/yyyy">

                    <input type="text" class="form-control" id="from" value="<?=$today?>" name="Rates[first_night_rate]" readonly>

                    <span class="input-group-addon"> to </span>

                    <input type="text" class="form-control" id="to" value="<?=$tomorrow?>" name="Rates[last_night_rate]"> 

                </div>
            </div>
            <div class="col-xs-1">
                <?= $form->field($model, 'vat_rate', [
                    'inputOptions' => [
                        'id' => 'vat_rate',
                        'class' => 'form-control'
                        ]
                    ])->textInput(['maxlength' => true])->hint(' e.g. xx,xx')  ?>

                <input type="text" value="<?=$model->vat_rate?>" name="Rates[server_vat_rate]" hidden>
            </div>
            <div class="col-xs-2">
                <?= $form->field($model, 'lodging_tax_per_night', [
                    'inputOptions' => [
                        'id' => 'lodging_tax_per_night',
                        'class' => 'form-control'
                        ]
                    ])->textInput(['maxlength' => true])->hint(' e.g. 999.999') ?>

                <input type="text" value="<?=$model->lodging_tax_per_night?>" name="Rates[server_lodging_tax_per_night]" hidden>
            </div>
            <div class="col-xs-1">
                <?= $form->field($model, 'min_price', [
                    'inputOptions' => [
                        'id' => 'min_price',
                        'class' => 'form-control'
                        ]
                    ])->textInput(['maxlength' => true])->hint(' e.g. 999.999') ?>

                <input type="text" value="<?=$model->min_price?>" name="Rates[server_min_price]" hidden>
            </div>
            <div class="col-xs-1">
                <?= $form->field($model, 'max_price', [
                    'inputOptions' => [
                        'id' => 'max_price',
                        'class' => 'form-control'
                        ]
                    ])->textInput(['maxlength' => true])->hint(' e.g. 999.999') ?>

                <input type="text" value="<?=$model->max_price?>" name="Rates[server_max_price]" hidden>
            </div>
        </div>

        <label class=" control-label">Minimum / Maximum Stay</label>
        <div class="form-group">
            <input id="min_max_stay" type="text" name="Rates[min_max_stay]" />
        </div>

        <label class=" control-label">Minimum days rate can be booked before check-in / Maximum days rate can be booked into the future</label>
        <div class="form-group">
            <input id="rate_period" value="<?=$model->rate_period ?>" type="text" name="Rates[rate_period]" />
        </div>

        <!-- <label class=" control-label">Minimum Booking & Maximum Booking</label>
        <div class="form-group min-max-booking">
            <input id="min_max_booking" type="text" name="Rates[min_max_booking]" />
        </div> -->
    </div>
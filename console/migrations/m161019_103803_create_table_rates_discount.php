<?php

use yii\db\Migration;

class m161019_103803_create_table_rates_discount extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `rates_discount_code`( `id` INT(11) NOT NULL AUTO_INCREMENT, `rates_id` INT(11), `discount_code_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_rates_discount_code_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON DELETE SET NULL, CONSTRAINT `FK_rates_discount_id` FOREIGN KEY (`discount_code_id`) REFERENCES `discount_code`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161019_103803_create_table_rates_discount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

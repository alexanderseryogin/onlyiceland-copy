<?php
    use yii\web\View;
    use yii\helpers\Url;
?>

<style type="text/css">
    .align-center {
        text-align: center;
    }
    table tr th {
        text-align: center;
    }
    .free{background: #DFF0D8;}
    .extra{background: #FCF8E3}
    .not_available{background: #FFEB89}
    .not_display{background: #F2DEDE}
    .table-scrollable {border: none !important; overflow-x: hidden !important;}
    .dataTables_scrollBody {overflow-x: hidden !important;}
    table.dataTable thead .sorting_desc {
        background-image: none;
    }
    table.dataTable thead .sorting_asc {
        background-image: none;
    }
</style>

<!-- BEGIN ACCORDION PORTLET-->
<div class="tab-pane" id="amenities">

    <div id="amenities_div">
        <?php if(!empty($model->unit_id) || !$model->isNewRecord): ?>
        <?=  
            $this->render('total_amenities_dynamic',[
                'selected_amenities' => $model->amenities,
                'selected_banners' => $model->amenities_banners,
                'amenities' => $amenities
            ]); 
        ?>
        <?php else: ?>
        <span class="label label-danger"> Note! </span>
        <span>&nbsp;  Please First select "Item Name" from dropdown to add amenities. </span> 
        <?php endif ?>
    </div>
    <br>
</div>

<?php
$this->registerJs("

function initDataTable(tableSelector)
{
    return $(tableSelector).DataTable({
            language: {
                aria: {
                    sortAscending: \": activate to sort column ascending\",
                    sortDescending: \": activate to sort column descending\"
                },
                emptyTable: \"No data available in table\",
                info: \"Showing _TOTAL_ amenities\",
                infoEmpty: \"No amenities found\",
                infoFiltered: \"(filtered 1 from _MAX_ total amenities)\",
                lengthMenu: \"_MENU_ amenities\",
                search: \"Search:\",
                zeroRecords: \"No matching records found\"
            },
            scrollY: 300,
            scrollCollapse: true,
            //scroller: !0,
            statesave : false,
            columns: [
                { 'width': '30%' },
                { 'width': '15%' },
                { 'width': '15%' },
                { 'width': '15%' },
                { 'width': '15%' },
            ],
            order: [
                [0, \"asc\"]
            ],
            \"columnDefs\": [
                { \"orderable\": false, \"targets\": [1,2,3,4,5] }
            ],
            lengthMenu: [
                [10, 15, 20, -1],
                [10, 15, 20, \"All\"]
            ],
            paging: false,
            retrieve: true
        }); 
}

function redrawDataTable(tableSelector)
{
    var table = initDataTable(tableSelector);
    table.draw();
}

function redrawAmenitiesDataTable()
{
    redrawDataTable('#rates_amenities');
}

jQuery(document).ready(function() 
{
    initDataTable('#rates_amenities');
    setTimeout(function()
    { 
        redrawAmenitiesDataTable(); 
    }, 500);
    
});",View::POS_END);
?>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AddOn */

$this->title = 'Update Add-On: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Add Ons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="add-on-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

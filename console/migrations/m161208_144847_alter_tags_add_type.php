<?php

use yii\db\Migration;

class m161208_144847_alter_tags_add_type extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `tags` ADD COLUMN `type` INT(11) UNSIGNED NOT NULL AFTER `name`;");
    }

    public function down()
    {
        echo "m161208_144847_alter_tags_add_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

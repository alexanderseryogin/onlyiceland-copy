<?php

?>
<footer class="site-footer-bottom">
  <div class="container">
    <div class="row">
      <div class="copyrights-col-left col-md-6 col-sm-6">
        <p> &copy; 2016–<?php echo date('Y') ?> Núna ehf. All rights reserved worldwide.</p>
      </div>
      <div class="copyrights-col-right col-md-6 col-sm-6">
        <div class="social-icons">
            <a href="//www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
           <a href="//twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
           <a href="//www.pinterest.com/" target="_blank"><i class="fa fa-pinterest"></i></a>
           <a href="//plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
           <a href="//www.pinterest.com/" target="_blank"><i class="fa fa-youtube"></i></a>
           <a href="#"><i class="fa fa-rss"></i></a>
        </div>
      </div>
    </div>
  </div>
</footer>
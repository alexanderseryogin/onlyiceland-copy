<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use bedezign\yii2\audit\models\AuditTrail;

class BookingsItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookings_items';

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'provider_id', 'temp_flag', 'item_id', 'rate_conditions', 'pricing_type', 'flag_id', 'status_id', 'created_by', 'created_at', 'updated_at', 'deleted_at','item_name_id','estimated_arrival_time_id','travel_partner_id', 'offers_id',], 'integer'],
            [['arrival_date', 'departure_date','balance','notes','comments','voucher_no','reference_no','no_of_booking_days','averaged_total','net_total','gross_total','paid_amount','booking_to_arrival','booking_to_cancellation','cancellation_to_arrival','cancelled_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']], 
            [['item_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItemsNames::className(), 'targetAttribute' => ['item_name_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingStatuses::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['housekeeping_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HousekeepingStatus::className(), 'targetAttribute' => ['housekeeping_status_id' => 'id']],  
            [['flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookingTypes::className(), 'targetAttribute' => ['flag_id' => 'id']],
            [['estimated_arrival_time_id'], 'exist', 'skipOnError' => true, 'targetClass' => EstimatedArrivalTimes::className(), 'targetAttribute' => ['estimated_arrival_time_id' => 'id']],
            [['offers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offers::className(), 'targetAttribute' => ['offers_id' => 'id']],
            [['travel_partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => DestinationTravelPartners::className(), 'targetAttribute' => ['travel_partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            // 'booking_number' => Yii::t('app', 'Booking Number'),
            'provider_id' => Yii::t('app', 'Destination'),
            'created_by' => Yii::t('app', 'Created By'),
            'temp_flag' => Yii::t('app', 'Temp Flag'),
            'item_id' => Yii::t('app', 'Item ID'),
            'rate_conditions' => Yii::t('app', 'Remove Rate Conditions'),
            'arrival_date' => Yii::t('app', 'Arrival Date'),
            'departure_date' => Yii::t('app', 'Departure Date'),
            'pricing_type' => Yii::t('app', 'Pricing Type'),
            'item_name_id' => Yii::t('app', 'Item Name ID'),
            'flag_id' => Yii::t('app', 'Flag ID'), 
            'status_id' => Yii::t('app', 'Status ID'), 
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            // 'bedezign\yii2\audit\AuditTrailBehavior'

        ];
    }

    public function getAuditTrails()
    {
        // return $this->hasMany(AuditTrail::className(), ['model_id' => 'id'])
        //     ->andOnCondition(['model' => get_class($this)]);

        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id, 
                'audit_trail.model' => get_class($this),
              ])
            ->orOnCondition([
                'audit_trail.model_id' => ArrayHelper::map($this->getBookingDatesSortedAndNotNull()->all(), 'id', 'id'), 
                'audit_trail.model' => 'common\models\BookingDates',
            ]);
    }

    public function getEstimatedArrivalTime()
    {
        return $this->hasOne(EstimatedArrivalTimes::className(), ['id' => 'estimated_arrival_time_id']);
    }

    public function getOffers()
    {
        return $this->hasOne(Offers::className(), ['id' => 'offers_id']);
    }
    
    public function getTravelPartner()
    {
        return $this->hasOne(DestinationTravelPartners::className(), ['id' => 'travel_partner_id']);
    }

    public function getStatus() 
    { 
       return $this->hasOne(BookingStatuses::className(), ['id' => 'status_id']); 
    } 

    public function getFlag()
    {
       return $this->hasOne(BookingTypes::className(), ['id' => 'flag_id']);
    }

    public function getItemName() 
    { 
        return $this->hasOne(BookableItemsNames::className(), ['id' => 'item_name_id']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'item_id']);
    }

    public function getHouseKeepingStatus()
    {
        return $this->hasOne(HousekeepingStatus::className(), ['id' => 'housekeeping_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    public function getBookingDates()
    {
        return $this->hasMany(BookingDates::className(), ['booking_item_id' => 'id'])
                    ->orderBy(['booking_dates.date'=>SORT_ASC]);
    }

    public function getBookingDatesSortedAndNotNull()
    { 
        // changed for csv as rate is 0
        return $this->hasMany(BookingDates::className(), ['booking_item_id' => 'id'])
                    // ->andWhere(['>','booking_dates.rate_id', 0])
                    ->orderBy(['booking_dates.date'=>SORT_ASC]);
    }

   /**
    * @return \yii\db\ActiveQuery
    */
    public function getBookingItemGroups()
    {
       return $this->hasOne(BookingItemGroup::className(), ['booking_item_id' => 'id']);
    }

    public function FirstBookingDate()
    {
        return BookingDates::findOne(['booking_item_id' => $this->id]);
    }

    public static function RateAllowedQualifies($arrival_date,$departure_date,$rateAllowedDays)
    {
        $rateAllowedDays = explode(';', $rateAllowedDays);
        $date = $arrival_date;

        $date1=date_create($departure_date);
        $date2=date_create($arrival_date);
        $diff=date_diff($date1,$date2);
        $difference =  $diff->format("%a")+1;

        $bookingDays = [];

        for ($i=0; $i < $difference ; $i++) 
        {
            $day = date('N',strtotime($date));

            if(!in_array($day, $bookingDays))
            {
                $bookingDays[] = $day;
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }  

        foreach ($bookingDays as $key => $value) 
        {
            if(in_array($value-1, $rateAllowedDays))
            {
                return true;
            }
        }
        return false;
    }

    public static function itemsAvailability($difference,$arrival_date,$item_id)
    {
        $arr = [];
        $date = $arrival_date;
        $model = BookableItems::findOne(['id' => $item_id]);

        for ($i=0; $i < $difference ; $i++)
        {
            $booked_item_quantity = BookingDates::find()->joinWith('bookingItem')->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->sum('booking_dates.quantity');

            $cart_item_quantity = BookingsItems::find()->joinWith('bookingDates')->where(['created_by' => Yii::$app->user->identity->id, 'bookings_items.temp_flag' => 1, 'bookings_items.deleted_at'=>0, 'date' => $date, 'bookings_items.item_id' => $item_id])->sum('booking_dates.quantity');

            $arr[$date] = $model->item_quantity - $booked_item_quantity - $cart_item_quantity;

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $arr;  
    }

    public static function itemsAvailabilityExcludedCart($difference,$arrival_date,$item_id)
    {
        $arr = [];
        $date = $arrival_date;
        $model = BookableItems::findOne(['id' => $item_id]);

        for ($i=0; $i < $difference ; $i++)
        {
            $booked_item_quantity = BookingDates::find()->joinWith('bookingItem')->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->sum('booking_dates.quantity');

            $arr[$date] = $model->item_quantity - $booked_item_quantity;

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $arr;  
    }

    public static function itemsAvailabilityForCalendar($difference,$arrival_date,$item_id,$booked_item_id)
    {
        $arr = [];
        $date = $arrival_date;
        $model = BookableItems::findOne(['id' => $item_id]);

        for ($i=0; $i < $difference ; $i++)
        {
            $booked_item_quantity = BookingDates::find()
                                                ->joinWith('bookingItem')
                                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                                ->andWhere(['!=','booking_item_id',$booked_item_id])
                                                ->sum('booking_dates.quantity');

            $arr[$date] = $model->item_quantity - $booked_item_quantity;

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $arr;  
    }

    public static function totalAvailableRates($rates,$difference,$arrival_date,$departure_date)
    {
        $count = 0;
        foreach ($rates as $key => $rate) 
        {
            $min_max_stay_compatible = 0;

            if(!empty($rate->min_max_stay))
            {
                $arr = explode(';', $rate->min_max_stay);
                if($difference >= $arr[0] && $difference <= $arr[1])
                {
                    $min_max_stay_compatible = 1;
                }
            }

            $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
            $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

            if((($arrival_date >= $first_night && $arrival_date <= $last_night) ||
                ($departure_date >= $first_night && $departure_date <= $last_night) || 
                ($first_night >= $arrival_date && $first_night <= $departure_date) ||
                ($last_night >= $arrival_date && $last_night <= $departure_date)) &&
                $min_max_stay_compatible == 1)
            {
                $count++;    
            }
        }
        return $count;
    }

    public static function checkAvailableRates($item_id,$date,$difference,$remove_rate_condtions='')
    {
        $arr = [
            'availableRates' => '',
            'availableRatesIds' => '',
        ];
        $availableRates = [];
        $availableRatesIds = [];
        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            for ($i=0; $i < $difference ; $i++)
            {
                foreach ($rates as $key => $rate) 
                {
                    $rate_arr  = [];
                    $min_max_stay_compatible = 0;

                    if(!empty($rate->min_max_stay))
                    {
                        $arr = explode(';', $rate->min_max_stay);
                        if($difference >= $arr[0] && $difference <= $arr[1])
                        {
                            $min_max_stay_compatible = 1;
                        }
                    }

                    $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
                    $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

                    if(($date >= $first_night && $date <= $last_night) && $min_max_stay_compatible == 1)
                    {
                        $availableRatesIds[$date][] = $rate->id;
                        $availableRates[$date][]= $rate;
                    }
                }

                $date = strtotime("+1 day", strtotime($date));
                $date = date("Y-m-d", $date);
            }

            if(!empty($availableRatesIds))
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    $availableRatesIds[$key] = implode(',', $value);
                }
            }

            $arr['availableRatesIds'] = $availableRatesIds;
            $arr['availableRates'] = $availableRates;
            return $arr;
        }
        else
        {
            return $arr;
        }
    }

    public static function checkAvailableRatesForUpdate($item_id,$date,$difference,$savedDates)
    {
        $arr = [
            'availableRates' => '',
            'availableRatesIds' => '',
        ];
        $availableRates = [];
        $availableRatesIds = [];
        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            for ($i=0; $i < $difference ; $i++)
            {
                if(!in_array($date, $savedDates))
                {
                    foreach ($rates as $key => $rate) 
                    {
                        $rate_arr  = [];
                        $min_max_stay_compatible = 0;

                        if(!empty($rate->min_max_stay))
                        {
                            $arr = explode(';', $rate->min_max_stay);
                            if($difference >= $arr[0] && $difference <= $arr[1])
                            {
                                $min_max_stay_compatible = 1;
                            }
                        }

                        $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
                        $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

                        if(($date >= $first_night && $date <= $last_night) && $min_max_stay_compatible == 1)
                        {
                            $availableRatesIds[$date][] = $rate->id;
                            $availableRates[$date][]= $rate;
                        }
                    }
                }

                $date = strtotime("+1 day", strtotime($date));
                $date = date("Y-m-d", $date);
            }

            if(!empty($availableRatesIds))
            {
                foreach ($availableRatesIds as $key => $value) 
                {
                    $availableRatesIds[$key] = implode(',', $value);
                }
            }

            $arr['availableRatesIds'] = $availableRatesIds;
            $arr['availableRates'] = $availableRates;
            return $arr;
        }
        else
        {
            return $arr;
        }
    }

    public static function getSeasonGroupArray($item_id)
    {
        $groups = [];

        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            $x = 0;
            foreach ($rates as $key => $rate) 
            {
                $first_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate ASC')->one()->night_rate;
                $last_night = RatesDates::find()->where(['rate_id' => $rate->id])->orderBy('night_rate DESC')->one()->night_rate;

                $arr['start_date'] = $first_night;
                $arr['end_date'] = $last_night;

                if(!empty($groups))
                {
                    $flag = false;
                    foreach ($groups as $key => $value) 
                    {
                        if($first_night == $value['start_date'] && $last_night == $value['end_date'])
                        {
                            $flag = true;
                            break;
                        }
                    }

                    if($flag == false)
                        $groups[] = $arr;
                }

                if(!$x)
                {
                    $groups[] = $arr;
                    $x = 1;
                }
            }
            return $groups;
        }
        else
        {
            return $groups;
        }
    }

    public static function checkRateConditions($item_id,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions)
    {
        $rates = Rates::find()
                        ->where(['unit_id' => $item_id])
                        ->andWhere(['published' => 1 ])
                        ->all();

        if(!empty($rates))
        {
            foreach ($rates as $key => $rate) 
            {
                $checkin_allowed = explode(';', $rate->check_in_allowed);
                $checkout_allowed = explode(';', $rate->check_out_allowed);

                if(!(self::RateAllowedQualifies($arrival_date,$departure_date,$rate->rate_allowed) && in_array($arrival_day-1, $checkin_allowed) && in_array($departure_day-1, $checkout_allowed)) && $remove_rate_conditions==0)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public static function checkRateConditionsForUpdate($availableRates,$arrival_date,$departure_date,$arrival_day,$departure_day,$remove_rate_conditions)
    {
        $rates = [];

        foreach ($availableRates as $dateKey => $rates) 
        {
            foreach ($rates as $key => $rate) 
            {
                if(!in_array($rate, $rates))
                {
                    array_push($rates, $rate);
                }
            }
        }

        if(!empty($rates))
        {
            foreach ($rates as $key => $rate) 
            {
                $checkin_allowed = explode(';', $rate->check_in_allowed);
                $checkout_allowed = explode(';', $rate->check_out_allowed);

                if(!(self::RateAllowedQualifies($arrival_date,$departure_date,$rate->rate_allowed) && in_array($arrival_day-1, $checkin_allowed) && in_array($departure_day-1, $checkout_allowed)) && $remove_rate_conditions==0)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public static function checkAvailableItemNamesIncludedCart($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()
                                        ->where(['bookable_item_id' => $item_id])
                                        ->orderBy('item_order ASC')
                                        ->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDatesCart::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.created_by' => Yii::$app->user->identity->id,'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates_cart.item_id' => $item_id])->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        if(!empty($bookableItemNames))
        {
            foreach ($bookableItemNames as $key => $value) 
            {
                if(!in_array($value, $bookedItemNames))
                {
                    $availableItemNames[] = $value;
                }
            }
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNames($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        if(!empty($bookableItemNames))
        {
            foreach ($bookableItemNames as $key => $value) 
            {
                if(!in_array($value, $bookedItemNames))
                {
                    $availableItemNames[] = $value;
                }
            }
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNamesIncludedSaved($item_id,$date,$difference,$booking_item_id)
    {
        $date_temp = $date;
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->andWhere(['!=','booking_item_id',$booking_item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(!in_array($model->item_name_id, $bookedItemNames))
                        $bookedItemNames[] = $model->item_name_id;
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        if(!empty($bookableItemNames))
        {
            foreach ($bookableItemNames as $key => $value) 
            {
                // added this code to chcek if unit is closed or not // 
                $bookable_item_name_open_flag = true;
                for ($i=0; $i < $difference ; $i++)
                {
                    // $status_override = BookingOverrideStatusDetails::findOne(['date' => $date,'bookable_item_name_id' => $item_name->id,'booking_override_status_id' =>  3]);
                    // if(!empty($status_override))
                    // {
                    //     $bookable_item_name_flag = false;
                    // }

                    $unit_closed = UnitAvailability::findOne(['date' => $date_temp, 'unit_id' => $value]);
                    if(!empty($unit_closed))
                    {
                        $bookable_item_name_open_flag = false;
                    }

                    $date_temp = strtotime("+1 day", strtotime($date_temp));
                    $date_temp = date("Y-m-d", $date_temp);
                }
                // code to check if unit is closed ends //
                if(!in_array($value, $bookedItemNames) && $bookable_item_name_open_flag)
                {
                    $availableItemNames[] = $value;
                }
            }

        }

        return $availableItemNames;
    }

    //////////////////////////////////////////////
    // This copy has item names in seperate rates and dates
    //////////////////////////////////////////////

    public static function checkAvailableItemNamesIncludedCartCopy($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.created_by' => Yii::$app->user->identity->id, 'bookings_items.temp_flag' => 1, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(isset($bookedItemNames[$date]))
                    {
                        if(!in_array($model->item_name_id, $bookedItemNames[$date]))
                            $bookedItemNames[$date][] = $model->item_name_id;
                    }
                    else
                    {
                        $bookedItemNames[$date][] = $model->item_name_id;
                    }
                }
            }
            else
            {
                $bookedItemNames[$date] = [];
            }

            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(isset($bookedItemNames[$date]))
                    {
                        if(!in_array($model->item_name_id, $bookedItemNames[$date]))
                            $bookedItemNames[$date][] = $model->item_name_id;
                    }
                    else
                    {
                        $bookedItemNames[$date][] = $model->item_name_id;
                    }
                }
            }

            if(!empty($bookableItemNames))
            {
                foreach ($bookableItemNames as $key => $value) 
                {
                    if(!in_array($value, $bookedItemNames[$date]))
                    {
                        $availableItemNames[$date][] = $value;
                    }
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $availableItemNames;
    }

    public static function checkAvailableItemNamesCopy($item_id,$date,$difference)
    {
        $bookableItemNames = ArrayHelper::map(BookableItemsNames::find()->where(['bookable_item_id' => $item_id])->all(),'id','id');
        $bookedItemNames = [];
        $availableItemNames = [];

        for ($i=0; $i < $difference ; $i++)
        {
            $bookingDatesModel = BookingDates::find()
                                ->joinWith('bookingItem')
                                ->where(['bookings_items.temp_flag' => 0, 'bookings_items.deleted_at'=>0, 'date' => $date, 'booking_dates.item_id' => $item_id])
                                ->all();

            if(!empty($bookingDatesModel))
            {
                foreach ($bookingDatesModel as $key => $model) 
                {
                    if(isset($bookedItemNames[$date]))
                    {
                        if(!in_array($model->item_name_id, $bookedItemNames[$date]))
                            $bookedItemNames[$date][] = $model->item_name_id;
                    }
                    else
                    {
                        $bookedItemNames[$date][] = $model->item_name_id;
                    }
                }
            }
            else
            {
                $bookedItemNames[$date] = [];
            }

            if(!empty($bookableItemNames))
            {
                foreach ($bookableItemNames as $key => $value) 
                {
                    if(!in_array($value, $bookedItemNames[$date]))
                    {
                        $availableItemNames[$date][] = $value;
                    }
                }
            }

            $date = strtotime("+1 day", strtotime($date));
            $date = date("Y-m-d", $date);
        }

        return $availableItemNames;
    }

    public function getBookingDateFinances()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_item_id' => 'id']);
    }

    public function getBookingDateFinancesReverse()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_item_id' => 'id'])->orderBy('id DESC');
    }

    public function getBookingDateCharges()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_item_id' => 'id'])
                    ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT])
                    ->orderBy(['date' => SORT_ASC,'entry_type'=> SORT_ASC]);
    }

    public function getBookingDatePayments()
    {
        return $this->hasMany(BookingDateFinances::className(), ['booking_item_id' => 'id'])
                    ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT])
                    ->orderBy(['date'=> SORT_ASC]);
    }
    
    public function getBookingHousekeepingStatus()
    {
        return $this->hasMany(BookingHousekeepingStatus::className(), ['booking_item_id' => 'id']);
    }

    public static function getChargesRow()
    {
        $html = "";
        $html .= '<tr class="custom charge" >';
            $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
            $html.='<td> <input   class="form-control  calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
            $html.='<td><input class="form-control" type="text" name=""></td>';
            $html.='<td><input class="form-control qty" type="text" name="" value=1 ></td>';
            $html.='<td><input class="form-control price" type="text" name="" style="text-align: right;"></td>';
            $html.='<td><input class="form-control vat" type="text" name="" style="text-align: right;"></td>';
            $html.='<td><input class="form-control tax tax_new" type="text" name="" style="text-align: right;"></td>';
            $html.='<td><input class="form-control discount" type="text" name="" style="text-align: right;" readonly></td>';
            $html.='<td><input class="form-control discount_amount discount_amount_new" type="text" name="" style="text-align: right;" ></td>';
            $html.='<td><input class="form-control vat_amount" type="text" name="" style="text-align: right;" readonly></td>';
            $html.='<td><input class="form-control" type="text" name="" style="text-align: right;" readonly></td>';

            // $html.= '<td><div class="col-sm-1" style="margin-top:7px;">';
            //         $html.= '<a href="javascript:void(0)" class="save-dynamic-row-charges"><i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color:green;"></i></i></a>';
            //     $html.= '</div></td>';
            $html.= '<td>';
            // $html.='<a class="btn btn-icon-only green save-dynamic-row-charges" href="javascript:;">
            //             <i class="fa fa-plus"></i>
            //         </a>
            //         <a class="btn btn-icon-only red delete-dynamic-row-charges" href="javascript:;">
            //             <i class="fa fa-times"></i>
            //         </a>';
            $html.= '<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-charges hide" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus" aria-hidden="true"></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-charges" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        $html .= '</tr>';    

        return $html;
    }

    public static function getAddOnDropdownRow($id,$item_id)
    { 

        $add_ons = DestinationAddOns::find()->where(['destination_id' => $id])->all();
        $bookingItem = BookingsItems::findOne(['id' => $item_id ]);

        $rates_arr = array();
        foreach ($bookingItem->bookingDatesSortedAndNotNull as $value) 
        {
            if(!in_array($value->rate, $rates_arr))
            {
                array_push($rates_arr, $value->rate);
            }
        }


        $html = "";
        if(!empty($add_ons))
        {
            $html .= '<tr class="custom add_on">';
                $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
                $html.='<td><input class="checkbox disable_discount_checkbox" type="checkbox" name=""></td>';
                $html.='<td> <input   class="form-control calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
                $html.='<td class="hasSelect">';
                //$html.='<button class="btn btn-default btn-small show_dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></button>';
                $html.='<div class="form-group" style="margin-bottom: 0px;">';
                $html.='<div class="input-group" style="text-align:left">';
                $html.='<span class="input-group-btn">';
                $html.='<a href="javascript:;" class="btn btn-default btn-small show_dropdown" >';
                $html.='<i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                $html.='</span>';
                $html.='<input class="form-control charge_description" type="text" name="" >';
                $html.='<select class="form-control add_on_dropdown" data-placeholder="Select an Option" style="width:100%;">';
                //$html.='<option value=0 selected data-default>Select an Option</option>';
                $html.='<option></option>';
                $html.='<optgroup label="Add-Ons">';
                //$html.='<option value=0 selected data-default>Select Add-On</option>';
                foreach ($add_ons as $add_on) 
                {
                    $html.='<option data-type=3 value="'.$add_on->id.'">'.$add_on->name.'</option>';
                }
                $html.='</optgroup>';
                if(!empty($rates_arr))
                {
                    $html.='<optgroup label="Upsells">';
                    foreach ($rates_arr as $rate) 
                    {
                        if(!empty($rate->ratesUpsells))
                        {
                            
                            foreach ($rate->ratesUpsells as $rate_upsell) 
                            {
                                $html.='<option data-type=2 value="'.$rate_upsell->id.'">'.$rate_upsell->upsell->name.'</option>';
                            }
                            
                        }
                        
                    }
                    $html.='</optgroup>';
                }
                
                $html.='</select>';
                $html.='</div></div></td>';
                $html.='<td><input class="form-control qty" type="text" name="" value=1 ></td>';
                $html.='<td><input class="form-control price" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control vat" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control tax tax_new" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control discount" type="text" name="" style="text-align: right;" readonly></td>';
                $html.='<td><input class="form-control discount_amount discount_amount_new" type="text" name="" style="text-align: right;"></td>';
                $html.='<td><input class="form-control vat_amount" type="text" name="" style="text-align: right;" readonly></td>';
                $html.='<td><input class="form-control" type="text" name="" style="text-align: right;" readonly></td>';
                // $html.= '<td><div class="col-sm-1" style="margin-top:7px;">';
                //     $html.= '<a href="javascript:void(0)" class="save-dynamic-row-charges"><i class="fa fa-check-circle-o fa-2x" aria-hidden="true" style="color:green;"></i></i></a>';
                // $html.= '</div></td>';
                $html.= '<td>';
                // $html.='<a class="btn btn-icon-only green save-dynamic-row-charges" href="javascript:;">
                //             <i class="fa fa-plus"></i>
                //         </a>
                //         <a class="btn btn-icon-only red delete-dynamic-row-charges" href="javascript:;">
                //             <i class="fa fa-times"></i>
                //         </a>';
                $html.= '<button class="btn btn-icon-only green save-dynamic-row-charges" title="Save/Update" data-toggle="tooltip" disabled><i class="fa fa-plus" aria-hidden="true"></i></button><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-charges hide" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>';
            $html.= '</td>';
            $html .= '</tr>';
        }    

        return $html;
    }
    public static function getPaymentsRow()
    {
        $html = "";
        $html .= '<tr class="custom payment">';
            $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
            $html.='<td> <input   class="form-control  calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" data-date-start-date="+0d"></td>';
            $html.='<td><input class="form-control" type="text" name=""></td>';
            $html.='<td></td>';
            $html.='<td></td>';
            $html.='<td></td>';
            $html.='<td></td>';
            $html.='<td><input class="form-control total" type="text" name="" style="text-align: right;"></td>';

            $html.= '<td>';
                $html.= '<a href="javascript:void(0)" class="btn btn-icon-only green save-dynamic-row-payments" title="Save/Update" data-toggle="tooltip"><i class="fa fa-plus" aria-hidden="true" ></i></a><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments"title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>';
            $html.= '</td>';
        $html .= '</tr>';    

        return $html;
    }

    public static function getPaymentmethodsDropdown($id, $booking_item_id)
    { 
        $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id ])
                                          ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
                                          ->sum('final_total');

        $total_credit = BookingDateFinances::find()->where(['booking_item_id' =>$booking_item_id ])
                                                  ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
                                                  ->sum('final_total');
        $total_debit = empty($total_debit)?0:$total_debit;
        $total_credit =  empty($total_credit)?0:$total_credit;      

        $balance_due = $total_debit - $total_credit;

        $payment_methods = PaymentMethodCheckin::find()->where(['provider_id' => $id])->all();

        
        $html = "";
        if(!empty($payment_methods))
        {
            $html .= '<tr class="custom payment has-switch">';
                $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
                $html.='<td> <input   class="form-control calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" ></td>';
                $html.='<td class="hasSelect">';
                //$html.='<button class="btn btn-default btn-small show_dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></button>';
                // $html.='<input class="form-control" type="text" name="" style="float: right; width: 92%;">';
                $html.='<div class="form-group" style="margin-bottom: 0px;">';
                $html.='<div class="input-group" style="text-align:left">';
                $html.='<span class="input-group-btn">';
                $html.='<a href="javascript:;" class="btn btn-default btn-small show_dropdown" >';
                $html.='<i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                $html.='</span>';
                $html.='<input class="form-control payment_description" type="text" name="" >';
                $html.='<select class="form-control payment_method_dropdown" data-placeholder="Select an Option" style="width:100%">';
                //$html.='<option value=0 selected data-default>Select an Option</option>';
                //$html.='<option value=0 selected data-default></option>';
                $html.='<option ></option>';
                foreach ($payment_methods as $payment_method) 
                {
                    $html.='<option value="'.$payment_method->id.'">'.$payment_method->pm->name.'</option>';
                }
                $html.='</select></div></div></td>';
                $html.='<td><input type="checkbox" class="make-switch payment-switch" data-on-text="Deposit" data-off-text="Normal" data-size="small"></td>';
                $html.='<td>';
                $html.='<div class="form-group deposit_percentage hide" style="margin-bottom: 0px;">';
                $html.='<div class="input-group" style="text-align:left">';
                $html.='<span class="input-group-btn">';
                $html.='<a href="javascript:;" class="btn btn-default btn-small show_dropdown2" >';
                $html.='<i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                $html.='</span>';
                $html.='<input class="form-control deposit_amount" type="text" name="" >';
                $html.='<select class="form-control  payment_percentage_dropdown " style="width:100%" data-placeholder="Select an Option">';
                // $html.='<option value="" disabled selected style="display: none;">Please Choose</option>';
                $html.='<option ></option>';
                $html.='<option value=10>10 </option>';
                $html.='<option value=25>25 </option>';
                $html.='<option value=50>50 </option>';
                $html.='<option value=75>75 </option>';
                $html.='<option value=100>100 </option>';
                $html.='</select></div></div></td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='<td><input class="form-control total" type="text" name="" style="text-align: right;" value="'.Yii::$app->formatter->asDecimal($balance_due,0).'"></td>';

                $html.= '<td>';
                $html.= '<button class="btn btn-icon-only green save-dynamic-row-payments" title="Save/Update" data-toggle="tooltip" disabled><i class="fa fa-plus" aria-hidden="true" ></i></button><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments hide" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>';
            $html.= '</td>';
            $html .= '</tr>';
        }    
        // echo "<pre>";
        // print_r($html);
        // exit(); 
        return $html;
    }

    public static function getGroupPaymentmethodsDropdown($booking_group_id)
    { 
        // $total_debit = BookingDateFinances::find()->where(['booking_item_id' => $booking_item_id ])
        //                                   ->andWhere(['type' => BookingDateFinances::TYPE_DEBIT ])
        //                                   ->sum('final_total');

        // $total_credit = BookingDateFinances::find()->where(['booking_item_id' =>$booking_item_id ])
        //                                           ->andWhere(['type' => BookingDateFinances::TYPE_CREDIT ])
        //                                           ->sum('final_total');
        // $total_debit = empty($total_debit)?0:$total_debit;
        // $total_credit =  empty($total_credit)?0:$total_credit;      

        // $balance_due = $total_debit - $total_credit;
        $bookingGroup = BookingGroup::findOne(['id' => $booking_group_id ]);
        $balance_due = $bookingGroup->balance;

        $booking_group_items = BookingItemGroup::find()->where(['booking_group_id' => $booking_group_id])->all();
        $destinations_arr = array();
        foreach ($booking_group_items as $item) 
        {
            if(!in_array($item->bookingItem->provider_id, $destinations_arr))
            {
                array_push($destinations_arr, $item->bookingItem->provider_id);
            }
        }
        $payment_methods = PaymentMethodCheckin::find()->where(['in','provider_id' ,$destinations_arr])->all();

        
        $html = "";
        if(!empty($payment_methods))
        {
            $html .= '<tr class="custom payment">';
                $html.='<td><input class="checkbox" type="checkbox" name=""></td>';
                $html.='<td> <input   class="form-control calendar form-control-inline input-small date-picker" data-date-format="dd-mm-yyyy" size="16" type="text" value="'.date('d-m-Y').'" ></td>';
                $html.='<td class="hasSelect">';
                //$html.='<button class="btn btn-default btn-small show_dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></button>';
                // $html.='<input class="form-control" type="text" name="" style="float: right; width: 92%;">';
                $html.='<div class="form-group" style="margin-bottom: 0px;">';
                $html.='<div class="input-group" style="text-align:left">';
                $html.='<span class="input-group-btn">';
                $html.='<a href="javascript:;" class="btn btn-default btn-small show_dropdown" >';
                $html.='<i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                $html.='</span>';
                $html.='<input class="form-control payment_description" type="text" name="" >';
                $html.='<select class="form-control payment_method_dropdown" style="width:100%" data-placeholder="Select an Option">';
                //$html.='<option value=0 selected data-default>Select an Option</option>';
                //$html.='<option value=0 selected data-default>Select Method</option>';
                $html.='<option ></option>';
                foreach ($payment_methods as $payment_method) 
                {
                    $html.='<option value="'.$payment_method->id.'">'.$payment_method->pm->name.'</option>';
                }
                $html.='</select></div></div></td>';
                $html.='<td><input type="checkbox" class="make-switch payment-switch" data-on-text="Deposit" data-off-text="Normal" data-size="small"></td>';
                $html.='<td>';
                $html.='<div class="form-group deposit_percentage hide" style="margin-bottom: 0px;">';
                $html.='<div class="input-group" style="text-align:left">';
                $html.='<span class="input-group-btn">';
                $html.='<a href="javascript:;" class="btn btn-default btn-small show_dropdown2" >';
                $html.='<i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                $html.='</span>';
                $html.='<input class="form-control deposit_amount" type="text" name="" >';
                $html.='<select class="form-control  payment_percentage_dropdown " style="width:100%" data-placeholder="Select an Option">';
                // $html.='<option value="" disabled selected style="display: none;">Please Choose</option>';
                $html.='<option ></option>';
                $html.='<option value=10>10 </option>';
                $html.='<option value=25>25 </option>';
                $html.='<option value=50>50 </option>';
                $html.='<option value=75>75 </option>';
                $html.='<option value=100>100 </option>';
                $html.='</select></div></div></td>';

                $html.='<td></td>';
                $html.='<td></td>';
                $html.='<td><input class="form-control total" type="text" name="" style="text-align: right;" value="'.$balance_due.'"></td>';

                $html.= '<td>';
                $html.= '<button class="btn btn-icon-only green save-dynamic-row-payments" title="Save/Update" data-toggle="tooltip" disabled><i class="fa fa-plus" aria-hidden="true" ></i></button><a href="javascript:void(0)" class="btn btn-icon-only red delete-dynamic-row-payments hide" title="Delete" data-toggle="tooltip"><i class="fa fa-minus" aria-hidden="true"></i></a>';
            $html.= '</td>';
            $html .= '</tr>';
        }    
        // echo "<pre>";
        // print_r($html);
        // exit(); 
        return $html;
    }
    
}

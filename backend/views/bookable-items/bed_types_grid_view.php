<?php

use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<style type="text/css">
    #bed_type-gridview div
    {
        
    }
</style>

<?php Pjax::begin(['id' => 'bed_type-gridview','timeout' => 1000000, 'enablePushState' => false]) ?>

    <?= GridView::widget([
    'dataProvider' => $dataProviderBedType,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Bed Type',
            'value' => function($data)
            {
                return $data->bedType->name;
            },
        ],
        [
            'label' => 'Quantity',
            'value' => function($data)
            {
                return $data->quantity;
            },
            'contentOptions' => function($data)
            {
                return ['style' => 'text-align:right;'];
            },
        ],
        [
            'label' => 'Max Capacity',
            'value' => function($data)
            {
                return $data->quantity*$data->bedType->max_sleeping_capacity;
            },
            'contentOptions' => function($data)
            {
                return ['style' => 'text-align:right;'];
            },
        ],

        ['class' => 'yii\grid\ActionColumn',
        'template' => '{delete}',
        'buttons' => [
              'delete' => function ($url, $model, $key) {
                   return '<a style="text-decoration: none;" aria-label="delete" data-id='.$model->id.' title="delete" name="bed_type_delete[]">
                            <span class="glyphicon glyphicon-trash"></span>
                            </a>';
              },
          ]
        ],
    ],
]); ?>

<?php Pjax::end() ?>
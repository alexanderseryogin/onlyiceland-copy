<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DestinationsFinancial;

/**
 * DestinationsFinancialSearch represents the model behind the search form about `common\models\DestinationsFinancial`.
 */
class DestinationsFinancialSearch extends DestinationsFinancial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provider_id'], 'integer'],
            [['bank_info', 'vat_rate', 'license_type', 'license_expiration_date', 'license_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DestinationsFinancial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'license_expiration_date' => $this->license_expiration_date,
        ]);

        $query->andFilterWhere(['like', 'bank_info', $this->bank_info])
            ->andFilterWhere(['like', 'vat_rate', $this->vat_rate])
            ->andFilterWhere(['like', 'license_type', $this->license_type])
            ->andFilterWhere(['like', 'license_image', $this->license_image]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookable_items_housekeeping_status".
 *
 * @property integer $id
 * @property integer $bookable_item_id
 * @property integer $housekeeping_status_id
 *
 * @property BookableItems $bookableItem
 * @property HousekeepingStatus $housekeepingStatus
 */
class BookableItemsHousekeepingStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookable_items_housekeeping_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bookable_item_id', 'housekeeping_status_id'], 'integer'],
            [['bookable_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookableItems::className(), 'targetAttribute' => ['bookable_item_id' => 'id']],
            [['housekeeping_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => HousekeepingStatus::className(), 'targetAttribute' => ['housekeeping_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bookable_item_id' => 'Bookable Item ID',
            'housekeeping_status_id' => 'Housekeeping Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookableItem()
    {
        return $this->hasOne(BookableItems::className(), ['id' => 'bookable_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHousekeepingStatus()
    {
        return $this->hasOne(HousekeepingStatus::className(), ['id' => 'housekeeping_status_id']);
    }
}

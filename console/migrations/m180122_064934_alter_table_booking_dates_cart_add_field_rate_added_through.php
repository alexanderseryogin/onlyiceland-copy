<?php

use yii\db\Migration;

class m180122_064934_alter_table_booking_dates_cart_add_field_rate_added_through extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `booking_dates_cart`   
                          ADD COLUMN `rate_added_through` INT(11) DEFAULT 0 AFTER `custom_rate`;
                        ');
    }

    public function safeDown()
    {
        echo "m180122_064934_alter_table_booking_dates_cart_add_field_rate_added_through cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180122_064934_alter_table_booking_dates_cart_add_field_rate_added_through cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170322_081735_alter_bookings_items extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` ADD COLUMN `no_of_people` INT(11) NULL AFTER `beds_combinations_id`, ADD COLUMN `upsell_extra_bed_quantity` INT(11) NULL AFTER `no_of_people`; 
        ");
    }

    public function down()
    {
        echo "m170322_081735_alter_bookings_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_guests_cart".
 *
 * @property string $id
 * @property string $first_name
 * @property string $last_name
 * @property integer $country_id
 *
 * @property Countries $country
 */
class BookingGuestsCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_guests_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    public function getBookingsItems()
    {
        return $this->hasMany(BookingsItemsCart::className(), ['guest_id' => 'id']);
    }
}

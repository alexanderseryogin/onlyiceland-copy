<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Rates */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(isset($_SESSION['copy_model']) && !empty($_SESSION['copy_model']))
{
	$model = $_SESSION['copy_model'];

	$model->rate_allowed = explode(';', $model->rate_allowed);
    $model->check_in_allowed = explode(';', $model->check_in_allowed);
    $model->check_out_allowed = explode(';', $model->check_out_allowed);

	unset($_SESSION['copy_model']);
}
                
?>
<div class="rates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tab_href' => $tab_href,
        'multiNightDiscount' => $multiNightDiscount,
        'amenities' => $amenities
    ]) ?>

</div>

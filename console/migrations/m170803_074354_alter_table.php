<?php

use yii\db\Migration;

class m170803_074354_alter_table extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `bookings_items_cart`   
                          DROP COLUMN `booking_number`;
                        ');
    }

    public function down()
    {
        echo "m170803_074354_alter_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

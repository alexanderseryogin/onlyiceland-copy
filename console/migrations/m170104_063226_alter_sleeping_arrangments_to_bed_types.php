<?php

use yii\db\Migration;

class m170104_063226_alter_sleeping_arrangments_to_bed_types extends Migration
{
    public function up()
    {
        $this->execute("
            RENAME TABLE `sleeping_arrangements` TO `bed_types`; 
            CREATE TABLE `sleeping_arrangements`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`) );
            ");
    }

    public function down()
    {
        echo "m170104_063226_alter_sleeping_arrangments_to_bed_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

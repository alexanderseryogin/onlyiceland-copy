<?php

use yii\db\Migration;

/**
 * Handles the creation for table `places_of_interest`.
 */
class m160930_131407_create_places_of_interest_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("CREATE TABLE `places_of_interest`(  
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `region_id` INT(11),
          `name` VARCHAR(256) NOT NULL,
          `description` TEXT NOT NULL,
          `title_picture` VARCHAR(256),
          PRIMARY KEY (`id`),
          CONSTRAINT `FK_places_of_interest_region_id` FOREIGN KEY (`region_id`) REFERENCES `regions`(`id`) ON DELETE SET NULL
        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('places_of_interest');
    }
}

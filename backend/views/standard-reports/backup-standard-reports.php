<?php
public function getGrossRevenueReportDetailsBasedOnDates($destination_id,$report_to,$report_from,$cancelled_bookings)
{
    $destination = Destinations::findOne(['id' => $destination_id]);
    $occupancy_report = array();
    $date = $report_from;
   // $arrival_date = str_replace('/', '-', $arrival_date);
    $report_from_date = date('Y-m-d',strtotime($report_from));

    //$departure_date = str_replace('/', '-', $departure_date);
    $report_to_date = date('Y-m-d',strtotime($report_to));

    $year1 = date('Y', strtotime($report_from));
    $year2 = date('Y', strtotime($report_to));

    $month1 = date('m', strtotime($report_from));
    $month2 = date('m', strtotime($report_to));

    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

    $date = date('Y-m', strtotime($report_from));
    $final_result = array();
    if($diff == 0)
    {
        if($cancelled_bookings)
        {
            $month_arr = array();

            $search_date = $date.'-%%'; 
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                
            SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
            (
            SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
            (
            SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
            FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
            WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id."
            GROUP BY `BI`.`id`
            UNION 
            SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
            FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
            WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
            GROUP BY `BI`.`id` 
            ) AS `A`
            GROUP BY `A`.id
            ) AS `B`
            GROUP BY `B`.`item_id`;

             ");

            $result = $command->queryAll();
            // echo "<pre>";
            // print_r($result);
            // exit();
            foreach ($result as  $value) 
            {
                $month_arr[$value['item_id']] = $value['final'];
            }

            $month_name = date('Y',strtotime($date));
            $month_year = date('m',strtotime($date));
            $final_result[$month_name.'.'.$month_year] = $month_arr;
        }
        else
        {   
            $month_arr = array();

            $search_date = $date.'-%%'; 
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                
            SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
            (
            SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
            (
            SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
            FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
            WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id."
            GROUP BY `BI`.`id`
            UNION 
            SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
            FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
            WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
            GROUP BY `BI`.`id`
            ) AS `A`
            GROUP BY `A`.id
            ) AS `B`
            GROUP BY `B`.`item_id`;

             ");

            $result = $command->queryAll();
            // echo "<pre>";
            // print_r($result);
            // exit();
            foreach ($result as  $value) 
            {
                $month_arr[$value['item_id']] = $value['final'];
            }

            $month_name = date('Y',strtotime($date));
            $month_year = date('m',strtotime($date));
            $final_result[$month_name.'.'.$month_year] = $month_arr;
        }
            
    }
        
    else
    {
        for ($i=0; $i <= $diff ; $i++) 
        { 
            $month_arr = array();
            $search_date = $date.'-%%';

            // echo $search_date."<br>";
            if($i==0)
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id."
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id."
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;   
                }
                    
               
            }
            else if($i == $diff && $i != 0)
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id."
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id."
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                    
            }
            else
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id."
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id."
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                    
            }
            $date=date("Y-m", strtotime("+1 month", strtotime($date)));
        }
    }
   
    $data_array = array();
    // $final_report = array();
    // echo "<pre>";
    // print_r($final_result);
    // exit();
    
    foreach ($final_result as $key => $value) 
    {
        // foreach ($value as $key1 => $value1) 
        // {
            foreach ($destination->bookableItems as $b_item) 
            {
                // echo "<pre>";
                // print_r($b_item->id);
                // exit();
                
                if(array_key_exists($b_item->id, $value))
                {
                    $data_array[$key][$b_item->itemType->name]['gross_amount']   = $value[$b_item->id];   
                }
                else
                {
                    $data_array[$key][$b_item->itemType->name]['gross_amount']   = 0;  
                }
                
        }
        // exit();
    
        
    }
    
    //      echo "<pre>";
    // print_r($data_array);
    // exit();
        //$data_array[$b_item->itemType->name]['avail']   = 0;

    return $data_array;
}

public function getGrossRevenueReportDetailsBasedOnDepartureDates($destination_id,$report_to,$report_from,$cancelled_bookings)
{
    $destination = Destinations::findOne(['id' => $destination_id]);
    $occupancy_report = array();
    $date = $report_from;
   // $arrival_date = str_replace('/', '-', $arrival_date);
    $report_from_date = date('Y-m-d',strtotime($report_from));

    //$departure_date = str_replace('/', '-', $departure_date);
    $report_to_date = date('Y-m-d',strtotime($report_to));

    $year1 = date('Y', strtotime($report_from));
    $year2 = date('Y', strtotime($report_to));

    $month1 = date('m', strtotime($report_from));
    $month2 = date('m', strtotime($report_to));

    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

    $date = date('Y-m', strtotime($report_from));
    $final_result = array();
    if($diff == 0)
    {
        if($cancelled_bookings)
        {
            $month_arr = array();

            $search_date = $date.'-%%'; 
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                
            SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
            (
            SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
            (
            SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
            FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
            WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."' AND `BI`.`departure_date` <='".$report_to_date."'
            GROUP BY `BI`.`id`
            UNION 
            SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
            FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
            WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."' AND `BI`.`departure_date` <='".$report_to_date."'
            GROUP BY `BI`.`id` 
            ) AS `A`
            GROUP BY `A`.id
            ) AS `B`
            GROUP BY `B`.`item_id`;

             ");

            $result = $command->queryAll();
            // echo "<pre>";
            // print_r($result);
            // exit();
            foreach ($result as  $value) 
            {
                $month_arr[$value['item_id']] = $value['final'];
            }

            $month_name = date('Y',strtotime($date));
            $month_year = date('m',strtotime($date));
            $final_result[$month_name.'.'.$month_year] = $month_arr;
        }
        else
        {   
            $month_arr = array();

            $search_date = $date.'-%%'; 
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                
            SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
            (
            SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
            (
            SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
            FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
            WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."' AND `BI`.`departure_date` <='".$report_to_date."'
            GROUP BY `BI`.`id`
            UNION 
            SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
            FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
            WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."' AND `BI`.`departure_date` <='".$report_to_date."'
            GROUP BY `BI`.`id`
            ) AS `A`
            GROUP BY `A`.id
            ) AS `B`
            GROUP BY `B`.`item_id`;

             ");

            $result = $command->queryAll();
            // echo "<pre>";
            // print_r($result);
            // exit();
            foreach ($result as  $value) 
            {
                $month_arr[$value['item_id']] = $value['final'];
            }

            $month_name = date('Y',strtotime($date));
            $month_year = date('m',strtotime($date));
            $final_result[$month_name.'.'.$month_year] = $month_arr;
        }
            
    }
        
    else
    {
        for ($i=0; $i <= $diff ; $i++) 
        { 
            $month_arr = array();
            $search_date = $date.'-%%';

            // echo $search_date."<br>";
            if($i==0)
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;   
                }
                    
               
            }
            else if($i == $diff && $i != 0)
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                    
            }
            else
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` <='".$report_to_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` <='".$report_to_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` <='".$report_to_date."' 
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`departure_date` LIKE '".$search_date."' AND `BI`.`departure_date` <='".$report_to_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                    
            }
            $date=date("Y-m", strtotime("+1 month", strtotime($date)));
        }
    }
   
    $data_array = array();
    // $final_report = array();
    // echo "<pre>";
    // print_r($final_result);
    // exit();
    
    foreach ($final_result as $key => $value) 
    {
        // foreach ($value as $key1 => $value1) 
        // {
            foreach ($destination->bookableItems as $b_item) 
            {
                // echo "<pre>";
                // print_r($b_item->id);
                // exit();
                
                if(array_key_exists($b_item->id, $value))
                {
                    $data_array[$key][$b_item->itemType->name]['gross_amount']   = $value[$b_item->id];   
                }
                else
                {
                    $data_array[$key][$b_item->itemType->name]['gross_amount']   = 0;  
                }
                
        }
        // exit();
    
        
    }
    
    //      echo "<pre>";
    // print_r($data_array);
    // exit();
        //$data_array[$b_item->itemType->name]['avail']   = 0;

    return $data_array;
}

public function getGrossRevenueReportDetailsBasedOnArrivalDates($destination_id,$report_to,$report_from,$cancelled_bookings)
{
    $destination = Destinations::findOne(['id' => $destination_id]);
    $occupancy_report = array();
    $date = $report_from;
   // $arrival_date = str_replace('/', '-', $arrival_date);
    $report_from_date = date('Y-m-d',strtotime($report_from));

    //$departure_date = str_replace('/', '-', $departure_date);
    $report_to_date = date('Y-m-d',strtotime($report_to));

    $year1 = date('Y', strtotime($report_from));
    $year2 = date('Y', strtotime($report_to));

    $month1 = date('m', strtotime($report_from));
    $month2 = date('m', strtotime($report_to));

    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

    $date = date('Y-m', strtotime($report_from));
    $final_result = array();
    if($diff == 0)
    {
        if($cancelled_bookings)
        {
            $month_arr = array();

            $search_date = $date.'-%%'; 
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                
            SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
            (
            SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
            (
            SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
            FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
            WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
            GROUP BY `BI`.`id`
            UNION 
            SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
            FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
            WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
            GROUP BY `BI`.`id` 
            ) AS `A`
            GROUP BY `A`.id
            ) AS `B`
            GROUP BY `B`.`item_id`;

             ");

            $result = $command->queryAll();
            // echo "<pre>";
            // print_r($result);
            // exit();
            foreach ($result as  $value) 
            {
                $month_arr[$value['item_id']] = $value['final'];
            }

            $month_name = date('Y',strtotime($date));
            $month_year = date('m',strtotime($date));
            $final_result[$month_name.'.'.$month_year] = $month_arr;
        }
        else
        {   
            $month_arr = array();

            $search_date = $date.'-%%'; 
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                
            SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
            (
            SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
            (
            SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
            FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
            WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
            GROUP BY `BI`.`id`
            UNION 
            SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
            FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
            WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
            GROUP BY `BI`.`id`
            ) AS `A`
            GROUP BY `A`.id
            ) AS `B`
            GROUP BY `B`.`item_id`;

             ");

            $result = $command->queryAll();
            // echo "<pre>";
            // print_r($result);
            // exit();
            foreach ($result as  $value) 
            {
                $month_arr[$value['item_id']] = $value['final'];
            }

            $month_name = date('Y',strtotime($date));
            $month_year = date('m',strtotime($date));
            $final_result[$month_name.'.'.$month_year] = $month_arr;
        }
            
    }
        
    else
    {
        for ($i=0; $i <= $diff ; $i++) 
        { 
            $month_arr = array();
            $search_date = $date.'-%%';

            // echo $search_date."<br>";
            if($i==0)
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` >='".$report_from_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` >='".$report_from_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` >='".$report_from_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;   
                }
                    
               
            }
            else if($i == $diff && $i != 0)
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                    
            }
            else
            {
                if($cancelled_bookings)
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                else
                {
                    $search_date = $date.'-%%'; 
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        
                    SELECT `B`.`item_id`,SUM(`B`.`final`) as final FROM
                    (
                    SELECT `A`.id,`A`.`item_id`,SUM(`A`.`total`) AS total,SUM(`A`.`days`) AS days,SUM(`A`.`no_of_nights`) AS nights,(SUM(`A`.`total`)/SUM(`A`.`no_of_nights`))*SUM(`A`.`days`) AS final FROM 
                    (
                    SELECT  `BI`.`id`,SUM(`BD`.`person_price`) AS total , COUNT(`BD`.`id`) AS days, `BD`.`no_of_nights` ,`BI`.`item_id`
                    FROM (booking_dates AS `BD` INNER JOIN bookings_items `BI` ON `BD`.`booking_item_id` = `BI`.`id`)
                    WHERE `BD`.`date` LIKE '".$search_date."' AND `BD`.`date` <='".$report_to_date."' AND `BI`.`deleted_at` = 0 AND `BI`.`status_id` != 3 AND `BI`.`provider_id` = ".$destination->id." AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` <='".$report_to_date."' 
                    GROUP BY `BI`.`id`
                    UNION 
                    SELECT `BI`.`id`,SUM(`BDF`.`amount`),0 AS days,0 AS nights,`BI`.`item_id`
                    FROM `bookings_items` AS `BI` LEFT JOIN `booking_date_finances` AS `BDF` ON `BI`.`id` = `BDF`.`booking_item_id`
                    WHERE `BI`.`deleted_at` = 0 AND `BI`.`provider_id` = ".$destination->id." AND `BDF`.`date` LIKE '".$search_date."' AND `BDF`.`date` <='".$report_to_date."' AND (`BDF`.`item_type` = 3 OR `BDF`.`item_type` = 2) AND `BDF`.`type` = 1 AND `BI`.`arrival_date` LIKE '".$search_date."' AND `BI`.`arrival_date` <='".$report_to_date."'
                    GROUP BY `BI`.`id`
                    ) AS `A`
                    GROUP BY `A`.id
                    ) AS `B`
                    GROUP BY `B`.`item_id`;

                     ");
                    $result = $command->queryAll();
                    foreach ($result as  $value) 
                    {
                        $month_arr[$value['item_id']] = $value['final'];
                    }

                    $month_name = date('Y',strtotime($date));
                    $month_year = date('m',strtotime($date));
                    $final_result[$month_name.'.'.$month_year] = $month_arr;
                }
                    
            }
            $date=date("Y-m", strtotime("+1 month", strtotime($date)));
        }
    }
   
    $data_array = array();
    // $final_report = array();
    // echo "<pre>";
    // print_r($final_result);
    // exit();
    
    foreach ($final_result as $key => $value) 
    {
        // foreach ($value as $key1 => $value1) 
        // {
            foreach ($destination->bookableItems as $b_item) 
            {
                // echo "<pre>";
                // print_r($b_item->id);
                // exit();
                
                if(array_key_exists($b_item->id, $value))
                {
                    $data_array[$key][$b_item->itemType->name]['gross_amount']   = $value[$b_item->id];   
                }
                else
                {
                    $data_array[$key][$b_item->itemType->name]['gross_amount']   = 0;  
                }
                
        }
        // exit();
    
        
    }
    
    //      echo "<pre>";
    // print_r($data_array);
    // exit();
        //$data_array[$b_item->itemType->name]['avail']   = 0;

    return $data_array;
}
<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "destination_types".
 *
 * @property integer $id
 * @property string $logo
 * @property string $name
 */
class AttractionTypes extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attraction_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'name'], 'string', 'max' => 256],
            ['name','required'],
            [['name'], 'unique'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png','jpeg','jpg']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'logo' => Yii::t('app', 'Logo'),
            'name' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Upload Logo'),
        ];
    }
}

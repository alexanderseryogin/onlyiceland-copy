<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Regions;
use yii\helpers\ArrayHelper;
use common\models\PropertyTypes;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PropertiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Properties');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properties-index">
    <p>
        <?= Html::a(Yii::t('app', 'ADD NEW PROPERTY'), ['create'], ['class' => 'pull-right btn btn-primary']) ?>
    </p>
    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',            
            'name',
            //'owner_id',
            [
                'attribute' => 'owner_id',
                'value' => 'owner.username',
                'filter' => Html::textInput('PropertiesSearch[user]', $searchModel['user'], ['class'=>'form-control'])
             ],
            //'property_type_id',
            [
                'attribute' => 'property_type_id',
                //'label' => 'Client Name',
                'value' => function($data){
                    return isset($data->propertyType->name) ? $data->propertyType->name : 'N/A';
                },
                'filter' => Html::dropDownList(
                    'PropertiesSearch[property_type_id]', 
                    $searchModel['property_type_id'], 
                    ArrayHelper::map(PropertyTypes::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Type', 
                        'id'=>'PropertiesSearch-regions', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'width:10%'],
            ],
            //'region_id',
            [
                'attribute' => 'region_id',
                //'label' => 'Client Name',
                'value' => function($data){
                    return isset($data->region->name) ? $data->region->name : 'N/A';
                },
                'filter' => Html::dropDownList(
                    'PropertiesSearch[region_id]', 
                    $searchModel['region_id'], 
                    ArrayHelper::map(Regions::find()->all(),'id','name'), 
                    [
                        'prompt' => 'Select Region', 
                        'id'=>'PropertiesSearch-regions', 
                        'class'=>'form-control'
                    ]),
                'contentOptions' =>['style' => 'width:10%'],
            ],
            //'city_id',
            [
                'attribute' => 'city_id',
                'value' => 'city.name',
                'filter' => Html::textInput('PropertiesSearch[city]', $searchModel['city'], ['class'=>'form-control'])
             ],
            'reservations_email:email',
            // 'address:ntext',
            // 'city_id',
            // 'postal_code',
            // 'latitude',
            // 'longitude',
            // 'reservations_email:email',
            // 'business_name',
            // 'business_id_number',
            // 'business_address:ntext',
            // 'business_city_id',
            // 'business_postal_code',
            // 'invoice_email:email',
            // 'youngest_age',
            // 'payment_methods_for_gauarantee:ntext',
            // 'payment_methods_checkin:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>

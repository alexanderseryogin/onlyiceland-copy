<?php

use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;

use common\models\User;
use common\models\BookableItems;
use common\models\Destinations;
use common\models\TravelPartner;
use common\models\Offers;
use common\models\BookingTypes;
use common\models\BookingStatuses;
use common\models\BookingConfirmationTypes;
use common\models\Rates;
use common\models\Countries;
use common\models\EstimatedArrivalTimes;
$Options = [
        0 => '',
        1 => 'checked',
        2 => 'selected',
        3 => 'disabled',
    ];
?>

<script type="text/javascript">
var isNewRecord = '<?= $model->isNewRecord ?>';
</script>

<div class="tab-pane active" id="general_info">
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'provider_id', [
                'inputOptions' => [
                    'id' => 'provider_dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Destinations::find()->where(['active' => 1])->all(),'id','name'),['prompt'=>'Select a Destination',
                    'onchange' => 'App.blockUI({target:".bookings-form", animate: !0}); $.post("'.Yii::$app->urlManager->createUrl('bookings/listitems?provider_id=').'"+$(this).val(), function( data ) 
                        {
                            data = jQuery.parseJSON( data );
                            $("#estimated_arrival_time_dropdown").empty().html(data.estimated_arrival_times);

                            if(isNewRecord==1)
                            {
                                $("#cancellation_dropdown").val(data.general_booking).trigger("change");
                                $("#flag_dropdown").val(data.flag).trigger("change");
                                $("#status_dropdown").val(data.status).trigger("change");
                                $("#confirmation_dropdown").val(data.confirmation).trigger("change");

                                if(data.offers.length == 4)
                                {
                                    $(".offers_dropdown_div").hide();
                                }
                                else
                                {
                                    $(".offers_dropdown_div").show();
                                }

                                $("#offers_dropdown").empty().html(data.offers);
                                $("#special_requests").empty().html(data.special_requests);
                                $("#travel_partner_dropdown").empty().html(data.travel_partners);
                                pricing_type = data.pricing_type;
                                $("#travel_partner_dropdown").attr("disabled",false);
                                $("#offers_dropdown").attr("disabled",false);
                            }
                            
                        }).done(function() 
                        {
                            App.unblockUI(".bookings-form");
                        })
                        .fail(function() 
                        {
                            alert( "error" );
                            App.unblockUI(".bookings-form");
                        });'
                ]) ?>
        </div>
        <div class="col-sm-3">
            <label class="control-label">Travel Partner</label>
            <div class="form-group" >
                <select style="width: 100%;" class="form-control" id="travel_partner_dropdown">
                    <option value=""> Select Referrer </option>
                </select>
            </div>
        </div>
        <div class="col-sm-3 offers_dropdown_div">
            <label class="control-label">Offers</label>
            <div class="form-group" >
                <select style="width: 100%;" class="form-control" id="offers_dropdown">
                    <option value=""> Select an Offer </option>
                </select>
            </div>
        </div>

        <div class="col-sm-3">
            <label class="control-label" >Estimated Arrival Time</label>
            <div class="form-group" >
                <select style="width: 100%;" class="form-control" id="estimated_arrival_time_dropdown" name="Bookings[estimated_arrival_time_id]" >
                    <?php
                        if(!empty($model->provider_id))
                        {
                            $estimated_arrival_times = EstimatedArrivalTimes::find()->where(['time_group' => $model->provider->bookingPolicies->time_group])->all();

                            if(!empty($estimated_arrival_times))
                            {
                                foreach ($estimated_arrival_times as $key =>  $obj) 
                                {
                                    $start_time = '';

                                    if(empty($obj->text))
                                    {
                                        $start_time = date('H:i',strtotime($obj->start_time));
                                    }
                                    else
                                    {
                                        $start_time = $obj->text;
                                    }
                                    $obj->end_time = date('H:i',strtotime($obj->end_time));
                                    
                                    echo "<option value='".$obj->id."'>".$start_time." - ".$obj->end_time."</option>";  
                                }
                            }  
                        }
                    ?>
                </select>
            </div>
        </div>

        <!--<div class="col-sm-2 pull-right">
         //################  shifted booking group button to sidebar #####################//
             <br><a id="group_settings" class="btn btn-sm btn-warning">Group Settings</a> 
        </div> -->
    </div>

    <?php
        $disabled = '';
        $display = '';

        if(!$model->isNewRecord)
        {
            $disabled = 'disabled';
            $display = 'none';
        }
    ?>

	<div class="row">
        <div class="col-sm-3 hide-fields">
            <label class="control-label">Arrival Date/Departure Date</label>
            <div class="form-group" >
                <div class="input-group input-medium date-picker input-daterange" data-date-start-date="0d" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control" id="arrival_date" value="<?=$model->arrival_date?>" name="BookingsItems[arrival_date]" required <?php echo $disabled; ?>>
                    <span class="input-group-addon"> to </span>
                    <input type="text" class="form-control" id="departure_date" value="<?=$model->departure_date?>" name="BookingsItems[departure_date]" required <?php echo $disabled; ?>>
                </div>
            </div>
        </div>

        <div class="col-sm-1 hide-fields" style="margin-top:30px; margin-left: -20px">
            <button id="today" type="button" class="btn btn-xs btn-primary">TODAY</button>
        </div>

        <div class="col-sm-3 hide-fields" style="display: <?php echo $display ?>">
            <?= $form->field($model, 'rate_conditions')->checkbox(array('labelOptions'=>array('style'=>'margin-top:30px;'))); ?>
        </div>

        <div class="col-sm-2 pull-right hide-fields">
            <br>
            <button id="check_availability" type="button" class="btn btn-primary">Check Availability</button>
        </div>

	</div>

    <div id="details_note">
        <span class="label label-danger"> Note! </span>
        <span>&nbsp; Select a Destination to continue</span>
    </div>

    <div class="row hide-fields">
        <div class="col-sm-10" id="special_requests">
        </div>
        <div class="col-sm-2 pull-right">
            <br>
            <button id="panel_handler" style="display: none" type="button" class="btn btn-primary">Expand All Items</button>
        </div>
    </div>

    <input type="text" id="selected_arrival_date" value="<?=$model->arrival_date?>" name="BookingsItems[selected_arrival_date]" hidden>
    <input type="text" id="selected_departure_date" value="<?=$model->departure_date?>" name="BookingsItems[selected_departure_date]" hidden>
    <br>
    <div class="hide-fields available-rates">
    </div>
    <div id="hidden_available_rates" style="display: none">
    </div>
</div>

<div id="group-modal" class="modal fade bs-modal-md" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Group Settings</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <label class=" control-label">Group Name</label>
                        <input id="modal_group_name" name="BookingGroupCart[group_name]" type="text" value="<?php echo ($bookingGroupModel->group_name== null)?$group_name:$bookingGroupModel->group_name ?>" class="form-control">
                    </div>
                </div>
                <br>
                <!-- ####### adding radio button in group modal #######-->
                <div class="row">
                    <div class="col-sm-2">
                        <input id="modal-guest-radio" class="modal-user-radio" type="radio" name="selected_user" value="guest_user" checked> Guest<br>
                        
                    
                    </div>
                    <div class="col-sm-2">
                       <input id="modal-user-radio" class="modal-user-radio" type="radio" name="selected_user" value="user" > User<br>
                        
                    </div>
                </div>
                <!-- #######  #######-->
                <br>
                <div class="row hide" id="modal-user-dropdown" >
                    <div class="col-sm-11">
                        <?= $form->field($bookingGroupModel, 'group_user_id', [
                            'inputOptions' => [
                                'id' => 'user_dropdown',
                                'class' => 'form-control',
                                'style' => 'width: 100%',
                                ]
                        ])->dropdownList(ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_USER])->all(),
                            'id',
                            function($model, $defaultValue) 
                            {
                                if(empty($model->profile->country_id))
                                    $country = '';
                                else
                                    $country = ' - '.$model->profile->country->name;

                                return $model->username.' - '.$model->email.$country;
                            }
                        ),['prompt'=>'Select a User']) ?>
                    </div>
                    <div class="col-sm-1" style="margin-top:35px;">
                        <a id="add_user" ><i class="fa fa-plus fa-2x" style="color: #EC971F" aria-hidden="true"></i></a>
                    </div>
                </div>
                <!-- ####### adding guest details in group modal #######-->
                <div class="row" id="modal-guest-form">
                    <div class="col-sm-4">
                        <label class="control-label">Guest First Name</label>
                        <div class="form-group" >
                            <input type="text" id="modal-guest-first-name" class="form-control" name ="guest_first_name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Guest Last Name</label>
                        <div class="form-group" >
                            <input type="text" id="modal-guest-last-name" class="form-control" name ="guest_last_name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Guest Country</label>
                        <div class="form-group" >
                            <select id="modal-guest-country-dropdown" style="width: 100%;" class="form-control" name ="guest_country">
                                <?php
                                    $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                    foreach ($countries as $key => $value) 
                                    {
                                        $selected = ($value == 'Iceland') ? 2 : 0 ;
                                        echo '<option value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label"><strong>Comment</strong></label>
                        <div class="form-group" >
                            <textarea class="form-control modal-comment>" type="" name="group_comment" rows="3"></textarea>
                        </div>
                    </div>
                </div>

                <!-- #######  #######-->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="add-user-modal" tabindex="1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New User</h4>
            </div>

            <div class="modal-body user-modal-body">

                <div class="alert alert-danger modal-errors-block"></div>
                
                <div class="row">
                    <div class="col-sm-3">
                        <label class=" control-label">Username</label>
                        <input id="username" type="text" class="form-control user-modal-field">
                        <div class="modal-help-block">Username cannot be blank.</div>
                    </div>
                    <div class="col-sm-3">
                        <label class=" control-label">Email</label>
                        <input id="email" type="email" class="form-control user-modal-field" >
                        <div class="modal-help-block">Email cannot be blank.</div>
                        <div class="email-help-block">Email is not a valid email address.</div>
                    </div>
                    <div class="col-sm-3">
                        <label class=" control-label">First Name</label>
                        <input id="fname" type="text" class="form-control user-modal-field">
                        <div class="modal-help-block">First Name cannot be blank.</div>
                    </div>
                    <div class="col-sm-3">
                        <label class=" control-label">Last Name</label>
                        <input id="lname" type="text" class="form-control user-modal-field">
                        <div class="modal-help-block">Last Name cannot be blank.</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <label class="control-label">Country</label>
                        <div class="form-group" >
                            <select style="width: 100%;" class="form-control" id="country_dropdown" >
                                <?php
                                    $countries = ArrayHelper::map(Countries::find()->all(),'id','name');

                                    foreach ($countries as $key => $value) 
                                    {
                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">State</label>
                        <div class="form-group" >
                            <select style="width: 100%;" class="form-control" id="state_dropdown" >
                                <option value=''></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">City</label>
                        <div class="form-group" >
                            <select style="width: 100%;" class="form-control" id="city_dropdown" >
                                <option value=''></option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success mt-ladda-btn ladda-button create-btn" data-style="slide-down">
                    Create
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
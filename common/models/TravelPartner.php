<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "travel_partner".
 *
 * @property integer $id
 * @property string $company_name
 * @property string $phone
 * @property string $emergency_number
 * @property string $booking_email
 * @property string $invoice_email
 * @property string $commission
 * @property string $kennitala
 * @property string $address
 * @property string $second_address
 * @property integer $country_id
 * @property integer $state_id
 * @property integer $city_id
 * @property string $postal_code
 *
 * @property Cities $city
 * @property Countries $country
 * @property States $state
 */
class TravelPartner extends \yii\db\ActiveRecord
{
    public $server_phone='';
    public $server_emergency='';

    const CALC_DISCOUNT = 1;
    const CALC_COMMISSION = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'travel_partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'state_id', 'city_id','company_name','booking_email', 'invoice_email','address','postal_code'], 'required'],

            [['booking_email', 'invoice_email'],'email'],
            [['address', 'second_address','server_emergency','server_phone'], 'string'],
            [['commission'], 'integer','min' => 0],
            [['company_name', 'phone', 'emergency_number', 'booking_email', 'invoice_email', 'kennitala', 'postal_code','primary_contact','billing_contact'], 'string', 'max' => 256],
            [['company_name'], 'unique'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [ ['calculation' ],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'phone' => Yii::t('app', 'Telephone Number'),
            'emergency_number' => Yii::t('app', 'Emergency Number'),
            'booking_email' => Yii::t('app', 'Email address (for bookings)'),
            'invoice_email' => Yii::t('app', 'Email address (for invoices)'),
            'commission' => Yii::t('app', 'Commission %'),
            'kennitala' => Yii::t('app', 'Kennitala'),
            'address' => Yii::t('app', 'Address'),
            'second_address' => Yii::t('app', 'Second Address'),
            'country_id' => Yii::t('app', 'Country'),
            'state_id' => Yii::t('app', 'State'),
            'city_id' => Yii::t('app', 'City'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'primary_contact' => Yii::t('app', 'Primary Contact'),
            'billing_contact' => Yii::t('app', 'Billing Contact'),  
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }
}

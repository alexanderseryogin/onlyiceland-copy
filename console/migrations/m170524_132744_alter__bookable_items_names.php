<?php

use yii\db\Migration;

class m170524_132744_alter__bookable_items_names extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items_names` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;");
    }

    public function down()
    {
        echo "m170524_132744_alter__bookable_items_names cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

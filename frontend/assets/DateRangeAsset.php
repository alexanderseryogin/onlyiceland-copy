<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class DateRangeAsset extends AssetBundle
{
    public $sourcePath = '@metronic/resources';

    public $css = [
        //'global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
        //'global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        //'global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
    ];
    public $js = [
        //'global/plugins/moment.min.js',
        //'global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
        //'global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        //'pages/scripts/components-date-time-pickers.min.js',
        //'global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    ];
}
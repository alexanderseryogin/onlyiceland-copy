<?php

use yii\db\Migration;

class m180521_084220_alter_table_add_on_add_commissionable_column extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('add_on', 'commissionable', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('add_on', 'commissionable');
    }
}

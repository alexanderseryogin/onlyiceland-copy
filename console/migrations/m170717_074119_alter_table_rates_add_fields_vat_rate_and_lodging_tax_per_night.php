<?php

use yii\db\Migration;

class m170717_074119_alter_table_rates_add_fields_vat_rate_and_lodging_tax_per_night extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `rates`   
                          ADD COLUMN `vat_rate` VARCHAR(256) NULL AFTER `general_booking_cancellation`,
                          ADD COLUMN `lodging_tax_per_night` INT(11) NULL AFTER `vat_rate`;
                        ');
    }

    public function down()
    {
        echo "m170717_074119_alter_table_rates_add_fields_vat_rate_and_lodging_tax_per_night cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

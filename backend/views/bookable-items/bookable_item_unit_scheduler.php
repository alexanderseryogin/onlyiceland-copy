
<?php
	use common\models\BookableItems;
	use common\models\Destinations;
	if(!empty($provider_id))
	{
		$destination = Destinations::findOne(['id' => $provider_id]);
		$items = BookableItems::find()
                ->where(['provider_id' => $provider_id])
                ->all();

		if(!empty($items))
		{
			// echo "<option value=''></option>";
			foreach ($items as $key =>  $item) 
		    {
		    	foreach ($item->bookableItemsNames as $item_name) 
                {
			    	echo '<li><a href="#" class="small" data-value="'.$item_name->id.'" tabIndex="-1"><input type="checkbox" class="bookable_items_checkboxes"  value="'.$item_name->id.'"/>&nbsp'.$item->itemType->name." - ".$item_name->item_name.'</a></li>';
			        // echo "<option value='".$item->id."'>".$destination->name.' - '.$item->itemType->name."</option>";  
			    }
		    }
		    echo '<li style="margin-bottom: 10px;">
				     <div class="row">
				        <div class="col-md-3" >
				          <a href="javescript:void(0);" class="small select_all_bookable_items" style="margin-left: 14px;" >Select all</a>
				       </div>
				       <div class="col-md-1" style="margin-left: -11%;">
				         |
				       </div>
				       <div class="col-md-3">
				          <a href="javescript:void(0);" class="small deselect_all_bookable_items" style="margin-left: -48%;">Deselect all</a>
				       </div>
				    </div>
				   </li>';
		}  
	}
	

?>


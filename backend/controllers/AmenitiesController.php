<?php

namespace backend\controllers;

use Yii;
use common\models\Amenities;
use common\models\AmenitiesSearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\models\UploadFile;

/**
 * AmenitiesController implements the CRUD actions for Amenities model.
 */
class AmenitiesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],  
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Amenities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmenitiesSearch();
        $params = Yii::$app->request->queryParams;

        $session = Yii::$app->session;
        $session->open();

        if(!empty($params))
        {
            if(isset($params['AmenitiesSearch']) && !$this->isArrayEmpty($params['AmenitiesSearch']) && !isset($params['page']))
            {
                $params['page'] = 1;
            }
            $session[Yii::$app->controller->id.'-grid'] = json_encode($params);
        }
        else if(isset($session[Yii::$app->controller->id.'-grid']) && !empty($session[Yii::$app->controller->id.'-grid']))
        {
            $params = json_decode($session[Yii::$app->controller->id.'-grid'], true);           
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Amenities model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Amenities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Amenities();

        if($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->save();
            
            if(empty($model->getErrors()))
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'temp_icon')) 
                {
                    if($uploadFile->uploadFile('amenities',$model->id,'icons'))
                    {
                        $model->icon = $uploadFile->FileName;
                        $model->update();
                    }
                }
            }
            
            return $this->redirect(['index']);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Amenities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $model->save();

            if(empty($model->getErrors()))
            {
                $uploadFile = new UploadFile();

                if ($uploadFile->FileToUpload = UploadedFile::getInstance($model,'temp_icon')) 
                {
                    if($uploadFile->uploadFile('amenities',$model->id,'icons'))
                    {
                        if(!empty($model->icon))
                        {
                            $prev_image = Yii::getAlias('@app').'/../uploads/amenities/'.$model->id.'/icons/'.$model->icon;
                            unlink($prev_image);
                        }

                        $model->icon = $uploadFile->FileName;
                        $model->update();
                    }
                }
            }
            return $this->redirect(['index']);
        } 
        else 
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function removeDirectory($path) 
    {
        $files = glob($path . '/*');
        foreach ($files as $file) 
        {
          is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }
    /**
     * Deletes an existing Amenities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $path = Yii::getAlias('@app'). '/../uploads/amenities/'.$id;
        if(is_dir($path))
        {
            $this->removeDirectory($path);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Amenities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Amenities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Amenities::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

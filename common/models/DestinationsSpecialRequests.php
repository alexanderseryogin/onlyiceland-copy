<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "providers_special_requests".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $request_id
 *
 * @property Providers $provider
 * @property SpecialRequests $request
 */
class DestinationsSpecialRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_special_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'request_id'], 'integer'],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpecialRequests::className(), 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Destination ID'),
            'request_id' => Yii::t('app', 'Request ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(SpecialRequests::className(), ['id' => 'request_id']);
    }
}

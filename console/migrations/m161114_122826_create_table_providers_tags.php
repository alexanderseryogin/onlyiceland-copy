<?php

use yii\db\Migration;

class m161114_122826_create_table_providers_tags extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `providers_tags`( `id` INT(11) NOT NULL AUTO_INCREMENT, `tag_id` INT(11), `provider_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `FK_provider_tags_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags`(`id`) ON DELETE CASCADE, CONSTRAINT `FK_provider_tags_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers`(`id`) ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci; 
        ");
    }

    public function down()
    {
        echo "m161114_122826_create_table_providers_tags cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

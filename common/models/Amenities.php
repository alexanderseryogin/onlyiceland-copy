<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "amenities".
 *
 * @property string $id
 * @property string $name
 * @property string $icon
 */
class Amenities extends \yii\db\ActiveRecord
{
    public static $types = ['Free','Available At Extra Cost','Not Available','Do not display','Display without comment'];

    public $temp_icon='';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'amenities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['icon'], 'string'],
            [['name','type'],'required'],
            [['name'], 'string', 'max' => 256],
            //[['name'], 'unique'],
            [['type'],'integer'],
            
            // checks if "primaryImage" is a valid image with proper size
            ['temp_icon', 'image','skipOnEmpty' => true, 'extensions' => 'png, jpg',
                'maxWidth' => 50,
                'maxHeight' => 50,
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'temp_icon' => Yii::t('app', 'Upload an Icon'),
        ];
    }

    public function getProvidersAmenities()
    {
        return $this->hasMany(DestinationsAmenities::className(), ['amenities_id' => 'id']);
    }
}

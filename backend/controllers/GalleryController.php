<?php

namespace backend\controllers;

use Yii;
use common\models\Gallery;
use common\models\GallerySearch;
use backend\components\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\helpers\FileHelper;
use common\models\PlacesOfInterest;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex($id=null, $type=null)
    {
        // $searchModel = new GallerySearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);

        $model = $this->getModel($id, $type);
        $gallery = null;
        if(!empty($model))
        {
            $gallery_type_id = "";
            switch($type)
            {
                case Gallery::GALLERY_TYPE_POI:
                    $gallery_type_id = "poi_id";
                    break;
                default:
                    return $this->redirect(['/']);
                    break;
            }
            $searchModel = new GallerySearch;
            $queryParams= Yii::$app->request->getQueryParams();
            $queryParams['Gallery']['type'] = $type;
            $queryParams['Gallery'][$gallery_type_id] = $id;
            $gallery = $searchModel->search($queryParams);
            // ->orderBy('id')
            // ->all();
        }
        // echo "<pre>";
        // print_r($gallery);
        // exit();
        return $this->render('gallery', [
            'model' => $model,
            'gallery' => $gallery,
            'id' => $id,
            'type' => $type,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        // $request = Yii::$app->request;
        // $id = $request->get('id');
        // $id = $request->get('id');
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImageUpload($id, $type)
    {
        $model = $this->getModel($id, $type);

        $imageFile = UploadedFile::getInstance($model, 'picture_name');
        $directory = realpath(dirname(__FILE__).'../../../').'/uploads/gallery/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $model->picture_name = $fileName;
                $model->save(false);
                $path = \Yii::getAlias('@web') . "/../uploads/gallery/".$fileName;
                return Json::encode([
                    'files' => [[
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        "url" => $path,
                        "thumbnailUrl" => $path,
                        "deleteUrl" => 'image-delete?name=' . $fileName,
                        "deleteType" => "POST"
                    ]]
                ]);
            }
        }
        return '';
    }

    public function actionImageDelete($name)
    {
        $directory = realpath(dirname(__FILE__).'../../../').'/uploads/gallery/';
        if (is_file($directory . DIRECTORY_SEPARATOR . $name)) {
            unlink($directory . DIRECTORY_SEPARATOR . $name);
        }
        $files = FileHelper::findFiles($directory);
        $output = [];
        foreach ($files as $file){
            $path = \Yii::getAlias('@web') . "/../uploads/gallery/".$file;
            $output['files'][] = [
                'name' => basename($file),
                'size' => filesize($file),
                "url" => $path,
                "thumbnailUrl" => $path,
                "deleteUrl" => 'image-delete?name=' . basename($file),
                "deleteType" => "POST"
            ];
        }
        return Json::encode($output);
    }

    private function getModel($id, $type)
    {
        $model = new Gallery();

        if(!isset($id) || !isset($type))
        {
            return $this->redirect(['/']);
        }

        switch($type)
        {
            case Gallery::GALLERY_TYPE_POI:
                if(empty(PlacesOfInterest::findOne(['id' => $id])))
                {
                    return $this->redirect(['/']);
                    exit();
                }
                else
                {
                    $model->poi_id = $id;
                    $model->type = Gallery::GALLERY_TYPE_POI;
                }
                break;
            default:
                return $this->redirect(['/']);
                exit();
                break;
        }

        return $model;
    }

}

<?php

namespace common\models;

use Yii;
use yii\web\JsExpression;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $title
 * @property string $first_name
 * @property string $last_name
 * @property string $company_name
 * @property string $address
 * @property string $postal_code
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $country_id
 * @property string $telephone_number
 * @property string $profile_picture
 * @property string $business_id_number
 * @property string $business_address
 * @property integer $business_city_id
 * @property integer $business_state_id
 * @property integer $business_country_id
 * @property string $business_postal_code
 *
 * @property Cities $businessCity
 * @property Countries $businessCountry
 * @property States $businessState
 * @property Cities $city
 * @property Countries $country
 * @property States $state
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    public $same_address;

    const TITLE_EMPTY = 'Select Title';
    const TITLE_MR = 'Mr.';
    const TITLE_MS = 'Ms.';
    const TITLE_MRS = 'Mrs.';
    const TITLE_DR ='Dr.';
    public $server_phone;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['same_address'], 'boolean'],
            /*[['business_address', 'business_postal_code'], 'required', 'when' => function ($model) {
                return !$model->same_address;
            }, 'whenClient' => new JsExpression("
                function (attribute, value) {
                    return $('#userprofile-same_address').is(':unchecked');
                }
            ")],*/
            [['user_id','first_name'], 'required'],
            ['server_phone','safe'],
            // 'title','city_id', 'state_id', 'country_id', 'business_city_id', 'business_state_id', 'business_country_id'
            [['user_id',  ], 'integer'],
            [['address', 'business_address','title'], 'string'],
            [['first_name', 'last_name', 'company_name', 'profile_picture', 'business_id_number', 'business_postal_code'], 'string', 'max' => 256],
            [['postal_code', 'telephone_number'], 'string', 'max' => 128],
            [['business_city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['business_city_id' => 'id']],
            [['business_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['business_country_id' => 'id']],
            [['business_state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['business_state_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'same_address' => Yii::t('app', 'Use same address for business?'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'company_name' => Yii::t('app', 'Business Name'),
            'address' => Yii::t('app', 'Address'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'city_id' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'telephone_number' => Yii::t('app', 'Telephone Number'),
            'profile_picture' => Yii::t('app', 'Profile Picture'),
            'business_id_number' => Yii::t('app', 'Kennitala'),
            'business_address' => Yii::t('app', 'Business Address'),
            'business_city_id' => Yii::t('app', 'Business City'),
            'business_state_id' => Yii::t('app', 'Business State'),
            'business_country_id' => Yii::t('app', 'Business Country'),
            'business_postal_code' => Yii::t('app', 'Business Postal Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'business_city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'business_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessState()
    {
        return $this->hasOne(States::className(), ['id' => 'business_state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation for table `gallery`.
 */
class m160930_135524_create_gallery_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("CREATE TABLE `gallery`(  
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `picture_name` VARCHAR(256) NOT NULL,
          `type` TINYINT NOT NULL,
          `poi_id` INT(11),
          `p_id` INT(11),
          PRIMARY KEY (`id`),
          CONSTRAINT `FK_gallery_places_of_interest_id` FOREIGN KEY (`poi_id`) REFERENCES `places_of_interest`(`id`) ON DELETE SET NULL,
          CONSTRAINT `FK_gallery_property_id` FOREIGN KEY (`p_id`) REFERENCES `properties`(`id`) ON DELETE SET NULL
        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('gallery');
    }
}

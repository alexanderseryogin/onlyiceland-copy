<?php

use yii\db\Migration;

class m170215_111004_alter_bookings_create_booking_dates extends Migration
{
    public function up()
    {
        $this->execute("
            ALTER TABLE `bookings` DROP COLUMN `arrival_date`, DROP COLUMN `departure_date`; 
            CREATE TABLE `booking_dates`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `booking_id` INT(11) UNSIGNED, `date` DATE, `over_ride` TINYINT DEFAULT 0, PRIMARY KEY (`id`), CONSTRAINT `FK_booking_dates_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `bookings`(`id`) ON UPDATE SET NULL ON DELETE CASCADE ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;  
        ");
    }

    public function down()
    {
        echo "m170215_111004_alter_bookings_create_booking_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

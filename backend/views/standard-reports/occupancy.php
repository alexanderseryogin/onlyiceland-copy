<?php

?>

<div class="tiles">
	<!--Daily Occupancy Report -->
			<div class="tile bg-purple-studio" id="daily_occupancy" data-toggle="tooltip" title="Arrivals, departures, occupied rooms, room nights, available rooms and occupancy per day" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Daily Occupancy </div>
			        <div class="number"></div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!-- Room Occupancy Report -->
			<div class="tile bg-yellow-saffron" id="room_occupancy" data-toggle="tooltip" title="Guests, Room Nights, Available rooms and occupancy per day" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Room Occupancy </div>
			        <div class="number"></div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!-- monthly Occupancy Report -->
			<div class="tile bg-blue" id="monthly_occupancy" data-toggle="tooltip" title="Occupancy per month and in total" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Monthly Occupancy</div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
	<!-- Country Occupancy Report -->
			<div class="tile bg-green-meadow" id="country_occupancy" data-toggle="tooltip" title="Guest Country, booking value from invoice charges" data-placement="top">
			    <div class="tile-body">
			        <i class="fa fa-bar-chart-o"></i>
			    </div>
			    <div class="tile-object">
			        <div class="name"> Country Occupancy</div>
			        <div class="number"> </div>
			        <!-- <div class="number"> 12 </div> -->
			    </div>
			</div>
		<!-- </a>	 -->	
	<!-- </div> -->
</div>

<?php
$this->registerJs("
	$(document).ready(function(){
		$('[data-toggle=\"tooltip\"]').tooltip();
	});
	 
");

?>

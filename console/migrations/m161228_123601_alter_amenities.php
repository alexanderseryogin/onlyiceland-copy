<?php

use yii\db\Migration;

class m161228_123601_alter_amenities extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `amenities` ADD COLUMN `type` INT(11) NULL AFTER `icon`; ");
    }

    public function down()
    {
        echo "m161228_123601_alter_amenities cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

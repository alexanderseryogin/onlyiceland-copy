<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rates_upsell".
 *
 * @property integer $id
 * @property integer $rates_id
 * @property integer $upsell_id
 *
 * @property Rates $rates
 * @property UpsellItems $upsell
 */
class RatesUpsell extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates_upsell';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rates_id', 'upsell_id','price','show_on_frontend', 'discountable', 'commissionable'], 'integer'],
            [['rates_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rates_id' => 'id']],
            [['upsell_id'], 'exist', 'skipOnError' => true, 'targetClass' => UpsellItems::className(), 'targetAttribute' => ['upsell_id' => 'id']],
             [['vat', 'tax', 'fee'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rates_id' => Yii::t('app', 'Rates ID'),
            'upsell_id' => Yii::t('app', 'Upsell ID'),
            'price' => Yii::t('app', 'Price'),
            'discountable' => Yii::t('app', 'Discountable'),
            'commissionable' => Yii::t('app', 'Commissionable'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rates_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpsell()
    {
        return $this->hasOne(UpsellItems::className(), ['id' => 'upsell_id']);
    }
}

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Bookings */

$this->title = Yii::t('app', 'Create New Booking');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bookings-create">

<?php //    <h1>Html::encode($this->title) </h1> ?>

    <?= $this->render('_form', [
        'model' => $model,
        'bookingGroupModel' => $bookingGroupModel,
        'group_name'        => $group_name,  
    ]) ?>

</div>

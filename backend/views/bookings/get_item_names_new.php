<?php
use yii\helpers\ArrayHelper;
use common\models\BookableItemsNames;
?>

<option value=""> - - - - - -</option>
<?php
    $selected_item = $b_item->item_name_id;
    $check = 0;
    if(!empty($availableItemNames))
    {
        if(in_array($selected_item, $availableItemNames))
        {
            $check = 1;
        }
        foreach ($availableItemNames as $key => $value) 
        {
            $model = BookableItemsNames::findOne(['id' => $value]);
            if($check == 1 && $value == $selected_item)
            {
                if(in_array($key, $Org_availableItemNames))
                {
                    $previous = $key;
                    echo '<option data-data="1" value="'.$model->id.'"selected>'.$model->item_name.'</option>';
                }
                else
                {
                    echo '<option data-data="0" value="'.$model->id.'"selected>'.$model->item_name.'</option>';   
                }
            }
            else if($check ==0 )
            {
                if(in_array($key, $Org_availableItemNames))
                {
                    echo '<option  data-data="1" value="'.$model->id.'"selected>'.$model->item_name.'</option>';
                }
                else
                {
                    echo '<option  data-data="0" value="'.$model->id.'"selected>'.$model->item_name.'</option>';
                }
                $check = 1;
            }
            else
            {
                if(in_array($key, $Org_availableItemNames))
                {
                    echo '<option data-data="1" value="'.$model->id.'">'.$model->item_name.'</option>';
                }
                else
                {
                    echo '<option data-data="0" value="'.$model->id.'">'.$model->item_name.'</option>';
                }
            }
            
        }

        //$previous =null;
        // foreach ($item_names as $key => $value) 
        // {
        //     $selected = ($key == $viewDetailsRecord['item_name_id'])?2:0;
        //     if(in_array($key, $availableItemNames))
        //     {
        //         if(($key == $viewDetailsRecord['item_name_id']))
        //         {
        //             $previous = $key;
        //         }
                
                
                
        //         echo '<option data-data="1" value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
        //     }
        //     else
        //     {
        //         echo '<option data-data="0" value="'.$key.'" '.$Options[$selected].'>'.$value.'</option>';
        //     }
        // }
    }
?>
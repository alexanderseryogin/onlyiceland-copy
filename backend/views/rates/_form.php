<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use common\models\Rates;

/* @var $this yii\web\View */
/* @var $model common\models\Providers */
/* @var $form yii\widgets\ActiveForm */
\metronic\assets\BackendUserProfileAsset::register($this);

// ********** Maintain state of previous tabs

if(!empty($tab_href))
{
    $this->registerJs("
        $('.rates_tabs li').each(function()
        {
            var href_val = $(this).find('a').attr('href');
            var tab_activate = '$tab_href';
            $('#tab_href').val(tab_activate);

            if(href_val==tab_activate)
            {
                $(this).addClass('active');
                $(this).find('a').attr('aria-expanded','true');
                $(tab_activate).addClass('active');
            }
            else
            {
                $(this).removeClass('active');
                $(this).find('a').attr('aria-expanded','false');
                $(href_val).removeClass('active');
            }
        });
    ");
}

// ********** Maintain state of amenities

if(isset($model->general_booking_cancellation) && !empty($model->general_booking_cancellation))
{
    $this->registerJs('
            var temp = "'.$model->general_booking_cancellation.'";
            $("#booking_cancellation").val(temp).trigger("change");
        ');
}

if($model->isNewRecord)
{
    $dataProvider = '';
    $savedNights = '';
}

// *********** Maintain state of dynamically created checkboxes
//$discount_codes = !empty($model->discount_code) ? json_encode($model->discount_code,JSON_FORCE_OBJECT) : '';

?>
<script type="text/javascript">
//var discount_code_array = '<?php //$discount_codes ?>';
var isNewRecord = '<?= $model->isNewRecord ?>'; 
</script>

<style>
    .hint-block{
        color:blue;
    }
</style>

<div class="rates-form">

<div class="profile-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Rates</span>
                    </div>
                    <ul class="nav nav-tabs rates_tabs">
                        <li class="active">
                            <a href="#details" data-toggle="tab">General Info</a>
                        </li>
                        <li>
                            <a href="#amenities" data-toggle="tab">Amenities</a>
                        </li>
                        <li>
                            <a href="#conditions" data-toggle="tab">Conditions</a>
                        </li>
                        <li>
                            <a href="#discounts" data-toggle="tab">Discounts</a>
                        </li>
                        <li>
                            <a href="#upsell" data-toggle="tab">Upsell</a>
                        </li>
                    </ul>
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'rates-form',
                    'errorSummaryCssClass' => 'alert alert-danger',
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>

                <?= $form->errorSummary($model); ?>
                <input type="text" name="Rates[tab_href]" id="tab_href" hidden="">

                <div class="portlet-body">
                    <div class="tab-content">
                        
                        <?= $this->render('general_info',
                                [
                                    'form' => $form, 
                                    'model' => $model
                                ])?>

                        <?= $this->render('amenities',
                                [
                                    'model' => $model,
                                    'amenities' => $amenities,
                                ])?>

                        <?= $this->render('conditions',
                                [
                                    'form' => $form, 
                                    'model' => $model
                                ])?>
                        
                        <?= $this->render('discount_codes',
                                [
                                    'model' => $model,
                                    'multiNightDiscount' => $multiNightDiscount,
                                    'dataProvider' => $dataProvider,
                                    'savedNights' => $savedNights,
                                ])?>

                        <?= $this->render('upsell_items',
                                [
                                    'model' => $model
                                ])?>
                    </div>        
                </div>
                <div class="form-group">
                    <?php
                        if(!$model->isNewRecord)
                        { ?>
                            <button type="button" class="btn btn-primary" id="save-btn">Update</button>
                    <?php     }
                    ?>
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['id'=>'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    
                    <a class="btn btn-default" href="<?= Url::to(['/rates']) ?>" >Cancel</a>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>

<?php

$from = '';
$to = '';
if(!empty($model->min_max_booking))
{
    $booking_arr = explode(';', $model->min_max_booking);
    $from = $booking_arr[0];
    $to = $booking_arr[1];
}
else
{
    $from = 1;
    $to = 2;
}

$min_max_rate_period_from = '';
$min_max_rate_period_to = '';

if(!empty($model->rate_period))
{
    $arr= explode(';', $model->rate_period);
    $min_max_rate_period_from = $arr[0];
    $min_max_rate_period_to = $arr[1];
}
else
{
    $min_max_rate_period_from = 1;
    $min_max_rate_period_to = 2;
}

if(!$model->isNewRecord)
{
    $this->registerJs('
        $("#provider-dropdown").prop("disabled", true);
        $("#unit_dropdown").prop("disabled", true);
    ');
}

$this->registerJs("
    function firstAdditionalPricesHtml()
    {
        str = '<div class=\"row\">'+
                    '<div class=\"col-xs-3\">'+
                        '<label class=\"control-label\">Item Price First Adult</label>'+
                        '<input type=\"text\" class=\"form-control first-additional-prices\">'+
                        '<input type=\"text\" name=\"Rates[server_item_price_first_adult]\" hidden>'+
                    '</div>'+
                    '<div class=\"col-xs-3\">'+
                        '<label class=\"control-label\">Item Price Additional Adult</label>'+
                        '<input type=\"text\" class=\"form-control first-additional-prices\">'+
                        '<input type=\"text\" name=\"Rates[server_item_price_additional_adult]\" hidden>'+
                    '</div>'+
                    '<div class=\"col-xs-3\">'+
                        '<label class=\"control-label\">Item Price First Child</label>'+
                        '<input type=\"text\" class=\"form-control first-additional-prices\">'+
                        '<input type=\"text\" name=\"Rates[server_item_price_first_child]\" hidden>'+
                    '</div>'+
                    '<div class=\"col-xs-3\">'+
                        '<label class=\"control-label\">Item Price Additional Child</label>'+
                        '<input type=\"text\" class=\"form-control first-additional-prices\">'+
                        '<input type=\"text\" name=\"Rates[server_item_price_additional_child]\" hidden>'+
                    '</div>'+
                '</div>';
        return str;
    }
    
    function initFirstAdditionalPrices()
    {
        $('.first-additional-prices').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        $('.first-additional-prices').on('change',function()
        {
            $(this).siblings('input').val($(this).inputmask('unmaskedvalue'));
        });
    }
",View::POS_BEGIN);

$this->registerJs("
jQuery(document).ready(function() {
    // handeling Update button //

    $('#save-btn').on('click',function(){
        //alert('clicked');
        var input = $('<input>')
               .attr('type', 'hidden')
               .attr('name', 'save').val('1');
            $('#rates-form').append($(input));
            $('#rates-form').submit();

    });
    
    function initMutiNightPrice()
    {
        $('.multi-night-price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        $('.multi-night-price').on('change',function()
        {
            $(this).siblings('input').val($(this).inputmask('unmaskedvalue'));
        });
    }

    function initMultiNightPercent()
    {
        $('.multi-night-percent').inputmask('999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
        $('.multi-night-percent').on('change',function()
        {
            $(this).siblings('input').val($(this).inputmask('unmaskedvalue'));
        });
    }

    function initMultiNights()
    {
        $('.dynamic-multi-night-dropdown').select2({
            placeholder: \"Select Nights\",
        });
    }

    initMutiNightPrice();
    initMultiNightPercent();
    initMultiNights();
    initFirstAdditionalPrices();

    $('#min_price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});
    $('#max_price').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('#vat_rate').inputmask('99,99',{numericInput:true,showMaskOnFocus: true,rightAlign: true,showMaskOnHover: false,clearMaskOnLostFocus: true});
    $('#vat_rate').on('change',function()
    {
        $(this).parent().siblings('input').val($(this).inputmask().val());
    });

    $('#lodging_tax_per_night').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('#lodging_tax_per_night').on('change',function()
    {
        $(this).parent().siblings('input').val($(this).inputmask('unmaskedvalue'));
    });
    
    $('.upsell_pricing').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

    $('.upsell_pricing').on('change',function()
    {
        $(this).siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

    $('#min_price').on('change',function()
    {
        $(this).parent('div').siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

    $('#max_price').on('change',function()
    {
        $(this).parent('div').siblings('input').val($(this).inputmask('unmaskedvalue'));
    });

    $('#rate_check_all').change(function() 
    {
        if(this.checked)
        {
           $('#rate_clear_all').prop('checked', false);

           $('input[name=\"Rates[rate_allowed][]\"]').each(function()
            {
                $(this).prop('checked', true);
            }); 
        }
    });

    $('#rate_clear_all').change(function() 
    {
        if(this.checked)
        {
            $('#rate_check_all').prop('checked', false);

            $('input[name=\"Rates[rate_allowed][]\"]').each(function()
            {
                $(this).prop('checked', false);
            });
        }
    });

    $('input[name=\"Rates[rate_allowed][]\"]').change(function() 
    {
        $('#rate_check_all').prop('checked', false);
        $('#rate_clear_all').prop('checked', false);
    });

    $('#in_check_all').change(function() 
    {
        if(this.checked)
        {
           $('#in_clear_all').prop('checked', false);

           $('input[name=\"Rates[check_in_allowed][]\"]').each(function()
            {
                $(this).prop('checked', true);
            }); 
        }
    });

    $('#in_clear_all').change(function() 
    {
        if(this.checked)
        {
            $('#in_check_all').prop('checked', false);

            $('input[name=\"Rates[check_in_allowed][]\"]').each(function()
            {
                $(this).prop('checked', false);
            });
        }
    });

    $('input[name=\"Rates[check_in_allowed][]\"]').change(function() 
    {
        $('#in_check_all').prop('checked', false);
        $('#in_clear_all').prop('checked', false);
    });

    $('#out_check_all').change(function() 
    {
        if(this.checked)
        {
           $('#out_clear_all').prop('checked', false);

           $('input[name=\"Rates[check_out_allowed][]\"]').each(function()
            {
                $(this).prop('checked', true);
            }); 
        }
    });

    $('#out_clear_all').change(function() 
    {
        if(this.checked)
        {
            $('#out_check_all').prop('checked', false);

            $('input[name=\"Rates[check_out_allowed][]\"]').each(function()
            {
                $(this).prop('checked', false);
            });
        }
    });

    $('input[name=\"Rates[check_out_allowed][]\"]').change(function() 
    {
        $('#out_check_all').prop('checked', false);
        $('#out_clear_all').prop('checked', false);
    });

    $('#rate_period').ionRangeSlider({
        type:'double',
        grid:!0,
        min: 1,
        max: 1000,
        from: $min_max_rate_period_from,
        to: $min_max_rate_period_to,
    });

    $('#same-day-bookings').ionRangeSlider({
        grid:!0,
        values:['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'],
    });

    $('#provider-dropdown').select2({
        placeholder: \"Select a Destination\",
    });

    $('#booking-status-dropdown').select2({
        placeholder: \"Select a Status\",
        allowClear: true
    });

    $('#booking-flag-dropdown').select2({
        placeholder: \"Select a Flag\",
        allowClear: true
    });

    $('#unit_dropdown').select2({
        placeholder: \"Select an Item\",
        //allowClear: true
    });

    $('#booking_confirmation_type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#booking_cancellation').select2({
        placeholder: \"Select a Guest Policy\",
        allowClear: true,
    });

    var sliderChangedPicker = 0;

    /*$('input[name = \"Rates[last_night_rate]\"]').on('change',function()
    {
        if(sliderChangedPicker==0)
        {
            var a = moment($('#from').val(),'D/M/YYYY');
            var b = moment($(this).val(),'D/M/YYYY');
            var diffDays = b.diff(a, 'days');

            if(diffDays==0)
                diffDays=1;

            // Save slider instance to var
            var slider = $('#min_max_booking').data('ionRangeSlider');

            // Call sliders update method with any params
            slider.update({
                to: diffDays,
            });
        }
        sliderChangedPicker = 0;
    });

    $('#min_max_booking').ionRangeSlider({
        type:'double',
        grid:!0,
        min: 1,
        max: 365,
        from:$from,
        to:$to,

        onFinish: function (data) 
        {
            sliderChangedPicker = 1;
            
            startdate = $('#from').val();
            var new_date = moment(startdate, 'D/M/YYYY').add('days', data.to);

            $('.input-daterange input').each(function() 
            {
                if($(this).attr('id')=='to')
                {
                    $(this).datepicker('setDate',new_date.format('DD/MM/YYYY'));
                }
            });
        },
    });*/

    $('.rates_tabs li').click(function()
    {
        var href = $(this).find('a:first').attr('href');
        $('#tab_href').val(href);

        if(href == '#amenities')
        {
            setTimeout(function(){ 
                if (typeof redrawAmenitiesDataTable == 'function') {
                    redrawAmenitiesDataTable(); 
                }
            }, 500);
        }
        else if(href == '#upsell')
        {
            setTimeout(function(){ 
                if (typeof redrawUpsellItemsDataTable == 'function') {
                    redrawUpsellItemsDataTable(); 
                }
            }, 500);

            $('.upsell_pricing').inputmask('999.999',{numericInput:true,rightAlign: true,showMaskOnFocus: true,showMaskOnHover: false});

            $('.upsell_pricing').on('change',function()
            {
                $(this).siblings('input').val($(this).inputmask('unmaskedvalue'));
            });
        }  
    });

    // **************** Multi Night Script ***************//

    var nights_arr = [];

    for (var i=2; i <= 50 ; i++) 
    { 
        nights_arr[i-2] = i;
    }

    $('.add-dynamic-field').on('click',function()
    {
        if( $('#multi_night_static').val()>1 && ( $('#percent_static').val()>0 || $('#per_night_static').val()>0 || $('#once_off_static').val()>0 || $('#price_cap_static').val()>0 ))
        {
            if($('#percent_static').val()<=100)
            {
                $('#multi_night_dynamic_div').append(multiNightHtml());

                $('#multi_night_static').find('option:selected').prop('disabled',true);
                $('#multi_night_static').val('');
                $('.static-fields').val('');

                initMutiNightPrice();
                initMultiNightPercent();
                initMultiNights();
            }
            else
            {
                alert('Percent field value must be less than or equal to 100');
            }
        }
        else
        {
            alert('You must select a value for Nights AND enter a value in one or more of the other fields before adding.');
        }
    });

    $('.row').on('click','.delete-dynamic-field',function(event)
    {
        var removedOption = $(this).closest('.row').find('.nights-readonly').val();
        $(this).closest('.row').remove();

        $('#multi_night_static').find('option[value=\"'+removedOption+'\"]').prop('disabled',false);
        initMultiNights();
    });

    function multiNightHtml()
    {
        str = '<div class=\"row\">'+
            '<div style=\"margin-top: 20px\"> </div>'+
            '<div class=\"col-sm-2\">'+
            '<input type=\"text\" class=\"form-control nights-readonly\" name=\"Rates[multi_night][nights][]\" value='+$('#multi_night_static').val()+' readonly>'+
            '</div>'+
            '<div class=\"col-sm-2\">'+
            '<input readonly type=\"text\" class=\"form-control multi-night-percent\" value='+$('#percent_static').val()+'>'+
            '<input type=\"text\" value='+$('#percent_static').val()+' name=\"Rates[multi_night][percent][]\" hidden>'+
            '</div>'+
            '<div class=\"col-sm-2\">'+
            '<input readonly type=\"text\" class=\"form-control multi-night-price\" value='+$('#per_night_static').val()+'>'+
            '<input type=\"text\" value='+$('#per_night_static').val()+' name=\"Rates[multi_night][per_night][]\" hidden>'+
            '</div>'+
            '<div class=\"col-sm-2\">'+
            '<input readonly type=\"text\" class=\"form-control multi-night-price\" value='+$('#once_off_static').val()+'>'+
            '<input type=\"text\" value='+$('#once_off_static').val()+' name=\"Rates[multi_night][once_off][]\" hidden>'+
            '</div>'+
            '<div class=\"col-sm-2\">'+
            '<input readonly type=\"text\" class=\"form-control multi-night-price\" value='+$('#price_cap_static').val()+'>'+
            '<input type=\"text\" value='+$('#price_cap_static').val()+' name=\"Rates[multi_night][price_cap][]\" hidden>'+
            '</div>'+
            '<div class=\"col-sm-2\" style=\"margin-top:07px;\" >'+
            '<a class=\"delete-dynamic-field\" href=\"javascript:void(0)\" ><i style=\"color:red;\" class=\"fa fa-times fa-2x\" aria-hidden=\"true\"></i></a>'+
            '</div>'+
            '<br>'+
            '</div>';
        return str;
    }

    var multi_night_row_id='';
    var nights_to_be_delete = '';

    $('a[name=\"multi_night_delete[]\"]').click(function()
    {
        multi_night_row_id = $(this).attr('data-id');
        nights_to_be_delete = $(this).parent().siblings().eq(0).text();
        $('#delete-multi-night-modal').modal('show');
    });

    $('#yes_multi_night').click(function()
    {
        $('#delete-multi-night-modal').modal('hide');

        $.ajax(
        {
            url: 'delete-multi-night?id='+multi_night_row_id,
            success: function(result)
            {
                $('#multi_night_static').find('option[value=\"'+nights_to_be_delete+'\"]').prop('disabled',false);
                nights_to_be_delete = '';
                initMultiNights();
                $.pjax.reload({container:'#multi_night-gridview'});  //Reload GridView
            },
        });
    });

    $('a[name=\"multi_night_update[]\"]').click(function()
    {
        multi_night_row_id = $(this).attr('data-id');
        
        //$('#ml_nights').val($(this).parent().siblings().eq(0).text()).trigger('change');
        $('#ml_percent').val($(this).parent().siblings().eq(1).text());
        $('#ml_per_night').val($(this).parent().siblings().eq(2).text());
        $('#ml_once_off').val($(this).parent().siblings().eq(3).text());
        $('#ml_price_cap').val($(this).parent().siblings().eq(4).text());
        
        $('#update-multi-night-modal').modal('show');
    });

    $('#save_changes_multi_night').click(function()
    {
        if($('#ml_percent').inputmask('unmaskedvalue')<=100)
        {
            $('#update-multi-night-modal').modal('hide');
            App.blockUI({target:\".rates-form\", animate: !0});
            
            var object = {
                            'id' : multi_night_row_id,
                            //'nights' : $('#ml_nights').val(),
                            'percent' : $('#ml_percent').inputmask('unmaskedvalue'),
                            'per_night' : $('#ml_per_night').inputmask('unmaskedvalue'),
                            'once_off' : $('#ml_once_off').inputmask('unmaskedvalue'),
                            'price_cap' : $('#ml_price_cap').inputmask('unmaskedvalue'),
                        };

            $.ajax(
            {
                type: 'POST',
                data: object,
                url: 'update-multi-night',
                success: function(result)
                {
                    $.pjax.reload({container:'#multi_night-gridview'});  //Reload GridView
                    App.unblockUI(\".rates-form\");
                },
                error: function()
                {
                    App.unblockUI(\".rates-form\");
                    alert('error');
                }
            });
        }
        else
        {
            alert('Percent field value must be less than or equal to 100');
        }
    });

    $('#multi_night-gridview').on('pjax:end', function() 
    {
        $('a[name=\"multi_night_delete[]\"]').click(function()
        {
            multi_night_row_id = $(this).attr('data-id');
            nights_to_be_delete = $(this).parent().siblings().eq(0).text();
            $('#delete-multi-night-modal').modal('show');
        });

        $('a[name=\"multi_night_update[]\"]').click(function()
        {
            multi_night_row_id = $(this).attr('data-id');
            
            //$('#ml_nights').val($(this).parent().siblings().eq(0).text()).trigger('change');
            $('#ml_percent').val($(this).parent().siblings().eq(1).text());
            $('#ml_per_night').val($(this).parent().siblings().eq(2).text());
            $('#ml_once_off').val($(this).parent().siblings().eq(3).text());
            $('#ml_price_cap').val($(this).parent().siblings().eq(4).text());
            
            $('#update-multi-night-modal').modal('show');
        });
    });

    $('#from').datepicker('remove');

});",View::POS_END);
?>


<?php

namespace common\models;

use Yii;
use common\models\DestinationTypes;
/**
 * This is the model class for table "providers_sight".
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $owner_id
 * @property integer $region_id
 * @property integer $city_id
 * @property string $address
 * @property string $description
 * @property string $gps_coordinates
 * @property string $phone
 * @property string $website
 * @property string $price_range
 *
 * @property Cities $city
 * @property User $owner
 * @property Providers $provider
 * @property States $region
 */
class DestinationsSight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers_sight';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'owner_id', 'region_id', 'city_id'], 'integer'],
            [['region_id', 'owner_id','address', 'description'], 'required',
            'when' => function ($model) 
            {
                if(isset($model->destination->provider_type_id))
                {
                    $obj = DestinationTypes::findOne(['id' => $model->destination->provider_type_id]);
                    return  $obj->name == 'Sight';
                }
            },
            'whenClient' => "function (attribute, value) {
                return $('#select2-provider-type-dropdown-container').attr('title') == 'Sight';
            }"],

            [['address', 'description', 'gps_coordinates', 'price_range'], 'string'],
            [['phone','postal_code'], 'string', 'max' => 256],
            [['website'], 'string', 'max' => 500],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['provider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Destinations::className(), 'targetAttribute' => ['provider_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider ID'),
            'owner_id' => Yii::t('app', 'Owner'),
            'region_id' => Yii::t('app', 'Region'),
            'city_id' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Address'),
            'description' => Yii::t('app', 'Description'),
            'gps_coordinates' => Yii::t('app', 'GPS Coordinates'),
            'phone' => Yii::t('app', 'Phone'),
            'website' => Yii::t('app', 'Website'),
            'price_range' => Yii::t('app', 'Price Range'),
            'postal_code' => Yii::t('app', 'Postal Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination()
    {
        return $this->hasOne(Destinations::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(States::className(), ['id' => 'region_id']);
    }
}

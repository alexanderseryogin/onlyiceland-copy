<?php

use yii\db\Migration;

class m170303_123113_alter_booking_items_add_user_id extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookings_items` ADD COLUMN `user_id` INT(11) NULL AFTER `master`, ADD CONSTRAINT `bookings_items_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON UPDATE SET NULL ON DELETE SET NULL; 
        ");
    }

    public function down()
    {
        echo "m170303_123113_alter_booking_items_add_user_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

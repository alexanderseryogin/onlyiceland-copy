<?php

use yii\db\Migration;

class m170103_123430_alter_bookable_items_add_max_guests extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `bookable_items` ADD COLUMN `max_guests` INT(11) NULL AFTER `no_of_guests`; ");
    }

    public function down()
    {
        echo "m170103_123430_alter_bookable_items_add_max_guests cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

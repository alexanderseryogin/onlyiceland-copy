<?php
/**  $amenities */
use yii\widgets\LinkPager;
use yii\web\View;
use yii\helpers\Url;
use common\models\DestinationsImages;

$this->title = "Destination's Items";
$slide_path = "//placehold.it/600x400&text=IMAGE+PLACEHOLDER";
$path = '//placehold.it/1200x260&amp;text=IMAGE+PLACEHOLDER';
$destination_image_path = Yii::getAlias('@web').'/uploads/Destinations/'.$destination->id.'/images/';

if(!empty($destination->featured_image))
{
  $path = Yii::getAlias('@web').'/uploads/Destinations/'.$destination->id.'/images/'.$destination->featured_image;
}
$tcount = '';
?>

<script>
var count = 1;
var totalCount = 0;
var flag=true;
var btn_text='';
</script>

  <!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">

              	<div class="col-md-8">
                  <div class="single-property">

                    <h2 class="page-title">116 Waverly Place, <span class="location">New York</span></h2>
                    <div class="price"><strong>$</strong><span>2800 Monthly</span></div>
                    <div class="property-amenities clearfix"> <span class="area"><strong>For</strong>Rent</span> <span class="area"><strong>5000</strong>Area</span> <span class="baths"><strong>3</strong>Baths</span> <span class="beds"><strong>3</strong>Beds</span> <span class="parking"><strong>1</strong>Parking</span> </div>
                    
                    <?php
                        $count = 1;
                        $query = DestinationsImages::find()->where(['provider_id' => $destination->id]);
                        $images = $query->all();
                        $tcount = $query->count();

                        $caption_arr = [];
                        $full_description = []; 

                        if(!empty($images)):
                    ?>

                    <div class="property-slider" >
                      <div id="property-images" class="flexslider">

                        <?= '<div class="slide_text"><strong id="slide_num">['.$count.'/'.$query->count().']</strong></div>' ?>

                        <ul class="slides">
                          <?php

                            if(!empty($images))
                            {
                              foreach ($images as $key => $obj) 
                              {
                                  $description = $obj->description;
                                  $full_description[$count] = $description;

                                  if(strlen($description)>=180)
                                  {
                                    $description = substr($description, 0, 180);
                                    $description = $description.'....';
                                  }

                                  echo'<li style="z-index:-1" class="item frame" data="flexslider" id="slide-'.$count.'">'.
                                              '<img class="slide_image" src="'.$destination_image_path.$obj->image.'" alt="">'.
                                          '</li>';

                                  $caption_arr[$count] = $description;

                                  $count++;
                              }
                              $count = 1;
                            }
                          ?>
                          
                        </ul>

                      </div>
                      <div class="caption">
                          <p title="" id="description">Loading....</p>
                      </div>
                    </div>

                    <div class="thumbnail-grid" style="margin-top: -15px;">
                        <ul class="thumbnail-list">
                          <?php

                            if(!empty($images))
                            {
                              foreach ($images as $key => $obj) 
                              {
                                if($count==1)
                                {
                                  echo '<li class=""><a class="thumbnail selected" id="thumbnail-'.$count.'"> <img src="'.$destination_image_path.'thumb_'.$obj->image.'" ></a></li>';
                                }
                                else if($count>14)
                                {
                                  echo '<li class="hide-thumbnail"><a class="thumbnail" id="thumbnail-'.$count.'"> <img src="'.$destination_image_path.'thumb_'.$obj->image.'" ></a></li>';
                                }
                                else
                                {
                                  echo '<li class=""><a class="thumbnail" id="thumbnail-'.$count.'"> <img src="'.$destination_image_path.'thumb_'.$obj->image.'" ></a></li>';
                                }

                                $count++;
                              }

                              if($count-1>14)
                              {
                                echo '<li class="toggle-thumbnails">
                                        <button> See All '.($count-1).' Photos </button>
                                      </li>';
                              }
                            }

                          ?>
                        </ul>
                    </div>

                    <?php endif ?>

                  </div>
                  <!-- Start Related Properties -->
                  <hr>
                  <div class="property-grid">
                    <ul class="grid-holder col-3">
                      <li class="grid-item type-rent">
                        <div class="property-block"> <a href="#" class="property-featured-image"> <img src="//placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Rent</span> </a>
                          <div class="property-info">
                            <h4><a href="#">116 Waverly Place</a></h4>
                            <span class="location">NYC</span>
                            <div class="price"><strong>$</strong><span>2800 Monthly</span></div>
                          </div>
                          <div class="property-amenities clearfix"> <span class="area"><strong>5000</strong>Area</span> <span class="baths"><strong>3</strong>Baths</span> <span class="beds"><strong>3</strong>Beds</span> <span class="parking"><strong>1</strong>Parking</span> </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>

                <!-- Start Sidebar -->
                <div class="sidebar right-sidebar col-md-4">
                    <div class="widget">
                        <h3 class="widgettitle">Agent</h3>
                        <div class="agent">
                          <img src="//placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt="Mia Kennedy" class="margin-20">
                            <h4><a href="agent-single.html">Mia Keneddy</a></h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vehicula dapibus mauris, quis ullamcorper enim aliquet sed. Maecenas quis eget tellus dui. Vivamus condimentum egestas.</p>
                          <div class="agent-contacts clearfix">
                            <a href="#" class="btn btn-primary pull-right btn-sm">Contact Agent</a>
                              <ul>
                                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                              </ul>
                            </div>
                        </div>
                    </div>
                    <div class="widget">
                        <h3 class="widgettitle">Description</h3>
                        <div id="description">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vehicula dapibus mauris, quis ullamcorper enim aliquet sed. Maecenas quis eget tellus dui. Vivamus condimentum egestas.</p>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa. Ut sed adipiscing enim, pellentesque ultrices erat. Integer placerat felis neque, et semper augue ullamcorper in. Pellentesque iaculis leo iaculis aliquet ultrices. Suspendisse potenti. Aenean ac magna faucibus, consectetur ligula vel, feugiat est. Nullam imperdiet semper neque eget euismod. Nunc commodo volutpat semper.</p>
                        </div>
                     </div>
                    <div class="widget">
                        <h3 class="widgettitle">Additional Amenities</h3>
                        <div id="amenities">
                              <div class="additional-amenities">
                                  <?php foreach($destination->destinationsAmenities as $amenity) {?>

                                          <?php if($amenity->type == 0) { ?>
                                          <span class="available">
                                            <i class="fa fa-check-square"></i>
                                          <?php } elseif($amenity->type == 1) {?>
                                          <span class="am-money">
                                              <i class="fa fa-usd "></i>
                                          <?php } elseif($amenity->type == 2) { ?>
                                          <span class="am-no">
                                              <i class="fa fa-times-circle-o"></i>
                                          <?php } ?>
                                          <?= $amenity->amenities->name;?>
                                      </span>
                                  <?php } ?>
                               </div>
                        </div>
                    </div>
                 </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var caption_arr = <?php echo json_encode($caption_arr,true); ?>;
  var description_arr = <?php echo json_encode($full_description,true); ?>;
</script>

<?php

$bookable_items_Url =  Url::to(['/search/bookable-items'],true);
$count--;

$this->registerJs("
jQuery(document).ready(function() 
{
  $('#description').text(caption_arr[1]);
  $('#description').attr('title',description_arr[1]);
  totalCount = $tcount;

  // ******** Change slide to next ******** //

  function nextSlide()
  {
    if(count<$count)
    {
      $('.thumbnail-list #thumbnail-'+count).removeClass('selected');
      count++;
      $('.thumbnail-list #thumbnail-'+count).addClass('selected');
    }
    else if(count==$count)
    {
      $('.thumbnail-list #thumbnail-'+count).removeClass('selected');
      $('.thumbnail-list #thumbnail-1').addClass('selected');

      $('.slides .flex-active-slide').removeClass('flex-active-slide');
      $('.slides #slide-1').addClass('flex-active-slide');

      $('.flexslider').data(\"flexslider\").flexAnimate(0,true,true);
    
      count=1;
    }
    $('#slide_num').text('['+count+'/'+totalCount+']');

    if(caption_arr[count]!='Not Set')
    {
      $('.caption').show();
      $('#description').text(caption_arr[count]);
      $('#description').attr('title',description_arr[count]);
    }
    else
      $('.caption').hide();
  }

  // ******** Change slide to prev ******** //

  function prevSlide()
  {
    if(count>1)
    {
      $('.thumbnail-list #thumbnail-'+count).removeClass('selected');
      count--;
      $('.thumbnail-list #thumbnail-'+count).addClass('selected');
    }
    else if(count==1)
    {
      $('.thumbnail-list #thumbnail-'+count).removeClass('selected');
      $('.thumbnail-list #thumbnail-'+$count).addClass('selected');

      $('.slides .flex-active-slide').removeClass('flex-active-slide');
      $('.slides #slide-'+$count).addClass('flex-active-slide');

      var index = $('.slides li').index($('.slides li:last'));
      $('.flexslider').data(\"flexslider\").flexAnimate(index,true,true);
    
      count=$count;
    }
    $('#slide_num').text('['+count+'/'+totalCount+']');
    
    if(caption_arr[count]!='Not Set')
    {
      $('.caption').show();
      $('#description').text(caption_arr[count]);
      $('#description').attr('title',description_arr[count]);
    }
    else
      $('.caption').hide();
  }

  // ******** Slider Next ******** //

  $('.flex-next').click(function()
  {
    nextSlide();
  });

  // ******** Slider Prev ******** //

  $('.flex-prev').click(function()
  {
    prevSlide();
  });

  // ******** Thumbnail Click handler ******** //

  $('.thumbnail').click(function()
  {
    var id = $(this).attr('id');
    var arr = id.split('-');

    // add class selected on click thumbnail

    count = arr[1];
    $('.thumbnail-list .selected').removeClass('selected');
    $('.thumbnail-list #'+id).addClass('selected');

    // select slide

    $('.slides .flex-active-slide').removeClass('flex-active-slide');
    $('.slides #slide-'+count).addClass('flex-active-slide');

    var index = $('.slides li').index($('.slides #slide-'+count));
    $('.flexslider').data(\"flexslider\").flexAnimate(index,true,true);

    $('#slide_num').text('['+count+'/'+totalCount+']');
    
    if(caption_arr[count]!='Not Set')
    {
      $('.caption').show();
      $('#description').text(caption_arr[count]);
      $('#description').attr('title',description_arr[count]);
    }
    else
      $('.caption').hide();
  });

  // ******** Button Click handler ******** //

  btn_text = $('.toggle-thumbnails button').text();

  $('.toggle-thumbnails').click(function()
  {
    if(flag==true)
    {
      $('ul').find('.hide-thumbnail').each(function()
      {
        $(this).addClass('show-thumbnail');
        $(this).removeClass('hide-thumbnail');
      });
      $('.toggle-thumbnails button').text('See less');
      flag=false;
    }
    else
    {
      $('ul').find('.show-thumbnail').each(function()
      {
        $(this).addClass('hide-thumbnail');
        $(this).removeClass('show-thumbnail');
      });
      $('.toggle-thumbnails button').text(btn_text);
      flag=true;
    }
  });

  // ******** Key left or right handling ******** //

  $('body').keydown(function(e) 
  {
    if(e.keyCode == 37) // left
    {
      prevSlide();
    }
    else if(e.keyCode == 39) // right
    {
      nextSlide();
    }
  });

  $('#list_view').click(function()
  {

  });

  $('#grid_view').click(function()
  {
    
  });

});",View::POS_END);
?>
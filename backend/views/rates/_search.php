<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rates-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'provider_id') ?>

    <?= $form->field($model, 'booking_confirmation_type_id') ?>

    <?= $form->field($model, 'unit_id') ?>

    <?php // echo $form->field($model, 'rate_period') ?>

    <?php // echo $form->field($model, 'item_price_first_adult') ?>

    <?php // echo $form->field($model, 'item_price_additional_adult') ?>

    <?php // echo $form->field($model, 'item_price_first_child') ?>

    <?php // echo $form->field($model, 'item_price_additional_child') ?>

    <?php // echo $form->field($model, 'first_night_rate') ?>

    <?php // echo $form->field($model, 'last_night_rate') ?>

    <?php // echo $form->field($model, 'min_max_booking') ?>

    <?php // echo $form->field($model, 'rate_allowed') ?>

    <?php // echo $form->field($model, 'check_in_allowed') ?>

    <?php // echo $form->field($model, 'check_out_allowed') ?>

    <?php // echo $form->field($model, 'min_max_stay') ?>

    <?php // echo $form->field($model, 'booking_status_id') ?>

    <?php // echo $form->field($model, 'booking_flag_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

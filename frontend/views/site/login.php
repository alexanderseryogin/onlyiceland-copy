<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Start Content -->
  <div class="main" role="main">
      <div id="content" class="content full">
            <div class="container">
                <div class="page">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="alert alert-default fade in">
                                <h4>Before you Login/Register!</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod sollicitudin nunc, eget pretium massa. Ut sed adipiscing enim, pellentesque ultrices erat. Integer placerat felis neque, et semper augue ullamcorper in. Pellentesque iaculis leo iaculis aliquet ultrices. Suspendisse potenti. Aenean ac magna faucibus, consectetur ligula vel, feugiat est. Nullam imperdiet semper neque eget euismod. Nunc commodo volutpat semper.</p>
                         </div>
                        </div>
                        <div class="col-md-6 col-sm-6 login-form">
                            <h1><?= Html::encode($this->title) ?></h1>

                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                            <?php //echo $form->errorSummary($model); ?>

                                <?= $form->field($model, 'username',
                                    [ 
                                        'template' => "<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>\n{input}\n</div>\n{error}",//\n{error}
                                    ])->textInput(['autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Username']) ?>
                                <br>
                                <?= $form->field($model, 'password',
                                    [ 
                                        'template' => "<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>\n{input}\n</div>\n{error}",//\n{error}
                                    ])->passwordInput(['class' => 'form-control', 'placeholder' => 'Password']) ?>
                                <br>
                                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                                <div style="color:#999;margin:1em 0">
                                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                </div>

                                <div class="form-group">
                                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
<?php

use yii\db\Migration;

class m180222_110710_create_table_custom_report_referers extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `custom_reports_referers`(  
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `travel_partner_id` INT(11),
                          `custom_report_id` INT(11),
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`custom_report_id`) REFERENCES `custom_reports`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180222_110710_create_table_custom_report_referers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180222_110710_create_table_custom_report_referers cannot be reverted.\n";

        return false;
    }
    */
}

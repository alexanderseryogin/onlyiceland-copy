<?php

use yii\db\Migration;

class m180116_121255_create_table_rates_override extends Migration
{
    public function safeUp()
    {
        $this->execute('CREATE TABLE `rates_override`(
                          `id` INT(11) NOT NULL AUTO_INCREMENT,
                          `rate_id` INT(11),
                          `bookable_item_id` INT(11),
                          `date` DATE,
                          `override_type` INT(11),
                          `single` DOUBLE,
                          `double` DOUBLE,
                          `triple` DOUBLE,
                          `quad` DOUBLE,
                          `item_price_first_adult` DOUBLE,
                          `item_price_additional_adult` DOUBLE,
                          `item_price_first_child` DOUBLE,
                          `item_price_additional_child` DOUBLE,
                          PRIMARY KEY (`id`),
                          FOREIGN KEY (`rate_id`) REFERENCES `rates`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
                          FOREIGN KEY (`bookable_item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE
                        ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
                        ');
    }

    public function safeDown()
    {
        echo "m180116_121255_create_table_rates_override cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180116_121255_create_table_rates_override cannot be reverted.\n";

        return false;
    }
    */
}

<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    Use common\models\Cities;
    Use common\models\States;
    Use common\models\Countries;
    Use common\models\User;
    Use common\models\Destinations;
    use common\models\DestinationTypes;
    use yii\helpers\ArrayHelper;
    use yii\web\View;
    use yii\helpers\Url;
    use common\models\AccommodationTypes;
    use common\models\Tags;
    use common\models\UpsellItems;

    $Checkbox = [
        0 => '',
        1 => 'checked'
    ];

    $accommodation_model = AccommodationTypes::findOne(['name' => 'Hotel']);

    if(!empty($provider_model->provider_type_id))
    {
        switch ($provider_model->destinationType->name) 
        {
            case 'Accommodation':
                $this->registerJs("
                    $('.field-accommodation-dropdown').show();
                    $('.offers_accommodation_field').hide();
                ");

                if($provider_model->accommodation_id == $accommodation_model->id)
                {
                    $this->registerJs("
                        $('div').find('.hotel_hide_field').each(function()
                        {
                            $(this).addClass('hotel_show_field');
                            $(this).removeClass('hotel_hide_field');
                        });
                    ");
                }

                break;
            
            case 'Event':
                $this->registerJs("
                    $('div').find('.event_hide_field').each(function()
                    {
                        $(this).addClass('event_show_field');
                        $(this).removeClass('event_hide_field');
                    });
                ");
                break;
        }

    }

    if($provider_model->offers_accommodation)
    {
        $this->registerJs("
                $('div').find('.hide_field').each(function()
                {
                    $(this).addClass('show_field');
                    $(this).removeClass('hide_field');
                });
            ");
    }
    else
    {
        $this->registerJs("
                $('div').find('.show_field').each(function()
                {
                    $(this).addClass('hide_field');
                    $(this).removeClass('show_field');
                }); 
            ");
    }
?>

<style type="text/css">
    .hide_field
    {
        display: none;
    }
    .show_field
    {
        display: block;
    }
    .event_hide_field
    {
        display: none;
    }
    .event_show_field
    {
        display: block;
    }
    .hotel_hide_field
    {
        display: none;
    }
    .hotel_show_field
    {
        display: block;
    }
    .preview_box
    {
        height: 35px; 
        width: 100px;
        font-size: 20px;
        padding-left: 10px;
        padding-top: 4px;
        margin-top: 25px;
    }
</style>

<div class="tab-pane active" id="details">

    <input type="text" name="Destinations[server_phone]" value="" id="server_phone" hidden="">
    <input type="text" name="Destinations[common_server_phone]" value="" id="common_server_phone" hidden="">
    <input type="text" name="Destinations[server_fax]" value="" id="server_fax" hidden="">
    <input type="text" name="Destinations[destination_title]" value="<?=$provider_model->destination_title ?>" id="destination_title" hidden="">

    <div class="row">
        <div class="col-sm-8">
            <?= $form->field($provider_model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4 offers_accommodation_field">
            <label class="control-label" >Offers Accommodation?</label>
            <div class="form-group" >
                <input type="checkbox" id="offers_accommodation" <?= $Checkbox[$provider_model->offers_accommodation] ?> class="make-switch" name="Destinations[offers_accommodation]" data-on-color="success" data-off-color="danger" data-on-text="Yes" data-off-text="No" >
            </div>
        </div>
        <div class="col-sm-4 hotel_hide_field">
            <div class="col-sm-6">
                <label class="control-label">Rating</label>
                <div id="star_rating"></div>
                <input type="text" name="Destinations[star_rating]" id="star_rating_field" hidden="">
            </div>
        </div>
    </div>

    <div class="row event_hide_field">
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'event_place')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($provider_model, 'provider_admin_id', [
                'inputOptions' => [
                    'id' => 'owner-dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_PMANAGER])->all(),'id','username'),['prompt'=>'Select an Owner']) ?>
        </div>
        <div class="col-sm-4 hide_field">
            <?= $form->field($provider_model, 'provider_admin', [
                'inputOptions' => [
                    'id' => 'provider_admin_dropdown',
                    'class' => 'form-control',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_PMANAGER])->all(),'id','username'),['prompt'=>'Select an Admin']) ?>
        </div>
        <div class="col-sm-4 hide_field">
            <?= $form->field($provider_model, 'providers_staff', [
                'inputOptions' => [
                    'id' => 'staff-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(User::find()->where(['type'=> User::USER_TYPE_STAFF])->all(),'id','username')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            
            <?= $form->field($provider_model, 'phone', [
                                            'inputOptions' => [
                                                'id' => 'telephone_number',
                                                'class' => 'form-control',
                                                'style' => 'width: 326px'
                                                ],
                                                'template' => "{label}<br>{input}"
                                            ])->textInput() ?>
            <div id="phone_hint" style="margin-top: -20px; color:red; display: none;">Phone is invalid.</div>
        </div>
        <div class="col-sm-3">
            <?= $form->field($provider_model, 'fax', [
                                            'inputOptions' => [
                                                'id' => 'fax',
                                                'class' => 'form-control',
                                                'style' => 'width: 326px'
                                                ],
                                                'template' => "{label}<br>{input}"
                                            ])->textInput()?>
            <div id="fax_hint" style="margin-top: -20px; color:red; display: none;">Fax is invalid.</div>
        </div>
        <div class="col-sm-3 hide_field">
            <?= $form->field($provider_model, 'kennitala', [
                                        'inputOptions' => [
                                            'id' => 'kennitala-input',
                                            'class' => 'form-control'
                                            ]
                                        ])->textInput(['maxlength' => true])->hint(' e.g. 123456-7890') ?>
        </div>
        <div class="col-sm-3 hide_field">
            <?= $form->field($provider_model, 'vat_id', [
                                        'inputOptions' => [
                                            'id' => 'vat_id-input',
                                            'class' => 'form-control'
                                            ]
                                        ])->textInput(['maxlength' => true]) ?>
        </div>
        
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($provider_model, 'website')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 hide_field">
            <?= $form->field($provider_model, 'booking_email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($provider_model, 'business_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 hide_field">
            <?= $form->field($provider_model, 'providers_tags', [
                'inputOptions' => [
                    'id' => 'tag-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 0])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 hide_field">
            <?= $form->field($provider_model, 'suitable_tags', [
                'inputOptions' => [
                    'id' => 'suitable-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 1])->all(),'id','name')) ?>
        </div>
        <div class="col-sm-6 hide_field">
            <?= $form->field($provider_model, 'difficulty_tags', [
                'inputOptions' => [
                    'id' => 'difficulty-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 2])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row resturant_dropdown_div">
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'upsell_items', [
                'inputOptions' => [
                    'id' => 'upsell_items-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(UpsellItems::find()->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row resturant_dropdown_div" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'resturant_tags', [
                'inputOptions' => [
                    'id' => 'resturant_tags-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 3])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'dish', [
                'inputOptions' => [
                    'id' => 'dish-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 4])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'type', [
                'inputOptions' => [
                    'id' => 'type-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 5])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'dietary_restriction', [
                'inputOptions' => [
                    'id' => 'dietary-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 6])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'meal', [
                'inputOptions' => [
                    'id' => 'meal-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 7])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'price', [
                'inputOptions' => [
                    'id' => 'price-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 8])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'feature', [
                'inputOptions' => [
                    'id' => 'feature-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 9])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'alcohol', [
                'inputOptions' => [
                    'id' => 'alcohol-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 10])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row" >
        <div class="col-sm-12">
            <?= $form->field($provider_model, 'good_for', [
                'inputOptions' => [
                    'id' => 'good_for-dropdown',
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'style' => 'width: 100%',
                    ]
            ])->dropdownList(ArrayHelper::map(Tags::find()->where(['type' => 11])->all(),'id','name')) ?>
        </div>
    </div>

    <div class="row">
                <div class="col-sm-6">

                    <?= $form->field($provider_model, 'address',[
                                'inputOptions' => [
                                    'class' => 'form-control',
                                    'style' => 'resize: none;',
                                    ]
                                ])->textarea(['rows' => 4]) ?>

                    <div class="row">

                        <div class="col-sm-4">

                            <?php

                                if($provider_model->isNewRecord && !isset($provider_model->country_id))
                                {
                                    $this->registerJs("$('#countries-dropdown').val(100).trigger('change');");
                                }
                            ?>
                            
                            <?= $form->field($provider_model, 'country_id', [
                            'inputOptions' => [
                                'id' => 'countries-dropdown',
                                'class' => 'form-control',
                                'style' => 'width: 100%',
                                ]
                            ])->dropdownList(ArrayHelper::map(Countries::find()->all(),'id','name'),[
                                'prompt'=>'Select a Country',
                                'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/liststates?country_id=').'"+$(this).val(), function( data ) 
                                    {
                                      if(flagState==1)
                                      {
                                        $("#states-dropdown").html( data );
                                        $("#states-dropdown").val(state_id).trigger("change");
                                        flagState=0;
                                      }
                                      else
                                      {
                                        $("#states-dropdown").html( data );
                                        $("#states-dropdown").trigger("change");
                                      }

                                    });'
                            ]) ?>      
                        </div>
                        
                        <div class="col-sm-4">
                            <?= $form->field($provider_model, 'state_id', [
                            'inputOptions' => [
                                'id' => 'states-dropdown',
                                'class' => 'form-control',
                                'style' => 'width: 100%',
                                ]
                            ])->dropdownList(ArrayHelper::map(States::find()->where(['country_id'=>$provider_model->country_id])->all(),'id','name'),[
                                'prompt'=>'Select a State',
                                'onchange' => '$.post("'.Yii::$app->urlManager->createUrl('destinations/listcities?state_id=').'"+$(this).val(), function( data ) 
                                    {
                                      if(flagCity==1)
                                      {
                                        $("#cities-dropdown").html( data );
                                        $("#cities-dropdown").val(city_id).trigger("change");
                                        flagCity=0;
                                      }
                                      else
                                      {
                                        $("#cities-dropdown").html( data );
                                      }
                                    });'
                            ]) ?>  
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($provider_model, 'city_id', [
                            'inputOptions' => [
                                'id' => 'cities-dropdown',
                                'class' => 'form-control',
                                'style' => 'width: 100%',
                                ]
                            ])->dropdownList(ArrayHelper::map(Cities::find()->where(['state_id'=>$provider_model->state_id])->all(),'id','name'),[
                                'prompt'=>'Select a City',
                            ]) ?> 
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <?= $form->field($provider_model, 'postal_code')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->field($provider_model, 'latitude')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->field($provider_model, 'longitude')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
                <?= $form->field($provider_model, 'description',[
                                        'inputOptions' => [
                                            'class' => 'form-control',
                                            'style' => 'resize: none;',
                                            ]
                                        ])->textarea(['rows' => 4]) ?>
        </div>
    </div>

    <?php
        if(!empty($provider_model->featured_image))
        {
            echo '<img src='.Yii::getAlias('@web').'/../uploads/Destinations/'.$provider_model->id.'/images/'.$provider_model->featured_image.' height="120" width="150">';
            echo '<br>'; 
        }
    ?>

    <span class="label label-danger"> NOTE! </span>
    <span>&nbsp; Recommended Dimensions (1200x450) </span><br><br>
    <?= $form->field($provider_model, 'image')->fileInput() ?>

    <div class="row">
        <div class="col-sm-12">
            Pricing Model for Bookable Items
            <div class="mt-radio-inline">
                <?php
                    foreach (Destinations::$pricing as $key => $value) 
                    {
                        $checked = 0;

                        if($provider_model->pricing == $key)
                        {
                            $checked = 1;
                        }

                        echo '<label class="mt-radio mt-radio-outline">
                                <input type="radio" name="Destinations[pricing]" '.$Checkbox[$checked].' value="'.$key.'" >'.$value.'
                                <span></span>
                            </label>';
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <label class="control-label" > Use Housekeeping </label>
            <div class="form-group" >
                <input type="checkbox" id="use_housekeeping" class="make-switch" name="Destinations[use_housekeeping]" data-on-color="success" data-off-color="danger" data-on-text="Yes" data-off-text="No" >
            </div>
        </div>

    </div>

    <?php
        $use_housekeeping_value = isset($provider_model->use_housekeeping)?$provider_model->use_housekeeping:0;
        $this->registerJs("
                
                option = $use_housekeeping_value;
                if(option==0)
                {
                    $('#use_housekeeping').bootstrapSwitch('state',false);
                }
                else
                {
                    $('#use_housekeeping').bootstrapSwitch('state',true);
                }
            ");
    ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Destinations;
use common\models\UpsellItems;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\UpsellItems */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .hint-block{
        color:blue;
    }
</style>

<div class="upsell-items-form">
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">

                    <?php $form = ActiveForm::begin([
                        'errorSummaryCssClass' => 'alert alert-danger',
                    ]); ?>
                    <?= $form->errorSummary($model); ?>

                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-xs-6">

                                <?= $form->field($model, 'name') ?>

                                <?= $form->field($model, 'type', [
                                        'inputOptions' => [
                                                        'id' => 'type_dropdown',
                                                        'class' => 'form-control',
                                                        'style' => 'max-width: 500px'
                                                        ]
                                ])->dropdownList(UpsellItems::$types,['prompt'=>'Select a Type']) ?> 
                            </div>

                            <?php
                                if($model->isNewRecord)
                                {
                                    $model->applies_to = 0;
                                    $model->period = 0;
                                }
                            ?>
                            <div class="col-xs-6">

                                <?= $form->field($model, 'applies_to', [
                                            'inputOptions' => [
                                                            'id' => 'applies_dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                    ])->dropdownList(UpsellItems::$applies_to,['prompt'=>'Select an Option']) ?>

                                <?= $form->field($model, 'period', [
                                            'inputOptions' => [
                                                            'id' => 'period_dropdown',
                                                            'class' => 'form-control',
                                                            'style' => 'max-width: 500px'
                                                            ]
                                    ])->dropdownList(UpsellItems::$period,['prompt'=>'Select a Period']) ?>
                            </div>
                                
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                            <?= $form->field($model, 'description',[
                                                        'inputOptions' => [
                                                            'class' => 'form-control',
                                                            'style' => 'resize: none;',
                                                            ]
                                                        ])->textarea(['rows' => 8]) ?>
                            </div>
                        </div>

					    <div class="form-group">
					        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					        <a class="btn btn-default" href="<?= Url::to(['/upsell-items']) ?>" >Cancel</a>
					    </div>

					    <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
jQuery(document).ready(function() {

    $('#type_dropdown').select2({
        placeholder: \"Select a Type\",
        allowClear: true
    });

    $('#applies_dropdown').select2({
        placeholder: \"Select an Option\",
        allowClear: true
    });

    $('#period_dropdown').select2({
        placeholder: \"Select a Period\",
        allowClear: true
    });

});",View::POS_END);
?>

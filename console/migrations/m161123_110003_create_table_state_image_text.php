<?php

use yii\db\Migration;

class m161123_110003_create_table_state_image_text extends Migration
{
    public function up()
    {
        $this->execute("
            DROP TABLE IF EXISTS `state_image_text`
            ");
        $this->execute("            
            CREATE TABLE `state_image_text`( `id` INT(11) NOT NULL AUTO_INCREMENT, `state_id` INT(11), `image` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, `description` TEXT CHARSET utf8 COLLATE utf8_unicode_ci, PRIMARY KEY (`id`), CONSTRAINT `FK_state_image_text_state_id` FOREIGN KEY (`state_id`) REFERENCES `states`(`id`) ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_unicode_ci;
            ");
    }

    public function down()
    {
        echo "m161123_110003_create_table_state_image_text cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

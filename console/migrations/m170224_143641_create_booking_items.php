<?php

use yii\db\Migration;

class m170224_143641_create_booking_items extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `bookings_items`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `booking_id` INT(11) UNSIGNED, `rates_id` INT(11), `item_id` INT(11), `booking_cancellation` VARCHAR(256) CHARSET utf8 COLLATE utf8_unicode_ci, `flag_id` INT(11), `status_id` INT(11), `confirmation_id` INT(11), PRIMARY KEY (`id`), CONSTRAINT `bookings_items_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `bookings`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `bookings_items_rates_id` FOREIGN KEY (`rates_id`) REFERENCES `rates`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `bookings_items_item_id` FOREIGN KEY (`item_id`) REFERENCES `bookable_items`(`id`) ON UPDATE SET NULL ON DELETE CASCADE, CONSTRAINT `bookings_items_flag_id` FOREIGN KEY (`flag_id`) REFERENCES `booking_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, CONSTRAINT `bookings_items_status_id` FOREIGN KEY (`status_id`) REFERENCES `booking_statuses`(`id`) ON UPDATE SET NULL ON DELETE SET NULL, CONSTRAINT `bookings_items_confirmation_id` FOREIGN KEY (`confirmation_id`) REFERENCES `booking_confirmation_types`(`id`) ON UPDATE SET NULL ON DELETE SET NULL ) ENGINE=INNODB CHARSET=utf8; 
        ");
    }

    public function down()
    {
        echo "m170224_143641_create_booking_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
